alter PROCEDURE [dbo].[proc_dsh_atualizarDashboard]

AS

UPDATE DSH_BASE SET PRODUTO = LTRIM(RTRIM(REPLACE(PRODUTO, '(E3S)', '')))
UPDATE DSH_BASE SET PRODUTO = LTRIM(RTRIM(REPLACE(PRODUTO, '(OFH)', '')))   
UPDATE DSH_BASE SET PRODUTO = LTRIM(RTRIM(REPLACE(PRODUTO, '(UKT)', '')))

/* ADICIONANDO PILARES */
INSERT INTO DSH_PILAR (ID_EMPRESA, PILAR)
SELECT 
	V.ID_EMPRESA, 
	D.PILAR 
FROM 
	DSH_BASE D 
LEFT JOIN EMPRESA V   ON V.CODIGO = LEFT(D.SETOR,1)
LEFT JOIN DSH_PILAR P ON P.PILAR = D.PILAR AND P.ID_EMPRESA = V.ID_EMPRESA
WHERE 
	P.PILAR IS NULL AND D.PILAR IS NOT NULL
GROUP BY 
	V.ID_EMPRESA, 
	D.PILAR 
ORDER BY 
	V.ID_EMPRESA, 
	D.PILAR
	
/*ATUALIZANDO OS MESES DOS PILARES*/	
UPDATE DSH_PILAR SET
	MES_ATUAL = B.MES,
	ANO_ATUAL = B.ANO	
FROM DSH_PILAR P
INNER JOIN EMPRESA E ON P.ID_EMPRESA = E.ID_EMPRESA
INNER JOIN DSH_BASE B ON B.PILAR = P.PILAR
WHERE E.CODIGO = LEFT(B.SETOR, 1)	

UPDATE DSH_PILAR SET CABECALHO_PADRAO = 0

UPDATE DSH_PILAR SET
	CABECALHO_PADRAO = 1
FROM DSH_PILAR P INNER JOIN (
	SELECT MIN(P.ID_PILAR) AS ID_PILAR, P.ID_EMPRESA FROM (
		SELECT ID_EMPRESA, MONTH(MAX(CAST(CAST(ANO_ATUAL  AS VARCHAR) + '-' +  CAST(MES_ATUAL  AS VARCHAR) + '-01' AS DATE))) AS MES, YEAR(MAX(CAST(CAST(ANO_ATUAL  AS VARCHAR) + '-' +  CAST(MES_ATUAL  AS VARCHAR) + '-01' AS DATE))) AS ANO FROM DSH_PILAR WHERE MES_ATUAL IS NOT NULL GROUP BY ID_EMPRESA
	) A INNER JOIN DSH_PILAR P ON P.ID_EMPRESA = A.ID_EMPRESA AND P.MES_ATUAL = A.MES AND P.ANO_ATUAL = A.ANO GROUP BY P.ID_EMPRESA
) X ON X.ID_PILAR = P.ID_PILAR
	

/*
SELECT 0, B.PILAR FROM DSH_BASE B LEFT JOIN DSH_PILAR P ON P.PILAR = B.PILAR
WHERE P.PILAR IS NULL GROUP BY B.PILAR
*/

/* ADICIONANDO PRODUTOS */
INSERT INTO DSH_PRODUTO (PRODUTO)
SELECT 
	B.PRODUTO 
FROM 
	DSH_BASE B 
LEFT JOIN 
	DSH_PRODUTO P ON P.PRODUTO = B.PRODUTO
WHERE 
	P.PRODUTO IS NULL 
GROUP BY 
	B.PRODUTO


/* ADICIONANDO FATO */
TRUNCATE TABLE [DSH_FATO]
INSERT INTO [DSH_FATO]
           ([ID_PRODUTO]
           ,[ID_PILAR]
           ,[ID_SETOR]
           ,[MAT01]
           ,[MAT00]
           ,[SEM01]
           ,[SEM00]
           ,[TRM03]
           ,[TRM02]
           ,[TRM01]
           ,[TRM00]
           ,[M12]
           ,[M11]
           ,[M10]
           ,[M09]
           ,[M08]
           ,[M07]
           ,[M06]
           ,[M05]
           ,[M04]
           ,[M03]
           ,[M02]
           ,[M01]
           ,[M00]
           ,[E_MAT01]
           ,[E_MAT00]
           ,[E_SEM01]
           ,[E_SEM00]
           ,[E_TRM03]
           ,[E_TRM02]
           ,[E_TRM01]
           ,[E_TRM00]
           ,[E_M12]
           ,[E_M11]
           ,[E_M10]
           ,[E_M09]
           ,[E_M08]
           ,[E_M07]
           ,[E_M06]
           ,[E_M05]
           ,[E_M04]
           ,[E_M03]
           ,[E_M02]
           ,[E_M01]
           ,[E_M00])
           
SELECT 
	P2.ID_PRODUTO 
	,P1.ID_PILAR 
	,S.ID_SETOR
	,[B].[MAT01]
    ,[B].[MAT00]
    ,[B].[SEM01]
    ,[B].[SEM00]
    ,[B].[TRM03]
    ,[B].[TRM02]
    ,[B].[TRM01]
    ,[B].[TRM00]
	,[B].[M12]
    ,[B].[M11]
    ,[B].[M10]
    ,[B].[M09]
    ,[B].[M08]
    ,[B].[M07]
    ,[B].[M06]
    ,[B].[M05]
    ,[B].[M04]
    ,[B].[M03]
    ,[B].[M02]
    ,[B].[M01]
    ,[B].[M00]
    ,[B].[E_MAT01]
    ,[B].[E_MAT00]
    ,[B].[E_SEM01]
    ,[B].[E_SEM00]
    ,[B].[E_TRM03]
    ,[B].[E_TRM02]
    ,[B].[E_TRM01]
    ,[B].[E_TRM00]
	,[B].[E_M12]
    ,[B].[E_M11]
    ,[B].[E_M10]
    ,[B].[E_M09]
    ,[B].[E_M08]
    ,[B].[E_M07]
    ,[B].[E_M06]
    ,[B].[E_M05]
    ,[B].[E_M04]
    ,[B].[E_M03]
    ,[B].[E_M02]
    ,[B].[E_M01]
    ,[B].[E_M00]
FROM DSH_BASE B
INNER JOIN SETOR S ON S.SETOR = B.SETOR
INNER JOIN DSH_PILAR P1 ON P1.PILAR = B.PILAR AND P1.ID_EMPRESA = LEFT(B.SETOR,1)
INNER JOIN DSH_PRODUTO P2 ON P2.PRODUTO = B.PRODUTO
GROUP BY
	P2.ID_PRODUTO 
	,P1.ID_PILAR 
	,S.ID_SETOR
	,[B].[MAT01]
    ,[B].[MAT00]
    ,[B].[SEM01]
    ,[B].[SEM00]
    ,[B].[TRM03]
    ,[B].[TRM02]
    ,[B].[TRM01]
    ,[B].[TRM00]
	,[B].[M12]
    ,[B].[M11]
    ,[B].[M10]
    ,[B].[M09]
    ,[B].[M08]
    ,[B].[M07]
    ,[B].[M06]
    ,[B].[M05]
    ,[B].[M04]
    ,[B].[M03]
    ,[B].[M02]
    ,[B].[M01]
    ,[B].[M00]
    ,[B].[E_MAT01]
    ,[B].[E_MAT00]
    ,[B].[E_SEM01]
    ,[B].[E_SEM00]
    ,[B].[E_TRM03]
    ,[B].[E_TRM02]
    ,[B].[E_TRM01]
    ,[B].[E_TRM00]
	,[B].[E_M12]
    ,[B].[E_M11]
    ,[B].[E_M10]
    ,[B].[E_M09]
    ,[B].[E_M08]
    ,[B].[E_M07]
    ,[B].[E_M06]
    ,[B].[E_M05]
    ,[B].[E_M04]
    ,[B].[E_M03]
    ,[B].[E_M02]
    ,[B].[E_M01]
    ,[B].[E_M00]
           
           /*
SELECT 
	P2.ID_PRODUTO 
	,P1.ID_PILAR 
	,S.ID_SETOR
	,[B].[MAT01]
    ,[B].[MAT00]
    ,[B].[SEM01]
    ,[B].[SEM00]
    ,[B].[TRM03]
    ,[B].[TRM02]
    ,[B].[TRM01]
    ,[B].[TRM00]
	,[B].[M12]
    ,[B].[M11]
    ,[B].[M10]
    ,[B].[M09]
    ,[B].[M08]
    ,[B].[M07]
    ,[B].[M06]
    ,[B].[M05]
    ,[B].[M04]
    ,[B].[M03]
    ,[B].[M02]
    ,[B].[M01]
    ,[B].[M00]
    ,[B].[E_MAT01]
    ,[B].[E_MAT00]
    ,[B].[E_SEM01]
    ,[B].[E_SEM00]
    ,[B].[E_TRM03]
    ,[B].[E_TRM02]
    ,[B].[E_TRM01]
    ,[B].[E_TRM00]
	,[B].[E_M12]
    ,[B].[E_M11]
    ,[B].[E_M10]
    ,[B].[E_M09]
    ,[B].[E_M08]
    ,[B].[E_M07]
    ,[B].[E_M06]
    ,[B].[E_M05]
    ,[B].[E_M04]
    ,[B].[E_M03]
    ,[B].[E_M02]
    ,[B].[E_M01]
    ,[B].[E_M00]
FROM DSH_BASE B
INNER JOIN SETOR S ON S.SETOR = B.SETOR
INNER JOIN DSH_PILAR P1 ON P1.PILAR = B.PILAR
INNER JOIN DSH_PRODUTO P2 ON P2.PRODUTO = B.PRODUTO
GROUP BY
	P2.ID_PRODUTO 
	,P1.ID_PILAR 
	,S.ID_SETOR
	,[B].[MAT01]
    ,[B].[MAT00]
    ,[B].[SEM01]
    ,[B].[SEM00]
    ,[B].[TRM03]
    ,[B].[TRM02]
    ,[B].[TRM01]
    ,[B].[TRM00]
	,[B].[M12]
    ,[B].[M11]
    ,[B].[M10]
    ,[B].[M09]
    ,[B].[M08]
    ,[B].[M07]
    ,[B].[M06]
    ,[B].[M05]
    ,[B].[M04]
    ,[B].[M03]
    ,[B].[M02]
    ,[B].[M01]
    ,[B].[M00]
    ,[B].[E_MAT01]
    ,[B].[E_MAT00]
    ,[B].[E_SEM01]
    ,[B].[E_SEM00]
    ,[B].[E_TRM03]
    ,[B].[E_TRM02]
    ,[B].[E_TRM01]
    ,[B].[E_TRM00]
	,[B].[E_M12]
    ,[B].[E_M11]
    ,[B].[E_M10]
    ,[B].[E_M09]
    ,[B].[E_M08]
    ,[B].[E_M07]
    ,[B].[E_M06]
    ,[B].[E_M05]
    ,[B].[E_M04]
    ,[B].[E_M03]
    ,[B].[E_M02]
    ,[B].[E_M01]
    ,[B].[E_M00]
*/
DROP TABLE VW_DSH_PAINEL
SELECT * INTO VW_DSH_PAINEL FROM VW_DSH_PAINEL_A
ALTER TABLE VW_DSH_PAINEL ADD ID INT IDENTITY CONSTRAINT PK_ID PRIMARY KEY CLUSTERED
CREATE NONCLUSTERED INDEX [IND_BUSCA] ON [dbo].[VW_DSH_PAINEL] 
(
	[ID_EMPRESA] ASC,
	[PILAR] ASC,
	[PRODUTO] ASC,
	[SETOR] ASC,
	[ID_PILAR] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

  DELETE FROM [DSH_PERMISSAO]WHERE ID_COLABORADOR IN (92,94)
  
  INSERT INTO DSH_PERMISSAO (ID_COLABORADOR, ID_LINHA, ID_PILAR, ID_PRODUTO, ID_ADMIN)
  SELECT DISTINCT 92, ID_LINHA, ID_PILAR, ID_PRODUTO, 94 FROM VW_DSH_PAINEL
  UNION ALL
  SELECT DISTINCT 94, ID_LINHA, ID_PILAR, ID_PRODUTO, 94 FROM VW_DSH_PAINEL

exec proc_dsh_atualizarTabelaMes

DECLARE @ProcName VARCHAR(200)
SET @ProcName = OBJECT_NAME(@@PROCID);  
EXEC _PROC_ATUALIZACAO 'DASHBOARD' , @ProcName
go

