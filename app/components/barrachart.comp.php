<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 

?>


<div class="panel panel-default m-3">
    <div class="panel-body">
        <div id="<?php echo $timestamp ?>" class="charts" style="width: 100%; height: 200px; margin: 0 auto"></div>
    </div>
</div>


<script>

var dom = document.getElementById("<?php echo $timestamp ?>");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '坐标轴刻度与标签对齐';

option = {
    title : {
        textStyle: {
            fontSize:15
        },
        text: 'Representativididade PX / Meso',
        subtext: '',
        left: 'center'
    },
    color: ['#3398DB'],
    tooltip : {
        trigger: 'axis',
        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    xAxis : [
        {
            type : 'category',
            data : ['Mon', 'Tue'],
            axisTick: {
                alignWithLabel: true
            }
        }
    ],
    yAxis : [
        {
            type : 'value'
        }
    ],
    series : [
        {
            name:'直接访问',
            type:'bar',
            barWidth: '60%',
            data:[10, 52]
        }
    ]
};
;
if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
       

</script>
