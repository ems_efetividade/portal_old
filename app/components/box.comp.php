<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 

$timestamp =  (!isset($name)) ? $timestamp : $name;

$data = array(
    'titulo'    => (isset($titulo)    ? $titulo    : 'Titulo do Box'),
    'valor'     => (isset($valor)     ? $valor     : ''),
    'descricao' => (isset($descricao) ? $descricao : ''),
    'subtitulo' => (isset($subtitulo) ? $subtitulo : '')
);





?>


<div id="<?php echo $timestamp ?>" class="panel panel-default m-3" style="fonts-family:arial">
    
    <div class="panel-body">

        <div class="text-center">
            <p class="m-0 p-0"><small><b><?php echo $data['titulo'] ?></b></small></p>
            <p class="m-0 p-0"><small><small><?php echo $data['subtitulo'] ?></small></small></p>
            <p class="m-0 p-0 value" style="font-size:35px;"><strong><?php echo $data['valor'] ?></strong></p>  
            <p class=""><small><?php echo $data['descricao'] ?></small></p>  
        </div>
        

    </div>

</div>

<script>
 $('#<?php echo $timestamp ?> .value').hide().fadeIn();
    

</script>