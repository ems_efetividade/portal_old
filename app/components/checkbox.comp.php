<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 

?>

<input type="text" class="form-control input-sm mb-10 mt-10" placeholder="Pesquisar..." id="input-<?php echo $timestamp ?>">
<div style="max-height: 300px; overflow:auto" class="mb-10 mt-10" id="cont-<?php echo $timestamp ?>"> 


<table>
<tr>
    <td  class="p-3">
        <input type="checkbox" id="chk-all">
    </td>
    <td class="p-3" valign="middle">(SELECIONAR TUDO)</td>
</tr>
<?php foreach($dados as $dado) { ?>
    <tr class="filter" data-name="<?php echo $dado['pesquisar'] ?>">
        <td  class="p-3">
            <input type="checkbox" <?php echo ($dado['checked'] !== false) ? "checked" : '' ?> <?php echo $checked ?> value="<?php echo $dado['valor'] ?>" <?php echo $javascript ?> name="<?php echo $nome ?>">
        </td>
        <td class="p-3" valign="middle"><?php echo $dado['label'] ?></td>
    </tr>
  
<?php } ?>
</table>
</div>

<script>
    
$('#cont-<?php echo $timestamp ?> #chk-all').click(function (e) { 
    $('#cont-<?php echo $timestamp ?> input:checkbox:visible').not(this).prop('checked', this.checked);
});


$('#input-<?php echo $timestamp ?>').on("keyup", function() {
    var valor = $(this).val();
    var value = $(this).val().toLowerCase();
    $("#cont-<?php echo $timestamp ?> .filter").filter(function() {
      $(this).toggle($(this).data('name').toLowerCase().indexOf(value) > -1)
    });
});


</script>


