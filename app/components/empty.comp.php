<?php

$data = array(
    'descricao' => (isset($descricao) ? $descricao : 'Informações não encontradas.'),
    'tam_fonte_icone' => (isset($tam_fonte_icone) ? $tam_fonte_icone : '80'),
    'h_type' => (isset($h_type) ? $h_type : '3'),
);
?>

<div style="color:#e5e5e5" class="text-center m-10">
    <i style="font-size:<?php echo $data['tam_fonte_icone']; ?>px;" class="far fa-frown"></i>
    <h<?php echo $data['h_type']; ?> class="mt-10 text-center">
            <?php echo $data['descricao']; ?>
    </h<?php echo $data['h_type']; ?>>
</div>