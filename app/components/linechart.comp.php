<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 
$timestamp =  (!isset($name)) ? $timestamp : $name;

$obj = array(
    'data'   => isset($data)   ? $data   : 'null',
    'width'  => isset($width)  ? $width  : '100%',
    'height' => isset($height) ? $height : '200px',

    'text' => isset($title) ? $title : 'Pie',
    'subtext' => isset($subtext) ? $subtext : null,
    'title-left' => isset($left) ? $left : null,
);


?>



        <div id="<?php echo $timestamp ?>" class="echart-comp" style="width: <?php echo $obj['width'] ?>; height:<?php echo $obj['height'] ?>; margin: 0 auto"></div>



<script>

var dom = document.getElementById("<?php echo $timestamp ?>");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '坐标轴刻度与标签对齐';

option = {
    title: {
        text: 'Top 5 CNPJs',
        subtext: '',
        /*left: 'center'*/
    },
    tooltip: {
        trigger: 'axis'
    },
    legend: {
        data:['Jan/19','Atual']
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ['Jan', 'Atu']
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: [820, 932, 901, 934, 1290, 1330, 1320],
        type: 'line',
        smooth: true
    },{
        data: [8320, 932, 901, 934, 1290, 1330, 1320],
        type: 'line',
    }]
};


option = <?php echo $option ?>

//if (option && typeof option === "object") {
    myChart.setOption(option, true);
//}
       

</script>
