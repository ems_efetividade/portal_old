<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 18/09/2018
 * Time: 12:02
 */
require_once('lib/appConexao.php');

class mailSender extends appConexao
{

    public function enviar($assunto = null, $nome_remetente = null, $remetente = null, $CCo = null, $destino = null, $msg = null)
    {
        require_once('/../../email/class.phpmailer.php');
        $mail = new PHPMailer();
        $mail->Host = "emsmail.ems.com.br";
        $mail->Port = 25;
        //$mail->AddBCC('jean.carlos@ems.com.br');
        $mail->Subject = utf8_decode($assunto);
        //$mail->ConfirmReadingTo = $remetente;
        if ($assunto == null) {
            $mail->Subject = utf8_decode('PORTAL EFETIVIDADE - [NÃO RESPONDA]');
        }
        $mail->From = $remetente;
        if ($remetente == null) {
            $mail->From = 'portalnoreply@gruponc.br';
        }
        if ($CCo != null) {
            $mail->AddBCC($CCo);
        }
        if ($destino == null || $msg == null) {
            return 0;
        }
        $mail->FromName = $nome_remetente;
        if ($nome_remetente == null) {
            $mail->FromName = 'EMS';
        }
        $mail->addAddress($destino);
        $mail->IsHTML(true);
        $mail->Body = $msg;
        if ($mail->Send()) {
            $this->salvarEnviado($assunto, $nome_remetente, $remetente, $CCo, $destino, $msg);
            return true;
        }
        return false;
    }

    public function salvarEnviado($assunto = null, $nome_remetente = null, $remetente = null, $CCo = null, $destino = null, $msg = null)
    {
        $sql = "INSERT INTO EMAIL_LOG ([ASSUNTO],[NOME_REMETENTE],[REMETENTE],[CCO],[DESTINO],[MENSAGEM]) VALUES ";
        $sql .= "('$assunto','$nome_remetente','$remetente','$CCo','$destino','$msg')";
        $this->executar($sql);
    }

}