<?php

$addConf['paginas']         = ceil($conf['total'] / $conf['max_por_pagina']);
$addConf['pagina_atual']    = (!isset($conf['pagina_atual']) ? 1 : $conf['pagina_atual']);
$addConf['proxima_pagina']  = ($conf['pagina_atual'] <= $addConf['paginas']) ? ($conf['pagina_atual'] + 1) : $addConf['paginas'];
$addConf['pagina_anterior'] = ($conf['pagina_atual'] >= 1) ? ($conf['pagina_atual'] - 1) : 1;

$conf = array_merge($addConf, $conf);

$preg = '/\{(.*?)\}/';
$conf = replaceTags($preg, $conf, $addConf);


$botoes = array(
  'primeiro'  => $conf['primeiro'] , 
  'anterior'  => $conf['anterior'] , 
  'proximo'   => $conf['proximo'] ,
  'ultimo'    => $conf['ultimo'] 
);

foreach($botoes as $key => $botao) {

  if(!isset($botao['label'])) {
    $botao['label'] = ucfirst($key);
  }

  if(!isset($botao['link'])) {
    $botao['link'] = '#';
  }

  $botoes[$key] = $botao;
}

if($addConf['pagina_atual'] >= $addConf['paginas']) {
  $botoes['proximo']['function'] = '';
  $botoes['ultimo']['function'] = '';
}

if($conf['pagina_atual'] <= 1) {
  $botoes['anterior']['function'] = '';
  $botoes['primeiro']['function'] = '';
}


?>


<?php if ($conf['total'] > $conf['max_por_pagina']) { ?>

<nav>
  <ul class="pager">

    <?php if(isset($botoes)) { ?>
    <li class="<?php echo ($conf['pagina_atual'] <= 1) ? 'disabled' : '' ?>">
      <a <?php echo $botoes['primeiro']['function'] ?> href="<?php echo $botoes['primeiro']['link'] ?>">
         <?php echo $botoes['primeiro']['label'] ?>
      </a>
    </li>
    <?php } ?>

    <li class="<?php echo ($conf['pagina_atual'] <= 1) ? 'disabled' : '' ?>">
      <a <?php echo $botoes['anterior']['function'] ?> href="<?php echo $botoes['anterior']['link'] ?>">
        <?php echo $botoes['anterior']['label'] ?>
      </a>
    </li>

    <?php if(isset($conf['paginacao_label'])) { ?>
    <li>
      <?php echo $conf['paginacao_label'] ?>
    </li>
    <?php } ?>

    <li class="<?php echo ($conf['pagina_atual'] >= $addConf['paginas']) ? 'disabled' : '' ?>">
      <a <?php echo $botoes['proximo']['function'] ?> href="<?php echo $botoes['proximo']['link'] ?>">
        <?php echo $botoes['proximo']['label'] ?>
      </a>
    </li>



    <?php if(isset($botoes['ultimo'])) { ?>
    <li class="<?php echo ($conf['pagina_atual'] >= $addConf['paginas']) ? 'disabled' : '' ?>">
      <a <?php echo $botoes['ultimo']['function'] ?> href="<?php echo $botoes['ultimo']['link'] ?>">
        <?php echo $botoes['ultimo']['label'] ?>
      </a>
    </li>
    <?php } ?>
  </ul>
</nav>



    <?php } ?>