<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 


$obj = array(
    'width'  => isset($width)  ? $width  : '100% !important',
    'height' => isset($height) ? $height : '200px',
    'name' => null
);

$timestamp =  (!isset($name)) ? $timestamp : $name;
?>


<div id="<?php echo $timestamp; ?>" class="echart-comp" data-name="<?php echo $name ?>" style="width: <?php echo $obj['width'] ?>; height:<?php echo $obj['height'] ?>; margin: 0 auto"></div>



<script>
    /*
option = {
    title : {
        textStyle: {
            fontSize:15
        },
        text:    '<?php //echo $obj['text'] ?>',
        subtext: '<?php //echo $obj['subtext'] ?>',
        left:    '<?php //echo $obj['title-left'] ?>'
    },
    toolbox: {
        feature: {
            saveAsImage: {
                show: true,
                title: 'Exportar',
                name: 'PxShare'
            }
        }
    },
    tooltip : {
        trigger: 'item',
        textStyle: {
            fontSize:10
        },
        formatter : function(params) {
           param = params;
           return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + param.color + ';"></span> Share: ' + param.data.name;
       },
    },
    
    legend: {
        bottom: 5,
        left: 'center',
        textStyle: {
            fontSize:9
        },
        data:[]    
    },

    series : [
        {
            name: 'Mercado',
            type: 'pie',
            center: ['50%', '50%'],
            radius: '65%',
            data: '<?php //echo $obj['data'] ?>',
            label: {
               show: true,
               normal: {
                    position: 'inner',
                    formatter: '{c}%',
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};
*/

<?php if($option != "") { ?>
var option = <?php echo $option ?>;


var pizza = echarts.init(document.getElementById('<?php echo $timestamp ?>'));
    pizza.setOption(option, true);
    pizza.resize('100%', '300px');



    
<?php } ?>



$(window).on('resize', function(){
        if(pizza != null && pizza != undefined){
            pizza.resize();
        }
    });
</script>
