<?php


?>
<table class="<?php echo $table['class'] ?>">


    <?php if(isset($header)) { ?>
        <thead>
            <tr>
            <?php foreach($header as $head) {  ?>
                <th>
                    <?php echo $head ?>
                </th>
            <?php } ?>
            </tr>
        </thead>
    <?php } ?>


    <?php if(isset($data)) { ?>
        <tbody>
            <?php foreach($data as $row) {  ?>
            <tr>
                <?php foreach($row as $col) {  ?>
                    <?php if(!is_array($col)) { ?>
                        <td>
                            <?php echo $col ?>
                        </td>
                    <?php } else { ?>
                        <td class="<?php echo $col['class'] ?>">
                            <?php echo $col['content'] ?>
                        </td>
                    <?php } ?>
                <?php } ?>
               
            </tr>
            <?php } ?>
        </tbody>
    <?php } ?>

</table>
