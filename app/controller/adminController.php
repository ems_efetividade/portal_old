<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 18/09/2018
 * Time: 13:28
 */

require_once('/../model/mAdmin.php');

class adminController
{

    public function gerarMensagem()
    {
        $model = new mAdmin();
        $mensagem = $_POST['mensagem'];
        $nomeColaborador = $_POST['nomeColaborador'];
        $idColaborador = $_POST['idColaborador'];
        $hora = $_POST['hora'];
        $ultimo = $model->salvarMensagem($mensagem, $nomeColaborador, $idColaborador, $hora);
        $arquivo = "public/ultimo.txt";
        $fp = fopen($arquivo, "w");
        fwrite($fp, $ultimo[1][0]);
        fclose($fp);
    }

    public function fotoColaborador($id)
    {
        $model = new mAdmin();
        echo $model->fotoColaborador($id);
    }

    public function listarMensagem()
    {
        $model = new mAdmin();
        $ultimo = $_POST['ultimo'];
        $arquivo = "public/ultimo.txt";
        $fp = fopen($arquivo, "r");
        $conteudo = fread($fp, filesize($arquivo));
        fclose($fp);
        if ($conteudo > $ultimo) {
            $resultado = $model->listarMensagem($ultimo);
            echo json_encode($resultado);
            return;
        }
        echo 0;
    }

    public function listarRankingTotal()
    {
        $model = new mAdmin();
        $setores = $model->listarDadosSetor();
        $resultado = "public/extracoes/ranking.csv";
        unlink($resultado);
        $fileHandle = fopen($resultado, 'w');
        fwrite($fileHandle, "NOME;SETOR;LINHA;PERFIL;EMAIL;UGN;RNK_REGIONAL;RNK_BRASIL");
        fwrite($fileHandle, "\r\n");
        foreach ($setores as $setor) {
            $ranking = $model->buscarRanking($setor['SETOR']);
            $ranking = $ranking[1];
            fwrite($fileHandle, $setor['NOME'] . ';' . $setor['SETOR'] . ';' . $setor['LINHA'] . ';' . $setor['PERFIL'] . ';' . $setor['EMAIL'] . ';' . $setor['UGN'] . ';' . $ranking['RNK_REGIONAL'] . ';' . $ranking['RNK_BRASIL']);
            fwrite($fileHandle, "\r\n");
        }
        fclose($fileHandle);
        echo "Abacate";
    }

    public function mensagensSFNET()
    {
        error_reporting(E_ALL);
        $directory = "public/sfnet/";
        $resultado = "public/sfnet/Resultado/resultado.csv";
        $diretorio = dir($directory);
        $meuArray = Array();
        $final = Array();
        unlink($resultado);
        while ($arquivo = $diretorio->read()) {
            $extensao = explode('.', $arquivo);
            if ($extensao[count($extensao) - 1] == 'csv') {
                $file = fopen($directory . $arquivo, 'r');
                while (($line = fgetcsv($file)) !== false) {
                    $line = explode(";", $line[0]);
                    $meuArray[$line[1] . '-' . $line[3]][$line[0]] = $line[4] . '|' . $line[2];
                }
            }
        }
        foreach ($meuArray as $key => $value) {
            $i = 0;
            foreach ($value as $chave => $mensagem) {
                if ($i == 0)
                    $final[$key][$chave] = $mensagem;
                $i++;
            }
        }
        $fileHandle = fopen($resultado, 'w');
        foreach ($final as $key => $value) {
            $crm = explode('-', $key);
            foreach ($value as $chave => $mensagem) {
                $arrayMensagem = explode('|', $mensagem);
                //$mensagem = utf8_decode($arrayMensagem[0]);
                $mensagem = $arrayMensagem[0];
                fwrite($fileHandle, $crm[0] . ';' . $arrayMensagem[1] . ';' . $crm[1] . ';' . $mensagem);
                fwrite($fileHandle, "\r\n");
            }
        }
        fclose($fileHandle);
        $assunto = 'Mensagens MKT SFNet';
        $nomeRemetente = 'Portal Efetividade';
        $remetente = 'efetividade@ems.com.br';
        $emailDestino = 'maiara.cavallaro@ems.com.br';
        $Mensagem = 'https://www.novoemsprescricao-qas.com.br/public/sfnet/Resultado/resultado.csv';
        $this->disparaEmail($assunto, $nomeRemetente, $remetente, $CCo, $emailDestino, $Mensagem);
        //crm nome setor mensagem
        echo 'Cabo BB';
        exit;


        exit;
        ini_set('memory_limit', '20000048M');
        $meuArray = Array();
        $file = fopen('Esomex.csv', 'r');
        while (($line = fgetcsv($file)) !== false) {
            $meuArray[] = explode(";", utf8_encode($line[0]));
        }
        echo '<body bgcolor="black" style="color:white">';
        fclose($file);
        unset($file);
        unset($line);
        $tratado = Array();
        $dados = Array();
        ini_set('display_errors', 'Off');
        foreach ($meuArray as $array) {
            $dados[$array[2]][$array[3]] = $array[4];
            $tratado[$array[0]][$array[1]] = $dados;
        }
        echo '</body>';
        var_dump($tratado);
    }

    public function adminController()
    {
        session_start();
    }

    public function main()
    {
        require_once('/../view/vAdmin.php');
        $view = new vAdmin();
        $view->main();
    }

    public function resetar($setor)
    {

        $model = new mAdmin();
        //$retorno = $model->verificaSetor($setor);
        //if ($retorno[1] == null) {
        //    echo "Nenhum Usuário Encontrado";
        //    return;
        //}
        //$nome = $retorno[1]['NOME'];
        //$email = $retorno[1]['EMAIL'];
        //$id_colaborador = $retorno[1]['ID_COLABORADOR'];
        $nova = $model->resetar($setor);
        //$msg = "<p>Olá " . $this->tratarNome($nome) . ",<o:p></o:p></p><p>Acesso ao portal liberado , senha alterada para $nova, por favor acesse o portal e cadastre uma nova senha segura.<o:p></o:p></p><p>Em caso de dúvidas estaremos a disposição.<o:p></o:p></p><p>Atenciosamente Equipe Produtividade e Efetividade.<o:p></o:p></p>";
        //$resultado = $this->disparaEmail("Reset Senha Portal - [Não Responda]", "Portal Efetividade", null, null, $email, $msg);

        //if ($resultado) {
        echo "Operação realizada Com Sucesso";
        //    return;
        //}
        //echo "Ocorreu uma falha";
        return;
    }

    public function token()
    {
        error_reporting(E_ALL);
        $today = date("H:i:s");
        $idColaborador = $_SESSION['id_colaborador'];
        $directory = "public/token.csv";
        $file = fopen($directory, 'r');
        while (($line = fgetcsv($file)) !== false) {
            var_dump($line);
            $line = explode(";", $line[0]);
            if ($line[0] == $idColaborador) {
                fwrite($file, $idColaborador . ';' . $today);
                fwrite($file, "\r\n");
                continue;
            }
            fwrite($file, $line[0] . ';' . $line[1]);
            fwrite($file, "\r\n");
        }

        fclose($file);

//      $today = date("H:i:s");
//      $outra = '17:26:30';
//      $data = new DateTime($outra);
//      $date_time = new DateTime($today);
//      print_r(date_diff($data, $date_time));


    }

    public function disparaEmail($assunto = null, $nome_remetente = null, $remetente = null, $CCo = null, $destino = null, $msg = null)
    {
        require_once('/../components/mailSender.php');
        $sender = new mailSender();
        return $sender->enviar($assunto, $nome_remetente, $remetente, $CCo, $destino, $msg);
    }

    public function limparBasePx()
    {
        $model = new mAdmin();
        $model->limparBasePx();
        echo 0;
    }

    public function rodarBase()
    {
        $model = new mAdmin();
        $model->rodarBase();
        echo 0;
    }

    public function subirPXBanco()
    {
        ini_set("memory_limit", "9999999999M");
        $sql = $_POST['sql'];
        $model = new mAdmin();
        $model->subirPXBanco($sql);
        echo 0;
    }

    public function tratarNome($nome)
    {
        $nome = explode(' ', $nome);
        $nome = strtolower($nome[0]);
        $nome[0] = strtoupper($nome[0]);
        return $nome;
    }

    public function listarAlteracoes($status)
    {
        $model = new mAdmin();
        $retorno = $model->listarAlteracoes($status);
        echo json_encode($retorno);
    }

    public function loadNomeUsuarioByCpf($cpf)
    {
        $model = new mAdmin();
        $retorno = $model->loadNomeUsuarioByCpf($cpf);
        echo $retorno[1][0];
    }

    public function gerarSenha($cpf, $data)
    {
        require_once('cSenhaAdmin.php');
        $model = new mAdmin();
        $id_colaborador = $model->getIdUsuarioByCpf($cpf);
        $cSenhaAdmin = new cSenhaAdmin();
        $str = "abcdefghijklmnopqrstuvxz1234567890ABCDEFGHIJKLMNOPQRSTUVXZ";
        $codigo = substr(str_shuffle($str), 0, 8);
        $md5 = md5($codigo);
        $_POST['senha'] = $md5;
        $_POST['fkusuario'] = $id_colaborador[1][0];
        $_POST['dataexpirar'] = $data;
        $cSenhaAdmin->save();
        echo $codigo;
    }

    public function listarPendenciasLogin()
    {
        $model = new mAdmin();
        $retorno = $model->listarPendenciasLogin();
        echo json_encode($retorno);
    }

    public function criarNovosAcessos()
    {
        $model = new mAdmin();
        $retorno = $model->listarPendenciasLogin();
        $i = 1;
        $msg = '';
        foreach ($retorno as $usuario) {
            $retorno[1]['ACESSO'] = 0;
            $msg = $this->gerarMensagens(1);
            $msg = str_replace('[NOME]', $nomeDestino, $msg);
            echo json_encode($msg);
        }
    }

    public function disparador()
    {
        error_reporting(0);
        ini_set(“display_errors”, 0);
        $assunto = $_POST['assunto'];
        $nomeRemetente = $_POST['nomeRemetente'];
        $remetente = $_POST['remetente'];
        $CCo = $_POST['CCo'];
        $Mensagem = $_POST['Mensagem'];
        $nomeDestino = $this->tratarNome($_POST['nomeDestino']);
        $emailDestino = $_POST['emailDestino'];
        $Mensagem = str_replace('[macronome]', $nomeDestino, $Mensagem);
        $this->disparaEmail($assunto, $nomeRemetente, $remetente, $CCo, $emailDestino, $Mensagem);
        echo "enviado para $emailDestino";
    }

    public function gerarMensagens($tipo)
    {
        $msg = '';
        switch ($tipo) {
            case 1 :
                $msg = "<p>Ol&aacute; [NOME],</p>";
                $msg .= "<p>Identificamos uma altera&ccedil;&atilde;o no seu cadastro no Portal Efetividade.</p>";
                $msg .= "<p>Seus dados foram recebidos pela nossa equipe e j&aacute; estamos trabalhando em&nbsp;todos os sistemas necess&aacute;rios para sua rotina de trabalho.</p>";
                $msg .= "<p>Para acessar o portal acesse [ENDERECO] e utilize o login [USUARIO], senha Inicial no Portal Efetividade &eacute;&nbsp;[SENHA], para sua seguran&ccedil;a nunca compartilhe&nbsp;sua senha com ningu&eacute;m e nem salve em mensagens ou e-mails.</p>";
                $msg .= "<p>Para solicitar seu usu&aacute;rio SFnet entre em contato com o Close-up atrav&eacute;s do&nbsp;&nbsp;<strong>0800 770 8608.</strong></p>";
                $msg .= "<p>Identificamos que voc&ecirc; possui um c&oacute;digo de usu&aacute;rio Mercanet, as informa&ccedil;&otilde;es necess&aacute;rias para a mudan&ccedil;a de setor j&aacute; foram encaminhadas para a equipe HB20, por favor aguarde 7 dias para que seus dados sejam sincronizados com a base Mercanet.</p>";
                $msg .= "<p>Caso o iPad esteja logado com o antigo usu&aacute;rio, coloque estes novos dados e realize a sincroniza&ccedil;&atilde;o completa.</p>";
                $msg .= "<p>&nbsp;</p>";
                $msg .= "<p>Atenciosamente equipe Produtividade e Efetividade</p>";
                break;
        }
        return $msg;

    }

}