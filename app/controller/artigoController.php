<?php

require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/artigoModel.php');

class artigoController extends appController {
	
	private $artigo = null;
	private $funcao = null;
	
	function artigoController() {
		$this->artigo = new artigoModel();
		$this->funcao = new appFunction();	
	}
        

	public function ultimaNoticia() {
		$this->artigo->ultimaNoticia();
	}
	
	public function ler($idArtigo) {
		
		$this->artigo->setIdArtigo($idArtigo);
		$this->artigo->selecionar();		
		$this->artigo->somarVisualizacao();	
		
		$this->set('tituloArtigo', $this->artigo->getTitulo());
		$this->set('resumoArtigo', $this->artigo->getResumo());
		$this->set('escritorArtigo', $this->artigo->getEscritor());
		$this->set('dataArtigo', $this->funcao->formatarData($this->artigo->getData()));
		$this->set('visualizacaoArtigo', $this->artigo->getVisualizacao());
		$this->set('imgArtigo', $this->funcao->validarImagemArtigo($this->artigo->getImg()));
		$this->set('imgWidth', $this->artigo->getImgWidth());
		$this->set('imgHeight', $this->artigo->getImgHeight());
		$this->set('textoArtigo', $this->artigo->getTexto());
		$this->set('fotosArtigo', $this->fotosArtigo($this->artigo->listarFotos()));
		$this->set('listaArtigos', $this->listarArtigosMaisLidos());

		$this->render('artigoView');

	}
	
	public function listar($pagina=1) {
		
		$max = 5;
		
		$totalArt = count($this->artigo->listar());
		$numMaxPagina = round($totalArt / $max);
		
		$prox = $pagina + 1;
		$ant = $pagina - 1;
		
		if($ant <= 0) {
			$ant = 1;
		}
		
		if($prox > $numMaxPagina) {
			$prox = $numMaxPagina;	
		}
		
		$paginacao = '<ul class="pager">
						<li><a href="'.appConf::caminho.'artigo/listar/'.$ant.'">Página Anterior</a></li>
						<li><a href="'.appConf::caminho.'artigo/listar/'.$prox.'">Próxima Página</a></li>
					  </ul>';
		
		
		$rs = $this->artigo->listarPaginacao($max, $pagina);
		$artigo = '';
		
		for($i=1;$i<=count($rs);$i++) {
			
		$artigo .= '<a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($rs[$i]['ID_ARTIGO'], $rs[$i]['TITULO']).'">
						<img  class="pull-left" data-src="holder.js/64x64" src="'.$rs[$i]['IMG'].'" style="width: 180px; height: 120px; margin-right:10px">
					  </a>
					 
						<h4 class="media-heading text-primary"><strong><a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($rs[$i]['ID_ARTIGO'], $rs[$i]['TITULO']).'">'.$rs[$i]['TITULO'].'</a></strong></h4>
						<h4><small>'.$this->funcao->formatarData($rs[$i]['DATA']).' por '.$rs[$i]['ESCRITOR'].' - '.$rs[$i]['VISUALIZACOES'].' visualizações.</small></h4></small></h4>
						<h4 class="text-justify"><p>'.$rs[$i]['RESUMO'].' <a class="abrir-artigo" artigo="'.$rs[$i]['ID_ARTIGO'].'" href="#">(leia mais...)</a></p></h4>
					  
					<hr>';

		}
		
		$this->set('listaArtigo', $artigo.$paginacao);
		$this->render('artigoListaView');
	}
	
	
	
	public function listarArtigosMaisLidos() {
		
		$rs = $this->artigo->listarMaisLidos(10);	
		$artigo = $this->funcao->formatarListaArtigos($rs);
		
		$link = '<a href="'.appConf::caminho.'artigo/listar/" class="pull-right btn btn-primary" id="lista-artigo"><strong><span class="glyphicon glyphicon-plus"></span> Mais Artigos</strong></a>';
		
		return $artigo.$link;
	}
	
	public function fotosArtigo($rs) {
		$modal = '';
		
		if(count($rs) > 0) {
			$fotografo = '';
			$thumbs = '<div class="item active">
						<ul class="list-inline" style="margin-left:5%;">';
			$album = '';
			
			$thumbMax = 7;
			$mMover = $thumbMax+1;
			$nThumb=0;
			
			$pagina = 0;
					
			for($i=1;$i<=count($rs);$i++) {
							
				if($nThumb > $thumbMax) {
					$thumbs .= '</ul></div>
					
								<div class="item">
									<ul class="list-inline" style="margin-left:5%;">';
	
					$nThumb = 0;
					$pagina++;
					
				}
	
				if($i == 1) {
					$itm = 'active';
				} else {
					$itm = '';	
				}
				
				if($rs[$i]['FOTOGRAFO'] != '') {
					$fotografo = 'Foto: '.$rs[$i]['FOTOGRAFO'];
				}
				
				$thumbs .= '<li><a href="#" class="selected car-tumb-'.($i-1).'" data-target="#carousel-example-generic1" data-slide-to="'.($i-1).'"><img class="img-thumbnail" alt="120x120" src="'.appConf::caminho.appConf::folder_artigo_album.$rs[$i]['FOTO'].'" data-holder-rendered="true" style="width: 80px; height: 80px;"></a></li>';
				
				$album .= '
					<div class="item '.$itm.'" pagina="'.$pagina.'">
						<img src="'.appConf::caminho.'public/img/logo_prescricao.png" class="img-responsive" style="position:absolute;top:3%;left:2%" />
						<div style="position:absolute;bottom:0;left:5px;color:#fff"><small>'.$fotografo.'</small></div>
						<img data-src="holder.js/900x500/auto/#777:#555/text:First slide" alt="First slide [900x500]" src="'.appConf::caminho.appConf::folder_artigo_album.$rs[$i]['FOTO'].'" data-holder-rendered="true">
						<div class="carousel-caption">
							<small>'.$rs[$i]['DESCRICAO'].'</small>
							<p><small>(Foto '.$i.' de '.count($rs).')</small></p>
						</div>
					</div>
				';
				
				$nThumb++;
			}
			$thumbs .= '</ul></div>';
			
			$modal = '
			  <h4>Confira as Fotos</h4><div id="carousel-example-generic1" class="carousel slide" data-ride="carousel" max="'.$thumbMax.'">
				  <!--<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic1" data-slide-to="1" class=""></li>
					<li data-target="#carousel-example-generic1" data-slide-to="2" class=""></li>
				  </ol>-->
				  <div class="carousel-inner" role="listbox">
					
							'.$album.'
				  </div>
				  <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>	
				
				
				
				<div id="carousel-example-generic2" class="carousel slide" data-interval="false" data-ride="carousel">
		 
		  <div class="carousel-inner" role="listbox">
			'.$thumbs.'
	
		  </div>
		  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
			';
			
		}
		
		return $modal;
	}
	
	
	
}


?>