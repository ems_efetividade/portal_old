<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 06/11/2018
 * Time: 17:51
 */

class assinaturasController
{
    public function main(){
        session_start();
        $_POST['modulo'] = 0;
        if($_SESSION['nivel_admin']!=2){
            require_once ('/../view/bloqueadoView.php');
            session_destroy();
        }
        require_once ('/../view/vAssinaturas.php');
        new vAssinaturas();
    }
}