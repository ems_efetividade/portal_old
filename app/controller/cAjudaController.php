<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 27/08/2018
 * Time: 11:44
 */
require_once('/../model/mAjuda.php');
require_once('/../view/vAjuda.php');


class cAjudaController
{
    public function cAjudaController()
    {
        session_start();
    }

    public function main()
    {
        $view = new vAjuda();
        $view->main();
    }

    public function listObj()
    {
        $model = new mAjuda();
        return $model->listObj();
    }

    /*ESTOU CRIANDO ISSO SEM OBJETO
    POIS ESTA EM CIMA DA HORA PARA O CAB
    SE VOCÊ ESTÁ LENDO ISSO UTILIZE
    O GERADOR E CRIE O PADRÃO MVC - PAZ*/
    public function salvarLog($id_duvida)
    {
        $id_user = $_SESSION['id_colaborador'];
        $model = new mAjuda();
        $model->salvarLog($id_user, $id_duvida);
    }

    /*ESTOU CRIANDO ISSO SEM OBJETO
    POIS ESTA EM CIMA DA HORA PARA O CAB
    SE VOCÊ ESTÁ LENDO ISSO UTILIZE
    O GERADOR E CRIE O PADRÃO MVC - PAZ*/
    public function getAvaliacao($id)
    {
        $id_user = $_SESSION['id_colaborador'];
        $model = new mAjuda();
        $avaliacao = $model->getAvaliacao($id, $id_user);
        if ($avaliacao == null) {
            return 'text-default';
        } elseif ($avaliacao == 0) {
            return 'text-danger';
        } elseif ($avaliacao == 1) {
            return 'text-success';
        }
    }

    /*ESTOU CRIANDO ISSO SEM OBJETO
    POIS ESTA EM CIMA DA HORA PARA O CAB
    SE VOCÊ ESTÁ LENDO ISSO UTILIZE
    O GERADOR E CRIE O PADRÃO MVC - PAZ*/
    public function salvarRating($parametro)
    {
        $id_user = $_SESSION['id_colaborador'];
        $model = new mAjuda();
        $parametro = explode('-', $parametro);
        $model->salvarRating($id_user, $parametro[0], $parametro[1]);
    }
}