<?php
require_once('lib/appController.php');
require_once('app/model/mAssinaturasDocumentos.php');
require_once('app/view/formsAssinaturasDocumentos.php');

class cAssinaturasDocumentosController extends appController
{

    private $modelAssinaturasDocumentos = null;

    public function __construct()
    {
        $this->modelAssinaturasDocumentos = new mAssinaturasDocumentos();
    }

    public function main()
    {
        //$this->render('vAssinaturasDocumentos');
    }

    public function listUsuarios()
    {
        return $this->modelAssinaturasDocumentos->listUsuarios();
    }

    public function listByLogin()
    {
        return $this->modelAssinaturasDocumentos->listByLogin();
    }

    public function controlSwitch($acao)
    {
        switch ($acao) {
            case 'cadastrar':
                $forms = new formsAssinaturasDocumentos();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsAssinaturasDocumentos();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsAssinaturasDocumentos();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function listarAssinadosByIdEmpresa($usuario)
    {
        $assinados = $this->modelAssinaturasDocumentos->listarAssinadosByIdColaborador($usuario);
        $path = appFunction::caminho_fisico . "public\politicas\assinadas/";
        $diretorio = dir($path);
        $arquivos;
        while ($arquivo = $diretorio->read()) {
            $arqu = explode('.', $arquivo);
            if ($arqu[1] == 'pdf') {
                $arqu = explode('-', $arquivo);
                if ($arqu[0] == $usuario) {
                    $arquivos[] = $arquivo;
                }
            }
        }
        $diretorio->close();
        return $arquivos;
    }

    public function gerarPDF($html, $nome)
    {
        require_once('/../../plugins/mpdf57/mpdf.php');
        require_once('/../../lib/appFunction.php');
        $mpdf = new mPDF();
        $mpdf->SetDisplayMode('fullpage');
        $mpdf->WriteHTML($html);
        $mpdf->Output(appFunction::caminho_fisico . "public/politicas/assinadas/" . $nome, 'F');
    }

    public function getCaptcha()
    {
        $colaborador = $_SESSION['id_colaborador'];
        $font_size = 50; // Font size is in pixels.
        $font_file = appFunction::caminho_fisico . "public/img/times_new_yorker.ttf"; // This is the path to your font file.
        $background = appFunction::caminho_fisico . "public/img/fundo.png";
        $char = 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789';
        $length = mt_rand(5, 12);
        while (strlen($codigoCaptcha) < $length) {
            $codigoCaptcha .= substr($char, mt_rand() % (strlen($char)), 1);
        }
        $_SESSION['captcha'] = $codigoCaptcha;
        $imagemCaptcha = imagecreatefrompng($background);
        $corCaptcha = imagecolorallocate($imagemCaptcha, 0, 0, 0);
        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);
        $type_space = imagettfbbox($font_size, 0, $font_file, $codigoCaptcha);
        // Determine image width and height, 10 pixels are added for 5 pixels padding:
        $image_width = abs($type_space[4] - $type_space[0]) + 10;
        $image_height = abs($type_space[5] - $type_space[1]) + 10;
        $text_pos_x_max = ($bg_width) - ($image_width);
        $text_pos_y_max = ($bg_height) - ($image_height / 2);
        $text_pos_y_min = $bg_height;
        if ($text_pos_y_min > $text_pos_y_max) {
            $temp_text_pos_y = $text_pos_y_min;
            $text_pos_y_min = $text_pos_y_max;
            $text_pos_y_max = $temp_text_pos_y;
        }
        imagettftext($imagemCaptcha,
            $font_size,
            0,
            mt_rand(10, $text_pos_x_max),
            mt_rand($text_pos_y_min, $text_pos_y_max),
            $corCaptcha,
            $font_file,
            $codigoCaptcha);
        imagepng($imagemCaptcha, appFunction::caminho_fisico . "public/img/" . $colaborador . "captcha.png");
        imagedestroy($imagemCaptcha);
    }

    public function imprimirTermo()
    {
        $this->limparPasta();
        require_once('/../model/mRecalBrasart.php');
        $termom = new mRecalBrasart();
        $termos = $termom->listtermo();
        $nomeT = '<h3>RELATÓRIO DE MONITORAÇÃO DAS DISTRIBUIDORAS PARA OS DETENTORES DE REGISTRO</h3>';
        $i = 0;
        set_time_limit('180000000');
        foreach ($termos as $termo) {
            $i++;
            $setor = $termom->loadSetor($termo['FK_USER']);
            $assinatura = $this->criarAssinatura($setor[1]['NOME'], $setor[1]['SETOR'], 1, $nomeT, $termo['DATA_IN'], $termo['ASSINATURA']);
            $this->gerarPDF($termo['TERMO'] . $assinatura, $termo['ID'] . ' - ' . $this->tirarAcentos($setor[1]['NOME']) . ' - ' . $setor[1]['SETOR'] . '.pdf');
        }
        $this->zipar();
        require_once('/../../app/components/mailSender.php');
        $mail = new mailSender();
        //$mail->enviar('Termo Brasart', 'Portal Efetividade [Não Responda]', 'efetividade@ems.com.br', 'luciana.cavaletti@ems.com.br','thais.martho@ncfarma.com.br', 'https://www.novoemsprescricao-qas.com.br/public/politicas/assinadas/backup.zip');
        //$mail->enviar('Termo Brasart', 'Portal Efetividade [Não Responda]', 'efetividade@ems.com.br', 'luciana.cavaletti@ems.com.br','melissafalivene@ncfarma.com.br', 'https://www.novoemsprescricao-qas.com.br/public/politicas/assinadas/backup.zip');
        //$mail->enviar('Termo Brasart', 'Portal Efetividade [Não Responda]', 'efetividade@ems.com.br', 'luciana.cavaletti@ems.com.br','geisa@ncfarma.com.br', 'https://www.novoemsprescricao-qas.com.br/public/politicas/assinadas/backup.zip');
        //$mail->enviar('Termo Brasart', 'Portal Efetividade [Não Responda]', 'efetividade@ems.com.br', 'luciana.cavaletti@ems.com.br','flavia.silva@ncfarma.com.br', 'https://www.novoemsprescricao-qas.com.br/public/politicas/assinadas/backup.zip');
        $mail->enviar('Termo Brasart', 'Portal Efetividade [Não Responda]', 'efetividade@ems.com.br', 'luciana.cavaletti@ems.com.br','maiara.cavallaro@ems.com.br', 'https://www.novoemsprescricao-qas.com.br/public/politicas/assinadas/backup.zip');
        echo $i;
        exit;
        $assinatura = $this->criarAssinatura($setor, $resposta, $documento->getNome(), $date, $codigo);
        $nome_arquivo = $id_colaborador . '-' . $setor . '-' . $date . '.pdf';
        $this->gerarPDF($html . $assinatura, $nome_arquivo);
        var_dump($termos);

        return;
    }

    public function browse($dir)
    {
        $filenames;
        if ($handle = opendir($dir)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != ".." ) {
                    $filenames[] = $dir . '/' . $file;
                } else if ($file != "." && $file != ".." && is_dir($dir . '/' . $file)) {
                    browse($dir . '/' . $file);
                }
            }
            closedir($handle);
        }
        return $filenames;
    }

    public function limparPasta(){
        $directory = "public/politicas/assinadas";
        $zipfile = 'public/politicas/assinadas/backup.zip';
        $filenames = $this->browse($directory);
        unlink($zipfile);
        foreach ($filenames as $filename) {
            $file = $filename;
            $arquivo = substr($file, -3);
            if ($arquivo == "pdf") {
                unlink($file);
            }
        }
    }

    public function zipar()
    {
        $directory = "public/politicas/assinadas";
        $zipfile = 'public/politicas/assinadas/backup.zip';
        $filenames = $this->browse($directory);
        $zip = new ZipArchive();
        if ($zip->open($zipfile, ZIPARCHIVE::CREATE) !== TRUE) {
            exit("Não pode abrir: <$zipfile>\n");
        }
        foreach ($filenames as $filename) {
            $file = $filename;
            $arquivo = substr($file, -3);
            if ($arquivo == "pdf") {
                echo "Arquivo adicionado: <b>" . $filename . "<br/></b>";
                $zip->addFile($file);
            }
        }
        echo "Total de arquivos: <b>" . $zip->numFiles . "</b>\n";
        $zip->close();
    }

    public function criarAssinatura($nome, $setor, $resposta, $documento, $date, $codigo)
    {
        if ($resposta == 1) {
            $cor = 'rgba(122,255,122,0.6)';
            $termos = "Termos e condições aceitas";
        } else if ($resposta == 2) {
            $cor = '#f2dede';
            $termos = "Termos e condições Recusadas";
        }
        $assinatura = '<br><br>';
        $assinatura .= '<div style="background-color:' . $cor . '; padding: 12px; text-align: center">';
        $assinatura .= '<h4><strong>Documento digitalmente assinado</strong></h4>';
        $assinatura .= '<label>' . $nome . '</label>&nbsp; - &nbsp;<label>' . $setor . '</label><br>';
        $assinatura .= '<label>' . $termos . '</label><br>';
        $assinatura .= '<label>' . $documento . '</label><br>';
        $assinatura .= '<label>' . $date . '</label><br>';
        $assinatura .= '<label>Cod. validação: ' . $codigo . '</label><br>';
        $assinatura .= '</div>';
        return $assinatura;
    }

    public function validarResposta($captcha, $resposta, $documento)
    {
        session_start();
        if ($_SESSION['captcha'] != $captcha) {
            echo "Código inválido";
            return;
        }
        if (isset($_SESSION['id_colaborador']))
            $id_colaborador = $_SESSION['id_colaborador'];
        if (isset($_SESSION['id_colaborador']))
            $setor = $_SESSION['setor'];
        $date = date('d-m-Y-hms');
        $codigo = $this->modelAssinaturasDocumentos->validarResposta($id_colaborador, $setor, $resposta, $documento, $date);
        $_POST['id'] = $documento;
        $documento = $this->modelAssinaturasDocumentos->loadObj();
        $html = $documento->getArquivo();
        $assinatura = $this->criarAssinatura($setor, $resposta, $documento->getNome(), $date, $codigo);
        $nome_arquivo = $id_colaborador . '-' . $setor . '-' . $date . '.pdf';
        $this->gerarPDF($html . $assinatura, $nome_arquivo);
        echo $nome_arquivo;
    }

    public function validarRespostaAmostras($captcha, $resposta, $documento)
    {
        require_once('cTermoAnvisaController.php');
        $termo = new cTermoAnvisaController();
        session_start();
        if ($resposta != 2) {
            if ($_SESSION['captcha'] != $captcha) {
                echo "Código inválido";
                return;
            }
        }
        if (isset($_SESSION['id_colaborador']))
            $id_colaborador = $_SESSION['id_colaborador'];
        if (isset($_SESSION['id_colaborador']))
            $setor = $_SESSION['setor'];
        $date = date('d-m-Y-hms');
        $codigo = $termo->validarResposta($id_colaborador, $setor, $resposta, $documento, $date);
        if ($codigo == 1)
            return;
        $documento = $termo->loadTermo($documento);
        $html1 = $documento[1]['TERMO'];
        $html = '<body>';
        $html .= '<h3>RELATÓRIO DE MONITORAÇÃO DAS DISTRIBUIDORAS PARA OS DETENTORES DE REGISTRO</h3>';
        $html .= $html1;
        $assinatura = $this->criarAssinatura($setor, $resposta, 'RELATÓRIO DE MONITORAÇÃO DAS DISTRIBUIDORAS PARA OS DETENTORES DE REGISTRO', $date, $codigo);
        $nome_arquivo = $id_colaborador . '-' . $setor . '-' . $date . '.pdf';
        $this->gerarPDF($html . $assinatura, $nome_arquivo);
        echo $nome_arquivo;
    }

    public function verificaAssinatura($id_Documento)
    {
        $retorno = $this->modelAssinaturasDocumentos->verificaAssinatura($id_Documento);
        if (count($retorno) > 0)
            return true;
        return false;
    }

    public function guardar()
    {
        $id = $_POST['id'];
        $inicio = explode('-', $_POST['inicio']);
        $expira = explode('-', $_POST['expira']);
        $inicio = $inicio[0] . '-' . $inicio[1] . '-' . $inicio[2] . " 00:00:00";
        $expira = $expira[0] . '-' . $expira[1] . '-' . $expira[2] . " 00:00:00";
        $empresa = array_filter(explode('|', $_POST['empresa']));
        $unidade = array_filter(explode('|', $_POST['unidade']));
        $linha = array_filter(explode('|', $_POST['linha']));
        $perfil = array_filter(explode('|', $_POST['perfil']));
        $this->modelAssinaturasDocumentos->guardarPermissoes($id, $inicio, $expira, $empresa, $unidade, $linha, $perfil);
        echo "Salvo Com sucesso!";
    }

    public function salvarArquivos()
    {
        $_POST['nome'] = $this->tirarAcentos($_POST['nome']);
        if ($this->guardarArquivo()) {
            $this->save();
            echo '1';
            return;
        } else {
            echo '2';
        }
    }

    public function carregarPermissoes()
    {
        require_once('/../model/mRelatorios.php');
        $model = new mRelatorios();
        $id_empresa = $_SESSION['id_empresa'];
        $id_un_negocio = $_SESSION['id_un_negocio'];
        $result = $model->carregarPermissoes($id_empresa, $id_un_negocio);
        foreach ($result as $value) {
            $array_permissoes [$value['INDICE']][] = array("ID" => $value['id'], "NOME" => $value['NOME']);
        }
        return $array_permissoes;
    }

    public function viewUpload()
    {
        require_once('/../view/vAssinaturas.php');
        $_POST['modulo'] = 1;
        new vAssinaturas();
    }

    public function viewPermissoes()
    {
        require_once('/../view/vAssinaturas.php');
        $_POST['modulo'] = 2;
        new vAssinaturas();
    }

    public function viewAssinadas()
    {
        require_once('/../view/vAssinaturas.php');
        $_POST['modulo'] = 3;
        new vAssinaturas();
    }

    public function tirarAcentos($string)
    {
        return preg_replace(array("/(ç)/", "/(Ç)/", "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "c C a A e E i I o O u U n N"), $string);
    }

    public function guardarArquivo()
    {
        $arquivo = $_FILES['arquivo'];
        $nome = $_POST['nome'];
        $caminho = appConf::folder_politicas;
        move_uploaded_file($arquivo['tmp_name'], $caminho . $nome);
        $arquivo = $caminho . $nome;
        $linha = '';
        if (file_exists($arquivo)) {
            $arquivo = fopen($caminho . $nome, 'r');
            while (!feof($arquivo)) {
                $linha .= fgets($arquivo, 1024);
            }
            fclose($arquivo);
            $_POST['arquivo'] = $linha;
        }
        $_POST['caminho'] = $caminho . $nome;
        return file_exists($caminho . $nome);
    }

    public function save()
    {
        $nome = $_POST['nome'];
        $this->modelAssinaturasDocumentos->save();
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);
        if (isset($_POST['id']))
            $id = $_POST['id'];
        if (isset($_POST['nome']))
            $nome = $_POST['nome'];
        if (isset($_POST['arquivo']))
            $arquivo = $_POST['arquivo'];
        if (isset($_POST['ativo']))
            $ativo = $_POST['ativo'];
        if (isset($_POST['datain']))
            $dataIn = $_POST['datain'];
        $this->modelAssinaturasDocumentos->delete();

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelAssinaturasDocumentos->listObj();
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelAssinaturasDocumentos->loadObj();
    }

    public function updateObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelAssinaturasDocumentos->updateObj();
    }
}
