<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 03/10/2018
 * Time: 09:31
 */

class cHomeController
{
    public function cHomeController()
    {
        session_start();
    }

    public function getNomeLinhaById($linha)
    {
        require_once('/../model/mHome.php');
        $model = new mHome();
        return $model->getNomeLinhaById($linha);
    }

    public function listPilaresBySetor()
    {
        require_once('/../model/mHome.php');
        $setor = $_SESSION['setor'];
        $model = new mHome();
        return $model->listPilaresBySetor($setor);
    }

    public function listInformativos()
    {
        require_once('/../model/mHome.php');
        $linha = $_SESSION['id_linha'];
        $model = new mHome();
        return $model->listInformativos($linha);
    }

    public function listNoticias()
    {
        require_once('/../model/mHome.php');
        $linha = $_SESSION['id_linha'];
        $model = new mHome();
        return $model->listNoticias($linha);
    }
}