<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 10/07/2018
 * Time: 16:48
 */
require_once('/../model/mRelatorios.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('/../view/formsRelatorios.php');

class cRelatoriosController extends appController
{
    public function cRelatoriosController()
    {
        session_start();
        ini_set('max_execution_time', 0);
    }

    public function getNivelAcessoPasta($idPasta)
    {
        $model = new mRelatorios();
        $retorno = $model->getNivelAcessoPasta($idPasta);
        return $retorno[1][0];
    }

    public function getNomePastaById($idPasta)
    {
        $model = new mRelatorios();
        return $model->getNomePastaById($idPasta);
    }

    public function listRelatoriosByPasta($pasta)
    {
        $model = new mRelatorios();
        $resultado = $model->listRelatoriosByPasta($pasta);
        if (count($resultado) == 0)
            return 0;
        else
            return $resultado;
    }

    public function carregarListaDiretório()
    {
        $forms = new formsRelatorios();
        $forms->carregarListaDiretório();
    }

    public function formExcluirArquivos()
    {
        $forms = new formsRelatorios();
        $forms->formExcluirArquivos();
    }

    public function carregaPasta($idPasta)
    {
        $_SESSION['pasta_atual'] = $idPasta;
        $forms = new formsRelatorios();
        $forms->listagemArquivos($idPasta);
    }

    public function limpaAtual()
    {
        $_SESSION['pasta_atual'] = 0;
    }

    public function formNovaPasta()
    {
        $forms = new formsRelatorios();
        $forms->formNovaPasta();
    }

    public function listarPermissoesById($id)
    {
        $model = new mRelatorios();
        $result = $model->listarPermissoesById($id);
        foreach ($result as $key => $value) {
            if ($value['ID_LINHA'] != 0) {
                if (!in_array($value['ID_LINHA'], $array_linha)) {
                    $array_linha[] = $value['ID_LINHA'];
                }
            }
            if ($value['ID_UNIDADE'] != 0) {
                if (!in_array($value['ID_UNIDADE'], $array_unidade)) {
                    $array_unidade[] = $value['ID_UNIDADE'];
                }
            }
            if ($value['ID_EMPRESA'] != 0) {
                if (!in_array($value['ID_EMPRESA'], $array_empresa)) {
                    $array_empresa[] = $value['ID_EMPRESA'];
                }
            }

            if ($value['ID_USUARIO'] != 0) {
                if (!in_array($value['ID_USUARIO'], $array_user)) {
                    $array_user[] = $value['ID_USUARIO'];
                }
            }
            if ($value['ID_PERFIL'] != 0) {
                if (!in_array($value['ID_PERFIL'], $array_perfil)) {
                    $array_perfil[] = $value['ID_PERFIL'];
                }
            }
        }
        $_POST['empresas'] = $array_empresa;
        $_POST['linhas'] = $array_linha;
        $_POST['unidades'] = $array_unidade;
        $_POST['usuarios'] = $array_user;
        $_POST['perfis'] = $array_perfil;
    }

    public function carregarPermissoes()
    {
        $model = new mRelatorios();
        $id_empresa = $_SESSION['id_empresa'];
        $id_un_negocio = $_SESSION['id_un_negocio'];
        $result = $model->carregarPermissoes($id_empresa, $id_un_negocio);
        foreach ($result as $value) {
            $array_permissoes [$value['INDICE']][] = array("ID" => $value['id'], "NOME" => $value['NOME']);
        }
        return $array_permissoes;
    }

    public function formUpload()
    {
        $forms = new formsRelatorios();
        $forms->formUpload();
    }

    public function formPermissaoPasta($nome, $id)
    {
        $forms = new formsRelatorios();
        $forms->formPermissaoPasta($nome, $id);
    }

    public function formRenomearPasta($nome, $id)
    {
        $forms = new formsRelatorios();
        $nome = str_replace("*", " ", $nome);
        $forms->formRenomearPasta($nome, $id);
    }

    public function formRenomearArqu($nome, $id)
    {
        $forms = new formsRelatorios();
        $forms->formRenomearArqu($nome, $id);
    }

    public function atualizaPasta($nome, $id)
    {
        $nome = str_replace("*", " ", $nome);
        $model = new mRelatorios();
        $model->atualizaPasta($nome, $id);
    }

    public function atualizaArqu($nome, $id)
    {
        $model = new mRelatorios();
        $model->atualizaArqu($nome, $id);
    }

    public function relacionarPasta($id, $pasta, $id_colaborador, $setor)
    {
        $model = new mRelatorios();
        $model->relacionarPasta($id, $pasta, $id_colaborador, $setor);
    }

    public function relacionarPastaUsuario($id, $pasta, $id_colaborador, $setor)
    {
        $model = new mRelatorios();
        $model->relacionarPastaUsuario($id, $pasta, $id_colaborador, $setor);
    }

    public function validaTipo($tipo)
    {
        return $tipo;
    }

    public function tirarAcentos($string)
    {
        return preg_replace(array("/(Ç)/", "/(ç)/", "/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "C c a A e E i I o O u U n N"), $string);
    }

    public function validaCPF($cpf = null)
    {
        // Verifica se um número foi informado
        if (empty($cpf)) {
            return false;
        }
        // Elimina possivel mascara
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);
        // Verifica se o numero de digitos informados é igual a 11
        if (strlen($cpf) != 11) {
            return false;
        }
        // Verifica se nenhuma das sequências invalidas abaixo
        // foi digitada. Caso afirmativo, retorna falso
        else if ($cpf == '00000000000' ||
            $cpf == '11111111111' ||
            $cpf == '22222222222' ||
            $cpf == '33333333333' ||
            $cpf == '44444444444' ||
            $cpf == '55555555555' ||
            $cpf == '66666666666' ||
            $cpf == '77777777777' ||
            $cpf == '88888888888' ||
            $cpf == '99999999999') {
            return false;
            // Calcula os digitos verificadores para verificar se o
            // CPF é válido
        } else {

            for ($t = 9; $t < 11; $t++) {

                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }
                $d = ((10 * $d) % 11) % 10;
                if ($cpf{$c} != $d) {
                    return false;
                }
            }
            return true;
        }
    }

    public function salvarArquivos()
    {
        $model = new mRelatorios();
        $_POST = appSanitize::filter($_POST);
        $nome = $this->tirarAcentos($_POST['nome']);
        $pasta = $_POST['pasta'];
        $tamanho = $_POST['tamanho'];
        $metadados = $_POST['metadados'];
        $identificador = $_POST['identificador'];
        $idResponsavel = $_SESSION['id_colaborador'];
        $id_colaborador = 0;
        $setor = 0;
        switch ($identificador){
            case 1:
                $cpf = substr($nome, 0, 11);
                $id_colaborador = $model->getIdByCpf($cpf);
                break;
            case 2:
                $setor = substr($nome, 0, 8);
                break;
        }
        $extensao = explode('.', $nome);
        $tipo = $extensao[count($extensao) - 1];
        if ($this->guardarArquivo()) {
            $id = $model->salvarArquivos($nome, $tipo, $metadados, $idResponsavel, $tamanho);
            if ($_SESSION['nivel_admin'] > 0) {
                $this->relacionarPasta($id[1][0], $pasta, $id_colaborador, $setor);
            } else {
                $this->relacionarPastaUsuario($id[1][0], $pasta, $id_colaborador, $setor);
            }
            echo 1;
            return;
        }
        echo 2;
    }

    public function excluirPasta($id)
    {
        $model = new mRelatorios();
        $model->excluirPasta($id);
    }

    public function guardarArquivo()
    {
        $arquivo = $_FILES['arquivo'];
        $metadados = $_POST['metadados'];
        $nome = $this->tirarAcentos($_POST['nome']);
        $caminho = appConf::folder_relatorios;
        $extensao = explode('.', $nome);
        $chavearquivo = $metadados . '.' . $extensao[count($extensao) - 1];
        return move_uploaded_file($arquivo['tmp_name'], $caminho . $chavearquivo);
    }

    public function salvarLog($id)
    {
        $model = new mRelatorios();
        return $model->salvarLog($id);
    }

    public function listPastas($raiz)
    {
        $model = new mRelatorios();
        return $model->listPastas($raiz);
    }

    public function excluir()
    {
        $model = new mRelatorios();
        $_POST = appSanitize::filter($_POST);
        $ids = $_POST['id'];
        foreach ($ids as $id) {
            $model->excluir($id);
        }
        echo 1;
    }

    public function listUsuariosByNomeSetorMatricula($parametro)
    {
        $model = new mRelatorios();
        $parametro = str_replace("*", " ", $parametro);
        $retorno = $model->listUsuariosByNomeSetorMatricula($parametro);
        echo json_encode($retorno);
    }

    public function loadUsuariosByID($parametro)
    {
        $model = new mRelatorios();
        $retorno = $model->loadUsuariosByID($parametro);
        echo json_encode($retorno);
    }

    public function atualizarPermissaoPasta()
    {
        $model = new mRelatorios();
        $_POST = appSanitize::filter($_POST);
        $nome_pasta = $_POST['nome_pasta'];
        $id = $_POST['id'];
        $empresa = $_POST['Empresa'];
        $linha = $_POST['Linha'];
        $perfil = $_POST['Perfil'];
        $_POST["id_colaborador"] = $_SESSION['id_colaborador'];
        $model->permissoesPasta($id);
        $this->location(appFunction::caminho . "diretorio");
    }

    public function criarPasta()
    {
        $model = new mRelatorios();
        $_POST['pasta_atual'] = $_SESSION['pasta_atual'];
        $_POST['id_colaborador'] = $_SESSION['id_colaborador'];
        $_POST = appSanitize::filter($_POST);
        $usuario = $_POST['id_colaborador'];
        $nome = $_POST['nome_pasta'];
        $super = $_POST['pasta_atual'];
        $perfil = $_POST["Perfil"];
        $userPermitidos = explode('-', $_POST["userPermitidos"]);
        if ($perfil == null) {
            echo "<h4>Informe pelo menos um perfil de acesso</h4>";
            return;
        }
        if ($nome == '') {
            echo "<h4>Informe o nome do diretório</h4>";
            return;
        }
        $id_pasta = $model->criarPasta();
        $model->permissoesPasta($id_pasta);
    }

    public function usuariosForm()
    {
        $forms = new formsRelatorios();
        $forms->usuariosForm();
    }

    public function usuariosFormEditar()
    {
        $forms = new formsRelatorios();
        $forms->usuariosFormEditar();
    }

    public function setoresForm()
    {
        $forms = new formsRelatorios();
        $forms->setoresForm();
    }

    public function listSubPastaByPasta($pasta)
    {
        $model = new mRelatorios();
        $resultado = $model->listSubPastaByPasta($pasta);
        if (count($resultado) == 0)
            return 0;
        else
            return $resultado;
    }

    public function getSetorLinha($parametro)
    {
        $model = new mRelatorios();
        $retorno = $model->getSetorLinha($parametro);
        echo json_encode($retorno);
    }

}