<?php
require_once('lib/appController.php');
require_once('/../model/mSenhaAdmin.php');
require_once('/../view/formsSenhaAdmin.php');

class cSenhaAdmin extends appController {

    private $modelSenhaAdmin = null;

    public function __construct(){
        $this->modelSenhaAdmin = new mSenhaAdmin();
    }

    public function main(){
        $this->render('vSenhaAdmin');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsSenhaAdmin();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsSenhaAdmin();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsSenhaAdmin();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $this->modelSenhaAdmin->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $this->modelSenhaAdmin->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        return $this->modelSenhaAdmin->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        return $this->modelSenhaAdmin->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $this->modelSenhaAdmin->updateObj();
    }
}
