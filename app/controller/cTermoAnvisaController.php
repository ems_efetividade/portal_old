<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 27/05/2019
 * Time: 16:05
 */

class cTermoAnvisaController
{
    public function salvarTermo(){
        require_once ('/../model/mRecalBrasart.php');
        $model = new mRecalBrasart();
        $termo = $_POST['termo'];
        $arrayLotes = $_POST['arrayLotes'];
        session_start();
        $idUsuario = $_SESSION['id_colaborador'];
        $model->salvarTermo($termo,$idUsuario,$arrayLotes);
        echo 0;
    }

    public function carregaTermo($id){
        require_once ('/../model/mRecalBrasart.php');
        $model = new mRecalBrasart();
        return $model->carregaTermo($id);
    }

    public function loadTermo($id){
        require_once ('/../model/mRecalBrasart.php');
        $model = new mRecalBrasart();
        return $model->loadTermo($id);
    }

    public function validarResposta($id_colaborador, $setor, $resposta, $documento, $date){
        require_once ('/../model/mRecalBrasart.php');
        $model = new mRecalBrasart();
        return $model->validarResposta($id_colaborador, $setor, $resposta, $documento, $date);
    }
}