<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 26/09/2018
 * Time: 16:10
 */
require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/colaboradorModel.php');
require_once('app/model/diretorio2Model.php');
require_once('app/model/noticiaModel.php');
require_once('app/model/noticiaFotosModel.php');
require_once('app/model/ferramentaModel.php');
require_once('app/model/perfilModel.php');
require_once('app/model/linhaModel.php');
require_once('app/model/informativoModel.php');
require_once('app/model/popupModel.php');

class colaboradoresController extends appController
{
    private $colaborador = null;
    private $diretorio = null;
    private $noticia = null;
    private $funcao = null;
    private $noticiaFoto = null;
    private $ferramenta = null;
    private $perfil = null;
    private $linha = null;
    private $informativo = null;
    private $popup = null;

    public function colaboradoresController()
    {
        appFunction::validarSessao();
        $this->colaborador = new colaboradorModel();
        $this->diretorio = new diretorio2Model();
        $this->funcao = new appFunction();
        $this->noticia = new noticiaModel();
        $this->noticiaFoto = new noticiaFotosModel();
        $this->noticiaFoto = new noticiaFotosModel();
        $this->ferramenta = new ferramentaModel();
        $this->perfil = new perfilModel();
        $this->linha = new linhaModel();
        $this->informativo = new informativoModel();
        $this->popup = new popupModel();
    }

    public function main()
    {
        $this->pesquisarColaborador();
    }

    public function colaboradores($pagina = 0, $departamento = '')
    {
        //echo "EXEC proc_listarColaborador '".$this->funcao->dadoSessao('id_setor')."', '".utf8_decode($departamento)."', '".$criterio."";
        $rsCol = $this->colaborador->listarColaborador(utf8_decode(str_replace("-", " ", $departamento)), '');
        $this->set('listaColaborador', $this->formatarListaColaborador($rsCol, $departamento, $pagina));
        $this->set('comboDepartamento', $this->listaDepartamento());
        $this->set('comboExportar', $this->comboExportar());
        $this->set('botaoAdicionar', $this->botaoAdicionar());
        $this->set('tabColaboradores', "active");
        $this->render('listaColaboradoresView');
    }

    public function noticias()
    {
        $noticias = $this->noticia->listar();
        $this->set('listaNoticias', $this->formatarListaNoticias($noticias));
        $this->set('linhas', $this->listarLinha());
        $this->set('tabNoticias', "active");
        require_once('/../view/vComunicacao.php');
        $view = new vComunicacao();
        $this->render('listaNoticiasView');
    }

    public function formFerramenta($idFerramenta = 0)
    {
        $ferramentas = $this->ferramenta->listar();
        $this->ferramenta->setIdFerramenta($idFerramenta);
        $this->ferramenta->selecionar();
        $html = '<option value="0">(Sem Módulo)</option>';
        foreach ($ferramentas as $fer) {
            if ($fer->getNivel() == 0) {
                $html .= '<option value="' . $fer->getIdFerramenta() . '">' . $fer->getFerramenta() . '</option>';
            }
        }
        $arr = array('idFerramenta' => $this->ferramenta->getIdFerramenta(),
            'ferramenta' => $this->ferramenta->getFerramenta(),
            'caminho' => $this->ferramenta->getCaminho(),
            'fornecedor' => $this->ferramenta->getEmpresa(),
            'tipoAcesso' => $this->ferramenta->getTipoAcesso(),
            'tipoAbrir' => $this->ferramenta->getTarget(),
            'descricao' => $this->ferramenta->getDescricao(),
            'nivel' => $this->ferramenta->getNivel(),
            'ativo' => $this->ferramenta->getAtivo(),
            'cmbModulo' => $html);
        echo json_encode($arr);
    }

    public function listarLinha($arrSelecionar = '')
    {

        $this->linha->setIdUnNegocio(appFunction::dadoSessao('id_un_negocio'));
        $linhas = $this->linha->listar();
        $check = '';
        foreach ($linhas as $key => $linha) {

            if ($arrSelecionar == "") {
                $selecionar = 0;
            } else {
                if (in_array($linha->getIdLinha(), $arrSelecionar)) {
                    $selecionar = 1;
                } else {
                    $selecionar = 0;
                }
            }

            $check .= $this->formatarListaLinha($linha, $selecionar);
        }

        return $check;
    }

    public function formatarListaLinha($linha, $selecionar = 0)
    {
        if ($selecionar == 1) {
            $selecionado = 'checked="checked"';
        } else {
            $selecionado = '';
        }
        $check = '<div class="checkbox" style="margin:0px;">
						<label>
						  <input type="checkbox" ' . $selecionado . ' name="chkLinha[]" value="' . $linha->getIdLinha() . '"> 
						  	<h5 style="margin:5px"><small>' . $linha->getNome() . '</small></h5>
						</label>
					  </div>';
        return $check;
    }

    public function listarPermissao()
    {
        $idFerramenta = $_POST['idFerramenta'];
        $arrPerfil = array();
        $this->ferramenta->setIdFerramenta($idFerramenta);
        $rs = $this->ferramenta->listarPermissoes();
        for ($i = 1; $i <= count($rs); $i++) {
            $perfil = new perfilModel();
            $perfil->setIdPerfil($rs[$i]['ID_PERFIL']);
            $perfil->selecionar();

            $arrPerfil[] = $perfil;
        }
        echo $this->formatarListaPerfil($arrPerfil);
    }

    public function listarPerfilCompleto()
    {
        $perfis = $this->perfil->listar();
        echo $this->formatarListaPerfil($perfis);
    }

    public function formatarListaPerfil($perfis)
    {
        $lista = '';
        foreach ($perfis as $key => $perfil) {
            $lista .= '<div class="perfil">
						<div class="checkbox" style="margin:0px;">
						<label>
						  <input type="checkbox"  value="' . $perfil->getIdPerfil() . '"> <h5 style="margin:5px"><small>' . $perfil->getPerfil() . '</small></h5>
						  <input type="hidden" name="permissao[]" value="' . $perfil->getIdPerfil() . '" />
						</label>
					  </div>
					</div>';
        }

        return $lista;
    }

    public function salvarPermissaoFerramenta()
    {
        $idFerramenta = $_POST['txtIdFerramenta'];
        $permissao = $_POST['permissao'];
        $excluir = $_POST['permExcluir'];
        $this->ferramenta->setIdFerramenta($idFerramenta);
        for ($i = 0; $i < count($permissao); $i++) {
            $this->ferramenta->setPermissao($permissao[$i]);
        }

        for ($i = 0; $i < count($excluir); $i++) {
            $this->ferramenta->setPermissaoExcluir($excluir[$i]);
        }
        $this->ferramenta->salvarPermissao($excluir);
        $this->reload('ferramenta');

    }

    public function pesquisarColaborador()
    {
        $criterio = $_POST['txtCriterio'];

        $rs = $this->colaborador->listarColaborador('', $criterio);
        $this->set('listaColaborador', $this->formatarListaColaborador($rs, $pagina, 0));
        $this->set('comboDepartamento', $this->listaDepartamento());
        $this->set('comboExportar', $this->comboExportar());
        $this->set('botaoAdicionar', $this->botaoAdicionar());
        $this->render('listaColaboradoresView');
    }

    public function formatarListaColaborador($rs, $departamento, $pagina = 0, $mostrarPaginacao = 1)
    {

        $max = 10;
        $totalRegistros = count($rs);
        $totalpag = ceil($totalRegistros / $max);

        if ($pagina <= 0) {
            $inicio = 1;
        } else {
            $inicio = ($pagina * $max) + 1;
        }

        if ($mostrarPaginacao == 1) {
            $maximo = $inicio + $max;
        } else {
            $maximo = $totalRegistros;
        }

        $tabela = '<table class="table table-striped table-condensed" style="font-size:12px">
					<tr>
						<th>ID</th>
						<th>COLABORADOR</th>
						<th>DEPARTAMENTO</th>
						<th>FONE CORP.</th>
						<th>FONE PART.</th>
						<th>EMAIL</th>
						<th>&nbsp;</th>
					</tr>';

        for ($i = $inicio; $i < $maximo; $i++) {
            if ($rs[$i]['SETOR'] . $rs[$i]['NOME'] != "") {
                $tabela .= '<tr>
								<td>' . $rs[$i]['ID'] . '</td>
								<td><img style="height:40px" src="' . appFunction::fotoColaborador($rs[$i]['ID_COLABORADOR']) . '" />&nbsp;' . $rs[$i]['SETOR'] . ' ' . $rs[$i]['NOME'] . '</td>
								<td>' . $rs[$i]['PERFIL'] . '</td>
								<td>(' . $rs[$i]['DDD_CORPORATIVO'] . ') ' . $rs[$i]['F_CORPORATIVO'] . '</td>
								<td>(' . $rs[$i]['DDD_PARTICULAR'] . ') ' . $rs[$i]['F_PARTICULAR'] . '</td>
								<td>' . $rs[$i]['EMAIL'] . '</td>
								<td>
									<div class="pull-right">
										' . $this->botoes($rs[$i]['NIVEL'], $rs[$i]['ID_COLABORADOR']) . '
									</div>
								</td>
							</tr>';
            }
        }

        $tabela .= '</table>';

        $paginaAnterior = ($pagina - 1);
        $proximaPagina = ($pagina + 1);

        if ($paginaAnterior <= 0) {
            $linkAnterior = '#';
        } else {
            $linkAnterior = appConf::caminho . 'comunicacao/colaboradores/' . $paginaAnterior . '/' . $departamento;
        }


        if ($proximaPagina == $totalpag) {
            $linkProximo = '#';
        } else {
            $linkProximo = appConf::caminho . 'comunicacao/colaboradores/' . $proximaPagina . '/' . $departamento;
        }


        $paginacao = '<nav>
					  <ul class="pager">
					  	<li><a href="' . appConf::caminho . 'comunicacao/colaboradores/0/' . $departamento . '">Primeiro</a></li>
						<li><a href="' . $linkAnterior . '">Anterior</a></li>
						<li><small>Pagina ' . ($pagina + 1) . ' de ' . $totalpag . '</small></li>
						<li><a href="' . $linkProximo . '">Próximo</a></li>
						<li><a href="' . appConf::caminho . 'comunicacao/colaboradores/' . ($totalpag - 1) . '/' . $departamento . '">Último</a></li>
					  </ul>
					</nav>';


        if ($mostrarPaginacao == 1) {
            return $tabela . $paginacao;
        } else {
            return $tabela;
        }
    }

    public function visualizarColaborador()
    {

        $idColaborador = $_POST['idColaborador'];
        $this->colaborador->setId($idColaborador);
        $this->colaborador->selecionar();


        $html = '<!--foto e dados pessoals-->
			<div class="row">
			
				<div class="col-lg-3">
					<img  class="img-responsive" src="' . $this->funcao->fotoColaborador($idColaborador) . '"  />
				</div>
				
				<div class="col-lg-9">
					<div class="row">
						<div class="col-lg-12">
							<h3 style="margin-top:2px;"><b>' . $this->colaborador->getNome() . '</b></h3>
							<hr />
						</div>
						
						
						<div class="col-lg-6">
							<p><strong>ID: </strong>' . $this->colaborador->getIdColaborador() . '</p>
							<p><strong>Matrícula: </strong>' . $this->colaborador->getMatricula() . '</p>
							<p><strong>Departamento: </strong>' . $this->colaborador->getNomeDepartamento() . '</p>
							<p><strong>Função: </strong>' . $this->colaborador->getFuncao() . '</p>
						</div>
						
						<div class="col-lg-6">
							
								<p><strong>Endereço: </strong>' . $this->colaborador->getEndereco() . ', ' . $this->colaborador->getNumero() . ' - ' . $this->colaborador->getComplemento() . '</p>
								<p><strong>Bairro: </strong>' . $this->colaborador->getBairro() . '</p>
								<p><strong>Cidade: </strong>' . $this->colaborador->getCidade() . '/' . $this->colaborador->getUf() . '</p>
								<p><strong>Cep: </strong>' . $this->colaborador->getCep() . '</p>
							
						</div>
					</div>
					
					
					<div class="row">
				<div class="col-lg-12">
					<h4><strong><span class="glyphicon glyphicon-earphone"></span> Contatos</strong></h4>
					<hr />
					<div class="row">
					 <div class="col-lg-6">
						<p><strong>Fone Residencial:</strong> (' . $this->colaborador->getDDDFoneRes() . ') ' . $this->colaborador->getFoneRes() . '</p>
						</div>
						<div class="col-lg-6">
						<p><strong>Cel. Pessoal:</strong> (' . $this->colaborador->getDDDCelPess() . ') ' . $this->colaborador->getCelPess() . '</p></div>
						<div class="col-lg-6">
						<p><strong>Cel. Corporativo:</strong> (' . $this->colaborador->getDDDCelCorp() . ') ' . $this->colaborador->getCelCorp() . '</p>
						</div>
						<div class="col-lg-6">
						<p><strong>Email:</strong> ' . $this->colaborador->getEmail() . '</p>
						</div>
						<div class="col-lg-12">
						<p><strong>Aeroporto:</strong> ' . $this->colaborador->getAeroporto() . '</p>
						</div>
					  </div>
				</div>
				
				<div class="col-lg-12">
					<h4><strong><span class="glyphicon glyphicon-home"></span> Dados Pessoais</strong></h4>
					<hr />
					 <div class="row">
						<div class="col-lg-4">
							<p><strong>Nascimento:</strong> ' . $this->funcao->formatarData($this->colaborador->getDataNasc()) . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Est. Civil:</strong> ' . $this->colaborador->getEstadoCivil() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>CPF:</strong> ' . $this->colaborador->getCPF() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>RG:</strong> ' . $this->colaborador->getRG() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Camiseta:</strong> ' . $this->colaborador->getCamiseta() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Sapato:</strong> ' . $this->colaborador->getSapato() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Fumante:</strong> ' . $this->colaborador->getFumante() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Carro:</strong> ' . $this->colaborador->getCarro() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Placa:</strong> ' . $this->colaborador->getPlaca() . '</p>
						 </div>
					 </div>
				</div>
			</div>
			
			
			<div class="row">
				<div class="col-lg-12">
					<h4><strong><span class="glyphicon glyphicon-phone"></span> iPad</strong></h4>
					<hr />
					
					<div class="row">
						<div class="col-lg-4">
							<p><strong>Modelo:</strong> ' . $this->colaborador->getModeloIpad() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Conexão:</strong> ' . $this->colaborador->getConexao() . '</p>
						</div>
						<div class="col-lg-4">
							<p><strong>Registro:</strong> ' . $this->colaborador->getRegistro() . '</p>
						</div>
						
					</div>
					<div class="row">
						<div class="col-lg-4">
							<p><strong>Ativo?</strong> ' . $this->colaborador->getAtivo() . '</p>
						</div>
						
						<div class="col-lg-4">
							<p><strong>Número Ativo:</strong> ' . $this->colaborador->getNumAtivo() . '</p>
						</div>
					</div>
										
				</div>
				<div class="col-lg-12"></div>
			</div>
					
					
					
				</div>
			</div>
			
			
<br>
			
			
	
			';

        echo $html;

    }

    public function salvarColaborador()
    {
        $chave[] = "";

        foreach ($_POST as $key => $value) {
            $chave[$key] = $value;
        }

        $this->colaborador->setId($chave['txtId']);
        $this->colaborador->setIdColaborador($chave['txtIdCol']);
        $this->colaborador->setMatricula($chave['txtMatricula']);
        $this->colaborador->setDepartamento($chave['selDepto']);
        $this->colaborador->setFuncao($chave['selFuncao']);
        $this->colaborador->setNome($chave['txtNome']);
        $this->colaborador->setEndereco($chave['txtEndereco']);
        $this->colaborador->setNumero($chave['txtNumero']);
        $this->colaborador->setComplemento($chave['txtComplemento']);
        $this->colaborador->setBairro($chave['txtBairro']);
        $this->colaborador->setUf($chave['selUF']);
        $this->colaborador->setCidade($chave['selCidade']);
        $this->colaborador->setCep($chave['txtCep']);

        $this->colaborador->setDDDFoneRes($chave['txtDddRes']);
        $this->colaborador->setFoneRes($chave['txtFoneRes']);
        $this->colaborador->setDDDCelCorp($chave['txtDddCelCorp']);
        $this->colaborador->setCelCorp($chave['txtCelCorp']);
        $this->colaborador->setDDDCelPess($chave['txtDddCelPess']);
        $this->colaborador->setCelPess($chave['txtCelPess']);

        $this->colaborador->setEmail($chave['txtEmail']);
        $this->colaborador->setAeroporto($chave['selAeroporto']);
        $this->colaborador->setDataNasc($this->funcao->formatarData($chave['txtDataNasc']));
        $this->colaborador->setCPF($chave['txtCpf']);
        $this->colaborador->setRG($chave['txtRg']);
        $this->colaborador->setEstadoCivil($chave['selEstadoCivil']);
        $this->colaborador->setCamiseta($chave['selCamiseta']);
        $this->colaborador->setSapato($chave['txtSapato']);
        $this->colaborador->setFumante($chave['selFumante']);
        $this->colaborador->setCarro($chave['selCarro']);
        $this->colaborador->setPlaca($chave['txtPlaca']);
        $this->colaborador->setModeloIpad($chave['selModeloIpad']);
        $this->colaborador->setConexao($chave['selConexao']);
        $this->colaborador->setRegistro($chave['txtRegistro']);
        $this->colaborador->setAtivo($chave['selAtivo']);
        $this->colaborador->setNumAtivo($chave['txtNumAtivo']);

        //
        $retorno = $this->colaborador->salvar();
        echo $retorno;
    }

    public function manipularColaborador()
    {

        $idColaborador = $_POST['idColaborador'];
        $this->colaborador->setId($idColaborador);
        $this->colaborador->selecionar();

        $html = '<div class="row">
					  <div class="col-xs-12" id="mensagem-erro"></div>
				</div>
				
				<!-- LINHA1 -->
				<div class="row">
				  <div class="col-xs-2">
					<input type="hidden" name="txtId" id="txtId" value="' . $this->colaborador->getId() . '">
					<div class="form-group">
					  <label class="control-label" for="txtId">ID*</label>
					  <input type="text" class="form-control required" name="txtIdCol" value="' . $this->colaborador->getIdColaborador() . '">
					</div>
					
				  </div>
				  
				  <div class="col-xs-2">
					<div class="form-group">
					  <label class="control-label" for="txtMatricula">Matrícula</label>
					  <input type="text" class="form-control" name="txtMatricula" value="' . $this->colaborador->getMatricula() . '">
					</div>
				  </div>
				  
				  <div class="col-xs-3">
					 <div class="form-group">
					  <label class="control-label" for="selDepto">Departamento*</label>
						' . $this->funcao->comboPerfil($this->colaborador->getDepartamento()) . '
					</div>
				  </div>
				  
				  <div class="col-xs-5">
					 <div class="form-group">
					  <label class="control-label" for="selDepto">Função*</label>
						<g id="comboFuncao">
							' . $this->funcao->comboFuncao($this->colaborador->getDepartamento(), $this->colaborador->getFuncao()) . '
						</g>
					</div>
				  </div>

				</div>  
				
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-12">
						<div class="form-group">
						  <label class="control-label" for="txtNome">Nome do colaborador*</label>
						  <input type="text" class="form-control required" name="txtNome" value="' . $this->colaborador->getNome() . '">
						</div>
					</div>
				</div>
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-10">
						<div class="form-group">
						  <label class="control-label" for="txtEndereco">Endereço</label>
						  <input type="text" class="form-control" name="txtEndereco" value="' . $this->colaborador->getEndereco() . '">
						</div>
					</div>
					
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtNumero">Nº</label>
						  <input type="text" class="form-control" name="txtNumero" value="' . $this->colaborador->getNumero() . '">
						</div>
					</div>
				</div>
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
						  <label class="control-label" for="txtComplemento">Complemento</label>
						  <input type="text" class="form-control" name="txtComplemento" value="' . $this->colaborador->getComplemento() . '">
						</div>
					</div>
					
					<div class="col-xs-6">
						<div class="form-group">
						  <label class="control-label" for="txtBairro">Bairro</label>
						  <input type="text" class="form-control" name="txtBairro" value="' . $this->colaborador->getBairro() . '">
						</div>
					</div>
				</div>
				
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="selUF">UF</label>
						  ' . $this->funcao->comboUf($this->colaborador->getUf()) . '
						  </div>
					</div>
					
					<div class="col-xs-7">
						<div class="form-group">
						  <label class="control-label" for="selCidade">Cidade</label>
						  <div id="comboCidade">
						  	' . $this->funcao->comboCidade($this->colaborador->getUf(), $this->colaborador->getCidade()) . '
						  </div>
						</div>
					</div>
					
					<div class="col-xs-3">
						<div class="form-group">
						  <label class="control-label" for="txtCep">CEP</label>
						  <input type="text" class="form-control" name="txtCep" value="' . $this->colaborador->getCep() . '">
						</div>
					</div>
				</div>
				
				
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header"><strong>Contatos</strong></h3>
					</div>
				</div>
				
				
				
				
				 <!-- LINHA2 -->
				<div class="row">
				
					<div class="col-xs-1" style="padding-right:0px;">
						<div class="form-group">
						  <label class="control-label" for="txtDddRes">Fone</label>
						  <input type="text" class="form-control" name="txtDddRes" value="' . $this->colaborador->getDDDFoneRes() . '">
						</div>
					</div>
					<div class="col-xs-3" style="padding-left:5px;">
						<div class="form-group">
						  <label class="control-label" for="txtFoneRes">Residencial</label>
						  <input type="text" class="form-control" name="txtFoneRes" value="' . $this->colaborador->getFoneRes() . '">
						</div>
					</div>
					
					<div class="col-xs-1" style="padding-right:0px;">
						<div class="form-group">
						  <label class="control-label" for="txtDddCelCorp">Cel.</label>
						  <input type="text" class="form-control" name="txtDddCelCorp" value="' . $this->colaborador->getDDDCelCorp() . '">
						</div>
					</div>
					<div class="col-xs-3" style="padding-left:5px;">
						<div class="form-group">
						  <label class="control-label" for="txtCelCorp">Corporativo</label>
						  <input type="text" class="form-control" name="txtCelCorp" value="' . $this->colaborador->getCelCorp() . '">
						</div>
					</div>
					
					<div class="col-xs-1" style="padding-right:0px;">
						<div class="form-group">
						  <label class="control-label" for="txtDddCelPess">Cel.</label>
						  <input type="text" class="form-control" name="txtDddCelPess" value="' . $this->colaborador->getDDDCelPess() . '">
						</div>
					</div>
					<div class="col-xs-3" style="padding-left:5px;">
						<div class="form-group">
						  <label class="control-label" for="txtCelPess">Pessoal</label>
						  <input type="text" class="form-control" name="txtCelPess" value="' . $this->colaborador->getCelPess() . '">
						</div>
					</div>
				</div>
				
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-6">
						<div class="form-group">
						  <label class="control-label" for="txtEmail">Email*</label>
						  <input type="text" class="form-control required" name="txtEmail" value="' . $this->colaborador->getEmail() . '" style="text-transform:lowercase;">
						</div>
					</div>
					
					<div class="col-xs-6">
						<div class="form-group">
						  <label class="control-label" for="selAeroporto">Aeroporto mais próximo</label>
						  ' . $this->funcao->comboAeroporto() . '
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<h3 class="page-header"><strong>Dados Pessoais</strong></h3>
					</div>
				</div>
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtDataNasc">Data Nasc.</label>
						  <input type="text" class="form-control" name="txtDataNasc" id="txtDataNasc" value="' . $this->funcao->formatarData($this->colaborador->getDataNasc()) . '">
						</div>
					</div>
					
					<div class="col-xs-3">
						<div class="form-group">
						  <label class="control-label" for="txtCpf">CPF</label>
						  <input type="text" class="form-control" name="txtCpf" value="' . $this->colaborador->getCPF() . '">
						</div>
					</div>
					
					<div class="col-xs-3">
						<div class="form-group">
						  <label class="control-label" for="txtRg">RG</label>
						  <input type="text" class="form-control" name="txtRg" value="' . $this->colaborador->getRG() . '">
						</div>
					</div>
					
					<div class="col-xs-4">
						<div class="form-group">
						  <label class="control-label" for="selEstadoCivil">Estado Civil</label>
						  ' . $this->funcao->comboEstadoCivil($this->colaborador->getEstadoCivil()) . '
						</div>
					</div>
				</div>
				
				
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="selCamiseta">Camiseta</label>
						  ' . $this->funcao->comboTamanhoCamiseta($this->colaborador->getCamiseta()) . '
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtSapato">Sapato</label>
						  <input type="text" class="form-control" name="txtSapato" value="' . $this->colaborador->getSapato() . '">
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="selFumante">Fumante?</label>
							' . $this->funcao->comboSimNao($this->colaborador->getFumante(), 'comboFumante') . '
						</div>
					</div>
					<div class="col-xs-4">
						<div class="form-group">
						  <label class="control-label" for="selCarro">Carro</label>
							' . $this->funcao->comboCarro($this->colaborador->getCarro()) . '
						</div>
					</div>
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtPlaca">Placa</label>
						  <input type="text" class="form-control" name="txtPlaca" value="' . $this->colaborador->getPlaca() . '">
						</div>
					</div>
				</div>
				
				 <div class="row">
					<div class="col-xs-12">
						<h3 class="page-header"><strong>iPad</strong></h3>
					</div>
				</div>
				
				<!-- LINHA2 -->
				<div class="row">
					<div class="col-xs-3">
						<div class="form-group">
						  <label class="control-label" for="selModeloIpad">Modelo iPad</label>
							' . $this->funcao->comboModeloIpad($this->colaborador->getModeloIpad()) . '  
						</div>
					</div>
					
					<div class="col-xs-3">
						<div class="form-group">
						  <label class="control-label" for="selConexao">Conexão</label>
						  ' . $this->funcao->comboConexaoIpad($this->colaborador->getConexao()) . '
						</div>
					</div>
					
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtRegistro">Registro</label>
						  <input type="text" class="form-control" name="txtRegistro" value="' . $this->colaborador->getRegistro() . '">
						</div>
					</div>
					
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="selAtivo">Possui ativo?</label>
						  ' . $this->funcao->comboSimNao($this->colaborador->getAtivo(), 'comboPossuiAtivo') . '
						</div>
					</div>
					
					<div class="col-xs-2">
						<div class="form-group">
						  <label class="control-label" for="txtComplemento">Ativo</label>
						  <input type="text" class="form-control" name="txtNumAtivo" value="' . $this->colaborador->getNumAtivo() . '">
						</div>
					</div>
				</div>
				';
        echo $html;

    }

    public function excluirColaborador()
    {
        $idColaborador = $_POST['txtIdColaborador'];
        $this->colaborador->setId($idColaborador);
        $this->colaborador->excluir();
        header("Location: " . appConf::caminho . 'comunicacao/colaboradores');
    }

    public function popularComboCidade()
    {
        $uf = $_POST['uf'];
        echo $this->funcao->comboCidade($uf);
    }

    public function popularComboFuncao()
    {
        $funcao = $_POST['funcao'];
        echo $this->funcao->comboFuncao($funcao);
    }

    public function botoes($perfil, $codigo = 0)
    {

        $botao[0] = '<button type="button" data-toggle="modal" data-target="#modalVisualizarColaborador" data-idcolaborador="' . $codigo . '" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-list-alt"></span>&nbsp;</button> ';
        $botao[1] = '<button type="button" data-toggle="modal" data-target="#modalAdicionarColaborador" data-idcolaborador="' . $codigo . '" class="editar-col btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</button> ';
        $botao[2] = '<button type="button" data-toggle="modal" data-target="#modalExcluirColaborador" data-idcolaborador="' . $codigo . '" class="remover-col btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span>&nbsp;</button>';

        if ($perfil == 0) {
            return $botao[0] . $botao[1] . $botao[2];
        } else {
            return $botao[0];
        }

    }

    public function botaoAdicionar()
    {
        if ($this->funcao->dadoSessao('perfil') == 0) {
            return '<button type="button" id="btn-formulario" data-toggle="modal" data-target="#modalAdicionarColaborador" data-idcolaborador="0" class="btn btn-sm btn-warning"><span class="glyphicon glyphicon-plus"></span> Novo Colaborador</button>';
        }
    }

    public function comboExportar()
    {
        if ($this->funcao->dadoSessao('perfil') == 0) {
            $botoesExportar = '<li><a href="' . appConf::caminho . 'comunicacao/exportarColaborador/0" acao="0" class="ddw-exp" depto=""><span class="glyphicon glyphicon-ok"></span> Equipe Interna</a></li>';
        }
        $combo = '<div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					<span class="glyphicon glyphicon-asterisk"></span> 
					  Exportar
					  <span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
						<li><a href="' . appConf::caminho . 'comunicacao/exportarColaborador/1" acao="1" class="ddw-exp" depto=""><span class="glyphicon glyphicon-ok"></span> Todos</a></li>
					' . $botoesExportar . '
					</ul>
				  </div>';

        return $combo;
    }

    public function exportarColaborador($tipo)
    {
        $arrColuna = array();
        $rs = $this->colaborador->exportar($tipo);

        $arrColuna = array_keys($rs[1]);
        $nome_arquivo = "exportacao_" . $this->funcao->dadoSessao('setor') . "_" . date('Y_m_d_H_i_s');
        $df = fopen(appConf::caminho_fisico . "public\\exportacao\\" . $nome_arquivo . ".csv", 'w');

        $caminho = appConf::caminho_fisico . "public\\exportacao\\";

        $arr[1] = 'ULTIMA ATUALIZACAO: ' . date('d/n/Y') . ' as 07:35';

        for ($i = 1; $i <= count($rs); $i++) {
            for ($x = 0; $x <= count($arrColuna) - 1; $x++) {
                $rs[$i][$arrColuna[$x]] = utf8_decode($rs[$i][$arrColuna[$x]]);
            }
        }

        fputcsv($df, $arr, ';');
        fputcsv($df, $arrColuna, ';');
        for ($i = 1; $i <= count($rs); $i++) {
            fputcsv($df, $rs[$i], ';');
        }
        fclose($df);
        exec("C:\\winRAR.exe a -ep " . $caminho . $nome_arquivo . " " . $caminho . $nome_arquivo . ".csv", $arrs);
        unlink($caminho . $nome_arquivo . ".csv");

        $arquivo = $caminho . $nome_arquivo . '.rar';

        //header("Location: ".$arquivo);
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $nome_arquivo . '.rar"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($arquivo));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($arquivo);

        unlink($caminho . $nome_arquivo . ".rar");
    }

    public function listaDepartamento()
    {
        $rs = $this->colaborador->listarDepartamento();

        $depto = '<li><a href="' . appConf::caminho . 'comunicacao/colaboradores/0/"><small>TODOS</small></a></li>';

        for ($i = 1; $i <= count($rs); $i++) {
            $depto .= '<li><a href="' . appConf::caminho . 'comunicacao/colaboradores/0/' . str_replace(" ", "-", $rs[$i]['PERFIL']) . '"><small>' . $rs[$i]['PERFIL'] . '</small></a></li>';
        }


        $combo = '<div class="btn-group">
					<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
					<span class="glyphicon glyphicon-asterisk"></span> 
					  Departamento
					  <span class="caret"></span>
					</button>
					<ul class="dropdown-menu pull-right" role="menu">
					' . $depto . '
					</ul>
				  </div>';

        return $combo;

    }

    public function salvarFotoPerfil()
    {

        $foto = $_POST['imageData'];
        $idUsuario = $this->funcao->dadoSessao('id_colaborador');
        $nomeArquivo = md5($idUsuario . strtotime(date('d-m-Y H:i:s'))) . '.jpg';

        $this->colaborador->setId($idUsuario);
        $this->colaborador->setFotoColaborador($nomeArquivo);
        $this->colaborador->salvarFoto();

        $data = substr($foto, strpos($foto, ",") + 1);
        $decode = base64_decode($data);
        $fp = fopen(appConf::caminho_fisico . appConf::folder_profile_fisico . $nomeArquivo, 'wb');

        //echo appConf::caminho_fisico.appConf::folder_profile_fisico.$nomeArquivo;

        fwrite($fp, $decode);
        fclose($fp);
    }

    public function trocarSenha()
    {
        $senhaAntiga = $_POST['txtSenhaAntiga'];
        $senhaNova = $_POST['txtSenhaNova'];
        $idSetor = $this->funcao->dadoSessao('id_setor');

        echo $this->colaborador->trocarSenha($senhaAntiga, $senhaNova, $idSetor);
    }

}
