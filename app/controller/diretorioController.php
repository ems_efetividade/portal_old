<?php
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/diretorio2Model.php');
require_once('app/model/arquivoModel.php');


class diretorioController extends appController {

	private $diretorio = null;
	private $arquivo = null;
	private $funcao = null;
	
	private $arrObjetos = array();
	
	
	
	public function diretorioController() {
	    parent::__construct();
		$this->diretorio = new diretorio2Model();	
		$this->arquivo = new arquivoModel();
		$this->funcao = new appFunction();
		appFunction::validarSessao();
	}
	
	public function main() {
		
		
		$this->set('menuPadrao', $this->menuPadrao());
		$this->set('paginaInicial', $this->paginaInicial());
		$this->set('listaPermissao', $this->diretorio->listarPermissao());
		
		$this->render('diretorioView');
	}	
	
	public function paginaInicial() {
		return '<img src="'.appConf::caminho.'public/img/fundo_relatorio.jpg" class="text-center img-responsive" />';
	}
	
	public function selecionarDiretorio($idDiretorio, $idRaiz) {
		
		if($idDiretorio != 0) {
			$this->diretorio->setIdPasta($idDiretorio);
			$this->diretorio->selecionar();
			
			$idRaiz = $this->diretorio->getIdDiretorio();
			$nomeDiretorio = $this->diretorio->getPasta();
		}

		$dados = array(
						'idDiretorio' => $idDiretorio,
						'idRaiz' => $idRaiz,
						'pasta' => $nomeDiretorio
					);
		
		echo json_encode($dados);
	}
	
	public function excluirDiretorio() {
		$idDiretorio = $_POST['txtIdDiretorio'];	
		$this->diretorio->setIdPasta($idDiretorio);
		$this->diretorio->excluir();
		
	}
	
	public function salvarDiretorio() {
		$idDiretorio = $_POST['txtIdDiretorio'];
		$idRaiz = $_POST['txtIdPai'];
		$nomeDiretorio = utf8_decode($_POST['txtNomeDiretorio']);
		
		$this->diretorio->setIdPasta($idDiretorio);
		$this->diretorio->setIdDiretorio($idRaiz);
		$this->diretorio->setPasta($nomeDiretorio);
		$r = $this->diretorio->salvar();
		
		echo $r;
		
		//echo utf8_encode($nomeDiretorio);
		
		//echo $this->diretorio->getIdPasta().' ';
		//echo $this->diretorio->getIdDiretorio().' ';
		//echo $this->diretorio->getPasta();

	}
	
	public function permissaoDiretorio($idDiretorio) {
		$this->diretorio->setIdPasta($idDiretorio);	
		echo $this->diretorio->listarPermissaoDiretorio();
	}
	
	public function listaPermissaoDiretorio() {
		echo $this->diretorio->listarPermissao();
	}
	
	
	public function salvarPermissao() {
		$permissoes = $_POST['permissao'];
		$idPasta = $_POST['idPasta'];
		
		$this->diretorio = new diretorio2Model();

		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->setPermissao($permissoes);
		$this->diretorio->salvarPermissao();	
	}
	
	public function raizPastaMover() {
		
		$rs = $this->relatorio->listarDiretorio();
		$pasta = '';
		for($i=1;$i<=count($rs);$i++) {
			$pasta .= '<a class="listarSubDiretorio" value="'.$rs[$i]['ID_PASTA'].'"><p><small><b><img src="'.appConf::caminho.'public/img/foldermini.png"> '.$rs[$i]['PASTA'].'</b></small></p></a>';
		}
		//return $pasta;
		
		$mover = '
			<div class="row">
				<div class="col-lg-5">
					<div style="overflow-x:auto;height:300px">'.$pasta.'</div>
					
				</div>
				
				<div class="col-lg-7">
					<div id="divSubDiretorio" style="overflow-x:auto;height:300px"></div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<div id="local" style="margin-top:20px;border:1px solid #ddd;padding:5px"></div>
				</div>
			</div>
		';
		
		echo $mover;
	}
	
	public function visualizarPermissao() {
		$permissoes = $_POST['permissao'];
		$this->diretorio->setPermissao($permissoes);
		$rs = $this->diretorio->visualizarPermissao();
		
		$tabela = '<table class="table table-striped table-condensed">';
		for($i=1;$i<=count($rs);$i++) {
			$tabela .= '<tr>
							<td>
								<p><h5><small><b>'.$rs[$i]['USUARIO'].'</b></small></h5></p>
								<p><h5><small>'.$rs[$i]['PERFIL'].'</small></h5></p>
							</td>
							<td><h5><small>'.$rs[$i]['LINHA'].'</small></h5></td>
						</tr>';
		}
		$tabela .= '</table>';
		
		echo $tabela;
	}
	
	public function menuPadrao() {
		$menuPadrao = $this->diretorio->listarMenuPadrao();
		return $this->formatarMenu($menuPadrao);
		
	}
	
	public function menuPadraoMover() {
		$menuPadrao = $this->diretorio->listarMenuPadrao();
		echo $this->formatarMenu($menuPadrao);
		
	}
	
	public function subMenu($idPasta) {
		$this->diretorio->setIdPasta($idPasta);
		$subDir = $this->diretorio->listarSubDiretorio();
		echo $this->formatarMenu($subDir, 1);
	}
	
	private function formatarMenu($menuPadrao, $b='') {
		$listaMenu = '<ul class="menu-raiz">';
		foreach($menuPadrao as $key => $menu) {
			
			if($b == "") {
				$nomePasta = '<b><small>'.$menu->getPasta().'</small></b>';
			} else {
				$nomePasta = '<small>'.$menu->getPasta().'</small>';
			}

			$listaMenu .= '<li>
							<span id="diretorio'.$menu->getIdPasta().'" class="raiz folder-open" diretorio="'.$menu->getIdPasta().'">
								<span class="folder">
									
								</span>
								
							</span>
							<span class="abrir-diretorio" diretorio="'.$menu->getIdPasta().'">'.$nomePasta.'</span>
							<div id="subDiretorio'.$menu->getIdPasta().'" class="sub-diretorio"></div>
						   </li>';
		}
		$listaMenu .= '</ul>';
		
		return $listaMenu;
	}
	
	
	public function abrir($idPasta=0) {
            
		$this->initSessionResultado();
		$objetos = array();
		
		$this->diretorio->setIdPasta($idPasta);
		$this->arquivo->setIdPasta($idPasta);
		
		$this->diretorio->selecionar();

		$arquivos = $this->arquivo->listarArquivosDiretorio();
		$subDir = $this->diretorio->listarSubDiretorio();
				
		$i=0;
                
                //print_r($diretorio->getIdPasta());
                
		foreach($subDir as $key => $diretorio) {

			$this->arrayResultado($i, 
                                            $diretorio->getIdPasta(),
                                            appConf::caminho.'public/img/foldermini.png',
                                            $diretorio->getPasta(),
                                            '',
                                            appFunction::formatarData($diretorio->getDataCriacao()),
                                            'Diretório',
                                            '',
                                            $this->barraDiretorio($diretorio->getIdPasta(), $diretorio->getIdDiretorio())
                                            );
			
			$i++;
		}
		
		foreach($arquivos as $key => $arquivo) {
                    $arquivoFisico = utf8_decode(appConf::folder_report.$arquivo->getDiretorio().'\\'.$arquivo->getArquivo());
                    if(file_exists($arquivoFisico)) {
			$this->arrayResultado($i, 
					  $arquivo->getIdArquivo(),
					  $this->funcao->extensaoArquivo($arquivo->getExtensao()),
					  $arquivo->getArquivo(),
					  $arquivo->getDiretorio(),
					  appFunction::formatarData($arquivo->getDataInclusao()),
					  'Arquivo',
					  appFunction::formatarTamanho($arquivo->getTamanho()),
					  $this->barraArquivo($arquivo->getIdArquivo())
					  );
			
			$i++;
                    }
                }
		

		$this->setSessionResultado();
	
	}
	
	public function initSessionResultado() {
		@session_start();
		$this->arrObjetos = array();
		$_SESSION['listaArquivos'] = "";
	}
	
	public function setSessionResultado() {
		@session_start();
                print_r($this->arrObjetos);
		$_SESSION['listaArquivos'] = $this->arrObjetos;
	}
	
	public function getSessionResultado() {
		@session_start();
		return $_SESSION['listaArquivos'];
	}
	
	public function arrayResultado($i, $id, $icone, $nome, $caminho, $data, $tipo, $tamanho, $acao) {
		$this->arrObjetos[$i]['id'] = $id;
		$this->arrObjetos[$i]['icone'] = $icone;
		$this->arrObjetos[$i]['nome'] = $nome;
		$this->arrObjetos[$i]['caminho'] = $caminho;
		$this->arrObjetos[$i]['data'] = $data;
		$this->arrObjetos[$i]['tipo'] = $tipo;
		$this->arrObjetos[$i]['tamanho'] = $tamanho;
		$this->arrObjetos[$i]['acao'] = $acao;
	}
	
	public function pesquisar() {
		$dados = array();
		
		$idPasta = $_POST['txtIdPasta'];
		$criterio = htmlentities(utf8_decode($_POST['txtCriterio']));
		
		$this->diretorio->setIdPasta($idPasta);
		$arquivos  = $this->diretorio->pesquisar($criterio);
		
		$this->initSessionResultado();
		$i=0;
		foreach($arquivos as $key => $arquivo) {
			$this->arrayResultado($i, 
								  $arquivo->getIdArquivo(),
								  $this->funcao->extensaoArquivo($arquivo->getExtensao()),
								  utf8_encode($arquivo->getArquivo()),
								  $arquivo->getDiretorio(),
								  appFunction::formatarData($arquivo->getDataInclusao()),
								  'Arquivo',
								  appFunction::formatarTamanho($arquivo->getTamanho()),
								  $this->barraArquivo($arquivo->getIdArquivo())
								  );
			
			$i++;
		}
		
		
		$this->setSessionResultado();
	}
	
	public function listarPesquisa($pagina=1, $idPasta=0) {
		$maxResultado = 14;
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		$nomePasta = 'Resultado da Pesquisa';
	
		$objetos = $this->getSessionResultado();
			
		$paginas = ceil(count($objetos) / $maxResultado);
		$inicio = ($pagina * $maxResultado) - $maxResultado;
		$maximo = $maxResultado + $inicio;
		
		$paginacao = "";
		
		$html =  $this->barraAdministrador($idPasta);
		$html .= $this->cabecalhoDiretorio($nomePasta, count($objetos));
		
		$html .= '<form id="formArquivo">';
		for($i=$inicio;$i<$maximo;$i++) {
			
			if($pesquisar != "") {
				$caminho = '<p>'.$objeto['caminho'].'</p>';
			}
			
			$objeto = $objetos[$i];
			
			$classe = 'abrir-diretorio';
			if($objeto['tipo'] == 'Arquivo') {
				$classe ='';
			}
			
			$html .= '<div class="row">
						<div class="col-lg-5">
						
						<div diretorio="'.$objeto['id'].'" class="'.$classe.'">
							<p>
								<small class="tedxt-nowrap">
									<span><img src="'.$objeto['icone'].'" /></span>
									'.$objeto['nome'].'
									'.$caminho.'
								</small>
							</p>
						</div>

						</div>
						<div class="col-lg-2"><small>'.$objeto['data'].'</small></div>
						<div class="col-lg-1"><small>'.$objeto['tipo'].'</small></div>
						<div class="col-lg-2"><small>'.$objeto['tamanho'].$objeto['existe'].'</small></div>
						<div class="col-lg-2">'.$objeto['acao'].'</div>
					 </div>';
		}
		$html .= '</form>';
		
		if(count($objetos) > $maxResultado) {
			
			
			if($pagina == $paginas) {
				$bloqUltimo = 'disabled';
			}
			
			if($pagina == 1) {
				$bloqPrim = 'disabled';
			}
			
		$paginacao = '<small><nav>
				  <ul class="pager">
					<li class="'.$bloqPrim.'"><a href="#" pagina="1" class="paginacao">|< Primeiro</a></li>
    				<li class="'.$bloqPrim.'"><a href="#" pagina="'.($pagina-1).'" class="paginacao">< Anterior</a></li>
					<li>Página '.number_format($pagina, 0, "", ".").' de '.number_format($paginas, 0, "", ".").'</li>
					<li class="'.$bloqUltimo.'"><a href="#" pagina="'.($pagina+1).'" class="paginacao">Próximo ></a></li>
    				<li class="'.$bloqUltimo.'"><a href="#" pagina="'.$paginas.'" class="paginacao">Último >|</a></li>
				  </ul>
				</nav></small>';
				
		}
		
		
		
		
		echo $html.$paginacao;
	}
	
	
	public function listarArquivos($pagina=1, $idPasta=0) {
		$maxResultado = 14;
		$objetos = array();
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		$nomePasta = $this->diretorio->getPasta();
	
		$objetos = $this->getSessionResultado();
			
		$paginas = ceil(count($objetos) / $maxResultado);
		$inicio = ($pagina * $maxResultado) - $maxResultado;
		$maximo = $maxResultado + $inicio;
		
		$paginacao = "";
		
		$html =  $this->barraAdministrador($idPasta);
		$html .= $this->cabecalhoDiretorio($nomePasta, count($objetos));
		
		
		
		$html .= '<form id="formArquivo">';
		for($i=$inicio;$i<$maximo;$i++) {
			
			if($pesquisar != "") {
				$caminho = '<p>'.$objeto['caminho'].'</p>';
			}
			
			$objeto = $objetos[$i];

			$classe = 'abrir-diretorio';
			if($objeto['tipo'] == 'Arquivo') {
				$classe ='';
			}
			
			$html .= '<div class="row">
						<div class="col-lg-5 col-md-9 col-sm-5 col-xs-8">
						
						<div diretorio="'.$objeto['id'].'" class="'.$classe.'">
							<p>
								<small class="tedxt-nowrap">
									<span><img src="'.$objeto['icone'].'" /></span>
									'.$objeto['nome'].'
									'.$caminho.'
								</small>
							</p>
						</div>

						</div>
						<div class="hidden-xs hidden-md j-hidden-sm col-lg-2"><small>'.$objeto['data'].'</small></div>
						<div class="hidden-xs hidden-md j-hidden-sm col-lg-1"><small>'.$objeto['tipo'].'</small></div>
						<div class="hidden-xs hidden-md j-hidden-sm col-lg-2"><small>'.$objeto['tamanho'].$objeto['existe'].'</small></div>
						<div class="col-lg-2 col-md-3 col-xs-4">'.$objeto['acao'].'</div>
					 </div>';
		}
		$html .= '</form>';
		
		if(count($objetos) > $maxResultado) {
			
			
			if($pagina == $paginas) {
				$bloqUltimo = 'disabled';
			}
			
			if($pagina == 1) {
				$bloqPrim = 'disabled';
			}
			
		$paginacao = '<small><nav>
				  <ul class="pager">
					<li class="'.$bloqPrim.'"><a href="#" pagina="1" class="paginacao">|< Primeiro</a></li>
    				<li class="'.$bloqPrim.'"><a href="#" pagina="'.($pagina-1).'" class="paginacao">< Anterior</a></li>
					<li>Página '.number_format($pagina, 0, "", ".").' de '.number_format($paginas, 0, "", ".").'</li>
					<li class="'.$bloqUltimo.'"><a href="#" pagina="'.($pagina+1).'" class="paginacao">Próximo ></a></li>
    				<li class="'.$bloqUltimo.'"><a href="#" pagina="'.$paginas.'" class="paginacao">Último >|</a></li>
				  </ul>
				</nav></small>';
				
		}
		
		
		
		
		echo $html.$paginacao;
		
		//print_r($objetos);
		
	}
	
	public function cabecalhoDiretorio($nomeDiretorio='', $totalArquivos=0) {
		return '<div class="nome-diretorio">
            		<div class="row">
						<div class="col-lg-8">
							<div>
							
								<div style="font-size:20px;">
									<img style="padding-right:5px;" src="'.appConf::caminho.'/public/img/closedfolder.png" class="pull-left" />
									<strong>'.$nomeDiretorio.'</strong>
								</div> 
								
								<div><small>'.number_format($totalArquivos, 0, "", ".").' íten(s).</small></div>
							</div>
						</div>
                
                		<div class="col-lg-4">
 
						</div>

            		</div>
            
            	</div>
		
		
				<div class="row" >
                 	<div class="col-lg-5 col-md-9 col-sm-5 col-xs-8" style="border-right:1px solid #ddd">
						<small><strong>Nome</strong></small>
					</div>
					
					<div class="j-hidden-sm hidden-xs hidden-md col-lg-2" style="border-right:1px solid #ddd">
						<small><strong>Data</strong></small>
					</div>
					
					<div class="j-hidden-sm hidden-xs hidden-md col-lg-1" style="border-right:1px solid #ddd">
						<small><strong>Tipo</strong></small>
					</div>
					
					<div class="j-hidden-sm hidden-xs hidden-md col-lg-2" style="border-right:1px solid #ddd">
						<small><strong>Tamanho</strong></small>
					</div>
					
                    <div class="col-lg-2 col-md-3 col-xs-4" style="border-right:1px solid #ddd">
						<div><small><strong>Ação</strong></small></div>
					</div>
					
                  </div>
				  
				  <br />';
	}
	
	public function barraAdministrador($idRaiz) {
		if($this->funcao->dadoSessao('nivel_admin') >= 1) {
			$botaoAdmin = '

				<div class="pull-right">
					
  <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalEditarPasta" data-id="0" data-raiz="'.$idRaiz.'"><span class="glyphicon glyphicon-plus"></span> Nova Pasta</button>
  
                      <button id="btnUpload" type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modsalUpload"><span class="glyphicon glyphicon-arrow-up"></span> Upload</button>
                    
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          Arquivos
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                          <li><a href="#" id="btnSelecionarTudo"><small><span class="glyphicon glyphicon-check"></span> Selecionar Tudo</a></small></li>
                          <li><a href="#" id="btnRemoverSelecao"><small><span class="glyphicon glyphicon-unchecked"></span> Remover Seleção</a></small></li>
                          <li class="divider"></li>
                          <li><a href="#"  data-toggle="modal" data-target="#modalMoverConteudo" data-acao="selecionado" data-id="'.$idRaiz.'"><small><span class="glyphicon glyphicon-transfer"></span> Mover Selecionados</a></small></li>
                          <li><a href="#" data-toggle="modal" data-target="#modalExcluirSelecionado" data-id="'.$idRaiz.'"><small><span class="glyphicon glyphicon-trash"></span> Excluir Selecionados</a></small></li>
                          <li class="divider"></li>
                          <li><a href="#" data-toggle="modal" data-target="#modalMoverConteudo" data-acao="tudo" data-id="'.$idRaiz.'" ><small><span class="glyphicon glyphicon-ok"></span> Mover Tudo</a></small></li>
                          <li><a href="#" id="btnExcluirsConteudo" data-toggle="modal" data-target="#modalExcluirConteudo" data-id="'.$idRaiz.'"><small><span class="glyphicon glyphicon-minus"></span> Excluir Tudo</a></small></li>
                        </ul>
                      </div>

					</div>
					
					<form id="formUploadFile" action="'.appConf::caminho.'diretorio/upload" method="post" enctype="multipart/form-data">
						<input type="hidden" name="txtIdPasta" value="'.$idRaiz.'" />
					  	<input type="file" id="fileArquivo" name="fileArquivo[]" multiple="multiple" />
					</form>';
					
					
					
					
					
					
			$pesquisar =' <form id="formPesquisar" method="post" action="'.appConf::caminho.'diretorio/pesquisar"><div class="input-group">
					  <input type="text" name="txtCriterio" class="form-control input-sm" placeholder="Procurar...">
					  <input type="hidden" name="txtIdPasta" value="'.$idRaiz.'" />
					  <span class="input-group-btn">
						<button class="btn btn-warning btn-sm" id="btnPesquisar" type="button"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
					  </span>
					</div></form>';
					
					
					
			
			
			
			
			
			return '<div class="row">
						<div class="col-lg-8 col-xs-6">
						  '.$pesquisar.'
						</div>
				
						<div class="col-lg-4 col-xs-6">
							'.$botaoAdmin.'
						</div>
					</div>
					
					<br/>';
		}
	}
	
	
	public function barraDiretorio($idDiretorio='', $idRaiz='') {
		
		$barraBotoes[0] = '<a href="#" data-toggle="modal" data-id="'.$idDiretorio.'" data-raiz="'.$idRaiz.'" data-target="#modalEditarPasta" class="btn btn-warning btn-xs"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a> ';
		
		$barraBotoes[1] = '<a href="#" data-toggle="modal"  data-id="'.$idDiretorio.'"  data-raiz="'.$idRaiz.'" data-target="#modalExcluirPasta" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-remove"></span>&nbsp;</a> ';
		
		$barraBotoes[2] = '<a href="#" data-toggle="modal" data-id="'.$idDiretorio.'" data-target="#modalPermissaoPasta" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-user"></span>&nbsp;</a> ';
		
		if($this->funcao->dadoSessao('nivel_admin') >= 1) {
			return ''.
					$barraBotoes[0].
					$barraBotoes[1].
					$barraBotoes[2]
					.'';
		}
	}
	
	public function barraArquivo($idArquivo='') {
		
		$barraBotoes[0] = '<a href="'.appConf::caminho.'diretorio/download/'.base64_encode($idArquivo).'" class="btn btn-info btn-xs"><span class="glyphicon glyphicon-download-alt"></span>&nbsp;</a> ';
		
		$barraBotoes[1] = '<a class="btn btn-info btn-xs" data-toggle="modal" data-target="#modalEditarArquivo" data-id="'.$idArquivo.'"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a> ';
		
		$barraBotoes[2] = '<div class="btn-group" role="group" aria-label="...">
								<input type="checkbox" class="chk" id="chkArquivo" name="chkArquivo[]" value="'.$idArquivo.'">
							</div>';
		
		
		if($this->funcao->dadoSessao('nivel_admin') >= 1) {
			return '<div class="bdtn-group" role="group" aria-label="...">'.
						$barraBotoes[0].
						$barraBotoes[1].
						$barraBotoes[2]
					.'</div>';
		} else {
		return '<div class="btn-grdoup" role="group" aria-label="...">'.
						$barraBotoes[0]
					.'</div>';
		}
	}
	
	public function download($idArquivo) {
		$idArquivo = base64_decode($idArquivo);
		
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
		
		$arquivo = $this->arquivo->getArquivo();
		$diretorio = $this->arquivo->getDiretorio();		
		
		$this->funcao->download($diretorio, $arquivo);	
	}
	
	public function selecionarArquivo($idArquivo) {
				
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
			
		$idArquivo = $this->arquivo->getIdArquivo();
		$idPasta = $this->arquivo->getIdPasta();
		$nomeArquivo = $this->arquivo->getArquivo();

		$dados = array(
						'idArquivo' => $idArquivo,
						'idPasta' => $idPasta,
						'arquivo' => $nomeArquivo,
					);
		
		echo json_encode($dados);
	}
	
	public function salvarArquivo() {
		$idArquivo = $_POST['txtIdArquivo'];
		$nomeArquivo = $_POST['txtNomeArquivo'];
		
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
		
		$this->arquivo->setArquivo($nomeArquivo);
		
		
		$r = $this->arquivo->salvar();
		
		//echo $r;

		
	}
	
	
	public function excluirArquivos() {
		
		$array = $_POST['chkArquivo'];
		
		for($i=0;$i<count($array);$i++) {
			$idArquivo = $array[$i];
			
			$this->arquivo->setIdArquivo($idArquivo);
			$this->arquivo->excluir();
		}
	}
	
	
	public function moverArquivo($idPastaMover) {
		$erro = '';
		
		if($idPastaMover != 0) {
			$array = $_POST['chkArquivo'];
			
			//print_r($array);
			
			$this->diretorio->setIdPasta($idPastaMover);
			$this->diretorio->selecionar();
			
			$caminhoDestino = utf8_decode(appConf::folder_report.$this->diretorio->getDiretorio()).'\\';
			
			for($i=0;$i<count($array);$i++) {
				$idArquivo = $array[$i];
				
				$this->arquivo->setIdArquivo($idArquivo);
				$this->arquivo->selecionar();
							
				$origem = utf8_decode(appConf::folder_report.$this->arquivo->getCaminhoArquivo());
				$destino = $caminhoDestino.utf8_decode($this->arquivo->getArquivo());
				
				if(file_exists($caminhoDestino)) {
					if(file_exists($origem)) {
						if(copy($origem, $destino)) {
							@unlink($origem);
							
							$this->arquivo->setArquivo($this->arquivo->getArquivo()); 
							$this->arquivo->setIdPasta($idPastaMover);
							$this->arquivo->salvar();
						} else {
							$erro .= ' '.$this->arquivo->getArquivo();	
						}
					}
				}
			}
			
			if($erro != "") {
				$errors= error_get_last();
				echo 'Houve erro ao mover alguns arquivos<br>'.$erro.'<br>'.$errors['message'];	
			}
			
			
		
		}
	}
	
	
	
	public function excluirTudo($idPasta=0) {
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->excluirConteudoDiretorio(); 			
	}
	
	
	
	
	public function moverTudo($idPastaDE=0, $idPastaPARA=0) {
		if($idPastaPARA != 0) {
			
			$dirDe = new diretorio2Model();
			$dirPara = new diretorio2Model();
			
			$dirDe->setIdPasta($idPastaDE);
			$dirDe->selecionar();
			
			$dirPara->setIdPasta($idPastaPARA);
			$dirPara->selecionar();
	
			$origem = utf8_decode(appConf::folder_report.$dirDe->getDiretorio()).'\\*.*';
                        $destino = utf8_decode(appConf::folder_report.$dirPara->getDiretorio()).'\\';
			
                        $cmd = 'move "'.$origem.'" "'.$destino.'"';
                        $retorno = exec($cmd);
                        
                        $this->arquivo->alterarPasta($idPastaDE, $idPastaPARA);
                        //echo $retorno;
                        
                        
                        
                        /*
			foreach(glob($origem ."*.*") as $file) {
				
				$destino = appConf::folder_report.$dirPara->getDiretorio().'\\'.basename($file);
				copy($file, $destino);
				if(file_exists($file)) {
					@unlink($file);
				}
				//echo 'Mover '.$file.' para '.$destino;
			}
			*/
			
		
		}
	}
	
	public function upload() {
		//print_r($_FILES['fileArquivo']);
		//print_r($_POST);
		
		$arquivos = $_FILES['fileArquivo'];
		$idPasta = $_POST['txtIdPasta'];	
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		$max = count($arquivos['name']);
		$this->arquivo->setIdPasta($idPasta);
		
		

		
		$erro = '';
                if(!file_exists(utf8_decode(appConf::folder_report.$this->diretorio->getDiretorio()))) {
                    mkdir(utf8_decode(appConf::folder_report.$this->diretorio->getDiretorio()),0777, true);
                }
		
		chmod (utf8_decode(appConf::folder_report.$this->diretorio->getDiretorio()).'\\', 0777);
		
		for($i=0;$i<$max;$i++) {
			
			$extensao = explode(".", $arquivos['name'][$i]);
			
			$arquivoFinal = appConf::folder_report.utf8_decode($this->diretorio->getDiretorio().'\\'.$arquivos['name'][$i]);
			
			$moveu = move_uploaded_file($arquivos['tmp_name'][$i], $arquivoFinal);

			//chmod($pasta, 0755)
			
			if($moveu) {
				$this->arquivo->setExtensao($extensao[count($extensao)-1]);
				$this->arquivo->setArquivo($arquivos['name'][$i]);	
				$this->arquivo->setTamanho($arquivos['size'][$i]);
				$this->arquivo->setTipo($arquivos['type'][$i]);
				$this->arquivo->salvar();
			} else {
				$erro .= 'Erro no arquivo '. $arquivos['name'][$i] .' -> '.$moveu.$arquivos['error'][$i].$arquivoFinal;
			}
		}
		
		if($erro != "") {
			//echo $erro;
		}
		
		//header('Location: '.appConf::caminho.'relatorio/abrir/'.$_POST['txtIdPasta']);
			
	}
	
	
	public function barraProgresso() {
		echo '
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<div style="display: table;width: 100%;height:340px;">
					<div style="display: table-cell;vertical-align: middle;text-align:center;">
					
					<div class="panel panel-default" style="padding:0px;">
					  <div class="panel-body">
						<small><strong>Listando...</strong></small>
					<div class="progress">
					  <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						<span class="sr-only">45% Complete</span>
					  </div>
					</div>
					  </div>
					</div>
					
					
					
					</div>
					</div>
				</div>
				<div class="col-lg-4"></div>
			</div>';
	}
	
		
	
}