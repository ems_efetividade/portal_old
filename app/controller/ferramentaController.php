<?php
require_once('lib/appController.php');
require_once('lib/appConexao.php');
require_once('lib/appFunction.php');
require_once('app/model/ferramentaModel.php');

class ferramentaController extends appController {

	private $ferramenta = null;
	private $conexao = null;

	public function ferramentaController() {
            $this->ferramenta = new ferramentaModel();	
            $this->conexao = new appConexao();
	}
	
	public function main() {
            $this->render('ferramentaView');	
	}
	
	public function porvoce() {
            $this->set('porvoce', 1);
            $this->render('ferramentaView');	
	}
	
	public function abrir($idFerramenta) {
		
            $novaUrl = "";

            $this->ferramenta->setIdFerramenta($idFerramenta);
            $this->ferramenta->selecionar();
            
            $view = 'ferramentaView';

            switch($this->ferramenta->getEmpresa()) {

                    case 'CEDAT': {

                        $setor = appFunction::dadoSessao('setor');

                        if($setor == '45110' && $this->ferramenta->getIdFerramenta() == 12) {
                            $setor = '45105';
                        }


                        $query = "SELECT 
                                        LINHA.SIGLA 
                                  FROM 
                                        SETOR 
                                  INNER JOIN LINHA ON SETOR.ID_LINHA = LINHA.ID_LINHA 
                                  WHERE SETOR.SETOR = '".$setor."'";

                        $rs = $this->conexao->executarQueryArray($query);

                        if(appFunction::dadoSessao('perfil') == 0) {
                                $usuario = 'ecd.01957';
                                $senha   = 'epx@2016';
                        } else {
                                $usuario = $rs[1]['SIGLA'].".".$setor;
                                //$senha = 'emspx@2015';
                                $senha = md5('abc.'.$usuario.'.123');
                        }


                        $newUrl = str_replace('%USUARIO%',$usuario, $this->ferramenta->getCaminho());
                        $newUrl = str_replace('%SENHA%',$senha,$newUrl);

                        $novaUrl = $newUrl;
                        break;
                    }

                    case 'IMS': {
                        $dados = array('us' => base64_encode('e3s_'.appFunction::dadoSessao('setor')),
                                       'ps' => base64_encode('j@naina'),
                                       'sr' => base64_encode('http://www.emsprescricao.com.br/portal/'),
                                       'cl' => base64_encode('EMS'));
                        
                        $novaUrl = $this->ferramenta->getCaminho().  urlencode(json_encode($dados));
                        //$view = 'ferramenta/wrs';
                        break;	
                    }
                    
                    


                    default: {
                            $novaUrl = $this->ferramenta->getCaminho();	
                    }



            }

            $this->set('ferramenta', $this->ferramenta);
            $this->set('dados', $dados);    
            $this->set('target', $this->ferramenta->getTarget());
            $this->set('url',$novaUrl);
            $this->set('nomeFerramenta', $this->ferramenta->getFerramenta());
            $this->render($view);

	}
}