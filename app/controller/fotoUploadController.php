<?php

require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/fotoConvencaoModel.php');
require_once('app/model/colaboradorModel.php');


class fotoUploadController extends appController {
    
    private $foto = null;
    private $colaborador = null;

    public function __construct() {
        $this->foto         = new fotoConvencaoModel();	
        $this->colaborador  = new colaboradorModel();
    }
    
    public function main() {
        $this->set('listaFotos', $this->listar());
        $this->render('fotoView');
    }
    
    public function exportar() {
        
        $dados = $this->foto->exportarLista();
        $nomeDoArquivo = 'FotosConvencao';
        $fileRoot = appConf::caminho_fisico.appConf::folder_profile_fisico.'convencao\\';
        
        $arrColuna = array_keys($dados[1]);
        
        $arquivoCSV = $nomeDoArquivo.'_'.date('d_m_y_H-i-s').'.csv';

        
        $df = fopen($fileRoot.$arquivoCSV, 'w');
        

        fputcsv($df, array_keys(reset($dados)),';');
        
        for($i=1; $i<=count($dados); $i++) {
            for($x=0; $x<=count($arrColuna)-1; $x++) {
                $dados[$i][$arrColuna[$x]] = str_replace(PHP_EOL, '', utf8_decode($dados[$i][$arrColuna[$x]]));
            }
        }

        
        foreach($dados as $row) {
            fputcsv($df, $row, ';', '"');
        }
       
        fclose($df);
        
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$arquivoCSV);
        header('Content-Type: application/x-msexcel');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($fileRoot.$arquivoCSV));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($fileRoot.$arquivoCSV);

        unlink($fileRoot.$arquivoCSV);
    }
    
    public function excluir($idSetor=0, $idColab=0) {

        $this->foto->setIdSetor($idSetor);
        $this->foto->setIdColaborador($idColab);
        $this->foto->excluir();
        
        header('Location: '.appConf::caminho.'fotoUpload');
        
    }
    
    public function listar() {
        $fotos = $this->foto->listar();
        
        $html = '<h3>'.count($fotos).' fotos(s).</h3>
            
                    <hr>
                    <div class="row">
                        <div class="col-lg-2">
                            Foto
                        </div>
                        <div class="col-lg-4">Colaborador</div>
                        <div class="col-lg-2">Tamanho da Foto</div>
                        <div class="col-lg-2">Data de Upload</div>
                        <div class="col-lg-2">Resetar</div>
                    </div>';
        
        foreach($fotos as $c => $foto) { 
            
            $arquivo = appConf::caminho_fisico.appConf::folder_profile_fisico.'convencao\\'.$foto->getFoto();
            
            $this->colaborador->setId($foto->getIdColaborador());
            $this->colaborador->selecionar();
            
            //if(file_exists($arquivo)) {
            
            $img = (substr($foto->getFoto(), (strlen($foto->getFoto())-3), 3) == 'tif') ? '<a href="'.appConf::caminho.'public/profile/convencao/'.$foto->getFoto().'">Para visualizar, clique aqui!</a>' : '<img src="'.appConf::caminho.'public/profile/convencao/'.$foto->getFoto().'" class="img-responsive" />' ;

                $html .= '
                    <hr>
                    <div class="row" style="'.(($c%2) ? 'background-color:#f9f9f9' : '').'">
                        <div class="col-lg-2">
                            '.$img.'
                        </div>
                        <div class="col-lg-4">
                            <p><b>'.$this->colaborador->getSetor().' '.$this->colaborador->getNome().'</b></p>
                            <p><b>Email:</b> '.$this->colaborador->getEmail().'</p>
                            <p><b>Telefone:</b> ('.$this->colaborador->getDDDCelCorp().') '.$this->colaborador->getCelCorp().'</p>
                            <p><small>Arquivo: '.$foto->getFoto().'</small></p>
                        </div>
                        <div class="col-lg-2">'.appFunction::formatartamanho($foto->getTamanho()) .'</div>
                        <div class="col-lg-2">'. appFunction::formatarData($foto->getDataUpload()).'</div>
                        <div class="col-lg-2 text-right">
                            <a class="btn btn-danger btn-sm" href="'.appConf::caminho.'fotoUpload/excluir/'.$foto->getIdSetor().'/'.$foto->getIdColaborador().'">Excluir Foto</a>
                        </div>
                    </div>';
            //}
            
        }
        
        return $html;
    }
        
}