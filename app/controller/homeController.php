<?php

require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/noticiaModel.php');
require_once('app/model/informativoModel.php');
require_once('app/model/popupModel.php');
require_once('app/model/grafHomeModel.php');
require_once ('cHomeController.php');


class homeController extends appController {
	
	private $funcao = null;
	private $noticia = null;
	private $informativo = null;
	private $popup = null;
	private $graf = null;
	
	function homeController() {
	    parent::__construct();
		$this->funcao = new appFunction();	
		$this->noticia = new noticiaModel();
		$this->informativo = new informativoModel();
		$this->popup = new popupModel();
        $this->graf = new grafHomeModel();
	}
	
    public function transformadores($acao='') {
        
        switch ($acao) {
            case '': {
                $this->render('transformadoresView');	
                break;
            }
            case 'inscrever': {
                sleep(1.5);
                
                $sessao= appFunction::dadoSessao('setor');
                $cn = new appConexao();
                $query = "INSERT INTO [EMS_TRANSFORMADORES] (ID_SETOR, ID_COLABORADOR, DATA_CADASTRO) SELECT V.ID_SETOR, V.ID_COLABORADOR, GETDATE() FROM vw_colaboradorSetor V LEFT JOIN EMS_TRANSFORMADORES T ON T.ID_SETOR = V.ID_SETOR AND T.ID_COLABORADOR = V.ID_COLABORADOR WHERE SETOR = '".$sessao."' AND T.ID_SETOR IS NULL";
                $cn->executarQueryArray($query);
                
                $query = "SELECT * FROM [EMS_TRANSFORMADORES]";
                $rs = $cn->executarQueryArray($query);
                
                //print_r($rs);
                break;
            }
            
        }
    }
        
	public function main() {
		
		$this->noticia->selecionarUltimaNoticia();
		$popup = $this->popup->selecionarPopupAtivo();
		$imagem = "";
		
		if($popup->getImagem() != "") {
            $imagem = appConf::caminho.appConf::folder_popup.$popup->getImagem();
            $link = $popup->getLink();
		}
		
		
		$this->set('nomeColaborador', $this->funcao->nomeColaborador($this->funcao->dadoSessao('nome')));
		$this->set('periodoDia', $this->funcao->periodoDia());
		$this->set('dataExtenso', $this->funcao->dataExtenso());
		
		$this->set('informativos', $this->listarInformativo());
		$this->set('tituloArtigo', $this->noticia->getTitulo());
		$this->set('urlArtigo', $this->funcao->criarUrlArtigo($this->noticia->getIdNoticia(), $this->noticia->getTitulo()));
		$this->set('imgArtigo', $this->noticia->getImagem());
		$this->set('resumoArtigo', $this->noticia->getResumo());
		$this->set('listaArtigos', $this->listaArtigo());
		$this->set('imagemPopup', $imagem);
        $this->set('linkPopup', $link);

        $this->set('existeFoto', $this->verificarFoto());
       
        if($this->funcao->getConfPortal('MANUTENCAO') == 1 && appFunction::dadoSessao('nivel_admin') < 2) {
            $this->render('manutencaoView');	
        } else {
            $this->render('homeView');
        }
                
	}
	
	
        public function getDadosGraficoHome($idGrafico=0) {
            $arr['categories'] = '';
            $titulo = $this->graf->getGrafico($idGrafico);
            $dados = $this->graf->getDados($titulo[1]['ID_GRAFICO']);
            $a = [];
            $keys = array_keys($dados[1]);
            foreach($keys as $j => $key) {
                if(is_numeric($keys[$j])) {
                    unset($keys[$j]);
                } else {
                    if($key != "NOME" && $key != 'COR') {
                        $a[] =  utf8_encode($key);
                        //$keys[$j] = utf8_encode($keys[$j]);
                    }
                }
            }
            $keys = array_values($keys);
            $b = [];
            foreach($dados as $dado) {
                $d = [];
                
                if(count(explode("|", $dado['COR'])) > 1) {
                    $c['colors'] = explode("|", $dado['COR']);
                } else {
                    $c['color'] = $dado['COR'];
                }
                $c['name'] = $dado['NOME'];
                for($i=2;$i<count($keys);$i++) {
                    $d[] = ($dado[$keys[$i]] == 0) ? null : $dado[$keys[$i]];
                }
                $c['data'] = $d;
                
                $b[] = $c;
            }
            $arr['tipo'] = $titulo[1]['TIPO'];
            $arr['categories']     = $a;
            $arr['serie']          = $b;
            //$arr['colors']         = explode(",", $dados[1]['COR']);
            $arr['titulo']         = $titulo[1]['TITULO'];
            $arr['subtitulo']      = $titulo[1]['SUBTITULO'];
            $arr['texto']          = $titulo[1]['DESCRICAO'];
            $arr['atualizacao']    = appFunction::formatarData($titulo[1]['DATA_ATUALIZACAO']);
            $arr['texto_esquerda'] = $titulo[1]['TEXTO_ESQUERDA'];
            $arr['texto_direita'] = $titulo[1]['TEXTO_DIREITA'];
            //echo '<pre>';
            //print_r($arr);
            //echo '</pre>';
            echo json_encode($arr, JSON_NUMERIC_CHECK | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }
        
	public function listarInformativo() {
		
		$informativos = $this->informativo->listarHome(1);
		        foreach($informativos as $key => $informativo) {

			$lista = '<span style="position:absolute; margin-top: 190px;">
                            <a class="nav-informativo nav-informativo-l" style="z-index:999!important;" acao="prev">
                                <span class="glyphicon glyphicon-chevron-left">
                                </span>
                            </a>
                        </span>
                        <span style="position:absolute; margin-top: 190px;margin-left: 270px">
                        <a class="nav-informativo nav-informativo-r" style="z-index:999;"acao="next">
                        <span class="glyphicon glyphicon-chevron-right">
                        </span>
                        </a>
                        </span>';
			if($key == 0) {
				$ativo = 'active';
			}
			
			$lista .= '
		<div class="item '.$ativo.'" >
            <div alt="...">
                <h3 class="text-primary">
                    <strong>'.$informativo->getTitulo().'</strong>
                </h3>'.$informativo->getTexto().'
            </div>
            <div class="carousel-caption">
            </div>
        </div>';
		}
		return $lista;
		
	}
	
	public function listaArtigo(){
		$noticias = $this->noticia->listar(4, 1);

		$artigo = '';
		
		foreach($noticias as $key => $noticia) {
			
			
			$artigo .= '<div class="row">
							
							<div class="col-lg-5">
								'.$this->funcao->imagemNoticia($noticia->getImagem(), $noticia->getLocalImagem(), '120', '90').'
							</div>
							
							<div class="col-lg-7">
								<strong>
									<a href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a>
								</strong>
								
								<h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).'</small></h4>
						  
						  <h5>Por: '.$noticia->getAutor().'</h5>
							</div>
							
						</div>
						
						<hr style="margin-top:5px" />';
			
		
			/*if($key > 0) {
				$artigo .= '
					  <div class="media">
		
						<a href="#" class="pull-left">
							'.$this->funcao->imagemNoticia($noticia->getImagem(), $noticia->getLocalImagem(), '50%', '40').'
						  </a>
						
						<div class="media-body">
						  <h4 class="media-heading">
							<strong>
								<a href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a>
							</strong>
						  </h4>
						  
						  <h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).'</small></h4>
						  
						  <h5>Por: '.$noticia->getAutor().'</h5>
						</div>
					  </div>';
					  
			}*/
		}

		
		$link = '<a href="'.appConf::caminho.'noticia/" class="pull-right btn btn-primary" id="lista-artigo"><strong><span class="glyphicon glyphicon-plus"></span> Mais Artigos</strong></a>';
		
		return $artigo.$link;
				
	}
        
        
        
        public function salvarFoto () {
            
            require_once('app/model/fotoConvencaoModel.php');
            $tbFoto = new fotoConvencaoModel();
            
            
            
            $foto = $_FILES['foto'];
            
            $conf['folder'] = appConf::caminho_fisico.appConf::folder_profile_fisico.'convencao\\';
            
            $nome = $foto['name'];
            $ext = explode('.', $nome);
            $arquivo = appFunction::dadoSessao('setor').'_'.date('YmdHis');
            
            //print_r($foto);
            
           // echo 'salvar em '.$conf['folder'];
            
            $retorno = $this->validarFoto($foto);
            
            if($retorno['erro'] == '') {
           
            if(move_uploaded_file($foto['tmp_name'], $conf['folder'].$arquivo.'.'.end($ext))) {
                //echo 'Upload Realizado';
                
                
                $tbFoto->setIdSetor(appFunction::dadoSessao('id_setor'));
                $tbFoto->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
                $tbFoto->setFoto($arquivo.'.'.end($ext));
                $tbFoto->salvar();
                
                
                $retorno['erro'] = 'OK';
                $retorno['link'] = appConf::caminho.appConf::folder_profile.'convencao/'.$arquivo.'.'.end($ext);
            } else {
                $retorno['erro'] = 'erro upload';
            }
            
            }
            
            echo json_encode($retorno);
            
        }
        
        public function verificarFoto() {
            require_once('app/model/fotoConvencaoModel.php');
            $tbFoto = new fotoConvencaoModel();
            $tbFoto->setIdSetor(appFunction::dadoSessao('id_setor'));
            $tbFoto->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
            
            return $tbFoto->verificarFoto();
            
        }
        
        public function validarFoto($foto) {
            $conf['minSize'] = (1048576 / 2); // 2 megas
            $conf['maxSize'] = (1048576 * 10); // 10mb
            $conf['width'] = 1800;
            $conf['height'] = 2400;
            $conf['ext'] = array('jpg', 'gif', 'bmp',  'tif');
            
            
            $retorno['erro'] = '';
            
            
            $nome = $foto['name'];
            
           // verifica a extensao
            $ext = explode('.', $nome);
            if(!in_array(strtolower(end($ext)), $conf['ext'])) {
                $retorno['erro'] = 'Tipo de arquivo não permitido! Arquivos permitidos: '.implode(', ', $conf['ext']);
                return $retorno;
            }
            
            
            //verifica o tamanho
            if($foto['size'] < $conf['minSize']) {
                $retorno['erro'] = 'O tamanho minimo do arquivo deve ser 2 megabytes. ';
                return $retorno;
            }
            
            if($foto['size'] > $conf['maxSize']) {
                $retorno['erro'] = 'Tamanho máximo do arquivo excedido (Máximo 10 megabytes).';
                return $retorno;
            }
            
            //verifica a largura e altura
            list($w, $h, $type, $attr) = getimagesize($foto['tmp_name']);
            
            if($w < $conf['width'] && $h < $conf['height']) {
                $retorno['erro'] = 'A resolução da foto está abaixo do mínimo('.$w.'x'.$h.')';
                return $retorno;
            }
            
            if($w > $h) {
                $retorno['erro'] = 'Permitido apenas foto no modo "Retrato"';
                return $retorno;
            }
        }
        
	
}


?>