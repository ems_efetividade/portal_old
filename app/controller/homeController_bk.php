<?php

require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/noticiaModel.php');
require_once('app/model/informativoModel.php');
require_once('app/model/popupModel.php');


class homeController extends appController {
	
	private $funcao = null;
	private $noticia = null;
	private $informativo = null;
	private $popup = null;
	
	function homeController() {
		$this->funcao = new appFunction();	
		$this->noticia = new noticiaModel();
		$this->informativo = new informativoModel();
		$this->popup = new popupModel();
	}
	
	public function main() {
		
		$this->noticia->selecionarUltimaNoticia();
		$popup = $this->popup->selecionarPopupAtivo();
		$imagem = "";
		
		if($popup != "") {
                    $imagem = appConf::caminho.appConf::folder_popup.$popup;
		}
		
		
		$this->set('nomeColaborador', $this->funcao->nomeColaborador($this->funcao->dadoSessao('nome')));
		$this->set('periodoDia', $this->funcao->periodoDia());
		$this->set('dataExtenso', $this->funcao->dataExtenso());
		
		$this->set('informativos', $this->listarInformativo());
		$this->set('tituloArtigo', $this->noticia->getTitulo());
		$this->set('urlArtigo', $this->funcao->criarUrlArtigo($this->noticia->getIdNoticia(), $this->noticia->getTitulo()));
		$this->set('imgArtigo', $this->noticia->getImagem());
		$this->set('resumoArtigo', $this->noticia->getResumo());
		$this->set('listaArtigos', $this->listaArtigo());
		$this->set('imagemPopup', $imagem);
		
                $this->set('existeFoto', $this->verificarFoto());
                
                if($this->funcao->getConfPortal('MANUTENCAO') == 0) {
                    
                    
                    
                    $this->render('homeView');	
                } else {
                   $this->render('manutencaoView');	
                }
                
	}
	
	
	public function listarInformativo() {
		
		$informativos = $this->informativo->listar(1);
		$lista = '';
		
		foreach($informativos as $key => $informativo) {
			
			$ativo = '';
			
			if($key == 0) {
				$ativo = 'active';
			}
			
			$lista .= '<div class="item '.$ativo.'">
                      	<div alt="...">
                          <h3 class="text-primary"><strong>'.$informativo->getTitulo().'</strong></h3>
                           '.$informativo->getTexto().'
                        </div>
                        <div class="carousel-caption"></div>
                      </div>';
		}
		
		return $lista;
		
	}
	
	public function listaArtigo(){
		$noticias = $this->noticia->listar(4, 1);

		$artigo = '';
		
		foreach($noticias as $key => $noticia) {
			
			
			$artigo .= '<div class="row">
							
							<div class="col-lg-5">
								'.$this->funcao->imagemNoticia($noticia->getImagem(), $noticia->getLocalImagem(), '120', '90').'
							</div>
							
							<div class="col-lg-7">
								<strong>
									<a href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a>
								</strong>
								
								<h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).'</small></h4>
						  
						  <h5>Por: '.$noticia->getAutor().'</h5>
							</div>
							
						</div>
						
						<hr style="margin-top:5px" />';
			
		
			/*if($key > 0) {
				$artigo .= '
					  <div class="media">
		
						<a href="#" class="pull-left">
							'.$this->funcao->imagemNoticia($noticia->getImagem(), $noticia->getLocalImagem(), '50%', '40').'
						  </a>
						
						<div class="media-body">
						  <h4 class="media-heading">
							<strong>
								<a href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a>
							</strong>
						  </h4>
						  
						  <h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).'</small></h4>
						  
						  <h5>Por: '.$noticia->getAutor().'</h5>
						</div>
					  </div>';
					  
			}*/
		}

		
		$link = '<a href="'.appConf::caminho.'noticia/" class="pull-right btn btn-primary" id="lista-artigo"><strong><span class="glyphicon glyphicon-plus"></span> Mais Artigos</strong></a>';
		
		return $artigo.$link;
				
	}
        
        
        
        public function salvarFoto () {
            
            require_once('app/model/fotoConvencaoModel.php');
            $tbFoto = new fotoConvencaoModel();
            
            
            
            $foto = $_FILES['foto'];
            
            $conf['folder'] = appConf::caminho_fisico.appConf::folder_profile_fisico.'convencao\\';
            
            $nome = $foto['name'];
            $ext = explode('.', $nome);
            $arquivo = appFunction::dadoSessao('setor').'_'.date('YmdHis');
            
            //print_r($foto);
            
           // echo 'salvar em '.$conf['folder'];
            
            $retorno = $this->validarFoto($foto);
            
            if($retorno['erro'] == '') {
           
            if(move_uploaded_file($foto['tmp_name'], $conf['folder'].$arquivo.'.'.end($ext))) {
                //echo 'Upload Realizado';
                
                
                $tbFoto->setIdSetor(appFunction::dadoSessao('id_setor'));
                $tbFoto->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
                $tbFoto->setFoto($arquivo.'.'.end($ext));
                $tbFoto->salvar();
                
                
                $retorno['erro'] = 'OK';
                $retorno['link'] = appConf::caminho.appConf::folder_profile.'convencao/'.$arquivo.'.'.end($ext);
            } else {
                $retorno['erro'] = 'erro upload';
            }
            
            }
            
            echo json_encode($retorno);
            
        }
        
        public function verificarFoto() {
            require_once('app/model/fotoConvencaoModel.php');
            $tbFoto = new fotoConvencaoModel();
            $tbFoto->setIdSetor(appFunction::dadoSessao('id_setor'));
            $tbFoto->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
            
            return $tbFoto->verificarFoto();
            
        }
        
        public function validarFoto($foto) {
            $conf['minSize'] = (1048576 / 2); // 2 megas
            $conf['maxSize'] = (1048576 * 10); // 10mb
            $conf['width'] = 1800;
            $conf['height'] = 2400;
            $conf['ext'] = array('jpg', 'gif', 'bmp',  'tif');
            
            
            $retorno['erro'] = '';
            
            
            $nome = $foto['name'];
            
           // verifica a extensao
            $ext = explode('.', $nome);
            if(!in_array(strtolower(end($ext)), $conf['ext'])) {
                $retorno['erro'] = 'Tipo de arquivo não permitido! Arquivos permitidos: '.implode(', ', $conf['ext']);
                return $retorno;
            }
            
            
            //verifica o tamanho
            if($foto['size'] < $conf['minSize']) {
                $retorno['erro'] = 'O tamanho minimo do arquivo deve ser 2 megabytes. ';
                return $retorno;
            }
            
            if($foto['size'] > $conf['maxSize']) {
                $retorno['erro'] = 'Tamanho máximo do arquivo excedido (Máximo 10 megabytes).';
                return $retorno;
            }
            
            //verifica a largura e altura
            list($w, $h, $type, $attr) = getimagesize($foto['tmp_name']);
            
            if($w < $conf['width'] && $h < $conf['height']) {
                $retorno['erro'] = 'A resolução da foto está abaixo do mínimo('.$w.'x'.$h.')';
                return $retorno;
            }
            
            if($w > $h) {
                $retorno['erro'] = 'Permitido apenas foto no modo "Retrato"';
                return $retorno;
            }
        }
        
	
}


?>