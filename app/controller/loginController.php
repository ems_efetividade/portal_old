<?php

require_once('lib/appController.php');
require_once('app/model/loginModel.php');

class loginController extends appController
{

    private $login = null;
    private $conteudo = null;

    public function loginController()
    {
        $this->login = new loginModel();
    }

    public function loginAd($login, $password)
    {
        $ldap_server = '172.16.147.200';
        $dominio = 'EMS.COM.BR'; //Dominio local ou global
        $user = $login . '@' . $dominio;
        $ldap_porta = '389';
        $ldap_pass = $password;
        $ldapconn = ldap_connect("ldap://" . $ldap_server, $ldap_porta);
        ldap_set_option($ldapconn, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldapconn, LDAP_OPT_REFERRALS, 0);
        if ($ldapconn) {
            ini_set('display_errors', FALSE);
            $bind = ldap_bind($ldapconn, $user, $ldap_pass);
            ini_set('display_errors', true);
            if ($bind) {
                $base_dn = "DC=" . join(',DC=', explode('.', $dominio));
                $filter = "(&(objectClass=user)(objectCategory=person)(userPrincipalName=" . ldap_escape($user, null, LDAP_ESCAPE_FILTER) . "))";
                $result = ldap_search($ldapconn, $base_dn, $filter);
                $info = ldap_get_entries($ldapconn, $result);
                //Variável arquivo armazena o nome e extensão do arquivo.
                $this->descarregaArray($info);
                $arquivo = appConf::caminho_fisico . "/public/pdf/".$login."teste.txt";
                //Variável $fp armazena a conexão com o arquivo e o tipo de ação.
                $fp = fopen($arquivo, "a+");
                //Escreve no arquivo aberto.
                fwrite($fp, $this->conteudo);
                //Fecha o arquivo.
                fclose($fp);
                return $info[0]['description'][0];
            } else {
                return 0;
            }
        }
    }

    public function descarregaArray($array)
    {
        if (is_array($array)) {
            foreach ($array as $item) {
                $this->descarregaArray($item);
            }
        } else {
            $this->conteudo .= "<br>".$array."<br>";
        }
    }

    public function login()
    {
        $setor = (isset($_POST['txtSetor'])) ? appSanitize::filter($_POST['txtSetor']) : '';
        $senha = (isset($_POST['txtSenha'])) ? $_POST['txtSenha']: '';
        $conectado = (isset($_POST['conectado'])) ? appSanitize::filter($_POST['conectado']) : '';
        if ($senha == '' || $setor == '') {
            echo 'Os campos "Setor" e "Senha" não podem estar vazios!';
            return;
        }
        $cpf = $this->loginAd($setor, $senha);
        $this->login->setSetor($setor);
        $this->login->setCpf($cpf);
        $this->login->setSenha($senha);
        $this->login->setConectado($conectado);
        $this->login->setAdmin($this->validaSenhaAdmin($cpf));
        $retorno = $this->login->login();
        switch ($retorno) {
            case '0':
                echo "Usuário ou senha inválido";
                break;
            case '1':
                echo "Usuário não cadastrado na Base do Portal";
                break;
        }
    }

    public function validaSenhaAdmin($cpf)
    {
        if ($cpf != 0)
            return false;
        $acesso = $this->login->acessoAdministrador();
        if (count($acesso) == 0)
            return false;
        $data = $acesso[1]['DATA_EXPIRAR'];
        $today = date("Y-m-d H:i:s");
        $date_time = new DateTime($today);
        $data = new DateTime($data);
        if ($data < $date_time)
            return false;
        $this->login->setIdAdmin($acesso[1]['ID']);
        return true;
    }

    public function logout()
    {
        $this->login->logout();
        header('Location: ' . appConf::caminho);
    }

    public function main()
    {
        $this->render('loginView');
    }

    public function controlarEmpresa($empresa)
    {
        session_start();
        $_SESSION['id_linha'] = $_SESSION['acessos'][$empresa]['id_linha'];
        $_SESSION['sigla'] = $_SESSION['acessos'][$empresa]['sigla'];
        $_SESSION['empresa'] = $_SESSION['acessos'][$empresa]['empresa'];
        $_SESSION['unidade_neg'] = $_SESSION['acessos'][$empresa]['unidade_neg'];
        $_SESSION['id_un_negocio'] = $_SESSION['acessos'][$empresa]['id_un_negocio'];
        $_SESSION['id_empresa'] = $_SESSION['acessos'][$empresa]['id_empresa'];
        $_SESSION['id_setor'] = $_SESSION['acessos'][$empresa]['id_setor'];
        $_SESSION['setor'] = $_SESSION['acessos'][$empresa]['setor'];
    }
}

?>