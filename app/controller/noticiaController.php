<?php

require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/noticiaModel.php');
require_once('app/model/noticiaFotosModel.php');

class noticiaController extends appController {
	
	private $noticia = null;
	private $noticiaFotos = null;
	private $funcao = null;
	
	function noticiaController() {
		$this->noticia = new noticiaModel();
		$this->noticiaFotos = new noticiaFotosModel();
		$this->funcao = new appFunction();	
	}
	
	public function ultimaNoticia() {
		$this->noticia->ultimaNoticia();
	}

	public function main() {
		$noticias = $this->noticia->listar();
		
		foreach($noticias as $noticia) {
			
		$artigo .= '<a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">
						<img  class="pull-left" data-src="holder.js/64x64" src="'.$noticia->getImagem().'" style="width: 180px; height: 120px; margin-right:10px">
					  </a>
					 
						<h4 class="media-heading text-primary"><strong><a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a></strong></h4>
						<h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).' por '.$noticia->getAutor().' - '.$noticia->getVisualizacao().' visualizações.</small></h4></small></h4>
						<h4 class="text-justify"><p>'.$noticia->getResumo().' <a class="abrir-artigo" artigo="'.$noticia->getIdNoticia().'" href="#">(leia mais...)</a></p></h4>
					  
					<hr>';

		}
		
		$this->set('listaNoticia', $artigo.$paginacao);
		$this->render('noticiaListaView');
			
	}
	
	public function ler($idNoticia) {
		
		$this->noticia->setIdNoticia($idNoticia);
		$this->noticiaFotos->setIdNoticia($idNoticia);
		$this->noticia->selecionar();		
		$this->noticia->somarVisualizacao();	
		
		$this->set('tituloArtigo', $this->noticia->getTitulo());
		$this->set('resumoArtigo', $this->noticia->getResumo());
		$this->set('escritorArtigo', $this->noticia->getAutor());
		$this->set('dataArtigo', $this->funcao->formatarData($this->noticia->getDataPublicacao()));
		$this->set('visualizacaoArtigo', $this->noticia->getVisualizacao());
		$this->set('imgArtigo', $this->funcao->imagemNoticia($this->noticia->getImagem(), $this->noticia->getLocalImagem()));
		$this->set('textoArtigo', $this->noticia->getMateria());
		$this->set('fotosArtigo', $this->fotosArtigo($this->noticiaFotos->listar()));
		$this->set('listaArtigos', $this->listarArtigosMaisLidos());

		$this->render('noticiaView');

	}
	public function imagemNoticia($imagem, $local) {
		
		$imagemFinal = '';
		
		if($imagem != "") {
			if($local == 0) {
				$pastaImgArtigo = appConf::caminho . appConf::folder_img_noticia;
				
				if(file_exists(appConf::folder_img_noticia.$imagem)) {
					$imagemFinal = $pastaImgArtigo.$imagem;
				} 
			} else {
				$imagemFinal = $imagem;	
			}
		}
		
		
		return '<img style="margin-right:10px;margin-bottom:10px;"class="pull-left img-responsive"  src="'.$imagemFinal.'" />';
		
		
		
	}
	
	public function listar($pagina=1) {
		
		$max = 5;
		
		$totalArt = count($this->noticia->listar());
		$numMaxPagina = round($totalArt / $max);
		
		$prox = $pagina + 1;
		$ant = $pagina - 1;
		
		if($ant <= 0) {
			$ant = 1;
		}
		
		if($prox > $numMaxPagina) {
			$prox = $numMaxPagina;	
		}
		
		$paginacao = '<ul class="pager">
						<li><a href="'.appConf::caminho.'artigo/listar/'.$ant.'">Página Anterior</a></li>
						<li><a href="'.appConf::caminho.'artigo/listar/'.$prox.'">Próxima Página</a></li>
					  </ul>';
		
		
		$rs = $this->artigo->listarPaginacao($max, $pagina);
		$artigo = '';
		
		for($i=1;$i<=count($rs);$i++) {
			
		$artigo .= '<a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($rs[$i]['ID_ARTIGO'], $rs[$i]['TITULO']).'">
						<img  class="pull-left" data-src="holder.js/64x64" src="'.$rs[$i]['IMG'].'" style="width: 180px; height: 120px; margin-right:10px">
					  </a>
					 
						<h4 class="media-heading text-primary"><strong><a class="media-left abrir-artigo" href="'.$this->funcao->criarUrlArtigo($rs[$i]['ID_ARTIGO'], $rs[$i]['TITULO']).'">'.$rs[$i]['TITULO'].'</a></strong></h4>
						<h4><small>'.$this->funcao->formatarData($rs[$i]['DATA']).' por '.$rs[$i]['ESCRITOR'].' - '.$rs[$i]['VISUALIZACOES'].' visualizações.</small></h4></small></h4>
						<h4 class="text-justify"><p>'.$rs[$i]['RESUMO'].' <a class="abrir-artigo" artigo="'.$rs[$i]['ID_ARTIGO'].'" href="#">(leia mais...)</a></p></h4>
					  
					<hr>';

		}
		
		$this->set('listaArtigo', $artigo.$paginacao);
		$this->render('noticiaListaView');
	}
	
	
	
	public function listarArtigosMaisLidos() {
		
		$noticias = $this->noticia->listarMaisLidos(5);

		$artigo = '';
		
		foreach($noticias as $key => $noticia) {
			
			
			$artigo .= '<div class="row">
							
							<div class="col-lg-5">
								'.$this->funcao->imagemNoticia($noticia->getImagem(), $noticia->getLocalImagem(), '120', '90').'
							</div>
							
							<div class="col-lg-7">
								<strong>
									<a href="'.$this->funcao->criarUrlArtigo($noticia->getIdNoticia(), $noticia->getTitulo()).'">'.$noticia->getTitulo().'</a>
								</strong>
								
								<h4><small>'.$this->funcao->formatarData($noticia->getDataPublicacao()).'</small></h4>
						  
						  <h5>Por: '.$noticia->getAutor().'</h5>
							</div>
							
						</div>
						
						<hr style="margin-top:5px" />';
						
		}
		
		return $artigo;

	}
	
	public function fotosArtigo($rs) {
		$modal = '';
		
		if(count($rs) > 0) {
			$fotografo = '';
			$thumbs = '<div class="item active">
						<ul class="list-inline" style="margin-left:5%;">';
			$album = '';
			
			$thumbMax = 7;
			$mMover = $thumbMax+1;
			$nThumb=0;
			
			$pagina = 0;
			
			foreach($rs as $key => $foto) {		
			//for($i=1;$i<=count($rs);$i++) {
							
				if($nThumb > $thumbMax) {
					$thumbs .= '</ul></div>
					
								<div class="item">
									<ul class="list-inline" style="margin-left:5%;">';
	
					$nThumb = 0;
					$pagina++;
					
				}
	
				if($key == 0) {
					$itm = 'active';
				} else {
					$itm = '';	
				}
				
				//if($rs[$i]['FOTOGRAFO'] != '') {
				//	$fotografo = 'Foto: '.$rs[$i]['FOTOGRAFO'];
				//}
				
				$thumbs .= '<li><a href="#" class="selected car-tumb-'.($key).'" data-target="#carousel-example-generic1" data-slide-to="'.($key).'"><img class="img-thumbnail" alt="120x120" src="'.appConf::caminho.appConf::folder_foto_noticia.$foto->getFoto().'" data-holder-rendered="true" style="width: 80px; height: 80px;"></a></li>';
				
				$album .= '
					<div class="item '.$itm.'" pagina="'.$pagina.'">
						<img src="'.appConf::caminho.'public/img/logo_prescricao.png" class="img-responsive" style="position:absolute;top:3%;left:2%" />
						<div style="position:absolute;bottom:0;left:5px;color:#fff"><small>'.$fotografo.'</small></div>
						<img data-src="holder.js/900x500/auto/#777:#555/text:First slide" alt="First slide [900x500]" src="'.appConf::caminho.appConf::folder_foto_noticia.$foto->getFoto().'" data-holder-rendered="true">
						<div class="carousel-caption">
							<small></small>
							<p><small>(Foto '.($key+1).' de '.count($rs).')</small></p>
						</div>
					</div>
				';
				
				$nThumb++;
			}
			$thumbs .= '</ul></div>';
			
			$modal = '
			  <h4>Confira as Fotos</h4><div id="carousel-example-generic1" class="carousel slide" data-ride="carousel" max="'.$thumbMax.'">
				  <!--<ol class="carousel-indicators">
					<li data-target="#carousel-example-generic1" data-slide-to="0" class="active"></li>
					<li data-target="#carousel-example-generic1" data-slide-to="1" class=""></li>
					<li data-target="#carousel-example-generic1" data-slide-to="2" class=""></li>
				  </ol>-->
				  <div class="carousel-inner" role="listbox">
					
							'.$album.'
				  </div>
				  <a class="left carousel-control" href="#carousel-example-generic1" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				  </a>
				  <a class="right carousel-control" href="#carousel-example-generic1" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				  </a>
				</div>	
				
				
				
				<div id="carousel-example-generic2" class="carousel slide" data-interval="false" data-ride="carousel">
		 
		  <div class="carousel-inner" role="listbox">
			'.$thumbs.'
	
		  </div>
		  <a class="left carousel-control" href="#carousel-example-generic2" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#carousel-example-generic2" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		  </a>
		</div>
			';
			
		}
		
		return $modal;
	}
	
	
	
}


?>