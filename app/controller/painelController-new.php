<?php
require_once('app/view/painel/functions.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/painelModel.php');

class painelController extends appController {

    private $funcao = null;
    private $painel = null;
    private $html = null;
    private $upload = appConf::caminho_fisico.'public\\painel\\';
    
    
    function painelController() {
        appFunction::validarSessao();
        $this->funcao = new appFunction();
        $this->painel = new painelModel();
        $this->painel->setSetor(appFunction::dadoSessao('setor'));
    }

    function shortName($nome) {
        $data = explode(" ", $nome);
        return $data[0].' '.$data[(count($data)-1)];
    }
    
    function main() {
        appFunction::validarSessao();
        $this->abrirPainel();
    }

    function admin() {
        $this->set('modulo', 'Painel Administrativo');
        $this->render('painel/admin/admin-home');
    }
    
    function permissao() {
        $this->set('modulo', 'Permissão');
        $this->set('permitidos', $this->painel->getColabPermitidos());
        $this->render('painel/admin/admin-permissao');
    }

    function listarPermissaok($id, $get, $where) {
        
        $dados = $this->painel->getPerm($id, $get, $where);
        
        $html = "";
        
        foreach($dados as $row) {
            $k = md5(uniqid(rand(), true));
            $html .= '<p>
                <input type="checkbox" data-container="'.$k.'" id="chk-linha-"'.$k.'" class="chk-linha" value="">
                <a class="" data-toggle="collapse" href="#'.$k.'" aria-expanded="false" aria-controls="'.$k.'">
                  '.$row['COLUNA'].'
                </a>
            </p>
            <div class="collapse collapse-pad" id="'.$k.'" data-id="'.$row['ID'].'" data-get="PRODUTO" data-where="ID_PILAR">
                
            </div>';
        }
        
        echo $html;
    }
    
    function formPermissao($id=0) {
        
        $this->set('permitido', $this->painel->selecionarPermissao($id));
        $this->set('grupo', $this->painel->getListaPermissoes());
        $this->set('linhas', $this->painel->getLinhas());
        $this->set('pilares', $this->painel->getPilarProduto());
        echo $this->renderToString('painel/admin/admin-form-permissao');
    }
    
    function pesquisarColab() {
        $crit = $_POST['criterio'];
        $retorno = $this->painel->getPesquisaColaboradores(urldecode($crit));
        
        $html = '';
        
        foreach($retorno as $row) {
            $html .= '<tr>'
                    . '<td><a href="#" data-nome="('.$row['SETOR'].') '.$row['NOME'].'" data-id="'.$row['ID_COLABORADOR'].'">'.$row['SETOR'].'</a></td>'
                    . '<td><a href="#" data-nome="('.$row['SETOR'].') '.$row['NOME'].'" data-id="'.$row['ID_COLABORADOR'].'">'.$row['NOME'].' </a></td>'
                    . '<td><a href="#" data-nome="('.$row['SETOR'].') '.$row['NOME'].'" data-id="'.$row['ID_COLABORADOR'].'">'.$row['PERFIL'].'</a></td>'
                    . '</tr>';
        }
        
        echo $html;
    }
    
    function salvarPermissao() {
        $idColab  = $_POST['txtIdColab'];
        $produtos = $_POST['chk-prod'];
        
       echo  $this->painel->salvarPermissao($idColab, $produtos);
        $this->location(appConf::caminho.'painel/permissao');
    }
    
    function excluirPermissao($id) {
        $this->painel->excluirPermissao($id);
        $this->location(appConf::caminho.'painel/permissao');
    }
    
    function abrirPainel($setor='') {
        
        
        
        $rs = $this->painel->listarSetores(appFunction::dadoSessao('setor'));
        
        $setor = ($setor == '') ? $rs[1]['SETOR'] :  $setor;
        $this->painel->setSetor($setor);
        
        $this->set('admin', $this->painel->validarAdmin());
        $this->set('modulo', 'Seja Bem Vindo');
        $this->set('setores', $rs);
        $this->set('colab', $this->painel->dadosColaborador());
        $this->set('tabelaHeader', $this->painel->getHeaderTabela());
        $this->set('tabela', $this->painel->getTabela($setor));
        
        $this->set('dadosGrafico', $this->gerarGrafico($setor));
        $this->set('rnkPainel', $this->painel->getRankingPainel($setor));
        $this->set('tabelaPilares', $this->criarTabelaPilares($setor));
        $this->set('linhas', $this->painel->getLinhas());
        $this->set('boxes', $this->painel->getBoxes($setor));
        $this->set('ponts', $this->painel->getPontuacao($setor));
        $this->render('painel/painel');
        //$this->render('painelView');
    }
    
    public function gerarGrafico($setor) {
        $retorno = $this->painel->getDadosGrafico($setor);
        
        
        $final = [];
        $eixo = [];
        foreach($retorno['DADOS'] as $dado) {
            $data = [];
            $eixo[$dado['GRAFICO_EIXO']][] = $dado['PILAR'];
            
            $data['name'] = $dado['PILAR'];
            $data['yAxis'] = $dado['GRAFICO_EIXO'];
            for($i=11;$i>=0;$i--) {
                $key = 'M'.str_pad($i,2, 0,STR_PAD_LEFT);
                $data['data'][] = ($dado[$key] == "" || $dado[$key] == 0) ? null : $dado[$key];
            }
            $final[] = $data;
        }
        
        $cab = [];
        foreach($retorno['CAB'] as  $value) {
            $cab[] = $value;
        }
        
        $arrEixo['e0'] = (isset($eixo[0])) ? implode(' / ', $eixo[0]) : '';
        $arrEixo['e1'] = (isset($eixo[1])) ? implode(' / ', $eixo[1]) : '';
        
        $arrayGrafico['EIXOS'] = $arrEixo;
        $arrayGrafico['DADOS'] = json_encode($final, JSON_NUMERIC_CHECK);
        $arrayGrafico['CAB']   = json_encode($cab, JSON_NUMERIC_CHECK);
        
        return $arrayGrafico;
        
    }
    
    function comparar($setor='') {
        $setor = ($setor == '') ? appFunction::dadoSessao('setor') :  $setor;
        $this->painel->setSetor($setor);
        
        $rs = $this->painel->listarSetores(appFunction::dadoSessao('setor'));
        
        $this->set('admin', $this->painel->validarAdmin());
        $this->set('modulo', 'Comparação de Setores');
        $this->set('setores', $rs);
        $this->set('colab', $this->painel->dadosColaborador());
        $this->set('tabelaHeader', $this->painel->getHeaderTabela());
        $this->set('tabela', $this->painel->getTabela($setor));
        $this->set('boxes', $this->painel->getBoxes($setor));
        $this->render('painel/comparar');
    }
    
    function ranking() {
        
        
        
        $setor = ($setor == '') ? appFunction::dadoSessao('setor') :  $setor;
        $this->painel->setSetor($setor);
        
        $perfis = $this->painel->getPerfis();
        $linhas = $this->painel->getLinhas($setor);
        $regionais = $this->painel->getRegional($setor);
        $periodo = $this->painel->getPeriodo();
        
        $rs = $this->painel->listarSetores(appFunction::dadoSessao('setor'));
        
        $this->set('admin', $this->painel->validarAdmin());
        $this->set('head', $this->painel->getCabecalhoMes());
        $this->set('modulo', 'Ranking');
        $this->set('setores', $rs);
        $this->set('periodo', $periodo);
        $this->set('perfis', $perfis);
        $this->set('linhas', $linhas);
        $this->set('regionais', $regionais);
        $this->set('pilares', $this->painel->getListaPilares($setor));
        $this->set('colab',   $this->painel->dadosColaborador());
        $this->set('tabelaHeader', $this->painel->getHeaderTabela());
        $this->set('tabela', $this->painel->getTabela($setor));
        $this->set('boxes', $this->painel->getBoxes($setor));
        
        $this->render('painel/ranking');
    }
    
    function rank() {
        $setor   = appFunction::dadoSessao('setor');
        
        
        $funcao  = $_POST['cmbFuncao'];
        $pilar   = $_POST['cmbPilar'];
        $produto = $_POST['cmbProduto'];
        $periodo = $_POST['cmbPeriodo'];
        $linha   = $_POST['cmbLinha'];
        $regional = $_POST['cmbRegional'];
        
        $filtros = explode("|", $_POST['filtros']);
        
        
        if($periodo == "" || $produto == "" || $pilar == "") {
        //if(!isset($periodo) && !isset($produto) && !isset($pilar)) {
            echo 'Erro';
            
        } else {
            
            $arr = array('PRODUTO'  => $produto, 
                         'PILAR'    => $pilar, 
                         'PERIODO'  => $periodo, 
                         'FUNCAO'   => $funcao,
                         'LINHA'    => $linha,
                         'REGIONAL' => $regional);

            $dados = $this->painel->rankearEquipe($setor, $arr);

            
            @session_start();
            $x['dados'] = $dados;
            $x['criterios'] = $arr;
            $x['filtros'] = $filtros;
            $_SESSION['sess-rank'] = $x;
            
            $icon[1] = 'gold64.png';
            $icon[2] = 'silver64.png';
            $icon[3] = 'bronze64.png';
            $icon['out'] = 'part3s2.png';

            $final = [];
            foreach($dados as $i => $dado) {
                if($i <= 5) {
                    
                    $format['dec'] = $dado['DECIMAL'];
                    $format['sufix'] = $dado['SUFIXO'];
                    
                    $maxY = ($dado['VALOR'] > $maxY) ? $dado['VALOR'] : floor($maxY);
                    
                    $a[] = $dado['SETOR'].' '.$this->shortName($dado['NOME']);
                    //$mark['marker'] = array('symbol' => 'url(https://www.emsprescricao.com.br/public/profile/'.$dado['FOTO'].')', 'width' => '35', 'height' => '45');

                    //$mark['marker'] = array('symbol' => 'url(https://DHTEMS140796/app/view/painel/img/gold.png)', 'width' => '45', 'height' => '55');
                    $ar['data'][] = array('y' => $dado['VALOR'], 'color' => ($i <=3) ? '#2A3F54' : '#73879C', 'dataLabels' => true);


                    $foto[$dado['SETOR'].' '.$this->shortName($dado['NOME'])] = fotoPerfil($dado['FOTO']);

                    $i = ($i <= 3) ? $i : 'out';

                    $mark['marker'] = array('symbol' => 'url(https://'.$_SERVER['HTTP_HOST'].'/app/view/painel/img/'.$icon[$i].')', 'width' => '45', 'height' => '55');
                    $arg['data'][] = array('y' => $dado['VALOR'], 'dataLabels' => false, 'marker' => $mark['marker']);
                } else {
                    break;
                }
            }

            $ar['type'] = 'column';
            $arg['type'] = 'scatter';

            $final[] = $ar;
            $final[] = $arg;

            $series['cat'] = json_encode($a, JSON_NUMERIC_CHECK);
            $series['serie'] = json_encode($final, JSON_NUMERIC_CHECK);
            $series['format'] = $format;

            $this->set('filtros', $filtros);
            $this->set('maxY', $maxY);
            $this->set('fotosGrafico', json_encode($foto));
            $this->set('serie', $series);
            $this->set('dados', $dados);
            $this->set('criterio', $arr);
            echo $this->renderToString('painel/ranking-lista');
        }
    }
    
    public function imprimirRank() {
        @session_start();
        $dados = $_SESSION['sess-rank'];

        $this->set('dadoss', $dados['dados']);
        $this->set('criterio', $dados['criterios']);
        $this->set('filtros', $dados['filtros']);
        $html = utf8_decode($this->renderToString('painel/imprimir-rank'));
        appFunction::gerarPDF($html, '', "RankingDeSetores");
        
    }
    
    public function exportarRank() {
        $nomeDoArquivo = 'exportacao';
        @session_start();
        $dados = $_SESSION['sess-rank']['dados'];
        
        //print_r($dados);
        
        $arrColuna = array_keys($dados[1]);
        $arquivoCSV = $nomeDoArquivo.'_'.date('d_m_y_H-i-s').'.csv';
        $df = fopen(AppConf::caminho_fisico.'\\'.$arquivoCSV, 'w');
        
        //fputcsv($df, array_keys(reset($dados)),';');
        
        fputcsv($df, $arrColuna, ';', '"');
        
        foreach($dados as $row) {
            fputcsv($df, $row, ';', '"');
        }
        
        //print_r($arrColuna);
        
        fclose($df);
        
        
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$arquivoCSV);
        header('Content-Type: application/x-msexcel');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize(AppConf::caminho_fisico.'\\'.$arquivoCSV));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($arquivoCSV);
        
        
        unlink(AppConf::caminho_fisico.'\\'.$arquivoCSV);
        
        
        
        
    }
    
    function listarProdutoRank() {
        $setor   = appFunction::dadoSessao('setor');
        $pilar = $_POST['pilar'];
        $linha = $_POST['linha'];
        
        
        $dados = $this->painel->getListaProdutos($setor, $linha, $pilar);
        
        $periodo = $this->painel->getCabecalhoMes($pilar);
        $headers = $this->painel->getHeaderTabela();
        
        $u = [];
        foreach($headers as $k => $her) {
            if($her['NUMERICO']) {
                $u[] = array(
                    'ID' => $her['COLUNA'],
                    'DESC' => isset($periodo[$her['COLUNA']]) ? $periodo[$her['COLUNA']] : $her['LABEL']
                ); 
            }
        }
        
        
        $a['dados'] = $dados;
        $a['periodo'] = $u;
        echo json_encode($a, JSON_NUMERIC_CHECK);
    }
    
    function listarPilarRank() {
        $setor   = appFunction::dadoSessao('setor');
        $pilar = $_POST['linha'];
        $dados = $this->painel->getListaPilares($setor, $pilar);
        echo json_encode($dados, JSON_NUMERIC_CHECK);
    }
    
    function  listaSetor() {
        $rs = $this->painel->listarSetores(appFunction::dadoSessao('setor'));
        $this->set('setores', $rs);
        echo $this->renderToString('painel/modal-lista-setor');
    }
    
    function selComparar($setor='', $container='') {
        $this->painel->setSetor($setor);
        
        $colab = $this->painel->dadosColaborador();
        $dados = $this->painel->getColaboradorComparar($setor);
        
        $this->set('setor', $setor);
        $this->set('container', $container);
        $this->set('colab', $colab[1]);
        $this->set('dados', $dados); 
        echo $this->renderToString('painel/setor-comparar');
    }
    
    public function listarProduto($setor='',$pilar='') {
        
        if($setor != '' || $pilar != '') {
            $headers = $this->painel->getHeaderTabela();
            $dados = $this->painel->getTabelaProduto($setor, $pilar);
            $cab = $this->painel->getCabecalhoMes($pilar);

            $this->set('cab', $cab);
            $this->set('headers', $headers);
            $this->set('dados', $dados);
            echo $this->renderToString('painel/tabela-produtos');
        }
    }
    
    private function criarTabelaPilares($setor='') {
        $headers = $this->painel->getHeaderTabela();
        
        $cab = $this->painel->getCabecalhoMes();
        
        
        $dados = $this->painel->getTabela($setor);
       
      
      
        
        
        /*
        foreach($headers as $coluna) {
            if($coluna['VISIVEL'] == 1) {
                $arrCol[] = $coluna;   
            }
        }
        
        foreach($dados as $row) {
           foreach($arrCol as $col) {
               if($row[$col] == '(TOTAL)') {
                   $arr['GRUPO'] = 1;
               } else {
                   $arr['GRUPO'] = 0;
               }               
           }
        }
        */
        $this->set('cab', $cab);
        $this->set('headers', $headers);
        $this->set('dados', $dados);
        return $this->renderToString('painel/tabela-pilares');
    }

    function listarAvaliacaoPMA($setor) {
        $tabela = "";
        if ($setor != 0) {
            $this->painel->setSetor($setor);
            $rsAvaliacao = $this->painel->listarAvaliacaoPMA();

            if (count($rsAvaliacao) > 0) {
                $tabela = '<small><table class="table table-condensed table-striped">
                                <tr>
                                    <th>Ciclo</th>
                                    <th>Colaborador Avaliado</th>
                                    <th>Avaliado Por</th>
                                    <th>&nbsp;</th>
                                </tr>';

                for ($i = 1; $i <= count($rsAvaliacao); $i++) {
                $tabela .= '<tr>
                                <td>' . $rsAvaliacao[$i]['NOME_CICLO'] . '</td>
                                <td>' . $rsAvaliacao[$i]['REPRESENTANTE'] . '</td>
                                <td>' . $rsAvaliacao[$i]['GERENTE'] . '</td>
                                <td>
                                    <button type="button" href="'.appCOnf::caminho.'sistemas/PMA/avaliacao/imprimir/' . $rsAvaliacao[$i]['ID_AVALIACAO'] . '" class="btn btn-info btn-sm imprimir-avaliacao" data-value="' . $rsAvaliacao[$i]['ID_AVALIACAO'] . '" arquivo="' . $rsAvaliacao[$i]['SETOR'] . ' ' . $rsAvaliacao[$i]['NOME'] . '"><span class="glyphicon glyphicon-print"></span></button>
                                </td>
                            </tr>';
                }
                $tabela .= '</table></small>';
            }
        }
        return $tabela;
    }

    function imprimirAvaliacaoPMA() {
        $html = utf8_decode($_POST['html']);
        $nomeArquivo = $_POST['nomeArquivo'];
        $this->funcao->gerarPDF($html, '', $nomeArquivo);
    }

    function cabecalhoGrafico() {
        $arr = array();
        $rs = $this->painel->cabecalhoMeses();

        for ($i = 1; $i <= count($rs); $i++) {
            $arr[$i - 1] = $rs[$i]['MES'];
        }

        echo json_encode($arr);
    }

    function graficoPontuacao($setor) {
        $this->painel->setSetor($setor);
        $rs = $this->painel->graficoPontuacao();
        echo $rs[1]['MEDIA'];
    }

    function graficoProdutividade($setor) {
        $data = '';
        $this->painel->setSetor($setor);
        $rs = $this->painel->graficoProdutividade();

        $arr_final = array();

        for ($i = 1; $i <= count($rs); $i++) {
            $arr = array();
            $arr['name'] = $rs[$i]['PILAR'];
            $arr['yAxis'] = $rs[$i]['EIXO'];
            
            $eixo[$rs[$i]['EIXO']][] = $arr['name'];
            
            
            if (@$pdf == '1') {
                $arr['enableMouseTracking'] = false;
                $arr['shadow'] = false;
                $arr['animation'] = true;
            }

            for ($x = 1; $x <= 13; $x++) {
                $valor = $rs[$i]['M' . $x];
                if ($valor == '0') {
                    $valor = null;
                }
                $arr['data'][] = ($valor);
            }

            array_push($arr_final, $arr);
        }
        
        $k['eixo0'] = implode(' / ', $eixo[0]);
        $k['eixo1'] = implode(' / ', $eixo[1]);
        
        $wow = array('dados' => $arr_final, 'eixos' => $k);
        
        echo json_encode($wow, JSON_NUMERIC_CHECK);
    }

    function painelRanking($impressao = 0) {
        $rs_rnk = $this->painel->dadosRankingSetor();

        $tabela = '<p class="text-center lead">' . $rs_rnk[1]['RNK'] . '</p>';

        if ($impressao == 0) {

            $tabela = '<table width="100%">
                            <tr>
                                <td width="80%"><h5><small>RANKING</small></h5></td>
                                <td align="right"><span class="label label-default">' . $rs_rnk[1]['RNK'] . '</span></td>
                            </tr>
                        </table>';
        } else {

            $tabela = '<table width="100%" id="table-rank">
                            <tr>
                                <td align="center">' . $rs_rnk[1]['RNK'] . '</span></td>
                            </tr>
                    </table>';
        }

        return $tabela;
    }

    function painelMercado($impressao = 0) {
        $rs_mercado = $this->painel->dadosMercadoProduto();

        if ($impressao == 0) {

            $tabela = '<table width="100%">
                            <tr>
                                <td width="80%"><h5><small>MERCADO</small></h5></td>
                                <td align="right"><div class="label label-default">' . $this->formatarMoeda($rs_mercado[1]['MERCADO']) . '</div></td>
                            </tr>
                            <tr>
                                <td width="80%"><h5><small>PRODUTO</small></h5></td>
                                <td align="right"><div class="label label-default">' . $this->formatarMoeda($rs_mercado[1]['PRODUTO']) . '</div></td>
                            </tr>
                    </table>';
        } else {
            $tabela = '<table width="100%">
                            <tr>
                                <td><strong>MERCADO:</strong></td>
                                <td><div class="label label-default">' . $this->formatarMoeda($rs_mercado[1]['MERCADO']) . '</div></td>
                            </tr>
                            <tr>
                                <td><strong>PRODUTO:</strong></td>
                                <td><div class="label label-default">' . $this->formatarMoeda($rs_mercado[1]['PRODUTO']) . '</div></td>
                            </tr>
                        </table>';
        }
        return $tabela;
    }

    function painelPontuacao($impressao = 0) {
        $rs = $this->painel->dadosPontuacao();
        $tabela = '<table width="100%">';

        for ($i = 1; $i <= count($rs); $i++) {

            $icone = '';
            $label = '';

            switch ($rs[$i]['VALOR']) {

                case 1: {
                    $label = 'label-danger';
                    $icone = 'bullet_red.png';
                    $labelImp = 'danger';
                    break;
                }

                case 2: {
                    $label = 'label-warning';
                    $icone = 'bullet_yellow.png';
                    $labelImp = 'warning';
                    break;
                }

                case 3: {
                    $label = 'label-success';
                    $icone = 'bullet_green.png';
                    $labelImp = 'success';
                    break;
                }

                case 4: {
                    $label = 'label-primary';
                    $icone = 'bullet_blue.png';
                    $labelImp = 'primary';
                    break;
                }
            }

            if ($impressao == 0) {

                $tabela .= '<tr>
                                <td width="80%"><h5><small>' . $rs[$i]['PILAR'] . '</small></h5></td>
                                <td class="' . $labelImp . '" width="20%"><div class="label ' . $label . ' ' . $labelImp . '"><img src="' . appConf::caminho . 'public/img/' . $icone . '" class="img-rounded" />' . $rs[$i]['VALOR'] . '</div></td>
                            </tr>';
            } else {
                $tabela .= '<tr>
                                <td width="85%">' . $rs[$i]['PILAR'] . '</td>
                                <td width="15%" class="' . $labelImp . '"><div><!--<img src="' . appConf::caminho . 'public/img/' . $icone . '" />-->' . $rs[$i]['VALOR'] . '</div></td>
                            </tr>';
            }
        }

        $tabela .= '</table>';

        return $tabela;
    }

    function painelMktShare($impressao = 0) {
        $rs_mercado = $this->painel->dadosMercadoProduto();
        $rs_brasil = $this->painel->dadosShareBrasil();

        if ($rs_mercado[1]['SHARE'] >= $rs_brasil[1]['BRASIL']) {
            $img = 'up.png';
            $class = 'label label-success';
            $classImp = 'success';
        } else {
            $img = 'down.png';
            $class = 'label label-danger';
            $classImp = 'danger';
        }


        if ($impressao == 0) {
            $tabela = '<table width="100%">
                        <tr>
                            <td><h5><small>MKT SHARE</small></h5></td>
                            <td align="right"><span class="' . $class . '">
                                <img id="tooltip"  src="' . appConf::caminho . 'public/img/' . $img . '" data-container="body" data-toggle="popover" data-placement="top" data-content="<img src=' . appConf::caminho . 'public/img/flag_brazil.png /> <strong>' . $this->formatarMoeda($rs_brasil[1]['BRASIL']) . '%</strong>" />
                                ' . $this->formatarMoeda($rs_mercado[1]['SHARE']) . '%</span>
                            </td>
                        </tr>
                    </table>';
        } else {
            $tabela = '<table width="100%">		
                            <tr>
                                <td align="center" >
                                    <table width="100%">
                                          <tr><td class="' . $classImp . '" ><div style="font-size:14.5px">
                                            ' . $this->formatarMoeda($rs_mercado[1]['SHARE']) . '%</div></td></tr>
                                    </table>
                                </td>
                            </tr>
                        </table>';
        }
        return $tabela;
    }

    function painelColaborador() {
        $rs = $this->painel->dadosColaborador();

        if (count($rs) > 0) {

            $gd = '';
            $gr = '';
            $gn = '';

            if ($rs[1]['GD'] != "") {
                $gd = '<h5><small><strong>GD: </strong>' . $rs[1]['GD'] . '</small></h5>';
            }
            if ($rs[1]['GR'] != "") {
                $gr = '<h5><small><strong>GR: </strong>' . $rs[1]['GR'] . '</small></h5>';
            }

            $painel = '
                <h5><strong>' . $rs[1]['COLABORADOR'] . '</strong></h5>
                <h5><small><strong>FUNÇÃO:</strong> ' . $rs[1]['FUNCAO'] . '</small></h5>
                <h5><small><strong>NOME SETOR:</strong>  ' . $rs[1]['NOME_SETOR'] . '</small></h5>
                <h5><small><strong>LINHA:</strong>  ' . $rs[1]['LINHA'] . '</small></h5>
                ' . $gd . $gr . $gn . '
                <h5><small><strong>INICIO NO SETOR: </strong>' . $this->funcao->formatarData($rs[1]['DT_INICIO']) . '</small></h5>';

            return
                    '<div class="row">
                        <div class="col-lg-3 col-xs-3 col-imp-foto">
                                <img widsth="100%" height="158" class="ismg-responsive" src="' . str_replace(appConf::caminho, '', $this->funcao->fotoColaborador($rs[1]['ID_COLABORADOR'])) . '" />
                        </div>

                        <div class="col-lg-9 col-xs-9 col-imp-dados">'
                        . $painel .
                    '</div>
                    </div>';
        }
    }

    function painelPilaresProdutividade($impressao = 0, $listaPxShare = '', $listaMktShare = '', $listaCobObj = '') {

        $nMeses = 13;

        $rsProdutividade           = $this->painel->dadosProdutividade();
        $rsProdutividadeTotal      = $this->painel->dadosProdutividadeTotal();
        $rsPrescriptionShare       = $this->painel->dadosPrescriptionShare($listaPxShare);
        $rsPrescriptionShareTotal  = $this->painel->dadosPrescriptionShareTotal();
        $rsPrescriptionShareBrasil = $this->painel->dadosPrescriptionShareBrasil();
        $rsDemanda                 = $this->painel->dadosDemanda($listaMktShare);
        $rsDemandaTotal            = $this->painel->dadosDemandaTotal();
        $rsCoberturaObjetivo       = $this->painel->dadosCoberturaObjetivo($listaCobObj);
        $rsCoberturaObjetivoTotal  = $this->painel->dadosCoberturaObjetivoTotal();

        $mAtual = $this->funcao->mesAtualPainel();
        $mInicio = -(((13 - $mAtual) - 1) - 13);
        $m = $mInicio;

        if ($impressao == 0) {

            $tabela = '<div class="table-responsive">
			<table id="tb-dados-pilares">' . $this->cabecalhoMeses();

            // PRODUTIVIDADE
            $tabela .= '<tr class="pai"><td class="pilar_plus nome-pilar"><span class="glyphicon glyphicon-plus"></span> <strong>PRODUTIVIDADE</strong></td>';

            for ($i = 1; $i <= $nMeses; $i++) {
                $tabela .= '<td>
                    <p class="' . $this->rankProdutividadeTitulo($rsProdutividadeTotal[1]['S' . $i], 1) . '">
                            <strong>' . $this->funcao->formatarMoeda($rsProdutividadeTotal[1]['M' . $i], 1, '%') . '</strong>
                    </p></td>';
            }

            $tabela .= '</tr>';

            $m = $mInicio;


            for ($i = 1; $i <= count($rsProdutividade); $i++) {
                $tabela .= '<tr class="filho"><td><p class="">' . $rsProdutividade[$i]['PILAR'] . '</p></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankProdutividade($rsProdutividade[$i]['S' . $j]) . '">' . $this->funcao->formatarMoeda($rsProdutividade[$i]['M' . $j]) . '</p></td>';
                }

                $tabela .= '</tr>';
            }

            // PRESCRIPTION SHARE
            for ($i = 1; $i <= count($rsPrescriptionShareTotal); $i++) {

                $tabela .= '<tr class="pai"><td class="nome-pilar"><span class="glyphicon glyphicon-plus"></span> <strong>PRESCRIPTION SHARE</strong></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValorTotalBrasil($rsPrescriptionShareTotal[1]['M' . $j], $rsPrescriptionShareBrasil[1]['M' . $j]) . '"><strong>' . $this->funcao->formatarMoeda($rsPrescriptionShareTotal[1]['M' . $j], 1, '%') . '</strong></p></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsPrescriptionShare); $i++) {

                $rsPrescriptionShareBrasil = $this->painel->dadosPrescriptionShareBrasil($rsPrescriptionShare[$i]['PRODUTO']);

                $tabela .= '<tr class="filho">
                                <td class="">
                                        <p class=""><input name="chkPxShare[]" value="' . $rsPrescriptionShare[$i]['PRODUTO'] . '" type="checkbox" />' . $rsPrescriptionShare[$i]['PRODUTO'] . '</p>
                                </td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValorProduto($rsPrescriptionShare[$i]['M' . $j], @$rsPrescriptionShareBrasil[1]['M' . $j]) . '">' . $this->funcao->formatarMoeda($rsPrescriptionShare[$i]['M' . $j], 1, '%') . '</p></td>';
                }

                $tabela .= '</tr>';
            }

            // MARKET SHARE
            for ($i = 1; $i <= count($rsDemandaTotal); $i++) {

                $rsDemandaBrasil = $this->painel->dadosDemandaBrasil($rsDemandaTotal[$i]['PRODUTO']);

                $tabela .= '<tr class="pai">
                                <td class="nome-pilar"><span class="glyphicon glyphicon-plus"></span> <strong>MARKET SHARE</strong></td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValorTotalBrasil($rsDemandaTotal[1]['M' . $j], $rsDemandaBrasil[1]['M' . $j]) . '"><strong>' . $this->funcao->formatarMoeda($rsDemandaTotal[1]['M' . $j], 1, '%') . '</strong></p></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsDemanda); $i++) {

                $rsDemandaBrasil = $this->painel->dadosDemandaBrasil($rsDemanda[$i]['PRODUTO']);

                $tabela .= '<tr class="filho">
                                <td class="">
                                        <p class=""><input name="chkMktShare[]" value="' . $rsDemanda[$i]['PRODUTO'] . '" type="checkbox" />' . $rsDemanda[$i]['PRODUTO'] . '</p>
                                </td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValorProduto($rsDemanda[$i]['M' . $j], @$rsDemandaBrasil[1]['M' . $j]) . '">' . $this->funcao->formatarMoeda($rsDemanda[$i]['M' . $j], 1, '%') . '</p></td>';
                }

                $tabela .= '</tr>';
            }

            // COBERTURA OBJETIVO
            for ($i = 1; $i <= count($rsCoberturaObjetivoTotal); $i++) {

                $tabela .= '<tr class="pai">
                                <td class="nome-pilar"><span class="glyphicon glyphicon-plus"></span><strong> COB. OBJETIVO</strong></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValorTotal($rsCoberturaObjetivoTotal[1]['M' . $j], 1) . '"><strong>' . $this->funcao->formatarMoeda($rsCoberturaObjetivoTotal[1]['M' . $j], 1, '%') . '</strong></p></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsCoberturaObjetivo); $i++) {

                $tabela .= '<tr class="filho">
                                <td class="">
                                        <p class=""><input name="chkCobObj[]" value="' . $rsCoberturaObjetivo[$i]['PRODUTO'] . '" type="checkbox" />' . $rsCoberturaObjetivo[$i]['PRODUTO'] . '</p>
                                </td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td><p class="' . $this->rankValor($rsCoberturaObjetivo[$i]['M' . $j]) . '">' . $this->formatarMoeda($rsCoberturaObjetivo[$i]['M' . $j], 1, '%') . '</p></td>';
                }

                $tabela .= '</tr>';
            }

            $tabela .= '</table></div>';
        } else {

            $tabela = '<div class="table-responsive"><table id="tb-dados-pilares">';

            // PRODUTIVIDADE
            $tabela .= '<tr><td class="pilar_plus nome-pilar"><span class="glyphicon glyphicon-plus"></span> <strong>PRODUTIVIDADE</strong></td>';

            for ($i = 1; $i <= $nMeses; $i++) {
                $tabela .= '<td class="' . $this->rankProdutividadeTitulo($rsProdutividadeTotal[1]['S' . $i], 1) . '">
                                <strong>' . $this->funcao->formatarMoeda($rsProdutividadeTotal[1]['M' . $i], 1, '%') . '</strong>
                            </td>';
            }

            $tabela .= '</tr>';

            $m = $mInicio;

            for ($i = 1; $i <= count($rsProdutividade); $i++) {
                $tabela .= '<tr><td>' . $rsProdutividade[$i]['PILAR'] . '</td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="valor-pilar ' . $this->rankProdutividade($rsProdutividade[$i]['S' . $j]) . '">' . $this->funcao->formatarMoeda($rsProdutividade[$i]['M' . $j]) . '</td>';
                }

                $tabela .= '</tr>';
            }

            // PRESCRIPTION SHARE
            for ($i = 1; $i <= count($rsPrescriptionShareTotal); $i++) {

                $tabela .= '<tr><td class="nome-pilar"><span class="glyphicon glyphicon-plus"></span> <strong>PRESCRIPTION SHARE</strong></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValorTotalBrasil($rsPrescriptionShareTotal[1]['M' . $j], $rsPrescriptionShareBrasil[1]['M' . $j]) . '"><strong>' . $this->funcao->formatarMoeda($rsPrescriptionShareTotal[1]['M' . $j], 1, '%') . '</strong></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsPrescriptionShare); $i++) {

                $rsPrescriptionShareBrasil = $this->painel->dadosPrescriptionShareBrasil($rsPrescriptionShare[$i]['PRODUTO']);

                $tabela .= '<tr><td>' . $rsPrescriptionShare[$i]['PRODUTO'] . '</td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValorProduto($rsPrescriptionShare[$i]['M' . $j], @$rsPrescriptionShareBrasil[1]['M' . $j]) . '">' . $this->funcao->formatarMoeda($rsPrescriptionShare[$i]['M' . $j], 1, '%') . '</td>';
                }

                $tabela .= '</tr>';
            }

            // MARKET SHARE
            for ($i = 1; $i <= count($rsDemandaTotal); $i++) {

                $rsDemandaBrasil = $this->painel->dadosDemandaBrasil($rsDemandaTotal[$i]['PRODUTO']);

                $tabela .= '<tr><td><span class="glyphicon glyphicon-plus"></span> <strong>MARKET SHARE</strong></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValorTotalBrasil($rsDemandaTotal[1]['M' . $j], $rsDemandaBrasil[1]['M' . $j]) . '"><strong>' . $this->funcao->formatarMoeda($rsDemandaTotal[1]['M' . $j], 1, '%') . '</strong></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsDemanda); $i++) {

                $rsDemandaBrasil = $this->painel->dadosDemandaBrasil($rsDemanda[$i]['PRODUTO']);

                $tabela .= '<tr><td>' . $rsDemanda[$i]['PRODUTO'] . '</td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValorProduto($rsDemanda[$i]['M' . $j], @$rsDemandaBrasil[1]['M' . $j]) . '">' . $this->funcao->formatarMoeda($rsDemanda[$i]['M' . $j], 1, '%') . '</td>';
                }

                $tabela .= '</tr>';
            }

            // COBERTURA OBJETIVO
            for ($i = 1; $i <= count($rsCoberturaObjetivoTotal); $i++) {

                $tabela .= '<tr><td class="nome-pilar"><span class="glyphicon glyphicon-plus"></span><strong> COB. OBJETIVO</strong></td>';

                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValorTotal($rsCoberturaObjetivoTotal[1]['M' . $j], 1) . '"><strong>' . $this->funcao->formatarMoeda($rsCoberturaObjetivoTotal[1]['M' . $j], 1, '%') . '</strong></td>';
                }

                $tabela .= '</tr>';
            }

            for ($i = 1; $i <= count($rsCoberturaObjetivo); $i++) {

                $tabela .= '<tr><td>' . $rsCoberturaObjetivo[$i]['PRODUTO'] . '</td>';


                for ($j = 1; $j <= $nMeses; $j++) {
                    $tabela .= '<td class="' . $this->rankValor($rsCoberturaObjetivo[$i]['M' . $j]) . '">' . $this->formatarMoeda($rsCoberturaObjetivo[$i]['M' . $j], 1, '%') . '</td>';
                }

                $tabela .= '</tr>';
            }

            $tabela .= '</table></div>';
        }

        return $tabela;
    }

    function comboSetor($setor, $retorno = 1) {

        $combo = $this->funcao->comboSetor($setor);

        if ($retorno == 0) {

            return $combo;
        } else {
            echo $combo;
        }
    }

    function mesDemanda() {
        return $this->painel->dadosMesDemanda();
    }

    private function numeroEstrela($valor) {

        $full_star = '<img src="img/star_full_small.png" />';
        $empty_star = '<img src="img/star_empty_small.png" />';
        $retorno = '';

        switch ($valor) {

            case $valor <= 500: {
                $retorno = $full_star . $empty_star . $empty_star . $empty_star . $empty_star;
                break;
            }

            case $valor >= 501 && $valor <= 1000: {
                $retorno = $full_star . $full_star . $empty_star . $empty_star . $empty_star;
                break;
            }

            case $valor >= 1001 && $valor <= 2000: {
                $retorno = $full_star . $full_star . $full_star . $empty_star . $empty_star;
                break;
            }

            case $valor >= 2001 && $valor <= 4000: {
                $retorno = $full_star . $full_star . $full_star . $full_star . $empty_star;
                break;
            }

            case $valor >= 4001 : {
                $retorno = $full_star . $full_star . $full_star . $full_star . $full_star;
                break;
            }
        }

        return $retorno;
    }

    function produtosDestaque() {
        $rs = $this->painel->dadosProdutosDestaque();
        $rsCab = $this->painel->cabecalhoProdutosDestaque();

        $tabela = '<table id="tb-incentivo" class="table table-striped table-bordered">
                    <tr>
			<td class="fundo"><h5><small><strong>PRODUTO</strong></small></h5></td>';

        for ($i = 1; $i <= 13; $i++) {
            $tabela .= '<td class="fundo"><h5><small><strong>' . $rsCab[1]['M' . $i] . '</strong></h5></small></td>';
        }
        $tabela .= '</tr>';

        for ($i = 1; $i <= count($rs); $i++) {

            $tabela .= '<tr>
                            <td>
                                <table>
                                    <tr>
                                        <td width="90" ><h5><small>' . $rs[$i]['PRODUTO'] . '</small></h5></td>
                                        <td width="90" align="right">' . $this->numeroEstrela($rs[$i]['TOTAL']) . '</td>
                                    </tr>
                                </table>
                            </td>';

            for ($j = 1; $j <= 12; $j++) {
                $tabela .= '<td><h5><small>' . $this->funcao->formatarMoeda($rs[$i]['M' . $j], 0) . '</small></h5></td>';
            }
            $tabela .= '<td><h5><small>' . $this->funcao->formatarMoeda($rs[$i]['TOTAL'], 0) . '</small></h5></td>
				</tr>';
        }
        $tabela .= '</table>';
        return $tabela;
    }

    function imprimirPainel() {

        $setor            = $_POST['setor'];
        $listaCobObj      = $this->pivotArray($_POST['chkCobObj']);
        $listaPxShare     = $this->pivotArray($_POST['chkPxShare']);
        $listaMktShare    = $this->pivotArray($_POST['chkMktShare']);
        $graficoEvolucao  = trim($_POST['graficoEvolucao']).'.png';
        $graficoPontuacao = trim($_POST['graficoPontuacao']).'.png';


        $nomeArquivo = appFunction::dadoSessao('setor') . '_Painel_EMS_Prescricao';
        $this->painel->setSetor($setor);

        $rs_brasil = $this->painel->dadosShareBrasil();

        $this->set('variavel', 'Jeff');
        $this->set('setor', $setor);
        $this->set('dadosColaborador', $this->painelColaborador());

        $this->set('graficoEvolucao', $graficoEvolucao);
        $this->set('graficoPontuacao', $graficoPontuacao);

        $this->set('mktShare', $this->painelMktShare(1));
        $this->set('mercado', $this->painelMercado(1));
        $this->set('ranking', $this->painelRanking(1));
        $this->set('dadosShareBrasil', $this->formatarMoeda($rs_brasil[1]['BRASIL']));
        $this->set('cabecalhoMes', $this->cabecalhoMeses('Pilares'));
        $this->set('mesDemanda', $this->mesDemanda());
        $this->set('produtividade', $this->painelPilaresProdutividade(1, $listaPxShare, $listaMktShare, $listaCobObj));
        $this->set('comboSetor', $this->comboSetor($this->funcao->dadoSessao('setor'), 0));
        $this->set('pontuacao', $this->painelPontuacao(1));
        $html = utf8_decode($this->renderToString('painelImprimirView'));

        
        //echo $html;
        appFunction::gerarPDF($html, '', $nomeArquivo);
        
        $pathGraficos = PORTAL_ROOT.'plugins/exporting-server/files/';
        //echo $pathGraficos.$graficoEvolucao;
        
        
        if(file_exists($pathGraficos.$graficoEvolucao)) {
            @unlink($pathGraficos.$graficoEvolucao);
            @unlink($pathGraficos.$graficoPontuacao);
        }
    }

    private function pivotArray($arr) {
        $tmp = '';
        if (count($arr) > 0) {

            for ($i = 0; $i < count($arr); $i++) {
                if ($arr[$i] != "") {
                    $tmp .= $arr[$i] . ", ";
                }
            }

            $tmp = substr($tmp, 0, strlen($tmp) - 2);
        }

        return $tmp;
    }

    private function formatarMoeda($numero, $decimal = 1, $simbolo = '') {

        if ($numero == '' || is_null($numero)) {
            return '  &nbsp;';
        } else {
            return number_format($numero, $decimal, ",", ".") . $simbolo;
        }
    }

    private function rankProdutividadeTitulo($valor = '', $bullet = '') {
        $classe = '';
        $bl = '';
        if ($valor == '') {
            $classe = 'alert alert-gray text-center';
        } else {
            if ($valor == 1) {
                $classe = 'alert alert-success text-center';
                $bl = ' icon-green';
            } else {
                $classe = 'alert alert-danger text-center';
                $bl = ' icon-red';
            }
        }

        if ($bullet == '') {
            $bl = '';
        }

        return $classe . $bl;
    }

    private function rankProdutividade($valor = '', $bullet = '') {

        if ($valor == '') {
            $classe = 'list-group-item disabled text-center';
        } else {

            if ($valor == 1) {
                $classe = 'list-group-item list-group-item-success text-center';
            } else {
                $classe = 'list-group-item list-group-item-danger text-center';
            }
        }

        return $classe;
    }

    private function rankValorTotal($valor, $bullet = '') {
        $classe = '';
        $bl = '';
        if ($valor == '' || is_null($valor)) {
            $classe = 'alert alert-gray text-center';
        } else {

            if ($valor > 100) {
                $classe = 'alert alert-success text-center';
                $bl = ' icon-green';
            } else {
                $classe = 'alert alert-danger text-center';
                $bl = ' icon-red';
            }
        }

        if ($bullet == '') {
            $bl = '';
        }

        return $classe . $bl;
    }

    private function rankValorTotalBrasil($valor, $brasil = 0) {

        if ($valor == '' || is_null($valor)) {
            //$classe = 'alert alert-warning text-center';	
            $classe = 'alert alert-gray text-center';
        } else {

            if ($valor == $brasil) {
                $classe = 'alert alert-gray text-center';
            } else {
                if ($valor >= $brasil) {
                    $classe = 'alert alert-success text-center';
                } else {
                    $classe = 'alert alert-danger text-center';
                }
            }
        }


        return $classe;
    }

    private function rankValor($valor) {

        if ($valor == '') {
            return 'list-group-item disabled text-center';
        } else {

            if ($valor > 100) {
                return 'list-group-item list-group-item-success text-center';
            } else {
                return 'list-group-item list-group-item-danger text-center';
            }
        }
    }

    private function rankValorProduto($valor, $brasil = 0) {



        if ($valor == '') {
            //return 'list-group-item-warning text-center';	
            return 'list-group-item disabled text-center';
        } else {
            if ($valor == $brasil) {
                return 'list-group-item disabled text-center';
            } else {

                if ($valor >= $brasil) {
                    return 'list-group-item list-group-item-success text-center ' . $valor . '-' . $brasil . '';
                } else {
                    return 'list-group-item list-group-item-danger text-center';
                }
            }
        }
    }

    private function cabecalhoMeses($titulo = '&nbsp;') {
        $rs_cab_meses = $this->painel->cabecalhoMeses();
        $tabela = "";
        $tabela = '<tr><td id="titulo-pilar">' . $titulo . '</td>';

        for ($i = 1; $i <= count($rs_cab_meses); $i++) {
            $tabela .= '<td id="nome-mes" class=""><p class="text-center"><strong>' . $rs_cab_meses[$i]['MES'] . '</strong></p></td>';
        }
        $tabela .= '</tr>';
        return $tabela;
    }

}

?>