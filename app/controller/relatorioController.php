<?php
require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/relatorioModel.php');
require_once('app/model/arquivoModel.php');
require_once('app/model/diretorioModel.php');


class relatorioController extends appController {

	private $relatorio = null;
	private $funcao = null;
	private $arquivo = null;
	private $diretorio = null;
	
	private $barraAdmin = null;
	private $barraDiretorio = null;
	private $barraArquivo = null;

	public function relatorioController() {
		$this->relatorio = new relatorioModel();
		$this->funcao = new appFunction();	
		$this->arquivo = new arquivoModel();
		$this->diretorio = new diretorioModel();
		
		//$this->barraAdminArquivos();
	}


	public function listarDiretorio($selecionar='') {
		$rs = $this->relatorio->listarDiretorio();
		$pasta = '';
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['ID_PASTA']) {
				$classe = 'ativo';	
			} else {
				$classe = '';
			}
			
			$pasta .= '<a class="'.$classe.'" href="'.appConf::caminho.'relatorio/abrir/'.$rs[$i]['ID_PASTA'].'">
						<p><small><b><img src="'.appConf::caminho.'public/img/foldermini.png"> '.$rs[$i]['PASTA'].'</b></small></p></a>';

		}
		return $pasta;
	}
	
	public function arvoreMenu($abrir='') {
		$rs = $this->relatorio->listarDiretorio();
		
		
		$out = "";
		$bar = $separador;
		$nivel++;
		$out .= '<ul>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($abrir != "") {
				$out .= '<li>'.$this->arvoreMenu($rs[$i]['ID_PASTA']).'</li>';	
			
			} else {
				$out .= '<li>'.$rs[$i]['PASTA'].'</li>';	
			}
		}

		
		$out .= "</ul>";
		
		return $out;
	}
	
	public function mensuRaiz($selecionar='', $abrir='') {
		$rs = $this->relatorio->listarDiretorio();
		
		
		
	}
	
	public function menuRaisz($selecionar='') {
		$rs = $this->relatorio->listarDiretorio();
		$pasta = '';
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['ID_PASTA']) {
				$classe = 'panel-default';	
			} else {
				$classe = 'panel-defaultt';
			}
			
			$pasta .= '<div class="menu-padrao">
						<div class="panel '.$classe.'">
						<div class="panel-heading"><img class="subPasta" value="'.$rs[$i]['ID_PASTA'].'" src="'.appConf::caminho.'public/img/foldermini.png">
						<a href="'.appConf::caminho.'relatorio/abrir/'.base64_encode($rs[$i]['ID_PASTA']).'"><small><b> '.$rs[$i]['PASTA'].'</b></small></a>
						<div id="sub'.$rs[$i]['ID_PASTA'].'"></div>
				       </div>
					   </div>
					   </div>';
		}
		return $pasta;
	}
	
public function menuRaiz($selecionar='') {
		$rs = $this->relatorio->listarDiretorio();
		$pasta = '<ul class="menu-raiz">';
		for($i=1;$i<=count($rs);$i++) {
			

			$pasta .= '<li><div class="menuPadrao folder-open" value="'.$rs[$i]['ID_PASTA'].'"></div><img src="'.appConf::caminho.'public/img/foldermini.png">
						<!--<a href="'.appConf::caminho.'relatorio/abrir/'.base64_encode($rs[$i]['ID_PASTA']).'"><small><b> '.$rs[$i]['PASTA'].'</b></small></a>-->
						
						<a href="#" class="abrir-diretorio" diretorio="'.base64_encode($rs[$i]['ID_PASTA']).'"><small><b> '.$rs[$i]['PASTA'].'</b></small></a>
						
						<div id="sub'.$rs[$i]['ID_PASTA'].'" style="display:none"></div>
				      </li>';
		}
		$pasta .= '</ul>';
		return $pasta;
	}
	
	public function subPasta($idPasta) {
		$this->diretorio->setIdPasta($idPasta);
		$rs = $this->diretorio->selecionarSubDiretorio();

		$s = new diretorioModel();
		
		$pasta = '<ul>';
		

		
		for($i=1;$i<=count($rs);$i++) {
			
			$s->setIdPasta($rs[$i]['ID_PASTA']);
			$r = $s->contarSubDiretorio();
			
			if($r > 0) {
				$classe = 'folder-open';
			} else {
				$classe = 'no-folder';	
			}
			

			$pasta .= '<li><div class="subPasta '.$classe.'" value="'.$rs[$i]['ID_PASTA'].'" ></div><img src="'.appConf::caminho.'public/img/foldermini.png">
						
						<!--<a href="'.appConf::caminho.'relatorio/abrir/'.base64_encode($rs[$i]['ID_PASTA']).'"><small> '.$rs[$i]['PASTA'].'</small></a>
						-->
						<a href="#" class="abrir-diretorio" diretorio="'.base64_encode($rs[$i]['ID_PASTA']).'"><small> '.$rs[$i]['PASTA'].'</small></a>
						
						<div id="sub'.$rs[$i]['ID_PASTA'].'" style="display:none"></div>
				      </li>';
		}
		$pasta .= '</ul>';
		echo $pasta;
	}
	
	public function pesquisar() {
		set_time_limit(0);
		
		$criterio = htmlentities(utf8_decode($_POST['txtCriterio']));
		$idPasta = $_POST['txtIdPasta'];
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		$rs = $this->relatorio->pesquisar($criterio, $idPasta);
		
		$total = "";
		
		for($i=1;$i<=count($rs);$i++) {
			
			$this->arquivo->setIdArquivo($rs[$i]['ID_ARQUIVO']);
			$this->arquivo->selecionar();
					
			$tipo = '.'.$this->arquivo->getExtensao();
			$icone = $this->funcao->extensaoArquivo($this->arquivo->getExtensao());
			$link = '';//appConf::caminho.'relatorio/download/'.$this->arquivo->getIdArquivo();
			$data = $this->funcao->dataFisicoArquivo($this->arquivo->getCaminhoArquivo());				
			$tamanho = $this->funcao->tamanhoFisicoArquivo($this->arquivo->getCaminhoArquivo());
			$acao = $this->barraArquivo($this->arquivo->getIdArquivo());
			
			
			$total .= '<div class="row" style="margin-bottom:10px;">
						<div class="col-lg-5">
							<img src="'.$icone.'">'.$rs[$i]['ARQUIVO'].'
							<h5 style="margin-top:2px;margin-left:17px;"><small><a style="color:#aaa" href="'.appConf::caminho.'relatorio/abrir/'.base64_encode($this->arquivo->getIdpasta()).'">'.$this->arquivo->getCaminho().'</a></small></h5>
							
						</div>
						<div class="col-lg-2"><small>'.$data.'</small></div>
						<div class="col-lg-1"><small>'.$tipo.'</small></div>
						<div class="col-lg-2"><small>'.$tamanho.'</small></div>
						<div class="col-lg-2">
							'.$acao.'
						</div>
					   </div>';
		}
		
		$retornoPesquisa = '<form id="formManipularArquivo" method="post" action="'.appConf::caminho.'relatorio/manipularArquivo">
					<input type="hidden" id="acao" name="acao" value="" />
					<input type="hidden" name="idPastaAtual" value="'.base64_encode($idPasta).'" />
					<input type="hidden" id="idPastaMover" name="idPastaMover" value="" />
					'.$total.'
				</form>';
				
		
		
		$this->set('nomeDiretorio', "Resultado da Pesquisa por: '".utf8_encode($criterio)."'");
		$this->set('caminhoDiretorio', "");
		
		$this->set('caminhoDiretorio', count($rs)." registro(s) encontrado(s) em ".$this->diretorio->getPasta().".");
		$this->set('idPasta', $retornoPesquisa);
		$this->set('diretorio', $this->menuRaiz());
		$this->set('barraAdmin', $this->barraAdmin($idPasta));
		$this->set('barraPesquisa', $this->barraPesquisa($idPasta));
		
		
		$this->render('relatorioView');	
		
	}	

	
	public function abrirDiretorio($idPasta, $pagina=0) {

		
		$rs = $this->relatorio->listarConteudoDiretorio($idPasta, $pagina);
		$numRegistro = $rs[1]['MAIOR'];
		$total = '';
		
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($rs[$i]['TIPO'] == 1) {
				$this->diretorio->setIdPasta($rs[$i]['ID']);
				$this->diretorio->selecionar();
				
				
				$tipo = 'Diretório';
				$icone = appConf::caminho.'public/img/foldermini.png';
				$link = 'href="#" diretorio="'.base64_encode($rs[$i]['ID']).'"'; //appConf::caminho.'relatorio/abrir/'.base64_encode($rs[$i]['ID']);
				$data = $this->funcao->formatarData($rs[$i]['DATA_CRIACAO']);
				$tamanho = $this->diretorio->getTamanho();
				$acao = $this->barraDiretorio($rs[$i]['ID'], $rs[$i]['ID_DIRETORIO']);
			} else {
				
				$this->arquivo->setIdArquivo($rs[$i]['ID']);
				$this->arquivo->selecionar();
				
				$tipo = '.'.$this->arquivo->getExtensao();
				$icone = $this->funcao->extensaoArquivo($this->arquivo->getExtensao());
				$link = 'href="#"';//appConf::caminho.'relatorio/download/'.$this->arquivo->getIdArquivo();
				$data = $this->funcao->dataFisicoArquivo($this->arquivo->getCaminhoArquivo());				
				$tamanho = $this->funcao->tamanhoFisicoArquivo($this->arquivo->getCaminhoArquivo());
				$acao = $this->barraArquivo($this->arquivo->getIdArquivo());
			}
			
			
			$total .= '<div class="row">
						<div class="col-lg-5"><a '.$link.' class="abrir-diretorio"><p><small class="tedxt-nowrap"><img src="'.$icone.'">'.$rs[$i]['ARQUIVO'].'</small></p></a></div>
						<div class="col-lg-2"><small>'.$data.'</small></div>
						<div class="col-lg-1"><small>'.$tipo.'</small></div>
						<div class="col-lg-2"><small>'.$tamanho.'</small></div>
						<div class="col-lg-2">
							'.$acao.'
						</div>
					   </div>';
		}
		
		$paginacao = $this->paginacao($numRegistro, $pagina, $idPasta);
		return '<form id="formManipularArquivo" method="post" action="'.appConf::caminho.'relatorio/manipularArquivo">
					<input type="hidden" id="acao" name="acao" value="" />
					<input type="hidden" name="idPastaAtual" value="'.base64_encode($idPasta).'" />
					<input type="hidden" id="idPastaMover" name="idPastaMover" value="" />
					'.$total.'
				</form>'.
				
				$paginacao;
	}
	

	
	public function paginacao($numRegistro=0, $pagina=0, $pasta=0) {
		$maxResultado = 14;
		
		$paginas = floor($numRegistro / $maxResultado);
		$paginacao = '';
		$proximo = $maxResultado + $pagina;
		$anterior = $pagina - $maxResultado;
		$ultimo = $numRegistro-1;
		$paginaAtual = '';
		
		
		
		
		if($numRegistro > $maxResultado) {
			
		if($pagina == $numRegistro-1) {
			$bloqUltimo = 'disabled';
		}
		
		if($pagina == 0) {
			$bloqPrim = 'disabled';
		}
		
		$pasta = base64_encode($pasta);
		
		return '<small><nav>
				  <ul class="pager">
					
					<li class="'.$bloqPrim.'"><a href="'.appConf::caminho.'relatorio/abrir/'.$pasta.'">|< Primeiro</a></li>
    				<li class="'.$bloqPrim.'"><a href="'.appConf::caminho.'relatorio/abrir/'.$pasta.'/'.$anterior.'">< Anterior</a></li>
					<li>'.floor(($pagina / $maxResultado)+1).' de '.($paginas+1).'</li>
					<li class="'.$bloqUltimo.'"><a href="'.appConf::caminho.'relatorio/abrir/'.$pasta.'/'.$proximo.'">Próximo ></a></li>
    				<li class="'.$bloqUltimo.'"><a href="'.appConf::caminho.'relatorio/abrir/'.$pasta.'/'.$ultimo.'">Último >|</a></li>
					
				  </ul>
				</nav></small>';	
		}	
	}
	
	public function barraArquivo($idArquivo='') {
		
		$barraBotoes[0] = '<a href="'.appConf::caminho.'relatorio/download/'.base64_encode($idArquivo).'" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-download-alt"></span></a> ';
		
		$barraBotoes[1] = '<a class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalEditarArquivo" data-id="'.base64_encode($idArquivo).'"><span class="glyphicon glyphicon-pencil"></span></a> ';
		
		$barraBotoes[2] = '<div class="btn-group" role="group" aria-label="...">
								<input type="checkbox" class="chk" id="chkArquivo" name="chkArquivo[]" value="'.base64_encode($idArquivo).'">
							</div>';
		
		
		if($this->funcao->dadoSessao('perfil') == 0) {
			return '<div class="bdtn-group" role="group" aria-label="...">'.
						$barraBotoes[0].
						$barraBotoes[1].
						$barraBotoes[2]
					.'</div>';
		} else {
		return '<div class="btn-grdoup" role="group" aria-label="...">'.
						$barraBotoes[0]
					.'</div>';
		}
	}
	
	public function barraPesquisa($idRaiz) {
		if($this->funcao->dadoSessao('perfil') == 0) {
			
			$this->diretorio->setIdPasta($idRaiz);
			$this->diretorio->selecionar();
			
			
			return ' <form method="post" action="'.appConf::caminho.'relatorio/pesquisar"><div class="input-group">
					  <input type="text" name="txtCriterio" class="form-control input-sm" placeholder="Procurar em '.$this->diretorio->getPasta().'">
					  <input type="hidden" name="txtIdPasta" value="'.$idRaiz.'" />
					  <span class="input-group-btn">
						<button class="btn btn-warning btn-sm" id="btnPesquisar" type="submit"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
					  </span>
					</div></form>';
		}
				
	}
	
	public function barraAdmin($idRaiz) {
		if($this->funcao->dadoSessao('perfil') == 0) {
			return '
			
			
					
					
					
				<div class="pull-right">
					
  <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modalEditarPasta" data-id="'.base64_encode(0).'" data-raiz="'.base64_encode($idRaiz).'"><span class="glyphicon glyphicon-plus"></span> Nova Pasta</button>
                      <button id="btnUpload" type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#modsalUpload"><span class="glyphicon glyphicon-arrow-up"></span> Upload</button>
                    
                      <div class="btn-group" role="group">
                        <button type="button" class="btn btn-warning btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                          Arquivos
                          <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                          <li><button id="btnSelecionarTudo"><span class="glyphicon glyphicon-check"></span> Selecionar Tudo</button></li>
                          <li><button id="btnRemoverSelecao"><span class="glyphicon glyphicon-unchecked"></span> Remover Seleção</button></li>
                          <li class="divider"></li>
                          <li><button id="btnMoverSelecao"><span class="glyphicon glyphicon-transfer"></span> Mover Selecionados</button></li>
                          <li><button id="btnExcluirSelecao"><span class="glyphicon glyphicon-trash"></span> Excluir Selecionados</button></li>
                          <li class="divider"></li>
                          <li><button id="btnMoverConteudo"><span class="glyphicon glyphicon-ok"></span> Mover Tudo</button></li>
                          <li><button id="btnExcluirConteudo"><span class="glyphicon glyphicon-minus"></span> Excluir Tudo</button></li>
                        </ul>
                      </div>

					</div>
					
					<form id="formUploadFile" action="'.appConf::caminho.'relatorio/upload" method="post" enctype="multipart/form-data">
						<input type="hidden" name="txtIdPasta" value="'.base64_encode($idRaiz).'" />
					  	<input type="file" id="fileArquivo" name="fileArquivo[]" multiple="multiple" />
					</form>';
		}
	}
	
	public function barraDiretorio($idDiretorio='', $idRaiz='') {
		
		$barraBotoes[0] = '<a href="#" data-toggle="modal" data-id="'.base64_encode($idDiretorio).'" data-raiz="'.base64_encode($idRaiz).'" data-target="#modalEditarPasta" class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-pencil"></span></a> ';
		$barraBotoes[1] = '<a href="#" data-toggle="modal"  data-id="'.$idDiretorio.'" data-target="#modalExcluirPasta" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-remove"></span></a> ';
		$barraBotoes[2] = '<a href="#" data-toggle="modal" data-id="'.base64_encode($idDiretorio).'" data-target="#modalPermissaoPasta" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-user"></span></a> ';
		
		if($this->funcao->dadoSessao('perfil') == 0) {
			return ''.
					$barraBotoes[0].
					$barraBotoes[1].
					$barraBotoes[2]
					.'';
		}
	}
	
	public function upload() {
		$arquivos = $_FILES['fileArquivo'];
		$idPasta = base64_decode($_POST['txtIdPasta']);	
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		$max = count($arquivos['name']);
		
		$this->arquivo->setIdPasta($idPasta);
		for($i=0;$i<=$max;$i++) {
			
			$extensao = explode(".", $arquivos['name'][$i]);
			
			$arquivoFinal = appConf::folder_report.utf8_decode($this->diretorio->getDiretorio().'\\'.$arquivos['name'][$i]);
			if(move_uploaded_file($arquivos['tmp_name'][$i], $arquivoFinal)) {
				
				$this->arquivo->setExtensao($extensao[count($extensao)-1]);
				$this->arquivo->setArquivo($arquivos['name'][$i]);	
				$this->arquivo->setTamanho($arquivo['size'][$i]);
				$this->arquivo->setTipo($arquivo['type'][$i]);
				$this->arquivo->salvar();
			}
		}
		
		header('Location: '.appConf::caminho.'relatorio/abrir/'.$_POST['txtIdPasta']);
			
	}
	
	public function abrir($idPasta, $pagina=0) {
		$idPasta = base64_decode($idPasta);
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		session_start();
		$_SESSION['DIRETORIO_UPLOAD'] = appConf::folder_report.$this->diretorio->getDiretorio().'\\';
		$_SESSION['DIRETORIO_ID'] = $idPasta;
		
		$this->set('nomeDiretorio', $this->diretorio->getPasta());
		$this->set('caminhoDiretorio', $this->diretorio->getNavegacao());
		
		$this->set('diretorio', $this->menuRaiz($this->diretorio->selecionarDiretorioRaiz($idPasta)));
		$this->set('idPasta', $this->abrirDiretorio($idPasta, $pagina));
		$this->set('barraAdmin', $this->barraAdmin($idPasta));
		$this->set('barraPesquisa', $this->barraPesquisa($idPasta));
		
		
		$this->render('relatorioView');	

	}
	
	public function abrir2() {
		
		$idPasta = base64_decode($_POST['idDiretorio']);
		if($idPasta != "") {
		
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		session_start();
		$_SESSION['DIRETORIO_UPLOAD'] = appConf::folder_report.$this->diretorio->getDiretorio().'\\';
		$_SESSION['DIRETORIO_ID'] = $idPasta;
		
		$nomeDiretorio =  $this->diretorio->getPasta();
		$caminhoDiretorio =  $this->diretorio->getNavegacao();
		
		$diretorio =  $this->menuRaiz($this->diretorio->selecionarDiretorioRaiz($idPasta));
		$idPastaa =  $this->abrirDiretorio($idPasta, $pagina);
		$barraAdmin =  $this->barraAdmin($idPasta);
		$barraPesquisa =  $this->barraPesquisa($idPasta);
		
		
		
		$html = '<div class="nome-diretorio">
					<div class="row">
					
						
						<div class="col-lg-8">
							<div>
								<div style="font-size:20px;"><img style="padding-right:5px;" src="'.appConf::caminho.'public/img/closedfolder.png" class="pull-left" /><strong>'.$nomeDiretorio.'</strong></div>
								<div><small>'.$caminhoDiretorio.'</small></div>
							</div>
						</div>
						
						<div class="col-lg-4">
						
							
						
						</div>
		
					</div>
					
					</div>
				   
				   
				   
					<div class="row">
						<div class="col-lg-12">';
						
							if ($idPastaa != '') {
								
					$html .= '			
							
							<div class="row" >
								<div class="col-lg-5" style="border-right:1px solid #ddd"><small><strong>Nome</strong></small></div>
								<div class="col-lg-2" style="border-right:1px solid #ddd"><small><strong>Data</strong></small></div>
								<div class="col-lg-1" style="border-right:1px solid #ddd"><small><strong>Tipo</strong></small></div>
								<div class="col-lg-2" style="border-right:1px solid #ddd"><small><strong>Tamanho</strong></small></div>
								<div class="col-lg-2" style="border-right:1px solid #ddd"><div><small><strong>Ação</strong></small></div></div>
							</div>
						
							<br />
						
							<div id="conteudo-diretorio">
							'.$idPastaa.'
							'.$paginacao.'
							</div>';
							
							} else {
								$html .= '<p class="text-center">Não existe arquivos nesta pasta.</p>';
							}
				$html .='			
						</div>
					</div>';
		
		
		echo $html;
		
		}
		//$this->render('relatorioView');	

	}
	
	public function barraProgresso() {
		echo '
			<div class="row">
				<div class="col-lg-4"></div>
				<div class="col-lg-4">
					<div style="display: table;width: 100%;height:340px;">
					<div style="display: table-cell;vertical-align: middle;text-align:center;">
					
					<div class="panel panel-default">
					  <div class="panel-body">
						<small><strong>Listando arquivos...</strong></small>
					<div class="progress">
					  <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
						<span class="sr-only">45% Complete</span>
					  </div>
					</div>
					  </div>
					</div>
					
					
					
					</div>
					</div>
				</div>
				<div class="col-lg-4"></div>
			</div>';
	}
	
	public function download($idArquivo) {
		$idArquivo = base64_decode($idArquivo);
		
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
		
		$arquivo = $this->arquivo->getArquivo();
		$diretorio = $this->arquivo->getDiretorio();		
		
		$this->funcao->download($diretorio, $arquivo);	
	}
	
	public function selecionarDiretorio($idDiretorio, $idRaiz) {
		$idDiretorio = base64_decode($idDiretorio);
		$idRaiz = base64_decode($idRaiz);
		
		if($idDiretorio != 0) {
			$this->diretorio->setIdPasta($idDiretorio);
			$this->diretorio->selecionar();
			
			$idRaiz = $this->diretorio->getIdDiretorio();
			$nomeDiretorio = $this->diretorio->getPasta();
		}
		
		
		
		
		$dados = array(
						'idDiretorio' => $idDiretorio,
						'idRaiz' => $idRaiz,
						'pasta' => $nomeDiretorio
					);
		
		echo json_encode($dados);
	}
	

	
	public function salvarPermissao() {
		$permissoes = $_POST['permissao'];
		$idPasta = $_POST['idPasta'];

		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->setPermissao($permissoes);
		$this->diretorio->salvarPermissao();
	}
	
	public function visualizarPermissao() {
		$permissoes = $_POST['permissao'];
		
		$this->diretorio->setPermissao($permissoes);
		$rs = $this->diretorio->listarPreviewPermissao();
		$td = '';
		for($i=1;$i<=count($rs);$i++) {
			$td .= '<tr>
						<td><small>'.$rs[$i]['USUARIO'].'</small></td>
						<td><small>'.$rs[$i]['PERFIL'].'</small></td>
						<td><small>'.$rs[$i]['LINHA'].'</small></td>
					</tr>';
		}
		
		echo '<table class="table table-striped"><tr><td>USUARIO</td><td>PERFIL</td><td>LINHA</td></tr>'.$td.'</table>';
		
	}
	
	public function permissaoDiretorio($idDiretorio) {
		$idDiretorio = base64_decode($idDiretorio);
		
		$this->diretorio->setIdPasta($idDiretorio);
		
		$html = '<div class="row">
					<div class="col-lg-12">
						<div>
						
							<div class="col-esq" style="width:45%;float:left">
								<div class="form-group">
									<label for="exampleInputEmail1">Pesquisar</label>
									<input type="text" class="form-control text-uppercase" id="txtPesquisarPerfil" placeholder="">
				  				</div>
								
								<div class="lista-permissao">
								'
									.$this->diretorio->listarPermissao().
								'</div>
							</div>
							
							<div class="col-mid" style="width:10%;height:425px;border:0px solid #ddd;float:left">
								<div style="position:relative; margin: -20px -20px; width:100px;  top:50%; left:50%;">
									<button  type="button" class="btn btn-default btn-md" id="btnAdicionarPerfil"><span class="glyphicon glyphicon-chevron-right"></span></button>
									<div style="height:10px"></div>
									<button type="button" class="btn btn-default btn-md" id="btnRemoverPerfil"><span class="glyphicon glyphicon-chevron-left"></span></button>
								</div>
							</div>
							
							<div class="col-dir" style="width:45%;border:0px solid #ddd;float:left">
							
								<button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#modalVisualizarPermissao"><span class="glyphicon glyphicon-search"></span> Visualizar Permitidos</button>
								<div class="lista-permitido">
									<input type="hidden" id="idPastaPermissao" value="'.$idDiretorio.'" />
									'.$this->diretorio->listarPermissaoDiretorio().'
								</div>
							</div>
							
						</div>
					</div>
				</div>';
		
		echo $html;
		
	}
	
	public function salvarDiretorio() {
		$idDiretorio = $_POST['txtIdDiretorio'];
		$idRaiz = $_POST['txtIdPai'];
		$nomeDiretorio = $_POST['txtNomeDiretorio'];
		
		$this->diretorio->setIdPasta($idDiretorio);
		$this->diretorio->setIdDiretorio($idRaiz);
		$this->diretorio->setPasta($nomeDiretorio);
		$r = $this->diretorio->salvar();
		
		echo $r;

		
	}
	
	public function excluirDiretorio() {
		$idDiretorio = $_POST['txtIdDiretorio'];	
		$this->diretorio->setIdPasta($idDiretorio);
		$this->diretorio->excluir();
		
	}
	
	public function selecionarArquivo($idArquivo) {
		$idArquivo = base64_decode($idArquivo);
		
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
			
		$idArquivo = $this->arquivo->getIdArquivo();
		$nomeArquivo = $this->arquivo->getArquivo();

		$dados = array(
						'idArquivo' => $idArquivo,
						'arquivo' => $nomeArquivo
					);
		
		echo json_encode($dados);
	}
	
	public function salvarArquivo2() {
		$nome = $_POST['nome'];	
		$tamanho = $_POST['tamanho'];
		$tipo = $_POST['tipo'];
		$idPasta = $_POST['idPasta'];
		
		$tmp = explode('.', $nome);
		$extensao = $tmp[count($tmp)-1];
		
		$this->arquivo->setArquivo($nome);
		$this->arquivo->setTamanho($tamanho);
		$this->arquivo->setTipo($tipo);
		$this->arquivo->setIdPasta($idPasta);
		$this->arquivo->setExtensao($extensao);
		echo $this->arquivo->salvar();
	}
	
	public function salvarArquivo() {
		$idArquivo = $_POST['txtIdArquivo'];
		$nomeArquivo = $_POST['txtNomeArquivo'];
		
		$this->arquivo->setIdArquivo($idArquivo);
		$this->arquivo->selecionar();
		
		$this->arquivo->setArquivo($nomeArquivo);
		
		
		$r = $this->arquivo->salvar();
		
		echo $r;

		
	}
	
	public function excluirArquivo($array) {
		for($i=0;$i<count($array);$i++) {
			$idArquivo = base64_decode($array[$i]);
			
			$this->arquivo->setIdArquivo($idArquivo);
			$this->arquivo->excluir();
		}
	}
	
	public function moverArquivo($array, $idPastaMover) {
		$this->diretorio->setIdPasta($idPastaMover);
		$this->diretorio->selecionar();
		
		$caminhoDestino = utf8_decode(appConf::folder_report.$this->diretorio->getDiretorio()).'\\';
		
		for($i=0;$i<count($array);$i++) {
			$idArquivo = base64_decode($array[$i]);
			
			$this->arquivo->setIdArquivo($idArquivo);
			$this->arquivo->selecionar();
						
			$origem = utf8_decode(appConf::folder_report.$this->arquivo->getCaminhoArquivo());
			$destino = $caminhoDestino.utf8_decode($this->arquivo->getArquivo());
			
			if(file_exists($caminhoDestino)) {
				if(file_exists($origem)) {
					@copy($origem, $destino);
					@unlink($origem);
					
					$this->arquivo->setArquivo($this->arquivo->getArquivo()); //gambi pra transformar o arquivo em utf8
					$this->arquivo->setIdPasta($idPastaMover);
					$this->arquivo->salvar();
				}
				
				
			}
		}
		
		
	}
	
	public function moverTudo($idPastaDE, $idPastaPARA) {
		$dirDe = new diretorioModel();
		$dirPara = new diretorioModel();
		
		$dirDe->setIdPasta($idPastaDE);
		$dirDe->selecionar();
		
		$dirPara->setIdPasta($idPastaPARA);
		$dirPara->selecionar();

		$origem = appConf::folder_report.$dirDe->getDiretorio().'\\';
		
		foreach(glob($origem ."*.*") as $file) {
			
			$destino = appConf::folder_report.$dirPara->getDiretorio().'\\'.basename($file);
			copy($file, $destino);
			if(file_exists($file)) {
				@unlink($file);
			}
			//echo 'Mover '.$file.' para '.$destino;
		}
		
		$this->arquivo->alterarPasta($idPastaDE, $idPastaPARA);
		
		
	}
	
	public function selecionarPastaMover($idPasta) {
		$this->diretorio->setIdPasta($idPasta);
		$this->diretorio->selecionar();
		
		$rs = $this->diretorio->selecionarSubDiretorio();
		$pasta = '<div class="row">
					<div class="col-lg-10"><h4><strong>'.$this->diretorio->getPasta().'</strong></h4></div>
					<div class="col-lg-2"><a value="'.$this->diretorio->getIdDiretorio().'" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-arrow-up"></span></a></div>
				</div>';
		for($i=1;$i<=count($rs);$i++) {
			$pasta .= '<div class="row">
							<div class="col-lg-7">
								<a value="'.$rs[$i]['ID_PASTA'].'"><p><small><b><img src="'.appConf::caminho.'public/img/foldermini.png"> '.$rs[$i]['PASTA'].'</b></small></p></a>
							</div>
							
							<div class="col-lg-3">asas</div>
						</div>';
		}
		
		$arr = array('diretorio' => 'Mover para: <strong>'.$this->diretorio->getCaminho().'\\</strong>',
					'sub' => $pasta
					);
		
		echo json_encode($arr);
	}
	
	public function raizPastaMover() {
		
		$rs = $this->relatorio->listarDiretorio();
		$pasta = '';
		for($i=1;$i<=count($rs);$i++) {
			$pasta .= '<a class="listarSubDiretorio" value="'.$rs[$i]['ID_PASTA'].'"><p><small><b><img src="'.appConf::caminho.'public/img/foldermini.png"> '.$rs[$i]['PASTA'].'</b></small></p></a>';
		}
		//return $pasta;
		
		$mover = '
			
			
			
			<div class="row">
				<div class="col-lg-5">
					<div style="overflow-x:auto;height:300px">'.$pasta.'</div>
					
				</div>
				
				<div class="col-lg-7">
					<div id="divSubDiretorio" style="overflow-x:auto;height:300px"></div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-lg-12">
					<div id="local" style="margin-top:20px;border:1px solid #ddd;padding:5px"></div>
				</div>
			</div>
		';
		
		echo $mover;
	}
	
	
	
	public function manipularArquivo() {
		$acao = $_POST['acao'];	
		$idPasta = $_POST['idPastaAtual'];
		$idPastaMover = $_POST['idPastaMover'];
		$check = $_POST['chkArquivo'];
		
		switch($acao) {
			
			case 'excluir': {
				$this->excluirArquivo($check);		
				$this->abrir($idPasta, 0);		
				break;
			}
			
			case 'excluirTudo': {
				
				$this->diretorio->setIdPasta(base64_decode($idPasta));
				$this->diretorio->excluirConteudoDiretorio(); 	
					
				$this->abrir($idPasta, 0);		
				break;
			}
			
			case 'mover': {
				echo $this->moverArquivo($check, $idPastaMover);
				$this->abrir($idPasta, 0);		
				break;	
			}
			
			case 'moverTudo': {
				$this->moverTudo(base64_decode($idPasta), $idPastaMover);
				$this->abrir($idPasta, 0);		
				break;	
			}
				
		}
		
	}

	public function main() {
		
		$this->set('diretorio', $this->menuRaiz());
		
		$this->render('relatorioView');	
	}

}

?>