<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class arquivoModel extends appConexao {
	
	
	private $idArquivo;
	private $idPasta;
	private $diretorio;
	private $caminho;
	private $arquivo;
	private $extensao;
	private $tamanho;
	private $tipo;
	private $dataInclusao;
	private $caminhoCompleto;

	
	
	
	function arquivoModel() {
		$this->idArquivo = 0;
		$this->idPasta = 0;
		$this->diretorio = '';
		$this->caminho = '';
		$this->arquivo = '';
		$this->extensao = '';
		
		$this->tamanho = '';
		$this->tipo = '';
		$this->dataInclusao = '';
		$this->caminhoCompleto = '';
	}
	
	
	public function setIdArquivo($dado) {
		$this->idArquivo = $dado;	
	}
	
	public function setIdPasta($dado) {
		$this->idPasta = $dado;	
	}
	
	public function setArquivo($dado) {
		$this->arquivo = utf8_decode($dado);	
		
		if(strpos($this->arquivo, '.'.$this->extensao) == 0) {
			$this->arquivo = $this->arquivo.'.'.$this->extensao;
		}
	}
	
	public function setTamanho($dado) {
		$this->tamanho = $dado;	
	}
	
	public function setTipo($dado) {
		$this->tipo = $dado;	
	}
	
	public function setExtensao($dado) {
		$this->extensao = $dado;	
	}
	
	public function setDataInclusao($dado) {
		$this->dataInclusao = $dado;
	}
	
	
	
	
	
	public function getIdArquivo() {
		return $this->idArquivo;	
	}
	
	public function getIdPasta() {
		return $this->idPasta;	
	}
	
	public function getDiretorio() {
		return $this->diretorio;
	}
	
	public function getCaminho() {
		return $this->caminho;	
	}
	
	public function getArquivo() {
		return $this->arquivo;	
	}
	
	public function getTamanho() {
		return $this->tamanho;	
	}
	
	public function getTipo() {
		return $this->tipo;	
	}
	
	public function getExtensao() {
		return $this->extensao;	
	}
	
	public function getCaminhoArquivo() {
		return $this->diretorio.'\\'.$this->arquivo;	
	}
	
	public function getDataInclusao() {
		return $this->dataInclusao;	
	}
	
	
	function selecionar() {
		
		if($this->idArquivo != 0) {
		
			$rsArquivo = $this->executarQueryArray("SELECT * FROM ARQUIVO WHERE id_arquivo = ".$this->idArquivo);
			$rsDiretorio = $this->executarQueryArray("SELECT DIRETORIO, CAMINHO FROM VW_PASTA WHERE ID_PASTA = ".$rsArquivo[1]['ID_PASTA']."");
			
			$this->idArquivo = $rsArquivo[1]['ID_ARQUIVO'];
			$this->idPasta = $rsArquivo[1]['ID_PASTA'];
			$this->arquivo = $rsArquivo[1]['ARQUIVO'];
			$this->extensao = $rsArquivo[1]['EXTENSAO'];
			$this->tamanho = $rsArquivo[1]['TAMANHO'];
			$this->tipo = $rsArquivo[1]['TIPO'];
			$this->dataInclusao = $rsArquivo[1]['DATA_INCLUSAO'];
			
			$this->diretorio = $rsDiretorio[1]['DIRETORIO'];
			$this->caminho = $rsDiretorio[1]['CAMINHO'];
		
		}
		
	}
	
	function listarArquivosDiretorio() {
		$arrArquivo = array();
		$rs = $this->executarQueryArray("EXEC proc_listarArquivosSetor ".$this->idPasta.", ".appFunction::dadoSessao('id_setor')."");	
		
		for($i=1;$i<=count($rs);$i++) {
			$arq = new arquivoModel();

			$arq->setIdArquivo($rs[$i]['ID_ARQUIVO']);
			//$arq->setArquivo($rs[$i]['ARQUIVO']);
			//$arq->setTipo($rs[$i]['TIPO']);
			///$arq->setTamanho($rs[$i]['TAMANHO']);
			//$arq->setDataInclusao($rs[$i]['DATA_INCLUSAO']);
			//$arq->setExtensao($rs[$i]['EXTENSAO']);

			$arq->selecionar();
			$arrArquivo[] = $arq;
		}
		
		return $arrArquivo;
	}
	
	function salvar() {
		
		$arquivoAntigo = new arquivoModel();
		$arquivoAntigo->setIdArquivo($this->idArquivo);
		$arquivoAntigo->selecionar();	
			
		$rs = $this->executarQueryArray("EXEC proc_salvarArquivo 
										  '".$this->idArquivo."', 
										  '".$this->idPasta."', 
										  '".$this->arquivo."', 
										  '".$this->extensao."', 
										  '".$this->tamanho."',
										  '".$this->tipo."'");
		if($rs[1]['MSG'] == '') {
			
			if($this->idArquivo != 0) {
						
				if(utf8_decode($arquivoAntigo->getArquivo()) != $this->arquivo) {
		
					$old = utf8_decode(appConf::folder_report.$arquivoAntigo->getDiretorio()).'\\'.utf8_decode($arquivoAntigo->getArquivo());
					$new = utf8_decode(appConf::folder_report.$this->diretorio).'\\'.$this->arquivo;
					rename($old, $new);
					
				}
				
				//return $old.$new;
				
			
			}
			
		} else {
			return $rs[1]['MSG'];
		}
										  
										
	}
	
	function excluir() {
		$this->selecionar();
		$arquivo = utf8_decode(appConf::folder_report.$this->diretorio.'\\'.$this->arquivo);
		
		if(file_exists($arquivo)) {
			if(@unlink($arquivo)) {
				$this->executar("EXEC proc_excluirArquivo '".$this->idArquivo."'");
			}
		}
		
		$this->executar("EXEC proc_excluirArquivo '".$this->idArquivo."'");
		
	}
	
	function alterarPasta($de, $para) {
		$this->executar("UPDATE arquivo SET id_pasta = ".$para." WHERE id_pasta = ".$de);
	}
	
	
	function mover($idPastaDestino) {

		if($destino != $origem) {
			copy($origem, $destino);
			unlink($origem);
		}
		
	}
	
}