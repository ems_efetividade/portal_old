<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class artigoModel extends appConexao {
	
	private $idArtigo;
	private $data;
	private $titulo;
	private $escritor;
	private $texto;
	private $imgThumb;
	private $img;
	private $imgWidth;
	private $imgHeight;
	private $resumo;
	private $visualizacao;
	private $url;	
	
	private $funcao;
	
	function artigoModel() {
		$this->idArtigo = '';
		$this->data = '';
		$this->titulo = '';
		$this->escritor = '';
		$this->texto = '';
		$this->imgThumb = '';
		$this->img = '';
		$this->resumo = '';
		
		$this->funcao = new appFunction();
	}
	
	public function setIdArtigo($dado) {
		$this->idArtigo = $dado;	
	}
	
	public function getIdArtigo(){
		return $this->idArtigo;	
	}
	
	
	public function setData($dado) {
		$this->data = $dado;	
	}
	
	public function getData(){
		return $this->data;	
	}
	
	
	public function setTitulo($dado) {
		$this->titulo = $dado;	
	}
	
	public function getTitulo(){
		return $this->titulo;	
	}
	
	
	public function setEscritor($dado) {
		$this->escritor = $dado;	
	}
	
	public function getEscritor(){
		return $this->escritor;	
	}
	
	
	public function setTexto($dado) {
		$this->texto = $dado;	
	}
	
	public function getTexto(){
		return $this->texto;	
	}
	
	
	public function setImgThumb($dado) {
		$this->imgThumb = $dado;	
	}
	
	public function getImgThumb(){
		return $this->imgThumb;	
	}
	
	
	public function setImg($dado) {
		$this->img = $dado;	
	}
	
	public function getImg(){
		return $this->img;	
	}
	
	
	public function setImgWidth($dado) {
		$this->imgWidth = $dado;	
	}
	
	public function getImgWidth(){
		return $this->imgWidth;	
	}
	
	
	public function setImgHeight($dado) {
		$this->imgHeight = $dado;	
	}
	
	public function getImgHeight(){
		return $this->imgHeight;	
	}
	
	
	public function setResumo($dado) {
		$this->resumo = $dado;	
	}
	
	public function getResumo(){
		return $this->resumo;	
	}
	
	
	public function getURL(){
		return $this->url;	
	}
	
	public function getVisualizacao(){
		return $this->visualizacao;	
	}
		
	public function selecionar() {
			
		$rs = $this->executarQueryArray("SELECT * FROM [ARTIGO] WHERE id_artigo = ".$this->idArtigo."");	
		
		if(count($rs) > 0) {
			$this->idArtigo = $rs[1]['ID_ARTIGO'];
			$this->data = $rs[1]['DATA'];
			$this->titulo = $rs[1]['TITULO'];
			$this->escritor = $rs[1]['ESCRITOR'];
			$this->texto = $rs[1]['TEXTO'];
			$this->imgThumb = $rs[1]['IMG_THUMB'];
			$this->img = $rs[1]['IMG'];
			$this->imgWidth = $rs[1]['IMG_WIDTH'];
			$this->imgHeight = $rs[1]['IMG_HEIGHT'];
			$this->resumo = $rs[1]['RESUMO'];
			$this->visualizacao = $rs[1]['VISUALIZACOES'];
			$this->url = $this->funcao->criarUrlArtigo($this->idArtigo, $this->titulo);
		}
	}
	
	public function selecionarUltimaNoticia() {
		$rs = $this->executarQueryArray("SELECT TOP 1 * FROM [ARTIGO] ORDER BY DATA DESC");	
		
		if(count($rs) > 0) {
			$this->idArtigo = $rs[1]['ID_ARTIGO'];
			$this->data = $rs[1]['DATA'];
			$this->titulo = $rs[1]['TITULO'];
			$this->escritor = $rs[1]['ESCRITOR'];
			$this->texto = $rs[1]['TEXTO'];
			$this->imgThumb = $rs[1]['IMG_THUMB'];
			$this->img = $rs[1]['IMG'];
			$this->imgWidth = $rs[1]['IMG_WIDTH'];
			$this->imgHeight = $rs[1]['IMG_HEIGHT'];
			$this->resumo = $rs[1]['RESUMO'];	
			$this->visualizacao = $rs[1]['VISUALIZACOES'];
			$this->url = $this->funcao->criarUrlArtigo($this->idArtigo, $this->titulo);
		}
	}
	
	public function listar($max=0) {
		if($max == 0) {
			return $this->executarQueryArray("SELECT * FROM [ARTIGO] ORDER BY DATA DESC");	
		} else {
			return $this->executarQueryArray("SELECT TOP ".$max." * FROM [ARTIGO] ORDER BY DATA DESC");	
		}
	}
	
	public function listarPaginacao($maxPorPagina=5, $pagina=1) {
		
		return $this->executarQueryArray("SELECT TOP (".$maxPorPagina.") *
								FROM    ( SELECT    ROW_NUMBER() OVER ( ORDER BY DATA ) AS RowNum, *
										  FROM  ARTIGO
										) AS RowConstrainedResult
								WHERE   RowNum > ((".$pagina." - 1) * ".$maxPorPagina.")
								ORDER BY RowNum	");
			
	}
	
	public function listarMaisLidos($max=0) {
		if($max == 0) {
			return $this->executarQueryArray("SELECT * FROM [ARTIGO] ORDER BY VISUALIZACOES DESC");	
		} else {
			return $this->executarQueryArray("SELECT TOP ".$max." * FROM [ARTIGO] ORDER BY VISUALIZACOES DESC");	
		}
	}
	
	public function listarFotos() {
		return $this->executarQueryArray("SELECT * FROM ARTIGO_ALBUM WHERE ID_ARTIGO = ".$this->idArtigo."");
	}
	
	public function somarVisualizacao() {
		$this->executar("UPDATE [ARTIGO] SET VISUALIZACOES = VISUALIZACOES+1 WHERE ID_ARTIGO = ".$this->idArtigo."");	
	}
	
}