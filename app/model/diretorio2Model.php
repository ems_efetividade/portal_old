<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');
require_once('arquivoModel.php');

class diretorio2Model extends appConexao {

    private $idPasta;
    private $idDiretorio;
    private $pasta;
    private $subDiretorio;
    private $permissao;
    private $dataCriacao;
    private $diretorio;

    public function diretorio2Model() {
        $this->idPasta = 0;
        $this->idDiretorio = 0;
        $this->pasta = '';
        $this->permissao = '';
    }

    public function setIdPasta($valor) {
        $this->idPasta = $valor;
    }

    public function setIdDiretorio($valor) {
        $this->idDiretorio = $valor;
    }

    public function setPasta($valor) {
        $this->pasta = $valor;
    }

    public function setPermissao($valor) {
        $this->permissao = $valor;
    }

    public function getIdPasta() {
        return $this->idPasta;
    }

    public function getIdDiretorio() {
        return $this->idDiretorio;
    }

    public function getPasta() {
        return $this->pasta;
    }

    public function getDataCriacao() {
        return $this->dataCriacao;
    }

    public function getDiretorio() {
        return $this->diretorio;
    }

    public function getTamanho() {
        return $this->tamanhoPasta;
    }

    public function salvar() {
        $rs = $this->executarQueryArray("EXEC proc_salvarPasta '" . $this->idPasta . "', '" . $this->idDiretorio . "', '" . $this->pasta . "', '" . appFunction::dadoSessao('id_setor') . "'");
        $retorno = $rs[1]['MSG'];

        if ($this->idPasta == 0) {


            if ($retorno == "") {
                $idDiretorioCriado = $rs[1]['ID'];

                $novaPasta = new diretorio2Model();
                $novaPasta->setIdPasta($idDiretorioCriado);
                $novaPasta->selecionar();

                try {

                    mkdir(appConf::folder_report . utf8_decode($novaPasta->getDiretorio()), 0777);
                } catch (Exception $e) {
                    echo 'exceção: ', $e->getMessage(), "\n";
                }
            } else {
                return $retorno;
            }
        } else {
            return $retorno;
        }

        return $retorno;
    }

    public function salvarPastaRaiz() {

        if ($this->idPasta != 0) {
            $antigo = new diretorio2Model();
            $antigo->setIdPasta($this->idPasta);
            $antigo->selecionar();
        }

        $rs = $this->executarQueryArray("EXEC proc_salvarPasta '" . $this->idPasta . "', '" . $this->idDiretorio . "', '" . $this->pasta . "', '" . appFunction::dadoSessao('id_setor') . "'");
        $retorno = $rs[1]['MSG'];

        if ($this->idPasta == 0) {
            if ($retorno == "") {
                mkdir(appConf::folder_report . $this->pasta);
            }
        } else {
            rename(utf8_decode(appConf::folder_report . $antigo->getPasta()), appConf::folder_report . $this->pasta);
        }
    }

    private function excluiDir($Dir) {
        if (is_dir($Dir)) {
            if ($dd = opendir($Dir)) {
                while (false !== ($Arq = readdir($dd))) {
                    if ($Arq != "." && $Arq != "..") {
                        $Path = "$Dir\\$Arq";
                        if (is_dir($Path)) {
                            $this->excluiDir($Path);
                        } elseif (is_file($Path)) {
                            unlink($Path);
                        }
                    }
                }
                closedir($dd);
            }
            rmdir($Dir);
        }
    }

    public function excluir() {
        $this->selecionar();
        $this->executar("EXEC proc_excluirPasta '" . $this->idPasta . "'");

        $pastaExcluir = appConf::folder_report . utf8_decode($this->diretorio);

        if (file_exists($pastaExcluir) && utf8_decode($this->diretorio) != "") {
            $this->excluiDir($pastaExcluir);
            //echo 'excluir: '.$pastaExcluir;
        }
        //appConf::folder_report.utf8_decode($this->diretorio);
    }

    public function excluirConteudoDiretorio() {

        $this->selecionar();
        $this->executar("DELETE FROM arquivo WHERE id_pasta = " . $this->idPasta);

        $path = appConf::folder_report . utf8_decode($this->diretorio);

        $directory = dirname($path);

        if (is_writable($directory)) {
            foreach (glob($path . "*.*") as $file) {
                unlink($file); // Delete each file through the loop
            }
        } //   else {
        //     echo 'Acesso de leitura negado a esse diretório.';
        //}
    }

    private function popularVariaveis($rs, $i = 1) {
        if (count($rs) > 0) {
            $this->idPasta = $rs[$i]['ID_PASTA'];
            $this->idDiretorio = $rs[$i]['ID_DIRETORIO'];
            $this->pasta = $rs[$i]['PASTA'];
            $this->diretorio = $rs[$i]['DIRETORIO'];
            $this->caminho = $rs[$i]['CAMINHO'];
            $this->dataCriacao = $rs[$i]['DATA_CRIACAO'];
            $this->tamanhoPasta = '-';
        }
    }

    public function salvarPermissao() {
        $this->executar("EXEC proc_excluirPermissao '" . $this->idPasta . "'");

        if ($this->permissao) {
            foreach ($this->permissao as $dado) {
                $this->executar("EXEC proc_salvarPermissao '" . $this->idPasta . "', '" . $dado['id_perfil'] . "', '" . $dado['id_setor'] . "', '" . $dado['id_linha'] . "'");
            }
        }
    }

    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM vw_pasta WHERE id_pasta = " . $this->idPasta);
        $this->popularVariaveis($rs);
    }

    public function listarSubDiretorio() {
        $arrSubDiretorio = array();
        $rs = $this->executarQueryArray("EXEC proc_listarDiretorioSetor " . appFunction::dadoSessao('id_setor') . ", " . $this->idPasta . "");

        for ($i = 1; $i <= count($rs); $i++) {

            $subDir = new diretorio2Model();
            $subDir->popularVariaveis($rs, $i);

            $arrSubDiretorio[] = $subDir;
        }

        return $arrSubDiretorio;
    }

    public function visualizarPermissao() {

        if (count($this->permissao) > 0) {
            $array = $this->permissao;

            $query_perfil = "";
            $query_setor = "";
            $query_linha_perfil = "";

            foreach ($array as $dado) {

                if ($dado['id_linha'] == 0 && $dado['id_setor'] == 0) {
                    $query_perfil .= "(ID_PERFIL = " . $dado['id_perfil'] . ") OR ";
                } else {
                    if ($dado['id_linha'] != 0 && $dado['id_perfil'] != 0) {
                        $query_linha_perfil .= "(ID_PERFIL = " . $dado['id_perfil'] . " AND ID_LINHA = " . $dado['id_linha'] . ") OR ";
                    } else {
                        $query_setor .= "(ID_SETOR = " . $dado['id_setor'] . ") OR ";
                    }
                }
            }

            $query_final = $query_linha_perfil . $query_perfil . $query_setor;
            $query_final = substr($query_final, 0, strlen($query_final) - 4);




            return $this->executarQueryArray("select SETOR + ' ' + NOME AS 'USUARIO', LINHA, PERFIL from vw_permissao WHERE " . $query_final . " GROUP BY SETOR, NOME, LINHA, PERFIL ORDER BY SETOR ASC");
        }

        /* $permissoes = $this->permissao;
          $arrPerfil = array();
          $arrLinha = array();
          $arrSetor = array();




          return $this->executarQueryArray("select SETOR + ' ' + NOME as USUARIO, LINHA, PERFIL from vw_permissao
          where ID_SETOR in (".implode(',', $arrSetor).") or (ID_PERFIL in (".implode(',', $arrPerfil).") and ID_LINHA in (".implode(',', $arrLinha).")) OR (ID_PERFIL in (".implode(',', $arrPerfil)."))
          GROUP BY SETOR, NOME, LINHA, PERFIL
          ORDER BY SETOR ASC"); */
    }

    public function listarMenuPadrao() {
        $arrDirPadrao = array();
        $rs = $this->executarQueryArray("EXEC proc_listarDiretorioPadrao '" . appFunction::dadoSessao('id_setor') . "'");

        for ($i = 1; $i <= count($rs); $i++) {

            $dirPadrao = new diretorio2Model();
            $dirPadrao->popularVariaveis($rs, $i);

            $arrDirPadrao[] = $dirPadrao;
        }

        return $arrDirPadrao;
    }

    function pesquisar($criterio) {
        $arrArquivo = array();
        $rs = $this->executarQueryArray("EXEC proc_pesquisarAdmin " . $this->idPasta . ", '" . $criterio . "'");

        for ($i = 1; $i <= count($rs); $i++) {
            $arquivo = new arquivoModel();

            $arquivo->setIdArquivo($rs[$i]['ID_ARQUIVO']);
            $arquivo->setIdPasta($rs[$i]['ID_PASTA']);
            $arquivo->setArquivo($rs[$i]['ARQUIVO']);
            $arquivo->setExtensao($rs[$i]['EXTENSAO']);
            $arquivo->setDataInclusao($rs[$i]['DATA_INCLUSAO']);
            $arquivo->setTamanho($rs[$i]['TAMANHO']);

            $arrArquivo[] = $arquivo;
        }

        return $arrArquivo;
    }

    function listarConteudoDiretorio($pagina = 0, $criterio = '') {
        $maxResultado = 14;
        return $this->executarQueryArray("EXEC proc_listarConteudoDiretorio '" . $this->idPasta . "', '" . appFunction::dadoSessao('id_setor') . "', '" . $maxResultado . "', '" . $pagina . "', '" . $criterio . "'");
    }

    public function listarPermissaoDiretorio() {
        $rs = $this->executarQueryArray("EXEC proc_listarPermissao '" . $this->idPasta . "'");
        $users = "";
        $nome = '';
        $valor = 0;

        return $this->formatarCheckbox($rs);
    }

    public function listarPermissao() {
        $users = "";
        $rs = $this->executarQueryArray("select * from vw_permissaoperfil ORDER BY ID_SETOR ASC, ID_LINHA ASC, ID_PERFIL");

        return $this->formatarCheckbox($rs);
    }

    public function listarPastaRaiz() {
        $arrRaiz = array();

        $rs = $this->executarQueryArray("SELECT * FROM vw_tamanhoPastaRaiz ORDER BY ID_PASTA ASC");

        for ($i = 0; $i <= count($rs); $i++) {
            if ($rs[$i]['ID_PASTA'] != "") {
                $raiz = new diretorio2Model();
                $raiz->setIdPasta($rs[$i]['ID_PASTA']);
                $raiz->selecionar();
                $raiz->tamanhoPasta = $rs[$i]['TAMANHO'];

                $arrRaiz[] = $raiz;
            }
        }

        return $arrRaiz;
    }

    private function formatarCheckbox($rs) {

        for ($i = 1; $i <= count($rs); $i++) {

            $users .= '<div class="checkbox" style="margin:0px;">
						<label>
						  <input type="checkbox" class="check-usuario" usuario="' . $rs[$i]['PERFIL'] . '" id_perfil="' . $rs[$i]['ID_PERFIL'] . '" id_linha="' . $rs[$i]['ID_LINHA'] . '" id_setor="' . $rs[$i]['ID_SETOR'] . '"><h5 style="margin:5px"><small> ' . $rs[$i]['PERFIL'] . '</small></h5>
						</label>
					  </div>';
        }

        return $users;
    }

    private function tamanhoPasta() {
        $rs = $this->executarQueryArray("
										with grupo as (
											select id_pasta from vw_pasta where ID_PASTA = " . $this->idPasta . "
											
											union all
											
											select p.id_pasta from vw_pasta p inner join grupo g on g.ID_PASTA = p.id_DIRETORIO
										)
										
										
										select SUM(vw_arquivo.TAMANHO) as TAMANHO from vw_arquivo 
										inner join grupo on grupo.ID_PASTA = vw_arquivo.ID_PASTA
										inner join vw_pastaSetor on vw_pastaSetor.ID_PASTA = grupo.ID_PASTA
										where vw_pastaSetor.ID_SETOR = " . appFunction::dadoSessao('id_setor') . "");
        return $rs[1]['TAMANHO'];
    }

}
