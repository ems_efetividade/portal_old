<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

require_once('arquivoModel.php');

class diretorioModel extends appConexao {

	private $idPasta;
	private $idDiretorio;
	private $pasta;
	private $diretorio;
	private $caminho;
	private $dataCriacao;
	private $tamanhoPasta;
	private $permissao;
	private $arquivo;
	
	function diretorioModel() {
		$this->idPasta = 0;
		$this->idDiretorio = 0;	
		$this->pasta = '';
	}
	
	public function setIdPasta($dado) {
		$this->idPasta = $dado;	
	}

	public function setIdDiretorio($dado) {
		$this->idDiretorio = $dado;	
	}
	
	public function setPasta($dado) {
		$this->pasta = $dado;	
	}
	
	public function setPermissao($dado) {
		$this->permissao = $dado;	
	}	
	
	
	
	
	public function getIdPasta() {
		return $this->idPasta;	
	}
	
	public function getIdDiretorio() {
		return $this->idDiretorio;	
	}
	
	public function getPasta() {
		return $this->pasta;
	}
	
	public function getDiretorio() {
		return $this->diretorio;	
	}
	
	public function getCaminho() {
		return $this->caminho;	
	}
	
	public function getDataCriacao() {
		return $this->dataCriacao;	
	}
	
	public function getTamanho() {
		return $this->tamanhoPasta;	
	}
	
	public function getNavegacao() {
		$arrPasta = explode('\\', $this->caminho);
		$arrId = explode('\\', $this->diretorio);
		$pastaRaiz = $this->selecionarDiretorioRaiz($this->idPasta);
		
		$barraNav = '';
		
		for($i=0;$i<=count($arrPasta)-1;$i++) {
			
			if($arrId[$i] == $arrPasta[$i]) {
				$arrId[$i] = $pastaRaiz;
			}
			
			$barraNav .= '<li><a href="'.appConf::caminho.'relatorio/abrir/'.base64_encode($arrId[$i]).'">'.$arrPasta[$i].'</a></li>';
		}
		
		return '<ol class="breadcrumb">'.$barraNav.'</ol>';

		
	}
	private function excluiDir($Dir){
		if( is_dir($Dir) ){
			if ($dd = opendir($Dir)) {
				while (false !== ($Arq = readdir($dd))) {
					if($Arq != "." && $Arq != ".."){
						$Path = "$Dir\\$Arq";
						if(is_dir($Path)){
							$this->excluiDir($Path);
						}elseif(is_file($Path)){
							unlink($Path);
						}
					}
				}
				closedir($dd);
			}
			rmdir($Dir);
		}
	}
	
	
	public function listar() {
		$arrPasta = array();
		$rs = $this->executarQueryArray("exec proc_listarDiretorio ".appFunction::dadoSessao('id_setor')."");	
		
		for($i=1;$i<=count($rs);$i++) {
			$diretorio = new diretorioModel();
			$diretorio->setIdPasta($rs[$i]['ID_PASTA']);	
			$diretorio->selecionar();
			
			$arrPasta[] = $diretorio;
		}
		
		return $arrPasta;
	}
	
	public function salvar() {
		
		$rs = $this->executarQueryArray("EXEC proc_salvarPasta 
                                                                        '".$this->idPasta."', 
                                                                        '".$this->idDiretorio."', 
                                                                        '".$this->pasta."', 
                                                                        '".appFunction::dadoSessao('id_setor')."'");	
		$retorno =  $rs[1]['MSG'];
		
		if($this->idPasta == 0) {


			if($retorno == "") {
				$idDiretorioCriado = $rs[1]['ID'];
				
				$novaPasta = new diretorioModel();
				$novaPasta->setIdPasta($idDiretorioCriado);
				$novaPasta->selecionar();
				
				try {
					
					mkdir(appConf::folder_report.utf8_decode($novaPasta->getDiretorio()), 0777);
				} catch (Exception $e) {
					echo 'exceção: ', $e->getMessage(), "\n";
				}
				
			} else {
				return $retorno;	
			}
		
		} else {
			return $retorno;	
		}
		
		return $retorno;	
	}
	
	public function excluir() {
		$this->selecionar();
		$this->executar("EXEC proc_excluirPasta '".$this->idPasta."'");
		$this->excluiDir(appConf::folder_report.utf8_decode($this->diretorio));
		//return appConf::folder_report.utf8_decode($this->diretorio);
	}
	
	public function excluirConteudoDiretorio() {
		
		$this->selecionar();
		$this->executar("DELETE FROM arquivo WHERE id_pasta = ".$this->idPasta);
		
		$path = appConf::folder_report.$this->diretorio.'\\';

		foreach(glob($path ."*.*") as $file) {
			unlink($file); // Delete each file through the loop
		}

		
	}
	
	private function popularVariaveis($rs) {
		if(count($rs) > 0) {
			$this->idPasta = $rs[1]['ID_PASTA'];
			$this->idDiretorio = $rs[1]['ID_DIRETORIO'];
			$this->pasta = $rs[1]['PASTA'];
			$this->diretorio = $rs[1]['DIRETORIO'];
			$this->caminho = $rs[1]['CAMINHO'];
			$this->dataCriacao = $rs[1]['DATA_CRIACAO'];
			$this->tamanhoPasta = '-';
		}
	}
	
	
	private function tamanhoPasta() {
		$rs = $this->executarQueryArray("
										with grupo as (
											select id_pasta from vw_pasta where ID_PASTA = ".$this->idPasta."
											
											union all
											
											select p.id_pasta from vw_pasta p inner join grupo g on g.ID_PASTA = p.id_DIRETORIO
										)
										
										
										select SUM(vw_arquivo.TAMANHO) as TAMANHO from vw_arquivo 
										inner join grupo on grupo.ID_PASTA = vw_arquivo.ID_PASTA
										inner join vw_pastaSetor on vw_pastaSetor.ID_PASTA = grupo.ID_PASTA
										where vw_pastaSetor.ID_SETOR = ".appFunction::dadoSessao('id_setor')."");
		return $rs[1]['TAMANHO'];
	}
	
	function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM vw_pasta WHERE id_pasta = ".$this->idPasta);
		$this->popularVariaveis($rs);
	}
	
	function selecionarSubDiretorio() {
		return 	$this->executarQueryArray("SELECT * FROM vw_pasta WHERE id_diretorio = ".$this->idPasta);
	}
	
	function contarSubDiretorio() {
		$rs = $this->executarQueryArray("SELECT count(ID_PASTA) as C FROM vw_pasta WHERE id_diretorio = ".$this->idPasta);
		
		return $rs[1]['C'];
	}
	
	function selecionarDiretorioRaiz($idPasta) {
		$rs = $this->executarQueryArray("with grupo as (
											select id_pasta, ID_DIRETORIO from vw_pasta where ID_pasta = ".$idPasta."
											union all
											select p.id_pasta, p.ID_DIRETORIO from vw_pasta p inner join grupo on grupo.ID_DIRETORIO = p.ID_PASTA
											)
										select ID_PASTA from grupo where ID_DIRETORIO = 0");
		return $rs[1]['ID_PASTA'];
			
	}
	
	public function salvarPermissao() {
		$this->executar("EXEC proc_excluirPermissao '".$this->idPasta."'");
			
		foreach($this->permissao as $dado) {
			$this->executar("EXEC proc_salvarPermissao '".$this->idPasta."', '".$dado['id_perfil']."', '".$dado['id_setor']."', '".$dado['id_linha']."'");
		}
	}
	
	public function listarPreviewPermissao() {
		
		
		$array = $this->permissao;
		
		$query_perfil = "";
		$query_setor = "";
		$query_linha_perfil = "";
		
		foreach($array as $dado) {
			
			if($dado['id_linha'] == 0 && $dado['id_setor'] == 0) {
				$query_perfil .= "(ID_PERFIL = ".$dado['id_perfil'].") OR ";
			} else {
				if($dado['id_linha'] != 0 && $dado['id_perfil'] != 0) {
					$query_linha_perfil .= "(ID_PERFIL = ".$dado['id_perfil']." AND ID_LINHA = ".$dado['id_linha'].") OR ";
				} else {
					$query_setor .= "(ID_SETOR = ".$dado['id_setor'].") OR ";
				}
			}
			
			
		}
		
		$query_final = $query_linha_perfil.$query_perfil.$query_setor;
		$query_final = substr($query_final, 0, strlen($query_final)-4);
		
		
		
		
		return $this->executarQueryArray("select SETOR + ' ' + NOME AS 'USUARIO', LINHA, PERFIL from vw_permissao WHERE ".$query_final." GROUP BY SETOR, NOME, LINHA, PERFIL ORDER BY SETOR ASC");
		
		
		
		
		/*$permissoes = $this->permissao;
		$arrPerfil = array();
		$arrLinha = array();
		$arrSetor = array();
		
		
		
		
		return $this->executarQueryArray("select SETOR + ' ' + NOME as USUARIO, LINHA, PERFIL from vw_permissao 
											where ID_SETOR in (".implode(',', $arrSetor).") or (ID_PERFIL in (".implode(',', $arrPerfil).") and ID_LINHA in (".implode(',', $arrLinha).")) OR (ID_PERFIL in (".implode(',', $arrPerfil)."))
											GROUP BY SETOR, NOME, LINHA, PERFIL
											ORDER BY SETOR ASC");*/
	}
	
	public function listarPermissaoDiretorio() {
		$rs = $this->executarQueryArray("EXEC proc_listarPermissao '".$this->idPasta."'");
		$users = "";
		$nome = '';
		$valor = 0;
		
		
		
		for($i=1;$i<=count($rs);$i++) {
			
						
			$users .= '<div class="checkbox">
						<label >
						  <input type="checkbox" class="check-usuario" usuario="'.$rs[$i]['PERFIL'].'" id_perfil="'.$rs[$i]['ID_PERFIL'].'" id_linha="'.$rs[$i]['ID_LINHA'].'" id_setor="'.$rs[$i]['ID_SETOR'].'"> '.$rs[$i]['PERFIL'].'
						</label>
					</div>';
			
			
			/*$users .= '<div class=""><div class="usuario">
							<input type="checkbox" class="check-usuario" usuario="'.$rs[$i]['PERFIL'].'" id_perfil="'.$rs[$i]['ID_PERFIL'].'" id_linha="'.$rs[$i]['ID_LINHA'].'" id_setor="'.$rs[$i]['ID_SETOR'].'">
							'.$rs[$i]['PERFIL'].'
						</div></div>';*/
		}
		
		return $users;
	}
	
	public function listarPermissao() {
		$users = "";
		$rs = $this->executarQueryArray("select * from vw_permissaoperfil ORDER BY ID_SETOR ASC, ID_LINHA ASC, ID_PERFIL");
		
		
		for($i=1;$i<=count($rs);$i++) {
			
			$users .= '<div class="checkbox">
						<label>
						  <input type="checkbox" class="check-usuario" usuario="'.$rs[$i]['PERFIL'].'" id_perfil="'.$rs[$i]['ID_PERFIL'].'" id_linha="'.$rs[$i]['ID_LINHA'].'" id_setor="'.$rs[$i]['ID_SETOR'].'"> '.$rs[$i]['PERFIL'].'
						</label>
					  </div>';
			
			/*
			$users .= '<div class="usuario" criterio="'.$rs[$i]['PERFIL'].'">
							<input type="checkbox" class="check-usuario" usuario="'.$rs[$i]['PERFIL'].'" id_perfil="'.$rs[$i]['ID_PERFIL'].'" id_linha="'.$rs[$i]['ID_LINHA'].'" id_setor="'.$rs[$i]['ID_SETOR'].'">
							'.$rs[$i]['PERFIL'].'
						</div>';*/
		}
		//}

		return $users;
	}
	
	public function listarPastaRaiz() {
		$arrRaiz = array();
		
		$rs = $this->executarQueryArray("SELECT * FROM vw_tamanhoPastaRaiz ORDER BY ID_PASTA ASC");
		
		for($i=0;$i<=count($rs);$i++) {
			if($rs[$i]['ID_PASTA'] != "") {
				$raiz = new diretorioModel();
				$raiz->setIdPasta($rs[$i]['ID_PASTA']);
				$raiz->selecionar();
				$raiz->tamanhoPasta = $rs[$i]['TAMANHO'];
				
				$arrRaiz[] = $raiz;
			}
		}
		
		return $arrRaiz;
	}
	
	public function salvarPastaRaiz() {
		
		if($this->idPasta != 0) {
			$antigo = new diretorioModel();
			$antigo->setIdPasta($this->idPasta);
			$antigo->selecionar();
		}
		
		$rs = $this->executarQueryArray("EXEC proc_salvarPasta '".$this->idPasta."', '".$this->idDiretorio."', '".$this->pasta."', '".appFunction::dadoSessao('id_setor')."'");	
		$retorno =  $rs[1]['MSG'];
		
		if($this->idPasta == 0) {
			if($retorno == "") {
				mkdir(appConf::folder_report.$this->pasta);
			}
		} else {
			rename(utf8_decode(appConf::folder_report.$antigo->getPasta()), appConf::folder_report.$this->pasta);
		}
	}
	
	public function excluirPastaRaiz() {
		$this->selecionar();
		$this->executar("EXEC proc_excluirPasta '".$this->idPasta."'");
		$this->excluiDir(appConf::folder_report.utf8_decode($this->diretorio).'\\');
		//return appConf::folder_report.utf8_decode($this->diretorio);
	}
	
}