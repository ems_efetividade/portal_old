<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class ferramentaModel extends appConexao {

	private $idFerramenta;
	private $nivel;
	private $tipoAcesso;
	private $ferramenta;
	private $caminho;
	private $descricao;
	private $target;
	private $empresa;
        private $ativo;
	
	private $subFerramenta;
	
	private $permissao;
        private $permissaoExcluir;
	
	public function ferramentaModel() {
                $this->criarColuna();
            
		$this->idFerramenta = 0;
		$this->nivel = 0;
		$this->tipoAcesso = 0;
		$this->ferramenta = '';
		$this->caminho = '';
		$this->descricao = '';
		$this->target = '';
		$this->empresa = '';
                $this->ativo = 1;
		
		$this->permissao = array();
                $this->permissaoExcluir = array();
		$this->subFerramenta = array();
	}
	
	public function setIdFerramenta($valor) {
		$this->idFerramenta = $valor;	
	}
	
	public function setNivel($valor) {
		$this->nivel= $valor;	
	}
	
	public function setTipoAcesso($valor) {
		$this->tipoAcesso = $valor;	
	}
	
	public function setFerramenta($valor) {
		$this->ferramenta = utf8_decode(trim($valor));	
	}
	
	public function setCaminho($valor) {
		$this->caminho = trim($valor);	
	}
	
	public function setDescricao($valor) {
		$this->descricao = $valor;	
	}
	
	public function setTarget($valor) {
		$this->target = $valor;	
	}
	
	public function setEmpresa($valor) {
		$this->empresa = $valor;	
	}
	
	public function setPermissao($valor) {
		$this->permissao[] = $valor;
	}
        
        public function setPermissaoExcluir($valor) {
            $this->permissaoExcluir[] = $valor;
        }
        
        public function setAtivo($valor) {
            $this->ativo = $valor;
        }
	
	
	public function getIdFerramenta() {
		return $this->idFerramenta;	
	}
	
	public function getNivel() {
		return $this->nivel;	
	}
	
	public function getTipoAcesso() {
		return $this->tipoAcesso;	
	}
	
	public function getFerramenta() {
		return $this->ferramenta;	
	}
	
	public function getCaminho() {
		return $this->caminho;	
	}
	
	public function getDescricao() {
		return $this->descricao;	
	}
	
	public function getTarget() {
		return $this->target;	
	}
	
	public function getEmpresa() {
		return $this->empresa;	
	}
	
	public function getPermissao() {
		return $this->permissao;	
	}
	
	public function getSubFerramenta() {
		return $this->subFerramenta;	
	}
	
        public function getAtivo() {
            return $this->ativo;
        }
        
        public function salvar() {
            
            $validar = $this->validar();
            
            if(count($validar) == 0) {
                
                if($this->idFerramenta == 0) {
                    $query = "INSERT INTO FERRAMENTA2 "
                            . " (NIVEL, TIPO_ACESSO, FERRAMENTA, CAMINHO, DESCRICAO, TARGET, EMPRESA, ATIVO) "
                            . "VALUES "
                            . "(".$this->nivel.", ".$this->tipoAcesso.", '".$this->ferramenta."', '".$this->caminho."', '".$this->descricao."', '".$this->target."', '".$this->empresa."', ".$this->ativo.");";
                } else {
                    $query = "UPDATE FERRAMENTA2 SET NIVEL = ".$this->nivel.", TIPO_ACESSO = ".$this->tipoAcesso.", "
                            . "FERRAMENTA = '".$this->ferramenta."', CAMINHO = '".$this->caminho."', DESCRICAO = '".$this->descricao."', TARGET = '".$this->target."', EMPRESA = '".$this->empresa."', ATIVO = ".$this->ativo." WHERE ID_FERRAMENTA = ".$this->idFerramenta."";
                }
                
                $this->executar($query);
                
            } else {
                return $validar;
            }
            
        }
        
        private function validar() {
            $erro = [];
            
            if($this->ferramenta == '') {
                $erro[] = "Digite o nome da ferramenta!";
            }
            
            if($this->target == '') {
                $erro[] = "Selecione a forma de abertura da ferramenta!";
            }
            
            if($this->idFerramenta == 0) {
                $rs = $this->executarQueryArray("SELECT COUNT(*) AS C FROM FERRAMENTA2 WHERE FERRAMENTA = '".$this->ferramenta."' AND CAMINHO = '".$this->caminho."'");
                
                if($rs[1]['C'] > 0) {
                    $erro[] = 'Já existe uma ferramenta com esses dados!';
                }
            }
            
            
            return $erro;
        }
        
        public function excluir() {
            
            $query = "SELECT * FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ";
            
            $this->executar("DELETE FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);
            $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);
        }
	
	
	public function selecionar($log='') {
		if($this->idFerramenta != 0) {
                    $rs = $this->executarQueryArray("SELECT * FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);	

                    $this->idFerramenta = $rs[1]['ID_FERRAMENTA'];
                    $this->nivel        = $rs[1]['NIVEL'];
                    $this->tipoAcesso   = $rs[1]['TIPO_ACESSO'];
                    $this->ferramenta   = $rs[1]['FERRAMENTA'];
                    $this->caminho      = $rs[1]['CAMINHO'];
                    $this->descricao    = $rs[1]['DESCRICAO'];
                    $this->target       = $rs[1]['TARGET'];
                    $this->empresa      = $rs[1]['EMPRESA'];
                    $this->ativo        = $rs[1]['ATIVO'];

                    $this->gerarSubFerramenta();
                    $this->logFerramenta($log);
		}
	}	
	
	private function gerarSubFerramenta() {
		$this->subFerramenta = array();
/*		$query = "SELECT 
				F.ID_FERRAMENTA
			FROM 
				FERRAMENTA2 F
			INNER JOIN SETOR_FERRAMENTA2 S ON S.ID_FERRAMENTA = F.ID_FERRAMENTA
			INNER JOIN SETOR SS ON SS.ID_PERFIL = S.ID_PERFIL
			WHERE 
				F.NIVEL = ".$id." AND SS.SETOR = '".appFunction::dadoSessao('setor')."'";*/
		
		$query = "EXEC proc_listarFerramentaSetor ".$this->idFerramenta.", '".appFunction::dadoSessao('setor')."'";
		
		$rs = $this->executarQueryArray($query);
		

                
		for($i=1;$i<=count($rs);$i++) {
			
			$subferramenta = new ferramentaModel();
			
			$subferramenta->setIdFerramenta($rs[$i]['ID_FERRAMENTA']);
			$subferramenta->selecionar(1);
			
			$this->subFerramenta[] = $subferramenta;
		}
	}
	
	public function listar() {
		$arrFerramenta = array();
		
		$query = "EXEC proc_listarFerramentaSetor 0, '".appFunction::dadoSessao('setor')."'";

		$rs = $this->executarQueryArray($query);
		
                /*
                echo '<pre>';
                print_r($rs);
                echo '</pre>';
                */

                for($i=1;$i<=count($rs);$i++) {
			
			$ferramenta = new ferramentaModel();
			
			$ferramenta->setIdFerramenta($rs[$i]['ID_FERRAMENTA']);
			$ferramenta->selecionar(1);
			
			$arrFerramenta[] = $ferramenta;
		}
		
		return $arrFerramenta;
	}
	
	public function comboFerramenta() {
		
		$ferramentas = new ferramentaModel();
		
		$ferramenta = $ferramentas->listar();
		/*
                echo '<pre>';
		print_r($ferramenta);
                echo '</pre>';
                */
		$comboFerramenta = '<ul class="dropdown-menu">';
		foreach($ferramenta as $fer) {
			
			$link = appFunction::criarUrl(appConf::caminho.'ferramenta/abrir/'.$fer->getIdFerramenta().'/'.$fer->getFerramenta());
                        
                       // echo $link.'<br>';
                        
			$toggle='';	
			$comboSubFerramenta = "";
			$classeDropDown = "";
			$cadeado = '<span class="glyphicon">&nbsp;</span>';
			
			if(count($fer->getSubFerramenta()) > 0) {
				
				$comboSubFerramenta = '<ul class="dropdown-menu">';
				
				$subFerramentas = $fer->getSubFerramenta();
				$cadeadoSub = $cadeado;
				foreach($subFerramentas as $sub) {
					
					if($sub->getTipoAcesso() == 1) {
						$cadeadoSub ='<span class="glyphicon glyphicon-lock"></span>';
					}
					
					
					$comboSubFerramenta .= '<li>
                                                                    <a class="dropdown-toggle"  href="'.appFunction::criarUrl(appConf::caminho.'ferramenta/abrir/'.$sub->getIdFerramenta().'/'.$sub->getFerramenta()).'">'.$cadeadoSub.' '.$sub->getFerramenta().'</a>
					                       </li>';
				}
				$comboSubFerramenta .= '</ul>';
			}
			
			
			if($comboSubFerramenta != "") {
				$classeDropDown = 'dropdown dropdown-submenu';
				$link = "#";
				$toggle='data-toggle="dropdown"';
			}
			
			if($fer->getTipoAcesso() == 1) {
				$cadeado = '<span class="glyphicon glyphicon-lock"></span>';
			}
			
			$comboFerramenta .= '<li class="'.$classeDropDown.'">
                                            <a class="dropdown-toggle" '.$toggle.' href="'.$link.'">'.$cadeado.' '.$fer->getFerramenta().'</a>'.$comboSubFerramenta.'
                                         </li>';
			
		}
		
		
		$comboFerramenta .= '</ul>';
		
		return $comboFerramenta;
		
	}
	
	public function listarPermissoes() {

		$rs = $this->executarQueryArray("SELECT 
							PERFIL.ID_PERFIL, 
							PERFIL.PERFIL 
						FROM 
							PERFIL 
						INNER JOIN SETOR_FERRAMENTA2 ON SETOR_FERRAMENTA2.ID_PERFIL = PERFIL.ID_PERFIL
						WHERE 
							SETOR_FERRAMENTA2.ID_FERRAMENTA = ".$this->idFerramenta."");
		
		return $rs;
	}
	
	public function salvarPermissao() {
		//$query = '';
		//$queryPai = '';
                
               // $f = new ferramentaModel();
               // $f->setIdFerramenta($this->idFerramenta);
                //$f->selecionar(1);
                
            
                for($i=0;$i<count($this->permissaoExcluir);$i++) {
                    $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta." AND ID_PERFIL = ".$this->permissaoExcluir[$i]."");
                    $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = (SELECT NIVEL FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta.") AND ID_PERFIL = ".$this->permissaoExcluir[$i]."");
                }
            
		for($i=0;$i<count($this->permissao);$i++) {
                    $query = "EXEC proc_salvarPermissaoFerramenta '".$this->idFerramenta."', '".$this->permissao[$i]."'";
                    echo $this->executar($query);
                }
                
 
            /*
                $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);
                for($i=0;$i<count($this->permissao);$i++) {
                    //$query = "EXEC proc_salvarPermissaoFerramenta '".$this->idFerramenta."', '".$this->permissao[$i]."'";
                    $query =  "INSERT INTO SETOR_FERRAMENTA2 (ID_FERRAMENTA, ID_PERFIL) VALUES (".$this->idFerramenta.", ".$this->permissao[$i].") ";
                    $query .= "IF (SELECT COUNT(NIVEL) FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta." AND NIVEL <> 0) > 0 BEGIN DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = (SELECT NIVEL FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta.") AND ID_PERFIL = ".$this->permissao[$i]."  INSERT INTO SETOR_FERRAMENTA2 (ID_FERRAMENTA, ID_PERFIL) VALUES ((SELECT NIVEL FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta."), ".$this->permissao[$i].")  END ";
                    echo $this->executar($query);
                    //echo $query;
                }
                */
                   //// $query .= 'INSERT INTO [SETOR_FERRAMENTA2] (ID_FERRAMENTA, ID_PERFIL) VALUES ('.$this->idFerramenta.', '.$this->permissao[$i].'); ';
                    
                    
                //    if($f->getNivel() != 0) {
                //        $queryPai .= 'INSERT INTO [SETOR_FERRAMENTA2] (ID_FERRAMENTA, ID_PERFIL) VALUES ('.$f->getNivel().', '.$this->permissao[$i].'); ';
                //    }    
		
                
                

		
		////$this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);
		////$this->executar($query);
                
               // if($queryPai != '') {
               //     $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$f->getNivel());
               //     $this->executar($queryPai);
               // }
	}

	public function logFerramenta($log='') {
		
		if($log == '') {
			$this->executar("INSERT INTO LOG_FERRAMENTA 
								([ID_FERRAMENTA],[ID_SETOR],[ID_COLABORADOR],[DATA]) 
							VALUES 
								(".$this->idFerramenta.", ".appFunction::dadoSessao('id_setor').", ".appFunction::dadoSessao('id_colaborador').", '".date('Y-m-d H:i:s')."')");
		}
	}
        
        
        
        private function criarColuna() {
            $this->executar("IF NOT EXISTS(
                                SELECT TOP 1 1
                                FROM INFORMATION_SCHEMA.COLUMNS
                                WHERE 
                                  [TABLE_NAME] = 'FERRAMENTA2'
                                  AND [COLUMN_NAME] = 'ATIVO')
                              BEGIN
                                EXEC ('ALTER TABLE [FERRAMENTA2] ADD [ATIVO] INT')
                                EXEC ('UPDATE FERRAMENTA2 SET ATIVO = 1')

                                EXEC ('
                                ALTER PROCEDURE [dbo].[proc_listarFerramentaSetor]
                                            @idFerramenta INT = 0,
                                            @setor varchar(100)
                                    AS

                                    DECLARE @PERFIL INT = 0

                                    SET @PERFIL = (SELECT PERFIL.ADMIN FROM PERFIL INNER JOIN SETOR ON SETOR.ID_PERFIL = PERFIL.ID_PERFIL WHERE SETOR.SETOR = @setor)

                                    IF (@PERFIL) <= 1
                                    BEGIN


                                            SELECT 
                                                    F.ID_FERRAMENTA
                                            FROM 
                                                    FERRAMENTA2 F
                                            INNER JOIN SETOR_FERRAMENTA2 S ON S.ID_FERRAMENTA = F.ID_FERRAMENTA
                                            INNER JOIN SETOR SS ON SS.ID_PERFIL = S.ID_PERFIL
                                            WHERE 
                                                    F.NIVEL = @idFerramenta AND SS.SETOR = @setor AND F.ATIVO = 1
                                            ORDER BY F.TIPO_ACESSO,  F.FERRAMENTA ASC
                                    END
                                    ELSE
                                    BEGIN

                                            IF @idFerramenta = 0
                                            BEGIN
                                                            SELECT 
                                                                    F.ID_FERRAMENTA
                                                            FROM 
                                                                    FERRAMENTA2 F
                                                            WHERE F.NIVEL = 0
                                                            ORDER BY F.TIPO_ACESSO,  F.FERRAMENTA ASC
                                            END
                                            ELSE
                                            BEGIN
                                            SELECT 
                                                                    F.ID_FERRAMENTA
                                                            FROM 
                                                                    FERRAMENTA2 F
                                                                    WHERE NIVEL = @idFerramenta
                                                                    ORDER BY F.TIPO_ACESSO,  F.FERRAMENTA ASC
                                            END

                                    END')


                              END");
        }

}