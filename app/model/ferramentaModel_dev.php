<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class ferramentaModel extends appConexao {

	private $idFerramenta;
	private $nivel;
	private $tipoAcesso;
	private $ferramenta;
	private $caminho;
	private $descricao;
	private $target;
	private $empresa;
	
	private $subFerramenta;
	
	private $permissao;
	
	public function ferramentaModel() {
		$this->idFerramenta = 0;
		$this->nivel = 0;
		$this->tipoAcesso = 0;
		$this->ferramenta = '';
		$this->caminho = '';
		$this->descricao = '';
		$this->target = '';
		$this->empresa = '';
		
		$this->permissao = array();
		$this->subFerramenta = array();
	}
	
	public function setIdFerramenta($valor) {
		$this->idFerramenta = $valor;	
	}
	
	public function setNivel($valor) {
		$this->nivel= $valor;	
	}
	
	public function setTipoAcesso($valor) {
		$this->tipoAcesso = $valor;	
	}
	
	public function setFerramenta($valor) {
		$this->ferramenta = $valor;	
	}
	
	public function setCaminho($valor) {
		$this->caminho = $valor;	
	}
	
	public function setDescricao($valor) {
		$this->descricao = $valor;	
	}
	
	public function setTarget($valor) {
		$this->target = $valor;	
	}
	
	public function setEmpresa($valor) {
		$this->empresa = $valor;	
	}
	
	public function setPermissao($valor) {
		$this->permissao[] = $valor;
	}
	
	
	public function getIdFerramenta() {
		return $this->idFerramenta;	
	}
	
	public function getNivel() {
		return $this->nivel;	
	}
	
	public function getTipoAcesso() {
		return $this->tipoAcesso;	
	}
	
	public function getFerramenta() {
		return $this->ferramenta;	
	}
	
	public function getCaminho() {
		return $this->caminho;	
	}
	
	public function getDescricao() {
		return $this->descricao;	
	}
	
	public function getTarget() {
		return $this->target;	
	}
	
	public function getEmpresa() {
		return $this->empresa;	
	}
	
	public function getPermissao() {
		return $this->permissao;	
	}
	
	public function getSubFerramenta() {
		return $this->subFerramenta;	
	}
	
	
	
	public function selecionar($log='') {
		if($this->idFerramenta != 0) {
                    $rs = $this->executarQueryArray("SELECT * FROM FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);	

                    $this->idFerramenta = $rs[1]['ID_FERRAMENTA'];
                    $this->nivel        = $rs[1]['NIVEL'];
                    $this->tipoAcesso   = $rs[1]['TIPO_ACESSO'];
                    $this->ferramenta   = $rs[1]['FERRAMENTA'];
                    $this->caminho      = $rs[1]['CAMINHO'];
                    $this->descricao    = $rs[1]['DESCRICAO'];
                    $this->target       = $rs[1]['TARGET'];
                    $this->empresa      = $rs[1]['EMPRESA'];

                    $this->gerarSubFerramenta();
                    $this->logFerramenta($log);
		}
	}	
	
	private function gerarSubFerramenta() {
		$this->subFerramenta = array();
/*		$query = "SELECT 
				F.ID_FERRAMENTA
			FROM 
				FERRAMENTA2 F
			INNER JOIN SETOR_FERRAMENTA2 S ON S.ID_FERRAMENTA = F.ID_FERRAMENTA
			INNER JOIN SETOR SS ON SS.ID_PERFIL = S.ID_PERFIL
			WHERE 
				F.NIVEL = ".$id." AND SS.SETOR = '".appFunction::dadoSessao('setor')."'";*/
		
		$query = "EXEC proc_listarFerramentaSetor ".$this->idFerramenta.", '".appFunction::dadoSessao('setor')."'";
		
		$rs = $this->executarQueryArray($query);
		
		for($i=1;$i<=count($rs);$i++) {
			
			$subferramenta = new ferramentaModel();
			
			$subferramenta->setIdFerramenta($rs[$i]['ID_FERRAMENTA']);
			$subferramenta->selecionar(1);
			
			$this->subFerramenta[] = $subferramenta;
		}
	}
	
	public function listar() {
		$arrFerramenta = array();
		
		$query = "EXEC proc_listarFerramentaSetor 0, '".appFunction::dadoSessao('setor')."'";
		
		$rs = $this->executarQueryArray($query);
		
		for($i=1;$i<=count($rs);$i++) {
			
			$ferramenta = new ferramentaModel();
			
			$ferramenta->setIdFerramenta($rs[$i]['ID_FERRAMENTA']);
			$ferramenta->selecionar(1);
			
			$arrFerramenta[] = $ferramenta;
		}
		
		return $arrFerramenta;
	}
	
	public function comboFerramenta() {
		
		$ferramentas = new ferramentaModel();
		
		$ferramenta = $ferramentas->listar();
		
		
		$comboFerramenta = '<ul class="dropdown-menu">';
		foreach($ferramenta as $fer) {
			
			$link = appFunction::criarUrl(appConf::caminho.'ferramenta/abrir/'.$fer->getIdFerramenta().'/'.$fer->getFerramenta());
			$toggle='';	
			$comboSubFerramenta = "";
			$classeDropDown = "";
			$cadeado = '<span class="glyphicon">&nbsp;</span>';
			
			if(count($fer->getSubFerramenta()) > 0) {
				
				$comboSubFerramenta = '<ul class="dropdown-menu">';
				
				$subFerramentas = $fer->getSubFerramenta();
				$cadeadoSub = $cadeado;
				foreach($subFerramentas as $sub) {
					
					if($sub->getTipoAcesso() == 1) {
						$cadeadoSub ='<span class="glyphicon glyphicon-lock"></span>';
					}
					
					
					$comboSubFerramenta .= '<li>
                                                                    <a class="dropdown-toggle"  href="'.appFunction::criarUrl(appConf::caminho.'ferramenta/abrir/'.$sub->getIdFerramenta().'/'.$sub->getFerramenta()).'">'.$cadeadoSub.' '.$sub->getFerramenta().'</a>
					                       </li>';
				}
				$comboSubFerramenta .= '</ul>';
			}
			
			
			if($comboSubFerramenta != "") {
				$classeDropDown = 'dropdown dropdown-submenu';
				$link = "#";
				$toggle='data-toggle="dropdown"';
			}
			
			if($fer->getTipoAcesso() == 1) {
				$cadeado = '<span class="glyphicon glyphicon-lock"></span>';
			}
			
			$comboFerramenta .= '<li class="'.$classeDropDown.'">
										<a class="dropdown-toggle" '.$toggle.' href="'.$link.'">'.$cadeado.' '.$fer->getFerramenta().'</a>'.$comboSubFerramenta.'
								 </li>';
			
		}
		
		
		$comboFerramenta .= '</ul>';
		
		return $comboFerramenta;
		
	}
	
	public function listarPermissoes() {

		$rs = $this->executarQueryArray("SELECT 
							PERFIL.ID_PERFIL, 
							PERFIL.PERFIL 
						FROM 
							PERFIL 
						INNER JOIN SETOR_FERRAMENTA2 ON SETOR_FERRAMENTA2.ID_PERFIL = PERFIL.ID_PERFIL
						WHERE 
							SETOR_FERRAMENTA2.ID_FERRAMENTA = ".$this->idFerramenta."");
		
		return $rs;
	}
	
	public function salvarPermissao() {
		//$query = '';
		//$queryPai = '';
                
               // $f = new ferramentaModel();
               // $f->setIdFerramenta($this->idFerramenta);
                //$f->selecionar(1);
                
		for($i=0;$i<count($this->permissao);$i++) {
                    $query = "EXEC proc_salvarPermissaoFerramenta '".$this->idFerramenta."', '".$this->permissao[$i]."'";
                    echo $this->executar($query);
                    
                   //// $query .= 'INSERT INTO [SETOR_FERRAMENTA2] (ID_FERRAMENTA, ID_PERFIL) VALUES ('.$this->idFerramenta.', '.$this->permissao[$i].'); ';
                    
                    
                //    if($f->getNivel() != 0) {
                //        $queryPai .= 'INSERT INTO [SETOR_FERRAMENTA2] (ID_FERRAMENTA, ID_PERFIL) VALUES ('.$f->getNivel().', '.$this->permissao[$i].'); ';
                //    }    
		}
                
                

		
		////$this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$this->idFerramenta);
		////$this->executar($query);
                
               // if($queryPai != '') {
               //     $this->executar("DELETE FROM SETOR_FERRAMENTA2 WHERE ID_FERRAMENTA = ".$f->getNivel());
               //     $this->executar($queryPai);
               // }
	}

	public function logFerramenta($log='') {
		
		if($log == '') {
			$this->executar("INSERT INTO LOG_FERRAMENTA 
								([ID_FERRAMENTA],[ID_SETOR],[ID_COLABORADOR],[DATA]) 
							VALUES 
								(".$this->idFerramenta.", ".appFunction::dadoSessao('id_setor').", ".appFunction::dadoSessao('id_colaborador').", '".date('Y-m-d H:i:s')."')");
		}
	}

}