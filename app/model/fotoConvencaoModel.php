<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class fotoConvencaoModel extends appConexao {
    
    private $idSetor;
    private $idColaborador;
    private $foto;
    private $tamanho;
    private $dataUpload;
    
    public function __construct() {
        $this->idSetor = 0;
        $this->idColaborador = 0;
        $this->foto = "";
        $this->tamanho = 0;
        $this->dataUpload = '';
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }

    public function setTamanho($valor) {
        $this->tamanho = $valor;
    }
	
    public function setFoto($valor) {
        $this->foto = $valor;
    }    
    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }

    public function getTamanho() {
        return $this->tamanho;
    }
	
    public function getFoto() {
        return $this->foto;
    }    
    
    public function getDataUpload() {
        return $this->dataUpload;
    }    
    
    public function salvar() {
        
        
        $rs = $this->executarQueryArray("SELECT * FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".$this->idSetor." AND ID_COLABORADOR = ".$this->idColaborador."");
        if($rs[1]['FOTO'] != "") {
            if(file_exists($caminho.$rs[1]['FOTO'])) {
                if(unlink($caminho.$rs[1]['FOTO'])) {;
                    $this->executar("DELETE FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".$this->idSetor." AND ID_COLABORADOR = ".$this->idColaborador."");   
                }
            }
        }
        
        $query = "INSERT INTO TRN_FOTO_CONVENCAO (ID_SETOR, ID_COLABORADOR, FOTO, TAMANHO, DATA_UPLOAD) VALUES (".$this->idSetor.", ".$this->idColaborador.", '".$this->foto."', ".$this->tamanho.", '".date('Y-m-d H:i:s')."');";
        $this->executar($query);
    }
    
    public function excluir() {
        $caminho = appConf::caminho_fisico.appConf::folder_profile_fisico.'convencao\\';
        $rs = $this->executarQueryArray("SELECT * FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".($this->idSetor+0)." AND ID_COLABORADOR = ".($this->idColaborador+0)."");
        
        if(file_exists($caminho.$rs[1]['FOTO'])) {
            if(unlink($caminho.$rs[1]['FOTO'])) {;
                $this->executar("DELETE FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".$this->idSetor." AND ID_COLABORADOR = ".$this->idColaborador."");   
            }
        }
        
        $this->executar("DELETE FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".$this->idSetor." AND ID_COLABORADOR = ".$this->idColaborador."");   
        
    }
    
    public function verificarFoto() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS TOTAL FROM TRN_FOTO_CONVENCAO WHERE ID_SETOR = ".($this->idSetor+0)." AND ID_COLABORADOR = ".($this->idColaborador+0)."");
        return $rs[1]['TOTAL'];
    }
    
    public function exportarLista() {
        return $this->executarQueryArray("select * from vwFotoConvencao order by data_upload desc"); 
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("SELECT * FROM TRN_FOTO_CONVENCAO ORDER BY DATA_UPLOAD DESC"); 
        $arr = array();
        
        foreach($rs as $row) {
            $f = new fotoConvencaoModel();
            $f->popular($row);
            $arr[] = $f;
        }
        
        return $arr;
    }
    
    protected function popular($row) {
        $this->idSetor       = $row['ID_SETOR'];
        $this->idColaborador = $row['ID_COLABORADOR'];
        $this->foto          = $row['FOTO'];
        $this->tamanho       = $row['TAMANHO'];
        $this->dataUpload    = $row['DATA_UPLOAD'];
    }
    
}
