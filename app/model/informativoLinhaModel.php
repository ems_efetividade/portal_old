<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class informativoLinhaModel extends appConexao {

	private $idLinha;
	private $idInformativo;
	
	public function informativoLinhaModel() {
		$this->idLinha = 0;
		$this->idInformativo = 0;	
	}

	public function setIdInformativo($valor) {
		$this->idInformativo = $valor;	
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;	
	}

	public function getIdInformativo() {
		return $this->idInformativo;	
	}
	
	public function getIdLinha() {
		return $this->idLinha;	
	}

	public function salvar() {
		$this->excluir();
		$this->executar("INSERT INTO INFORMATIVO_LINHA VALUES (".$this->idInformativo.", ".$this->idLinha.")");
	}
	
	public function excluir() {
		$this->executar("DELETE FROM INFORMATIVO_LINHA WHERE ID_INFORMATIVO = ".$this->idInformativo." AND ID_LINHA = ".$this->idLinha);	
	}
}