<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class informativoModel extends appConexao {
	
	private $idInformativo;
	private $titulo;
	private $texto;
	private $ativo;
	private $dataCadastro;
	private $dataFim;
	private $ordem;
	
	private $diasExpirar;
	
	private $linha;

	
	public function informativoModel() {
		$this->idInformativo = 0;
		$this->titulo = '';
		$this->texto = '';
		$this->ativo = 0;	
		$this->dataCadastro = '';
		$this->dataFim = '';
		$this->ordem = 1;
		
		$this->linha = array();
	}
	
	
	public function setIdInformativo($valor) {
		$this->idInformativo = $valor;
	}
	
	public function setTitulo($valor) {
		$this->titulo = $valor;
	}
	
	public function setTexto($valor) {
		$this->texto = $valor;
	}
	
	public function setAtivo($valor) {
		$this->ativo = $valor;
	}	
	
	public function setDataFim($valor) {
		$this->dataFim = $valor;	
	}
	
	public function setOrdem($valor) {
		$this->ordem = $valor;
	}
	
	public function setLinha($valor) {
		$this->linha[] = $valor;	
	}
	
	
	
	public function getIdInformativo() {
		return $this->idInformativo;
	}
	
	public function getTitulo() {
		return $this->titulo;
	}
	
	public function getTexto() {
		return $this->texto;
	}
	
	public function getAtivo() {
		return $this->ativo;
	}	
	
	public function getDataCadastro() {
		return $this->dataCadastro;
	}	
	
	public function getLinha() {
		return $this->linha;	
	}
	
	public function getDataFim() {
		return $this->dataFim;	
	}
	
	public function getOrdem() {
		return $this->ordem;
	}
	
	public function getDiasExpirar() {
		return $this->diasExpirar;
	}
	
	
	
	public function salvar() {
		
		if($this->idInformativo == 0) {
			
			$query = "INSERT INTO [INFORMATIVO]
						   ([TITULO]
						   ,[TEXTO]
						   ,[ATIVO]
						   ,[DATA_CADASTRO]
						   ,[DATA_FIM]
						   ,[ORDEM])
					 VALUES
						   ('".$this->titulo."'
						   ,'".$this->texto."'
						   ,".$this->ativo."
						   ,'".date('Y-m-d H:i:s')."'
						   ,'".$this->dataFim."'
						   ,'".$this->ordem."')";
			
			$this->executar($query);
			$rs = $this->executarQueryArray("SELECT MAX(ID_INFORMATIVO) AS ID_INFORMATIVO FROM INFORMATIVO");
			
			$idInformativo = $rs[1]['ID_INFORMATIVO'];
						   
		} else {
			
			$query = "UPDATE [INFORMATIVO]
					   SET [TITULO] = '".$this->titulo."'
						  ,[TEXTO] = '".$this->texto."'
						  ,[ATIVO] = ".$this->ativo."
						  ,[DATA_FIM] = '".$this->dataFim."'
						  ,[ORDEM] = ".$this->ordem."
					 WHERE [ID_INFORMATIVO] = ".$this->idInformativo."";	
			
			$this->executar($query);
			
			$idInformativo = $this->idInformativo;
					 
		}
		
		$this->salvarLinha($idInformativo);
		
	}
	
	private function salvarLinha($idInformativo) {
		$this->executar("DELETE FROM INFORMATIVO_LINHA WHERE ID_INFORMATIVO = ".$idInformativo);
		
		for($i=0;$i<count($this->linha);$i++) {
			$this->executar("INSERT INTO INFORMATIVO_LINHA (ID_INFORMATIVO, ID_LINHA) VALUES (".$idInformativo.", ".$this->linha[$i].")");	
		}
	}
	
	public function excluir() {
		$this->executar("DELETE FROM INFORMATIVO WHERE ID_INFORMATIVO = ".$this->idInformativo);	
		$this->executar("DELETE FROM INFORMATIVO_LINHA WHERE ID_INFORMATIVO = ".$this->idInformativo);
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM INFORMATIVO WHERE ID_INFORMATIVO = ".$this->idInformativo);
		$rsLinha = $this->executarQueryArray("SELECT ID_LINHA FROM INFORMATIVO_LINHA WHERE ID_INFORMATIVO = ".$this->idInformativo);
		
		$rsExpira = $this->executarQueryArray("select DATEDIFF(d, CONVERT(DATE, GETDATE()), data_fim) as expira from informativo where id_informativo = ".$this->idInformativo);
		
		$this->idInformativo = $rs[1]['ID_INFORMATIVO'];
		$this->titulo = $rs[1]['TITULO'];
		$this->texto = $rs[1]['TEXTO'];
		$this->ativo = $rs[1]['ATIVO'];
		$this->dataCadastro = $rs[1]['DATA_CADASTRO'];
		$this->dataFim = $rs[1]['DATA_FIM'];
		$this->ordem = $rs[1]['ORDEM'];
		
		if($rsExpira[1]['expira'] > 0) {
			$this->diasExpirar = $rsExpira[1]['expira'];
		} else {
			$this->diasExpirar = 'Expirado';
		}
		
		for($i=1;$i<=count($rsLinha);$i++) {
			$this->linha[] = $rsLinha[$i]['ID_LINHA'];	
		}
	}
	
	public function listar($ativo="") {
		$arrInformativo = array();
		$idLinha = appFunction::dadoSessao('id_linha');
		
		
			
		if($idLinha == 0) {
			if($ativo == "") {
				$query = "SELECT * FROM INFORMATIVO ORDER BY ORDEM ASC";
			} else {
				$query = "SELECT * FROM INFORMATIVO WHERE ATIVO = 1 ORDER BY ORDEM ASC";
			}
		} else {
			$query = "SELECT
						INFORMATIVO.*
					FROM 
						INFORMATIVO
					INNER JOIN 
						INFORMATIVO_LINHA ON INFORMATIVO_LINHA.ID_INFORMATIVO = INFORMATIVO.ID_INFORMATIVO
					WHERE 
						INFORMATIVO_LINHA.ID_LINHA = ".$idLinha." AND INFORMATIVO.ATIVO = 1 
						--AND CONVERT(DATE, GETDATE()) <= DATA_FIM
					ORDER BY 
						INFORMATIVO.ORDEM ASC";
		}
		
		$rs = $this->executarQueryArray($query);	
		
		for($i=1;$i<=count($rs);$i++) {
			$informativo = new 	informativoModel();
			$informativo->setIdInformativo($rs[$i]['ID_INFORMATIVO']);	
			$informativo->selecionar();
			
			$arrInformativo[] = $informativo;
		}
		
		return $arrInformativo;
	}


	public function listarHome($ativo="") {
		$arrInformativo = array();
		$idLinha = appFunction::dadoSessao('id_linha');
		
		
		
			$query = "SELECT
						INFORMATIVO.*
					FROM 
						INFORMATIVO
					INNER JOIN 
						INFORMATIVO_LINHA ON INFORMATIVO_LINHA.ID_INFORMATIVO = INFORMATIVO.ID_INFORMATIVO
					WHERE 
						INFORMATIVO_LINHA.ID_LINHA = ".$idLinha." AND INFORMATIVO.ATIVO = 1 
						AND CONVERT(DATE, GETDATE()) <= DATA_FIM
					ORDER BY 
						INFORMATIVO.ORDEM ASC";
		
		
		$rs = $this->executarQueryArray($query);	
		
		for($i=1;$i<=count($rs);$i++) {
			$informativo = new 	informativoModel();
			$informativo->setIdInformativo($rs[$i]['ID_INFORMATIVO']);	
			$informativo->selecionar();
			
			$arrInformativo[] = $informativo;
		}
		
		return $arrInformativo;
	}
	
}