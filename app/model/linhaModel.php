<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class linhaModel extends appConexao {

	private $idLinha;
	private $nome;
	private $sigla;
	private $codigo;
	private $idUnNegocio;
	
	public function linhaModel() {
		$this->idLinha = 0;
		$this->nome = '';
		$this->sigla = '';
		$this->codigo ='';
		$this->idUnNegocio = 0;
	}
	
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;
	}
	
	public function setNome($valor) {
		$this->nome = $valor;
	}
	
	public function setSigla($valor) {
		$this->sigla = $valor;
	}
	
	public function setCodigo($valor) {
		$this->codigo = $valor;
	}
	
	public function setIdUnNegocio($valor) {
		$this->idUnNegocio = $valor;
	}
	
	public function getIdLinha() {
		return $this->idLinha;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
	public function getSigla() {
		return $this->sigla;
	}
	
	public function getCodigo() {
		return $this->codigo;
	}

	public function getIdUnNegocio() {
		return $this->idUnNegocio;
	}
	
	
	
	public function salvar() {
		if($this->idLinha == 0) {
			
			$query = "INSERT INTO [LINHA]
						   ([NOME]
						   ,[SIGLA]
						   ,[CODIGO]
						   ,[ID_UN_NEGOCIO])
					 VALUES
						   ('".$this->nome."'
						   ,'".$this->sigla."'
						   ,'".$this->codigo."'
						   ,".$this->idUnNegocio.")";	
						   
		} else {
			
			$query = "UPDATE [LINHA]
					   SET [NOME] = '".$this->nome."'
						  ,[SIGLA] = '".$this->sigla."'
						  ,[CODIGO] = '".$this->codigo."'
						  ,[ID_UN_NEGOCIO] = '".$this->idUnNegocio."'
					 WHERE [ID_LINHA] = ".$this->idLinha."";	
					 
		}
		
		$this->executar($query);
	}
	
	public function excluir() {
		$this->executar("DELETE FROM LINHA WHERE ID_LINHA = ".$this->idLinha);	
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM LINHA WHERE ID_LINHA = ".$this->idLinha);	
		
		$this->idLinha = $rs[1]['ID_LINHA'];
		$this->nome = $rs[1]['NOME'];
		$this->codigo = $rs[1]['CODIGO'];
		$this->sigla = $rs[1]['SIGLA'];
	}
	
	public function listar() {
		$arrLinha = array();
		
		$query = "SELECT * FROM LINHA";

		if($this->idUnNegocio > 0) {
			$query = "SELECT * FROM LINHA WHERE ID_UN_NEGOCIO = ".$this->idUnNegocio;
		}

		$rs = $this->executarQueryArray($query);	
		
		for($i=1;$i<=count($rs);$i++) {
			$linha = new linhaModel();
			$linha->setIdLinha($rs[$i]['ID_LINHA']);	
			$linha->selecionar();
			
			$arrLinha[] = $linha;
		}
		
		return $arrLinha;
	}

}


