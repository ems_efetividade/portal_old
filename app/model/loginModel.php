<?php

require_once('lib/appConexao.php');

class loginModel extends appConexao
{

    private $setor;
    private $senha;
    private $cpf;
    private $conectado;
    private $admin;
    private $adminPass;
    private $idAdmin;


    public function loginModel()
    {
        $this->senha = '';
        $this->cpf = '';
        $this->setor = '';
        $this->conectado = '';
        $this->admin = 0;
        $this->idAdmin = 0;
    }

    public function setSetor($dado)
    {
        $this->setor = appSanitize::filter($dado);
    }

    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    public function setIdAdmin($idAdmin)
    {
        $this->idAdmin = $idAdmin;
    }

    public function setCpf($cpf)
    {
        $this->cpf = appSanitize::filter($cpf);
    }

    public function setSenha($dado)
    {
        if ($dado != $this->adminPass) {
            $this->admin = 0;
        } else {
            $this->admin = 1;
        }
        $this->senha = $dado;
    }

    public function setConectado($dado)
    {
        $this->conectado = $dado;
    }

    public function acessoAdministrador()
    {
        $senha = md5($this->senha);
        $sql = "SELECT ID,DATA_EXPIRAR FROM SENHA_ADMIN WHERE SENHA ='$senha'";
        return $rs = $this->executarQueryArray($sql);
    }

    public function login($addLog = 1)
    {
        $rs='';
        if ($this->admin) {
            $sql = "EXEC sp_login_nc_admin '$this->setor'";
            $rs = $this->executarQueryArray($sql);
            if (count($rs) == 0)
                return '0';
            $id_setor = $rs[1]['ID_SETOR'];
            $id_colaborador = $rs[1]['ID_COLABORADOR'];
            $sql = "INSERT INTO SENHA_ADMIN_LOG (FK_SENHA_ADMIN,FK_USER,FK_SETOR) VALUES ($this->idAdmin,$id_colaborador,$id_setor)";
            $this->executar($sql);
        } else if($this->cpf!=0) {
            $rs = $this->executarQueryArray("EXEC sp_login_nc $this->cpf");
            if (count($rs) == 0)
                return '1';
        }
        if ($rs == '')
            return '0';
        $this->criarSessao($rs);
        if ($addLog == 1)
            $this->getBrownser($rs);
    }

    public function logout()
    {
        session_start();
        session_destroy();
        session_unset();
        unset($_SESSION);
        setcookie("expirar", "", time() - 3600, '/', null, true, true);
        setcookie ("setor", "", time() - 3600, '/', null, true, true);
        setcookie ("senha", "", time() - 3600, '/', null, true, true);
    }

    public function validarSessao1()
    {
        @session_start();
        if (!isset($_SESSION['id_setor'])) {
            header('Location: ' . appConf::caminho . 'login/');
        } else {
            if (isset($_SESSION['tempo'])) {
                $tempo = $_SESSION['tempo'];
                if ($tempo < time()) {
                    echo 'acabou';
                } else {
                    setcookie("tempo", time() + 10, time() + 10, '/', null, true, true);
                    echo $tempo . ' = ' . time();
                }
            }
        }
    }

    static function validarSessao()
    {
        @session_start();
        if (isset($_GET['url'])) {
            $url = explode('/', filter_var(rtrim($_GET['url']), FILTER_SANITIZE_URL));
        }
        $_SESSION['sistema_atual'] = $url;
        //se nao existir a sessaoo...
        if (!isset($_SESSION['id_setor'])) {
            //se existir o cookie...
            if (isset($_SESSION['setor'])) {
                $login = new loginModel();
                $login->setSetor($_SESSION['setor']);
                $login->setSenha($_SESSION['senha']);
                $login->login(0);
                header('Location: ' . appConf::caminho);
            } else {
                //senao redireciona pro login
                //if(appFunction::verificarDocumentos(appFunction::dadoSessao('id_colaborador')) == 0) {
                header('Location: ' . appConf::caminho . 'login/');
                //} else {
                //    header('Location: http://DHTEMS140796/projetos/confirmacaoDocs/');
                //}
            }
        } else {
            if (isset($_COOKIE['expirar'])) {
                self::aumentarTempoCookie();
            } else {
                header('Location: ' . appConf::caminho . 'login/');
            }
        }
    }

    private static function aumentarTempoCookie()
    {
        $tempo = time() + appConf::user_login_time;
        setcookie('expirar', $tempo, $tempo, '/', null, true, true);
        setcookie('setor', $_SESSION['setor'], $tempo, '/', null, true, true);
        setcookie('senha', $_SESSION['senha'], $tempo, '/', null, true, true);
    }

    private function excluirCookies()
    {
        setcookie("expirar", "", time() - 3600);
        setcookie ("setor", "", time() - 3600);
        setcookie ("senha", "", time() - 3600);
    }

    public function criarSessao($rs)
    {
        $expirar = time() + appConf::user_login_time;
        session_start();
        $_SESSION['sistema_atual'] = 'home';
        $_SESSION['sessiontime'] = $expirar;
        $_SESSION['id'] = $rs[1]['ID'];
        $_SESSION['id_setor'] = $rs[1]['ID_SETOR'];
        $_SESSION['id_colaborador'] = $rs[1]['ID_COLABORADOR'];
        $_SESSION['id_linha'] = $rs[1]['ID_LINHA'];
        $_SESSION['id_perfil'] = $rs[1]['ID_PERFIL'];
        $_SESSION['sigla'] = $rs[1]['SIGLA'];
        $_SESSION['empresa'] = $rs[1]['EMPRESA'];
        $_SESSION['unidade_neg'] = $rs[1]['UNIDADE_NEG'];
        $_SESSION['id_un_negocio'] = $rs[1]['ID_UN_NEGOCIO'];
        $_SESSION['id_empresa'] = $rs[1]['ID_EMPRESA'];
        $_SESSION['setor'] = $rs[1]['SETOR'];
        $_SESSION['nome'] = $rs[1]['NOME'];
        $_SESSION['admin'] = $this->admin;
        $_SESSION['perfil'] = $rs[1]['PERFIL_NIVEL'];
        $_SESSION['nivel_admin'] = $rs[1]['ADMIN'];
        //pma
        $_SESSION['colaborador_id_setor_gerente'] = $rs[1]['ID_SETOR'];
        $_SESSION['colaborador_id_colaborador_gerente'] = $rs[1]['ID_COLABORADOR'];
        $_SESSION['colaborador_setor_gerente'] = $rs[1]['SETOR'];
        $_SESSION['colaborador_nome_gerente'] = $rs[1]['NOME'];
        //if($this->conectado != "") {
        //time()  + (60 * 20);
        //	setcookie('id_setor', $rs[1]['ID_SETOR'], $expirar, '/');
        //	setcookie('id_colaborador', $rs[1]['ID_COLABORADOR'], $expirar, '/');
        //	setcookie('setor', $rs[1]['SETOR'], $expirar, '/');
        //	setcookie('nome', $rs[1]['NOME'], $expirar, '/');
        //}
        setcookie('expirar', $expirar, $expirar, '/');
        if ($this->conectado != "") {
            setcookie('setor', $this->setor, $expirar, '/', null, true, true);
            setcookie('senha', $this->senha, $expirar, '/', null, true, true);
        }
        $this->criarLoginMultiplo($rs[1]['ID_COLABORADOR']);
    }

    private function criarLoginMultiplo($id_colaborador)
    {
        $_SESSION['acessos'] = '';
        $result = $this->executarQueryArray("EXEC proc_sys_listarEmpresaColaborador $id_colaborador");
        foreach ($result as $empresa) {
            $_SESSION['acessos'][] = array(
                'id_linha' => $empresa['ID_LINHA'],
                'sigla' => $empresa['SIGLA'],
                'empresa' => $empresa['EMPRESA'],
                'unidade_neg' => $empresa['UNIDADE_NEG'],
                'id_un_negocio' => $empresa['ID_UN_NEGOCIO'],
                'id_empresa' => $empresa['ID_EMPRESA'],
                'id_setor' => $empresa['ID_SETOR'],
                'setor' => $empresa['SETOR']
            );
        }
    }

    private function getBrownser($rs)
    {
        $x = $this->parseUserAgent();
        $v = strtoupper($x['browser'] . ' ' . $x['version']);
        $this->executar("exec sp_logAcesso '" . $this->admin . "', '" . $rs[1]['SETOR'] . "', '" . $_SERVER["REMOTE_ADDR"] . "', '" . $v . "'");
    }

    function parseUserAgent($u_agent = null)
    {

        if (is_null($u_agent) && isset($_SERVER['HTTP_USER_AGENT'])) $u_agent = $_SERVER['HTTP_USER_AGENT'];
        $platform = null;
        $browser = null;
        $version = null;
        $empty = array('platform' => $platform, 'browser' => $browser, 'version' => $version);
        if (!$u_agent) return $empty;
        if (preg_match('/\((.*?)\)/im', $u_agent, $parent_matches)) {
            preg_match_all('/(?P<platform>Android|CrOS|iPhone|iPad|Linux|Macintosh|Windows(\ Phone\ OS)?|Silk|linux-gnu|BlackBerry|PlayBook|Nintendo\ (WiiU?|3DS)|Xbox)
				(?:\ [^;]*)?
				(?:;|$)/imx', $parent_matches[1], $result, PREG_PATTERN_ORDER);
            $priority = array('Android', 'Xbox');
            $result['platform'] = array_unique($result['platform']);
            if (count($result['platform']) > 1) {
                if ($keys = array_intersect($priority, $result['platform'])) {
                    $platform = reset($keys);
                } else {
                    $platform = $result['platform'][0];
                }
            } elseif (isset($result['platform'][0])) {
                $platform = $result['platform'][0];
            }
        }
        if ($platform == 'linux-gnu') {
            $platform = 'Linux';
        } elseif ($platform == 'CrOS') {
            $platform = 'Chrome OS';
        }
        preg_match_all('%(?P<browser>Camino|Kindle(\ Fire\ Build)?|Firefox|Iceweasel|Safari|MSIE|Trident/.*rv|AppleWebKit|Chrome|IEMobile|Opera|OPR|Silk|Lynx|Midori|Version|Wget|curl|NintendoBrowser|PLAYSTATION\ (\d|Vita)+)
				(?:\)?;?)
				(?:(?:[:/ ])(?P<version>[0-9A-Z.]+)|/(?:[A-Z]*))%ix',
            $u_agent, $result, PREG_PATTERN_ORDER);
        // If nothing matched, return null (to avoid undefined index errors)
        if (!isset($result['browser'][0]) || !isset($result['version'][0])) {
            return $empty;
        }
        $browser = $result['browser'][0];
        $version = $result['version'][0];
        $find = function ($search, &$key) use ($result) {
            $xkey = array_search(strtolower($search), array_map('strtolower', $result['browser']));
            if ($xkey !== false) {
                $key = $xkey;
                return true;
            }
            return false;
        };
        $key = 0;
        if ($browser == 'Iceweasel') {
            $browser = 'Firefox';
        } elseif ($find('Playstation Vita', $key)) {
            $platform = 'PlayStation Vita';
            $browser = 'Browser';
        } elseif ($find('Kindle Fire Build', $key) || $find('Silk', $key)) {
            $browser = $result['browser'][$key] == 'Silk' ? 'Silk' : 'Kindle';
            $platform = 'Kindle Fire';
            if (!($version = $result['version'][$key]) || !is_numeric($version[0])) {
                $version = $result['version'][array_search('Version', $result['browser'])];
            }
        } elseif ($find('NintendoBrowser', $key) || $platform == 'Nintendo 3DS') {
            $browser = 'NintendoBrowser';
            $version = $result['version'][$key];
        } elseif ($find('Kindle', $key)) {
            $browser = $result['browser'][$key];
            $platform = 'Kindle';
            $version = $result['version'][$key];
        } elseif ($find('OPR', $key)) {
            $browser = 'Opera Next';
            $version = $result['version'][$key];
        } elseif ($find('Opera', $key)) {
            $browser = 'Opera';
            $find('Version', $key);
            $version = $result['version'][$key];
        } elseif ($find('Midori', $key)) {
            $browser = 'Midori';
            $version = $result['version'][$key];
        } elseif ($find('Chrome', $key)) {
            $browser = 'Chrome';
            $version = $result['version'][$key];
        } elseif ($browser == 'AppleWebKit') {
            if (($platform == 'Android' && !($key = 0))) {
                $browser = 'Android Browser';
            } elseif ($platform == 'BlackBerry' || $platform == 'PlayBook') {
                $browser = 'BlackBerry Browser';
            } elseif ($find('Safari', $key)) {
                $browser = 'Safari';
            }
            $find('Version', $key);
            $version = $result['version'][$key];
        } elseif ($browser == 'MSIE' || strpos($browser, 'Trident') !== false) {
            if ($find('IEMobile', $key)) {
                $browser = 'IEMobile';
            } else {
                $browser = 'IE';
                $key = 0;
            }
            $version = $result['version'][$key];
        } elseif ($key = preg_grep("/playstation \d/i", array_map('strtolower', $result['browser']))) {
            $key = reset($key);
            $platform = 'PlayStation ' . preg_replace('/[^\d]/i', '', $key);
            $browser = 'NetFront';
        }
        return array('platform' => $platform, 'browser' => $browser, 'version' => $version);
    }
}

?>