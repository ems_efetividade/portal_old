<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 13/09/2018
 * Time: 15:36
 */
require_once('lib/appConexao.php');

class mAdmin extends appConexao
{


    function fotoColaborador($idColaborador = 0)
    {
        if ($idColaborador == 0) {
            $foto = appConf::caminho . 'public/img/exemplo_foteoo.jpg';
        } else {

            $cn = new appConexao();

            $rs = $cn->executarQueryArray("SELECT FOTO FROM FOTO_PERFIL WHERE ID_COLABORADOR = " . $idColaborador);

            if (count($rs) > 0) {

                if (!file_exists('public/profile/' . $rs[1]['FOTO'])) {
                    $foto = appConf::caminho . 'public/img/exemplo_foteoo.jpg';
                } else {
                    $foto = appConf::caminho . appConf::folder_profile . $rs[1]['FOTO'];
                }
                //$foto = appConf::caminho.appConf::folder_profile.$rs[1]['FOTO'];

            } else {
                $foto = appConf::caminho . 'public/img/exemplo_foteoo.jpg';
            }

            /*

*/
        }
        return $foto;

    }

    public function salvarMensagem($mensagem,$nomeColaborador,$idColaborador,$hora){
        $sql = "INSERT INTO MEET_CHAT (FK_USER,MENSAGEM,NOME,HORA) VALUES ($idColaborador,'$mensagem','$nomeColaborador','$hora'); SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        return $this->executarQueryArray($sql);
    }

    public function listarMensagem($ultimo){
        $sql = "SELECT * FROM MEET_CHAT WHERE ID > $ultimo ORDER BY ID ASC ";
        $sql = utf8_decode($sql);
        return $this->executarQueryArray($sql);
    }

    public function listarDadosSetor()
    {
        ini_set("memory_limit", "9999999999M");
        $sql = "SELECT NOME,SETOR,LINHA,PERFIL,EMAIL,UGN FROM vw_colaboradorSetor INNER JOIN UGN ON UGN.CODIGO = SUBSTRING(SETOR,4,1) WHERE ID_EMPRESA = 1 AND setor > '11200000' AND SETOR < '11999999'";
        return $this->executarQueryArray($sql);
    }
    public function buscarRanking($setor)
    {
        ini_set("memory_limit", "9999999999M");
        $sql = "exec proc_dsh_ranking '$setor'";
        return $this->executarQueryArray($sql);
    }

    public function subirPXBanco($sql)
    {
        ini_set("memory_limit", "9999999999M");
        return $this->executar($sql);
    }

    public function limparBasePx()
    {
        $sql = "TRUNCATE TABLE PS_BASE";
        return $this->executar($sql);
    }

    public function rodarBase()
    {
        $sql = "EXEC proc_ps_atualizarPrescriptionShare";
        return $this->executar($sql);
    }

    public function loadNomeUsuarioByCpf($cpf)
    {
        $sql = "SELECT NOME FROM vw_colaboradorSetor WHERE CPF = $cpf";
        return $this->executarQueryArray($sql);
    }

    public function getIdUsuarioByCpf($cpf)
    {
        $sql = "SELECT ID_COLABORADOR FROM vw_colaboradorSetor WHERE CPF = $cpf";
        return $this->executarQueryArray($sql);
    }

    public function verificaSetor($SETOR)
    {
        $sql = "SELECT NOME,EMAIL,ID_COLABORADOR FROM vw_colaboradorSetor WHERE SETOR = '$SETOR'";
        return $this->executarQueryArray($sql);
    }

    public function resetar($setor)
    {
        //$str = "abcdefghijklmnopqrstuvxz1234567890ABCDEFGHIJKLMNOPQRSTUVXZ";
        //$codigo = substr(str_shuffle($str), 0, 8);
        //$md5 = md5($codigo);
        $sql = "UPDATE SETOR SET SENHA = '123' WHERE SETOR = '$setor'";
        require_once('/../../SpeedLab/conectDb.php');
        $db = new conectDb();
        $db->executar($sql, 1);
        //return $codigo;
    }

    public function listarAlteracoes($status)
    {
        $sql = "SELECT ca.id, c.NOME,c.EMAIL,ca.SETOR,ca.SETOR_OLD,ca.DATA_IN from COLABORADOR_ALTERACAO ca JOIN COLABORADOR c on c.ID_COLABORADOR = ca.ID_COLABORADOR WHERE status = $status order by ca. DATA_IN";
        return $this->executarQueryArray($sql);
    }

    public function listarPendenciasLogin()
    {
        $sql = "EXEC proc_sys_listarUsuarioSenhaPadrao 0";
        return $this->executarQueryArray($sql);
    }


}