<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 27/08/2018
 * Time: 11:44
 */
require_once('lib/appConexao.php');

class mAjuda extends appConexao
{
    private $id;
    private $tags;
    private $titulo;
    private $resposta;
    private $dataCadastro;
    private $responsavel;

    public function __construct()
    {
        $this->id;
        $this->tags;
        $this->titulo;
        $this->resposta;
        $this->dataCadastro;
        $this->responsavel;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getTags()
    {
        return $this->tags;
    }

    public function getTitulo()
    {
        return $this->titulo;
    }

    public function getResposta()
    {
        return $this->resposta;
    }

    public function getDataCadastro()
    {
        return $this->dataCadastro;
    }

    public function getResponsavel()
    {
        $sql = "SELECT NOME FROM COLABORADOR WHERE ID_COLABORADOR = $this->responsavel";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setTags($Tags)
    {
        $this->tags = $Tags;
    }

    public function setTitulo($Titulo)
    {
        $this->titulo = $Titulo;
    }

    public function setResposta($Resposta)
    {
        $this->resposta = $Resposta;
    }

    public function setDataCadastro($DataCadastro)
    {
        $this->dataCadastro = $DataCadastro;
    }

    public function setResponsavel($Responsavel)
    {
        $this->responsavel = $Responsavel;
    }

    public function countRows()
    {

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];

        $sql = "select count(id) from ATENDIMENTO_DUVIDAS";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($tags, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "TAGS LIKE '%$tags%' ";
            $verif = true;
        }
        if (strcmp($titulo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "TITULO LIKE '%$titulo%' ";
            $verif = true;
        }
        if (strcmp($resposta, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "RESPOSTA LIKE '%$resposta%' ";
            $verif = true;
        }
        if (strcmp($dataCadastro, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if (strcmp($responsavel, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "RESPONSAVEL LIKE '%$responsavel%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "TAGS ";
        $sql .= ",";
        $sql .= "TITULO ";
        $sql .= ",";
        $sql .= "RESPOSTA ";
        $sql .= ",";
        $sql .= "DATA_CADASTRO ";
        $sql .= ",";
        $sql .= "RESPONSAVEL ";
        $sql .= ",";
        $sql .= "ID_SISTEMA_ACOPLADO ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM ATENDIMENTO_DUVIDAS ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tags, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TAGS LIKE '%$tags%' ";
            $verif = true;
        }
        if(strcmp($titulo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TITULO LIKE '%$titulo%' ";
            $verif = true;
        }
        if(strcmp($resposta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESPOSTA LIKE '%$resposta%' ";
            $verif = true;
        }
        if(strcmp($dataCadastro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if(strcmp($responsavel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESPONSAVEL LIKE '%$responsavel%' ";
            $verif = true;
        }
        if(strcmp($idSistemaAcoplado, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID_SISTEMA_ACOPLADO LIKE '%$idSistemaAcoplado%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $arrayResult = $this->executarQueryArray($sql);
        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {
        $o = new mAjuda();
        $o->setId($param[0]);
        $o->setTags($param[1]);
        $o->setTitulo($param[2]);
        $o->setResposta($param[3]);
        $o->setDataCadastro($param[4]);
        $o->setResponsavel($param[5]);
        return $o;
    }

    public function salvarLog($id_user, $id_duvida)
    {
        $sql = "INSERT INTO ATENDIMENTO_DUVIDAS_LOG (ID_USER,ID_ATENDIMENTO_DUVIDAS) VALUES ($id_user,$id_duvida)";
        $this->executar($sql);
    }

    public function salvarRating($id_user, $id_duvida,$nota){

        $sql = "SELECT ID FROM ATENDIMENTO_DUVIDA_AVALIACAO WHERE ID_COLABORADOR = $id_user AND ID_ATENDIMENTO_DUVIDA = $id_duvida";
        $retorno = $this->executarQueryArray($sql);
        $id = $retorno[1][0];
        if($id > 0 ){
            $sql = "UPDATE ATENDIMENTO_DUVIDA_AVALIACAO SET NOTA = $nota where ID = $id";
            $this->executar($sql);
            return;
        }
        $sql = "INSERT INTO ATENDIMENTO_DUVIDA_AVALIACAO (ID_COLABORADOR, ID_ATENDIMENTO_DUVIDA, NOTA) VALUES ($id_user,$id_duvida,$nota)";
        $this->executar($sql);
    }

    public function getAvaliacao($id,$id_user){
        $sql = "SELECT NOTA FROM ATENDIMENTO_DUVIDA_AVALIACAO WHERE ID_COLABORADOR = $id_user AND ID_ATENDIMENTO_DUVIDA = $id";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];

    }
}