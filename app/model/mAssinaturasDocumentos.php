<?php
require_once('lib/appConexao.php');

class mAssinaturasDocumentos extends appConexao
{

    private $id;
    private $nome;
    private $arquivo;
    private $ativo;
    private $dataIn;

    public function __construct()
    {
        $this->id;
        $this->nome;
        $this->arquivo;
        $this->ativo;
        $this->dataIn;
    }


    public function listarTermos()
    {
        $sql = "SELECT * FROM TERMO_GERADO WHERE ASSINADO = 1";
        return $this->executarQueryArray($sql);
    }

    public function listarnomes($id)
    {
        $sql = "SELECT * FROM vw_colaboradorSetor WHERE ID_COLABORADOR = $id";
        return $this->executarQueryArray($sql);
    }

    public function listUsuarios()
    {
        $sql = "select ID_COLABORADOR, NOME from COLABORADOR order by nome asc";
        return $this->executarQueryArray($sql);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getArquivo()
    {
        return $this->arquivo;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function getAtivoTratado()
    {
        if ($this->ativo == 1)
            return 'Sim';
        return 'Não';
    }

    public function getDataIn()
    {
        return $this->dataIn;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setNome($Nome)
    {
        $this->nome = $Nome;
    }

    public function setArquivo($Arquivo)
    {
        $this->arquivo = $Arquivo;
    }

    public function setAtivo($Ativo)
    {
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn)
    {
        $this->dataIn = $DataIn;
    }

    public function listarAssinadosByIdColaborador($user)
    {
        $sql = "SELECT ID,FK_USUARIO,FK_DOCUMENTO,SETOR,CHAVE,ACEITO,DATA_IN FROM ASSINATURAS WHERE ATIVO = 1 AND FK_USUARIO = $user";
        return $this->executarQueryArray($sql);
    }

    public function validarResposta($id_colaborador, $setor, $resposta, $documento, $date)
    {
        $chave = md5($id_colaborador . $setor . $documento . $date);
        $sql = 'INSERT INTO ASSINATURAS (FK_USUARIO,FK_DOCUMENTO,SETOR,CHAVE,ACEITO)';
        $sql .= "VALUES ($id_colaborador,$documento,'$setor','$chave',$resposta)";
        $this->executar($sql);
        return $chave;
    }

    public function guardarPermissoes($id, $inicio, $expira, $empresas, $unidades, $linhas, $perfis)
    {
        $sql = "";
        foreach ($empresas as $empresa) {
            foreach ($unidades as $unidade) {
                foreach ($linhas as $linha) {
                    foreach ($perfis as $perfil) {
                        $sql .= 'INSERT INTO ASSINATURAS_PERMISSAO ';
                        $sql .= ' (ID_PERFIL,FK_ASSINATURAS_DOCUMENTOS,DATA_INICIO,DATA_FIM,ID_EMPRESA,ID_UNIDADE,ID_LINHA)';
                        $sql .= ' VALUES ';
                        $sql .= "($perfil,$id,'$inicio','$expira',$empresa,$unidade,$linha);";
                    }
                }
            }
        }
        $this->executar($sql);
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $sql = "select count(id) from ASSINATURAS_DOCUMENTOS";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE ' % $id % ' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NOME LIKE ' % $nome % ' ";
            $verif = true;
        }
        if (strcmp($arquivo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ARQUIVO LIKE ' % $arquivo % ' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE ' % $ativo % ' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save()
    {
        $nome = $_POST['nome'];
        $arquivo = utf8_decode($_POST['arquivo']);
        $sql = "INSERT INTO ASSINATURAS_DOCUMENTOS ([NOME],[ARQUIVO])";
        $sql .= " VALUES ('$nome','$arquivo')";
        $this->executar($sql);
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == ' + ') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == ' - ') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (" . $sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= " . $atual;
        $paginacao .= " AND row <= " . $max . " ";
        return $paginacao;
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);
        $id = '';
        $nome = '';
        $arquivo = '';
        $ativo = '';
        $dataIn = '';
        $multiplos = '';
        if (isset($_POST['id']))
            $id = $_POST['id'];
        if (isset($_POST['nome']))
            $nome = $_POST['nome'];
        if (isset($_POST['arquivo']))
            $arquivo = $_POST['arquivo'];
        if (isset($_POST['ativo']))
            $ativo = $_POST['ativo'];
        if (isset($_POST['datain']))
            $dataIn = $_POST['datain'];


        $sql = "UPDATE ASSINATURAS_DOCUMENTOS SET ATIVO = 0 WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = $id ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NOME = '$nome' ";
            $verif = true;
        }
        if (strcmp($arquivo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ARQUIVO = '$arquivo' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ATIVO = '$ativo' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        $this->executar($sql);
        if (isset($_POST['multiplos']))
            $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $where = '';
        $id = '';
        $nome = '';
        $arquivo = '';
        $ativo = '';
        $dataIn = '';
        $multiplos = '';
        if (isset($_POST['id']))
            $id = $_POST['id'];
        if (isset($_POST['nome']))
            $nome = $_POST['nome'];
        if (isset($_POST['arquivo']))
            $arquivo = $_POST['arquivo'];
        if (isset($_POST['ativo']))
            $ativo = $_POST['ativo'];
        if (isset($_POST['datain']))
            $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "ARQUIVO ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM ASSINATURAS_DOCUMENTOS ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID = $id ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NOME LIKE ' % $nome % ' ";
            $verif = true;
        }
        if (strcmp($arquivo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ARQUIVO LIKE ' % $arquivo % ' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO = $ativo   ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $sql = "UPDATE  ASSINATURAS_DOCUMENTOS";
        $sql .= " SET ";
        $sql .= "NOME = '$nome' ";
        $sql .= " , ";
        $sql .= "ARQUIVO = '$arquivo' ";
        $sql .= " , ";
        $sql .= "ATIVO = '$ativo' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function verificaAssinatura($id_Documento)
    {
        $id_colaborador = $_SESSION['id_colaborador'];
        $sql = "SELECT * FROM ASSINATURAS WHERE FK_USUARIO = $id_colaborador AND FK_DOCUMENTO = $id_Documento AND ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function listByLogin()
    {
        $perfil = $_SESSION['id_perfil'];
        $linha = $_SESSION['id_linha'];
        $unidade = $_SESSION['id_un_negocio'];
        $empresa = $_SESSION['id_empresa'];
        $sql = "SELECT * FROM ASSINATURAS_PERMISSAO WHERE ID_PERFIL = $perfil AND ID_EMPRESA = $empresa AND ID_UNIDADE = $unidade AND ID_LINHA = $linha AND ATIVO = 1 AND DATA_INICIO <= getdate() AND DATA_FIM >= getdate()";
        return $this->executarQueryArray($sql);
    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $arquivo = $_POST['arquivo'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "ARQUIVO ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM ASSINATURAS_DOCUMENTOS ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID LIKE ' % $id % ' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NOME LIKE ' % $nome % ' ";
            $verif = true;
        }
        if (strcmp($arquivo, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ARQUIVO LIKE ' % $arquivo % ' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ATIVO = '$ativo' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {

        $o = new mAssinaturasDocumentos();
        $o->setId($param[0]);
        $o->setNome($param[1]);
        $o->setArquivo($param[2]);
        $o->setAtivo($param[3]);
        $o->setDataIn($param[4]);

        return $o;

    }
}