<?php
require_once('lib/appConexao.php');
require_once('/../../lib/appSanitize.php');

class mControleHeader extends appConexao
{

    private $id;
    private $campo;
    private $url;
    private $icone;
    private $nivel;
    private $ordem;
    private $super;
    private $fkFerramenta;
    private $ativo;
    private $imagem;

    public function __construct()
    {
        $this->id;
        $this->campo;
        $this->url;
        $this->icone;
        $this->nivel;
        $this->ordem;
        $this->super;
        $this->fkFerramenta;
        $this->ativo;
        $this->imagem;
    }

    public function getImagem()
    {
        return $this->imagem;
    }


    public function getId()
    {
        return $this->id;
    }

    public function getCampo()
    {
        return $this->campo;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function getIcone()
    {
        return $this->icone;
    }

    public function getNivel()
    {
        return $this->nivel;
    }

    public function getOrdem()
    {
        return $this->ordem;
    }

    public function getSuper()
    {
        return $this->super;
    }

    public function getFkFerramenta()
    {
        return $this->fkFerramenta;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setCampo($Campo)
    {
        $this->campo = $Campo;
    }

    public function setUrl($Url)
    {
        $this->url = $Url;
    }

    public function setIcone($Icone)
    {
        $this->icone = $Icone;
    }


    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    public function setNivel($Nivel)
    {
        $this->nivel = $Nivel;
    }

    public function setOrdem($Ordem)
    {
        $this->ordem = $Ordem;
    }

    public function setSuper($Super)
    {
        $this->super = $Super;
    }

    public function setFkFerramenta($FkFerramenta)
    {
        $this->fkFerramenta = $FkFerramenta;
    }

    public function setAtivo($Ativo)
    {
        $this->ativo = $Ativo;
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];

        $sql = "select count(id) from CONTROLE_HEADER";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($campo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if (strcmp($url, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "URL LIKE '%$url%' ";
            $verif = true;
        }
        if (strcmp($icone, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if (strcmp($nivel, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if (strcmp($ordem, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if (strcmp($super, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "SUPER LIKE '%$super%' ";
            $verif = true;
        }
        if (strcmp($fkFerramenta, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];

        $sql = "INSERT INTO CONTROLE_HEADER ([CAMPO],[URL],[ICONE],[NIVEL],[ORDEM],[SUPER],[FK_FERRAMENTA],[ATIVO])";
        $sql .= " VALUES ('$campo','$url','$icone','$nivel','$ordem','$super','$fkFerramenta','$ativo')";
        $this->executar($sql);
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == '+') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == '-') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (" . $sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= " . $atual;
        $paginacao .= " AND row <= " . $max . " ";
        return $paginacao;
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];

        $sql = "DELETE FROM CONTROLE_HEADER WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = '$id' ";
            $verif = true;
        }
        if (strcmp($campo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CAMPO = '$campo' ";
            $verif = true;
        }
        if (strcmp($url, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "URL = '$url' ";
            $verif = true;
        }
        if (strcmp($icone, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ICONE = '$icone' ";
            $verif = true;
        }
        if (strcmp($nivel, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NIVEL = '$nivel' ";
            $verif = true;
        }
        if (strcmp($ordem, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ORDEM = '$ordem' ";
            $verif = true;
        }
        if (strcmp($super, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "SUPER = '$super' ";
            $verif = true;
        }
        if (strcmp($fkFerramenta, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "FK_FERRAMENTA = '$fkFerramenta' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ATIVO = '$ativo' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CAMPO ";
        $sql .= ",";
        $sql .= "URL ";
        $sql .= ",";
        $sql .= "ICONE ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "ORDEM ";
        $sql .= ",";
        $sql .= "SUPER ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= " FROM CONTROLE_HEADER ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($campo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if (strcmp($url, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "URL LIKE '%$url%' ";
            $verif = true;
        }
        if (strcmp($icone, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if (strcmp($nivel, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if (strcmp($ordem, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if (strcmp($super, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "SUPER LIKE '%$super%' ";
            $verif = true;
        }
        if (strcmp($fkFerramenta, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $sql = "UPDATE  CONTROLE_HEADER";
        $sql .= " SET ";
        $sql .= "CAMPO = '$campo' ";
        $sql .= " , ";
        $sql .= "URL = '$url' ";
        $sql .= " , ";
        $sql .= "ICONE = '$icone' ";
        $sql .= " , ";
        $sql .= "NIVEL = '$nivel' ";
        $sql .= " , ";
        $sql .= "ORDEM = '$ordem' ";
        $sql .= " , ";
        $sql .= "SUPER = '$super' ";
        $sql .= " , ";
        $sql .= "FK_FERRAMENTA = '$fkFerramenta' ";
        $sql .= " , ";
        $sql .= "ATIVO = '$ativo' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);
        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CAMPO ";
        $sql .= ",";
        $sql .= "URL ";
        $sql .= ",";
        $sql .= "ICONE ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "ORDEM ";
        $sql .= ",";
        $sql .= "SUPER ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "IMAGEM ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ORDEM ) as row";
        $sql .= " FROM CONTROLE_HEADER ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($campo, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if (strcmp($url, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "URL LIKE '%$url%' ";
            $verif = true;
        }
        if (strcmp($icone, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if (strcmp($nivel, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if (strcmp($ordem, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if (strcmp($super, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "SUPER = $super ";
            $verif = true;
        }
        if (strcmp($fkFerramenta, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " and ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        //$_POST['numRows'] = $this->countRows();
        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function listSistemasDisponiveis()
    {
        $_POST = appSanitize::filter($_POST);
        $sql = "select * from CONTROLE_HEADER where ATIVO = 1 and FK_FERRAMENTA > 0 or ATIVO = 1 and FK_FERRAMENTA < 0 order by super";
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {
        $o = new mControleHeader();
        $o->setId($param[0]);
        $o->setCampo($param[1]);
        $o->setUrl($param[2]);
        $o->setIcone($param[3]);
        $o->setNivel($param[4]);
        $o->setOrdem($param[5]);
        $o->setSuper($param[6]);
        $o->setFkFerramenta($param[7]);
        $o->setAtivo($param[8]);
        $o->setImagem($param[9]);
        return $o;
    }

    public function listPermissoesEmpresas()
    {
        //session_start();
        $id_empresa = $_SESSION['id_empresa'];
        $id_bu = $_SESSION['id_un_negocio'];
        $id_linha = $_SESSION['id_linha'];
        $id_perfil = $_SESSION['id_perfil'];
        $sql = "SELECT ID_MENU FROM CONTROLE_HEADER_PERMISSOES WHERE ATIVO = 1 AND ID_EMPRESA = $id_empresa AND ID_LINHA = $id_linha AND ID_BU = $id_bu AND ID_PERFIL = $id_perfil";
        $arrayResult = $this->executarQueryArray($sql);
        foreach ($arrayResult as $retorno) {
            $array_id_menu[] = $retorno['ID_MENU'];
        }
        return $array_id_menu;
    }
}