<?php
require_once('lib/appConexao.php');

class mGridPermissao extends appConexao {

    private $id;
    private $idColaborador;
    private $idFerramenta;
    private $visualizacao;
    private $escrita;
    private $edicao;
    private $exclusao;
    private $gerenciar;
    private $idUsuario;

    public function __construct(){
        $this->id;
        $this->idColaborador;
        $this->idFerramenta;
        $this->visualizacao;
        $this->escrita;
        $this->edicao;
        $this->exclusao;
        $this->gerenciar;
        $this->idUsuario;
    }

    public function listNomeColaborador(){
        $sql = "SELECT NOME,ID_COLABORADOR from COLABORADOR order by NOME ";
        $retorno =  $this->executarQueryArray($sql);
        return $retorno;
    }

    public function listNomeFerramenta(){
        $sql = "SELECT ID_FERRAMENTA,FERRAMENTA from FERRAMENTA2 order by FERRAMENTA ";
        $retorno =  $this->executarQueryArray($sql);
        return $retorno;
    }

    public function getId(){
        return $this->id;
    }

    public function getIdColaborador(){
        return $this->idColaborador;
    }

    public function getIdFerramenta(){
        return $this->idFerramenta;
    }

    public function getVisualizacao(){
        return $this->visualizacao;
    }

    public function getEscrita(){
        return $this->escrita;
    }

    public function getEdicao(){
        return $this->edicao;
    }

    public function getExclusao(){
        return $this->exclusao;
    }

    public function getGerenciar(){
        return $this->gerenciar;
    }

    public function getIdUsuario(){
        return $this->idUsuario;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setIdColaborador($IdColaborador){
        $this->idColaborador = $IdColaborador;
    }

    public function setIdFerramenta($IdFerramenta){
        $this->idFerramenta = $IdFerramenta;
    }

    public function setVisualizacao($Visualizacao){
        $this->visualizacao = $Visualizacao;
    }

    public function setEscrita($Escrita){
        $this->escrita = $Escrita;
    }

    public function setEdicao($Edicao){
        $this->edicao = $Edicao;
    }

    public function setExclusao($Exclusao){
        $this->exclusao = $Exclusao;
    }

    public function setGerenciar($Gerenciar){
        $this->gerenciar = $Gerenciar;
    }

    public function setIdUsuario($IdUsuario){
        $this->idUsuario = $IdUsuario;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];

        $sql = "select count(id) from GRID_PERMISSAO";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($idColaborador, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_colaborador LIKE '%$idColaborador%' ";
            $verif = true;
        }
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_ferramenta LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($visualizacao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="visualizacao LIKE '%$visualizacao%' ";
            $verif = true;
        }
        if(strcmp($escrita, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="escrita LIKE '%$escrita%' ";
            $verif = true;
        }
        if(strcmp($edicao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="edicao LIKE '%$edicao%' ";
            $verif = true;
        }
        if(strcmp($exclusao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="exclusao LIKE '%$exclusao%' ";
            $verif = true;
        }
        if(strcmp($gerenciar, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="gerenciar LIKE '%$gerenciar%' ";
            $verif = true;
        }
        if(strcmp($idUsuario, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_usuario LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];

        $sql = "INSERT INTO GRID_PERMISSAO ([id_colaborador],[id_ferramenta],[visualizacao],[escrita],[edicao],[exclusao],[gerenciar],[id_usuario])";
        $sql .=" VALUES ('$idColaborador','$idFerramenta','$visualizacao','$escrita','$edicao','$exclusao','$gerenciar','$idUsuario')";
        $this->executar($sql);
    }

    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];

        $sql = "DELETE FROM GRID_PERMISSAO WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="id = '$id' ";
            $verif = true;
        }
        if(strcmp($idColaborador, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="id_colaborador = '$idColaborador' ";
            $verif = true;
        }
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="id_ferramenta = '$idFerramenta' ";
            $verif = true;
        }
        if(strcmp($visualizacao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="visualizacao = '$visualizacao' ";
            $verif = true;
        }
        if(strcmp($escrita, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="escrita = '$escrita' ";
            $verif = true;
        }
        if(strcmp($edicao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="edicao = '$edicao' ";
            $verif = true;
        }
        if(strcmp($exclusao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="exclusao = '$exclusao' ";
            $verif = true;
        }
        if(strcmp($gerenciar, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="gerenciar = '$gerenciar' ";
            $verif = true;
        }
        if(strcmp($idUsuario, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="id_usuario = '$idUsuario' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }

    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "id ";
        $sql .= ",";
        $sql .= "id_colaborador ";
        $sql .= ",";
        $sql .= "id_ferramenta ";
        $sql .= ",";
        $sql .= "visualizacao ";
        $sql .= ",";
        $sql .= "escrita ";
        $sql .= ",";
        $sql .= "edicao ";
        $sql .= ",";
        $sql .= "exclusao ";
        $sql .= ",";
        $sql .= "gerenciar ";
        $sql .= ",";
        $sql .= "id_usuario ";
        $sql .= " FROM GRID_PERMISSAO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($idColaborador, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_colaborador LIKE '%$idColaborador%' ";
            $verif = true;
        }
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_ferramenta LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($visualizacao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="visualizacao LIKE '%$visualizacao%' ";
            $verif = true;
        }
        if(strcmp($escrita, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="escrita LIKE '%$escrita%' ";
            $verif = true;
        }
        if(strcmp($edicao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="edicao LIKE '%$edicao%' ";
            $verif = true;
        }
        if(strcmp($exclusao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="exclusao LIKE '%$exclusao%' ";
            $verif = true;
        }
        if(strcmp($gerenciar, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="gerenciar LIKE '%$gerenciar%' ";
            $verif = true;
        }
        if(strcmp($idUsuario, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id_usuario LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj(){
        $id = $_POST['id'];
        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];
        $sql = "UPDATE  GRID_PERMISSAO";
        $sql .=" SET ";
        $sql .="id_colaborador = '$idColaborador' ";
        $sql .=" , ";
        $sql .="id_ferramenta = '$idFerramenta' ";
        $sql .=" , ";
        $sql .="visualizacao = '$visualizacao' ";
        $sql .=" , ";
        $sql .="escrita = '$escrita' ";
        $sql .=" , ";
        $sql .="edicao = '$edicao' ";
        $sql .=" , ";
        $sql .="exclusao = '$exclusao' ";
        $sql .=" , ";
        $sql .="gerenciar = '$gerenciar' ";
        $sql .=" , ";
        $sql .="id_usuario = '$idUsuario' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idColaborador = $_POST['idcolaborador'];
        $idFerramenta = $_POST['idferramenta'];
        $visualizacao = $_POST['visualizacao'];
        $escrita = $_POST['escrita'];
        $edicao = $_POST['edicao'];
        $exclusao = $_POST['exclusao'];
        $gerenciar = $_POST['gerenciar'];
        $idUsuario = $_POST['idusuario'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "id ";
        $sql .= ",";
        $sql .= "id_colaborador ";
        $sql .= ",";
        $sql .= "id_ferramenta ";
        $sql .= ",";
        $sql .= "visualizacao ";
        $sql .= ",";
        $sql .= "escrita ";
        $sql .= ",";
        $sql .= "edicao ";
        $sql .= ",";
        $sql .= "exclusao ";
        $sql .= ",";
        $sql .= "gerenciar ";
        $sql .= ",";
        $sql .= "id_usuario ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM GRID_PERMISSAO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($idColaborador, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="id_colaborador LIKE '%$idColaborador%' ";
            $verif = true;
        }
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="id_ferramenta LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($visualizacao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="visualizacao LIKE '%$visualizacao%' ";
            $verif = true;
        }
        if(strcmp($escrita, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="escrita LIKE '%$escrita%' ";
            $verif = true;
        }
        if(strcmp($edicao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="edicao LIKE '%$edicao%' ";
            $verif = true;
        }
        if(strcmp($exclusao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="exclusao LIKE '%$exclusao%' ";
            $verif = true;
        }
        if(strcmp($gerenciar, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="gerenciar LIKE '%$gerenciar%' ";
            $verif = true;
        }
        if(strcmp($idUsuario, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="id_usuario LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param){

        $o = new mGridPermissao();
        $o->setId($param[0]);
        $o->setIdColaborador($param[1]);
        $o->setIdFerramenta($param[2]);
        $o->setVisualizacao($param[3]);
        $o->setEscrita($param[4]);
        $o->setEdicao($param[5]);
        $o->setExclusao($param[6]);
        $o->setGerenciar($param[7]);
        $o->setIdUsuario($param[8]);

        return $o;

    }
}