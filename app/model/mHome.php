<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 03/10/2018
 * Time: 10:56
 */
require_once('lib/appConexao.php');
require_once('lib/appFunction.php');
class mHome extends appConexao
{
    public function listNoticiasByLinha(){

    }

    public function getNomeLinhaById($linha)
    {
        $sql = "SELECT NOME FROM LINHA WHERE ID_LINHA = $linha";
        return $this->executarQueryArray($sql);
    }

    public function listPilaresBySetor($setor){
        $sql = "SELECT PILAR, M00, EVOL_MES,DECIMAL FROM VW_DSH_PAINEL  WHERE PRODUTO = '(TOTAL)' AND SETOR = '$setor'";
        return $this->executarQueryArray($sql);
    }

    public function listInformativos($setor){
        $sql = "SELECT TITULO, TEXTO FROM INFORMATIVO JOIN INFORMATIVO_LINHA ON INFORMATIVO_LINHA.ID_INFORMATIVO = INFORMATIVO.ID_INFORMATIVO WHERE INFORMATIVO_LINHA.ID_LINHA = $setor AND INFORMATIVO.ATIVO = 1 ORDER BY ORDEM";
        return $this->executarQueryArray($sql);
    }
    public function listNoticias($setor){
        $sql = "SELECT * FROM NOTICIA";
        return $this->executarQueryArray($sql);
    }
}