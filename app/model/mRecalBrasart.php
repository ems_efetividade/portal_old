<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 23/05/2019
 * Time: 17:25
 */
require_once('lib/appConexao.php');

class mRecalBrasart extends appConexao
{
    public $ID;
    public $NUMERO_PEDIDO;
    public $EMPRESA;
    public $NUMERO_NOTA;
    public $HORA_EM;
    public $ORDEM_VENDA;
    public $REMESSA;
    public $RAZAO_SOCIAL;
    public $CLIENTE;
    public $CIDADE;
    public $UF;
    public $MATERIAL;
    public $TRANSPORTADORA;
    public $CANAL_DIST;
    public $DESCRICAO_MATERIAL;
    public $UN;
    public $SA;
    public $SETOR_DE_AMOSTRA;
    public $LOTE;
    public $DATA_EMISSAO;
    public $PU_UNIT;
    public $QUANTIDADE;
    public $VALOR_BRUTO;
    public $VALOR_MERCADO;
    public $DESC_IMP;
    public $DESC_COM;
    public $VALOR_ICMS;
    public $VALOR_IPI;
    public $VALOR_SUB_TRI;
    public $DESP_ACES;
    public $VL_TOT_NOTA;
    public $CPF;
    public $DATA_IN;

    /**
     * mRecalBrasart constructor.
     */
    public function __construct()
    {

    }

    public function salvarTermo($termo, $idUsuario, $arrayLotes)
    {
        $sql = "INSERT INTO TERMO_GERADO (FK_USER,TERMO,FK_LOTE) VALUES ('$idUsuario','$termo','$arrayLotes')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $arrayLotes = explode("-", $arrayLotes);
        foreach ($arrayLotes as $id) {
            $sql = "UPDATE TERMO_ANVISA SET STATUS = 2 WHERE ID = '$id'";
            $sql = utf8_decode($sql);
            $this->executar($sql);
        }
    }

    public function carregaTermo($id)
    {
        $sql = "SELECT TOP 1 * FROM TERMO_GERADO WHERE FK_USER = $id AND ASSINADO = 0";
        $sql = utf8_decode($sql);
        return $this->executarQueryArray($sql);
    }
    public function loadTermo($id)
    {
        $sql = "SELECT * FROM TERMO_GERADO WHERE ID = $id ";
        $sql = utf8_decode($sql);
        return $this->executarQueryArray($sql);
    }

    public function loadSetor($id)
    {
        $sql = "SELECT NOME, SETOR FROM vw_colaboradorSetor WHERE ID_colaborador = $id ";
        $sql = utf8_decode($sql);
        return $this->executarQueryArray($sql);
    }

    public function validarResposta($id_colaborador, $setor, $resposta, $documento, $date)
    {
        if($resposta==2){
            $sql = "UPDATE TERMO_GERADO SET ASSINADO = $resposta WHERE ID = $documento";
            $this->executar($sql);
            $sql = "SELECT FK_LOTE FROM TERMO_GERADO WHERE ID = $documento";
            $sql = utf8_decode($sql);
            $retorno  = $this->executarQueryArray($sql);
            $arrayLotes = $retorno[1][0];
            $arrayLotes = explode("-", $arrayLotes);
            foreach ($arrayLotes as $id) {
                $sql = "UPDATE TERMO_ANVISA SET STATUS = 1 WHERE ID = '$id'";
                $sql = utf8_decode($sql);
                $this->executar($sql);
            }
            return 1;
        }
        $chave = md5($id_colaborador . $setor . $documento . $date);
        $sql = "UPDATE TERMO_GERADO SET ASSINADO = $resposta, ASSINATURA = '$chave' WHERE ID = $documento";
        $this->executar($sql);
        $sql = "SELECT FK_LOTE FROM TERMO_GERADO WHERE ID = $documento";
        $sql = utf8_decode($sql);
        $retorno  = $this->executarQueryArray($sql);
        $arrayLotes = $retorno[1][0];
        $arrayLotes = explode("-", $arrayLotes);
        foreach ($arrayLotes as $id) {
            $sql = "UPDATE TERMO_ANVISA SET STATUS = 3 WHERE ID = '$id'";
            $sql = utf8_decode($sql);
            $this->executar($sql);
        }
        return $chave;
    }


    public function listtermo()
    {
        $sql = "SELECT * FROM TERMO_GERADO WHERE assinado = 1  ORDER BY ID DESC";
        $retorno = $this->executarQueryArray($sql);
        if (!is_array($retorno[1]))
            return 0;
        return $retorno;
    }

    public function listtermoByIdColaborador($id)
    {
        $cpf = $this->getCPFColaborador($id);
        $sql = "SELECT * FROM TERMO_ANVISA WHERE CPF = '$cpf' AND STATUS = 1";
        $retorno = $this->executarQueryArray($sql);
        if (!is_array($retorno[1]))
            return 0;
        return $retorno;
    }

    public function getCPFColaborador($id)
    {
        $sql = "SELECT CPf FROM COLABORADOR WHERE ID_COLABORADOR = '$id'";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function dadosCPFColaborador($id)
    {
        $sql = "SELECT * FROM VW_COLABORADOR WHERE ID_COLABORADOR = '$id'";
        $retorno = $this->executarQueryArray($sql);
        return $retorno;
    }



//    public function montaObject($array)
//    {
//        return $array;
//        $objeto = new mRecalBrasart();
//        $objeto->setNUMEROPEDIDO();
//        $objeto->setEMPRESA();
//        $objeto->setNU();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//        $objeto->setNUMERONOTA();
//    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->ID;
    }

    /**
     * @param mixed $ID
     */
    public function setID($ID)
    {
        $this->ID = $ID;
    }

    /**
     * @return mixed
     */
    public function getNUMEROPEDIDO()
    {
        return $this->NUMERO_PEDIDO;
    }

    /**
     * @param mixed $NUMERO_PEDIDO
     */
    public function setNUMEROPEDIDO($NUMERO_PEDIDO)
    {
        $this->NUMERO_PEDIDO = $NUMERO_PEDIDO;
    }

    /**
     * @return mixed
     */
    public function getEMPRESA()
    {
        return $this->EMPRESA;
    }

    /**
     * @param mixed $EMPRESA
     */
    public function setEMPRESA($EMPRESA)
    {
        $this->EMPRESA = $EMPRESA;
    }

    /**
     * @return mixed
     */
    public function getNUMERONOTA()
    {
        return $this->NUMERO_NOTA;
    }

    /**
     * @param mixed $NUMERO_NOTA
     */
    public function setNUMERONOTA($NUMERO_NOTA)
    {
        $this->NUMERO_NOTA = $NUMERO_NOTA;
    }

    /**
     * @return mixed
     */
    public function getHORAEM()
    {
        return $this->HORA_EM;
    }

    /**
     * @param mixed $HORA_EM
     */
    public function setHORAEM($HORA_EM)
    {
        $this->HORA_EM = $HORA_EM;
    }

    /**
     * @return mixed
     */
    public function getORDEMVENDA()
    {
        return $this->ORDEM_VENDA;
    }

    /**
     * @param mixed $ORDEM_VENDA
     */
    public function setORDEMVENDA($ORDEM_VENDA)
    {
        $this->ORDEM_VENDA = $ORDEM_VENDA;
    }

    /**
     * @return mixed
     */
    public function getREMESSA()
    {
        return $this->REMESSA;
    }

    /**
     * @param mixed $REMESSA
     */
    public function setREMESSA($REMESSA)
    {
        $this->REMESSA = $REMESSA;
    }

    /**
     * @return mixed
     */
    public function getRAZAOSOCIAL()
    {
        return $this->RAZAO_SOCIAL;
    }

    /**
     * @param mixed $RAZAO_SOCIAL
     */
    public function setRAZAOSOCIAL($RAZAO_SOCIAL)
    {
        $this->RAZAO_SOCIAL = $RAZAO_SOCIAL;
    }

    /**
     * @return mixed
     */
    public function getCLIENTE()
    {
        return $this->CLIENTE;
    }

    /**
     * @param mixed $CLIENTE
     */
    public function setCLIENTE($CLIENTE)
    {
        $this->CLIENTE = $CLIENTE;
    }

    /**
     * @return mixed
     */
    public function getCIDADE()
    {
        return $this->CIDADE;
    }

    /**
     * @param mixed $CIDADE
     */
    public function setCIDADE($CIDADE)
    {
        $this->CIDADE = $CIDADE;
    }

    /**
     * @return mixed
     */
    public function getUF()
    {
        return $this->UF;
    }

    /**
     * @param mixed $UF
     */
    public function setUF($UF)
    {
        $this->UF = $UF;
    }

    /**
     * @return mixed
     */
    public function getMATERIAL()
    {
        return $this->MATERIAL;
    }

    /**
     * @param mixed $MATERIAL
     */
    public function setMATERIAL($MATERIAL)
    {
        $this->MATERIAL = $MATERIAL;
    }

    /**
     * @return mixed
     */
    public function getTRANSPORTADORA()
    {
        return $this->TRANSPORTADORA;
    }

    /**
     * @param mixed $TRANSPORTADORA
     */
    public function setTRANSPORTADORA($TRANSPORTADORA)
    {
        $this->TRANSPORTADORA = $TRANSPORTADORA;
    }

    /**
     * @return mixed
     */
    public function getCANALDIST()
    {
        return $this->CANAL_DIST;
    }

    /**
     * @param mixed $CANAL_DIST
     */
    public function setCANALDIST($CANAL_DIST)
    {
        $this->CANAL_DIST = $CANAL_DIST;
    }

    /**
     * @return mixed
     */
    public function getDESCRICAOMATERIAL()
    {
        return $this->DESCRICAO_MATERIAL;
    }

    /**
     * @param mixed $DESCRICAO_MATERIAL
     */
    public function setDESCRICAOMATERIAL($DESCRICAO_MATERIAL)
    {
        $this->DESCRICAO_MATERIAL = $DESCRICAO_MATERIAL;
    }

    /**
     * @return mixed
     */
    public function getUN()
    {
        return $this->UN;
    }

    /**
     * @param mixed $UN
     */
    public function setUN($UN)
    {
        $this->UN = $UN;
    }

    /**
     * @return mixed
     */
    public function getSA()
    {
        return $this->SA;
    }

    /**
     * @param mixed $SA
     */
    public function setSA($SA)
    {
        $this->SA = $SA;
    }

    /**
     * @return mixed
     */
    public function getSETORDEAMOSTRA()
    {
        return $this->SETOR_DE_AMOSTRA;
    }

    /**
     * @param mixed $SETOR_DE_AMOSTRA
     */
    public function setSETORDEAMOSTRA($SETOR_DE_AMOSTRA)
    {
        $this->SETOR_DE_AMOSTRA = $SETOR_DE_AMOSTRA;
    }

    /**
     * @return mixed
     */
    public function getLOTE()
    {
        return $this->LOTE;
    }

    /**
     * @param mixed $LOTE
     */
    public function setLOTE($LOTE)
    {
        $this->LOTE = $LOTE;
    }

    /**
     * @return mixed
     */
    public function getDATAEMISSAO()
    {
        return $this->DATA_EMISSAO;
    }

    /**
     * @param mixed $DATA_EMISSAO
     */
    public function setDATAEMISSAO($DATA_EMISSAO)
    {
        $this->DATA_EMISSAO = $DATA_EMISSAO;
    }

    /**
     * @return mixed
     */
    public function getPUUNIT()
    {
        return $this->PU_UNIT;
    }

    /**
     * @param mixed $PU_UNIT
     */
    public function setPUUNIT($PU_UNIT)
    {
        $this->PU_UNIT = $PU_UNIT;
    }

    /**
     * @return mixed
     */
    public function getQUANTIDADE()
    {
        return $this->QUANTIDADE;
    }

    /**
     * @param mixed $QUANTIDADE
     */
    public function setQUANTIDADE($QUANTIDADE)
    {
        $this->QUANTIDADE = $QUANTIDADE;
    }

    /**
     * @return mixed
     */
    public function getVALORBRUTO()
    {
        return $this->VALOR_BRUTO;
    }

    /**
     * @param mixed $VALOR_BRUTO
     */
    public function setVALORBRUTO($VALOR_BRUTO)
    {
        $this->VALOR_BRUTO = $VALOR_BRUTO;
    }

    /**
     * @return mixed
     */
    public function getVALORMERCADO()
    {
        return $this->VALOR_MERCADO;
    }

    /**
     * @param mixed $VALOR_MERCADO
     */
    public function setVALORMERCADO($VALOR_MERCADO)
    {
        $this->VALOR_MERCADO = $VALOR_MERCADO;
    }

    /**
     * @return mixed
     */
    public function getDESCIMP()
    {
        return $this->DESC_IMP;
    }

    /**
     * @param mixed $DESC_IMP
     */
    public function setDESCIMP($DESC_IMP)
    {
        $this->DESC_IMP = $DESC_IMP;
    }

    /**
     * @return mixed
     */
    public function getDESCCOM()
    {
        return $this->DESC_COM;
    }

    /**
     * @param mixed $DESC_COM
     */
    public function setDESCCOM($DESC_COM)
    {
        $this->DESC_COM = $DESC_COM;
    }

    /**
     * @return mixed
     */
    public function getVALORICMS()
    {
        return $this->VALOR_ICMS;
    }

    /**
     * @param mixed $VALOR_ICMS
     */
    public function setVALORICMS($VALOR_ICMS)
    {
        $this->VALOR_ICMS = $VALOR_ICMS;
    }

    /**
     * @return mixed
     */
    public function getVALORIPI()
    {
        return $this->VALOR_IPI;
    }

    /**
     * @param mixed $VALOR_IPI
     */
    public function setVALORIPI($VALOR_IPI)
    {
        $this->VALOR_IPI = $VALOR_IPI;
    }

    /**
     * @return mixed
     */
    public function getVALORSUBTRI()
    {
        return $this->VALOR_SUB_TRI;
    }

    /**
     * @param mixed $VALOR_SUB_TRI
     */
    public function setVALORSUBTRI($VALOR_SUB_TRI)
    {
        $this->VALOR_SUB_TRI = $VALOR_SUB_TRI;
    }

    /**
     * @return mixed
     */
    public function getDESPACES()
    {
        return $this->DESP_ACES;
    }

    /**
     * @param mixed $DESP_ACES
     */
    public function setDESPACES($DESP_ACES)
    {
        $this->DESP_ACES = $DESP_ACES;
    }

    /**
     * @return mixed
     */
    public function getVLTOTNOTA()
    {
        return $this->VL_TOT_NOTA;
    }

    /**
     * @param mixed $VL_TOT_NOTA
     */
    public function setVLTOTNOTA($VL_TOT_NOTA)
    {
        $this->VL_TOT_NOTA = $VL_TOT_NOTA;
    }

    /**
     * @return mixed
     */
    public function getCPF()
    {
        return $this->CPF;
    }

    /**
     * @param mixed $CPF
     */
    public function setCPF($CPF)
    {
        $this->CPF = $CPF;
    }

    /**
     * @return mixed
     */
    public function getDATAIN()
    {
        return $this->DATA_IN;
    }

    /**
     * @param mixed $DATA_IN
     */
    public function setDATAIN($DATA_IN)
    {
        $this->DATA_IN = $DATA_IN;
    }
}