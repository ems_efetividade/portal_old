<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 10/07/2018
 * Time: 16:52
 */
require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class mRelatorios extends appConexao
{

    public function getIdByCpf($cpf)
    {
        $sql = "SElECT ID_COLABORADOR FROM COLABORADOR WHERE CPF = '$cpf'";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getNivelAcessoPasta($idPasta)
    {
        $sql = "SELECT NIVEL FROM RELATORIO_PERMISSAO WHERE ID_DIRETORIO = $idPasta AND ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function salvarLog($id)
    {
        $usuario = $_SESSION['id_colaborador'];
        $pasta = $_SESSION['pasta_atual'];
        $sql = "INSERT INTO RELATORIO_lOG (ID_ARQUIVO,ID_PASTA,ID_USUARIO) VALUES ($id,$pasta,$usuario)";
        $this->executar($sql);
    }

    public function listarPermissoesById($id)
    {
        session_start();
        $empresa = $_SESSION['id_empresa'];
        $sql = "SELECT ID_USUARIO, ID_PERFIL,ID_LINHA,ID_UNIDADE,ID_EMPRESA from RELATORIO_PERMISSAO where ID_DIRETORIO = $id AND ID_EMPRESA = $empresa AND ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function excluirPasta($parametro)
    {
        session_start();
        $empresa = $_SESSION['id_empresa'];
        $sql = "UPDATE RELATORIO_PERMISSAO SET ATIVO = 0 WHERE ID_DIRETORIO = $parametro ";
        $this->executar($sql);
        $sql = "UPDATE RELATORIO_DIRETORIO SET ATIVO = 0 WHERE ID = $parametro";
        $this->executar($sql);
    }

    public function getSetorLinha($parametro)
    {
        $sql = "SELECT setor.SETOR, setor.ID_SETOR, LINHA.NOME as linha, COLABORADOR.NOME as colaborador from SETOR left join LINHA on LINHA.ID_LINHA=SETOR.ID_LINHA inner join COLABORADOR_SETOR ON COLABORADOR_SETOR.ID_SETOR = SETOR.ID_SETOR inner join COLABORADOR on COLABORADOR_SETOR.ID_COLABORADOR = COLABORADOR.ID_COLABORADOR where SETOR.SETOR = '$parametro'";
        return $retorno = $this->executarQueryArray($sql);
    }

    public function relacionarPastaUsuario($id, $pasta, $id_colaborador, $setor)
    {
        $id_colaborador_sessao = $_SESSION["id_colaborador"];
        $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_USUARIO,ID_RESPONSAVEL,ID_DIRETORIO)";
        $sql .= " VALUES ($id,$id_colaborador_sessao,$id_colaborador_sessao,$pasta)";
        $this->executar($sql);
        return;
    }

    public function relacionarPasta($id, $pasta, $id_colaborador, $setor)
    {
        session_start();
        $empresa = $_SESSION['id_empresa'];
        $id_colaborador_sessao = $_SESSION["id_colaborador"];
        if ($id_colaborador > 0) {
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_USUARIO,ID_RESPONSAVEL,ID_DIRETORIO)";
            $sql .= " VALUES ($id,$id_colaborador,$id_colaborador_sessao,$pasta)";
            $this->executar($sql);
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_USUARIO,ID_RESPONSAVEL,ID_DIRETORIO)";
            $sql .= " VALUES ($id,$id_colaborador_sessao,$id_colaborador_sessao,$pasta)";
            $this->executar($sql);
            return;
        }
        if ($setor > 0) {
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,SETOR,ID_RESPONSAVEL,ID_DIRETORIO)";
            $sql .= " VALUES ($id,'$setor',$id_colaborador_sessao,$pasta)";
            $this->executar($sql);
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_USUARIO,ID_RESPONSAVEL,ID_DIRETORIO)";
            $sql .= " VALUES ($id,$id_colaborador_sessao,$id_colaborador_sessao,$pasta)";
            $this->executar($sql);
            return;
        }
        $sql = "SELECT ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO FROM RELATORIO_PERMISSAO ";
        $sql .= " WHERE ID_DIRETORIO = $pasta AND ID_ARQUIVO = 0 AND ID_EMPRESA = $empresa AND ATIVO = 1";
        $lista_permissoes = $this->executarQueryArray($sql);
        foreach ($lista_permissoes as $permissoes) {
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_PERFIL,ID_LINHA,ID_USUARIO)";
            $sql .= " VALUES ($id,$pasta,$permissoes[0],$permissoes[1],$permissoes[3],$permissoes[2],$id_colaborador_sessao)";
            $this->executar($sql);
        }
        $sql = "SELECT DISTINCT (ID_USUARIO) FROM RELATORIO_PERMISSAO ";
        $sql .= " WHERE ID_DIRETORIO = $pasta AND ID_EMPRESA = $empresa AND ID_UNIDADE = 0 ";
        $sql .= " AND ID_LINHA = 0 AND ID_PERFIL = 0 AND ID_USUARIO > 0 AND ATIVO = 1 AND ID_ARQUIVO = 0 ";
        $lista_permissoes = $this->executarQueryArray($sql);
        foreach ($lista_permissoes as $permissoes) {
            $sql = "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_DIRETORIO,ID_EMPRESA,ID_USUARIO,ID_RESPONSAVEL)";
            $sql .= " VALUES ($id,$pasta,$empresa,$permissoes[0],$id_colaborador_sessao)";
            $this->executar($sql);
        }
    }

    PUBLIC function validaTipo($tipo)
    {
        $sql = "select FORMATO FROM RELATORIO_FORMATO WHERE TIPO = '$tipo'";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getNomePastaById($idPasta)
    {
        $sql = "select NOME FROM RELATORIO_DIRETORIO WHERE ID = $idPasta";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function listRelatoriosByPasta($pasta)
    {
        session_start();
        $id_usuario = $_SESSION['id_colaborador'];
        $setor = $_SESSION['setor'];
        $linha = $_SESSION['id_linha'];
        $bu = $_SESSION['id_un_negocio'];
        $empresa = $_SESSION['id_empresa'];
        $perfil = $_SESSION['id_perfil'];
        $sql = "SELECT DISTINCT(RA.ID), RA.METADADOS, RA.NOME, RA.TIPO, RA.DATA, RD.NOME AS PASTA ";
        $sql .= "FROM RELATORIO_PERMISSAO RP ";
        $sql .= "INNER JOIN RELATORIO_ARQUIVO RA ON RP.ID_ARQUIVO = RA.ID ";
        $sql .= "INNER JOIN RELATORIO_DIRETORIO RD ON RP .ID_DIRETORIO = RD.ID ";
        if ($_SESSION['nivel_admin'] == 2) {
            $sql .= "WHERE ID_DIRETORIO = $pasta AND RP.ATIVO = 1 AND rp.ID_EMPRESA = $empresa";
            $sql .= "OR SETOR <> 0 AND ID_DIRETORIO = $pasta AND RP.ATIVO = 1";
            return $retorno = $this->executarQueryArray($sql);
        }
        $sql .= "WHERE ID_DIRETORIO = $pasta AND RP.ATIVO = 1 AND rp.ID_USUARIO = 0 AND rp.SETOR = 0";
        $sql .= " OR ID_DIRETORIO = $pasta AND RP.ATIVO = 1 AND rp.ID_USUARIO = $id_usuario";
        $sql .= " OR ID_DIRETORIO = $pasta AND RP.ATIVO = 1 AND rp.SETOR = '$setor'";
        $sql .= " OR ID_DIRETORIO = $pasta AND RP.ATIVO = 1 AND rp.ID_EMPRESA = $empresa AND rp.ID_UNIDADE = $bu AND rp.ID_LINHA = $linha AND rp.ID_PERFIL = $perfil";
        return $retorno = $this->executarQueryArray($sql);
    }

    public function listRelatoriosByCpf($cpf)
    {

    }

    public function excluir($id)
    {
        $sql = "UPDATE RELATORIO_ARQUIVO SET ATIVO = 0 WHERE ID = $id; UPDATE RELATORIO_PERMISSAO SET ATIVO = 0 WHERE ID_ARQUIVO = $id";
        return $this->executar($sql);
    }

    public function atualizaPasta($nome, $id)
    {
        $sql = "UPDATE RELATORIO_DIRETORIO SET RELATORIO_DIRETORIO.NOME = '$nome' WHERE ID = $id";
        $this->executar($sql);
    }

    public function atualizaArqu($nome, $id)
    {
        $sql = "UPDATE RELATORIO_ARQUIVO SET RELATORIO_ARQUIVO.NOME = '$nome' WHERE ID = $id";
        $this->executar($sql);
    }

    public function listPastasByCpf($cpf)
    {

    }

    public function listPastas($raiz)
    {
        session_start();
        $un = $_SESSION["id_un_negocio"];
        $emp = $_SESSION["id_empresa"];
        $linha = $_SESSION["id_linha"];
        $perfil = $_SESSION["id_perfil"];
        $id_colaborador = $_SESSION["id_colaborador"];
        if ($_SESSION['nivel_admin'] == 2) {
            $sql = "SELECT DISTINCT (RD.ID),NOME FROM RELATORIO_DIRETORIO RD INNER JOIN RELATORIO_PERMISSAO RP on RP.ID_DIRETORIO = RD.ID WHERE ID_SUPER = $raiz AND ID_UNIDADE = $un AND RP.ID_EMPRESA = $emp and RP.ATIVO = 1 order by NOME asc ";
        } else {
            $sql = "SELECT DISTINCT (RD.ID),NOME FROM RELATORIO_DIRETORIO RD INNER JOIN RELATORIO_PERMISSAO RP on RP.ID_DIRETORIO = RD.ID WHERE ID_SUPER = $raiz AND Rp.ATIVO = 1 AND RP.ID_EMPRESA = $emp AND ID_UNIDADE = $un AND ID_LINHA = $linha AND ID_PERFIL = $perfil or ID_USUARIO = $id_colaborador and id_super =$raiz and RP.ATIVO = 1 order by NOME asc";
        }
        return $retorno = $this->executarQueryArray($sql);
    }

    public function listSubPastaByPasta($pasta)
    {
        session_start();
        return $this->listPastas($pasta);
//        $perfil = $_SESSION["id_perfil"];
//        $id_colaborador = $_SESSION["id_colaborador"];
//        if ($_SESSION['nivel_admin'] == 2) {
//            $sql = "SELECT ID,NOME FROM RELATORIO_DIRETORIO WHERE ID_SUPER = $pasta AND ATIVO = 1 ";
//        } else {
//            $sql = "SELECT DISTINCT (RD.ID), NOME FROM RELATORIO_DIRETORIO rd inner join RELATORIO_PERMISSAO rp on rd.ID = rp.ID_DIRETORIO WHERE ID_SUPER = $pasta AND rp.ATIVO = 1 and rp.ID_PERFIL = $perfil or (rp.ID_USUARIO = $id_colaborador and ID_SUPER = $pasta and RP.ATIVO = 1)";
//        }
//        return $retorno = $this->executarQueryArray($sql);
    }

    public function criarPasta()
    {
        $nome = $_POST['nome_pasta'];
        $super = $_POST['pasta_atual'];
        $usuario = $_POST['id_colaborador'];
        $nome = utf8_decode($nome);
        $sql = "INSERT INTO RELATORIO_DIRETORIO (NOME, ID_RESPONSAVEL,ID_SUPER) VALUES ('$nome',$usuario,$super); select top 1 @@identity from RELATORIO_DIRETORIO";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function permissoesPasta($id)
    {
        $arquivos = "SELECT distinct(ID_ARQUIVO) FROM  RELATORIO_PERMISSAO WHERE ID_DIRETORIO = $id AND ID_ARQUIVO > 0 AND ATIVO = 1";
        $arquivos = $this->executarQueryArray($arquivos);
        $sql = "UPDATE RELATORIO_PERMISSAO SET ATIVO = 0 WHERE ID_DIRETORIO = $id";
        $this->executar($sql);
        $empresas = $_POST["Empresa"];
        $unidades = $_POST["Unidade"];
        $linhas = $_POST["Linha"];
        $perfil = $_POST["Perfil"];
        $userPermitidos = explode('-', $_POST["userPermitidos"]);
        $responsavel = $_SESSION["id_colaborador"];
        $sql = "";
        foreach ($empresas as $empresa) {
            foreach ($unidades as $unidade) {
                foreach ($linhas as $linha) {
                    if (is_array($perfil)) {
                        foreach ($perfil as $id_perfil) {
                            $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                            $sql .= "VALUES ($id,$empresa,$unidade,$linha,$id_perfil,0,$responsavel); ";
                            foreach ($arquivos as $id_arquivo) {
                                $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                                $sql .= "VALUES ($id_arquivo[0],$id,$empresa,$unidade,$linha,$id_perfil,0,$responsavel); ";
                            }
                        }
                    } else {
                        $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                        $sql .= "VALUES ($id,$empresa,$unidade,$linha,0,0,$responsavel); ";
                        foreach ($arquivos as $id_arquivo) {
                            $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                            $sql .= "VALUES ($id_arquivo[0],$id,$empresa,$unidade,$linha,0,0,$responsavel); ";
                        }
                    }
                }
            }
        }
        if ($userPermitidos[0] != '') {
            foreach ($userPermitidos as $user) {
                $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                $sql .= "VALUES ($id,0,0,0,0,$user,$responsavel); ";
                foreach ($arquivos as $id_arquivo) {
                    $sql .= "INSERT INTO RELATORIO_PERMISSAO (ID_ARQUIVO,ID_DIRETORIO,ID_EMPRESA,ID_UNIDADE,ID_LINHA,ID_PERFIL,ID_USUARIO,ID_RESPONSAVEL) ";
                    $sql .= "VALUES ($id_arquivo[0],$id,0,0,0,0,$user,$responsavel); ";
                }
            }
        }
        $this->executar($sql);
    }

    public function salvarArquivos($nome, $tipo, $metadados, $idResponsavel, $tamanho)
    {
        $nome = utf8_decode($nome);
        $tipo = utf8_decode($tipo);
        $sql = "INSERT INTO RELATORIO_ARQUIVO (NOME,TIPO,ID_RESPONSAVEL,METADADOS,TAMANHO)";
        $sql .= "VALUES ('$nome','$tipo',$idResponsavel,$metadados,$tamanho);select top 1 @@identity from RELATORIO_ARQUIVO";
        return $this->executarQueryArray($sql);
    }

    public function carregarPermissoes($id_empresa, $id_un_negocio)
    {
        $sql = "EXEC PROC_RELATORIO_PERMISSAO $id_empresa,$id_un_negocio";
        return $this->executarQueryArray($sql);
    }

    public function listUsuariosByNomeSetorMatricula($parametro)
    {
        $sql = "SELECT top 8 SETOR,NOME,EMAIL,NOME_SETOR,MATRICULA,ID_COLABORADOR from vw_colaboradorSetor where NOME LIKE '%$parametro%' or SETOR LIKE '%$parametro%' or MATRICULA LIKE '%$parametro%' or EMAIL LIKE '%$parametro%' ";
        return $this->executarQueryArray($sql);
    }

    public function loadUsuariosByID($parametro)
    {
        $sql = "SELECT SETOR,NOME,EMAIL,NOME_SETOR,MATRICULA,ID_COLABORADOR from vw_colaboradorSetor where ID_COLABORADOR = '$parametro'";
        return $this->executarQueryArray($sql);
    }

}