<?php
require_once('lib/appConexao.php');

class mSenhaAdmin extends appConexao
{

    private $id;
    private $senha;
    private $fkUsuario;
    private $dataExpirar;
    private $dataIn;

    public function __construct()
    {
        $this->id;
        $this->senha;
        $this->fkUsuario;
        $this->dataExpirar;
        $this->dataIn;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getSenha()
    {
        return $this->senha;
    }

    public function getFkUsuario()
    {
        return $this->fkUsuario;
    }

    public function getSituacaoSenha()
    {
        $data = $this->dataExpirar;
        $today = date("Y-m-d H:i:s");
        $date_time = new DateTime($today);
        $data = new DateTime($data);
        if ($data < $date_time)
            return false;
        return true;
    }

    public function getNomeUsuarioById()
    {
        $id = $this->getFkUsuario();
        $sql = "SELECT NOME from vw_colaboradorSetor where ID_COLABORADOR = '" . $id . "'";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['NOME'];
    }


    public function getDataExpirar()
    {
        return $this->dataExpirar;
    }

    public function getDataIn()
    {
        return $this->dataIn;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setSenha($Senha)
    {
        $this->senha = $Senha;
    }

    public function setFkUsuario($FkUsuario)
    {
        $this->fkUsuario = $FkUsuario;
    }

    public function setDataExpirar($DataExpirar)
    {
        $this->dataExpirar = $DataExpirar;
    }

    public function setDataIn($DataIn)
    {
        $this->dataIn = $DataIn;
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from SENHA_ADMIN";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($senha, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "SENHA LIKE '%$senha%' ";
            $verif = true;
        }
        if (strcmp($fkUsuario, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_USUARIO LIKE '%$fkUsuario%' ";
            $verif = true;
        }
        if (strcmp($dataExpirar, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_EXPIRAR IN ('$dataExpirar') ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];

        $sql = "INSERT INTO SENHA_ADMIN ([SENHA],[FK_USUARIO],[DATA_EXPIRAR])";
        $sql .= " VALUES ('$senha','$fkUsuario','$dataExpirar')";
        $this->executar($sql);
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == '+') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == '-') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (" . $sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= " . $atual;
        $paginacao .= " AND row <= " . $max . " ";
        return $paginacao;
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM SENHA_ADMIN WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = '$id' ";
            $verif = true;
        }
        if (strcmp($senha, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "SENHA = '$senha' ";
            $verif = true;
        }
        if (strcmp($fkUsuario, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "FK_USUARIO = '$fkUsuario' ";
            $verif = true;
        }
        if (strcmp($dataExpirar, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_EXPIRAR IN ('$dataExpirar') ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "SENHA ";
        $sql .= ",";
        $sql .= "FK_USUARIO ";
        $sql .= ",";
        $sql .= "DATA_EXPIRAR ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM SENHA_ADMIN ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($senha, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "SENHA LIKE '%$senha%' ";
            $verif = true;
        }
        if (strcmp($fkUsuario, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_USUARIO LIKE '%$fkUsuario%' ";
            $verif = true;
        }
        if (strcmp($dataExpirar, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_EXPIRAR IN ('$dataExpirar') ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  SENHA_ADMIN";
        $sql .= " SET ";
        $sql .= "SENHA = '$senha' ";
        $sql .= " , ";
        $sql .= "FK_USUARIO = '$fkUsuario' ";
        $sql .= " , ";
        $sql .= "DATA_EXPIRAR = '$dataExpirar' ";
        $sql .= " , ";
        $sql .= "DATA_IN = '$dataIn' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $senha = $_POST['senha'];
        $fkUsuario = $_POST['fkusuario'];
        $dataExpirar = $_POST['dataexpirar'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "SENHA ";
        $sql .= ",";
        $sql .= "FK_USUARIO ";
        $sql .= ",";
        $sql .= "DATA_EXPIRAR ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM SENHA_ADMIN ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($senha, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "SENHA LIKE '%$senha%' ";
            $verif = true;
        }
        if (strcmp($fkUsuario, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FK_USUARIO LIKE '%$fkUsuario%' ";
            $verif = true;
        }
        if (strcmp($dataExpirar, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_EXPIRAR IN ('$dataExpirar') ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {

        $o = new mSenhaAdmin();
        $o->setId($param[0]);
        $o->setSenha($param[1]);
        $o->setFkUsuario($param[2]);
        $o->setDataExpirar($param[3]);
        $o->setDataIn($param[4]);

        return $o;

    }
}