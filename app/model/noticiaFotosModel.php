<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class noticiaFotosModel extends appConexao {

	private $idFoto;
	private $idNoticia;
	private $foto;
	
	public function noticiaFotosModel() {
		$this->idFoto = 0;
		$this->idNoticia = 0;
		$this->foto = '';
	}
	
	public function setIdFoto($valor) {
		$this->idFoto = $valor;	
	}
	
	public function setIdNoticia($valor) {
		$this->idNoticia = $valor;	
	}
	
	public function setFoto($valor) {
		$this->foto = $valor;	
	}



	public function getIdFoto() {
		return $this->idFoto;	
	}
	
	public function getIdNoticia() {
		return $this->idNoticia;	
	}
	
	public function getFoto() {
		return $this->foto;	
	}

	
	
	
	public function salvar() {
		$query = "INSERT INTO [NOTICIA_FOTOS]
					   ([ID_NOTICIA]
					   ,[FOTO])
				 VALUES
					   (".$this->idNoticia."
					   ,'".$this->foto."')";	
		
		
		$this->executar($query);
	}
	
	public function excluir() {
		$fotoExcluir = new noticiaFotosModel();
		$fotoExcluir->setIdFoto($this->idFoto);
		$fotoExcluir->selecionar();
		
		$this->executar("DELETE FROM NOTICIA_FOTOS WHERE ID_FOTO = ".$this->idFoto);	
		
		$arquivo = appConf::caminho_fisico.appConf::folder_foto_noticia_fisico.$fotoExcluir->getFoto();
		if(file_exists($arquivo)) {
			unlink($arquivo);
		}
	}
	
	public function excluirTudo() {
		$rs = $this->executarQueryArray("SELECT * FROM NOTICIA_FOTOS WHERE ID_NOTICIA = ".$this->idNoticia);
		$this->executar("DELETE FROM NOTICIA_FOTOS WHERE ID_NOTICIA = ".$this->idNoticia);	
		
		for($i=0;$i<=count($rs);$i++) {
			if($rs[$i]['FOTO'] != "") {
				$arquivo = appConf::caminho_fisico.appConf::folder_foto_noticia_fisico.$rs[$i]['FOTO'];
				
				if(file_exists($arquivo)) {
					unlink($arquivo);
				}
			}
		}
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM NOTICIA_FOTOS WHERE ID_FOTO = ".$this->idFoto);	
		
		$this->idFoto = $rs[1]['ID_FOTO'];
		$this->idNoticia = $rs[1]['ID_NOTICIA'];
		$this->foto = $rs[1]['FOTO'];
	}
	
	public function listar() {
		$arrFotos = array();
		
		$rs = $this->executarQueryArray("SELECT * FROM NOTICIA_FOTOS WHERE ID_NOTICIA = ".$this->idNoticia);	
		
		for($i=1;$i<=count($rs);$i++) {
			$noticiaFoto = new noticiaFotosModel();
			$noticiaFoto->setIdFoto($rs[$i]['ID_FOTO']);
			$noticiaFoto->selecionar();
			$arrFotos[] = $noticiaFoto;
		}
		
		return $arrFotos;
	}
	
	
}

?>