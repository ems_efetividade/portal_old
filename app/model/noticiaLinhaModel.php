<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class noticiaLinhaModel extends appConexao {

	private $idLinha;
	private $idNoticia;
	
	public function noticiaLinhaModel() {
		$this->idLinha = 0;
		$this->idNoticia = 0;	
	}
	
	
	public function setIdNoticia($valor) {
		$this->idNoticia = $valor;	
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;	
	}
	


	public function getIdNoticia() {
		return $this->idNoticia;	
	}
	
	public function getIdLinha() {
		return $this->idLinha;	
	}


	public function salvar() {
		
	}
}