<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');
require_once('app/model/noticiaFotosModel.php');

class noticiaModel extends appConexao {

	private $idNoticia;
	private $dataPublicacao;
	private $imagem;
	private $titulo;
	private $autor;
	private $resumo;
	private $materia;
	private $visualizacao;
	private $ativo;
	private $localImagem;
	private $arrLinha;
	
	private $noticiaFotos;
	
	
	public function noticiaModel() {
		$this->idNoticia = 0;
		$this->dataPublicacao = '';
		$this->imagem = '';
		$this->titulo = '';
		$this->autor = '';
		$this->resumo = '';
		$this->materia = '';
		$this->visualizacao = 0;
		$this->ativo = '';
		$this->localImagem = 0;
		$this->arrLinha = array();
		
		$this->noticiaFotos = new noticiaFotosModel();
	}
	
	public function setIdNoticia($valor) {
		$this->idNoticia = $valor;	
	}
	
	public function setDataPublicacao($valor) {
		$this->dataPublicacao = $valor;	
	}
	
	public function setImagem($valor) {
		$this->imagem = $valor;	
	}
	
	public function setTitulo($valor) {
		$this->titulo = $valor;	
	}
	
	public function setAutor($valor) {
		$this->autor = $valor;	
	}
	
	public function setResumo($valor) {
		$this->resumo = $valor;	
	}
	
	public function setMateria($valor) {
		$this->materia = $valor;	
	}
	
	public function setVisualizacao($valor) {
		$this->visualizacao = $valor;	
	}
	
	public function setAtivo($valor) {
		$this->ativo = $valor;	
	}	
	
	public function setLocalImagem($valor) {
		$this->localImagem = $valor;	
	}			
	
	public function setArrLinha($valor) {
		$this->arrLinha[] = $valor;	
	}
	
	
	
	public function getIdNoticia() {
		return $this->idNoticia;	
	}
	
	public function getDataPublicacao() {
		return $this->dataPublicacao;	
	}
	
	public function getImagem() {
		return $this->imagem;	
	}
	
	public function getTitulo() {
		return utf8_decode($this->titulo);	
	}
	
	public function getAutor() {
		return utf8_decode($this->autor);	
	}
	
	public function getResumo() {
		return utf8_decode($this->resumo);	
	}
	
	public function getMateria() {
		return utf8_decode($this->materia);	
	}
	
	public function getVisualizacao() {
		return $this->visualizacao;	
	}
	
	public function getAtivo() {
		return $this->ativo;	
	}	
	
	public function getLocalImagem() {
		return $this->localImagem;	
	}		
	
	public function getArrLinha() {
		return $this->arrLinha;	
	}
	
	
	private function popularVariaveis($rs) {
		if(count($rs) > 0) {
		
		$this->idNoticia = $rs[1]['ID_NOTICIA'];
		$this->dataPublicacao = $rs[1]['DATA_PUBLICACAO'];
		$this->imagem = $rs[1]['IMAGEM'];
		$this->titulo = $rs[1]['TITULO'];
		$this->autor = $rs[1]['AUTOR'];
		$this->resumo = $rs[1]['RESUMO'];
		$this->materia = $rs[1]['MATERIA'];
		$this->visualizacao = $rs[1]['VISUALIZACAO'];
		$this->ativo = $rs[1]['ATIVO'];
		$this->localImagem = $rs[1]['LOCAL_IMAGEM'];
		
		$rs2 = $this->executarQueryArray("SELECT ID_LINHA FROM NOTICIA_LINHA WHERE ID_NOTICIA = ".$rs[1]['ID_NOTICIA']);
		
		for($i=1;$i<=count($rs2);$i++) {
			$this->arrLinha[] = $rs2[$i]['ID_LINHA'];
		}
		
		}
	}
	
	public function salvar() {
		
		if($this->idNoticia == 0) {

			$query = "INSERT INTO [NOTICIA]
						   ([DATA_PUBLICACAO]
						   ,[IMAGEM]
						   ,[TITULO]
						   ,[AUTOR]
						   ,[RESUMO]
						   ,[MATERIA]
						   ,[VISUALIZACAO]
						   ,[ATIVO]
						   ,[DATA_CADASTRO]
						   ,[LOCAL_IMAGEM])
					 VALUES
						   ('".appFunction::formatarData($this->dataPublicacao)."'
						   ,'".$this->imagem."'
						   ,'".$this->titulo."'
						   ,'".$this->autor."'
						   ,'".$this->resumo."'
						   ,'".$this->materia."'
						   ,".$this->visualizacao."
						   ,".$this->ativo."
						   ,'".date('Y-m-d H:i:s')."'
						   ,".$this->localImagem.")";
			
                       
			$this->executar($query);
			
			$rs = $this->executarQueryArray("SELECT MAX(ID_NOTICIA) AS ID_NOTICIA FROM [NOTICIA]");
			$idNoticia = $rs[1]['ID_NOTICIA'];
			
						   
				
		} else {
			$query = "UPDATE [NOTICIA]
					   SET [DATA_PUBLICACAO] = '".appFunction::formatarData($this->dataPublicacao)."'
						  ,[IMAGEM] = '".$this->imagem."'
						  ,[TITULO] = '".$this->titulo."'
						  ,[AUTOR] = '".$this->autor."'
						  ,[RESUMO] = '".$this->resumo."'
						  ,[MATERIA] = '".$this->materia."'
						  ,[ATIVO] = ".$this->ativo."
						  ,[LOCAL_IMAGEM] = ".$this->localImagem."
					 WHERE [ID_NOTICIA] = ".$this->idNoticia."";
			$this->executar($query);	
			
			$idNoticia = $this->idNoticia;
		}
		
		$this->executar("DELETE FROM [NOTICIA_LINHA] WHERE ID_NOTICIA = ".$idNoticia);
			
		for($i=0;$i<count($this->arrLinha);$i++) {

			
			$this->executar("INSERT INTO [NOTICIA_LINHA] 
                                        (ID_NOTICIA, ID_LINHA) 
                                        VALUES 
                                        (".$idNoticia.", ".$this->arrLinha[$i].")");
		}
		
		
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM NOTICIA WHERE ID_NOTICIA = ".$this->idNoticia);	
		$this->popularVariaveis($rs);
	}
	
	public function selecionarUltimaNoticia() {
		if(appFunction::dadoSessao('id_linha') == 0) {
			$query = "SELECT TOP 1 * FROM NOTICIA WHERE ATIVO = 1 ORDER BY ID_NOTICIA DESC";
		} else {
			$query = "SELECT TOP 1 * FROM vw_noticia WHERE ATIVO = 1 AND ID_LINHA = ".appFunction::dadoSessao('id_linha')." ORDER BY ID_NOTICIA DESC";
		}
		
		$rs = $this->executarQueryArray($query);	
		$this->popularVariaveis($rs);
	}
	
	public function excluir() {
		$this->executar('DELETE FROM NOTICIA WHERE ID_NOTICIA = '.$this->idNoticia);
		$this->executar('DELETE FROM NOTICIA_LINHA WHERE ID_NOTICIA = '.$this->idNoticia);	
		
		$this->noticiaFotos->setIdNoticia($this->idNoticia);
		$this->noticiaFotos->excluirTudo();
	}
	
	public function listar($max=0, $ativo="") {
		$arrNoticia = array();

		$idLinha = appFunction::dadoSessao('id_linha');

		if($idLinha == 0) {
			if($ativo == "") {
				$query = "SELECT * FROM NOTICIA ORDER BY ID_NOTICIA DESC";
			} else {
				if($max == 0) {
					$query = "SELECT * FROM NOTICIA WHERE ATIVO = 1 ORDER BY ID_NOTICIA DESC";
				} else {
					$query = "SELECT TOP ".$max." * FROM NOTICIA WHERE ATIVO = 1 ORDER BY ID_NOTICIA DESC";
				}
			}
		} else {
			
			if($max == 0) {
				$query = "SELECT * FROM vw_noticia WHERE ID_LINHA = ".$idLinha." ORDER BY ID_NOTICIA DESC";
			} else {
				$query = "SELECT TOP ".$max." * FROM vw_noticia WHERE ID_LINHA = ".$idLinha." ORDER BY ID_NOTICIA DESC";
			}

		}

		
		$rs = $this->executarQueryArray($query);	
		
		for($i=1;$i<=count($rs);$i++) {
			$noticia = new noticiaModel();
			$noticia->setIdNoticia($rs[$i]['ID_NOTICIA']);
			$noticia->selecionar();
			$arrNoticia[] = $noticia;
		}
		
		return $arrNoticia;
	}
	
	public function listarMaisLidos($max=5) {
		
		$idLinha = appFunction::dadoSessao('id_linha');

		if($idLinha == 0) {
		  if($max == 0) {
			  $query = "SELECT * FROM NOTICIA WHERE ATIVO = 1 ORDER BY VISUALIZACAO DESC";
		  } else {
			  $query = "SELECT TOP ".$max." * FROM NOTICIA WHERE ATIVO = 1 ORDER BY VISUALIZACAO DESC";
		  }
		} else {
			
			if($max == 0) {
				$query = "SELECT * FROM vw_noticia WHERE ID_LINHA = ".$idLinha." ORDER BY VISUALIZACAO DESC";
			} else {
				$query = "SELECT TOP ".$max." * FROM vw_noticia WHERE ID_LINHA = ".$idLinha." ORDER BY VISUALIZACAO DESC";
			}

		}
		
		$rs = $this->executarQueryArray($query);	
		$arrNoticia = array();
		
		for($i=1;$i<=count($rs);$i++) {
			$noticia = new noticiaModel();
			$noticia->setIdNoticia($rs[$i]['ID_NOTICIA']);
			$noticia->selecionar();
			$arrNoticia[] = $noticia;
		}
		
		return $arrNoticia;

	}
	
	public function somarVisualizacao() {
		$noticia = new noticiaModel();
		
		$noticia->setIdNoticia($this->idNoticia);
		$noticia->selecionar();
		
		if($noticia->getAtivo() == 1) {
			$this->executar("UPDATE [NOTICIA] SET VISUALIZACAO = VISUALIZACAO + 1 WHERE ID_NOTICIA = ".$this->idNoticia."");	
		}
	}
	
	public function ativar() {
		$this->executar("UPDATE NOTICIA SET ATIVO = 1 WHERE ID_NOTICIA = ".$this->idNoticia);	
	}
	
	public function desativar() {
				
		$this->executar("UPDATE NOTICIA SET ATIVO = 0 WHERE ID_NOTICIA = ".$this->idNoticia);	
	}
}