<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class painelModel extends appConexao {
	
	private $setor;
	
	public function setSetor($dado) {
            $this->setor = $dado;	
	}
	
	public function getSetor(){
            return $this->setor;	
	}
	
        public function listarSetores($setor) {
            
     
            $query = "EXEC proc_dsh_listarSetores '".$setor."' ";

            @session_start();
            if(!isset($_SESSION['data-setores'])) {
                $_SESSION['data-setores'] = $this->tratarArray($this->executarQueryArray($query));
            }
            
            //return $this->tratarArray($this->executarQueryArray($query));	
            
            return $_SESSION['data-setores'];
        }
        
        public function listarEquipe() {
            $query = "WITH GRUPO AS (
                        SELECT ID_SETOR, NIVEL, SETOR, NOME FROM vw_colaboradorSetor WHERE SETOR = '".$this->setor."'
                        UNION ALL
                        SELECT V.ID_SETOR, V.NIVEL, V.SETOR, V.NOME FROM vw_colaboradorSetor V INNER JOIN GRUPO G ON G.ID_SETOR = V.NIVEL
                        )
                        SELECT G.*, V.FOTO FROM GRUPO G INNER JOIN VW_COLABORADORSETOR V ON V.SETOR = G.SETOR ORDER BY G.SETOR ASC";
            
            return $this->executarQueryArray($query);
        }
        
        
        public function getBoxes($setor='') {
            $query = "SELECT

                        P.PILAR,
                        (F.M00 * 100) AS VALOR,
                        (F.M00-F.M01) * 100 AS EVOL

                        FROM 
                        DSH_FATO F 
                        INNER JOIN DSH_PILAR P ON P.ID_PILAR = F.ID_PILAR
                        INNER JOIN DSH_PRODUTO PR ON PR.ID_PRODUTO = F.ID_PRODUTO
                        INNER JOIN SETOR S ON S.ID_SETOR = F.ID_SETOR
                        WHERE S.SETOR = '".$setor."' AND PR.PRODUTO = '(TOTAL)'
                        ORDER BY P.ORDEM ASC";
            
            
            $query = "exec proc_dsh_boxes '".$setor."', ".appFunction::dadoSessao('id_colaborador')." ";
            
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function getPontuacao($setor='') {
            $query = "SELECT

                        P.PILAR,
                        (F.M00 * 100) AS VALOR,
                        (F.M00-F.M01) * 100 AS EVOL

                        FROM 
                        DSH_FATO F 
                        RIGHT JOIN DSH_PILAR P ON P.ID_PILAR = F.ID_PILAR
                        INNER JOIN DSH_PRODUTO PR ON PR.ID_PRODUTO = F.ID_PRODUTO
                        INNER JOIN SETOR S ON S.ID_SETOR = F.ID_SETOR
                        WHERE S.SETOR = '".$setor."' AND PR.PRODUTO = '(TOTAL)'
                        ORDER BY P.ORDEM ASC";
            
            $query = "exec proc_dsh_boxes '".$setor."', ".appFunction::dadoSessao('id_colaborador')." ";
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function getHeaderTabela() {
            $query = "SELECT * FROM [DSH_CONF_PILAR] ORDER BY ORDEM ASC";
            $rs = $this->executarQueryArray($query);
            return $this->tratarArray($rs);
        }
        
        public function getColaboradorComparar($setor='') {
            $query = "SELECT D.PILAR, ISNULL(A.M00,0) AS M00 FROM
                        (
                                SELECT PILAR, (M00*100) AS M00 FROM DSH_BASE WHERE SETOR = '".$setor."' AND PRODUTO = '(TOTAL)'
                        ) A RIGHT JOIN DSH_BASE D ON D.PILAR = A.PILAR 
                        INNER JOIN DSH_PILAR P ON P.PILAR = D.PILAR
                        GROUP BY D.PILAR, A.M00, P.ORDEM ORDER BY P.ORDEM ASC
                        ";
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        
        
        public function getListaPilares($idLinha=0) {
            $query = "SELECT PILAR AS PILAR FROM DSH_PILAR  ORDER BY PILAR";
//            $query = "SELECT P.PILAR  FROM DSH_FATO F INNER JOIN SETOR S ON S.ID_SETOR = F.ID_SETOR
//                        INNER JOIN DSH_PILAR P ON P.ID_PILAR = F.ID_PILAR
//                        WHERE S.ID_LINHA = ".$idLinha." GROUP BY P.PILAR, P.ORDEM  ORDER BY P.ORDEM ASC";
            
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function getListaProdutos($linha=0, $pilar='PILAR') {
            $query = "SELECT PRODUTO FROM DSH_BASE WHERE PILAR = '".$pilar."' AND SETOR = '".$setor."' GROUP BY PRODUTO ORDER BY PRODUTO";
            
//            $query = "SELECT A.PRODUTO FROM DSH_PRODUTO A INNER JOIN DSH_FATO F ON F.ID_PRODUTO = A.ID_PRODUTO"
//                    . " INNER JOIN DSH_PILAR P ON P.ID_PILAR = F.ID_PILAR WHERE P.PILAR = '".$pilar."' GROUP BY A.PRODUTO";
            
            if($linha <> 0) {
            $query = "SELECT PR.PRODUTO FROM DSH_FATO F INNER JOIN SETOR S ON S.ID_SETOR = F.ID_SETOR
                        INNER JOIN DSH_PILAR P ON P.ID_PILAR = F.ID_PILAR
                        INNER JOIN DSH_PRODUTO PR ON PR.ID_PRODUTO = F.ID_PRODUTO
                        WHERE S.ID_LINHA = ".$linha." AND P.PILAR = '".$pilar."'
                        GROUP BY PR.PRODUTO  ORDER BY PR.PRODUTO ASC";
            } else {
                $query = "SELECT ID_PRODUTO, PRODUTO FROM DSH_PRODUTO ORDER BY PRODUTO ASC";
            }
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function getDadosGrafico($setor) {
            $query = "SELECT PILAR, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00 FROM [SGP].[dbo].[DSH_BASE] where SETOR = '".$setor."' and PRODUTO = '(TOTAL)'";
            $query = "proc_dsh_grafico '".$setor."', ".appFunction::dadoSessao('id_colaborador')." ";
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function rankearEquipe($setor, $arrCriterios) {
            $query = "EXEC proc_dsh_rank '".$setor."', '".$arrCriterios['PERIODO']."', '".$arrCriterios['PILAR']."', '".$arrCriterios['PRODUTO']."', '".$arrCriterios['LINHA']."', ".($arrCriterios['FUNCAO']+0)."";
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        // Remove índices numéricos do array
        private function tratarArray($array) {
            
            for($i=1;$i<=count($array);$i++) {
                foreach($array[$i] as $key => $valor) {
                    if(is_int($key)) {
                    unset($array[$i][$key]) ;
                    }
                }
            }
            
            return $array;
        }
        
        public function getPerfis() {
            
            $rs =  $this->executarQueryArray("SELECT ID_PERFIL AS ID, PERFIL AS NOME FROM PERFIL P WHERE ID_PERFIL < (SELECT ID_PERFIL FROM SETOR WHERE SETOR = '".appFunction::dadoSessao('setor')."') AND NIVEL <> 0");
            //$perfis = array(array('ID' => 0, 'NOME' => '(TUDO)'), array('ID' => 1, 'NOME' => 'REPRESENTANTE'), array('ID' => 2, 'NOME' => 'GERENTE DISTRITAL'), array('ID' => 3, 'NOME' => 'GERENTE REGIONAL'), array('ID' => 4, 'NOME' => 'GERENTE NACIONAL'));           
            return $rs;
        }
        
        public function getLinhas() {
            $rs =  $this->executarQueryArray("SELECT * FROM LINHA WHERE SIGLA <> '' ORDER BY NOME ASC");
            return $rs;
        }
        
        public function getTabela($setor='') {
            $query = "EXEC proc_dsh_tabela '".$setor."', ".appFunction::dadoSessao('id_colaborador')."";
            $rs =  $this->executarQueryArray($query);
            
            return $this->tratarArray($rs);
//            $final = [];
//            foreach($rs as $row) {
//                $ar['PILAR'] = ($row['PRODUTO'] == 'TOTAL') ? $row['PILAR'] : $row['PRODUTO'];
//                $keys = array_keys($row);
//                for($i=1;$i<count($keys);$i++) {
//                    if(!is_numeric($keys[$i])) {
//                        $ar[$keys[$i]] = $row[$keys[$i]];
//                    }
//                }
//                
//                $final[] = $ar;
//            }
//            
//           return $final;
        }
        
        
        public function salvarPermissao($idColaborador, $arrProduto) {
            $this->executarQueryArray("DELETE FROM DSH_PERMISSAO WHERE ID_COLABORADOR = ".$idColaborador);
            
            foreach($arrProduto as $produto) {
                $prod = explode('-', $produto);
                
                $this->executarQueryArray("INSERT INTO DSH_PERMISSAO (ID_COLABORADOR, ID_LINHA, ID_PILAR, ID_PRODUTO) VALUES (".$idColaborador.", ".$prod[0].", ".$prod[1].", ".$prod[2].")");
            }
        }
        
        public function selecionarPermissao($id) {
            $a['COLAB'] = $this->tratarArray($this->executarQueryArray("SELECT * FROM VW_COLABORADORSETOR WHERE ID_COLABORADOR = ".$id));
            $b = $this->tratarArray($this->executarQueryArray("SELECT * FROM DSH_PERMISSAO WHERE ID_COLABORADOR = ".$id));
            
            
            foreach($b as $row) {
                $m[$row['ID_LINHA']]['checked'] = 'checked';
                $m[$row['ID_LINHA']][$row['ID_PILAR']]['checked'] = 'checked';
                $m[$row['ID_LINHA']][$row['ID_PILAR']][$row['ID_PRODUTO']] = 'checked';
            }
            $a['PERM'] = $m;
            return $a;
        }
        
        public function excluirPermissao($id) {
            $this->executarQueryArray("DELETE FROM DSH_PERMISSAO WHERE ID_COLABORADOR = ".$id);
        }
        
        public function getPerm($id, $get, $where, $idCol=0) {
            $query = "SELECT ID_".$get." AS ID, ".$get." AS COLUNA FROM VW_DSH_PAINEL WHERE ".$where." = ".$id." GROUP BY ID_".$get.", ".$get."";
            echo $query;
            return $this->tratarArray($this->executarQueryArray($query));
        }
        
        public function getListaPermissoes() {
            
            
            $linhas = $this->tratarArray($this->executarQueryArray("SELECT VW.ID_LINHA, LL.NOME  FROM VW_DSH_PAINEL VW INNER JOIN LINHA LL ON LL.ID_LINHA = VW.ID_LINHA  GROUP BY VW.ID_LINHA, LL.NOME ORDER BY LL.NOME ASC"));
            
            foreach($linhas as $linha) {
                
                $a['ID_LINHA'] = $linha['ID_LINHA'];
                $a['LINHA'] = $linha['NOME'];
                
                $pilares = $this->tratarArray($this->executarQueryArray("SELECT ID_PILAR, PILAR FROM VW_DSH_PAINEL WHERE ID_LINHA = ".$linha['ID_LINHA']." GROUP BY ID_PILAR, PILAR, ORDEM ORDER BY ORDEM"));
                
                $j = [];
                if($pilares) {
                    $k = [];
                    foreach($pilares as $pilar) {
                        $j['PILARES'] = array('ID_PILAR' => $pilar['ID_PILAR'], 'PILAR' => $pilar['PILAR']);
                        $produtos = $this->tratarArray($this->executarQueryArray("SELECT ID_PRODUTO, PRODUTO FROM VW_DSH_PAINEL WHERE ID_PILAR = ".$pilar['ID_PILAR']." AND ID_LINHA = ".$linha['ID_LINHA']." GROUP BY ID_PRODUTO, PRODUTO ORDER BY PRODUTO ASC"));
                        

                        if($produtos) {
                            $x = [];
                            foreach($produtos as $produto) {
                                $v['ID_PRODUTO'] = $produto['ID_PRODUTO'];
                                $v['PRODUTO'] = $produto['PRODUTO'];
                                
                                $x[] = $v;
                            }
                            //$k[]['PRODUTOS'] = $x;
                        }
                        
                        $j['PRODUTOS'] = $x;
                        
                        $k[] = $j;

                    }
                    $a['PILAR'] = $k;
                
                
                }
                $final[] = $a;
            }
            
            return $final;
        }
        
        public function getPesquisaColaboradores($criterio) {
            //return "SELECT TOP 100 * FROM VW_COLABORADORSETOR WHERE COLABORADOR LIKE '%".$criterio."%'";
            return $this->executarQueryArray("
                                            SELECT
                                                TOP 15
                                                    SD.ID_COLABORADOR,
                                                    SD.NOME,
                                                    SD.PERFIL,
                                                    A.NameValues AS SETOR
                                            FROM (

                                                    SELECT 
                                                      [ID_COLABORADOR],
                                                      STUFF((
                                                            SELECT ', ' + CAST([SETOR] AS VARCHAR(MAX)) 
                                                            FROM VW_COLABORADORSETOR
                                                            WHERE (ID_COLABORADOR = Results.ID_COLABORADOR) GROUP BY SETOR
                                                            FOR XML PATH(''),TYPE).value('(./text())[1]','VARCHAR(MAX)')
                                                      ,1,2,'') AS NameValues
                                                    FROM VW_COLABORADORSETOR Results
                                                    GROUP BY ID_COLABORADOR

                                            )A RIGHT JOIN  VW_COLABORADORSETOR SD ON SD.ID_COLABORADOR = A.ID_COLABORADOR  
                                            WHERE SD.NOME + SD.SETOR LIKE '%".$criterio."%' GROUP BY A.NameValues, SD.ID_COLABORADOR, SD.NOME, SD.PERFIL ORDER BY SD.NOME ASC");
        }
        
        
        public function getColabPermitidos() {
            $query = "SELECT 
                    VW.ID_COLABORADOR, 
                    VW.SETOR, 
                    VW.NOME,
                    VW.PERFIL
                    FROM DSH_PERMISSAO PP INNER JOIN vw_colaboradorSetor VW ON VW.ID_COLABORADOR = PP.ID_COLABORADOR
                    GROUP  BY 
                    VW.ID_COLABORADOR, VW.SETOR, VW.NOME, VW.PERFIL";
            
            return $this->executarQueryArray($query);
        }
        
        public function getPilarProduto() {
            $rs =  $this->executarQueryArray("SELECT ID_PILAR, PILAR FROM DSH_PILAR GROUP BY ID_PILAR, PILAR ORDER BY PILAR ASC");
            
            //$c = [];
            foreach($rs as $row) {
                $a['ID_PILAR'] = $row['ID_PILAR'];
                $a['PILAR']    = $row['PILAR'];
                
                 $query = "SELECT P.PRODUTO , P.ID_PRODUTO
                        FROM DSH_FATO F 
                        INNER JOIN DSH_PRODUTO P ON P.ID_PRODUTO = F.ID_PRODUTO
                        WHERE F.ID_PILAR = ".$row['ID_PILAR']."
                        GROUP BY P.PRODUTO, P.ID_PRODUTO
                        ORDER BY P.PRODUTO";
                 
                 $rs2 = $this->executarQueryArray($query);
                 
                 $b = [];
                 foreach($rs2 as $row2) {
                     $aa['ID_PRODUTO'] = $row2['ID_PRODUTO'];
                     $aa['PRODUTO']    = $row2['PRODUTO'];
                    $b[] = $aa;
                 }
                 
                 $a['PRODUTOS'] = $b;
                 
                 $c[] = $a;
                
            }
            
            return $c;
           
        }
        
        
        

        
        
        
        
        
        
        
	
	public function dadosColaborador() {
            return $this->executarQueryArray("EXEC proc_dashColaborador '".$this->setor."'");	
	}
	
	public function cabecalhoMeses() {
            return $this->executarQueryArray("EXEC proc_cabMeses");	
	}
	
	public function graficoProdutividade() {
            $query = "EXEC proc_dashGraficoProdutividade '".$this->setor."'";
            
            return $this->executarQueryArray($query);	
	}
	
	public function graficoPontuacao() {
            return $this->executarQueryArray("EXEC proc_dashPontuacao '". $this->setor ."'");
	}
	
	public function dadosMercadoProduto() {
            return $this->executarQueryArray("EXEC proc_dashMercadoProduto '".$this->setor."'");
	}
	
	public function dadosShareBrasil() {
            return $this->executarQueryArray("EXEC proc_dashShareBrasil '".$this->setor."'");
	}
	
	public function dadosRankingSetor() {
            return $this->executarQueryArray("EXEC proc_dashRanking '".$this->setor."'");
	}
	
	public function dadosProdutividadeTotal() {
            return $this->executarQueryArray("EXEC proc_dashProdutividadeTotal '".$this->setor."'");	
	}
	
	public function dadosProdutividade() {
            return $this->executarQueryArray("EXEC proc_dashProdutividade '".$this->setor."'");	
	}
	
	public function dadosPrescriptionShareTotal() {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShareTotal '".$this->setor."'");	
	}
	
	public function dadosPrescriptionShareBrasil($produto='TOTAL') {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShareBRASIL '".$this->setor."', '".$produto."'");	
	}
	
	public function dadosPontuacao() {
            return $this->executarQueryArray("EXEC proc_dashPontuacao '".$this->setor."'");
	}
	
	public function dadosPrescriptionShare($array='') {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShare '".$this->setor."', '".$array."'");	
	}
	
	public function dadosDemanda($array='') {
            return $this->executarQueryArray("EXEC proc_dashDemanda '".$this->setor."', '".$array."'");
	}
	
	public function dadosDemandaTotal() {
            return $this->executarQueryArray("EXEC proc_dashDemandaTotal '".$this->setor."'");
	}
	
	public function dadosDemandaBrasil($produto='TOTAL') {
            return $this->executarQueryArray("EXEC proc_dashDemandaBRASIL '".$this->setor."', '".$produto."'");
	}
	
	public function dadosCoberturaObjetivo($array='') {
            return $this->executarQueryArray("EXEC proc_dashCobObjetivo '".$this->setor."', '".$array."'");
	}
	
	public function dadosCoberturaObjetivoTotal() {
            return $this->executarQueryArray("EXEC proc_dashCobObjetivoTotal '".$this->setor."'");
	}
	
	public function dadosMesDemanda() {
            $rs =  $this->executarQueryArray('EXEC proc_mesDemanda');
            return $rs[1]['MES'];
	}
	
	public function dadosProdutosDestaque() {
            return $this->executarQueryArray("EXEC proc_dashIncentivo2 '".date('Y')."',  '".$this->setor."'");
	}
	
	public function cabecalhoProdutosDestaque() {
            return $this->executarQueryArray("EXEC proc_dashCabecalhoIncentivo '".date('Y')."'");
	}

	public function listarAvaliacaoPMA() {
		
		$rs = $this->executarQueryArray("SELECT 
                                                    ID_COLABORADOR 
                                             FROM 
                                                    vw_colaboradorSetor 
                                             WHERE setor = '".$this->setor."'");
		
		if($rs[1]['ID_COLABORADOR'] != null) {						 
			//$rsAvaliacao = $this->executarQueryArray('exec sp_listarAvaliacoes '.$rs[1]['ID_COLABORADOR'].'');
                    $rsAvaliacao = $this->executarQueryArray("exec proc_pma_AvaliacaoListarRep '".$rs[1]['ID_COLABORADOR']."', 0, 10");
		return $rsAvaliacao;
		}
	}
	
	
}

?>