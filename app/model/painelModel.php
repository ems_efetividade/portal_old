<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class painelModel extends appConexao
{

    private $setor;

    public function setSetor($dado)
    {
        $this->setor = $dado;
    }

    public function getSetor()
    {
        return $this->setor;
    }

    public function validarAdmin()
    {
        $query = "exec proc_dsh_validarAdmin " . appFunction::dadoSessao('id_colaborador') . "";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function listarSetores($setor)
    {
        @session_start();
        $id_empresa = $_SESSION['id_empresa'];
        $query = "EXEC proc_dsh_listarSetoresByEmpresa '" . $setor . "', " . $id_empresa;
        $_SESSION['data-setores'] = $this->tratarArray($this->executarQueryArray($query));
        return $_SESSION['data-setores'];
    }

    public function getBoxes($setor = '')
    {
        $query = "exec proc_dsh_resumoPilares '" . $setor . "', " . appFunction::dadoSessao('id_colaborador') . "";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getPontuacao($setor = '')
    {
        $query = "exec proc_dsh_resumoPontuacao '" . $setor . "', " . appFunction::dadoSessao('id_colaborador') . "";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getRankingPainel($setor = '')
    {
        $query = "exec proc_dsh_ranking '" . $setor . "'";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getHeaderTabela()
    {
        $query = "EXEC proc_dsh_headerTabela '" . appFunction::dadoSessao('id_empresa') . "'";
        $rs = $this->executarQueryArray($query);
        return $this->tratarArray($rs);
    }

    public function getColaboradorComparar($setor = '')
    {
        $query = "exec proc_dsh_resumoPontuacao '" . $setor . "', " . appFunction::dadoSessao('id_colaborador') . "";
        $rs = $this->tratarArray($this->executarQueryArray($query));

        $query1 = "exec proc_dsh_ranking '" . $setor . "'";
        $rs1 = $this->tratarArray($this->executarQueryArray($query1));

        $query2 = "exec proc_dsh_pilaresComprar " . appFunction::dadoSessao('id_colaborador') . "";
        $rs2 = $this->tratarArray($this->executarQueryArray($query2));

        return array(
            'PONTUACAO' => $rs,
            'RANKING' => $rs1[1],
            'PILAR_COMPARAR' => $rs2
        );
    }


    public function getListaPilares($setor = '', $idLinha = 0)
    {
        $query = "EXEC proc_dsh_pilaresRanking '" . $setor . "', " . $idLinha . ", " . appFunction::dadoSessao('id_empresa') . " ";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getCabecalhoMes($idPilar = 0)
    {
        $query = "EXEC proc_dsh_cabecalhoMes " . $idPilar . ", " . appFunction::dadoSessao('id_empresa') . " ";
        $rs = $this->tratarArray($this->executarQueryArray($query));
        return $rs[1];
    }

    public function getPeriodo()
    {
        $query = "EXEC proc_dsh_periodoRanking " . appFunction::dadoSessao('id_empresa') . "";
        $rs = $this->tratarArray($this->executarQueryArray($query));
        return $this->tratarArray($rs);
    }

    public function getListaProdutos($setor = 0, $linha = 0, $pilar = 0)
    {
        $query = "EXEC proc_dsh_produtoRanking '" . $setor . "', " . $linha . ", " . $pilar . "";
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getDadosGrafico($setor)
    {
        $query = "EXEC proc_dsh_grafico '" . $setor . "', " . appFunction::dadoSessao('id_colaborador') . " ";
        $query1 = "EXEC proc_dsh_graficoCab " . appFunction::dadoSessao('id_empresa') . "";

        $rs = $this->tratarArray($this->executarQueryArray($query));
        $rs1 = $this->tratarArray($this->executarQueryArray($query1));

        return array('DADOS' => $rs, 'CAB' => $rs1[1]);
    }

    public function rankearEquipe($setor, $arrCriterios)
    {
        $query = "EXEC proc_dsh_rank '" . $setor . "', '" . $arrCriterios['PERIODO'] . "', '" . $arrCriterios['PILAR'] . "', '" . $arrCriterios['PRODUTO'] . "', '" . $arrCriterios['LINHA'] . "', " . ($arrCriterios['FUNCAO'] + 0) . ", '" . $arrCriterios['REGIONAL'] . "'";
        return $this->tratarArray($this->executarQueryArray($query));
    }


    public function getEstruturaBaseCarga()
    {
        $query = "exec sp_columns [DSH_BASE_UP]";
        $rs = $this->tratarArray($this->executarQueryArray($query));

        $table = array();
        foreach ($rs as $row) {
            $a['COLUMN'] = $row['COLUMN_NAME'];
            $a['DATA_TYPE'] = $row['DATA_TYPE'];
            $a['TYPE_NAME'] = $row['TYPE_NAME'];

            $table[] = $a;
        }
        return $table;
    }

    public function insereNaTabela($query)
    {
        $this->executar($query);
        $erro = sqlsrv_errors();

        if (is_array($erro)) {
            return $erro[0]['message'] . '<br><br><br>';
        }
    }

    // Remove índices numéricos do array
    private function tratarArray($array)
    {
        for ($i = 1; $i <= count($array); $i++) {
            foreach ($array[$i] as $key => $valor) {
                if (is_int($key)) {
                    unset($array[$i][$key]);
                }
            }
        }
        return $array;
    }

    public function getPerfis()
    {
        $query = "EXEC proc_dsh_perfilRanking '" . appFunction::dadoSessao('setor') . "'";
        $rs = $this->executarQueryArray($query);
        return $rs;
    }

    public function getLinhas($setor = '')
    {
        $query = "EXEC proc_dsh_linhaRanking '" . $setor . "'";
        $rs = $this->executarQueryArray($query);
        return $rs;
    }

    public function getRegional($setor = '')
    {
        $query = "EXEC proc_dsh_regionalRanking '" . $setor . "'";
        $rs = $this->executarQueryArray($query);
        return $rs;
    }

    public function getTabela($setor = '')
    {
        $query = "EXEC proc_dsh_tabelaPilar '" . $setor . "', " . appFunction::dadoSessao('id_colaborador') . "";
        $rs = $this->executarQueryArray($query);
        return $this->tratarArray($rs);
    }

    public function getTabelaProduto($setor = '', $idPilar = 0)
    {
        $query = "EXEC proc_dsh_tabelaProduto '" . $setor . "', " . $idPilar . ", " . appFunction::dadoSessao('id_colaborador') . " ";
        $rs = $this->executarQueryArray($query);
        return $this->tratarArray($rs);
    }

    public function getTabelaEquipe($setor = '', $idPilar = 0)
    {
        $query = "EXEC proc_dsh_tabelaEquipe '" . $setor . "', " . $idPilar . ", " . appFunction::dadoSessao('id_colaborador') . " ";
        //echo $query;
        $rs = $this->executarQueryArray($query);
        return $this->tratarArray($rs);
    }


    public function salvarPermissao($idColaborador, $arrProduto)
    {
        session_start();
        $id_empresa = $_SESSION['id_empresa'];
        $this->executarQueryArray("EXEC proc_dsh_excluirPermissao $idColaborador,$id_empresa");
        foreach ($arrProduto as $produto) {
            $prod = explode('-', $produto);
            $this->executarQueryArray("EXEC proc_dsh_adicionarPermissao " . $idColaborador . ", " . $prod[0] . ", " . $prod[1] . ", " . $prod[2] . ", " . appFunction::dadoSessao('id_colaborador') . "");
        }
    }

    public function selecionarPermissao($id)
    {
        ///echo "SELECT * FROM VW_COLABORADORSETOR WHERE ID_COLABORADOR = ".$id." and ID_EMPRESA = ".appFunction::dadoSessao('id_empresa')."";
        $a['COLAB'] = $this->tratarArray($this->executarQueryArray("SELECT * FROM VW_COLABORADORSETOR WHERE ID_COLABORADOR = " . $id . " and ID_EMPRESA = " . appFunction::dadoSessao('id_empresa') . ""));
        //echo "EXEC proc_dsh_selecionarPermissao ".$id.", ".appFunction::dadoSessao('id_empresa')." ";
        $b = $this->tratarArray($this->executarQueryArray("EXEC proc_dsh_selecionarPermissao " . $id . ", " . appFunction::dadoSessao('id_empresa') . " "));

        foreach ($b as $row) {
            $m[$row['ID_LINHA']]['checked'] = 'checked';
            $m[$row['ID_LINHA']][$row['ID_PILAR']]['checked'] = 'checked';
            $m[$row['ID_LINHA']][$row['ID_PILAR']][$row['ID_PRODUTO']] = 'checked';
        }
        $a['PERM'] = $m;
        return $a;
    }

    public function excluirPermissao($id)
    {
        session_start();
        $id_empresa = $_SESSION['id_empresa'];
        $this->executarQueryArray("EXEC proc_dsh_excluirPermissao ,$id_empresa");
    }

    public function getPerm($id, $get, $where, $idCol = 0)
    {
        $query = "SELECT ID_" . $get . " AS ID, " . $get . " AS COLUNA FROM VW_DSH_PAINEL WHERE " . $where . " = " . $id . " GROUP BY ID_" . $get . ", " . $get . "";
        //echo $query;
        return $this->tratarArray($this->executarQueryArray($query));
    }

    public function getListaPermissoes()
    {
        //echo "EXEC proc_dsh_listarLinhaPermissao ".appFunction::dadoSessao('id_colaborador');
        $linhas = $this->tratarArray($this->executarQueryArray("EXEC proc_dsh_listarLinhaPermissao " . appFunction::dadoSessao('id_empresa') . ""));

        foreach ($linhas as $linha) {
            $a['ID_LINHA'] = $linha['ID_LINHA'];
            $a['LINHA'] = $linha['NOME'];

            $pilares = $this->tratarArray($this->executarQueryArray("EXEC proc_dsh_listarPilarPermissao " . $linha['ID_LINHA'] . ""));
            $j = [];
            if ($pilares) {
                $k = [];
                foreach ($pilares as $pilar) {
                    $j['PILARES'] = array('ID_PILAR' => $pilar['ID_PILAR'], 'PILAR' => $pilar['PILAR']);
                    $produtos = $this->tratarArray($this->executarQueryArray("EXEC proc_dsh_listarProdutoPermissao " . $pilar['ID_PILAR'] . " , " . $linha['ID_LINHA'] . ""));
                    if ($produtos) {
                        $x = [];
                        foreach ($produtos as $produto) {
                            $v['ID_PRODUTO'] = $produto['ID_PRODUTO'];
                            $v['PRODUTO'] = $produto['PRODUTO'];

                            $x[] = $v;
                        }
                    }
                    $j['PRODUTOS'] = $x;
                    $k[] = $j;
                }
                $a['PILAR'] = $k;
            }
            $final[] = $a;
        }
        return $final;
    }

    public function getPesquisaColaboradores($criterio)
    {
        return $this->executarQueryArray("EXEC proc_dsh_pesquisarColabPermissao '" . $criterio . "', " . appFunction::dadoSessao('id_colaborador') . ", " . appFunction::dadoSessao('id_empresa') . " ");
    }

    public function getColabPermitidos()
    {
        $query = "proc_dsh_listarColabPermissao " . appFunction::dadoSessao('id_colaborador') . ", " . appFunction::dadoSessao('id_empresa') . "";
        return $this->executarQueryArray($query);
    }

    public function getPilarProduto()
    {
        $rs = $this->executarQueryArray("EXEC proc_dsh_listarPilar " . appFunction::dadoSessao('id_empresa') . "");

        foreach ($rs as $row) {
            $a['ID_PILAR'] = $row['ID_PILAR'];
            $a['PILAR'] = $row['PILAR'];
            $query = "EXEC proc_dsh_listarProduto " . $row['ID_PILAR'] . "";
            $rs2 = $this->executarQueryArray($query);

            $b = [];
            foreach ($rs2 as $row2) {
                $aa['ID_PRODUTO'] = $row2['ID_PRODUTO'];
                $aa['PRODUTO'] = $row2['PRODUTO'];
                $b[] = $aa;
            }
            $a['PRODUTOS'] = $b;
            $c[] = $a;
        }
        return $c;
    }


    /************************* PAINEL ANTIGO **************************/
    public function dadosColaborador()
    {
        return $this->executarQueryArray("EXEC proc_dashColaborador '" . $this->setor . "'");
    }

    public function cabecalhoMeses()
    {
        return $this->executarQueryArray("EXEC proc_cabMeses");
    }

    public function graficoProdutividade()
    {
        $query = "EXEC proc_dashGraficoProdutividade '" . $this->setor . "'";
        return $this->executarQueryArray($query);
    }

    public function graficoPontuacao()
    {
        return $this->executarQueryArray("EXEC proc_dashPontuacao '" . $this->setor . "'");
    }

    public function dadosMercadoProduto()
    {
        return $this->executarQueryArray("EXEC proc_dashMercadoProduto '" . $this->setor . "'");
    }

    public function dadosShareBrasil()
    {
        return $this->executarQueryArray("EXEC proc_dashShareBrasil '" . $this->setor . "'");
    }

    public function dadosRankingSetor()
    {
        return $this->executarQueryArray("EXEC proc_dashRanking '" . $this->setor . "'");
    }

    public function dadosProdutividadeTotal()
    {
        return $this->executarQueryArray("EXEC proc_dashProdutividadeTotal '" . $this->setor . "'");
    }

    public function dadosProdutividade()
    {
        return $this->executarQueryArray("EXEC proc_dashProdutividade '" . $this->setor . "'");
    }

    public function dadosPrescriptionShareTotal()
    {
        return $this->executarQueryArray("EXEC proc_dashPrescriptionShareTotal '" . $this->setor . "'");
    }

    public function dadosPrescriptionShareBrasil($produto = 'TOTAL')
    {
        return $this->executarQueryArray("EXEC proc_dashPrescriptionShareBRASIL '" . $this->setor . "', '" . $produto . "'");
    }

    public function dadosPontuacao()
    {
        return $this->executarQueryArray("EXEC proc_dashPontuacao '" . $this->setor . "'");
    }

    public function dadosPrescriptionShare($array = '')
    {
        return $this->executarQueryArray("EXEC proc_dashPrescriptionShare '" . $this->setor . "', '" . $array . "'");
    }

    public function dadosDemanda($array = '')
    {
        return $this->executarQueryArray("EXEC proc_dashDemanda '" . $this->setor . "', '" . $array . "'");
    }

    public function dadosDemandaTotal()
    {
        return $this->executarQueryArray("EXEC proc_dashDemandaTotal '" . $this->setor . "'");
    }

    public function dadosDemandaBrasil($produto = 'TOTAL')
    {
        return $this->executarQueryArray("EXEC proc_dashDemandaBRASIL '" . $this->setor . "', '" . $produto . "'");
    }

    public function dadosCoberturaObjetivo($array = '')
    {
        return $this->executarQueryArray("EXEC proc_dashCobObjetivo '" . $this->setor . "', '" . $array . "'");
    }

    public function dadosCoberturaObjetivoTotal()
    {
        return $this->executarQueryArray("EXEC proc_dashCobObjetivoTotal '" . $this->setor . "'");
    }

    public function dadosMesDemanda()
    {
        $rs = $this->executarQueryArray('EXEC proc_mesDemanda');
        return $rs[1]['MES'];
    }

    public function dadosProdutosDestaque()
    {
        return $this->executarQueryArray("EXEC proc_dashIncentivo2 '" . date('Y') . "',  '" . $this->setor . "'");
    }

    public function cabecalhoProdutosDestaque()
    {
        return $this->executarQueryArray("EXEC proc_dashCabecalhoIncentivo '" . date('Y') . "'");
    }

    public function listarAvaliacaoPMA()
    {
        $rs = $this->executarQueryArray("SELECT 
                                                ID_COLABORADOR 
                                         FROM 
                                                vw_colaboradorSetor 
                                         WHERE setor = '" . $this->setor . "'");

        if ($rs[1]['ID_COLABORADOR'] != null) {
            //$rsAvaliacao = $this->executarQueryArray('exec sp_listarAvaliacoes '.$rs[1]['ID_COLABORADOR'].'');
            $rsAvaliacao = $this->executarQueryArray("exec proc_pma_AvaliacaoListarRep '" . $rs[1]['ID_COLABORADOR'] . "', 0, 10");
            return $rsAvaliacao;
        }
    }
}

?>