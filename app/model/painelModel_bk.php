<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class painelModel extends appConexao {
	
	private $setor;
	
	public function setSetor($dado) {
            $this->setor = $dado;	
	}
	
	public function getSetor(){
            return $this->setor;	
	}
	
	
	public function dadosColaborador() {
            return $this->executarQueryArray("EXEC proc_dashColaborador '".$this->setor."'");	
	}
	
	public function cabecalhoMeses() {
            return $this->executarQueryArray("EXEC proc_cabMeses");	
	}
	
	public function graficoProdutividade() {
            $query = "EXEC proc_dashGraficoProdutividade '".$this->setor."'";
            
            return $this->executarQueryArray($query);	
	}
	
	public function graficoPontuacao() {
            return $this->executarQueryArray("EXEC proc_dashPontuacao '". $this->setor ."'");
	}
	
	public function dadosMercadoProduto() {
            return $this->executarQueryArray("EXEC proc_dashMercadoProduto '".$this->setor."'");
	}
	
	public function dadosShareBrasil() {
            return $this->executarQueryArray("EXEC proc_dashShareBrasil '".$this->setor."'");
	}
	
	public function dadosRankingSetor() {
            return $this->executarQueryArray("EXEC proc_dashRanking '".$this->setor."'");
	}
	
	public function dadosProdutividadeTotal() {
            return $this->executarQueryArray("EXEC proc_dashProdutividadeTotal '".$this->setor."'");	
	}
	
	public function dadosProdutividade() {
            return $this->executarQueryArray("EXEC proc_dashProdutividade '".$this->setor."'");	
	}
	
	public function dadosPrescriptionShareTotal() {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShareTotal '".$this->setor."'");	
	}
	
	public function dadosPrescriptionShareBrasil($produto='TOTAL') {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShareBRASIL '".$this->setor."', '".$produto."'");	
	}
	
	public function dadosPontuacao() {
            return $this->executarQueryArray("EXEC proc_dashPontuacao '".$this->setor."'");
	}
	
	public function dadosPrescriptionShare($array='') {
            return $this->executarQueryArray("EXEC proc_dashPrescriptionShare '".$this->setor."', '".$array."'");	
	}
	
	public function dadosDemanda($array='') {
            return $this->executarQueryArray("EXEC proc_dashDemanda '".$this->setor."', '".$array."'");
	}
	
	public function dadosDemandaTotal() {
            return $this->executarQueryArray("EXEC proc_dashDemandaTotal '".$this->setor."'");
	}
	
	public function dadosDemandaBrasil($produto='TOTAL') {
            return $this->executarQueryArray("EXEC proc_dashDemandaBRASIL '".$this->setor."', '".$produto."'");
	}
	
	public function dadosCoberturaObjetivo($array='') {
            return $this->executarQueryArray("EXEC proc_dashCobObjetivo '".$this->setor."', '".$array."'");
	}
	
	public function dadosCoberturaObjetivoTotal() {
            return $this->executarQueryArray("EXEC proc_dashCobObjetivoTotal '".$this->setor."'");
	}
	
	public function dadosMesDemanda() {
            $rs =  $this->executarQueryArray('EXEC proc_mesDemanda');
            return $rs[1]['MES'];
	}
	
	public function dadosProdutosDestaque() {
            return $this->executarQueryArray("EXEC proc_dashIncentivo2 '".date('Y')."',  '".$this->setor."'");
	}
	
	public function cabecalhoProdutosDestaque() {
            return $this->executarQueryArray("EXEC proc_dashCabecalhoIncentivo '".date('Y')."'");
	}

	public function listarAvaliacaoPMA() {
		
		$rs = $this->executarQueryArray("SELECT 
                                                    ID_COLABORADOR 
                                             FROM 
                                                    vw_colaboradorSetor 
                                             WHERE setor = '".$this->setor."'");
		
		if($rs[1]['ID_COLABORADOR'] != null) {						 
			//$rsAvaliacao = $this->executarQueryArray('exec sp_listarAvaliacoes '.$rs[1]['ID_COLABORADOR'].'');
                    $rsAvaliacao = $this->executarQueryArray("exec proc_pma_AvaliacaoListarRep '".$rs[1]['ID_COLABORADOR']."', 0, 10");
		return $rsAvaliacao;
		}
	}
	
	
}

?>