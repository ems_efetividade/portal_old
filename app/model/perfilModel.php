<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class perfilModel extends appConexao {

	private $idPerfil;
	private $perfil;
	private $nivel;
	private $admin;

	public function perfilModel() {
		$this->idPerfil = 0;
		$this->perfil = '';
		$this->nivel = 0;
		$this->admin = 0;
	}
	
	public function setIdPerfil($valor) {
		$this->idPerfil = $valor;
	}
	
	public function setPerfil($valor) {
		$this->perfil = $valor;
	}
	
	public function setNivel($valor) {
		$this->nivel = $valor;
	}
	
	public function setAdmin($valor) {
		$this->admin = $valor;
	}
	
	
	public function getIdPerfil() {
		return $this->idPerfil;
	}
	
	public function getPerfil() {
		return $this->perfil;
	}
	
	public function getNivel() {
		return $this->nivel;
	}
	
	public function getAdmin() {
		return $this->admin;
	}
	
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM PERFIL WHERE ID_PERFIL = ".$this->idPerfil);
		
		$this->idPerfil = $rs[1]['ID_PERFIL'];
		$this->perfil = $rs[1]['PERFIL'];
		$this->nivel = $rs[1]['NIVEL'];
		$this->admin = $rs[1]['ADMIN'];
	}
	
	public function listar() {
		$arrPerfil = array();
		$rs = $this->executarQueryArray("SELECT * FROM PERFIL");
		
		for($i=1;$i<=count($rs);$i++) {
			$perfil = new perfilModel();
			$perfil->setIdPerfil($rs[$i]['ID_PERFIL']);
			$perfil->selecionar();
			
			$arrPerfil[] = $perfil;
		}
		
		return $arrPerfil;
	}
	
}