<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class politicaPremiacaoModel extends appConexao {
    
    private $id;
    private $idPolitica;
    private $idColaborador;
    private $setor;
    private $protocolo;
    private $data;
    private $aceito;
    
    public function __construct() {
        $this->id = 0;
        $this->idPolitica = 0;
        $this->idColaborador = 0;
        $this->setor = '';
        $this->protocolo = '';
        $this->data = '';
        $this->aceito = '';
    }
    
    public function setID($valor) {
        $this->id = $valor;
    }
    
    public function setIdPolitica($valor) {
        $this->idPolitica = $valor;
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }
    
    public function setSetor($valor) {
        $this->setor = $valor;
    }
    
    public function setAceito($valor) {
        $this->aceito = $valor;
    }
    
}