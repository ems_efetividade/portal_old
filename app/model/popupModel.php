<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class popupModel extends appConexao {

	private $idPopup;
	private $imagem;
	private $ativo;
	private $nome;
        private $link;

	public function popupModel() {
		$this->idPopup = 0;
		$this->imagem = '';
		$this->ativo = 0;
		$this->nome = '';
	}
        
        public function getLink() {
            return $this->link;
        }

        public function setLink($link) {
            $this->link = $link;
        }

        	
	public function setIdPopup($valor) {
		$this->idPopup = $valor;
	}
	
	public function setImagem($valor) {
		$this->imagem = $valor;
	}
	
	public function setAtivo($valor) {
		$this->ativo = $valor;
	}
	
	public function setNome($valor) {
		$this->nome = $valor;
	}

	
	
	public function getIdPopup() {
		return $this->idPopup;
	}
	
	public function getImagem() {
		return $this->imagem;
	}
	
	public function getAtivo() {
		return $this->ativo;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
	public function salvar() {
		$this->executar("INSERT INTO POPUP (IMAGEM, ATIVO, NOME) VALUES ('".$this->imagem."', ".$this->ativo.", '".$this->nome."')");	
	}
        
        public function salvarLink() {
		$this->executar("UPDATE POPUP SET LINK = '".$this->link."' WHERE ID_POPUP = ".$this->idPopup);	
	}
	
	public function excluir() {
		$popup = new popupModel();
		$popup->setIdPopup($this->idPopup);
		$popup->selecionar();
		
		$this->executar("DELETE FROM POPUP WHERE ID_POPUP = ".$this->idPopup);
		unlink(appConf::caminho_fisico.appConf::folder_popup_fisico.$popup->getImagem());
	}
	
	public function selecionar() {
		if($this->idPopup != 0) {
			$rs = $this->executarQueryArray("SELECT * FROM POPUP WHERE ID_POPUP = ".$this->idPopup);
			
			$this->idPopup = $rs[1]['ID_POPUP'];
			$this->imagem = $rs[1]['IMAGEM'];
			$this->ativo = $rs[1]['ATIVO'];
			$this->nome = $rs[1]['NOME'];
                        $this->link = $rs[1]['LINK'];
		}
	}
	
	public function listar() {
		$arrPopup = array();
		$rs = $this->executarQueryArray("SELECT * FROM POPUP");
		
		for($i=1;$i<=count($rs);$i++) {
			$popup = new popupModel();
			$popup->setIdPopup($rs[$i]['ID_POPUP']);
			$popup->selecionar();
			
			$arrPopup[] = $popup;
		}
		
		return $arrPopup;
	}
	
	public function ativar() {
		
		$popup = new popupModel();
		$popup->setIdPopup($this->idPopup);
		$popup->selecionar();
		
		if($popup->getAtivo() == 0) {
			$ativar = 1;	
		} else {
			$ativar = 0;
		}
		
		$this->executar('UPDATE POPUP SET ATIVO = 0');	
		$this->executar('UPDATE POPUP SET ATIVO = '.$ativar.' WHERE ID_POPUP = '.$this->idPopup);
	}
	
	public function selecionarPopupAtivo() {
		$popup = new popupModel();
		$linha = $_SESSION['id_linha'];
		$rs = $this->executarQueryArray("SELECT ID_POPUP FROM POPUP WHERE ATIVO = 1  AND ID_LINHA_EMPRESA = $linha");
		$popup->setIdPopup($rs[1]['ID_POPUP']);
		$popup->selecionar();
		return $popup;
	}
	
}