<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class relatorioModel extends appConexao {
	
	
	function listarDiretorio() {
		return $this->executarQueryArray("EXEC proc_listarDiretorioPadrao '".appFunction::dadoSessao('id_setor')."'");	
	}
	
	function listarConteudoDiretorio($idPasta, $pagina=0, $criterio='') {
		$maxResultado = 14;
		return $this->executarQueryArray("EXEC proc_listarConteudoDiretorio '".$idPasta."', '".appFunction::dadoSessao('id_setor')."', '".$maxResultado."', '".$pagina."', '".$criterio."'");
	}
	
	function selecionarRelatorio($idRelatorio) {
		return $this->executarQueryArray("SELECT * FROM vw_arquivo WHERE id_arquivo = ".$idRelatorio);
	}
	
	function selecionarDiretorioRaiz($idPasta) {
		$rs = $this->executarQueryArray("with grupo as (
											select id_pasta, ID_DIRETORIO from vw_pasta where ID_pasta = ".$idPasta."
											union all
											select p.id_pasta, p.ID_DIRETORIO from vw_pasta p inner join grupo on grupo.ID_DIRETORIO = p.ID_PASTA
											)
										select ID_PASTA from grupo where ID_DIRETORIO = 0");
		return $rs[1]['ID_PASTA'];
			
	}
	
	function pesquisar($criterio, $idPasta) {
		$query = "EXEC proc_pesquisarAdmin ".$idPasta.", '".$criterio."'";
		return $this->executarQueryArray($query);
	}
	
	function pastaRaiz() {
		return $this->executarQueryArray("SELECT * FROM vw_tamanhoPastaRaiz ORDER BY ID_PASTA ASC");
	}
	
	
}