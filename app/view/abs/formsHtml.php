<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */


abstract class formsHtml {
    
    public function modalVisualizar($titulo=""){
        if ($titulo == "")
            $titulo = "Detalhes";
        ?>
        <div class="modal fade" id="modalVisualizar" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <span  class="close" style="color: #FFFFFF" data-dismiss="modal">&times;</span>
                        <h4 class="modal-title text-left "><span class="glyphicon glyphicon-info-sign" >
                            </span> <?php echo $titulo ?></h4>
                    </div>
                    <div class="modal-body" id="visualizarrContent">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" id="btnFecharModalVisualizar" data-dismiss="modal" >Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function modalDefault($conteudo){
        if($conteudo[3]==''){
            $conteudo[3] = 'primary';
        }
        ?>
        <div class="modal fade" id="modal<?php echo $conteudo[2]?>" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header btn-<?php echo $conteudo[3]; ?>">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left ">
                            <span class="glyphicon glyphicon-<?php echo $conteudo[4] ?>" ></span><?php echo $conteudo[0] ?></h4>
                    </div>
                    <div class="modal-body" id="<?php echo $conteudo[2]; ?>Content">
                        <?php if($conteudo[6]==1){ ?>
                        <div class="row">
                            <div class="col-sm-12 text-right ">
                                <div class="col-sm-12 text-center ">
                                    <div class="form-group form-group-sm">
                                        <h3><?php echo $conteudo[5]?></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 text-right ">
                                <div class="form-group form-group-sm">
                                    <button id="confirma<?php echo $conteudo[2]; ?>" class="btn btn-<?php echo $conteudo[3]; ?>">Excluir</button>
                                    <button data-dismiss="modal" class="btn btn-default">Cancelar</button>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php if($conteudo[6]!=1){ ?>
                    <div class="modal-footer">
                        <button type="button" id="<?php echo $conteudo[1]?>" class="btn btn-default" data-dismiss="modal" >Fechar</button>
                    </div>
                    <?php }?>
                </div>
            </div>
        </div>
        <?php
    }

    public function modalEditar(){
        ?>
        <div class="modal fade" id="modalEditar" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left "><span class="glyphicon glyphicon-info-sign" ></span> Editar</h4>
                    </div>
                    <div class="modal-body" id="editarContent">

                    </div>
                    <div class="modal-footer" id="footerEditar">
                        <button type="button" id="salvarEditar" class="btn btn-success" >Salvar</button>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalMsgBoxConfirmEditar">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal modal-center fade" style="z-index:10000000;" id="modalMsgBoxConfirmEditar" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content ">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-left" id="myModalLabel"><span class="glyphicon glyphicon-remove"></span> Atenção!</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5><p class="text-center" id="msgboxTexto">Deseja realmente cancelar?<br><br><sub>Todos os dados serão descartados</sub></p></h5>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="cancelarEditar"> Sim</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Não</button>
                    </div>

                </div>
            </div>
        </div>
        <script>
            $('#cancelarEditar').click(function () {
                $('#editarContent').html("");
                $('#modalEditar').modal('hide');
            });
            $('#salvarEditar').click(function () {
                $.ajax({
                    type: "POST",
                    url: $('#formEditar').attr('action'),
                    data: new FormData($('#formEditar')[0]),
                    contentType: false,
                    processData: false,
                    success: function (retorno) {
                        if (retorno != '') {
                            msgBox(retorno);
                        } else {
                            location.reload();
                        }
                    },
                    beforeSend: function (a) {
                         modalAguarde('show');
                    }
                });
            });
        </script>
        <?php
    }

    public function modalCadastrar() {
        ?>
        <div class="modal modal-center fade" style="z-index:10000000;" id="modalMsgBoxConfirm" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content ">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-left" id="myModalLabel"><span class="glyphicon glyphicon-remove"></span> Atenção!</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5><p class="text-center" id="msgboxTexto">Deseja realmente cancelar?<br><br><sub>Todos os dados serão descartados</sub></p></h5>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="cancelarInserir"> Sim</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Não</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="modalCadastrar" data-backdrop="static" role="dialog">
            <div class="modal-dialog modal-lg" >
                <div class="modal-content">

                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title text-left "><span class="glyphicon glyphicon-plus"></span> Novo</h4>
                    </div>
                    <div class="modal-body" id="cadastrarContent">

                    </div>
                    <div class="modal-footer" id="footerCadastrar">
                        <span type="button" id="salvarInserir" class="btn btn-success" >Salvar</span>
                        <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalMsgBoxConfirm">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
        <script>
            $('#cancelarInserir').click(function () {
                $('#cadastrarContent').html("");
                $('#modalCadastrar').modal('hide');
            });
            $('#salvarInserir').click(function () {
                var result;
                $.ajax({
                    type: "POST",
                    url: $('#formSalvar').attr('action'),
                    data: new FormData($('#formSalvar')[0]),
                    contentType: false,
                    processData: false,
                    success: function (result) {
                        console.log(result);
                        if (result != '') {
                            msgBox(result);
                        } else {
                            location.reload();
                        }
                    },
                    beforeSend: function (a) {
                       //  modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
                    }
                });
            });
            $(document).keypress(function(e) {
                if(e.which == 13) { // se pressionar enter
                    return false;
                }
            });
        </script>
        <?php
    }
    
    public function modalExcluir() {
        ?>
        <div class="modal modal-center fade" style="z-index:10000000;" id="modalMsgBoxConfirmExcluir" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content ">
                    <div class="modal-header btn-danger">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title text-left" id="myModalLabel"><span class="glyphicon glyphicon-remove"></span> Atenção!</h5>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <h5><p class="text-center" id="msgboxTexto">Deseja realmente excluir este registro?<br><br><sub>Todos os dados serão apagados</sub></p></h5>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal" id="confirmarExcluir"> Sim</button>
                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"> Não</button>
                    </div>

                </div>
            </div>
        </div>
        <?php
    }

}
