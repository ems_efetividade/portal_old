<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once("formsHtml.php");


abstract class gridView extends formsHtml
{
    const caminho = "/../../../" . APP_FOLDER . "/app/controller/";
    private $grid_titulo = null;
    private $grid_sub_titulo = null;
    private $grid_lista;
    private $grid_cols;
    private $grid_msg_sem_resultados = 'Não há resultados para listar!';
    private $grid_cadastrar = 1;
    private $grid_editar = 1;
    private $grid_excluir = 1;
    private $grid_visualizar = 1;
    private $grid_pesquisar_nome = null;
    private $grid_pesquisar_cols = null;
    private $grid_order_by_default;
    private $grid_paginacao_qtde_pagina = 50;
    private $grid_objeto = null;
    private $grid_array_metodo = null;
    private $grid_metodo;
    private $grid_permissao;
    private $grid_botao_topo = null;

    public function getGrid_botao_topo()
    {
        return $this->grid_botao_topo;
    }


    public function setGrid_botao_topo($grid_botao_topo)
    {
        $this->grid_botao_topo = $grid_botao_topo;
    }

    private function verificarPermissoes()
    {
        require_once('/../../../sistemas/ControleNivel/app/controller/cControleHeaderNivel.php');
        $control_nivel = new cControleHeaderNivel();
        $_POST['fkperfil'] = $_SESSION['id_perfil'];
        $_POST['fkferramenta'] = $_SESSION['sistema_atual'];
        $permissao = $control_nivel->loadObj();
        $nivel_permissao = $permissao->nivelAcesso();
        $_POST['fkperfil'] ='';
        $_POST['fkferramenta'] ='';
//        switch ($nivel_permissao) {
//            case 1:
//                $this->setGrid_cadastrar(0);
//                $this->setGrid_editar(0);
//                $this->setGrid_excluir(0);
//                $this->setGrid_visualizar(0);
//                break;
//            case 2:
//                $this->setGrid_cadastrar(0);
//                $this->setGrid_editar(0);
//                $this->setGrid_excluir(0);
//                break;
//            case 3:
//                $this->setGrid_editar(0);
//                $this->setGrid_excluir(0);
//                $this->setGrid_visualizar(0);
//                break;
//            case 4:
//                $this->setGrid_editar(0);
//                $this->setGrid_excluir(0);
//                break;
//            case 5:
//                $this->setGrid_excluir(0);
//                break;
//            case 6:
//                break;
        //}

    }

    public function renderGrid()
    {
        $this->verificarPermissoes();
        ?>
        <div class="container">
            <?php
            if ($this->getGrid_objeto() != null && $this->getGrid_metodo() != null) {
                $this->carregardados();
            }
            $this->montaTitulo();
            $this->montaBotoesTopo();
            if (is_array($this->getGrid_cols())) {
                $indice = $this->montaGrid();
            }
            $this->gerarPaginacao();
            ?>
        </div>
        <?php

        $this->rodape();
    }

    private function rodape()
    {
        ?>
        <div style="position:fixed;bottom:0;width:100%; background-color: #5a5a5a; ">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h5>
                            <p class="text-center">
                                <small style="color: #fff">Portal Efetividade - Grupo NC</small>
                            </p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    private function gerarPaginacao()
    {
        $registros = $_POST['numRows'];
        $atual = $_POST['atual'];
        if ($atual == "")
            $atual = 1;
        elseif ($atual > 1) {
            $atual = intval($atual / 10) + 1;
        }
        if ($registros > 10) {
            if ($registros % 10 != 0) {
                $registros = $registros / 10;
                $registros = explode('.', $registros);
                $registros = $registros[0] + 1;
            } else {
                $registros = $registros / 10;
            }
            ?>
            <style>
                .paggerPoint {
                    cursor: pointer;
                }
            </style>
            <div class="row">
                <div class="col-12">
                    <nav>
                        <ul class="pager">
                            <li class="paggerPoint" onclick="primeiraPagina()"><a>Primeiro</a></li>
                            <li class="paggerPoint" onclick="recuarPagina()"><a>Anterior</a></li>
                            <li>
                                <small><?php echo $atual ?>/ <?php echo $registros ?></small>
                            </li>
                            <li class="paggerPoint" onclick="avancarPagina()"><a>Próximo</a></li>
                            <li class="paggerPoint" onclick="ultimaPagina()"><a>Último</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        <?php } ?>
        <script>
            function primeiraPagina() {
                if (document.getElementById('atual').value == "1")
                    return;
                document.getElementById('atual').value = '1';
                document.getElementById('max').value = '10';
                modalAguarde('show');
                document.getElementById('form_pesquisar').submit();
            }

            function ultimaPagina() {
                if (document.getElementById('max').value >= document.getElementById('numRows').value)
                    return;
                linhas = ((document.getElementById('numRows').value) / 10) + "";
                linhas = linhas.split(".");
                if (linhas[1] == null) {
                    document.getElementById('atual').value = parseInt(linhas[0] + '0') - 10;
                    document.getElementById('max').value = parseInt(linhas[0] + '0');
                    modalAguarde('show');
                    document.getElementById('form_pesquisar').submit();
                    return;
                }
                document.getElementById('atual').value = linhas[0] + '1';
                document.getElementById('max').value = parseInt(linhas[0] + '0') + 10;
                modalAguarde('show');
                document.getElementById('form_pesquisar').submit();
            }

            function avancarPagina() {
                max = parseInt(document.getElementById('max').value);
                numRows = parseInt(document.getElementById('numRows').value);
                if (max >= numRows)
                    return;
                document.getElementById('operador').value = '+';
                modalAguarde('show');
                document.getElementById('form_pesquisar').submit();
            }

            function recuarPagina() {
                atual = document.getElementById('atual').value;
                if (atual == '1')
                    return;
                document.getElementById('operador').value = '-';
                modalAguarde('show');
                document.getElementById('form_pesquisar').submit();
            }

            form = document.getElementById('form_pesquisar').innerHTML;
            campos = '<input type="hidden" class="form-control" name="max" id="max" value="">';
            campos += '<input type="hidden" class="form-control" name="atual" id="atual" value="">';
            campos += '<input type="hidden" class="form-control" name="operador" id="operador" value="">';
            campos += '<input type="hidden" class="form-control" name="numRows" id="numRows" value="">';
            document.getElementById('form_pesquisar').innerHTML = form + campos;
            <?php
            $post = $_POST;
            foreach ($post as $key => $value) {
                if ($key == 'operador')
                    continue;
                echo 'document.getElementById("' . $key . '").value="' . $value . '"; ';
            }
            ?>
        </script>
        <hr>
        <?php
    }

    private function carregardados()
    {
        $objeto = "c" . $this->getGrid_objeto();
        require_once(self::caminho . "c" . $this->getGrid_objeto() . ".php");
        $this->grid_instacia = new $objeto();
        $metodo = $this->getGrid_metodo();
        $retorno = $this->grid_instacia->$metodo();
        $this->setGrid_lista($retorno);
    }

    private function montaGrid()
    {
        ?>
        <hr> <?php
        if (is_array($this->getGrid_cols()) && !is_array($this->getGrid_lista())) {
            ?>
            <!--//constrói a mensagem caso tabela vazia-->
            <div class="row">
                <div class="col-sm-12">
                    <div class="alert alert-danger"
                         role="alert"> <?php echo $this->getGrid_msg_sem_resultados(); ?> </div>
                </div>
            </div>
            <?php
            return;
        }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <table class="table table-striped table-responsive table-hover table-condensed" id="table_grid">
                    <?php
                    $this->montaLinhasTH();
                    if ($this->getGrid_objeto() != null && $this->getGrid_metodo() != null) {
                        $indice = $this->montaLinhasTD();
                    } else {
                        //$indice = $this->montaLinhasTDArray();
                    }
                    ?>
                </table>
            </div>
        </div>
        <?php
        if ($this->getGrid_excluir() == 1) {
            formsHtml::modalExcluir();
            ?>
            <script>
                $('#checkExcluir').click(function () {
                    if ($('#checkExcluir').prop("checked")) {
                        $('.checkExcluir').prop("checked", true);
                    } else {
                        $('.checkExcluir').prop("checked", false);
                    }
                });
                $('#confirmarExcluir').click(function () {
                    contador = 0;
                    arrayLinhas = [];
                    linhas = $('.checkExcluir');
                    for (i = 0; i < linhas.length; i++) {
                        if (linhas[i].checked) {
                            arrayLinhas.push(linhas[i].value);
                        }
                    }
                    for (j = 0; j < arrayLinhas.length; j++) {
                        formdata = new FormData();
                        formdata.append("multiplos", '1');
                        formdata.append('id', arrayLinhas[j]);
                        i = 1;
                        $.ajax({
                            type: "POST",
                            url: '<?php print appConf::caminho ?><?php echo $this->getGrid_objeto()?>/controlSwitch/excluir',
                            data: formdata,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                contador += parseInt(retorno);
                                if (contador == arrayLinhas.length) {
                                    location.reload();
                                }
                            },
                            beforeSend: function (a) {
                                modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
                            }
                        });
                    }
                });
                $('#btn_grid_excluir').click(function () {
                    arrayLinhas = [];
                    linhas = $('.checkExcluir');
                    for (i = 0; i < linhas.length; i++) {
                        if (linhas[i].checked) {
                            arrayLinhas.push(linhas[i].value);
                        }
                    }

                    if (arrayLinhas.length == 0) {
                        msgBox("Selecione um registro antes de excluir!");
                        return;
                    }
                    $('#modalMsgBoxConfirmExcluir').modal('toggle');
                });
            </script>
            <?php
        }
        if ($this->getGrid_visualizar() == 1) {
            formsHtml::modalVisualizar();
            ?>
            <script>
                $(".btn_grid_visualizar").click(function () {
                    formdata = new FormData();
                    formdata.append('id', this.value);
                    $.ajax({
                        type: "POST",
                        url: '<?php print appConf::caminho ?><?php echo $this->getGrid_objeto()?>/controlSwitch/carregar',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        success: function (retorno) {
                            $("#visualizarrContent").html(retorno);
                        },
                        beforeSend: function (a) {
                            //modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
                        }
                    });
                });
            </script>

            <?php
        }
        if ($this->getGrid_editar() == 1) {
            formsHtml::modalEditar();
            ?>
            <script>
                $(".btn_grid_editar").click(function () {
                    formdata = new FormData();
                    formdata.append('id', this.value);
                    i = 1;
                    $.ajax({
                        type: "POST",
                        url: '<?php print appConf::caminho ?><?php echo $this->getGrid_objeto()?>/controlSwitch/editar',
                        data: formdata,
                        contentType: false,
                        processData: false,
                        success: function (retorno) {
                            $("#editarContent").html(retorno);
                        },
                        beforeSend: function (a) {
                            //modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
                        }
                    });
                });
            </script>

            <?php
        }
    }

    private function montaBotoesTabela($indice)
    {

        if ($this->getGrid_visualizar() == 1 || $this->getGrid_editar() == 1) {
            ?>
            <td class="text-right">
                <?php

                if ($this->getGrid_visualizar() == 1) {
                    formsHtml::modalVisualizar();
                    ?>
                    <button type="button" value="<?php echo $indice; ?>" alt="Visualizar"
                            class="btn btn-primary btn-xs btn_grid_visualizar" data-target="#modalVisualizar"
                            data-toggle="modal" id="btn_grid_visualizar">
                        <span class="glyphicon glyphicon-eye-open" style="padding:1px"></span></button>
                    <?php

                }
                if ($this->getGrid_editar() == 1) {
                    ?>
                    <button type="button" alt="Editar" value="<?php echo $indice; ?>"
                            class="btn btn-warning btn-xs btn_grid_editar" data-target="#modalEditar"
                            data-toggle="modal" id="btn_grid_editar">
                        <span class="glyphicon glyphicon-pencil " style="padding:1px"> </span></button>
                    <?php
                }
                ?>
            </td>
            <?php
        }
    }

    private function montaTitulo()
    {
        if ($this->getGrid_titulo() == null) {
            return;
        }
        ?>
        <div class="row">
            <div class="col-sm-12">
                <h3>
                    <?php echo $this->getGrid_titulo() ?>
                </h3>
                <p>
                    <?php echo $this->getGrid_sub_titulo() ?>
                </p>
            </div>
        </div>
        <?php
    }

    private function montaBotoesTopo()
    {
        if ($this->getGrid_cadastrar() != 1 && $this->getGrid_excluir() != 1) {
            ?>
            <br>
            <div class="row">
                <?php
                $this->montaPesquisa();
                ?>
            </div>
            <?php
            return;
        }
        ?>
        <br>
        <div class="row">
            <?php
            if ($this->montaPesquisa()) {
                echo '<div class="col-sm-12 col-md-6 text-right" style="padding-top: 2px;">';
            } else {
            ?>
            <div class="col-sm-12 text-right" style="padding-top: 2px;"><?php
                }
                if ($this->getGrid_botao_topo() != null) {
                    $botao = $this->getGrid_botao_topo();
                    ?>
                    <button type="button" class="btn  <?php echo $botao[3]; ?> " data-toggle="modal"
                            data-target="#modal<?php echo $botao[2]; ?>"
                            id="<?php echo $botao[2]; ?>">
                        <span class="glyphicon glyphicon-<?php echo $botao[4]; ?>"> </span><?php echo $botao[0]; ?>
                    </button>
                    <script>
                        $("#<?php echo $botao[2]; ?>").click(function () {
                            formularioCadastrar = requisicaoAjax('<?php print appConf::caminho ?><?php echo $this->getGrid_objeto()?>/controlSwitch/<?php echo $botao[1] ?>');
                            $("#<?php echo $botao[2]; ?>Content").html(formularioCadastrar);
                        });
                    </script> <?php
                    formsHtml::modalDefault($botao);
                }
                if ($this->getGrid_cadastrar() == 1) {
                    ?>
                    <button type="button" class="btn btn-success " data-toggle="modal" data-target="#modalCadastrar"
                            id="btn_grid_cadastrar">
                        <span class="glyphicon glyphicon-plus"> </span> Adicionar
                    </button>
                    <script>
                        $("#btn_grid_cadastrar").click(function () {
                            formularioCadastrar = requisicaoAjax('<?php print appConf::caminho ?><?php echo $this->getGrid_objeto()?>/controlSwitch/cadastrar');
                            $("#cadastrarContent").html(formularioCadastrar);
                        });
                    </script>
                    <?php
                    formsHtml::modalCadastrar();
                }
                if ($this->getGrid_excluir() == 1 && is_array($this->getGrid_lista())) {
                    ?>
                    <button type="button" class="btn btn-danger" id="btn_grid_excluir">
                        <span class="glyphicon glyphicon-trash"></span> Excluir
                    </button>
                <?php } ?>
            </div>
        </div>
        <?php
    }

    private function montaPesquisa()
    {
        if ($this->getGrid_pesquisar_cols() == null || $this->getGrid_pesquisar_nome() == null) {
            return false;
        }
        ?>
        <div class="col-sm-12 col-md-6 text-left" style="padding-top: 2px;">
            <div class="input-group">
                <form id="form_pesquisar"
                      action="<?php print appConf::caminho ?>c<?php echo $this->getGrid_objeto() ?>/controlSwitch/?>"
                      method="post">
                    <input type="search" class="form-control" id="pesquisaGrid" placeholder="Pesquisar">
                    <input type="hidden" name="isPesquisa" id="isPesquisa" value="1">
                    <?php
                    foreach ($this->getGrid_pesquisar_cols() as $campo) {
                        echo '<input type="hidden" class="form-control camposPesquisa" id="' . strtolower($campo) . '" name="' . strtolower($campo) . '"  placeholder="Pesquisar">';
                    }
                    ?>
                </form>
                <span class="input-group-btn">
                    <button class="btn btn-default" id="btnPesquisar" type="button"><span
                                class="glyphicon glyphicon-search"></span> Pesquisar </button>
                </span>
            </div>
            <script>
                $(document).ready(function () {
                    $(document).keypress(function (e) {
                        var code = null;
                        code = (e.keyCode ? e.keyCode : e.which);
                        if (code == 13)
                            $("#btnPesquisar").trigger("click");
                    });
                });
                $("#btnPesquisar").click(function () {
                    valor = $("#pesquisaGrid").val();
                    $(".camposPesquisa").val(valor);
                    document.getElementById("operador").value = "";
                    document.getElementById("max").value = "";
                    document.getElementById("atual").value = "";
                    $("#form_pesquisar").submit();
                });
            </script>
            <p style="font-size:12px ">
                <?php
                echo "<b>Pesquisar Por: </b>" . implode(" - ", $this->getGrid_pesquisar_nome());
                ?>
            </p>
        </div>
        <?php
        return true;
    }

    private function montaLinhasTD()
    {
        $indice = $_POST['atual'];
        if ($indice < 1)
            $indice = 1;
        foreach ($this->getGrid_lista() as $array) {
            if (is_object($array)) {
                ?>
                <tr id="<?php echo $array->getId(); ?>">
                <?php
                if ($this->getGrid_excluir() == 1) {
                    ?>
                    <td style="padding-right: 0px !important;">
                        <input type="checkbox" value="<?php echo $array->getId(); ?>" class="checkExcluir"/>
                    </td>
                    <?php
                }
            } else {
                ?>
                <tr id="<?php echo $array['id']; ?>">
                <?php
                if ($this->getGrid_excluir() == 1) {
                    ?>
                    <td style="padding-right: 0px !important;">
                        <input type="checkbox" value="<?php echo $array['id']; ?>" class="checkExcluir"/>
                    </td>
                    <?php
                }
            }
            ?>
            <td style="padding: 5px 5px 5px 2px !important;margin: 0px !important;">
                <?php echo "<b>" . $indice++ . "</b>"; ?>
            </td>
            <?php
            $j = 0;
            if ($this->getGrid_array_metodo() != null) {
                foreach ($this->getGrid_array_metodo() as $metodo) {
                    if (is_object($array)) {
                        if (strtolower($metodo) == 'id') {
                            ?>
                            <td style="display: none">
                            <?php
                        } else {
                            ?>
                            <td>
                            <?php
                        }
                        $metodo = "get" . $metodo;
                        $valor = $array->$metodo();
                        if ($this->validateDate($valor)) {
                            require_once('lib/appFunction.php');
                            $sis = new appFunction();
                            $valor = substr($sis->fn_formatarData($valor), 0, 10);
                            unset($sis);
                        }
                        echo $valor;
                        ?>
                        </td>
                        <?php
                    } else {
                        ?>
                        <td>
                            <?php
                            $valor = $array[$metodo];
                            if ($this->validateDate($valor)) {
                                require_once('lib/appFunction.php');
                                $sis = new appFunction();
                                $valor = substr($sis->fn_formatarData($valor), 0, 10);
                                unset($sis);
                            }
                            echo $valor;
                            ?>
                        </td>
                        <?php
                    }
                }
            }
            if (is_object($array)) {
                $this->montaBotoesTabela($array->getId());
            } else {
                $this->montaBotoesTabela($array['id']);
            }
            ?>
            </tr>
            <?php
        }
        return $indice;
    }

    private function validateDate($date, $format = 'Y-m-d H:i:s')
    {
        $d = DateTime::createFromFormat($format, $date);
        // The Y ( 4 digits year ) returns TRUE for any integer with any number of digits so changing the comparison from == to === fixes the issue.
        return $d && $d->format($format) === $date;
    }

    private function montaLinhasTDArray()
    {

        $indice = 0;
        foreach ($this->getGrid_lista() as $array) {
            ?>
            <tr id="<?php echo ++$indice ?>">
            <?php
            if ($this->getGrid_excluir() == 1) {
                ?>
                <td style="padding-right: 0px !important;">
                    <input type="checkbox" value="<?php echo $indice ?>" class="checkExcluir"/>
                </td>
                <?php
            }
            ?>
            <td style="padding: 5px 5px 5px 2px !important;margin: 0px !important;">
                <?php echo "<b>" . $indice . "</b>"; ?>
            </td>
            <?php
            foreach ($array as $value) {
                ?>
                <td>
                    <?php echo $value; ?>
                </td>
                <?php
            }
            $this->montaBotoesTabela($indice);
        }
        ?>
        </tr>
        <?php
        return $indice;

    }

    private function montaLinhasTH()
    {
        ?>
        <thead style="background-color: #dddddd;">
    <tr>
        <?php
        if ($this->getGrid_excluir() == 1) {
            ?>
            <th style="padding-right: 0px !important;">
                <input type="checkbox" id="checkExcluir"/>
            </th>
            <?php
        }
        ?>
        <th style="padding: 0px 5px 5px 5px !important;margin: 0px !important;">
            #
        </th>
        <?php
        foreach ($this->getGrid_cols() as $value) {
            if (strtolower($value) == 'id') {
                echo '<th style="display: none">';
            } else {
                echo '<th>';
            }
            echo $value; ?>
            </th>
            <?php
        }
        if ($this->getGrid_visualizar() == 1 || $this->getGrid_editar() == 1) {
            ?>
            <th class="text-right">
                Ações
            </th>
            <?php
        }
        ?>
    </tr>
        </thead><?php
    }

    public function getGrid_metodo()
    {
        return $this->grid_metodo;
    }

    public function setGrid_metodo($grid_metodo)
    {
        $this->grid_metodo = $grid_metodo;
    }

    public function getGrid_array_metodo()
    {
        return $this->grid_array_metodo;
    }

    public function setGrid_array_metodo($grid_array_metodo)
    {
        $this->grid_array_metodo = $grid_array_metodo;
    }

    public function getGrid_objeto()
    {
        return $this->grid_objeto;
    }

    public function setGrid_objeto($grid_objeto)
    {
        $this->grid_objeto = $grid_objeto;
    }

    public function getGrid_titulo()
    {
        return $this->grid_titulo;
    }

    public function setGrid_titulo($grid_titulo)
    {
        $this->grid_titulo = $grid_titulo;
    }

    public function getGrid_sub_titulo()
    {
        return $this->grid_sub_titulo;
    }

    public function setGrid_sub_titulo($grid_sub_titulo)
    {
        $this->grid_sub_titulo = $grid_sub_titulo;
    }

    public function getGrid_paginacao_qtde_pagina()
    {
        return $this->grid_paginacao_qtde_pagina;
    }

    public function setGrid_paginacao_qtde_pagina($grid_paginacao_qtde_pagina)
    {
        $this->grid_paginacao_qtde_pagina = $grid_paginacao_qtde_pagina;
    }

    public function getGrid_lista()
    {
        return $this->grid_lista;
    }

    public function setGrid_lista($grid_lista)
    {
        $this->grid_lista = $grid_lista;
    }

    public function getGrid_cols()
    {
        return $this->grid_cols;
    }

    public function setGrid_cols($grid_cols)
    {
        $this->grid_cols = $grid_cols;
    }

    public function getGrid_msg_sem_resultados()
    {
        return $this->grid_msg_sem_resultados;
    }

    public function setGrid_msg_sem_resultados($grid_msg_sem_resultados)
    {
        $this->grid_msg_sem_resultados = $grid_msg_sem_resultados;
    }

    public function getGrid_cadastrar()
    {
        return $this->grid_cadastrar;
    }

    public function setGrid_cadastrar($grid_cadastrar)
    {
        $this->grid_cadastrar = $grid_cadastrar;
    }

    public function getGrid_editar()
    {
        return $this->grid_editar;
    }

    public function setGrid_editar($grid_editar)
    {
        $this->grid_editar = $grid_editar;
    }

    public function getGrid_excluir()
    {
        return $this->grid_excluir;
    }

    public function setGrid_excluir($grid_excluir)
    {
        $this->grid_excluir = $grid_excluir;
    }

    public function getGrid_visualizar()
    {
        return $this->grid_visualizar;
    }

    public function setGrid_visualizar($grid_visualizar)
    {
        $this->grid_visualizar = $grid_visualizar;
    }

    public function getGrid_pesquisar_nome()
    {
        return $this->grid_pesquisar_nome;
    }

    public function setGrid_pesquisar_nome($grid_pesquisar_nome)
    {
        $this->grid_pesquisar_nome = $grid_pesquisar_nome;
    }

    public function getGrid_pesquisar_cols()
    {
        return $this->grid_pesquisar_cols;
    }

    public function setGrid_pesquisar_cols($grid_pesquisar_cols)
    {
        $this->grid_pesquisar_cols = $grid_pesquisar_cols;
    }

    public function getGrid_order_by_default()
    {
        return $this->grid_order_by_default;
    }

    public function setGrid_order_by_default($grid_order_by_default)
    {
        $this->grid_order_by_default = $grid_order_by_default;
    }

    public function getGrid_permissao()
    {
        return $this->grid_permissao;
    }

    public function setGrid_permissao($grid_permissao)
    {
        $this->setGrid_editar($grid_permissao->getEdicao());
        $this->setGrid_cadastrar($grid_permissao->getEscrita());
        $this->setGrid_excluir($grid_permissao->getExclusao());
        $this->setGrid_visualizar($grid_permissao->getVisualizacao());
    }

}
