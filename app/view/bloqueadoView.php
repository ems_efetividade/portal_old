<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 24/09/2018
 * Time: 10:55
 */

class bloqueadoView
{
    public function bloqueadoView()
    {
        ?>
        <head>
            <meta charset="utf-8">
            <title>Portal Efetividade - Grupo NC</title>
            <link rel="shortcut icon" href="https://www.gruponc.net.br/favicon.ico" type="image/x-icon"/>

            <link href="<?php echo appConf::caminho; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet"
                  type="text/css"/>

            <link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
                  type="text/css"/>
            <link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet"
                  type="text/css"/>

            <link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo appConf::caminho ?>public/css/ems_index.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css"/>
            <link href="<?php echo appConf::caminho ?>public/css/<?php echo $_SESSION['sigla'] ?>_index.css"
                  rel="stylesheet" type="text/css"/>


            <script type="text/javascript">
                function leave() {
                    window.location = "<?php echo appConf::caminho ?>";
                }
                setTimeout("leave()", 5000);
            </script>
        </head>
        <div class="w3-bar w3-personalizado w3-top " style="width:100%;height:50px">
            <a data-toggle="modal" data-target="#trocarEmpresa" class="w3-bar-item w3-button w3-logo " style="padding: px!important;padding-left: 15px!important;
                margin-right: 25px">
                <img height="27" style="margin-top: 12px" src="<?php echo appConf::caminho; ?>public/img/<?php echo $_SESSION['sigla'] ?>.png">
                <span class="hidden-xs" style="vertical-align: bottom; font-size:12px;position:relative;top:-4px;left:10px;font-weight:bold"><?php echo appFunction::dadoSessao('unidade_neg') ?></span>
            </a>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <br><br><br><br>
                    <img width="100" src="/../../public/img/block.png">
                    <h1 class="text-danger"><strong>401 - Acesso não autorizado</strong></h1>
                    <h3>Você não tem permissão de acesso a essa página.</h3>
                    <small class="text-muted"><strong>Para mais informações entre em contato com a administração do
                            Portal Efetividade, ou contate seu gestor.</strong></small>
                    <br><br>
                    <small class=""><strong>(Você será redirecionado para a página inicial)</strong></small>

                    <br><br><br><br><br><br>
                </div>
            </div>
        </div>
        <?php
        exit;
    }
}