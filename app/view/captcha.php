<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 07/11/2018
 * Time: 15:12
 */
$codigoCaptcha = substr(md5( time() ) ,0,5);
$_SESSION['captcha'] = $codigoCaptcha;
$imagemCaptcha = imagecreatefrompng(appFunction::caminho_fisico."/public/img/fundo.png");
$fonteCaptcha = imageloadfont(appFunction::caminho_fisico."/public/img/anonymus.gdf");
$corCaptcha = imagecolorallocate($imagemCaptcha,255,0,0);
imagestring($imagemCaptcha,$fonteCaptcha,15,5,$codigoCaptcha,$corCaptcha);
header("Content-type: image/png");
imagepng($imagemCaptcha);
imagedestroy($imagemCaptcha);