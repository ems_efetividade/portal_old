<?php
include ('vRelatorios.php');
$view  = new vRelatorios();
exit;
?>
<style>
.arvore-diretorio {
	max-height:500px;
	overflow:auto;	
}
.lista-permissao { border:1px solid #eee; height:340px; padding:5px; overflow:auto}
.vertical-container {
    display: table;
    width: 100%;
	height:340px;
}
.vertical-container .wow {
    display: table-cell;
    vertical-align: middle;
	text-align:center;
}

.sel-pasta-mover {
	background-color:#eee;
	border:1px solid #ddd;	
}

.container { width:100%; }
body { padding-top: 70px; }
#fileArquivo { display:none }
.nome-diretorio { border-bottom:1px solid #eee; margin-bottom:10px;padding-bottom:10px;}
.menu-raiz {
	list-style:none;
	padding:0px;	
}

.menu-raiz ul {
	padding:0px;
	padding-left:10px;
	
}
.menu-raiz li {
	list-style:none;
	padding:5px;
	
}
.folder {
	margin-left:15px;
	padding-left:20px;
	width:16px;
	height:16px;
	background:url(<?php echo appConf::caminho?>public/img/foldermini.png) no-repeat center left;	
}

.folder-open, .folder-close, .abrir-diretorio {
	cursor:pointer;	
}
#lista-diretorio .abrir-diretorio {
	padding:5px;	
}
.folder-open {
	background:url(<?php echo appConf::caminho ?>public/img/arrow-right2.png) no-repeat top left;	
}

.folder-close {
	background:url(<?php echo appConf::caminho ?>public/img/arrow-down2.png) no-repeat top left;	
}
.sub-diretorio {display:none}
.icone-folder{ margin-right:3px; }
.img-folder {
	width:45px;
	height:37px;
	background:url(<?php echo appConf::caminho?>public/img/closedfolder.png) no-repeat center left;	
}



@media all and (min-width: 768px) and (max-width: 1024px) {
   .j-hidden-sm{
      display: none !important;
   }
}


</style>
<script>
var aguarde;
$(document).ready(function(e) {
    
	aguarde = requisicaoAjax('<?php echo appConf::caminho ?>diretorio/barraProgresso');
	
	//modal editar/adicionar diretorio
	$('#modalEditarPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		
		var idDiretorio = button.data('id')
		var idRaiz = button.data('raiz') // Extract info from data-* attributes
		
		diretorio = $.parseJSON(requisicaoAjax('<?php echo appConf::caminho ?>diretorio/selecionarDiretorio/'+idDiretorio+'/'+idRaiz));
		
		$('#formSalvarDiretorio #txtNomeDiretorio').val(diretorio.pasta);
		$('#formSalvarDiretorio #txtIdDiretorio').val(diretorio.idDiretorio);
		$('#formSalvarDiretorio #txtIdPai').val(diretorio.idRaiz);

	});
	
	
	$('#modalEditarArquivo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idArquivo = button.data('id');
		arquivo = $.parseJSON(requisicaoAjax('<?php echo appConf::caminho ?>diretorio/selecionarArquivo/'+idArquivo));
		

		$('#formSalvarArquivo #txtIdArquivo').val(arquivo.idArquivo);
		$('#formSalvarArquivo #txtNomeArquivo').val(arquivo.arquivo);
		
		$('#btnSalvarArquivo').click(function(e) {
            sendForm('#formSalvarArquivo', '#modalEditarArquivo', arquivo.idPasta);
        });
		
	});
	
	
	$('#modalExcluirPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var idDiretorio = button.data('id');
		var idRaiz = button.data('raiz');
		
		$('#formExcluirDiretorio #txtIdDiretorio').val(idDiretorio);
		$('#formExcluirDiretorio #txtIdRaiz').val(idRaiz);
	});
	

	
	
	
	$('#modalPermissaoPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idDiretorio = button.data('id')
		
		
		diretorioPermissao = idDiretorio;
		htmlLista = requisicaoAjax('<?php echo appConf::caminho ?>diretorio/listaPermissaoDiretorio/');
		htmlPermissao = requisicaoAjax('<?php echo appConf::caminho ?>diretorio/permissaoDiretorio/'+idDiretorio);
		
		$('#permissao-diretorio').html("");
		$('#perfil-completo').html("");
		$('#perfil-completo').scrollTop(0);
		
		$('#perfil-completo').html(htmlLista);
		$('#permissao-diretorio').html(htmlPermissao);
		
		
		
		$('#txtIdPastaPermissao').val(idDiretorio);
		$('#txtPesquisarPerfil').val('');
		
		$('#btnAdicionarPermissao').click(function(e) {
            mover('#perfil-completo', '#permissao-diretorio');
        });
		
		$('#btnRemoverPermissao').click(function(e) {
           	$('#permissao-diretorio input:checkbox:checked').each(function () {
				$(this).closest('.checkbox').remove();
			});
        });
		
		
		
		$('#btnSalvarPermissao').click(function(e) {
			
			$.ajax({
				type: 'POST',
				url: '<?php echo appConf::caminho ?>diretorio/salvarPermissao/',
									
				data: {
					permissao:getPermitidos(),
					idPasta:diretorioPermissao
				},
																			 
				beforeSend: function() {
					modalAguarde('show');
				},
				complete: function(){
					modalAguarde('hide');
				},
																				
				success: function(retorno){	
					$('#modalPermissaoPasta').modal('hide');
					//e.stopImmediatePropagation();
					//$('#modalPermissaoPasta').modal('hide');
				}			
			});
			
			e.preventDefault();
			e.stopImmediatePropagation();
			
		});
	});
	
	
	
		//modal editar/adicionar diretorio
	$('#modalVisualizarPermissao').on('show.bs.modal', function (event) {		
		$.ajax({
			type: 'POST',
			url: '<?php echo appConf::caminho ?>diretorio/visualizarPermissao',
								
			data: {
				permissao:getPermitidos()
			},
																		 
			beforeSend: function() {
				modalAguarde('show');
			},
			complete: function(){
				modalAguarde('hide');
			},
																			
			success: function(retorno){	
				$('#visualizar-permissao').html(retorno);
				//$('#modalVisualizarPermissao').modal('show');
			}			
		});
	});
	
	$('#modalMoverConteudo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) ;// Button that triggered the modal
		var idRaiz = button.data('id');
		var acao = button.data('acao');
		
		$('#modalMoverConteudo .sub-diretorio').toggle();
				
		$('#lista-diretorio').html(requisicaoAjax('<?php echo appConf::caminho ?>diretorio/menuPadraoMover/'));
		$('#txtMoverPARA').val(0);
		$('#txtMoverDE').val(idRaiz);
		
		//ativa o menu raiz
		$('#lista-diretorio .menu-raiz .raiz').click(function(e) {
			idDiretorio = $(this).attr('diretorio');
			subMenuMover(idDiretorio);
		});
		
		$('#lista-diretorio .abrir-diretorio').click(function(e) {
			$('#lista-diretorio .abrir-diretorio').removeClass('sel-pasta-mover');
			$(this).addClass('sel-pasta-mover');
			$('#txtMoverPARA').val($(this).attr('diretorio'));
		});
		
		
		
		$('#btnMoverConteudo').click(function(e) {
			de   = $('#txtMoverDE').val();
			para = $('#txtMoverPARA').val();
			
			//alert(acao);
			
			if(acao == "tudo") {
				//requisicaoAjax('<?php echo appConf::caminho ?>diretorio/moverTudo/'+de+'/'+para);
				//abrirDiretorio(de);
				
				
				$.ajax({
					type: 'POST',
					url: '<?php echo appConf::caminho ?>diretorio/moverTudo/'+de+'/'+para,
																				 
					beforeSend: function() {
						modalAguarde('show');
					},
					complete: function(){
						modalAguarde('hide');
					},
																					
					success: function(retorno){
						abrirDiretorio(de);
					}			
				});
				
				
				
			} else {
				
				
					
				$.ajax({
					type: 'POST',
					url: '<?php echo appConf::caminho ?>diretorio/moverArquivo/'+para,
										
					data: $('#formArquivo').serialize(),
																				 
					beforeSend: function() {
						modalAguarde('show');
					},
					complete: function(){
						modalAguarde('hide');
					},
																					
					success: function(retorno){	
						if(retorno == "") {
							abrirDiretorio(de);
						} else {
							msgBox(retorno);	
						}
					
					}
					
				});
			}
			
			
		});

		
	});
	
	$('#modalExcluirConteudo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var idDiretorio = button.data('id');
		
		$('#btnExcluirConteudo').click(function(e) {
            requisicaoAjax('<?php echo appConf::caminho ?>diretorio/excluirTudo/'+idDiretorio);
			abrirDiretorio(idDiretorio);
        });
	});
	
	$('#modalExcluirSelecionado').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var idDiretorio = button.data('id');
		
		$('#btnExcluirSelecionado').click(function(e) {
			$.ajax({
				type: 'POST',
				url: '<?php echo appConf::caminho ?>diretorio/excluirArquivos',
									
				data: $('#formArquivo').serialize(),
																			 
				beforeSend: function() {
					modalAguarde('show');
				},
				complete: function(){
					modalAguarde('hide');
				},
																				
				success: function(retorno){	
					abrirDiretorio(idDiretorio);
					//$('#visualizar-permissao').html(retorno);
					//$('#modalVisualizarPermissao').modal('show');
				}			
			});
		});
	});
	
	
	

	
	// salva o formulario de editar/adicionar pasta
	$('#btnExcluirDiretorio').click(function(e) {
		sendForm("#formExcluirDiretorio", "#modalExcluirPasta", $('#formExcluirDiretorio #txtIdRaiz').val());
    });
	
	
	
	// salva o formulario de editar/adicionar pasta
	$('#btnSalvarDiretorio').on("click", function(){
		console.log('clique');
		sendForm("#formSalvarDiretorio", "#modalEditarPasta", $('#txtIdPai').val());
    });
	
	//ativa o menu raiz
	$('#painel-esquerdo .raiz').click(function(e) {
        idDiretorio = $(this).attr('diretorio');
		subMenu(idDiretorio);
    });
	
	$('#txtPesquisarPerfil').keyup(function(e) {
		var val = $(this).val().toUpperCase();
		if(val == "") {
			$("#perfil-completo .check-usuario").closest('.checkbox').show();	
		} else {
			$("#perfil-completo .check-usuario").closest('.checkbox').hide();	
			$("#perfil-completo .check-usuario[usuario*='"+val+"'").closest('.checkbox').show();
		}
	});
	
	

	
	
	setAbrirDiretorio('.arvore-diretorio ');
	
});

function sendForm(nomeForm, modal, idDiretorio) {
	i=0;
	//$(nomeForm).submit(function(e) {
		console.log(i++);
		  //e.preventDefault();
 
		  $.ajax({
			  type: "POST",
			  url: $(nomeForm).attr('action'),
			  data: $(nomeForm).serialize(),
			  success: function(u) {
								
				  if(u == '') {
					  abrirDiretorio(idDiretorio);
					  $(modal).modal('hide');
					  
				  } else {
                                        
					  alert(u);
				  }
			  }
		  });	
	  //});
		
	//$(nomeForm).submit();
}

function abrirDiretorio(idDiretorio) {
	$.ajax({
		
		type: 'POST',
		url: '<?php echo appConf::caminho ?>diretorio/abrir/'+idDiretorio,
																   
		beforeSend: function() {
			$('#conteudo-diretorio').html(aguarde);
		},
		
		complete: function(){
			$('#conteudo-diretorio').html('');
		},
																	  
		success: function(retorno){	
			subMenu(idDiretorio);
			//$('#painel-esquerdo #diretorio'+idDiretorio).click();
			listarArquivos(1, idDiretorio);
		}			
	});
}

function listarArquivos(pagina, diretorio) {
	if (pagina === undefined) {
		pagina = 1;
	}
	
	$.ajax({
	  
	  type: 'POST',
	  url: '<?php echo appConf::caminho ?>diretorio/listarArquivos/'+pagina+'/'+diretorio,
																   
	  beforeSend: function() {
		 // modalAguarde('show');
		 //$('.conteudo-diretorio').html(aguarde);
	  },
	  complete: function(){
		  //modalAguarde('hide');
	  },
																	  
	  success: function(retorno){	
	  	$('#conteudo-diretorio').hide();
	  	$('#conteudo-diretorio').html(retorno);
		$('#conteudo-diretorio').show();
		 
		 
		//$('#diretorio'+idDiretorio).click();
		setAbrirDiretorio('#conteudo-diretorio');

		$('.paginacao').click(function(e) {
			if($(this).closest('li').hasClass('disabled') == false) {
            	listarArquivos($(this).attr('pagina'), diretorio);
			}
        });
		
		
		$('#btnSelecionarTudo').click(function(e) {
			$('.chk').prop('checked', true);
		});
		
		$('#btnRemoverSelecao').click(function(e) {
			$('.chk').prop('checked', false);
		});
		
		
		$('#btnPesquisar').click(function(e) {
			

			
			$('#formPesquisar').submit(function(e) {
				  e.preventDefault();
				  $.ajax({
					  type: "POST",
					  url: $(this).attr('action'),
					  data: $(this).serialize(),
					  beforeSend: function() {
						 $('#conteudo-diretorio').html(aguarde);
					  },
					  complete: function(){
						  //modalAguarde('hide');
					  },
					  success: function(u) {
						$('#conteudo-diretorio').html(u);
						listarPesquisa(1, idDiretorio);
					  }
				  });	
			  });
				
			$('#formPesquisar').submit();
		});
		
		
		$('#btnUpload').click(function(e) {
			$('#fileArquivo').click();
			
			$('#fileArquivo').change(function(e) {
				//modalAguarde('show');
				//e.preventDefault();
				form = $('#formUploadFile');

				
				$.ajax({
						type: "POST",
						url: $('#formUploadFile').attr('action'),
						data: new FormData($('#formUploadFile')[0]),
						contentType: false,
						processData: false,
						beforeSend: function() {
							modalAguarde('show');
						},
						complete: function(e){
							console.log(e);
							modalAguarde('hide');
						},
						success: function(e) {
							abrirDiretorio(diretorio);
							listarArquivos(1, diretorio);
							//alert(e);
							if($.trim(e) != "") {
								alert(e);
							}
							
							
						}
					});
				
				
				
				
				
				
				//$('#formUploadFile').submit(function(e) {
				//	e.preventDefault();
					
				//});
				
				//$('#formUploadFile').submit();
			});
		});
		
		

		  
	  }			
  });
}


function listarPesquisa(pagina, diretorio) {
	if (pagina === undefined) {
		pagina = 1;
	}
	
	$.ajax({
	  
	  type: 'POST',
	  url: '<?php echo appConf::caminho ?>diretorio/listarPesquisa/'+pagina+'/'+diretorio,
																   
	  beforeSend: function() {
		 // modalAguarde('show');
		 //$('.conteudo-diretorio').html(aguarde);
	  },
	  complete: function(){
		  //modalAguarde('hide');
	  },
																	  
	  success: function(retorno){	
	  	$('#conteudo-diretorio').hide();
	  	$('#conteudo-diretorio').html(retorno);
		$('#conteudo-diretorio').show();
		 
		 
		//$('#diretorio'+idDiretorio).click();
		setAbrirDiretorio('#conteudo-diretorio');

		$('.paginacao').click(function(e) {
			if($(this).closest('li').hasClass('disabled') == false) {
            	listarArquivos($(this).attr('pagina'), diretorio);
			}
        });
		
		
		$('#btnSelecionarTudo').click(function(e) {
			$('.chk').prop('checked', true);
		});
		
		$('#btnRemoverSelecao').click(function(e) {
			$('.chk').prop('checked', false);
		});
		
		
		$('#btnPesquisar').click(function(e) {
			

			
			$('#formPesquisar').submit(function(e) {
				  e.preventDefault();
				  $.ajax({
					  type: "POST",
					  url: $(this).attr('action'),
					  data: $(this).serialize(),
					  beforeSend: function() {
						 $('#conteudo-diretorio').html(aguarde);
					  },
					  complete: function(){
						  //modalAguarde('hide');
					  },
					  success: function(u) {
						$('#conteudo-diretorio').html(u);
						listarPesquisa(1, idDiretorio);
					  }
				  });	
			  });
				
			$('#formPesquisar').submit();
		});
		
		
		  
	  }			
  });
}

function subMenuMover(idDiretorio) {
	//$('#txtMoverPARA').val(idDiretorio);
	modal = '#modalMoverConteudo #lista-diretorio';
	obj = modal + ' #subDiretorio'+idDiretorio;
	html = requisicaoAjax('<?php echo appConf::caminho ?>diretorio/subMenu/'+idDiretorio);
	$(obj).html(html);
	$(obj).toggle();
	$(obj+ ' .raiz').click(function(e) {
        idSubDiretorio = $(this).attr('diretorio');
		subMenuMover(idSubDiretorio);
    });
	$('#lista-diretorio .abrir-diretorio').click(function(e) {
		$('#lista-diretorio .abrir-diretorio').removeClass('sel-pasta-mover');
		$(this).addClass('sel-pasta-mover');
		$('#txtMoverPARA').val($(this).attr('diretorio'));
	});
	alterarIconeMenu('#lista-diretorio #diretorio'+idDiretorio);
}


function subMenu(idDiretorio) {
	obj = '#subDiretorio'+idDiretorio;	
	html = requisicaoAjax('<?php echo appConf::caminho ?>diretorio/subMenu/'+idDiretorio);
	//$('#painel-esquerdo #subDiretorio'+idDiretorio+' .sub-diretorio').hide();
	$(obj).html(html);
	setToggle(idDiretorio);
	$(obj+ ' .raiz').click(function(e) {
        idSubDiretorio = $(this).attr('diretorio');
		subMenu(idSubDiretorio);
    });
	setAbrirDiretorio(obj);
	alterarIconeMenu('#diretorio'+idDiretorio);
}
function setAbrirDiretorio(div) {
	$(div + ' .abrir-diretorio').click(function(e) {
       idDiretorio = $(this).attr('diretorio');
	   abrirDiretorio(idDiretorio);
    });
}
function setToggle(id) {
	$('#subDiretorio'+id).slideToggle('fast');	
}
function alterarIconeMenu(no) {
	classe = $(no).attr('class');
	if($(no).hasClass('folder-open')) {
		$(no).removeClass('folder-open');
		$(no).addClass('folder-close');
	} else {
		$(no).removeClass('folder-close');
		$(no).addClass('folder-open');
	}
	//$(no).attr('src', llink);
}
function mover(de, para) {
	$(de + ' input:checkbox:checked').each(function () {
		valorDE = $(this).attr('usuario');
		valorPARA = $(para + ' input:checkbox[usuario="'+valorDE+'"]').attr('usuario');
		checkbox = '<div class="checkbox" style="margin:0px;">' + $(this).closest('.checkbox').html() + '</div>';
		if(valorPARA === undefined) {
			$(para).html($(para).html() + checkbox);
		}
		$(de + ' input:checkbox[usuario="'+valorDE+'"]').prop('checked', false);
	});
}
function getPermitidos() {
	var array_final = [];
	$('#permissao-diretorio .check-usuario').each(function(index, element) {
		var array_add = {};
		array_add.id_perfil = $(this).attr('id_perfil');
		array_add.id_setor  = $(this).attr('id_setor');
		array_add.id_linha  = $(this).attr('id_linha');
		array_final.push(array_add);
	});
	return array_final;
}
</script>
<div class="container">
	<div class="row">
    	<div class="col-lg-3 col-md-4 col-xs-4" style="border-right:1px solid #eee;min-height:400px;">
            <p class="lead"><span class="glyphicon glyphicon-stats"></span> Relatórios</p>
            <div class="arvore-diretorio" id="painel-esquerdo">
				<?php echo $menuPadrao ?>
            </div>
        </div>
        <div id="conteudo-diretorio" class="col-lg-9 col-md-8  col-xs-8" style="min-height:400px;">
			<?php echo $paginaInicial ?>
        </div>
    </div>
</div>
<?php if(appFunction::dadoSessao('perfil') == 0) { ?>
<!--MODAL EDITAR/ADICIONAR NOVA PASTA-->
<div class="modal fade" id="modalEditarPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-file"></span> Criar/Editar Diretório</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Nome do Diretório</label>
            <form id="formSalvarDiretorio" method="post" action="<?php echo appConf::caminho ?>diretorio/salvardiretorio">
            	<input type="hidden" id="txtIdDiretorio" name="txtIdDiretorio" value="0" />
            	<input type="hidden" id="txtIdPai" name="txtIdPai" value="0" />
            	<input type="text" class="form-control" id="txtNomeDiretorio" name="txtNomeDiretorio" placeholder="">
            </form>
          </div>
        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-md" id="btnSalvarDiretorio"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL EXCLUIR PASTA-->
<div class="modal fade" id="modalExcluirPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img src="<?php echo appConf::caminho ?>public/img/warning.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja Excluir essa pasta e todas as suas subpastas?</h4>
            </div>
        </div>
        <div class="form-group">
            <form id="formExcluirDiretorio" method="post" action="<?php echo appConf::caminho ?>diretorio/excluirDiretorio">
            	<input type="hidden" id="txtIdDiretorio" name="txtIdDiretorio" value="0" />
                <input type="hidden" id="txtIdRaiz" name="txtIdRaiz" value="0" />
            </form>
          </div>          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" id="btnExcluirDiretorio"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL PERMISSAO-->
<div class="modal fade" id="modalPermissaoPasta" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Permissões</h4>
      </div>
      <div class="modal-body max-height">
       	<div class="row">
        	<div class="col-lg-5" style="padding-right:0px;">
            	<p><strong>Usuários/Perfis</strong></p>
                <div class="form-group">
                    <input type="text" class="form-control text-uppercase input-sm" id="txtPesquisarPerfil" placeholder="">
                </div>
            	<div class="lista-permissao" id="perfil-completo">
                <?php print $listaPermissao ?>
                </div>
            </div>
            <div class="col-lg-2" >
            	<div class="vertical-container">
            		<div class="wow">
                    	<p><button class="btn btn-default" id="btnAdicionarPermissao" /><span class="glyphicon glyphicon-chevron-right"></span> </button></p>
                        <p><button class="btn btn-default" id="btnRemoverPermissao" /><span class="glyphicon glyphicon-chevron-left"></span> </button></p>
                    </div>
                </div>
            </div>
            <div class="col-lg-5" style="padding-left:0px;">
                <input type="hidden" name="txtIdPastaPermissao" id="txtIdPastaPermissao" value="" />
                <div class="row">
                	<div class="col-lg-6">
                		<p><strong>Usuários Permitidos</strong></p>
                     </div>
                	<div class="col-lg-6"><div class="pull-right">
                		<button data-toggle="modal" data-id="18" data-target="#modalVisualizarPermissao" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-pencil"></span> Usuarios permitidos</button></div>
                     </div>
                </div>
                <p></p>
            	<div class="lista-permissao" id="permissao-diretorio"></div>
            </div>
        </div>
	  </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        <button type="submit" id="btnSalvarPermissao" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>


<!--MODAL ver PERMISSAO-->
<div class="modal fade" id="modalVisualizarPermissao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header btn-default">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Permissões de Acesso - Usuários Permitidos</h5>
      </div>
      <div class="modal-body" style="max-height:340px;overflow:auto">
      	<div id="visualizar-permissao"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL SELECIONAR PASTA-->
<div class="modal fade" id="modalSelecionarPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>
      <div class="modal-body">
      	<div id="divSelecionarPasta"></div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnMoverArquivos"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL MOVER TUDO-->
<div class="modal fade" id="modalMoverConteudo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Mover Arquivos</h4>
      </div>
      <div class="modal-body" style="height:340px; overflow:auto">
      	<div id="lista-diretorio">
			<?php echo $menuPadrao ?>
        </div>
        <input type="hidden" id="txtMoverDE" name="txtMoveDE" value="0" />
        <input type="hidden" id="txtMoverPARA" name="txtMovePARA" value="0" />
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-md" data-dismiss="modal" id="btnMoverConteudo"><span class="glyphicon glyphicon-refresh"></span> Mover</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL EXCLUIR TUDO-->
<div class="modal fade" id="modalExcluirConteudo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Mover Arquivos</h4>
      </div>
      <div class="modal-body">
      	Deseja excluir todos os arquivos dessa pasta?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnExcluirConteudo"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL EXCLUIR TUDO-->
<div class="modal fade" id="modalExcluirSelecionado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Mover Arquivos</h4>
      </div>
      <div class="modal-body">
      	Deseja excluir todos os arquivos selecionados?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnExcluirSelecionado"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>
<!--MODAL EDITAR ARQUIVO-->
<div class="modal fade" id="modalEditarArquivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Arquivo</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
            <label for="exampleInputEmail1">Nome do Arquivo</label>
            <form id="formSalvarArquivo" method="post" action="<?php echo appConf::caminho ?>relatorio/salvarArquivo">
            	<input type="hidden" id="txtIdArquivo" name="txtIdArquivo" value="0" />
            	<input type="text" class="form-control" id="txtNomeArquivo" name="txtNomeArquivo" placeholder="">
            </form>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-md" id="btnSalvarArquivo"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>
<?php }
	include('footerView.php') 
?>