
<!--Rodapé-->
<!-- barra final -->
</div>
<div class="jumbotron" id="home-rodape">
    <div class="container">
        <div class="row">
        
        	<div class="col-lg-4 col-sm-12">
                    
                <p class="header-box"><strong>Sobre o Grupo NC</strong></p>
                        <h4><small style="line-height: 1.5;">
                    O Grupo NC é um conglomerado de empresas brasileiras controladas pelo empresário Carlos Sanchez com sede em São Paulo, fundado em 2014. Compreende atividades na indústria farmacêutica, na área de incorporação, urbanismo, private equity e energia eólica, além de controlar veículos de comunicação (que incluem estações de rádio e TV, além de jornais) no estado de Santa Catarina. A principal empresa controlada pelo conglomerado é a farmacêutica EMS.
                    </small></h4>
            </div>
        
            <div class="col-lg-3 col-sm-12"></div>



        <div class="col-lg-5 col-sm-12">
            <p class="header-box"><strong>Nosso Jeito de Ser</strong></p>
            
            <div class="row">
                
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <img style="border-radius: 6px !important;" height="160" class="img-respodnsive img-rounded" src="<?php echo appConf::caminho ?>public/img/njs_vamos.png" alt="" srcset="">
                    <img style="border-radius: 6px !important;" height="160" class="img-ressponsive img-rounded" src="<?php echo appConf::caminho ?>public/img/njs_ousamos.png" alt="" srcset="">
                    <img style="border-radius: 6px !important;" height="160" class="img-respsonsive img-rounded" src="<?php echo appConf::caminho ?>public/img/njs_valorizamos.png" alt="" srcset="">
                    <img style="border-radius: 6px !important;" height="160" class="img-sresponsive img-rounded" src="<?php echo appConf::caminho ?>public/img/njs_fazemos.png" alt="" srcset="">
                </div>

               
                
            </div>
            


        </div>


        
           
        
<!--            <div class="col-lg-3 col-sm-12 col-md-12">
            	<div class="pull-right">
                    <p class="header-box"><strong>Nossas Linhas</strong></p>
                    <h3><SMALL>Sirius</SMALL></h3>
                    <h3><SMALL>Vital</SMALL></h3>
                    <h3><SMALL>Sinapse</SMALL></h3>
                    <h3><SMALL>Fênix</SMALL></h3>
                    <h3><SMALL>Clinica</SMALL></h3>
                    <h3><SMALL>Saúde</SMALL></h3>
                </div>
            </div>-->
            
        </div>
    </div>
</div>

<!-- copyright -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
        <h5><p class="text-center"><small>Efetividade EMS Prescrição</small></p></h5></div>
    </div>
</div>


</body>
</html>