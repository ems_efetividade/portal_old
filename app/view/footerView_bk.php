
<!--Rodapé-->
<!-- barra final -->
<div class="jumbotron" id="home-rodape">
    <div class="container">
        <div class="row">
        
        	<div class="col-lg-6 col-sm-12">
                    
                <p class="header-box"><strong>Sobre o Grupo NC</strong></p>
                        <h3><small>
                    O Grupo NC é um conglomerado de empresas brasileiras controladas pelo empresário Carlos Sanchez com sede em São Paulo, fundado em 2014. Compreende atividades na indústria farmacêutica, na área de incorporação, urbanismo, private equity e energia eólica, além de controlar veículos de comunicação (que incluem estações de rádio e TV, além de jornais) no estado de Santa Catarina. A principal empresa controlada pelo conglomerado é a farmacêutica EMS.
                    </small></h3>
<!--                <h3 class="text-justify"><small>Fundada há mais de 50 anos e com capital 100% nacional, a EMS é a líder do mercado farmacêutico brasileiro há dez anos consecutivos, pertencente ao Grupo NC. Esse sucesso resulta do investimento constante em pesquisa e desenvolvimento, da moderna infraestrutura fabril, do foco em inovação, da agilidade e pioneirismo no lançamento de produtos, da sinergia entre as diversas unidades de negócio e do talento de milhares de colaboradores. </small></h3>
                <h3 class="text-justify"><small>A empresa ocupa também a liderança no segmento de genéricos desde 2013 e está entre os maiores laboratórios em preferência prescritiva no Brasil. Esta big pharma tem forte presença em PDVs de todo o Brasil e atuação nas áreas de prescrição médica, genéricos, medicamentos de marca, OTC e hospitalar, fabricando produtos para praticamente todas as especialidades da Medicina. Tem o maior portfólio do setor, além de apoiar continuamente ações de responsabilidade social.</small></h3>-->
            </div>
        
             <div class="col-lg-6 col-sm-12 col-md-12">
                <div class="pull-right">
                    <p class="header-box"><strong>Valores</strong></p>
                    <h3><SMALL>Responsabilidade</SMALL></h3>
                    <h3><SMALL>Ousadia</SMALL></h3>
                    <h3><SMALL>Simplicidade</SMALL></h3>
                    <h3><SMALL>Excelência</SMALL></h3>
                    <h3><SMALL>Valorização de Pessoas</SMALL></h3>
                </div>
            </div>
        
<!--            <div class="col-lg-3 col-sm-12 col-md-12">
            	<div class="pull-right">
                    <p class="header-box"><strong>Nossas Linhas</strong></p>
                    <h3><SMALL>Sirius</SMALL></h3>
                    <h3><SMALL>Vital</SMALL></h3>
                    <h3><SMALL>Sinapse</SMALL></h3>
                    <h3><SMALL>Fênix</SMALL></h3>
                    <h3><SMALL>Clinica</SMALL></h3>
                    <h3><SMALL>Saúde</SMALL></h3>
                </div>
            </div>-->
            
        </div>
    </div>
</div>

<!-- copyright -->
<div class="container">
    <div class="row">
        <div class="col-lg-12">
        <h5><p class="text-center"><small>Efetividade EMS Prescrição</small></p></h5></div>
    </div>
</div>


</body>
</html>