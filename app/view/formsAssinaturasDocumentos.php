<?php
class formsAssinaturasDocumentos{

    public function formEditar($objeto){?>
    <form id="formEditar" action="<?php print appConf::caminho ?>AssinaturasDocumentos/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">NOME</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="arquivo">ARQUIVO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getArquivo()?>" name="arquivo" id="arquivo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">ATIVO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAtivo()?>" name="ativo" id="ativo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DATAIN</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getDataIn()?>" name="datain" id="dataIn" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($objeto){?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">NOME</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="arquivo">ARQUIVO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getArquivo()?>" name="arquivo" id="arquivo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">ATIVO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getAtivo()?>" name="ativo" id="ativo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DATAIN</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getDataIn()?>" name="datain" id="dataIn" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar(){?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>AssinaturasDocumentos/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="nome">NOME</label>
                        <input required type="text" class="form-control " name="nome" id="nome" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="arquivo">ARQUIVO</label>
                        <input required type="text" class="form-control " name="arquivo" id="arquivo" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">ATIVO</label>
                        <input required type="text" class="form-control " name="ativo" id="ativo" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">DATAIN</label>
                        <input required type="text" class="form-control " name="datain" id="dataIn" >
                    </div>
                </div>
            </div>
        </form><?php
    }
}
