<?php
/**
 * Created by PhpStorm.
 * User: jeanc
 * Date: 16/07/2018
 * Time: 16:54
 */
require_once ('/../view/abs/gridView.php');
include_once('/../controller/cRelatoriosController.php');
class formsRelatorios
{
    public function formUpload()
    {
        ?>
        <form id="formUpload" action="<?php echo appConf::caminho ?>cRelatorios/salvarArquivos" enctype="multipart/form-data">
            <div class="row text-left ">
                <div class="col-sm-7 text-left opl_div" style="display: none">
                    <div class="form-group form-group-sm" >
                        <label for="termo" > Arquivo</label >
                        <label  for="termo"  class="form-control"  id="nomeTermo" >
                        </label>
                    </div>
                </div>
                <div class="col-sm-1 text-left opl_div" >
                    <div class="form-group form-group-sm" >
                        <label for="termo" style="color:white;" >.</label><br>
                        <label for="termo" class="">
                        </label >
                        <input required style="display: none;" type="file" onchange="gerarListaUpload()" multiple="multiple" name="termo" id="termo" >
                    </div>
                </div>

                <div class="col-sm-12"  style="height:450px;overflow-x: scroll">
                    <table class="table tb_div table-responsive table-condensed" id="arquivos" style="display:none;" >
                        <thead>
                        <tr class="active">
                            <th class="text-left"> #</th>
                            <th class="text-left"> Nome</th>
                            <th class=""> Tipo</th>
                            <th class=""> Tamanho</th>
                            <th class=""> Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>

                 <div style="display:;" id="barraProgresso"  class="col-sm-12 text-left">
                 <br>
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" id="barra" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" >
                        <span id="textobarra">AGUARDE...</span>
                    </div>
                </div>
                <i id="contadorArqu" class="text-center">0 de 0 arquivo(s) enviado(s) </i><i id="tempo"></i>
                <!--img id="centro" style="align-content:center!important;" src="https://media.giphy.com/media/3oEdv5FBPbfMJbraBG/giphy.gif"/-->
            </div>
            <div class="col-sm-4">
                <br>
                <label for="identificador" id="lblIdentificador" class="form">Filtrar Arquivo</label>
                    <select id="identificador" class="form-control">
                    <option value="0">Nenhum</option>
                    <option value="1">CPF</option>
                    <option value="2">SETOR</option>
                    </select>
            </div>
                <div class="col-sm-12 text-right">
                    <br>
                    <span onclick="subirArquivos()" id="btnSubirArqu" class="btn btn-success">Confirmar</span>
                </div>
                </div>
        </form>
        <?php
    }

    public function formNovaPasta()
    {
        $control = new cRelatoriosController();
        $array_permissoes = $control->carregarPermissoes();
        ?>
        <form id="formSalvar" action="<?php echo appConf::caminho ?>cRelatorios/criarPasta">
            <div class="row">
                <div class="col-sm-12">
                    <label>Nome do Diretório</label>
                    <input type="text" name="nome_pasta" class="form-control" placeholder="Nome do Diretório"/>
                </div>
                <br>
                <hr>
                <br>
                <div class="col-sm-12 text-left" >
                    <label>Permissões</label>
                    <hr>
                    <div class="row">
                        <?php

                        foreach ($array_permissoes as $permissoes=>$array){
                            ?>
                            <div class="col-sm-4 text-left">
                            <?php
                             if ($permissoes=='Perfil')
                                 echo "<br>";
                             ?>
                            <label><?php echo $permissoes  ?></label>
                            <select
                             <?php
                                 echo "multiple";
                             ?>
                             name="<?php echo $permissoes ?>[]" class="form-control">
                            <?php
                                foreach ($array as $key=>$value){
                                    ?> <option value="<?php echo $value['ID'] ?>"><?php echo $value['NOME'] ?></option>
                                <?php
                                }
                               ?> </select>
                                                            <?php

                                 echo "<small>*Utilize a tecla Ctrl para selecionar várias opções.</small><br>";
                             ?><?php
                            ?></div>
                            <?php
                        }
                        ?>
                    <div class="col-sm-8 text-left" >
                    <br>
                        <label>Usuário(s)</label>
                        <input type="text" style="cursor: pointer" data-toggle="modal" id="userPermitidos" name="userPermitidos" data-target="#modalUsuarios" readonly  onclick="carregaUsuarios(0)" class="form-control"/>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function listagemArquivos($idPasta)
    {
        //'ID''METADADOS''NOME' 'TIPO''DATA'
        $control = new cRelatoriosController();
        $relatorios = $control->listRelatoriosByPasta($idPasta);
        $subs = $control->listSubPastaByPasta($idPasta);
        $nivel_acesso = $control->getNivelAcessoPasta($idPasta);
        if(is_array($subs)and is_array($relatorios)) {
            $nomePasta = $relatorios[1]['PASTA'];
            $quantidade = (count($relatorios)+ count($subs));
        }else if(is_array($subs)){
            $quantidade =  count($subs);
            $nomePasta = $control->getNomePastaById($idPasta);
        }else if(is_array($relatorios)){
            $nomePasta = $relatorios[1]['PASTA'];
            $quantidade = count($relatorios);
        }else{
            $quantidade=0;
            $nomePasta = $control->getNomePastaById($idPasta);
        }
        ?>
        <div class="col-xs-8 col-sm-9 text-left">
            <form id="frm_docs" style=" padding: 10px;margin-top: 10px">
                <div class="row">
                    <div class="col-sm-12 col-md-8">
                        <div class="input-group">
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-4">
                        <div class="pull-right">

                        <?php
                        if($nivel_acesso>0 && $_SESSION['nivel_admin']==0){?>
                            <button id="btnUpload" onclick="modalUpload()" type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                    data-target="#modalVisualizar">
                                <span class="glyphicon glyphicon-arrow-up"></span> Upload
                            </button>
                            <?php
                            }
                            if ($_SESSION['pasta_atual']>0 && $_SESSION['nivel_admin']==2) { ?>
                            <a class="btn btn-info btn-sm" onclick="voltarHome()">
                            <span class="glyphicon glyphicon-home" ></span>.
                            </a>
                            <?php
                             }
                             if ($_SESSION['nivel_admin']>0) {
                                if ($_SESSION['nivel_admin']==2||($_SESSION['nivel_admin']>1 and $_SESSION['pasta_atual']>0)) { ?>
                            <button type="button" onclick="modalCadastrar()" class="btn btn-info btn-sm" data-toggle="modal"
                                    data-target="#modalCadastrar" id="cadastrar">
                                <span class="glyphicon glyphicon-plus"></span> Nova Pasta
                            </button>
                             <?php }
                             if($_SESSION['pasta_atual']>0){
                                    ?>
                            <button id="btnUpload" onclick="modalUpload()" type="button" class="btn btn-info btn-sm" data-toggle="modal"
                                    data-target="#modalVisualizar">
                                <span class="glyphicon glyphicon-arrow-up"></span> Upload
                            </button>
                            <?php if($quantidade>0){?>
                            <div class="btn-group" role="group">
                                <button type="button" class="btn btn-info btn-sm dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    Ações
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                                    <li><a href="#" id="btnSelecionarTudo" onclick="marcarTrs()">
                                            <small><span class="glyphicon glyphicon-check"></span> Selecionar Tudo
                                            </small>
                                        </a></li>
                                    <li>
                                    <a href="#" id="btnRemoverSelecao"  onclick="desmarcarTrs()">
                                            <small><span class="glyphicon glyphicon-unchecked"></span> Remover
                                                Seleção
                                            </small>
                                        </a>
                                        </li>
                                    <li class="divider"></li>
                                    <li>
                                    <a href="#" style="display:none;" onclick="guardarIdsMover()" >
                                            <small>
                                                <span class="glyphicon glyphicon-transfer"></span>
                                                Mover Selecionados
                                            </small>
                                        </a>
                                    </li>
                                    <a href="#"  data-toggle="modal" id="btnChamaModal" data-target="#modalExcluirSelecionado"></a>
                                    <li><a href="#"  id="btnExcluirSelecao" onclick="excluirSelecao()">
                                            <small><span class="glyphicon glyphicon-trash"></span> Excluir
                                                Selecionados
                                            </small>
                                        </a></li>
                                </ul>
                            </div>
                            <?php }?>
                            <?php }?>
                            <?php }?>
                        </div>
                    </div>
                    <br>
                    <br>
                    <br>
                    <?php
                    if ($idPasta==0) {
                        echo '<img src="/public/img/fundo_relatorio.jpg" class="img-responsive" />';
                        echo "</div></div></form></div>";
                        return;
                    }

                    ?>

                    <div class="col-lg-8">
                        <div>
                            <div style="font-size:18px;">
                                <img width="30" style="padding-right:5px;" src="/public/img/foldermini.png" class="pull-left">
                                <strong id="pastaNome"><?php echo $nomePasta ?></strong>
                                <?php if($_SESSION['nivel_admin']>1){?>
                                <button type="button" onclick="" class="btn btn-default dropdown-toggle"
                                        data-toggle="dropdown" aria-expanded="false">
                                    <span class="caret"></span>
                                </button>
                                <ul class="dropdown-menu dropdown-menu-left" role="menu">
                                    <li><a href="#" data-toggle="modal" data-target="#modalpermissaoPasta" onclick="permissoesPasta('<?php echo $nomePasta?>','<?php echo $idPasta ?>')">
                                            <small><span class="glyphicon glyphicon-user"></span> Permissões
                                            </small>
                                        </a></li>
                                    <li>
                                    <?php if($_SESSION['nivel_admin']>1 ){?>
                                    <a href="#" data-toggle="modal" onclick="renomearPasta('<?php echo $nomePasta?>','<?php echo $idPasta ?>')"  data-target="#modalRenomearPastaSelecionada"  >
                                            <small>
                                                <span class="glyphicon glyphicon-pencil"></span>
                                                Renomear Pasta
                                            </small>
                                        </a>
                                    </li>
                                    <a></a>
                                    <li><a href="#" data-toggle="modal" onclick="excluirPasta(<?php echo $idPasta?>)" data-target="#modalExcluirPastaSelecionada">
                                            <small><span class="glyphicon glyphicon-trash"></span> Excluir Pasta
                                            </small>
                                        </a></li>
                                        <?php }?>
                                </ul>
                                    <?php }?>
                            </div>
                            <div>
                                <small><?php  echo $quantidade ?> item(s).</small>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
                <div class="row" id="msgMover" style="display: none">
                <div class="col-xs-9" >
                <span class="alert-warning form-control"><span id="qtdArquivos"> </span> arquivo(s) na área de transferência</span>
                </div>
                <div class="pull-right text-right col-xs-3">
                <span class="btn btn-success"><span class="glyphicon glyphicon-saved"></span> Colar</span>
                <span class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancelar</span>
                </div>
                </div>
                                     <div class="col-lg-12">
                     <?php
                         if($quantidade==0){
                             ?><br><br><div class=" alert-danger form-control text-center"><span>Pasta Vazia</span></div><?php
                         }
                     ?>
                </div>
                <hr>
                <?php
                if(is_array($subs)||is_array($relatorios)) {
                    ?>
                <table class="table table-condensed table-responsive">
                    <thead>
                    <tr class="active">
                        <th width="600">Nome</th>
                        <th width="120" class="text-center">Data</th>
                        <th width="120" class="text-center">Tipo</th>
                        <th width="120" class="text-center">Ação</th>
                    </tr>
                    </thead>
                    </table>
                   <div style="height: 400px;overflow: auto">
                    <table class="table table-condensed table-responsive">
                    <tbody>
                    <?php
                        if(is_array($subs)){
                            $this->subPastaTabela($subs);
                        }
                        if(is_array($relatorios)){
                            $this->arquivosTabela($relatorios);
                        }
                    ?>
                    </tbody>
                </table>
                </div>
                <?php } ?>
            </form>
        </div>
        <?php
    }

    public function carregarListaDiretório(){
        $control = new cRelatoriosController();
        $pastas = $control->listPastas(0);
        ?>
        <!--ul style="text-decoration: none; padding-inline-start: 8px">
        <li     class="pasta"
                style="font-size: 13px;
                padding-inline-start: 8px"
                onclick="capturaIdPasta(this)"
                id="pasta_doc">
        <img width="17" src="/public/img/foldermini.png"/>
        <b>Meus Documentos</b>
        </li>
        </ul-->
        <?php
        if (is_array($pastas)){
            foreach ($pastas as $pasta) {?>
                <ul style="text-decoration: none; padding-inline-start: 8px">
                <li class="pasta" style="font-size: 13px;
                padding-inline-start: 8px"  onclick="capturaIdPasta(this)"
                id="<?php echo "pasta_".$pasta['ID'] ?>">
                    <img width="17" src="/public/img/foldermini.png"/>
                    <b><?php echo $pasta['NOME'] ?></b>
                </li>
                <?php
                $this->subPasta($pasta['ID']);
                echo "</ul>";
            }
        }
        echo '<input type="hidden" name="txtIdPasta" id="txtIdPasta" value="0">';
    }

    public function formExcluirArquivos(){
        ?>
        <form>
            <div class="row text-left ">
                <div class="col-sm-12 text-center " >
                    <div class="form-group form-group-sm" >
                        <h3>Deseja excluir todos os arquivos selecionados?</h3>
                    </div>
                </div>
                <div class="col-sm-12 text-right " >
                    <div class="form-group form-group-sm">
                        <button onclick="confirmaExclusao()" id="confirmaExcluir" class="btn btn-danger">Excluir</button>
                        <button data-dismiss="modal" class="btn btn-default">Cancelar</button>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function usuariosForm(){
        ?>
        <form id="">
            <div class="row text-left ">
                <div class="col-sm-8 text-left">
                    <div class="form-group">
                        <label for="nomeUsuario"> Usuário(s)</label >
                        <sub for="nomeUsuario"> PESQUISAR POR: NOME - E-MAIL - SETOR - MATRICULA - LOGIN</sub >
                        <input type="text" class="form-control" onkeydown="buscaNomes()" id="nomeUsuario" ></input>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group">
                        <label for="add_user" style="color:#FFFFFF"> df</label>
                        <span class="btn btn-info form-control" onclick="adicionaIdLista()" id="add_user">
                            <span class="glyphicon glyphicon-plus"></span>
                            Adicionar
                        </span>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group">
                        <label for="add_user" style="color:#FFFFFF"> df</label>
                        <span class="btn btn-default form-control" data-dismiss="modal"  id="add_user">
                            <span class="glyphicon glyphicon-share" ></span>
                            Voltar
                        </span>
                    </div>
                </div>
                 <div class="col-sm-12 text-left">
                    <select multiple="7" class="form-control" name="listaUserSelect" id="listaUserSelect" style="height: 115px">

                    </select>
                    <br>
                 </div>
                 <div class="col-sm-12 text-left" style="max-height: 300px; overflow: auto">
                 <table id="userAdicionados" class="table table-responsive table-bordered table-condensed">
                     <thead>
                         <tr class="active">
                             <th width="60px">ID</th>
                             <th width="100px">Login</th>
                             <th>Nome</th>
                             <th width="20px">#</th>
                         </tr>
                     </thead>
                     <tbody>
                     </tbody>
                 </table>
                 </div>
            </div>
        </form>
        <?php
    }

    public function usuariosFormEditar(){
        ?>
        <form id="">
            <div class="row text-left ">
                <div class="col-sm-8 text-left">
                    <div class="form-group">
                        <label for="nomeUsuario"> Usuário(s)</label >
                        <sub for="nomeUsuario"> PESQUISAR POR: NOME - E-MAIL - SETOR - MATRICULA - LOGIN</sub >
                        <input type="text" class="form-control" onkeydown="buscaNomesEditar()" id="nomeUsuarioEditar" ></input>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group">
                        <label for="add_user" style="color:#FFFFFF"> df</label>
                        <span class="btn btn-info form-control" onclick="adicionaIdListaEditar()" id="add_user">
                            <span class="glyphicon glyphicon-plus"></span>
                            Adicionar
                        </span>
                    </div>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group">
                        <label for="add_user" style="color:#FFFFFF"> df</label>
                        <span class="btn btn-default form-control" data-dismiss="modal"  id="add_user">
                            <span class="glyphicon glyphicon-share" ></span>
                            Voltar
                        </span>
                    </div>
                </div>
                 <div class="col-sm-12 text-left">
                    <select multiple="7" class="form-control" name="listaUserSelectEditar" id="listaUserSelectEditar" style="height: 115px">

                    </select>
                    <br>
                 </div>
                 <div class="col-sm-12 text-left" style="max-height: 300px; overflow: auto">
                 <table id="userAdicionadosEditar" class="table table-responsive table-bordered table-condensed">
                     <thead>
                         <tr class="active">
                             <th width="60px">ID</th>
                             <th width="100px">Login</th>
                             <th>Nome</th>
                             <th width="20px">#</th>
                         </tr>
                     </thead>
                     <tbody>
                     </tbody>
                 </table>
                 </div>
            </div>
        </form>
        <?php
    }

    public function subPasta($pasta){
        $control = new cRelatoriosController();
        if ($pasta != null)
            $sub_pastas = $control->listSubPastaByPasta($pasta);
        if (is_array($sub_pastas)) {
            ?>
            <ul style="text-decoration: none;padding-inline-start: 8px"><?php
                foreach ($sub_pastas as $sub_pasta) {
                    ?>
                    <li style="display:none; font-size: 13px;padding-inline-start: 8px" onclick="capturaIdPasta(this)" class="pasta super_pasta_<?php echo $pasta?>" id="<?php echo "pasta_".$sub_pasta['ID'] ?>">
                        <img width="17" src="/public/img/foldermini.png"/>
                        <b><?php echo $sub_pasta['NOME']; ?></b>
                    </li>
                    <?php
                    $this->subPasta($sub_pasta['ID']);
                }
                ?>
            </ul>
            <?php
        }
    }

    public function arquivosTabela($relatorios){
        $control = new cRelatoriosController();
        foreach ($relatorios as $relatorio) {
            ?>
            <tr class="tr-linha" id="<?php echo "tr" . $relatorio['ID']; ?>">
                                    <td width="650"
                                    <?php if($_SESSION['nivel_admin']>0){?>
                                    onclick="marcarLinha(this)"
                                    <?php }?>
                                    style="cursor: pointer" class="td-linha"
                                        id="<?php echo "td" . $relatorio['ID']; ?>">
                                        <img width="18" src="/public/img/ext/<?php echo $control->validaTipo($relatorio['TIPO']); ?>.png"/> <?php echo $relatorio['NOME']; ?>
                                    </td>
                                    <td width="120"
                                    <?php if($_SESSION['nivel_admin']>0){?>
                                    onclick="marcarLinha(this)"
                                    <?php }?>
                                    style="cursor: pointer" class=" td-linha text-center"
                                        id="<?php echo "td" . $relatorio['ID']; ?>">
                                        <?php echo substr(appFunction::formatarData($relatorio['DATA']), 0, 10); ?>
                                    </td>
                                    <td width="120"
                                    <?php if($_SESSION['nivel_admin']>0){?>
                                    onclick="marcarLinha(this)"
                                    <?php }?>
                                    style="cursor: pointer" class="text-center td-linha"
                                        id="<?php echo "td" . $relatorio['ID']; ?>">
                                        <?php echo $control->validaTipo($relatorio['TIPO']); ?>
                                    </td>
                                    <td class="text-right" width="120">
                                    <?php if ($_SESSION['nivel_admin']>0) { ?>
                                    <a class="btn btn-primary btn-xs" data-toggle="modal"
                                    onclick="renomearArqu('<?php echo $relatorio['NOME']?>','<?php echo $relatorio['ID']; ?>')" data-target="#modalRenomearArquSelecionada">
                                    <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                                    </a>&nbsp;
                                    <?php } ?>
                                    <label  onclick="baixarArquivo(<?php echo $relatorio['ID']; ?>)" class="btn btn-warning btn-xs">
                                    <span class="glyphicon glyphicon-download-alt"></span>&nbsp;
                                    </label>
                                    <a id="arquivo_baixar<?php echo $relatorio['ID']; ?>" download="<?php echo $relatorio['NOME'].'.'.$relatorio['TIPO']?>" href="<?php echo appConf::caminho.'public/relatorios/'.$relatorio['METADADOS'].'.'.$relatorio['TIPO']?>"></a>

                                    </td>
                                </tr><?php
        }
    }

    public function formRenomearPasta($nome,$id){?>
        <form>
            <div class="row text-left ">
                <div class="col-sm-12 text-center " >
                    <div class="form-group form-group-sm" >
                        <input type="text " class="form-control" value="<?php echo $nome ?>" id="txtEditarNome"/>
                    </div>
                </div>
                <div class="col-sm-12 text-right " >
                    <div class="form-group form-group-sm">
                        <a onclick="confirmaEdicao(<?php echo $id ?>)" id="confirmarEditar" class="btn btn-success">Confirmar</a>
                        <button data-dismiss="modal" class="btn btn-default">Cancelar</button>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formRenomearArqu($nome,$id){
        ?>
        <form>
            <div class="row text-left ">
                <div class="col-sm-12 text-center " >
                    <div class="form-group form-group-sm" >
                        <input type="text " class="form-control" value="<?php echo $nome ?>" id="txtEditarNomeArqu"/>
                    </div>
                </div>
                <div class="col-sm-12 text-right " >
                    <div class="form-group form-group-sm">
                        <a onclick="confirmaEdicaoArqu(<?php echo $id ?>)" id="confirmarEditar" class="btn btn-success">Confirmar</a>
                        <button data-dismiss="modal" class="btn btn-default">Cancelar</button>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formPermissaoPasta($nome,$id){
        $control = new cRelatoriosController();
        $array_permissoes = $control->carregarPermissoes();
        $control->listarPermissoesById($id);
        $array_user = $_POST['usuarios'];
        $array_perfil = $_POST['perfis'];
        $array_empresa = $_POST['empresas'];
        $array_linha = $_POST['linhas'];
        $array_unidade = $_POST['unidades'];
        ?>
        <form id="formSalvar" method="post" action="<?php echo appConf::caminho ?>cRelatorios/atualizarPermissaoPasta">
            <div class="row">
                <div class="col-sm-12">
                    <label>Nome do Diretório</label>
                    <input type="text" readonly name="nome_pasta" class="form-control" value="<?php echo $nome ?>" placeholder="Nome do Diretório"/>
                    <input type="hidden"  name="id"  value="<?php echo $id ?>" />
                </div>
                <br>
                <hr>
                <br>
                <div class="col-sm-12 text-left" >
                    <label>Permissões</label>
                    <hr>
                    <div class="row">
                        <?php
                        foreach ($array_permissoes as $permissoes=>$array){
                            ?>
                            <div class="col-sm-4 text-left">
                            <?php
                             if ($permissoes=='Perfil')
                                 echo "<br>";
                             ?>
                            <label><?php echo $permissoes  ?></label>
                            <select style="height: 240pt"
                             <?php
                             echo "multiple";
                             ?>
                             name="<?php echo $permissoes ?>[]" class="form-control">
                            <?php
                                foreach ($array as $key=>$value){
                                    ?> <option value="<?php echo $value['ID'] ?>"
                                    <?php
                                    switch ($permissoes){
                                        case 'Perfil':
                                            if (in_Array($value['ID'],$array_perfil)){
                                            echo 'Selected';
                                            }
                                            break;
                                        case 'Empresa':
                                            if (in_Array($value['ID'],$array_empresa)){
                                            echo 'Selected';
                                            }
                                            break;
                                        case 'Linha':
                                            if (in_Array($value['ID'],$array_linha)){
                                            echo 'Selected';
                                            }
                                            break;
                                        case 'Unidade':
                                            if (in_Array($value['ID'],$array_unidade)){
                                            echo 'Selected';
                                            }
                                            break;
                                    }
                                    ?>
                                    ><?php echo $value['NOME'] ?></option>
                                <?php
                                }
                               ?> </select>
                                                            <?php

                                 echo "<small>*Utilize a tecla Ctrl para selecionar várias opções.</small><br>";
                             ?><?php
                            ?></div>
                            <?php
                        }
                        ?>
                    <div class="col-sm-8 text-left" >
                    <br>
                        <label>Usuário(s)</label>
                        <?php
                        $users = '';
                        $i= 0;
                        foreach ($array_user as $id_user){
                            if($i!=0)
                                $users .='-';
                             $users .= $id_user;
                             $i++;
                        }
                        ?>
                        <input value="<?php echo $users ?>" type="text" style="cursor: pointer" data-toggle="modal"
                        id="userPermitidos" name="userPermitidos" data-target="#modalUsuariosEditar"
                        readonly value="" onclick="carregaUsuariosEditar()" class="form-control"/>
                    </div>
                </div>
                <div class="col-sm-12 text-right" >
                <button class="btn btn-success">Atualizar</button>
                <button data-dismiss="modal" class="btn btn-danger">Cancelar</button>
                </div>
            </div>
        </form>
        <?php
    }

    public function subPastaTabela($sub){
        if(is_array($sub)){
            foreach ($sub as $sub_pastas){
            ?>
            <tr class="tr-pt-linha" >
                <td width="650" onclick="capturaIdPasta(this)" id="<?php echo "tabelaPasta_".$sub_pastas['ID']; ?>" style="cursor: pointer" class="td-pt-linha" id="<?php echo "td-pt" . $sub_pastas['ID']; ?>">
                    <img width="18" src="/public/img/foldermini.png"/> <?php echo $sub_pastas['NOME']; ?>
                </td>
                <td width="120" onclick="capturaIdPasta(this)" id="<?php echo "tabelaPasta_".$sub_pastas['ID']; ?>" style="cursor: pointer" class=" td-pt-linha text-center" id="<?php echo "td-pt" . $sub_pastas['ID']; ?>">
                    10/10/2018
                </td>
                <td width="120" onclick="capturaIdPasta(this)" id="<?php echo "tabelaPasta_".$sub_pastas['ID']; ?>" style="cursor: pointer" class="text-center td-pt-linha" id="<?php echo "td-pt" . $sub_pastas['ID']; ?>">
                    Diretório
                </td>
                <td width="120" class="text-right">
                <?php if ($_SESSION['nivel_admin']>1) { ?>
                <a class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirPastaSelecionada" onclick="excluirPasta(<?php echo $sub_pastas['ID']?>)">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;
                </a>&nbsp;
                <a class="btn btn-primary btn-xs" href="#" data-toggle="modal" onclick="renomearPasta('<?php echo $sub_pastas['NOME']?>','<?php echo $sub_pastas['ID']; ?>')" data-target="#modalRenomearPastaSelecionada">
                <span class="glyphicon glyphicon-pencil"></span>&nbsp;
                </a>&nbsp;
                <a class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalpermissaoPasta" onclick="permissoesPasta('<?php echo $sub_pastas['NOME']?>','<?php echo $sub_pastas['ID']; ?>')">
                <span class="glyphicon glyphicon-user"></span>&nbsp;
                </a>
                 <?php } ?>
                </td>
             </tr>
            <?php
            }
        }
    }
}