<?php

class formsSenhaAdmin
{

    public function formEditar($objeto)
    {
        ?>
        <form id="formEditar" action="<?php print appConf::caminho ?>SenhaAdmin/controlSwitch/atualizar">
            <div class="row">
                <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="senha">SENHA</label>
                        <input type="text" class="form-control " value="<?php echo $objeto->getSenha() ?>" name="senha"
                               id="senha">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkUsuario">FKUSUARIO</label>
                        <input type="text" class="form-control " value="<?php echo $objeto->getFkUsuario() ?>"
                               name="fkusuario" id="fkUsuario">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataExpirar">DATAEXPIRAR</label>
                        <input type="text" class="form-control " value="<?php echo $objeto->getDataExpirar() ?>"
                               name="dataexpirar" id="dataExpirar">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">DATAIN</label>
                        <input type="text" class="form-control " value="<?php echo $objeto->getDataIn() ?>"
                               name="datain" id="dataIn">
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formVisualizar($objeto)
    {
        ?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="senha">SENHA</label>
                    <input type="text" disabled class="form-control " readonly value="<?php echo $objeto->getSenha() ?>"
                           name="senha" id="senha">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fkUsuario">FKUSUARIO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getFkUsuario() ?>" name="fkusuario" id="fkUsuario">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataExpirar">DATAEXPIRAR</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getDataExpirar() ?>" name="dataexpirar" id="dataExpirar">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DATAIN</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getDataIn() ?>" name="datain" id="dataIn">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar()
    {
        ?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>SenhaAdmin/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="senha">SENHA</label>
                        <input required type="text" class="form-control " name="senha" id="senha">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkUsuario">FKUSUARIO</label>
                        <input required type="text" class="form-control " name="fkusuario" id="fkUsuario">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataExpirar">DATAEXPIRAR</label>
                        <input required type="text" class="form-control " name="dataexpirar" id="dataExpirar">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">DATAIN</label>
                        <input required type="text" class="form-control " name="datain" id="dataIn">
                    </div>
                </div>
            </div>
        </form><?php
    }
}
