<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 



<title>Portal EMS Prescrição</title>

<!--Plugin javascript do jquery e bootstrap-->
    <script src="<?php echo appConf::caminho ?>plugins/jquery/jquery-1.12.1.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/highcharts.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/highcharts-more.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/data.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/drilldown.js"></script>
<script src="<?php echo appConf::caminho ?>public/js/index.js"></script>


<link rel="shortcut icon" href="<?php echo appConf::caminho ?>public/img/liferay.ico" type="image/x-icon" />


<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css" />
</head>

    <body>
        
        
        <div class="container">
            <h3 class="well">Fotos Convenção
            <a class="pull-right btn btn-success btn-sm" href="<?php echo appConf::caminho ?>fotoUpload/exportar">Exportar Lista Excel</a>
            </h3>
        
        
        <?php print $listaFotos ?>
        </div>
    </body>
</html>