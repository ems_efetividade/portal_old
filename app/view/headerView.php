<?php
require_once('lib/appConf.php');
require_once('vAjuda.php');
require_once('vMenuFerramentas.php');
require_once('lib/appFunction.php');
require_once('recalBrasartView.php');
require_once('app/model/loginModel.php');
require_once('app/model/ferramentaModel.php');
require_once('sistemas/AceiteDeDocumentos/app/model/mDocumento.php');
require_once('/../model/mControleHeader.php');
require_once('/../controller/cAssinaturasDocumentosController.php');
require_once('/../controller/cTermoAnvisaController.php');
loginModel::validarSessao();
$header = new mControleHeader();
$cDocumentos = new cAssinaturasDocumentosController();
$documentos = $cDocumentos->listByLogin();
$viewAjuda = new vAjuda();
$ferramentas = new vMenuFerramentas();
$aceite = new mDocumento();
if (count($aceite->listarDocumentosAssinar(appFunction::dadoSessao('id_colaborador'))) > 0) {
    header('Location: ' . AppConf::caminho . 'sistemas/AceiteDeDocumentos/');
}
if (appFunction::verificarTermo(appFunction::dadoSessao('id_colaborador')) > 0) {
    header('Location: ' . AppConf::caminho . 'sistemas/TermoCompensacaoEventos/home/mostrarTermo/');
}
$url_sistema = $_SESSION['sistema_atual'];
$_POST['super'] = '';
$permitidos = $header->listPermissoesEmpresas();
if ($url_sistema == null) {
    $url_sistema = 'home';
}
if ($url_sistema[0] != 'ferramenta') {
    $_POST['url'] = $url_sistema[0];
    $sistema = $header->listObj();
    if (is_object($sistema[0])) {
        $id_sistema = $sistema[0]->getId();
        if ($id_sistema > 0) {
            $_SESSION['sistema_atual'] = $id_sistema;
        }
    }
    $_POST['url'] = '';
} else {
    $_POST['fkferramenta'] = $url_sistema[2];
    $sistema = $header->listObj();
    if (is_object($sistema[0])) {
        $id_sistema = $sistema[0]->getId();
        if ($id_sistema > 0) {
            $_SESSION['sistema_atual'] = $id_sistema;
        }
    }
    $_POST['fkferramenta'] = '';
    if (!in_array($id_sistema, $permitidos)) {
        if ($_SESSION['nivel_admin'] < 2) {
            require_once('/bloqueadoView.php');
            new bloqueadoView();
        }
    }

}
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <head>
         <link href="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.js"></script>
        <script type="text/javascript" src="/../../public/js/Notiflix/AIO/notiflix-aio-1.4.0.min.js"></script>
        <link href="<?php echo appConf::caminho ?>public/css/w3.css" rel="stylesheet" type="text/css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <meta name="viewport" content="width=device">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE"/>
        <title>Portal Efetividade</title>
        <script src="<?php echo appConf::caminho ?>plugins/jquery/jquery-1.12.1.js"></script>
        <script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.js"></script>
        <script src="<?php echo appConf::caminho ?>plugins/highchart/js/highcharts.js"></script>
        <script src="<?php echo appConf::caminho ?>plugins/highchart/js/highcharts-more.js"></script>
        <script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/data.js"></script>
        <script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/drilldown.js"></script>
        <script src="<?php echo appConf::caminho ?>public/js/index.js"></script>
        <!--Google Analitcs-->
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
            ga('create', 'UA-93236726-1', 'auto');
            ga('send', 'pageview');
        </script>
        <link rel="shortcut icon" href="https://www.gruponc.net.br/favicon.ico" type="image/x-icon"/>
        <!--Folhas de estilo do bootstrap, das fontes e do index-->
        <link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet"
              type="text/css"/>
        <link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet"
              type="text/css"/>

        <link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo appConf::caminho ?>public/css/ems_index.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo appConf::caminho ?>public/css/<?php echo $_SESSION['sigla'] ?>_index.css"
              rel="stylesheet" type="text/css"/>
    </head>
        <style>
        div {
            font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
        }
    </style>

    <script>


    </script>
    <div class="modal fade" style="z-index:100000;" id="modalMsgBox" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content ">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span>
                        Aviso
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-3">
                            <p class="text-center text-danger" style="font-size:500%;"><span
                                        class="glyphicon glyphicon-remove"></span>
                            <p>
                        </div>
                        <div class="col-lg-9">
                            <h5><p class="text-center" id="msgboxTexto"></p></h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button"
                            class="btn btn-default btn-md"
                            data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" style="z-index:100000;" id="modalAguarde" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content ">
                <div class="modal-body">
                    <p class="text-center"><strong>Processando...</strong></p>
                    <h5 class="text-center">
                        <small>(Esta operação pode demorar alguns minutos.)</small>
                    </h5>
                    <div class="progress">
                        <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar"
                             aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                            <span class="sr-only"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function (e) {
            $('#modalContato').on('show.bs.modal', function (event) {
                $.ajax({
                    type: "POST",
                    url: '<?php echo appConf::caminho ?>contato/formulario/',
                    success: function (retorno) {
                        $('#modalContato .modal-body').html(retorno);
                    },
                    beforeSend: function (a) {
                        //$('#cmbArea').html('<option>Carregando...</option>');
                    }
                });
            });
        });
    </script>
    <div class="modal fade" style="z-index:100000;" id="modalContato" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content ">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-envelope"></span> Fale
                        Conosco</h5>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-md" id="btnEnviarContato">
                        <span class="glyphicon glyphicon-off"></span> Enviar Email
                    </button>
                    <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header btn-info">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span>
                    </button>
                    <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Sair do
                        sistema
                    </h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 text-center text-danger">
                            <img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/question.png"/>
                        </div>
                        <div class="col-lg-8" style="height:128px;">
                            <h4 class="text-center">Deseja sair do sistema?</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form action="<?php echo appConf::caminho ?>login/logout" method="POST">
                        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Fechar
                        </button>

                        <button type="submit" class="btn btn-info btn-md">
                            <span class="glyphicon glyphicon-off"></span> Sair do sistema
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
<?php
$_POST['super'] = 3;
$_POST['ativo'] = 1;
$menus = $header->listObj();
?>
    <div class="w3-bar w3-personalizado w3-top ">
        <div id="btnAbrir" onclick="w3_open()"
             class="w3-dropdown-click  w3-personalizado-bt text-center
        <?php
             if (count($permitidos) == '0') {
                 echo "hidden-xs ";
             }
             if (!is_array($menus)) {
                 echo "hidden-sm hidden-md hidden-lg";
             }
             ?>"
             style="font-size: 25px!important; padding-top: 8px;">
            <span style="font-size: 23px!important;">&#9776;</span>
        </div>
        <div id="btnFechar" onclick="w3_close()"
             class="w3-dropdown-click  w3-personalizado-bt text-center "
             style="font-size: 21px; padding-top: 13px; display: none;">
            <span class="glyphicon glyphicon-remove"></span>
        </div>
        <a data-toggle='modal' data-target='#trocarEmpresa' class="w3-bar-item w3-button w3-logo "
           style="padding: px!important;padding-left: 16px!important;
        margin-right: 25px!important;">
            <img height="27" style="margin-top: 4px" src="/../../public/img/<?php echo $_SESSION['sigla'] ?>.png"/>
            <span id="nomeEmpresa" class="hidden-xs"
                  style="vertical-align: bottom; font-size:12px;position:relative;top:-4px;left:10px;font-weight:bold"><?php echo appFunction::dadoSessao('unidade_neg') ?></span>
        </a>
        <a href="#" class="w3-bar-item w3-button  w3-personalizado-bt w3-right" onclick="openRightMenu()">
            <div class="circleImgW3 ">
                <img src="<?php echo appFunction::fotoColaborador(appFunction::dadoSessao('id_colaborador')) ?>"/>
            </div>
        </a>
        <div class="w3-right">
            <?php
            $_POST['super'] = 0;
            $_POST['ativo'] = 1;
            $menus = $header->listObj();
            if (is_array($permitidos)) {
                foreach ($menus as $menu) {
                    $_POST['super'] = $menu->getId();
                    if (!in_array($menu->getId(), $permitidos)) {
                        continue;
                    }
                    $sub = $header->listObj();
                    if (!is_array($sub)) {
                        ?>
                        <a href="<?php echo appConf::caminho . $menu->getUrl() ?>"
                           title="<?php echo $menu->getCampo(); ?>"
                           class="w3-bar-item w3-button w3-personalizado-bt hidden-xs"
                           style="font-size: 13px!important;">
                <span style="margin: 10px!important;"
                      class="glyphicon glyphicon-<?php echo $menu->getIcone() ?>">
                </span>
                        </a>
                        <?php
                    }
                }
            }
            ?>
            <!--            <a href="#" data-toggle="modal" data-target="#modalLogout"-->
            <!--               title="Notificações"-->
            <!--               class="w3-bar-item w3-button w3-personalizado-bt hidden-xs"-->
            <!--               style="font-size: 13px!important;">-->
            <!--                <span id="notification" style="margin: 10px!important;"-->
            <!--                      class="glyphicon glyphicon-bell">-->
            <!--                </span>-->
            <!--            </a>-->
            <a href="#" data-toggle="modal" data-target="#modalLogout"
               title="Sair do Sistema"
               class="w3-bar-item w3-button w3-personalizado-bt hidden-xs"
               style="font-size: 13px!important;">
                <span style="margin: 10px!important;"
                      class="glyphicon glyphicon-off">
                </span>
            </a>
        </div>
    </div>
    <div class="w3-sidebar w3-bar-block w3-card w3-animate-left " style="display:none; font-size: 13px; "
         id="mySidebar">
        <br>
        <?php
        $inseridos = $ferramentas->montaModal();
        if ($inseridos == 0) {
            ?>
            <?php
        }
        ?>
    </div>
    <div class="w3-sidebar w3-bar-block w3-card w3-animate-right"
         style="display:none;
         right:0;
         font-size: 13px;
         overflow: hidden;
         margin-bottom: 0px!important;
         padding-bottom: 0px!important;"
         id="rightMenu">
        <br>
        <?php
        $ferramentas->menuUsuario();
        ?>
    </div>
<?php
if (true) {
    ?>
    <div id="botaoAjudaPadrao"></div>
    <script>
      //botaoAjuda();
    </script>
    <?php
}
?>
    <div class="modal fade" id="modalAjuda" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <?php
                   $viewAjuda->viewAjudaFerramentas();
                    ?>
                </div>
            </div>
        </div>
    </div>
<?php
//$cDocumentos->imprimirTermo();
$termoBrasart = new recalBrasartView($_SESSION['id_colaborador']);
if (count($documentos)>0) {
    foreach ($documentos as $documento) {
        $idDocumento = $documento['FK_ASSINATURAS_DOCUMENTOS'];
        if (!$cDocumentos->verificaAssinatura($idDocumento)) {
            $cDocumentos->getCaptcha();
            $_POST['id'] = $idDocumento;
            $arquivo = $cDocumentos->loadObj();
            ?>
            <div class="row">
                <div class="col-sm-12" style="margin: 20px;">
                    <br>
                    <legend><strong>Documento Pendente de Assinatura</strong></legend>
                    <h4><?php echo $arquivo->getNome() ?></h4>
                    <p style="margin-right: 25px" class="alert alert-warning text-center">
                        <strong>Atenção:</strong>Leia atentamente todo o documento abaixo e dê a sua resposta.
                    </p>
                </div>
                <div class="col-sm-11" style="margin: 40px;">
                    <?php echo $arquivo->getArquivo() ?>
            </div>
            <div class="col-sm-12" style="padding: 20px">
                <div>
                    <br>
                    <form method="post">
                        <input type="hidden" id="id_doc" value="<?php echo $arquivo->getId() ?>">
                        <br><br>
                        <img width="465"
                             src="<?php echo appFunction::caminho . 'public/img/' . $_SESSION['id_colaborador'] ?>captcha.png">
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Digite o código da imagem(O sistema diferencia maiúsculas de minúsculas)</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="captchaText" class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <button onclick="location.reload()" class="btn btn-info">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                    &nbsp;<strong>Gerar Outro Código</strong>
                                </button>
                            </div>
                        </div>
                        <br>
                        <a href="" style="display: none" id="arquivo" download>Download Text</a>
                        <div id="botoes">
                            <span id="aceitar" disabled="false" type="button"
                                  class="btn btn-lg btn-block btn-success btn-responder"><span
                                        class="glyphicon glyphicon-thumbs-up"></span> Li e ESTOU de acordo!
                                </span>
                            <span id="negar" disabled="false" type="button"
                                  class="btn btn-block btn-lg btn-danger btn-responder"
                                  daata-target="#modalRespostaa"><span
                                        class="glyphicon glyphicon-thumbs-down"></span>
                                    Li e NÃO estou de acordo!
                                </span>
                        </div>
                        <script>
                            var leuTudo = true;
                            $('#botoes').hover(function () {
                            $('#negar').attr("disabled", false);
                            $('#aceitar').attr("disabled", false);
                                if (!leuTudo)
                                    msgBox('Antes de aceitar os termos, você deve visualizar todo o documento');
                            });
                            $('#aceitar').click(function () {
                                if (!leuTudo) {
                                    msgBox('Você deve ler todo o arquivo antes de aceitar');
                                    return;
                                }
                                captcha = document.getElementById('captchaText').value
                                documento = document.getElementById('id_doc').value
                                if (captcha.length == 0) {
                                    msgBox('Informe o código da Imagem');
                                    return;
                                }
                                if (confirm('Você clicou em aceitar os termos do documento. \nDeseja Confirmar sua Resposta?'))
                                    retorno = requisicaoAjax('<?php echo appFunction::caminho ?>cAssinaturasDocumentos/validarResposta/' + captcha + '/1/' + documento);
                                if (retorno == "        Código inválido") {
                                    retorno = retorno.trim();
                                    alert(retorno);
                                    return;
                                }
                                retorno = retorno.trim();
                                document.getElementById('arquivo').href = 'https://' + window.location.host + '/public/politicas/assinadas/' + retorno;
                                document.getElementById("arquivo").click();
                                location.reload();
                            });
                            $('#negar').click(function () {
                                if (!leuTudo) {
                                    msgBox('Você deve ler todo o arquivo antes de aceitar');
                                    return;
                                }
                                captcha = document.getElementById('captchaText').value
                                documento = document.getElementById('id_doc').value
                                if (captcha.length == 0) {
                                    msgBox('Informe o código da Imagem');
                                    return;
                                }
                                if (confirm('Você clicou em rejeitar os termos do documento. \nDeseja Confirmar sua Resposta?'))
                                    retorno = requisicaoAjax('<?php echo appFunction::caminho ?>cAssinaturasDocumentos/validarResposta/' + captcha + '/2/' + documento);
                                if (retorno == "        Código inválido") {
                                    retorno = retorno.trim();
                                    alert(retorno);
                                    return;
                                }
                                retorno = retorno.trim();
                                document.getElementById('arquivo').href = 'https://' + window.location.host + '/public/politicas/assinadas/' + retorno;
                                document.getElementById("arquivo").click();
                                location.reload();
                            })
                        </script>
                    </form>
                </div>
            </div>

            <?php
            exit;
        }
    }
}

$cTermo = new cTermoAnvisaController();
$documentos = $cTermo->carregaTermo($_SESSION['id_colaborador']);
if (count($documentos)>0) {
    foreach ($documentos as $documento) {
            $cDocumentos->getCaptcha();
            ?>
            <div class="row">
                <div class="col-sm-12" style="margin: 20px;">
                    <br>
                    <legend><strong>Documento Pendente de Assinatura</strong></legend>
                    <h4>RELATÓRIO DE MONITORAÇÃO DAS DISTRIBUIDORAS PARA OS DETENTORES DE REGISTRO</h4>
                    <p style="margin-right: 25px" class="alert alert-warning text-center">
                        <strong>Atenção: </strong> Para Garantir o envio do formulário, verifique os dados, informe o código da imagem abaixo do documento e clique em <strong>'Dados confirmados - Assinar documento'</strong>.
                    </p>
                </div>
                <div class="col-sm-11" style="margin: 40px;">
                    <?php echo $documento['TERMO'] ?>
            </div>
            <div class="col-sm-12" style="padding: 20px">
                <div>
                    <br>
                    <form method="post">
                        <input type="hidden" id="id_doc" value="<?php echo $documento['ID'] ?>">
                        <br><br>
                        <img width="465"
                             src="<?php echo appFunction::caminho . 'public/img/' . $_SESSION['id_colaborador'] ?>captcha.png">
                        <br>
                        <br>
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Digite o código da imagem(O sistema diferencia maiúsculas de minúsculas)</label>
                            </div>
                            <div class="col-sm-6">
                                <input type="text" id="captchaText" class="form-control">
                            </div>
                            <div class="col-sm-6">
                                <button onclick="location.reload()" class="btn btn-info">
                                    <span class="glyphicon glyphicon-refresh"></span>
                                    &nbsp;<strong>Gerar Outro Código</strong>
                                </button>
                            </div>
                        </div>
                        <br>
                        <a href="" style="display: none" id="arquivo" download target="_blank">Download Text</a>
                        <div id="botoes">
                            <span id="aceitar" disabled="false" type="button"
                                  class="btn btn-lg btn-block btn-success btn-responder"><span
                                        class="glyphicon glyphicon-thumbs-up"></span> Dados confirmados - Assinar documento
                                </span>
                            <span id="negar" disabled="false" type="button"
                                  class="btn btn-block btn-lg btn-danger btn-responder"
                                  daata-target="#modalRespostaa"><span
                                        class="glyphicon glyphicon-thumbs-down"></span>
                                   Voltar e corrigir dados
                                </span>
                        </div>
                        <script>
                        Notiflix.Report.Init({});
                        Notiflix.Loading.Init({});
                        Notiflix.Confirm.Init({});
                        Notiflix.Notify.Init({backOverlay: true, distance: "5%", timeout: 2000});
                            var leuTudo = true;
                            $('#botoes').hover(function () {
                            $('#negar').attr("disabled", false);
                            $('#aceitar').attr("disabled", false);
                                if (!leuTudo)
                                    msgBox('Antes de aceitar os termos, você deve visualizar todo o documento');
                            });
                            $('#aceitar').click(function () {
                                if (!leuTudo) {
                                    msgBox('Você deve ler todo o arquivo antes de aceitar');
                                    return;
                                }
                                captcha = document.getElementById('captchaText').value
                                documento = document.getElementById('id_doc').value
                                if (captcha.length == 0) {
                                    Notiflix.Report.Warning('Atenção!', 'Informe o código da Imagem', 'Fechar');
                                    return;
                                }
                                Notiflix.Confirm.Show( 'Atenção!', 'Deseja confirmar os dados e assinar o documento?', 'Sim', 'Não',
                                function(){
                                    retorno = requisicaoAjax('https://' + window.location.host + '/cAssinaturasDocumentos/validarRespostaAmostras/' + captcha + '/1/' + documento);
                                    if (retorno == "        Código inválido") {
                                        retorno = retorno.trim();
                                        Notiflix.Report.Warning('Atenção!', retorno, 'Fechar');
                                        return;
                                    }
                                    retorno = retorno.trim();
                                    document.getElementById('arquivo').href = 'https://' + window.location.host + '/public/politicas/assinadas/' + retorno;
                                    document.getElementById("arquivo").click();
                                    setTimeout(function(){
                                        setTimeout(function(){
                                            Notiflix.Loading.Dots('Aguarde...');
                                            setTimeout(function(){
                                                location.reload();
                                                }, 3000);
                                          }, 3000);
                                    });
                                });
                            });
                            $('#negar').click(function () {
                                captcha = 0;
                                documento = document.getElementById('id_doc').value
                                Notiflix.Confirm.Show('Atenção!', 'Deseja realmente voltar e alterar os dados do arquivo?', 'Sim', 'Não', function(){
                                    retorno = requisicaoAjax('<?php echo appFunction::caminho ?>cAssinaturasDocumentos/validarRespostaAmostras/' + captcha + '/2/' + documento);
                                    Notiflix.Loading.Dots('Aguarde...');
                                    setTimeout(function(){
                                        location.reload();
                                        }, 3000);
                                });
                            });
                        </script>
                    </form>
                </div>
            </div>
            <?php
            exit;

    }
}
if (count($_SESSION['acessos']) > 1) {
    ?>
    <div class="modal fade" id="trocarEmpresa" role="dialog">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row text-center" style="margin: 20px">
                        <legend>Alternar Sistema</legend>
                        <?php
                        $i = 0;
                        foreach ($_SESSION['acessos'] as $empresa) {
                            ?>
                            <div onclick="trocarEmpresa(<?php echo $i ?>)" class="col-sm-12"
                                 style="background: rgba(233,233,233,0.0);
                             border-bottom: 1px solid rgba(168,168,168,0.17);
                             cursor:pointer;">
                                <div class="row">
                                    <div class="col-xs-4">
                                        <img style="margin-top: 18px;"
                                             height="25"
                                             src="/../../public/img/<?php echo $empresa['sigla'] ?>_card.png"/><br>
                                        <br>
                                    </div>
                                    <div class="col-xs-7">
                                        <small class="text-info text-justify">
                                            <br>
                                            <?php
                                            echo $empresa['unidade_neg']
                                            ?>
                                            <br><strong>Linha: </strong>
                                            <?php
                                            require_once('/../controller/cHomeController.php');
                                            $controlerHome = new cHomeController();
                                            $nomeLinha = $controlerHome->getNomeLinhaById($empresa['id_linha']);
                                            echo $nomeLinha[1][0];
                                            ?>
                                        </small>
                                    </div>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php

}
