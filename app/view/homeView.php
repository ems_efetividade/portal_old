<?php include('headerView.php');
//include ('vHome.php');
//$view  = new vHome();
//exit;
?>
    <script src="/../../public/js/home.js"></script>
    <link rel="stylesheet" href="../../public/css/homeView.css"/>
    <div style="margin-top:20px;"></div>
    <div class="container" id="dados-home">
        <div class="row">
            <div class="col-lg-8 col-sm-8">
                <div class="panel panel-default top-border" id="panel-colab">
                    <div class="panel-body">
                        <span class="pull-left">Olá <?php echo appFunction::tratarNome($nomeColaborador); ?>, seja bem-vindo(a)!</span>
                        <span class="pull-right">Hoje é <?php echo $dataExtenso ?> </span>
                    </div>
                    <h1>
                        <a href="<?php echo $urlArtigo ?>"><?php echo $tituloArtigo ?></a>
                    </h1>
                    <img class="img-responsive" style="margin:10px;" src="<?php echo $imgArtigo ?>"/>
                    <h2 class="text-justify">
                        <small style="line-height:25px;"><?php echo $resumoArtigo ?>
                            <a href="<?php echo $urlArtigo ?>">leia mais...</a>
                        </small>
                    </h2>
                </div>
            </div>
            <div class="col-lg-4 col-sm-4">
                <div class="panel panel-default" style="height: 400px!important;">
                    <div class="panel-body" style="height: 400px!important;">
                        <div id="carousel-informativo" class="carousel slide" data-ride="carousel" data-interval="4000">
                            <div class="carousel-inner">
                                <?php print $informativos ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default top-border">
                    <div class="panel-body">
                        <div class="col-lg-12">
                            <h4 class="header-box header-text-color">
                                <span class="glyphicon glyphicon-plus"></span> Veja também
                            </h4>
                            <?php //echo $listaArtigos; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- pilar equipe -->
        <!--
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default top-border">
                    <div class="panel-body">
                        <h4 class="header-box header-text-color">
                            <span class="glyphicon glyphicon-stats"></span> Nossos Resultados
                         </h4>

                          <div class="col-lg-3 col-sm-3">
                            <div class="bs-example">
                                <ul class="nav nav-pills nav-stacked" id="tabs">
                                    <li class="active"><a data-toggle="tab" href="#grafico1" chart="chart1">Vendas - Objetivo x Real</a></li>
                                    <li><a data-toggle="tab" href="#grafico2" chart="chart2">Demanda - EMS Prescrição x Labs Nacionais</a></li>

                                    <li><a data-toggle="tab" href="#grafico4" chart="chart4">Receituário - Prescription Share por Linha</a></li>

                                </ul>
                             </div>
                         </div>

                         <div class="col-lg-9 col-sm-9">
                            <div class="tab-content">
                                <div id="grafico1" class="tab-pane fade in active">


                                        <div id="chart1" style="max-width: 801px; height: 400px; margin: 0 auto; position:relative"></div>


                                </div>
                                <div id="grafico2" class="tab-pane fade">


                                        <div id="chart2" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                        <pre id="tsv" style="display:none">Browser Version	Total Market Share
                                            MERCADO ÉTICO NACIONAL 	10.7%
                                            EMS PRESCRICAO	14.9%
                                            ACHE	11.8%
                                            EUROFARMA	11.5%
                                            BIOLAB	9.8%
                                            LIBBS	6.8%
                                            </pre>
                                        <h4><i><span style="padding:20px" id="graf2-texto"></span></i></h4>

                                </div>
                                <div id="grafico3" class="tab-pane fade">

                                        <div id="chart3" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                        <h4><i>Dados fictício. O relatório esta sendo processado pela equipe.</i></h4>


                                </div>
                                <div id="grafico4" class="tab-pane fade">

                                        <div id="chart4" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                        <h4><span style="padding:20px" id="graf4-texto"></span></h4>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- slider -->
        <!--
<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel" data-interval="5000">
  <div class="carousel-inner">
    <div class="item active"><a href="http://www.ems.com.br/web/guest/home" target="_blank"><img src="<?php echo appConf::caminho ?>public/img/banner1.jpg" width="1920" height="680" class="img-responsive" /></a></div>
    <div class="item"><a href="http://www.ems.com.br/web/guest/home" target="_blank"><img src="<?php echo appConf::caminho ?>public/img/banner2.jpg" class="img-responsive" /></a></div>
  </div>
</div>
-->
        <div class="modal fade" id="modalPopupView">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <p>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                        </p>
                        <?php if ($linkPopup != '') { ?>
                            <a href="<?php print $linkPopup ?>"><img class="imagemPopup img-responsive"
                                                                     src="<?php print $imagemPopup ?>"/></a>
                        <?php } else { ?>
                            <img class="imagemPopup img-responsive" src="<?php print $imagemPopup ?>"/>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modalEnquete">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h3 class="modal-title">Insira sua foto!</h3>
                    </div>
                    <div class="modal-body">
                        <form id="formFoto" action="<?php echo appConf::caminho ?>home/salvarFoto"
                              enctype="multipart/form-data">
                            <input type="file" id="foto" name="foto" style="display: none"/>
                        </form>
                        <p>
                        </p>
                        <h5>Escolha uma foto sua, no estilo "3x4", respeitando os seguintes critérios:</h5>

                        <h5>

                            <ul>
                                <li>Fundo Branco</li>
                                <li>Foto no modo "Retrato"</li>
                                <li>Ter no mínimo 2 megabytes de tamanho</li>
                                <li>Estar no formato JPG, BMP, GIF ou TIF</li>
                                <li>Não utilizar "filtros" (Instagram)</li>
                            </ul>

                        </h5>
                        <p>
                            <button class="btn btn-primary btsn-block" id="btnSelFoto">Selecionar Foto</button>
                        </p>


                        <div id="ag"></div>
                        <!--         <img src="" id="fotoConv" class="img-responsive center-block" width="50%" style="display: none;" />-->


                        <div class="alert alert-danger teext-center" role="alert" id="alerta" style="display: none">

                        </div>


                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </div>
                </div>
            </div>
        </div>

<?php include('footerView.php') ?>