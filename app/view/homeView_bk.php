﻿<?php include('headerView.php');?>

<script src="<?php echo appConf::caminho ?>public/js/home.js"></script>
<script>
$(document).ready(function(e) {
	if($('.imagemPopup').attr('src') != "") {
		$('#modalPopupView').modal('show');
	}
	
	$('#width').html(screen.width + 'x' + screen.height);
	
	graficosHome();
        
        var a = $('#panel-grafico').height() + 3;
        var b = $('#panel-colab').height() + 20;

        c = (a + b);

        $('#panel-info').height(c);
        $('#carousel-informativo .item').height((c)-80);

});
$(window).ready(function(e) {

        
    
});
</script>

<!-- CSS -->
<link rel="stylesheet" href="public/css/homeView.css" />

<!-- espaçamento superior -->
<div style="margin-top:20px;"></div>

<div class="container" id="dados-home">

    
    
	<div class="row">
    	
    	<div class="col-lg-8 col-sm-8">
        sdasd
            <div class="panel panel-default top-border" id="panel-colab">
            	<div class="panel-body">
                	<span class="pull-left"><?php echo $periodoDia.' '.$nomeColaborador; ?>, seja bem-vindo!</span>
                    <span class="pull-right">Hoje é <?php echo $dataExtenso ?> </span>
                </div>
            </div>
            
            <div class="panel panel-default top-border " id="panel-grafico">
            	<div class="panel-body">
                	<h4 class="header-box header-text-color"><span class="glyphicon glyphicon-stats"></span> 
                    	Projeção EMS Prescrição 
                        <small class="pull-right">Atualizado em 17/10/2017</small></h4>
                	<div id="grafico-home1" style="width:100%;height:300px"></div>
                    <h4>No mês de Agosto fechamos com <b>R$ 132.996</b> milhões de Demanda contra um Objetivo de <b>R$ 141.735</b> milhões</b>.</h4>
                </div>
            </div>
            
        </div>
        

        
        <!-- coluna informativo -->
        <div class="col-lg-4 col-sm-4">
<!--            <p>
                <a href="#" class="btn btn-lg btn-warning btn-block" data-toggle="modal" data-target="#modalEnquete">
                    <span class="glyphicon glyphicon-pencil"></span> 
                    Foto
                </a>
            </p>
            <br>-->

            <div class="panel panel-default top-border-danger" id="panel-info">
                <div class="panel-body">

                        <div class="row">

                        <div class="col-lg-8">

                            <span class="pull-left">
                                <h4 class="header-text-color-danger">
                                    <b><span class="glyphicon glyphicon-list-alt"></span> Painel Informativo</b>
                                </h4>
                            </span>

                         </div>

                         <div class="col-lg-4">

                            <span class="pull-right" style="margin-top:11px;position:relative;z-index:999;">
                                <a class="nav-informativo btn btn-danger btn-sm" style="z-index:999;" acao="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>

                                <a class="nav-informativo btn btn-danger btn-sm" style="z-index:999;"  acao="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </span>

                           </div>

                       </div>


                    <div id="carousel-informativo" class="carousel slide" data-ride="carousel" data-interval="6500">



                      <!-- Wrapper for slides -->
                      <div class="carousel-inner">
                                                <?php print $informativos ?>
                      </div>

                    </div>
                </div>
            </div>
        </div>
        
    </div>
    
    
    
    
    
    
    
    
    <!-- painel ultimas noticias -->
    <div class="row">
    
    	<div class="col-lg-12">
        
            <div class="panel panel-default top-border">
            	<div class="panel-body">
                	<h4 class="header-box header-text-color"><span class="glyphicon glyphicon-file"></span> Últimas Notícias</h4>
                    
                    <!-- coluna noticia principal -->
                    <div class="col-lg-8">
                    	<h1><a href="<?php echo $urlArtigo ?>"><?php echo $tituloArtigo ?></a></h1>
                    	<img class="img-responsive" style="margin-bottom:10px;" src="<?php echo $imgArtigo ?>" />
                    	<h2 class="text-justify"><small style="line-height:25px;"><?php echo $resumoArtigo ?>
                        	<a href="<?php echo $urlArtigo ?>">leia mais...</a></small>
                        </h2>
                    </div>
                    
                    <!-- coluna outras noticias -->
                    <div class="col-lg-4">
                    	<h4 class="header-box header-text-color">
                        	<span class="glyphicon glyphicon-plus"></span> Veja também
                        </h4>
                    	<?php echo $listaArtigos; ?>
                    </div>
                    
            	</div>
            </div>
        </div>
    </div>

    
    
    
    
    
    <!-- pilar equipe -->
    <div class="row">
    	<div class="col-lg-12">
        	<div class="panel panel-default top-border">
            	<div class="panel-body">
                	<h4 class="header-box header-text-color">
                    	<span class="glyphicon glyphicon-stats"></span> Nossos Resultados
                     </h4>
                  
                  	<div class="col-lg-3 col-sm-3">
                        <div class="bs-example">
                            <ul class="nav nav-pills nav-stacked" id="tabs">
                                <li class="active"><a data-toggle="tab" href="#grafico1" chart="chart1">Vendas - Objetivo x Real</a></li>
                                <li><a data-toggle="tab" href="#grafico2" chart="chart2">Demanda - EMS Prescrição x Labs Nacionais</a></li>
                                <!--<li><a data-toggle="tab" href="#grafico3" chart="chart3">Demanda - Evolução por Linha</a></li>-->
                                <li><a data-toggle="tab" href="#grafico4" chart="chart4">Receituário - Prescription Share por Linha</a></li>
                                
                            </ul>
                         </div>
                     </div>
                     
                     <div class="col-lg-9 col-sm-9">
                        <div class="tab-content">
                            <div id="grafico1" class="tab-pane fade in active">
                                
                                	<!--<h4 class="header-box header-text-color"><span class="glyphicon glyphicon-stats"></span> Evolução Linhas EMS Prescrição</h4>
                                    <hr />-->
									<div id="chart1" style="max-width: 801px; height: 400px; margin: 0 auto; position:relative"></div>
                                    <!--<h4><span style="padding:20px"><i>Estamos há <b>3</b> Meses sem realizar a Cobertura de Vendas, porém estamos com uma Cobertura acumulada em 2014 de <b>98%</b>.</i></span></h4>-->
                                
                            </div>
                            <div id="grafico2" class="tab-pane fade">
                                	
	                                <!--<h4 class="header-box header-text-color"><span class="glyphicon glyphicon-stats"></span> Gráfico 2</h4>
                                    <hr />-->
                                    <div id="chart2" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                    <pre id="tsv" style="display:none">Browser Version	Total Market Share
                                        MERCADO ÉTICO NACIONAL 	12.5%
                                        EMS PRESCRICAO	14.2%
                                        ACHE	15.4%
                                        EUROFARMA	12.7%
                                        BIOLAB	14.0%
                                        LIBBS	9.7%
                                        </pre>
                                    <h4><i><span style="padding:20px">Estamos há mais <b>2</b> Anos evoluindo acima do Mercado Ético Nacional(apenas Labs Nacionais). Evoluímos acima dos Laboratórios <b><!--<span style="color:#FF1493"> Ache</span>,--><span style="color:#FFD700"> Eurofarma</span>,<span style="color:#1E90FF"> Biolab</span>,</b><span style="color:#9ACD32"><b> Libbs</b></span>.</span></i></h4>
                               
                            </div>
                            <div id="grafico3" class="tab-pane fade">
                                
                                	<div id="chart3" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                    <h4><i>Dados fictício. O relatório esta sendo processado pela equipe.</i></h4>
									
                                
                            </div>
                            <div id="grafico4" class="tab-pane fade">
                                    <!--<h4 class="header-box header-text-color"><span class="glyphicon glyphicon-stats"></span> Gráfico 4</h4>
                                    <hr />-->
                                    <div id="chart4" style="max-width: 801px; height: 400px; margin: 0 auto"></div>
                                    <h4><span style="padding:20px"><i>Neste gráfico temos a evolução de Prescription Share dos últimos <b>13 Meses por Linha</b>. </i></span></h4>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    

    
    
</div>

<!-- slider -->
<div id="carousel-example-generic1" class="carousel slide" data-ride="carousel" data-interval="5000">
  <div class="carousel-inner">
    <div class="item active"><a href="http://www.ems.com.br/web/guest/home" target="_blank"><img src="<?php echo appConf::caminho ?>public/img/banner1.jpg" width="1920" height="680" class="img-responsive" /></a></div>
    <div class="item"><a href="http://www.ems.com.br/web/guest/home" target="_blank"><img src="<?php echo appConf::caminho ?>public/img/banner2.jpg" class="img-responsive" /></a></div>
  </div>
</div>





<!--MODAL INFORMATIVO VIEW-->
<div class="modal fade" id="modalPopupView">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
     
	 
     <div class="modal-body">
     	<p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>
        <img class="imagemPopup img-responsive" src="<?php print $imagemPopup ?>" />
     				
	</div>
    </div>
  </div>
</div>






<!--MODAL INFORMATIVO VIEW-->
<div class="modal fade" id="modalEnquete">
  <div class="modal-dialog">
    <div class="modal-content">
     
<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h3 class="modal-title">Insira sua foto!</h3>
      </div>
        
     <div class="modal-body">
<!--     	<p><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></p>-->
       
         <form id="formFoto" action="<?php echo appConf::caminho ?>home/salvarFoto" enctype="multipart/form-data">
             <input type="file" id="foto" name="foto" style="display: none" />
         </form>
         
         <p>
             
             
             
         </p>
         
         
         
         <h5>Escolha uma foto sua, no estilo "3x4", respeitando os seguintes critérios:</h5>
         
         <h5>
             
             <ul>
                 <li>Fundo Branco</li>
                 <li>Foto no modo "Retrato"</li>
                 <li>Ter no mínimo 2 megabytes de tamanho</li>
                 <li>Estar no formato JPG, BMP, GIF ou TIF</li>
                 <li>Não utilizar "filtros" (Instagram)</li>
             </ul>
             
         </h5>
         <p><button class="btn btn-primary btsn-block" id="btnSelFoto">Selecionar Foto</button></p>

         
         <div id="ag"></div>
<!--         <img src="" id="fotoConv" class="img-responsive center-block" width="50%" style="display: none;" />-->
         
         
         <div class="alert alert-danger teext-center" role="alert" id="alerta" style="display: none">
             
         </div>

         
         
	</div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>

<script>
function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            
            //alert(e.target.result);
            $('#fotoConv').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}
</script>

<script>

function alerta(texto, classe) {
    $('#alerta').hide();
    $('#alerta').removeClass('alert-danger');
    $('#alerta').removeClass('alert-info');
    $('#alerta').removeClass('alert-success');
    
    $('#alerta').addClass(classe);
    $('#alerta').html(texto);
    $('#alerta').show();
}

$('#btnSelFoto').click(function(e) {
    $('#alerta').hide();
    $('#foto').click();
});

$('#foto').change(function(e) {
    
    alerta('<span class="glyphicon glyphicon-refresh"></span> Aguarde, fazendo o upload da sua foto!', 'alert-info');
    readURL(this);
   
    
    $.ajax({
            type: "POST",
            url: $('#formFoto').attr('action'),
            data: new FormData($('#formFoto')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                
            //try {
                dados = $.parseJSON(retorno);
                
                if($.trim(dados.erro) == "OK") {
                   alerta('<span class="glyphicon glyphicon-ok"></span> Sucesso! Sua foto foi enviada com sucesso!', 'alert-success');
                   setTimeout(function(){ location.reload(); }, 3000);
                    //readURL($('#foto'));
                    //$('#fotoConv').show();
                    
                    //$('#fotoConv').attr('src', dados.link);
                } else {
                    alerta('<b><span class="glyphicon glyphicon-remove"></span> Ops!</b> ' + dados.erro, 'alert-danger');
                }
                
            //} catch(err) {
                //alert(err);
            //}
                
                
            },
            beforeSend: function (a) {
               //modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
            }
        });
    
});




</script>

<?php include('footerView.php') ?>