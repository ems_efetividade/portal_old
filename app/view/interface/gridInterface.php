<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

interface gridInterface {


    /**
     * Está função é padronizada para funcionamento do gridview, recebe qualquer atributo por post e executa uma consulta sql
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     * @return arrayObj[]
     */
    public function listObj();

    /**
     * Está função é padronizada para funcionamento do gridview, recebe qualquer atributo por post e executa uma consulta sql
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     * @return object
     */
    public function loadObj();

    /**
     * função padrão para inserção de dados sql, pode ser reimplementada, ou utilizada em conjunto com outras ações
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     * @return null
     */
    public function save();

    /**
     * Função padrão para deletar registros do banco de dados através do gridview
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     * @return count Registros Excluidos
     */
    public function delete();

    /**
     * Função padrão para atualizar registros do banco de dados através do gridview, o atributo padrão para funcionalidade
     * correta deve ser o ID da tabela
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     *@return null
     */
    public function updateObj();

    /**
     * Função interna do framework, não deve sofrer reimplementação nem alterações, utilizada por padrão pelo gridview
     * para paginação, deve serpre estar associada a linha rowCount() em SQL e os parametros para funcionamento são
     * implementados automaticamete pela classe gridview
     * Os atributos devem seguir o padrão de nomeclatura "nomeDoAtributo" e devem ser iguais ao nome da coluna do banco
     * @param $sql
     * @return String SQL
     */
    public function pages($sql);
    
}
