<?php 	include('headerView.php');
require_once ('listaColaboradoresView.php');

?>
<script src="<?php echo appConf::caminho ?>plugins/maskedit/jquery.maskedinput.min.js"></script>
<script>

$(document).ready(function(e) {
	// Seleciona o colaborador pra mostrar os dados
	$('#modalExcluirColaborador').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idColaborador = button.data('idcolaborador') // Extract info from data-* attributes
		$('#formExcluirColaborador #txtIdColaborador').val(idColaborador);
		
		$('#btnExcluirColaborador').click(function(e) {
			modalAguarde('show');
			$('#modalExcluirColaborador').modal('hide');
            
        });
	});
	// Seleciona o colaborador pra mostrar os dados
	$('#modalVisualizarColaborador').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idColaborador = button.data('idcolaborador') // Extract info from data-* attributes
		
		$.post('<?php echo appConf::caminho ?>comunicacao/visualizarColaborador', {
			idColaborador: idColaborador
		}, function(retorno) {
			$('#conteudo-modal').html(retorno);
		});
	});
	//seleciona o colaborador para alterar o cadastro ou adicionar um novo
	$('#modalAdicionarColaborador').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idColaborador = button.data('idcolaborador') // Extract info from data-* attributes
		$.post('<?php echo appConf::caminho ?>comunicacao/manipularColaborador', {
			idColaborador: idColaborador
		}, function(retorno) {
			//abre o formulario
			$('#form-colaborador').html(retorno);
			$("input[name*='txtCep']").mask("99999-999",{placeholder:" "});
			$("input[name*='txtCpf']").mask("999.999.999-99",{placeholder:" "});
			$("input[name*='txtRg']").mask("99.999.999-9",{placeholder:" "});
			$("input[name*='txtDataNasc']").mask("99/99/9999",{placeholder:" "});
			//popula o combo de funcoes quando o DEPARTMENTO é alterado.
			$('#selDepto').change(function(e) {
                $.post('<?php echo appConf::caminho ?>comunicacao/popularComboFuncao', {
					funcao: $(this).val()
				}, function(retorno) {
					$('#comboFuncao').html(retorno);
				});
            });
			//popla o combo de cidades quando o de UF é alterado.
			$('#selUF').change(function(e) {
                $.post('<?php echo appConf::caminho ?>comunicacao/popularComboCidade', {
					uf: $(this).val()
				}, function(retorno) {
					$('#comboCidade').html(retorno);
				});
            });
			//envia o formulario
			$('#formSalvarUsuario').submit(function(e) {
                                e.preventDefault();
                                e.stopImmediatePropagation();
				$.ajax({
				 type: "POST",
				  url: $(this).attr('action'),
				  data: $(this).serialize(),
				  success: function(retorno) {
					if(retorno != "") {
						msgBox(retorno);	
					} else {
						$('#modalAdicionarColaborador').modal('hide');
						modalAguarde('show');
						window.location = window.location;
					}
				   }
				});
				e.preventDefault();
            });
		});
	});
});

</script>
<style>
#conteudo-modal{
	overflow:auto;	
	max-height:450px;
}

#conteudo-modal p {
	font-size:12px;	
	margin:7px;
	color:#333;
}

.modal-header {
   border-top-left-radius: 5px;
   border-top-right-radius: 5px;
}
.foto-usuario {
	text-align:center;
	border:1px solid #ccc;	
	margin-bottom:10px;
}
hr {
	margin-top:0px;
	margin-bottom: 0px;	
}

.table > tbody > tr > td {
     vertical-align: middle;
}
 body { padding-top:70px }
.container { width:100%; }
</style>
<div class="container" style="min-height:400px;">
    <br>
	<div class="row">
    	<div class="col-lg-2">
        	<span style="font-size:20px" class=""><strong><span class="glyphicon glyphicon-user"></span> Colaboradores</strong></span>
        </div>
		<div class="col-lg-5">
        	<form action="<?php echo appConf::caminho ?>colaboradores/pesquisarColaborador" method="post">
        	<div class="input-group">
				<input name="txtCriterio" type="text" class="form-control input-sm" style="font-weight:normal">
				<span class="input-group-btn">
					<button type="submit" class="btn btn-default btn-sm" ><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
				</span>
			</div>
            </form>
        </div>
    	<div class="col-lg-5">
        	<div class="pull-right">
        	<?php print $botaoAdicionar ?>
            <?php print $comboDepartamento ?>
            <?php print $comboExportar ?>
            </div>
        </div>
    </div>
    <p></p>
    <p></p>
    <div class="row">
    	<div class="col-lg-12">
        	<?php print $listaColaborador; ?>
        </div>
    </div>
</div>
<!--MODAL VISUALIZAR COLABORADOR-->
<div class="modal fade" id="modalVisualizarColaborador" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Dados do Colaborador</h4>
      </div>
      <div class="modal-body" id="conteudo-modal">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
       <!-- <button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div>
  </div>
</div>
<!--MODAL ADICIONAR COLABORADOR-->
<div class="modal fade" id="modalAdicionarColaborador" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Dados do Colaborador</h4>
      </div>
      <form id="formSalvarUsuario" action="<?php echo appConf::caminho ?>comunicacao/salvarColaborador" method="POST">
     <div class="modal-body max-height" id="form-colaborador">
	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
      </div>
      </form>
    </div>
  </div>
</div>
<!--MODAL EXCLUIR COLABORADOR-->
<div class="modal fade" id="modalExcluirColaborador" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Dados do Colaborador</h4>
      </div>
     <div class="modal-body max-height">
    	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/alert.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja excluir esse colaborador?</h4>
            </div>
        </div>
	</div>
      <div class="modal-footer">
      <form id="formExcluirColaborador" action="<?php echo appConf::caminho ?>comunicacao/excluirColaborador" method="POST">
      	<input type="hidden" value="" id="txtIdColaborador" name="txtIdColaborador"  />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger" id="btnExcluirColaborador">Excluir</button>
      </form>
      </div>
    </div>
  </div>
</div>
<?php include('footerView.php') ?>