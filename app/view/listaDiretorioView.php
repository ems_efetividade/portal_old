<?php 	include('headerConfiguracaoView.php'); ?>
<script>

$(document).ready(function(e) {
    // Seleciona a noticia pra mostrar os dados
	$('#modalAdicionarRaiz').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idPasta = button.data('id-pasta') // Extract info from data-* attributes
		
		if(idPasta == 0) {
			limparCampos();
		} else {
			$.getJSON('<?php echo appConf::caminho ?>configuracao/editarDiretorioRaiz/'+idPasta+'', function(pasta) {	
				popularCampos(pasta);
			});
		}
	});
	
	$('#modalExcluirRaiz').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idPasta = button.data('id-pasta') // Extract info from data-* attributes
		
		$('#formExcluirRaiz #txtIdRaiz').val(idPasta);
	});
	
});

function limparCampos() {
	$("#formAdicionarRaiz input[type=text]").val("");
}

function popularCampos(pasta) {
	$("input[name='txtIdRaiz']").val(pasta.idPasta);
	$("input[name='txtPasta']").val(pasta.pasta);
}
</script>

<div class="container">
	
    <div class="row">
    
     	<div class="col-lg-6">
        	<span style="font-size:25px" class=""><strong><span class="glyphicon glyphicon-folder-close"></span> Diretório raiz</strong></span>
        </div>
    
    	<div class="col-lg-6">
        	<div class="pull-right">
        		<button type="button" id="btn-formulario" data-toggle="modal" data-target="#modalAdicionarRaiz" data-id-pasta="0" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Adicionar Diretório</button>          	</div>
        </div>
    </div>
    
    <p></p>
    
    <div class="row">
    	<div class="col-lg-12">
        	<?php print $listaDiretorio; ?>
        </div>
    </div>
</div>


<!--MODAL ADICIONAR NOTICIA-->
<div class="modal fade" id="modalAdicionarRaiz" >
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Adicionar/Editar Notícia</h4>
      </div>
      
      <form id="formAdicionarRaiz" action="<?php echo appConf::caminho ?>configuracao/salvarDiretorioRaiz" method="POST">
      	<input type="hidden" name="txtIdRaiz" id="txtIdRaiz" value="" />
      
      <div class="modal-body max-height">
        	<div class="row">
                               
                 <div class="col-lg-12">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><small>Nome da Pasta</small></label>
                        <input type="text" class="form-control input-sm" name="txtPasta">
                    </div>
                </div>
    
            </div>	
	  </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        
        </form>
      </div>
     
    </div>
  </div>
</div>

<!--MODAL EXCLUIR NOTICIA-->
<div class="modal fade" id="modalExcluirRaiz" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Excluir Notícia</h4>
      </div>
      
     <div class="modal-body max-height">
	
    	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/alert.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja excluir essa pasta Raiz e todo o seu conteúdo?</h4>
            </div>
        </div>			
							
	</div>
      <div class="modal-footer">
      <form id="formExcluirRaiz" action="<?php echo appConf::caminho ?>configuracao/excluirPastaRaiz" method="POST">
      	<input type="hidden" value="" id="txtIdRaiz" name="txtIdRaiz"  />
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger" id="btnExcluirNoticia">Excluir</button>
      </form>
      </div>
      
    </div>
  </div>
</div>