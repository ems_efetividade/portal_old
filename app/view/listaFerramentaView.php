<?php 	include('headerConfiguracaoView.php'); ?>
<style>
.permissao { border:1px solid #eee; height:340px; padding:5px; overflow:auto}
.vertical-container {
    display: table;
    width: 100%;
	height:340px;
}
.vertical-container .wow {
    display: table-cell;
    vertical-align: middle;
	text-align:center;
}

.disable {
    text-decoration: line-through;
    color: #ccc;
}

</style>
<script>
    
function popularCampos(informativo) {
    $("input[name='txtIdFerramenta']").val(informativo.idFerramenta);
    $("input[name='txtNomeFerramenta']").val(informativo.ferramenta);
    $("input[name='txtCaminho']").val(informativo.caminho);
    $("input[name='txtFornecedor']").val(informativo.fornecedor);
    $("select[name='cmbIntegrado']").val(informativo.tipoAcesso).change();
    $("select[name='cmbAbrir']").val(informativo.tipoAbrir).change();
    $("textarea[name='txtDescricao']").text(informativo.descricao);

    $("select[name='cmbModulo']").html('');
    $("select[name='cmbModulo']").append(informativo.cmbModulo);
    $("select[name='cmbModulo']").val(informativo.nivel).change();
    $("select[name='cmbAtivo']").val(informativo.ativo).change();
}

$(document).ready(function(e) {
    
        $('#btnSalvarFerramenta').unbind().click(function(e) {        
            
            $.ajax({
                type: "POST",
                url: $('#formSalvarFerramenta').attr('action'),
                data: $('#formSalvarFerramenta').serialize(),
                success: function(retorno) {
                    if($.trim(retorno) != '') {
                        //alert(retorno);
                        
                        servers = $.parseJSON(retorno);
                        var html = 'Houve alguns erros:<br><br>';
                        $.each(servers, function(index, value) {
                            html = html + value + '<br>';
                        });
                        
                        $('#modalFerramentaErro .modal-body').html(html);
                        $('#modalFerramentaErro').modal('show');
                    } else {
                        location.reload();
                    }
                    
                }
            });
        });
    
    
    $('#modalAdicionarFerramenta').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idFerramenta = (typeof button.data('id-ferramenta') != 'undefined') ? button.data('id-ferramenta') : 0; // Extract info from data-* attributes

        //alert(idFerramenta);

        $.getJSON('<?php echo appConf::caminho ?>configuracao/formFerramenta/'+idFerramenta+'', function(informativo) {	
            popularCampos(informativo);
        });
        

    });
    
    $('#modalExcluirFerramenta').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idFerramenta = (typeof button.data('id-ferramenta') != 'undefined') ? button.data('id-ferramenta') : 0; // Extract info from data-* attributes

        $('#btnExcluirFerramenta').unbind().click(function(e) {
            
            $.post('<?php echo appConf::caminho ?>configuracao/excluirFerramenta', {
                    idFerramenta:idFerramenta
            }, function(retorno) {
                alert(retorno);
                   location.reload();
            });
            
            
        });

      
        

    });
    
    
    // Seleciona ferramenta
	$('#modalPermissaoFerramenta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idFerramenta = button.data('id-ferramenta') // Extract info from data-* attributes
		var nomeFerramenta = button.data('nome-ferramenta') // Extract info from data-* attributes

		
		$('#nomeFerramenta').html(nomeFerramenta);
		$('#txtIdFerramenta').val(idFerramenta);
		
		$.post('<?php echo appConf::caminho ?>configuracao/listarPerfilCompleto', {}, function(retorno) {
			$('#perfil-completo').html(retorno);
		});
		
		$.post('<?php echo appConf::caminho ?>configuracao/listarPermissao', {
			idFerramenta:idFerramenta
		}, function(retorno) {
			$('#permissao-ferramenta').html(retorno);
		});
		
		
		$('#btnAdicionarPermissao').click(function(e) {
			mover('#perfil-completo', '#permissao-ferramenta');
        });
		
		$('#btnRemoverPermissao').click(function(e) {
			$('#permissao-ferramenta input:checkbox:checked').each(function () {
                            
                                $('#formSalvarPermissao').append('<input type="hidden" name="permExcluir[]"  value="'+$(this).val()+'" />');
                            
				$(this).closest('.perfil').remove();
			});
        });
		
		$('#btnSalvarPermissao').click(function(e) {
			$('#modalPermissaoFerramenta').modal('hide');
			//modalAguarde('show');
                    $('#formSalvarPermissao').submit();
                });

	});
});

function mover(de, para) {
    $(de + ' input:checkbox:checked').each(function () {
        valor = $(this).val();
        checkbox = '<div class="perfil">' + $(this).closest('.perfil').html() + '</div>';

        if($(para + ' input:checkbox[value='+valor+']').val() != valor) {
                $(para).html($(para).html() + checkbox);
        }

        $(de + ' input:checkbox[value='+valor+']').prop('checked', false);
    });
}

function removerPermissao() {
	
}

</script>
<div class="container">

	<div class="row">
            <div class="col-lg-7">
                    <span style="font-size:25px" class=""><strong><span class="glyphicon glyphicon-globe"></span> Acessos OnLine</strong></span>
            </div>
            <div class="col-lg-5 text-right">
                <button type="button" id="btn-formulario" data-toggle="modal" data-target="#modalAdicionarFerramenta" data-id-informativo="0" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Adicionar Ferramenta</button>
            </div>
        </div>
    
    <p></p>
    
    <div class="row">
    	<div class="col-lg-12">
        	<?php print $listaFerramenta ?>
        </div>
        
        <div class="col-lg-6"></div>
    </div>
    
</div>


<!--MODAL ADICIONAR NOTICIA-->
<div class="modal fade" id="modalAdicionarFerramenta" >
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Adicionar/Editar Ferramenta</h4>
      </div>
	 
        <div class="modal-body" id="form-colaborador">
            <form id="formSalvarFerramenta" action="<?php echo appConf::caminho ?>configuracao/salvarFerramenta" method="POST">
                <input type="hidden" name="txtIdFerramenta" value="0">
                <div class="row">
                    <div class="col-lg-10">
                        <div class="form-group">
                            <label>Nome da Ferramenta</label>
                            <input type="text" class="form-control" placeholder="" name="txtNomeFerramenta">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>Ativo?</label>
                            <select class="form-control" name="cmbAtivo">
                                <option></option>
                                <option value="1">SIM</option>
                                <option value="0">NÃO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>URL</label>
                            <input type="text" class="form-control" placeholder="" name="txtCaminho">
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="form-group">
                            <label>Módulo</label>
                            <select class="form-control" name="cmbModulo">

                            </select>
                        </div>
                    </div>

                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>É Integrado?</label>
                            <select class="form-control" name="cmbIntegrado">
                                <option></option>
                                <option value="0">SIM</option>
                                <option value="1">NÃO</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label>Abrir em: </label>
                            <select class="form-control" name="cmbAbrir">
                                <option></option>
                                <option value="1">Popup</option>
                                <option value="0">Portal</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="form-group">
                            <label>Fornecedor</label>
                            <input type="text" class="form-control" placeholder="" name="txtFornecedor">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label>Descrição</label>
                            <textarea class="form-control" rows="5" placeholder="" name="txtDescricao"></textarea>
                        </div>
                    </div>
                </div>
            </form>
	</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarFerramenta"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
      
    </div>
  </div>
</div>

<!--MODAL PERMISSAO FERRAMENTA-->
<div class="modal fade" id="modalPermissaoFerramenta" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>
      
      
      	
      
      <div class="modal-body max-height">
       	<div class="row">
        
        	<div class="col-lg-5" style="padding-right:0px;">
            	<p><strong>Perfis</strong></p>
            	<div class="permissao" id="perfil-completo"></div>
            </div>
            
            <div class="col-lg-2" >
            	<div class="vertical-container">
            		<div class="wow">
                    	<p><button class="btn btn-default" id="btnAdicionarPermissao" /><span class="glyphicon glyphicon-chevron-right"></span> </button></p>
                        <p><button class="btn btn-default" id="btnRemoverPermissao" /><span class="glyphicon glyphicon-chevron-left"></span> </button></p>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5" style="padding-left:0px;">
            	<form id="formSalvarPermissao" action="<?php echo appConf::caminho ?>configuracao/salvarPermissaoFerramenta" method="POST">
                <input type="hidden" name="txtIdFerramenta" id="txtIdFerramenta" value="" />
                <p><strong>Perfis Permitidos</strong></p>
            	<div class="permissao" id="permissao-ferramenta"></div>
                </form>
            </div>
        </div>
	  </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        
        <button type="submit" id="btnSalvarPermissao" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        
        
      </div>
     
    </div>
  </div>
</div>


<!--MODAL PERMISSAO FERRAMENTA-->
<div class="modal fade" id="modalEditarFerramenta" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>

      <div class="modal-body max-height">
       	<div class="row">
        
        	<div class="col-lg-5" style="padding-right:0px;">
            	<p><strong>Perfis</strong></p>
            	<div class="permissao" id="perfil-completo"></div>
            </div>
            
            <div class="col-lg-2" >
            	<div class="vertical-container">
            		<div class="wow">
                    	<p><button class="btn btn-default" id="btnAdicionarPermissao" /><span class="glyphicon glyphicon-chevron-right"></span> </button></p>
                        <p><button class="btn btn-default" id="btnRemoverPermissao" /><span class="glyphicon glyphicon-chevron-left"></span> </button></p>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5" style="padding-left:0px;">
            	<form id="formSalvarPermissao" action="<?php echo appConf::caminho ?>configuracao/salvarPermissaoFerramenta" method="POST">
                <input type="hidden" name="txtIdFerramenta" id="txtIdFerramenta" value="" />
                <p><strong>Perfis Permitidos</strong></p>
            	<div class="permissao" id="permissao-ferramenta"></div>
                </form>
            </div>
        </div>
	  </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        
        <button type="submit" id="btnSalvarPermissao" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        
        
      </div>
     
    </div>
  </div>
</div>


<!-- EXCLUIR --->
<div class="modal fade" id="modalExcluirFerramenta" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>

      <div class="modal-body max-height">
          Deseja Excluir essa ferramenta?
        </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal" id="btnExcluirFerramenta"><span class="glyphicon glyphicon-trash"></span>  Excluir</button>
       
      </div>
     
    </div>
  </div>
</div>

<!-- ERRO --->
<div class="modal fade" id="modalFerramentaErro" style="z-index: 10000">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>

      <div class="modal-body max-height">
       	
        </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
       
      </div>
     
    </div>
  </div>
</div>