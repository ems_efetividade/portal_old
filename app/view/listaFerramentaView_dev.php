<?php 	include('headerConfiguracaoView.php'); ?>
<style>
.permissao { border:1px solid #eee; height:340px; padding:5px; overflow:auto}
.vertical-container {
    display: table;
    width: 100%;
	height:340px;
}
.vertical-container .wow {
    display: table-cell;
    vertical-align: middle;
	text-align:center;
}
</style>
<script>

$(document).ready(function(e) {
    // Seleciona ferramenta
	$('#modalPermissaoFerramenta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idFerramenta = button.data('id-ferramenta') // Extract info from data-* attributes
		var nomeFerramenta = button.data('nome-ferramenta') // Extract info from data-* attributes

		
		$('#nomeFerramenta').html(nomeFerramenta);
		$('#txtIdFerramenta').val(idFerramenta);
		
		$.post('<?php echo appConf::caminho ?>configuracao/listarPerfilCompleto', {}, function(retorno) {
			$('#perfil-completo').html(retorno);
		});
		
		$.post('<?php echo appConf::caminho ?>configuracao/listarPermissao', {
			idFerramenta:idFerramenta
		}, function(retorno) {
			$('#permissao-ferramenta').html(retorno);
		});
		
		
		$('#btnAdicionarPermissao').click(function(e) {
			mover('#perfil-completo', '#permissao-ferramenta');
        });
		
		$('#btnRemoverPermissao').click(function(e) {
			$('#permissao-ferramenta input:checkbox:checked').each(function () {
				$(this).closest('.perfil').remove();
			});
        });
		
		$('#btnSalvarPermissao').click(function(e) {
			$('#modalPermissaoFerramenta').modal('hide');
			//modalAguarde('show');
            $('#formSalvarPermissao').submit();
        });

	});
});

function mover(de, para) {
	$(de + ' input:checkbox:checked').each(function () {
		valor = $(this).val();
		checkbox = '<div class="perfil">' + $(this).closest('.perfil').html() + '</div>';
		
		if($(para + ' input:checkbox[value='+valor+']').val() != valor) {
			$(para).html($(para).html() + checkbox);
		}
		
		$(de + ' input:checkbox[value='+valor+']').prop('checked', false);
	});
}

function removerPermissao() {
	
}

</script>
<div class="container">

	<div class="row">
    	<div class="col-lg-12">
        	<span style="font-size:25px" class=""><strong><span class="glyphicon glyphicon-globe"></span> Acessos OnLine</strong></span>
        </div>
    </div>
    
    <p></p>
    
    <div class="row">
    	<div class="col-lg-12">
        	<?php print $listaFerramenta ?>
        </div>
        
        <div class="col-lg-6"></div>
    </div>
    
</div>

<!--MODAL PERMISSAO FERRAMENTA-->
<div class="modal fade" id="modalPermissaoFerramenta" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>
      
      
      	
      
      <div class="modal-body max-height">
       	<div class="row">
        
        	<div class="col-lg-5" style="padding-right:0px;">
            	<p><strong>Perfis</strong></p>
            	<div class="permissao" id="perfil-completo"></div>
            </div>
            
            <div class="col-lg-2" >
            	<div class="vertical-container">
            		<div class="wow">
                    	<p><button class="btn btn-default" id="btnAdicionarPermissao" /><span class="glyphicon glyphicon-chevron-right"></span> </button></p>
                        <p><button class="btn btn-default" id="btnRemoverPermissao" /><span class="glyphicon glyphicon-chevron-left"></span> </button></p>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5" style="padding-left:0px;">
            	<form id="formSalvarPermissao" action="<?php echo appConf::caminho ?>configuracao/salvarPermissaoFerramenta" method="POST">
                <input type="hidden" name="txtIdFerramenta" id="txtIdFerramenta" value="" />
                <p><strong>Perfis Permitidos</strong></p>
            	<div class="permissao" id="permissao-ferramenta"></div>
                </form>
            </div>
        </div>
	  </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        
        <button type="submit" id="btnSalvarPermissao" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        
        
      </div>
     
    </div>
  </div>
</div>


<!--MODAL PERMISSAO FERRAMENTA-->
<div class="modal fade" id="modalEditarFerramenta" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> <span id="nomeFerramenta"></span> - Permissões</h4>
      </div>

      <div class="modal-body max-height">
       	<div class="row">
        
        	<div class="col-lg-5" style="padding-right:0px;">
            	<p><strong>Perfis</strong></p>
            	<div class="permissao" id="perfil-completo"></div>
            </div>
            
            <div class="col-lg-2" >
            	<div class="vertical-container">
            		<div class="wow">
                    	<p><button class="btn btn-default" id="btnAdicionarPermissao" /><span class="glyphicon glyphicon-chevron-right"></span> </button></p>
                        <p><button class="btn btn-default" id="btnRemoverPermissao" /><span class="glyphicon glyphicon-chevron-left"></span> </button></p>
                        
                    </div>
                </div>
            </div>
            
            <div class="col-lg-5" style="padding-left:0px;">
            	<form id="formSalvarPermissao" action="<?php echo appConf::caminho ?>configuracao/salvarPermissaoFerramenta" method="POST">
                <input type="hidden" name="txtIdFerramenta" id="txtIdFerramenta" value="" />
                <p><strong>Perfis Permitidos</strong></p>
            	<div class="permissao" id="permissao-ferramenta"></div>
                </form>
            </div>
        </div>
	  </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
        
        <button type="submit" id="btnSalvarPermissao" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        
        
      </div>
     
    </div>
  </div>
</div>