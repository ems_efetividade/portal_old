﻿<?php ?>
    <script src="<?php echo appConf::caminho ?>plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo appConf::caminho ?>plugins/datepicker/js/bootstrap-datepicker.js"></script>
    <script>

        $(document).ready(function (e) {
            $("input[name='txtDataInicio'], input[name='txtDataFim']").datepicker({
                format: 'dd/mm/yyyy'
            });
            // Seleciona a noticia pra mostrar os dados
            $('#modalAdicionarInformativo').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idInformativo = button.data('id-informativo') // Extract info from data-* attributes
                if (typeof idInformativo != 'undefined') {
                    if (idInformativo == 0) {
                        limparCampos();
                    } else {
                        $.getJSON('<?php echo appConf::caminho ?>comunicacao/editarInformativo/' + idInformativo + '', function (informativo) {
                            popularCampos(informativo);
                        });
                    }
                }
            });
            // Seleciona a noticia pra mostrar os dados
            $('#modalExcluirInformativo').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idInformativo = button.data('id-informativo') // Extract info from data-* attributes

                $('#formExcluirInformativo #txtIdInformativo').val(idInformativo);

                $('#btnExcluirInformativo').click(function (e) {
                    modalAguarde('show');
                });
            });
            // Seleciona a noticia pra mostrar os dados
            $('#modalInformativoView').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idInformativo = button.data('id-informativo') // Extract info from data-* attributes

                $.post('<?php echo appConf::caminho ?>comunicacao/previewInformativo', {
                    idInformativo: idInformativo
                }, function (retorno) {
                    $('#preview-informativo').html(retorno);
                });
            });
        });

        function limparCampos() {
            $("#formSalvarInformativo input[type=text], textarea").val("");
            CKEDITOR.instances['txtTexto'].setData('');
        }

        function popularCampos(informativo) {
            $("input[name='txtIdInformativo']").val(informativo.idInformativo);
            $("input[name='txtAtivo']").val(informativo.ativo);
            $("input[name='txtTitulo']").val(informativo.titulo);
            $("input[name='txtDataFim']").val(informativo.dataFim);
            $("input[name='txtOrdem']").val(informativo.ordem);
            $('#linha').html(informativo.linha);
            CKEDITOR.instances['txtTexto'].setData(informativo.texto);
        }
    </script>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <span style="font-size:20px"><strong><span class="glyphicon glyphicon-info-sign"></span> Cadastro de Informativos</strong></span>
            </div>
            <div class="col-lg-6">
                <div class="pull-right">
                    <button type="button" id="btn-formulario" data-toggle="modal"
                            data-target="#modalAdicionarInformativo" data-id-informativo="0" class="btn btn-success">
                        <span class="glyphicon glyphicon-plus"></span> Adicionar Informativo
                    </button>
                </div>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-lg-12" style="min-height: 300px">
                <?php print $listaInformativos ?>
            </div>
        </div>
    </div>
    <!--MODAL ADICIONAR NOTICIA-->
    <div class="modal fade" id="modalAdicionarInformativo">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Adicionar/Editar Informativo
                    </h4>
                </div>
                <form id="formSalvarInformativo" action="<?php echo appConf::caminho ?>comunicacao/salvarInformativo"
                      method="POST">
                    <input type="hidden" name="txtIdInformativo" value="0">
                    <input type="hidden" name="txtAtivo" value="0">
                    <div class="modal-body" id="form-colaborador">
                        <div class="row">
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="col-lg-7">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">
                                                <small>Título</small>
                                            </label>
                                            <input type="text" class="form-control input-sm" name="txtTitulo">
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">
                                                <small>Expira em</small>
                                            </label>
                                            <input type="text" class="form-control input-sm" name="txtDataFim">
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">
                                                <small>Ordem</small>
                                            </label>
                                            <input type="text" class="form-control input-sm" name="txtOrdem">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea class="ckeditor" name="txtTexto" rows="2"></textarea>
                                </div>
                            </div>
                            <div class="col-lg-3">
                                <div class="form-group">
                                    <label for="exampleInputEmail1">
                                        <small>Visível para</small>
                                    </label>
                                    <div id="linha"
                                         style="height:450px;border:1px solid #ccc; padding:5px;border-radius:3px; overflow:auto">
                                        <?php print $linhas ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                    class="glyphicon glyphicon-remove"></span> Fechar
                        </button>
                        <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span>
                            Salvar
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!--MODAL EXCLUIR INFORMATIVO-->
    <div class="modal fade" id="modalExcluirInformativo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Excluir Notícia</h4>
                </div>
                <div class="modal-body max-height">
                    <div class="row">
                        <div class="col-lg-4 text-center text-danger">
                            <img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/alert.png"/>
                        </div>
                        <div class="col-lg-8" style="height:128px;">
                            <h4 class="text-center">Deseja excluir esse informativo?</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <form id="formExcluirInformativo"
                          action="<?php echo appConf::caminho ?>comunicacao/excluirInformativo" method="POST">
                        <input type="hidden" value="" id="txtIdInformativo" name="txtIdInformativo"/>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-danger" id="btnExcluirInformativo">Excluir</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL INFORMATIVO VIEW-->
    <div class="modal fade" id="modalInformativoView">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Preview Informativo</h4>
                </div>
                <div class="modal-body max-height" id="preview-informativo">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
<?php
require_once('footerView.php');