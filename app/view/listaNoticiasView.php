
<script src="<?php echo appConf::caminho ?>plugins/ckeditor/ckeditor.js"></script>
<script>
    $(document).ready(function(e) {
        // Seleciona a noticia pra mostrar os dados
        $('#modalAdicionarNoticia').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idNoticia = button.data('id-noticia') // Extract info from data-* attributes
		
            if(idNoticia == 0) {
                limparCampos();
            } else {
                $.getJSON('<?php echo appConf::caminho ?>comunicacao/editarNoticia/'+idNoticia+'', function(noticia) {
                    popularCampos(noticia);
                });
            }
        });
        // Seleciona a noticia pra mostrar os dados
        $('#modalExcluirNoticia').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idNoticia = button.data('id-noticia') // Extract info from data-* attributes
            $('#formExcluirNoticia #txtIdNoticia').val(idNoticia);
            $('#btnExcluirNoticia').click(function(e) {
                modalAguarde('show');
            });
        });
        // Seleciona a noticia pra mostrar os dados
        $('#modalAlbumFotos').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget) // Button that triggered the modal
            var idNoticia = button.data('id-noticia') // Extract info from data-* attributes
            $('#formUploadFotoNoticia #txtIdNoticia').val(idNoticia);
            listarFotoNoticias(idNoticia);
        });
        // prepara o formulario de envio da foto da noticia
        $('#formUploadImagem').submit(function(e) {
            //modalAguarde('show');
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    //modalAguarde('hide');
                    $("input[name='txtImagem']").val(response);
                }
            });
        });
        // prepara o formulario de envio da foto da noticia
        $('#formUploadFotoNoticia').submit(function(e) {
            modalAguarde('show');
            e.preventDefault();
            var formData = new FormData($(this)[0]);
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: formData,
                async: false,
                cache: false,
                contentType: false,
                enctype: 'multipart/form-data',
                processData: false,
                success: function (response) {
                    modalAguarde('hide');
                    $('#listaFotos').html(response);
                }
            });
        });
        // botao que abre a janela de busca da imagem da noticia
        $('#btnUploadImagem').click(function(e) {
            $('#imagemExcluir').val($("input[name='txtImagem']").val());
            $('#uploadImagem').click();
            $('#uploadImagem').change(function(e) {
                $('#formUploadImagem').submit();
            });
        });
        // botao que abre a janela de busca da imagem da noticia
        $('#btnFotoNoticia').click(function(e) {
            $('#uplFotosNoticia').click();
		
            $('#uplFotosNoticia').change(function(e) {
                $('#formUploadFotoNoticia').submit();
            });
        });
    });
    function excluirFotoNoticia(idFoto) {
        $.post('<?php echo appConf::caminho ?>comunicacao/excluirFotoNoticia/'+idFoto+'', {
        }, function(retorno) {
            listarFotoNoticias(retorno);
        });
    }
    function listarFotoNoticias(idNoticia) {
        $.post('<?php echo appConf::caminho ?>comunicacao/listarFotosNoticia/'+idNoticia+'', {
        }, function(retorno) {
            $('#listaFotos').html(retorno);
		
            $('.excluirFoto').click(function(e) {
                excluirFotoNoticia($(this).attr('value'));
            });
        });
    }
    function limparCampos() {
        $("#formSalvarNoticia input[type=text], textarea").val("");
        $("#formSalvarNoticia select[name='selLocalImagem']").val(0);
        CKEDITOR.instances['txtMateria'].setData('');
    }
    function popularCampos(noticia) {
        $("input[name='txtIdNoticia']").val(noticia.idNoticia);
        $("select[name='selLocalImagem']").val(noticia.localImagem);
        $("input[name='txtData']").val(noticia.data);
        $("input[name='txtTitulo']").val(noticia.titulo);
        $("input[name='txtAutor']").val(noticia.autor);
        $("input[name='txtImagem']").val(noticia.imagem);
        $("input[name='txtAtivo']").val(noticia.ativo);
        $("textarea[name='txtResumo']").val(noticia.resumo);
        $('#linha').html(noticia.linha);
        CKEDITOR.instances['txtMateria'].setData(noticia.materia);
    }
</script>
<div class="container">
    <div class="row">
        <div class="col-lg-6">
            <span style="font-size:20px"><strong><span class="glyphicon glyphicon-file"></span> Cadastro de Notícias</strong></span>
        </div>
        <div class="col-lg-6">
            <div class="pull-right">
                <button type="button" id="btn-formulario" data-toggle="modal" data-target="#modalAdicionarNoticia" data-id-noticia="0" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Adicionar Notícia</button>          </div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-lg-12" style="min-height: 300px">
            <?php print $listaNoticias ?>
        </div>
    </div>
</div>
<!--MODAL ADICIONAR NOTICIA-->
<div class="modal fade" id="modalAdicionarNoticia" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Adicionar/Editar Notícia</h4>
            </div>
            <form id="formUploadImagem" action="<?php echo appConf::caminho ?>comunicacao/uploadImagemNoticia" method="POST" enctype="multipart/form-data">
                <input type="file" style="display:none" id="uploadImagem" name="uploadImagem"  />
                <input type="hidden" name="imagemExcluir" id="imagemExcluir" value="" />
            </form>
            <form id="formSalvarNoticia" action="<?php echo appConf::caminho ?>comunicacao/salvarNoticia" method="POST">
                <input type="hidden" name="txtIdNoticia" value="0">
                <input type="hidden" name="txtAtivo" value="0">
                <div class="modal-body" id="form-colaborador">
                    <div role="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-list"></span> Dados da Notícia</a></li>
                            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Matéria</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="home">
                                <p></p>
                                <div class="row">
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Data</small></label>
                                            <input type="text" class="form-control input-sm" id="exampleInputEmail1" name="txtData">
                                        </div>
                                    </div>
                                    <div class="col-lg-10">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Título</small></label>
                                            <input type="text" class="form-control input-sm" id="exampleInputEmail1" name="txtTitulo">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Autor</small></label>
                                            <input type="text" class="form-control input-sm" id="exampleInputEmail1" name="txtAutor">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                	
                                            <label for="exampleInputEmail1"><small>Imagem da notícia</small></label>
                                            <div class="input-group">
                                                <input type="text" class="form-control input-sm" name="txtImagem">
                                                <span class="input-group-btn">
                                                    <button class="btn btn-default btn-sm" id="btnUploadImagem" type="button">...</button>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-2">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Local da Imagem</small></label>
                                            <select class="form-control input-sm" name="selLocalImagem">
                                                <option value="1">URL</option>
                                                <option value="0">Servidor</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-9">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Resumo</small></label>
                                            <textarea class="form-control input-sm" name="txtResumo" rows="10"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-3">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1"><small>Visível Para</small></label>
                                            <div id="linha" style="border:1px solid #ccc; padding:5px;border-radius:3px; overflow:auto">
                                                <?php print $linhas ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="profile">
                                <p></p>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <textarea class="ckeditor" name="txtMateria" rows="10">teste</textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                    <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--MODAL EXCLUIR NOTICIA-->
<div class="modal fade" id="modalExcluirNoticia" >
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Excluir Notícia</h4>
            </div>
            <div class="modal-body max-height">
                <div class="row">
                    <div class="col-lg-4 text-center text-danger">
                        <img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/alert.png" />
                    </div>
                    <div class="col-lg-8" style="height:128px;">
                        <h4 class="text-center">Deseja excluir essa notícia?</h4>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <form id="formExcluirNoticia" action="<?php echo appConf::caminho ?>comunicacao/excluirNoticia" method="POST">
                    <input type="hidden" value="" id="txtIdNoticia" name="txtIdNoticia"  />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger" id="btnExcluirNoticia">Excluir</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!--MODAL ADICIONAR NOTICIA-->
<div class="modal fade" id="modalAlbumFotos" >
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><span class="glyphicon glyphicon-user"></span> Adicionar/Editar Notícia</h4>
            </div>
            <form id="formUploadFotoNoticia" action="<?php echo appConf::caminho ?>comunicacao/uploadFotoNoticia" method="POST" enctype="multipart/form-data">
                <input type="file" style="display:none" id="uplFotosNoticia" name="uplFotosNoticia[]" multiple="multiple"  />
                <input type="hidden" name="txtIdNoticia" id="txtIdNoticia" value="" />
                <div class="modal-body max-height">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <button type="button" class="btn btn-success btn-sm" id="btnFotoNoticia"><span class="glyphicon glyphicon-camera"></span>  Adicionar Fotos</button>
                            </div>
                        </div>
                    </div>
                    <hr/>
                    <div class="row" id="listaFotos">
                    </div>
                </div>
            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span>  Fechar</button>
            </div>
        </div>
    </div>
</div>
<?php
require_once ('footerView.php');