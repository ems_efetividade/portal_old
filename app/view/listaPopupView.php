<?php ?>
    <script>
        $(document).ready(function (e) {
            $('#btnAddPopup').click(function (e) {
                $('#imagemPopup').trigger('click');
            });
            $('#imagemPopup').on('change', function () {
                $('#salvarPopup').submit();
            });
            $('#modalPopupView').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idPopup = button.data('id-popup') // Extract info from data-* attributes

                $.post('<?php echo appConf::caminho ?>comunicacao/imagemPopup/' + idPopup + '', function (imagem) {
                    $('.imagemPopup').attr('src', imagem).load(function () {
                    });
                });

                $('.btnLinkPopup').unbind().click(function (e) {
                    var id = $(this).data('id-popup');
                    $.ajax({
                        type: "POST",
                        url: '<?php echo appConf::caminho ?>comunicacao/popupGetLink/' + id,
                        success: function (u) {
                            dados = $.parseJSON(u);
                            $('input[name="txtIdPopup"]').val(dados.ID_POPUP);
                            $('input[name="txtLink"]').val(dados.LINK);
                            $('#modalPopupLink').modal('show');

                            $('#btnSalvarLinkPopup').unbind().click(function (e) {
                                $('#salvarLinkPopup').submit();
                            });
                        }
                    });
                });
            });
        });
    </script>
    <style>
        #modalPopupVieww .modal-dialog {
            max-width: 100%;
            /* height: 80%; */
            padding: 0;
        }

        #modalPopupVieww .modal-content {
            max-width: 100%;
        }

        #modalPopupVieww .modal-body {
            max-width: 100%;
        }
    </style>
    <form id="salvarPopup" enctype="multipart/form-data" method="post"
          action="<?php echo appConf::caminho ?>comunicacao/salvarPopup">
        <input type="file" name="imagemPopup" id="imagemPopup" style="display:none;">
    </form>
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <span style="font-size:20px"><strong><span class="glyphicon glyphicon-comment"></span> Popups Informativos</strong></span>
            </div>
            <div class="col-lg-6">
                <div class="pull-right">
                    <button type="button" id="btnAddPopup" class="btn btn-success">
                        <span class="glyphicon glyphicon-plus"></span> Adicionar Popup
                    </button>
                </div>
            </div>
        </div>
        <p></p>
        <div class="row">
            <div class="col-lg-12" style="min-height: 300px">
                <?php print $listaPopup ?>
            </div>
        </div>
    </div>
    <!--MODAL INFORMATIVO VIEW-->
    <div class="modal fade" id="modalPopupView">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <p>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </p>
                    <img class="imagemPopup img-responsive" src=""/>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPopupLink" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Link do Popup</h4>
                </div>
                <div class="modal-body">
                    <form id="salvarLinkPopup" method="post"
                          action="<?php echo appConf::caminho ?>comunicacao/salvarLinkPopup">
                        <input type="hidden" name="txtIdPopup" value=""/>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Link</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="txtLink">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" id="btnSalvarLinkPopup">Salvar</button>
                </div>
            </div>
        </div>
    </div>
<?php
require_once('footerView.php');