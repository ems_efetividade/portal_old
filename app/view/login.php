<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Portal Efetividade - EMS</title>
    <script src="/../../plugins/jquery/jquery-1.12.1.js"></script>
    <script src="../../public/js/vue.js" type="text/javascript"></script>
    <script src="../../public/js/vue-resource.js" type="text/javascript"></script>
    <!--      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    <link href="<?php echo appConf::caminho; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <style>
        * {
            border-radius: 0px !important;
        }

        body {
            background-image: url(../public/img/back-login.jpg);
            background-size: cover;
        }

        html, body {
            height: 100%;
        }

        /*            body {
                        display: flex;
                        align-items: center;
                    }*/

        .logo-footer {
            width: 240px;
            height: 80px;
            background-position: 0 -160px;
            margin: 0px auto;
        }

        .alert {
            position: relative;
            padding: 3px;
            margin-bottom: 10px;
            border: 1px solid transparent;
        }

        .card {
            background-color: rgba(255, 255, 255, 0.1);
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s;
        }

        .fade-enter, .fade-leave-to /* .fade-leave-active em versões anteriores a 2.1.8 */
        {
            opacity: 0;
        }
        /*
                        .img {
                            background-image: url(https://www.gruponc.net.br/view/images/sprite.png);
                        }*/
    </style>
</head>
<body>
<div id="app" class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class=" col-sm-12 col-md-12 col-lg-4 col-md-offset-4  mx-auto">
            <div class="card">
                <div class="card-body">
                    <img src="/public/img/ems-logo-high.png" class="mx-auto d-block" height="150"/>
                    <p class="text-white text-center">
                        <small>Portal Corporativo</small>
                    </p>
                    <form id="formLogin" method="post" action="login/logar">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" name="txtSetor" placeholder="Setor"
                                   value="" v-model="form.txtSetor">
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control input-lg" name="txtSenha" placeholder="Senha"
                                   v-model="form.txtSenha">
                        </div>
                        <button type="button" v-on:click="save()" class="btn btn-block btn-lg btn-success">Acessar
                        </button>
                        <p></p>
                        <!--                            <h6 class="text-right "><small><a href="#" class="text-white">Esqueci minha senha!</a></small></h6>-->
                        <div style="height: 25px; margin-bottom: 15px;">
                            <div class=" text-white text-center">
                                <transition name="fade">
                                    <p v-if="erro.msg" v-bind:class="erro.class"><span style="font-size: 12px"
                                                                                       v-if="erro.msg">{{ erro.msg }}</span>
                                    </p>
                                </transition>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>
</body>
<script>
    var app = new Vue({
        el: '#app',
        data: {
            message: 'Hello Vue!',
            erro: {
                msg: '',
                class: ''
            },
            form: {
                txtSetor: '',
                txtSenha: ''
            }
        },
        methods: {
            save() {
                this.erro.class = 'bg-secondary';
                this.erro.msg = 'Aguarde...';

                var t = this;
                var data = t.form;
                var form = new FormData();

                console.log(data);


                $.each(data, function (a, b) {
                    form.append(a, b);
                });

                //console.log(form);
                this.$http.post('<?php echo appConf::caminho ?>login/login', form).then(function (a) {
                    this.erro.msg = a.body;

                    if (this.erro.msg == "") {
                        this.erro.msg = 'Sucesso! Redirecionando...';
                        window.location.href = "<?php echo appConf::caminho ?>";
                    } else {
                        this.erro.class = 'bg-danger';
                        setTimeout(() => {
                            this.erro.msg = '';
                        }, 4000);
                    }
                }, function (error) {
                    //app.errorMessage = error.body.message;
                });
            },
        }
    })
</script>
</html>