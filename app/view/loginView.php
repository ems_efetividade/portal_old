<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Portal Efetividade - Grupo NC</title>
    <link rel="shortcut icon" href="https://www.gruponc.net.br/favicon.ico" type="image/x-icon"/>
    <script src="/../../plugins/jquery/jquery-1.12.1.js"></script>
    <!--      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    <link href="<?php echo appConf::caminho; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <style>
        body {
            background-image: url(../public/img/back-login.jpg);
            background-size: cover;
        }

        html, body {
            height: 100%;
        }

        .logo-footer {
            width: 240px;
            height: 80px;
            background-position: 0 -160px;
            margin: 0px auto;
        }

        .alert {
            position: relative;
            padding: 3px;
            margin-bottom: 10px;
            border: 1px solid transparent;
        }

        .card {
            background-color: rgba(255, 255, 255, 0.1);
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s;
        }

        .fade-enter, .fade-leave-to {
            opacity: 0;
        }
    </style>
</head>
<body>


<div id="app" class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class=" col-sm-1 col-md-12 col-lg-5 col-xl-4 mx-auto">
            <div class="card">
                <div class="card-body">
                    <img src="/public/img/logo_login.png" class="mx-auto d-block mt-5 mb-4 w-100"/>
                    <p class="text-white text-center">
                        <small>Portal Efetividade</small>
                    </p>
                    <form id="formLogin" method="post" action="login/logar">
                        <div class="form-group">
                            <input type="text" class="form-control input-lg" autofocus name="txtSetor" placeholder="Usuário"
                                   value="" v-model="form.txtSetor">
                        </div>
                        <div class="form-group">
                            <input type="password"  class="form-control input-lg" name="txtSenha" placeholder="Senha"
                                   v-model="form.txtSenha">
                        </div>
                        <button type="submit" class="btn btn-block btn-lg btn-success">Acessar</button>
                        <p></p>
                        <!--                            <h6 class="text-right "><small><a href="#" class="text-white">Esqueci minha senha!</a></small></h6>-->
                        <div style="height: 25px; margin-bottom: 15px;">
                            <div id="danger" class="bg-danger text-white text-center">
                                <p>
                                    <span style="font-size: 12px" id="texto"></span>
                                </p>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script>

    $(document).ready(function (e) {
        $('#danger').hide();
    });

    $(window).load(function () {


        $("#formLogin").submit(function (event) {
            var nomeForm = '#' + $(this).attr('id');
            event.preventDefault();

            $.ajax({
                type: "POST",
                url: $(this).attr('action'),
                data: $(this).serialize(),
                success: function (retorno) {

                    if ($.trim(retorno) != '') {
                        $('#danger').hide();
                        $('#texto').html(retorno);
                        $('#danger').fadeIn();
                        //setTimeout(function () {
                            //$('#danger').fadeOut();
                       // }, 5000);
                    } else {
                        location.href = "<?php echo appConf::caminho ?>";
                    }
                }
            })

        });


    });


</script>
<!--<script type="text/javascript">(function () {-->
<!--        var ldk = document.createElement('script');-->
<!--        ldk.type = 'text/javascript';-->
<!--        ldk.async = true;-->
<!--        ldk.src = 'https://s.cliengo.com/weboptimizer/5bd85268e4b038abb07d87ce/5bd85269e4b038abb07d87d1.js';-->
<!--        var s = document.getElementsByTagName('script')[0];-->
<!--        s.parentNode.insertBefore(ldk, s);-->
<!--    })();</script>-->
</html>