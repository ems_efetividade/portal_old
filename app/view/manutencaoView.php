<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Portal Corporativo - Grupo NC</title>
    <link rel="shortcut icon" href="https://www.gruponc.net.br/favicon.ico" type="image/x-icon"/>
    <!--      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">-->
    <link href="<?php echo appConf::caminho; ?>plugins/bootstrap/bootstrap.css" rel="stylesheet" type="text/css"/>
    <style>
        body {
            background-image: url(../public/img/back-login.jpg);
            background-size: cover;
        }

        html, body {
            height: 100%;
        }

        .logo-footer {
            width: 240px;
            height: 80px;
            background-position: 0 -160px;
            margin: 0px auto;
        }

        .alert {
            position: relative;
            padding: 3px;
            margin-bottom: 10px;
            border: 1px solid transparent;
        }

        .card {
            background-color: rgba(255, 255, 255, 0.1);
        }

        .fade-enter-active, .fade-leave-active {
            transition: opacity .5s;
        }

        .fade-enter, .fade-leave-to {
            opacity: 0;
        }
    </style>
</head>
<body>

<div id="app" class="container d-flex h-100">
    <div class="row align-self-center w-100">
        <div class=" col-sm-1 col-md-12 col-lg-5 col-xl-4 mx-auto">
            <div class="card">
                <div class="card-body">
                    <img src="/public/img/logo_login.png" class="mx-auto d-block mt-5 mb-4 w-100"/>
                    <p class="text-white text-center">
                        <small>Portal Efetividade</small>
                    </p>
                    <img src="/public/img/icon-manutencao.png" height="100" class="mx-auto d-block"/>
                    <h1><span class="glyphicon glyphicon-alert"></span></h1>
                    <h2 class="text-center text-white">Estamos em manutenção!</h2>
                    <br>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>