<?php
require_once ('/../model/colaboradorModel.php');
$meusDados = new colaboradorModel();
$meusDados->setId($_SESSION['id_colaborador']);
$meusDados->selecionar();
?>
    <script src="<?php echo appConf::caminho ?>plugins/cropfoto/cropbox.js"></script>
    <script>
        $(document).ready(function (e) {
            $('#btnClick').click(function (e) {
                $('#file').trigger('click');
            });
            $('#btnTrocarSenha').click(function (e) {
                $.post('<?php echo appConf::caminho ?>comunicacao/trocarSenha', {
                    txtSenhaAntiga: $('#txtSenhaAntiga').val(),
                    txtSenhaNova: $('#txtSenhaNova').val(),
                }, function (retorno) {
                    if (retorno != "") {
                        msgBox(retorno);
                    } else {
                        $('#modalAlterarSenha').modal('hide');
                        $('#modalAlterarSenhaSucesso').modal('show');
                    }
                });
            });
        });
        $(window).load(function () {
            var options =
                {
                    thumbBox: '.thumbBox',
                    spinner: '.spinner',
                    imgSrc: 'avatar.png'
                }
            var cropper = $('.imageBox').cropbox(options);
            $('#file').on('change', function () {
                var reader = new FileReader();
                reader.onload = function (e) {
                    options.imgSrc = e.target.result;
                    cropper = $('.imageBox').cropbox(options);
                }
                reader.readAsDataURL(this.files[0]);
                this.files = [];
            })
            $('#btnCrop').on('click', function () {
                var img = cropper.getDataURL();
                $.post('<?php echo appConf::caminho ?>comunicacao/salvarFotoPerfil', {
                    imageData: img
                }, function (data) {
                    //alert(data);
                    $('#modalFoto').modal('hide');
                    window.location.href = window.location.href = $(location).attr('href');
                });
            })
            $('#btnZoomIn').on('click', function () {
                cropper.zoomIn();
            })
            $('#btnZoomOut').on('click', function () {
                cropper.zoomOut();
            })
        });
    </script>
    <style>
        .modal-header {
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .container {
            width: 100%
        }

        .foto-usuario {
            text-align: center;
            border: 1px solid #ccc;
            margin-bottom: 10px;
        }

    </style>
    <link rel="stylesheet" href="<?php echo appConf::caminho ?>plugins/cropfoto/example/style.css" type="text/css"/>
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-sm-2">
                <label for="btnTrocarFoto" style="cursor: pointer" class="foto-usuario">
                    <img src="<?php echo appFunction::fotoColaborador($meusDados->getId()) ?>" class="img-responsive"/>
                </label>
                <button id="btnTrocarFoto" type="button" style="width:100%" class="btn btn-success" data-toggle="modal"
                        data-target="#modalFoto"><span class="glyphicon glyphicon-camera"></span> Alterar Foto
                </button>
                <p></p>
<!--                <button type="button" style="width:100%" class="pull-right btn btn-warning" data-toggle="modal"-->
<!--                        data-target="#modalAlterarSenha"><span class="glyphicon glyphicon-lock"></span> Alterar Minha-->
<!--                    Senha-->
<!--                </button>-->
                <br><br>
            </div>
            <div class="col-lg-10 col-sm-10">
                <div class="row">
                    <div class="col-lg-12">
                        <h3 class="wel" style="margin-top:0px;"><strong><?php echo $meusDados->getNome(); ?></strong>
                        </h3>
                        <hr/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <small>
                            <p><strong>ID:</strong> <?php echo $meusDados->getIdColaborador(); ?></p>
                            <p><strong>Maticula:</strong> <?php echo $meusDados->getMatricula(); ?></p>
                            <p><strong>CPF:</strong> <?php echo $meusDados->getCPF(); ?></p>
                            <p><strong>RG:</strong> <?php echo $meusDados->getRG(); ?></p>
                        </small>
                    </div>
                    <div class="col-lg-6">
                        <small>
                            <p>
                                <strong>Nascimento:</strong> <?php echo appFunction::formatarData($meusDados->getDataNasc()); ?>
                            </p>
                            <p><strong>Estado Civil:</strong> <?php echo $meusDados->getEstadoCivil(); ?></p>
                            <p><strong>Camiseta:</strong> <?php echo $meusDados->getCamiseta(); ?></p>
                            <p><strong>Sapato:</strong> <?php echo $meusDados->getSapato(); ?></p>
                            <p><strong>Fumante:</strong> <?php echo $meusDados->getFumante(); ?></p>
                        </small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12"><h4><strong>Endereço Residencial / Contatos</strong></h4>
                        <hr/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <small>
                            <p><strong>Endereço:</strong> <?php echo $meusDados->getEndereco(); ?>
                                , <?php echo $meusDados->getNumero(); ?></p>
                            <p><strong>Complemento:</strong> <?php echo $meusDados->getComplemento(); ?></p>
                            <p><strong>Bairro:</strong> <?php echo $meusDados->getBairro(); ?></p>
                            <p><strong>Cidade:</strong> <?php echo $meusDados->getCidade(); ?></p>
                            <p><strong>Cep:</strong> <?php echo $meusDados->getCep(); ?></p>
                        </small>
                    </div>
                    <div class="col-lg-6">
                        <small>
                            <p><strong>Email:</strong> <?php echo $meusDados->getEmail(); ?></p>
                            <p><strong>Tel. Comercial:</strong> (<?php echo $meusDados->getDDDCelCorp(); ?>
                                ) <?php echo $meusDados->getCelCorp(); ?></p>
                            <p><strong>Tel. Particular:</strong> (<?php echo $meusDados->getDDDCelPess(); ?>
                                ) <?php echo $meusDados->getCelPess(); ?></p>
                        </small>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-xs-6"><h4><strong>Informações do Setor / Ipad</strong></h4>
                        <hr/>
                    </div>
                    <div class="col-lg-6 col-xs-6"><h4><strong>Endereço de Amostra</strong></h4>
                        <hr/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 col-xs-6">
                        <small>
                            <p>
                                <strong>Setor:</strong> <?php echo $meusDados->getSetor() . ' ' . $meusDados->getNomeSetor(); ?>
                            </p>
                            <p><strong>Linha:</strong> <?php echo $meusDados->getLinha() ?></p>
                            <p><strong>Inicio:</strong> <?php echo $meusDados->getDataInicio() ?></p>
                            <p><strong>Função:</strong> <?php echo $meusDados->getFuncaoSetor() ?></p>

                            <br/>

                            <p><strong>Modelo Ipad:</strong> <?php echo $meusDados->getModeloIpad(); ?></p>
                            <p><strong>Conexão:</strong> <?php echo $meusDados->getConexao(); ?></p>
                            <p><strong>Registro:</strong> <?php echo $meusDados->getRegistro(); ?></p>
                            <p><strong>Ativo:</strong> <?php echo $meusDados->getAtivo(); ?></p>
                        </small>
                    </div>
                    <div class="col-lg-6 col-xs-6">
                        <small>
                            <p><strong>Endereço:</strong> <?php echo $meusDados->getEnderecoA(); ?>
                                , <?php echo $meusDados->getNumeroA(); ?></p>
                            <p><strong>Complemento:</strong> <?php echo $meusDados->getComplementoA(); ?></p>
                            <p><strong>Bairro:</strong> <?php echo $meusDados->getBairroA(); ?></p>
                            <p><strong>Cidade:</strong> <?php echo $meusDados->getCidadeA(); ?></p>
                            <p><strong>Cep:</strong> <?php echo $meusDados->getCepA(); ?></p>
                        </small>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL foto -->
    <div class="modal fade" id="modalFoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header btn-primary">

                    <h5>Foto do Perfil</h5>
                </div>
                <div class="modal-body">
                    <h5>Clique em 'Procurar Foto', ajuste a foto e clique em 'Salvar'</h5>
                    <div class="containerr">
                        <div class="imageBox">
                            <div class="thumbBox"></div>
                            <div class="spinner" style="display: none"></div>
                        </div>
                        <P></P>
                        <div class="action">
                            <button type="button" class="btn btn-default btn-sm" id="btnClick" value="Crop"><span
                                        class="glyphicon glyphicon-picture"></span> Procurar Foto
                            </button>

                            <button type="button" class="btn btn-default btn-sm" id="btnZoomIn" value="+"><span
                                        class="glyphicon glyphicon-zoom-in"></span> Aumentar zoom
                            </button>
                            <button type="button" class="btn btn-default btn-sm" id="btnZoomOut" value="-"><span
                                        class="glyphicon glyphicon-zoom-out"></span> Diminuir zoom
                            </button>
                            <input type="file" id="file" style="display:none;">
                        </div>
                        <div class="cropped">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                    <button type="button" class="btn btn-primary btn-md" id="btnCrop"><span
                                class="glyphicon glyphicon-ok"></span> Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalAlterarSenha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-lock"></span> Alterar
                        Senha</h4>
                </div>
                <div class="modal-body max-height">
                    <form role="form">
                        <div class="form-group">
                            <label for="exampleInputPassword1">Senha Atual</label>
                            <input type="password" class="form-control" id="txtSenhaAntiga">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Nova Senha</label>
                            <input type="password" class="form-control" id="txtSenhaNova">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                    <button id="btnTrocarSenha" type="button" class="btn btn-primary primary" dataa-dismiss="modal">
                        <span class="glyphicon glyphicon-ok"></span> Alterar Senha
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPoliticas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-primary">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-file"></span>
                        Documentos/Políticas</h4>
                </div>
                <div class="modal-body max-height">
                    <form>
                        <div class="form-group">
                            <!--                      <label for="exampleInputEmail1"></label>-->
                            <div id="comboPolitica"></div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!--MODAL SUCESSO ALTERACAO-->
    <div class="modal fade" id="modalAlterarSenhaSucesso" tabindex="-1" role="dialog"
         aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header btn-success">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-lock"></span> Alterar
                        Senha</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-4 text-center">
                            <img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/success.png"/>
                        </div>
                        <div class="col-lg-8" style="height:128px;">
                            <h4 class="text-center">Alteração realizada com sucesso! Utilize a nova senha em sem próximo
                                acesso!</h4>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><span
                                class="glyphicon glyphicon-remove"></span> Fechar
                    </button>
                </div>
            </div>
        </div>
    </div>

<?php include('footerView.php') ?>