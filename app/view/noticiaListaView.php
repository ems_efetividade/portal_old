<?php include('headerView.php') ?>
<style>

body {
	padding-top: 50px;			
}

.container, .enquete {
	font-family: "praxis_lt_light";	
	font-size:18px;
}

p {
	line-height:35px;
	padding-bottom:5px;
}

.page-header {
	margin-top:0px;	
}

.img-thumbnail {
	---margin-top:5px;	
}

.selected img {
	opacity:0.5;
}

#carousel-example-generic2 .carousel-control {
	width:7%;	
}


.top-border {
	border-top:2px solid #324b80;	
}
</style>
<!-- espaçamento superior -->
<div style="margin-top:20px;"></div>

<div class="container">

    <div class="panel panel-default top-border">
    	<div class="panel-body">
        
    		<div class="row">
        		<div class="col-lg-12">
            		<h4 class="page-header">Lista de Noticias</h4>
        		</div>
   			 </div>
    
    		<div class="row">
    
    			<div class="col-lg-12"> <a href="<?php echo appConf::caminho ?>" class="btn btn-primary" role="button">
                        	<span class="glyphicon glyphicon-home"></span> Voltar para o Home
                        </a><br /><br /></div>
        		<div class="col-lg-12" style="margin-bottom:30px;">
                
        				<?php echo $listaNoticia; ?>
                        
                        
                        <div class="col-lg-12"> <a href="<?php echo appConf::caminho ?>" class="btn btn-primary" role="button">
                        	<span class="glyphicon glyphicon-home"></span> Voltar para o Home
                        </a><br /><br /></div>

    			</div>
    		</div>
    
    	</div>
    
    </div>

</div>