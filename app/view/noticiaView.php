<?php include('headerView.php') ?>
<style>

body {
	padding-top: 50px;			
}

.container, .enquete {
	font-family: "praxis_lt_light";	
	font-size:18px;
}

p {
	line-height:35px;
	padding-bottom:5px;
}

.page-header {
	margin-top:0px;	
}

.img-thumbnail {
	---margin-top:5px;	
}

.selected img {
	opacity:0.5;
}

#carousel-example-generic2 .carousel-control {
	width:7%;	
}


.top-border {
	border-top:2px solid #324b80;	
}
</style>
<!-- espaçamento superior -->
<div style="margin-top:20px;"></div>

<div class="container">

    <div class="panel panel-default top-border">
    	<div class="panel-body">
        
    		<div class="row">
        		<div class="col-lg-12">
            		<h4 class="page-header">Artigos</h4>
        		</div>
   			 </div>
    
    		<div class="row">

        		<div class="col-lg-8" style="margin-bottom:30px;">
        
                    <h5>
                        <a href="<?php echo appConf::caminho ?>" class="btn btn-primary" role="button">
                        	<span class="glyphicon glyphicon-home"></span> Voltar para o Home
                        </a>
                    </h5>
        
                    <h1 class="text-justify"><strong><?php echo $tituloArtigo ?></strong></h1>
                    <h1 class="text-justify"><small><p><?php echo $resumoArtigo ?></p></small></h1>
                    <h4><small>Por: <?php echo $escritorArtigo ?> em <?php echo $dataArtigo; ?>&nbsp;&nbsp;
                        <span class="glyphicon glyphicon-eye-open"></span> <?php echo $visualizacaoArtigo ?></small></h4>
                    <h4></h4>
            
            
            
                    <h2 class="text-justify">
                       <?php echo $imgArtigo ?>
                    <small><p><?php echo $textoArtigo ?></p></small></h2>
                    
                    <div id="topo-album">
                    	<div id="album-foto" name="album-foto">
                        	<?php echo $fotosArtigo; ?>
                    	</div>
                    </div>
    

         <hr />
            		<a href="<?php print appConf::caminho ?>" class="btn btn-primary" role="button">
                    <span class="glyphicon glyphicon-home"></span> Voltar para o Home</a>
        
        		</div>
                
        		<div class="col-lg-4">
            		<h3><strong>Artigos mais lidos</strong></h3>
            		<?php echo $listaArtigos; ?>
        			                 <a href="<?php print appConf::caminho ?>noticia/" class="pull-right btn btn-primary" id="lista-artigo"><strong><span class="glyphicon glyphicon-plus"></span> Mais Artigos</strong></a>
        		</div>
    
    		</div>
    
    	</div>
    
    </div>

</div>


<?php include('footerView.php') ?>