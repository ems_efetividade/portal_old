<?php include('app/view/headerView.php'); ?>

<script src="<?php echo appConf::caminho ?>public/js/painel.js"></script>

<script src="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link href="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<script>

    $('[data-toggle="popover"]').popover();

    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
    }   

    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }
    
    
    $(window).ready(function(a){
        resizePanelLeft();
    });
    
    
</script>

<style>

</style>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/painel/admin/admin-menu.php'); ?>

        </div>
        
        <div class="col-lg-10 panel-right">

            
            <div class="nav-menu-right">
                <?php require_once('app/view/painel/nav.php'); ?>
            </div>
            
            <div class="padding">
                
                <div class="panel panel-default">
                    <div class="panel-body">
                <h4><b>Carga de Dados</b></h4>
                <br>
                
                
                
                <form method="post" action="<?php echo appConf:: caminho ?>painel/uploadcarga" enctype="multipart/form-data">

                  <input type="file" name="files[]" multiple>

                    <button type="submit" class="btn btn-success btn-sm">Adicionar Usuario</button>

               </form>
                
                <br>
                
                
                <form  method="post" action="<?php echo appConf::caminho ?>painel/excluirArquivo">
                <button type="submit" class="btn btn-danger btn-sm">Excluir</button>
                <table class="table table-condensed table-striped">
                    <tr>
                        <th><input type="checkbox" id="check-all" /> Arquivo</th>
                        <th>Data</th>
                        <th>Tamanho</th>
                    </tr>
                    <?php if($listaArquivos) { ?>
                    <?php foreach($listaArquivos as $arq) { ?>
                    <tr>
                        <td><input type="checkbox" name="chk[]" class="chk-excluir" value="<?php echo $arq['arquivo'] ?>" /> <?php echo $arq['arquivo'] ?></td>
                        <td><?php echo $arq['data'] ?></td>
                        <td><?php echo $arq['tamanho'] ?></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </table>
                </form>
                
                <button type="button" class="btn btn-primary btn-sm">Atualizar</button>
                
                </div>
                </div>
                
            </div>
            
            
       
        </div>
    </div>
</div>


<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="btn-permissao">Save changes</button>
      </div>
    </div>
  </div>
</div>

<?php include('app/view/footerView.php') ?>


<script>
    
$('#check-all').unbind().click(function(a){
    $('.chk-excluir').prop('checked', $(this).prop('checked'));
});

</script>
