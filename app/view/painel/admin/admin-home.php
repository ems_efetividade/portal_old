<?php
include('app/view/headerView.php');



?>



<script src="<?php echo appConf::caminho ?>public/js/painel.js"></script>


<script src="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link href="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<script>

    $('[data-toggle="popover"]').popover();

    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
    }   

    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }
    
    
    $(window).ready(function(a){
        resizePanelLeft();
    });
    
    
</script>

<style>

</style>

<div class="container-fluid">
    
    <div class="row">
        
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/painel/admin/admin-menu.php'); ?>
        </div>
        
        <div class="col-lg-10 panel-right">

            <div class="nav-menu-right">
                <?php require_once('app/view/painel/nav.php'); ?>
            </div>
            
            <div class="padding"></div>
            
        </div>
        
    </div>
</div>




<?php include('app/view/footerView.php') ?>

