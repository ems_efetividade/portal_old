<?php require_once('app/view/painel/functions.php'); ?>

<link href="<?php echo appConf::caminho ?>app/view/painel/css/painel.css" rel="stylesheet" type="text/css"/>

<style>
    .main-menu {
        margin: 0 -15px;
    }

    .main-menu li {
        --text-align: center;
    }

    .main-menu a {
        color: #4f5b5e;
        text-decoration: none;
    }
    .main-menu a:hover {
        background: #bbb;
        color: #000;
    }

    .main-menu a {
        position: relative;
        display: block;
        padding: 10px 15px
    }

    .main-menu .glyphicon {
        padding-right: 13px;
    }
    
    .user-data {
        padding-bottom: 4px;
    }
</style>
<br>

<p><a class="btn btn-primary btn-block" href="<?php echo appConf::caminho ?>painel/permissao">Permissões</a></p>
<!--<p><a class="btn btn-primary btn-block" href="<?php echo appConf::caminho ?>painel/adminpilares">Pilares</a></p>-->
<!--<p><a class="btn btn-primary btn-block" href="<?php echo appConf::caminho ?>painel/carga">Carga de Dados</a></p>
<p><a class="btn btn-primary btn-block" href="#">Produtos</a></p>-->