<?php include('app/view/headerView.php'); ?>

<script src="<?php echo appConf::caminho ?>public/js/painel.js"></script>

<script src="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link href="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<script>

    $('[data-toggle="popover"]').popover();

    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
    }   

    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }
    
    
    $(window).ready(function(a){
        resizePanelLeft();
    });
    
    
</script>

<style>

</style>

<div class="container-fluid">
    
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/painel/admin/admin-menu.php'); ?>

        </div>
        
        <div class="col-lg-10 panel-right">

            
            <div class="nav-menu-right">
                <?php require_once('app/view/painel/nav.php'); ?>
            </div>
            
            <div class="padding">
                
                <div class="panel panel-default">
                    <div class="panel-body">
                <h4><b>Permissões de Acesso</b></h4>
                <br>
                
                <p><a id="" data-id="0" class="btn-add-permissao btn btn-success btn-sm" href="#">Adicionar Usuario</a></p>
                
                
                <table class="table table-striped">
                    <tr>
                        <th>Colaborador</th>
                        <th>Depto</th>
                        <th>Adicionado por:</th>
                        <th>Ação:</th>
                    </tr>
                    <?php foreach($liberados as $col) { ?>
                    <tr>
                        <td><?php echo $col['SETOR'] ?> <?php echo $col['NOME'] ?></td>
                        <td><?php echo $col['PERFIL'] ?></td>
                        <td><?php echo $col['ADM'] ?></td>
                        <td>
                            <a class="btn-add-permissao" data-id="<?php echo $col['ID_COLABORADOR'] ?>" href="#">Editar</a>
                            <a href="<?php echo appConf::caminho ?>painel/excluirPermissao/<?php echo $col['ID_COLABORADOR'] ?>">Excluir</a>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                </div>
                </div>
                
            </div>
            
            
       
        </div>
    </div>
</div>


<div class="modal fade" id="modal-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Adicionar Permissão</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary" id="btn-permissao">Salvar</button>
      </div>
    </div>
  </div>
</div>

<?php include('app/view/footerView.php') ?>


<script>
    
$(document).ready(function(e){
    $(".scroll-custom").mCustomScrollbar({
            theme:"dark"
    });
});


$('.btn-add-permissao').unbind().click(function(e){
    var id = $(this).data('id');
    $.post('<?php echo appConf::caminho ?>painel/formPermissao/'+id, function(retorno) {
         $('#modal-form .modal-body').html(retorno);       
         $('#modal-form').modal('show');
         
         $('#btn-permissao').click(function(e){
             $('form').submit();
         })
    });
});



</script>
