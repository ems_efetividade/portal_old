<?php include('app/view/headerView.php'); ?>

<script src="<?php echo appConf::caminho ?>plugins/countUp/dist/countUp.min.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link href="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo appConf::caminho ?>app/view/painel/js/functions.js" type="text/javascript"></script>

<script>
    $('[data-toggle="popover"]').popover();
    $(window).ready(function(a){
        resizePanelLeft();
        getBootstrapDeviceSize();
    });


function getBootstrapDeviceSize() {
    col =  $('#users-device-size').find('div:visible').first().attr('id');
    console.log($('#users-device-size').find('div:visible').first().attr('id'));
    return col;
}

</script>

<style>
    .affix {top: 60px;width: 19%}
    .myAffix {z-index: 1000;}
</style>
<div id="users-device-size">
  <div id="xs" col="4" class="visible-xs"></div>
  <div id="sm" col="3" class="visible-sm"></div>
  <div id="md" col="3" class="visible-md"></div>
  <div id="lg" col="2" class="visible-lg"></div>
</div>

<div class="container-fluid">

    <div class="row">
        <!-- menu esquerdo -->
           <div class="col-lg-2 panel-left hidden-xs hidden-sm hidden-md" >
            <?php require_once('app/view/painel/menu.php'); ?>
            <?php require_once('app/view/painel/filtros.php'); ?>
        </div>


        <div class="col-lg-12 panel-mobile  hidden-lg" style="">
            <?php require('app/view/painel/menu.php'); ?>

        </div>
        <!-- conteudo -->
        <div class="col-lg-10 panel-right">
            <div class="nav-menu-right">
                <!-- menu superior no painel direito -->
                <?php require_once('app/view/painel/nav.php'); ?>
            </div>

            
            <div class="padding">
                <div class="padding table-responsive">
                    <div class="row">
                        <?php for ($i = 1; $i <= 4; $i++) { ?>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 nopad">
                                <div class="panel panel-default">
                                    <div class="panel-body text-center" id="c<?php echo $i ?>" sstyle="min-height: 600px">
                                        <a href="#" class="btn-sel-colab" data-container="c<?php echo $i ?>">
                                            <img height="100" class="center-block" src="<?php echo appConf::caminho ?>public/img/img-profile.png" />
                                            <small class="text-muted">
                                                Clique aqui para selecionar o setor
                                            </small>
                                        </a>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>
<?php include('app/view/footerView.php') ?>

<!-- modal lista colaboradores -->
<div class="modal fade" id="modalEscolher" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Selecionar Setor/Colaborador</h4>
            </div>
            <div class="modal-body" style="max-height: 400px; overflow: auto">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<script>

w = $('.panel-right').width() - 40;
$('#myAffix').css('width', w);
selColaborador();

function selColaborador() {
    $('.btn-sel-colab').unbind().click(function(e){
    var container = $(this).data('container');

    $.post('<?php echo appConf::caminho ?>painel/listaSetor', function(retorno) {
        $('#modalEscolher .modal-body').html("");
        $('#modalEscolher .modal-body').html(retorno);
        $('#modalEscolher').modal('show');
        
        $('.btn-sel').unbind().click(function(e){
            var setor = $(this).data('val');
            $('#modalEscolher').modal('hide');
            $.post('<?php echo appConf::caminho ?>painel/selComparar/'+setor+'/'+container, function(retorno) {
                $('#' + container).hide().html(retorno).fadeIn();
                
                $('.myAffix').affix({
                    offset: {
                      top:100,
                      bottom: function () {
                        return (this.bottom = $('.jumbotron').outerHeight(true))
                      }
                    }
                });
                resizePanelLeft();
                selColaborador();
            });
        });
    });
});
}

$(window).scroll(function() {

    if ($(this).scrollTop()>200) {
         $('#myAfsfix').fadeIn();
    } else {
        $('#myAffsix').fadeOut();
    }
});

</script>
