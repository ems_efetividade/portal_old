<?php require_once('functions.php'); ?>
<style>
    body {
        font-size:12px;
    }
    .strip {
        background-color: #f9f9f9;
        
    }
    .text-muted {
        color: #666;
    }
    table {border-collapse: collapse;}
    
    .table-data {
        width: 100%;
    }
    .table-data td {
        padding:5px; margin:0px; border-top: 1px solid #ddd;
    }
    
    .img-logo {
        margin-right: 10px;
    }
    
    .well {
        border:1px solid #ddd;
        background-color: #f9f9f9;
        padding:10px;
        margin-bottom:20px;
    }
    
</style>

<!--<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />-->
<table width="100%">
    <tr>
        <td><img class="img-logo" src="public/img/logoems.jpg" height="55" /></td>
        <td align="right">
            <h2><b> Relatório de Ranking de Setores</b></h2>
            <div class="text-muted">
<!--                <small><b>Filtros: </b><?php echo $criterio['PILAR'].' / '.$criterio['PRODUTO']  ?></small>-->
                <small>Painel EMS Prescrição</small>
            </div>
        </td>
    </tr>
</table>

<hr>


<div class="well well-sm">
    <b>Critérios utilizados</b>
    </div>
<table>
    <tr>
        <td><b><small>Linha: </small></b></td>
        <td><small><?php echo $filtros[1] ?></small></td>
    </tr>
    
    <tr>
        <td><b><small>Função: </small></b></td>
        <td><small><?php echo $filtros[0] ?></small></td>
    </tr>
    
    <tr>
        <td><b><small>Regional: </small></b></td>
        <td><small><?php echo $filtros[2] ?></small></td>
    </tr>
    
    <tr>
        <td><b><small>Pilar: </small></b></td>
        <td><small><?php echo $filtros[3] ?></small></td>
    </tr>
    
    <tr>
        <td><b><small>Produto: </small></b></td>
        <td><small><?php echo $filtros[4] ?></small></td>
    </tr>
    
    <tr>
        <td><b><small>Período: </small></b></td>
        <td><small><?php echo $filtros[5] ?></small></td>
    </tr>
</table>
<br>

<div class="well well-sm">
    <b>Ranking</b>
</div>

<table class="table-data" >
    
    <?php foreach($dadoss as $i => $dado) {  ?>
    
        <tr>
            
            <td align="center" class="<?php echo (($i%2)) ? 'strip' : '' ; ?>"><h4><b><?php echo $i ?>º</b></h4></td> 
            <td class="<?php echo (($i%2)) ? 'strip' : '' ; ?>"><img style="border-radius:10px !important" src="<?php echo str_replace(appConf::caminho, "", fotoPerfil($dado['FOTO'])) ?>" height="30" /></td>
            <td class="<?php echo (($i%2)) ? 'strip' : '' ; ?>">
                <div><small><?php echo $dado['SETOR'] ?> <?php echo $dado['NOME'] ?></small></div>
                
                <div class="text-muted"><small><small><?php echo $dado['PERFIL'] ?> <?php echo $dado['LINHA'] ?></small></small></div>
                <div class="text-muted"><small><small><?php echo $dado['NOME_SETOR'] ?></small></small></div>
            </td> 
            <td align="center" class="<?php echo (($i%2)) ? 'strip' : '' ; ?>">
                <div class="text-muted" style="font-size:10px;">
                <div><?php echo $filtros[2] ?></div>
                <div><?php echo $filtros[3] ?></div>
                <div><?php echo $filtros[4] ?></div>
                </div>
            </td>
            <td class="<?php echo (($i%2)) ? 'strip' : '' ; ?>">
                
                    <h4><b><?php echo appFunction::formatarMoeda($dado['VALOR'], $dado['DECIMAL']) ?><?php echo $dado['SUFIXO'] ?></b></h4>
            </td> 
            
        </tr> 
    <?php } ?>
    
</table>

<?php 

//print_r($dados);

?>