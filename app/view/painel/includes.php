
<script type="text/javascript" src="https://canvg.github.io/canvg/rgbcolor.js"></script> 
<script type="text/javascript" src="https://canvg.github.io/canvg/StackBlur.js"></script>
<script type="text/javascript" src="https://canvg.github.io/canvg/canvg.js"></script> 

<script src="<?php echo appConf::caminho ?>public/js/painel.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/solid-gauge.js"></script>

<script src="<?php echo appConf::caminho ?>plugins/countUp/dist/countUp.min.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/gauge.js/dist/gauge.js" type="text/javascript"></script>
<script src="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link   href="<?php echo appConf::caminho ?>plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>
