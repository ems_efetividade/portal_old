function pesquisarTabela(valor, tabela) {
    var value = valor.toLowerCase().trim().replace(/\s+/g, '');
    /*
    $("#" + tabela + " tbody tr").each(function (index) {
        //if (!index) return;
        $(this).find("td").each(function () {
            var id = $(this).text().toLowerCase().trim();
            var not_found = (id.indexOf(value) == -1);
            $(this).closest('tr').toggle(!not_found);
            return not_found;            
        });
    }); */
    
    
    $("#" + tabela + " tbody tr").each(function (index, v) {
        
        //if (!index) return;
        
        //var id = $(this).data('search');
        
        //cnso
        
        $(this).find("td").each(function () {
            var data = $(this).data('search');
            if(data !== undefined) {
           
            
            var id = $(this).data('search').toLowerCase().trim().replace(/\s+/g, '');
            
            //console.log('data: '+data+ ' id: '+id);
            var not_found = (id.indexOf(value) == -1);
            $(this).closest('tr').toggle(!not_found);
            return not_found;            
            }
        });
    }); 
    
   /*
    var elemens = $("#" + tabela + " tbody tr");
    searchInput = $("#search");
    
   elemens.each(function(){
            
            var re = new RegExp(searchInput.val(), 'gi');
            
            ///alert(re);
            
            if( $(this).attr('data-search').match(re) === null )
            {
                $(this).hide();
            }else{
                $(this).show();
            }
        
        });  */
}

function resizePanelLeft() {
    var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
    $('.panel-left').css('height', (screenSize));
    
    a = $('#container-colab').height();
    $('#container-setores').css('height', (screenSize -  a)-100);
    $('#final').css('height', (screenSize)-100);
}   

function aguarde(acao) {
    $('#modalAguarde').modal(acao);
}

function startCount(container) {

    $(container).each(function( key, item ) {
       
        var options = {
            useEasing:   true,
            useGrouping: (!$(this).data('group')) ? true : $(this).data('group'), 
            separator:   $(this).data('separator'), 
            decimal:     $(this).data('decimal'),
            suffix:      $(this).data('suffix')
        };

       countUp = new CountUp(
               $(this).attr('id'),
               $(this).data('start'),
               $(this).data('end'),
               $(this).data('num-decimal'),
               $(this).data('duration'), options
            );
       countUp.start();

    });
    
}