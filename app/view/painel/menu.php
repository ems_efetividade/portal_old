<?php require_once('functions.php'); ?>
<link href="<?php echo appConf::caminho ?>app/view/painel/css/painel.css" rel="stylesheet" type="text/css"/>


<style>
    .main-menu {
        margin: 0 -15px;
    }

    .main-menu li {
        --text-align: center;
    }

    .main-menu a {
        color: #4f5b5e;
        text-decoration: none;
    }
    .main-menu a:hover {
        background: #bbb;
        color: #000;
    }

    .main-menu a {
        position: relative;
        display: block;
        padding: 10px 15px
    }

    .main-menu .glyphicon {
        padding-right: 13px;
    }
    
    .user-data {
        padding-bottom: 4px;
    }
</style>

<br>
<!--<img class="center-block" src="public/img/logo_ems2.png" alt=""/>
<h4 class="text-center">
    <small><?php echo APP_NAME ?></small>
</h4>-->
<?php //if(appFunction::dadoSessao('perfil') <> 0) { ?>


<div id="container-colab">
    <?php if($colab[1]['LINHA'] != "") { ?>

    <div class="row">


        <div class="col-lg-12 col-sm-3 col-xs-3">
            <div class="paneal panael-default">
                <div class="apanel-body">
                    <img height="100" wsidth="100" class="center-block hidden-lg hidden-md hidden-sm" style="border-radius: 10px !important" src="<?php echo fotoPerfil($colab[1]['FOTO']) ?>" />
                    <img height="130" wsidth="100" class="center-block hidden-xs" style="border-radius: 10px !important" src="<?php echo fotoPerfil($colab[1]['FOTO']) ?>" />
                    
                </div>
                <br>
            </div>
            
        </div>

        <div class="col-lg-12 col-sm-9 col-xs-9">
            <div class="text-censter" style="font-size:11px">
                <div class="user-data "><b><?php echo $colab[1]['COLABORADOR'] ?></b></div>
                <div class="user-data"><small><?php echo $colab[1]['FUNCAO'] ?> (<?php echo $colab[1]['LINHA'] ?>)</small></div>
                <div class="user-data"><small><?php echo $colab[1]['NOME_SETOR'] ?></small></div>
                <div class="user-data"><small>Data Início: <?php echo appFunction::formatarData($colab[1]['DT_INICIO']) ?></small></div>
                <?php if($colab[1]['GD'] != "") { ?>
                <div class="user-data"><small><?php echo shortName($colab[1]['GD']) ?></small></div>
                <?php } ?>
                <?php //if($colab[1]['GR'] != "") { ?>
        <!--        <div class="user-data"><small><?php echo shortName($colab[1]['GR']) ?></small></div>-->
                <?php //} ?>
                <?php if($colab[1]['GN'] != "") { ?>
                <div class="user-data"><small><?php echo shortName($colab[1]['GN']) ?></small></div>
                <?php } ?>

                    
                    
                    

                <?php if($setores) { ?>

                <div class="hidden-lg " >
                <!-- <input type="text" id="search-mobile" onKeyUp="filtrarTabela(this.value, 'tb-search-mobile')"  class="form-control input-sm" placeholder="Pesquisar (<?php echo count($setores) ?> setor(es))" autocomplete="off">
                    <P></p> -->

                    <div id="div-mobile" style="max-height:100px; overflow:auto; position:relative; z-index:1;">
                        <select name="cmb-setor" id="inputcmb-setor" class="form-control input-xs" onChange="abrirPainelSelect(this.value)" required="required">
                            <?php foreach($setores as $setor) { ?>
                                <option value="<?php echo base64_encode($setor['SETOR']) ?>" <?php echo (($colab[1]['COLABORADOR'] == $setor['SETOR'].' '.$setor['NOME']) ? 'selected' : '') ?>><?php echo $setor['SETOR'].' '.shortName1($setor['NOME']) ?> </option>
                            <?php } ?>
                        </select>
                        <p></p>
                    </div>
                </div>

                <?php } ?>
                <a href="<?php echo  appConf::caminho  ?>/painel" class="btn btn-primary btn-xs btn-block"><small>Meu Painel</small></a>

            </div>
            <br>
        </div>
    </div>

    


    
    <?php } ?>

</div>

<?php if($setores) { ?>
    

    <?php if (count($setores) > 1) { ?>
        <div class="hidden-xs hidden-md hidden-sm">
            <input type="text" id="search" onKeyUp="filtrarTabela(this.value, 'tb-search')" class="form-control input-sm" placeholder="Pesquisar (<?php echo count($setores) ?> setor(es))" autocomplete="off">
            <P></p>
            
            <div id="container-setores">
            <table id="tb-search" class="table-condensed" style="font-size: 11px">
                <?php foreach($setores as $setor) { ?>

            
                
                <tr class="hidden-xs ">
                    <td data-search="<?php echo $setor['SETOR'].shortName1($setor['NOME'].$setor['LINHA'].$setor['REGIONAL']) ?>">
                        <img  height="20"  class="ismg-responsive" style="border-radius: 10px !important" src="<?php echo fotoPerfil($setor['FOTO']) ?>" />
                    </td>
                    <td> 

        <!--            <a href="<?php echo ($setor['VAGO'] == 1) ? '#' :  appConf::caminho ?>painel/abrirPainel/<?php echo $setor['SETOR'] ?>" style="color:#fff;">-->
                    <a href="<?php echo  appConf::caminho ?>painel/abrirPainel/<?php echo base64_encode($setor['SETOR']) ?>" style="color:#fff;">
                    <!-- <a href="#" style="color:#fff;" onClick="abrirPainel('<?php echo base64_encode($setor['SETOR']) ?>')"> -->
                        <?php echo $setor['SETOR'].' '.shortName1($setor['NOME']) ?> 
                    </a>
                    </td>
                </tr>
                <?php } ?>
            </table>
            </div>
            <?php } ?>
        </div>


        <div class="hidden-lg " >
           <!-- <input type="text" id="search-mobile" onKeyUp="filtrarTabela(this.value, 'tb-search-mobile')"  class="form-control input-sm" placeholder="Pesquisar (<?php echo count($setores) ?> setor(es))" autocomplete="off">
            <P></p> -->

            <!-- <div id="div-mobile" style="max-height:100px; overflow:auto; position:relative; z-index:1;">
                <select name="cmb-setor" id="inputcmb-setor" class="form-control" onChange="abrirPainelSelect(this.value)" required="required">
                    <?php foreach($setores as $setor) { ?>
                        <option value="<?php echo base64_encode($setor['SETOR']) ?>" <?php echo (($colab[1]['COLABORADOR'] == $setor['SETOR'].' '.$setor['NOME']) ? 'selected' : '') ?>><?php echo $setor['SETOR'].' '.shortName1($setor['NOME']) ?> </option>
                    <?php } ?>
                </select>
                <p></p>
            </div> -->
        </div>


<?php } ?>



<script src="<?php echo appConf::caminho ?>app/view/painel/js/functions.js" type="text/javascript"></script>
<script>
    //$("#search").keyup(function () {
       // var valor = $(this).val();
        //pesquisarTabela(valor, 'tb-search');
   // });

   // $("#search-mobile").keyup(function () {
    //    alert('asdfsad');
        //var valor = $(this).val();
        //pesquisarTabela(valor, 'tb-search-mobile');
    //});

    var to;


function abrirPainelSelect(setor) {
   location.href= "<?php echo  appConf::caminho ?>painel/abrirPainel/" + setor;
}


function mostrar() {
    document.getElementById('div-mobile').style.display = 'block';
}


function abrirPainel(setor) {
    $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>painel/abrir',
            data: {
                setor: setor,
            },
            beforeSend: function (xhr) {
                 
            },
            success: function(retorno) {
            $('#centro-painel').html('');
               $('#centro-painel').html(retorno);

            }
        });
}

function filtrarTabela(valor, tabela) {
    
    clearTimeout(to);
       to = setTimeout(function(){
            pesquisarTabela(valor, tabela);
            mostrar();
       }, 170);
    //pesquisarTabela(valor, );
}    
</script>

<script>
(function($){
    $(document).ready(function(e) {

        document.getElementById('div-mobile').style.display = 'none';
        console.log(document.getElementById('div-mobile').style.display);


        $("#container-setores").mCustomScrollbar({
            theme:"minimal"
        });
    });
})(jQuery);
</script>