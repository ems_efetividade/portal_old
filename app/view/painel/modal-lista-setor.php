<?php require_once('functions.php'); ?>
<input id="search-col" type="text" class="form-control" id="inputEmail3" placeholder="Digite o Nome ou o Setor para pesquisar...">
<br>
<table id="tb-search-col" class="table table-condesnsed table-striped" style="font-sisze:12px;">
    <?php foreach($setores as $setor) { ?>
    <?php if($setor['VAGO'] == 0) { ?>
    <tr>
        <td width="2%">
            <img   height="40"  class="center-block" style="border-radius: 10px !important" src="<?php echo fotoPerfil($setor['FOTO']) ?>" />
        </td>
        <td data-search="<?php echo $setor['SETOR'].shortName1($setor['NOME'].$setor['LINHA'].$setor['REGIONAL']) ?>">
            <a href="#" class="btn-sel" data-val="<?php echo $setor['SETOR'] ?>">
                <b><?php echo $setor['SETOR'].' '.$setor['NOME'] ?></b>
            </a>
            <div><small><?php echo $setor['PERFIL'].' '.$setor['LINHA'] ?></small></div>
        </td>
    </tr>
    <?php } ?>
    <?php } ?>
</table>


<script>
$("#search-col").keyup(function () {
    var valor = $(this).val();
    pesquisarTabela(valor, 'tb-search-col');
});
</script>