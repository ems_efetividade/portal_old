<style>

#nav > li > a {
    padding:15px 15px;
    margin: 1px 1px;
}

#nav > li > a:hover {
    color:#fff;
}

</style>

<div class="row">

    <div class="col-lg-7 col-sm-7 col-xs-6">
        <div style="font-size:18px;padding:13px">
        <!-- <button type="button" class="btn btn-default" id="btn-hide-panel" onClick="esconderPainel()">button</button> -->
        <span class="hidden-xs"><b><?php echo $modulo ?></b><small> - </small></span> <small>Painel <?php echo appFunction::dadoSessao('unidade_neg'); ?></small>
        </div>

    
       
    

    </div>


    <div class="col-lg-5 col-sm-5 col-xs-6">

        <ul id="nav" class="nav nav-pills pull-right">

            <?php if (count($setores) > 1) { ?>

                <li>
                    <a href="<?php echo appConf::caminho ?>painel/ranking">
                        <span class="glyphicon glyphicon-sort"></span> 
                        <span class="hidden-xs">Ranking</span>
                    </a>
                </li>

                <li>
                    <a href="<?php echo appConf::caminho ?>painel/comparar">
                        <span class="glyphicon glyphicon-tasks"></span> <span class="hidden-xs">Comparar</span>
                    </a>
                </li>

            <?php } ?>

            <?php if($admin[1]['EXISTE'] == 1) { ?>
                <li>
                    <a href="<?php echo appConf::caminho ?>painel/admin">
                        <span class="glyphicon glyphicon-cog"></span> 
                        <span class="hidden-xs">Administração</span>
                    </a>
                </li>
            <?php } ?>
        </ul>
    </div>



</div>

<script>

function esconderPainel() {
    var itv = null; 
    w  = $('#pnl-left').outerWidth();
    l =  $('#pnl-left').position();
    v =  $('#pnl-left').outerWidth();


    if($('#pnl-left').is(":visible")) {
        $('#pnl-left').hide();
        $('#pnl-left').removeClass('col-lg-10')
        $('#pnl-left').addClass('col-lg-12')
    } else {
        $('#pnl-left').show();
        $('#pnl-left').removeClass('col-lg-12')
        $('#pnl-left').addClass('col-lg-10')
    }


   //alert(w);
   //$('.panel-left').css('left',-w);

    //console.log(left);
    //$('.panel-right').css('width', v);
    //$('.panel-left').hide();
    
    /*
    x = 0
    itv = setInterval(function() {
        $('.panel-left').css('left', x);
        $('.panel-right').css('width', v);
        x = x - 1;
        //v++;
        if(x <= -w) {
            clearInterval(itv);
        }
    }, 0.5);
*/
}

</script>