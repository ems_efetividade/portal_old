<div class="psadding" style="padding-left:20px; padding-right:20px;">

                
                <div class="row">

                    <div id="container-box" style="position:relative; z-insdex:0; display:none;">
                        <?php foreach ($boxes as $i => $box) { ?>
                            
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 nopad panel-box " >
                                <div class="panel panel-default padding">
                                    <div class="panel-body text-center">
                                        <div id="box-<?php echo $i ?>">
                                        <?php $tool = ($box['DESC_PILAR'] != "") ? array('icon' => '*', 'text' => 'data-html="true" data-container="body" data-toggle="popover" data-placement="top" data-content="'.$box['DESC_PILAR'].'"' )  : array('icon' => null, 'text' => null) ?>
                                        <span <?php echo $tool['text'] ?>><small><b><?php echo $box['PILAR'] ?><?php echo $tool['icon'] ?></b></small></span>
                                        <div><small><small class="text-muted">(<?php echo $box['MES'] ?>)</small></small></div>
                                        
                                        <div class="box">
                                            <b>
                                                <div class="count-up" data-start="0" data-end="<?php echo $box['M00'] ?>" data-num-decimal="<?php echo $box['DECIMAL'] ?>" data-duration="<?php echo rand(1, 4) ?>" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="<?php echo $box['SUFIXO'] ?>" id="mkt<?php echo rand() ?>">
                                                    <?php echo appFunction::formatarMoeda($box['M00'], $box['DECIMAL']) ?><?php echo $box['SUFIXO'] ?>
                                                </div>
                                            </b>
                                        </div>

                                        <?php if($box['EVOL_MES'] != '') { ?>
                                            <small class="text-muted"><span style="border-radius:4px !important" class="label label-<?php echo ($box['EVOL_MES'] <= 0) ? 'danger' : 'success' ?>"><b><?php echo ($box['EVOL_MES'] <= 0) ? '' : '+' ?><?php echo appFunction::formatarMoeda($box['EVOL_MES'], $box['DECIMAL']) ?></b></span> do <?php echo $box['LABEL_EVOL'] ?> anterior.</small>
                                        <?php } else { ?>
                                            <small class="text-muted"><span style="border-radius:4px !important" class="label lasbel-danger"><b>&nbsp;</b></span>&nbsp;</small>
                                        <?php } ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>

                        <a href="#" id="btn-ant" onClick="moverBoxAnt()"   style="font-size:30px;color:#ddd;position:absolute; z-index:1; left:10px; top:100px;" class="">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                        
                        <a href="#" id="btn-prox" onClick="moverBoxProx()" style="font-size:30px;color:#ddd;position:absolute; z-index:1; right:10px; top:100px;" class="">
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </a>

                    </div>

                    
                    <!-- <button type="button" id="btn-prox" onClick="moverBoxProx()" style="" class="btn btn-default">></button> -->

                </div>
                </div>                            

                        <div class="padding">
                            <!-- <div class="padding">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 nopad">
                                        <div class="carousel carousel-showsixmoveone slide" id="carousel123">
                                            <div class="carousel-inner">
                                                <?php foreach ($boxes as $i => $box) { ?>
                                                    <div class="item <?php echo ($i == 1) ? 'active' : '' ?>">
                                                        <div class="col-lg-<?php echo $col ?> col-md-<?php echo $col ?> col-sm-<?php echo $col ?> col-xs-<?php echo $col ?> nopad">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body text-center">
                                                                    <?php $tool = ($box['DESC_PILAR'] != "") ? array('icon' => '*', 'text' => 'data-html="true" data-container="body" data-toggle="popover" data-placement="top" data-content="'.$box['DESC_PILAR'].'"' )  : array('icon' => null, 'text' => null) ?>
                                                                    <span <?php echo $tool['text'] ?>><small><b><?php echo $box['PILAR'] ?><?php echo $tool['icon'] ?></b></small></span>
                                                                    <div><small><small class="text-muted">(<?php echo $box['MES'] ?>)</small></small></div>
                                                                    
                                                                    <div class="box">
                                                                        <b>
                                                                            <div class="count-up" data-start="0" data-end="<?php echo appFunction::formatarMoeda($box['M00'],2) ?>" data-num-decimal="<?php echo $box['DECIMAL'] ?>" data-duration="<?php echo rand(1, 4) ?>" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="<?php echo $box['SUFIXO'] ?>" id="mkt<?php echo rand() ?>">
                                                                                <?php echo appFunction::formatarMoeda($box['M00'], $box['DECIMAL']) ?><?php echo $box['SUFIXO'] ?>
                                                                            </div>
                                                                        </b>
                                                                    </div>

                                                                    <?php if($box['EVOL_MES'] != '') { ?>
                                                                        <small class="text-muted"><span style="border-radius:4px !important" class="label label-<?php echo ($box['EVOL_MES'] <= 0) ? 'danger' : 'success' ?>"><b><?php echo ($box['EVOL_MES'] <= 0) ? '' : '+' ?><?php echo appFunction::formatarMoeda($box['EVOL_MES'], $box['DECIMAL']) ?></b></span> do <?php echo $box['LABEL_EVOL'] ?> anterior.</small>
                                                                    <?php } else { ?>
                                                                        <small class="text-muted"><span style="border-radius:4px !important" class="label lasbel-danger"><b>&nbsp;</b></span>&nbsp;</small>
                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <a class="left carousel-control" href="#carousel123" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                            <a class="right carousel-control" href="#carousel123" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-12">

                                    </div>
                                </div>
                            </div>

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 nopad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default" id="panel-ranking">
                                                    <div class="panel-body">
                                                        <h5 style="margin:0px"><a data-toggle="collapse" href=".collapseExample"><span class="pull-right caret"></span></a></h5>
                                                        <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Pontuação</b></h5>
                                                        <div class="collapse in collapseExample" id="collapseExample"> 
                                                            <div class="hr"></div>

                                                            <div class="scroll-custom" style="height: 140px">
                                                                <table class="table table-condensed" style="font-size:12px">
                                                                    <tr class="active">
                                                                        <th>Pilar</th>
                                                                        <th>Valor</th>
                                                                        <th class="text-center">Nota</th>
                                                                    </tr>
                                                                    <?php foreach ($ponts as $ponto) { ?>
                                                                    
                                                                        <tr>
                                                                            <td width="30%"><?php echo $ponto['PILAR'] ?></td>
                                                                            <td width="50%" class="text-center">
                                                                                <?php
                                                                                    $v = rand(1, 110);

                                                                                    $a[0] = 'danger';
                                                                                    $a[1] = 'danger';
                                                                                    $a[2] = 'warning';
                                                                                    $a[3] = 'success';
                                                                                    $a[4] = 'primary';

                                                                                    $b[1] = 'glyphicon glyphicon-thumbs-down';
                                                                                    $b[2] = 'glyphicon glyphicon-exclamation-sign';
                                                                                    $b[3] = 'glyphicon glyphicon-thumbs-up';
                                                                                    $b[4] = 'glyphicon glyphicon-star';
                                                                                ?>
                                                                                <div class="progress">
                                                                                    <div class="progress-bar progress-bar-<?php echo $a[$ponto['PONTO']] ?>" role="progressbar" data-value="<?php echo $ponto['M00'] ?>" aria-valuenow="<?php echo $ponto['M00'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                        <span class="progress-bar-title progress-bar-title-<?php echo ($ponto['M00'] <= 45) ? 'black' : 'white' ?>"><?php echo appFunction::formatarMoeda($ponto['M00'], $ponto['DECIMAL']) ?><?php echo $ponto['SUFIXO'] ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            </td >
                                                                            <td width="20%" class="text-center">
                                                                                <span class="label label-<?php echo $a[$ponto['PONTO']] ?> <?php echo $ponto['PONTO'] ?>">
                                                                                    <?php echo $ponto['PONTO'] ?>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>  
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>


                                                    <?php if ($rnkPainel[1]['COUNT_BRASIL'] != 1) { ?>
                                                    <!-- Rankings -->
                                                    <div class="collapse in collapseExample">
                                                        <div class="row">
                                                            <?php $col = ($rnkPainel[1]['COUNT_REGIONAL'] != 1) ? array(6, 0) : array(12, -2); ?>
                                                            <div class="relogio col-lg-<?php echo $col[0] ?>  col-xs-<?php echo $col[0] ?> col-sm-<?php echo $col[0] ?>" style="padding-right:<?php echo $col[1] ?>px">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 text-center">
                                                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> <?php echo $rnkPainel[1]['LABEL_BRASIL'] ?></b></h5>
                                                                                <canvas wisdth="150" hesight="80" id="foo" class="" style="padding:0px;width: 200px; height: 103px"></canvas>    
                                                                                <div class="center-block" style="width:125px">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><?php echo $rnkPainel[1]['COUNT_BRASIL'] ?>º</div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><b><span id="preview-textfield"></span>º </b></div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4">1º</div>
                                                                                    </div>
                                                                                </div>
                                                                                <small></small>                                                                     
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <?php if ($rnkPainel[1]['COUNT_REGIONAL'] != 1) { ?>
                                                            <div class="relogio col-lg-6 col-xs-6 col-sm-6" style="padding-left:0px">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 text-center">
                                                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> <?php echo $rnkPainel[1]['LABEL_REGIONAL'] ?></b></h5>

                                                                                <canvas wisdth="150" hesight="80" id="fooo" class="" style="padding:0px;width: 200px; height: 103px"></canvas>    
                                                                                <div class="center-block" style="width:125px">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><?php echo $rnkPainel[1]['COUNT_REGIONAL'] ?>º</div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><b><span id="preview-textfield2"></span>º </b></div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4">1º</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-7 col-md-7 nopad">
                                        <div class="panel panel-default" id="panel-grafico">
                                            <div class="panel-body">
                                                <h5 style="margin:0px;"><a data-toggle="collapse" href=".collapseExample2"><span class="pull-right caret"></span></a></h5>
                                                <h5 style="margin:0px;"><b><span class="glyphicon glyphicon-stats"></span> Evolução dos Pilares</b><small> </small></h5>
                                                <div class="collapse in collapseExample2">
                                                    <div class="hr"></div>
                                                    <div style="height: 306px">
                                                        <div id="container" style="min-width: 310px;max-width: 800px;height: 300px;margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-12 nopad">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Pilares de Produtividade</b> <small><span class="text-muted"></span></small></h5>
                                                <div class="hr"></div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <?php echo $tabelaPilares; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php //}  ?>
                        
                    </div>
                    
                    </div>
                </div>
            </div>
            </div>