
<?php include('app/view/headerView.php'); ?>
<?php require_once('includes.php'); ?>

<script>

    $('[data-toggle="popover"]').popover();

    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() < (screen.height - 100)) ? (screen.height - 100) : $('.panel-right').height();
        $('.panel-left').css('height', (screenSize));
        $('.relogio').find('div:visible').first().attr('class');
    }

    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }


    $(window).ready(function (a) {
        resizePanelLeft();
    });

</script>

<?php
/*
foreach($boxes as $x =>  $box) {
    $arr[] = $box;
    $box['PILAR'] = $box['PILAR']. ' '.$x;
    $arr[] = $box;
}
$boxes = $arr;
*/
$totalBox = count($boxes);

if ($totalBox <= 6 && $totalBox > 0) {
    $col = floor(12 / $totalBox);
} else {
    $col = 2;
}
?>
<style>
.tooltip, .popover-content {
  font-size: 11px;
}
    .carousel-showsixmoveone .carousel-control {
        width: 4%;
        background-image: none;
    }
    .carousel-showsixmoveone .carousel-control.left {
        margin-left: 15px;
    }
    .carousel-showsixmoveone .carousel-control.right {
        margin-right: 15px;
    }
    /*aqui*/
<?php for ($i = 1; $i <= $totalBox; $i++) { ?>
        .carousel-showsixmoveone .cloneditem-<?php echo $i ?> {
            display: none;
        }
<?php } ?>

    @media all and (min-width: 768px) {
        .carousel-showsixmoveone .carousel-inner > .active.left,
        .carousel-showsixmoveone .carousel-inner > .prev {
            left: -33.333%;
        }
        .carousel-showsixmoveone .carousel-inner > .active.right,
        .carousel-showsixmoveone .carousel-inner > .next {
            left: 33.333%;
        }
        .carousel-showsixmoveone .carousel-inner > .left,
        .carousel-showsixmoveone .carousel-inner > .prev.right,
        .carousel-showsixmoveone .carousel-inner > .active {
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner .cloneditem-1,
        .carousel-showsixmoveone .carousel-inner .cloneditem-2 {
            display: block;
        }
    }
    @media all and (min-width: 768px) and (transform-3d), all and (min-width: 768px) and (-webkit-transform-3d) {
        .carousel-showsixmoveone .carousel-inner > .item.active.right,
        .carousel-showsixmoveone .carousel-inner > .item.next {
            -webkit-transform: translate3d(33.333%, 0, 0);
            transform: translate3d(33.333%, 0, 0);
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner > .item.active.left,
        .carousel-showsixmoveone .carousel-inner > .item.prev {
            -webkit-transform: translate3d(-33.333%, 0, 0);
            transform: translate3d(-33.333%, 0, 0);
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner > .item.left,
        .carousel-showsixmoveone .carousel-inner > .item.prev.right,
        .carousel-showsixmoveone .carousel-inner > .item.active {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            left: 0;
        }
    }
    @media all and (min-width: 992px) {
        .carousel-showsixmoveone .carousel-inner > .active.left,
        .carousel-showsixmoveone .carousel-inner > .prev {
            left: -16.666%;
        }
        .carousel-showsixmoveone .carousel-inner > .active.right,
        .carousel-showsixmoveone .carousel-inner > .next {
            left: 16.666%;
        }
        .carousel-showsixmoveone .carousel-inner > .left,
        .carousel-showsixmoveone .carousel-inner > .prev.right,
        .carousel-showsixmoveone .carousel-inner > .active {
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner .cloneditem-3,
        .carousel-showsixmoveone .carousel-inner .cloneditem-4,
        .carousel-showsixmoveone .carousel-inner .cloneditem-5 {
            display: block;
        }
    }
    @media all and (min-width: 992px) and (transform-3d), all and (min-width: 992px) and (-webkit-transform-3d) {
        .carousel-showsixmoveone .carousel-inner > .item.active.right,
        .carousel-showsixmoveone .carousel-inner > .item.next {
            -webkit-transform: translate3d(16.666%, 0, 0);
            transform: translate3d(16.666%, 0, 0);
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner > .item.active.left,
        .carousel-showsixmoveone .carousel-inner > .item.prev {
            -webkit-transform: translate3d(-16.666%, 0, 0);
            transform: translate3d(-16.666%, 0, 0);
            left: 0;
        }
        .carousel-showsixmoveone .carousel-inner > .item.left,
        .carousel-showsixmoveone .carousel-inner > .item.prev.right,
        .carousel-showsixmoveone .carousel-inner > .item.active {
            -webkit-transform: translate3d(0, 0, 0);
            transform: translate3d(0, 0, 0);
            left: 0;
        }
    }



    .td-pad {
        padding-left:15px;
    }

    .td-border {
        border-right: 3px solid #ddd !important;   
    }


    .c-1 {
        color:green;
    }

    .c-0 {
        color: red;
    }
    
    .c-2 {
        color: orange;
    }

    .hand {
        cursor:pointer;
    }

body.modal-open {
    overflow: visible;
}
    
    
</style>

<?php
//print_r($_SESSION['data-setores'])
?>
<div id="users-device-size">
  <div id="xs" col="4" class="visible-xs"></div>
  <div id="sm" col="3" class="visible-sm"></div>
  <div id="md" col="3" class="visible-md"></div>
  <div id="lg" col="2" class="visible-lg"></div>
</div>


<div class="container-fluid">

    <div class="row">
    
        <div class="col-lg-2 panel-left hidden-xs hidden-sm hidden-md" id="pnl-left" style="z-index:0">
            <?php require_once('app/view/painel/menu.php'); ?>
            <?php require_once('app/view/painel/filtros.php'); ?>
        </div>

        <div class="col-lg-12 panel-mobile  hidden-lg" style="">
            <?php require('app/view/painel/menu.php'); ?>
        </div>

        <div class="col-lg-10 panel-right" >
            
            <div class="nav-menu-right">
                <?php require_once('app/view/painel/nav.php'); ?>
            </div>

            <div>
            <div class="psadding" style="padding-left:20px; padding-right:20px;">

                
                <div class="row" id="">

                    <div id="container-box" style="position:relative; displaay:none;">
                        <?php foreach ($boxes as $i => $box) { ?>
                            
                            <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 nopad panel-box " >
                                <div class="panel panel-default padding">
                                    <div class="panel-body text-center">
                                        <div id="box-<?php echo $i ?>">
                                        <?php $tool = ($box['DESC_PILAR'] != "") ? array('icon' => '*', 'text' => 'data-html="true" data-container="body" data-toggle="popover" data-placement="top" data-content="'.$box['DESC_PILAR'].'"' )  : array('icon' => null, 'text' => null) ?>
                                        <span <?php echo $tool['text'] ?>><small><b><?php echo $box['PILAR'] ?><?php echo $tool['icon'] ?></b></small></span>
                                        <div><small><small class="text-muted">(<?php echo $box['MES'] ?>)</small></small></div>
                                        
                                        <div class="box">
                                            <b>
                                                <div class="count-up" data-start="0" data-end="<?php echo $box['M00'] ?>" data-num-decimal="<?php echo $box['DECIMAL'] ?>" data-duration="<?php echo rand(1, 4) ?>" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="<?php echo $box['SUFIXO'] ?>" id="mkt<?php echo rand() ?>">
                                                    <?php echo appFunction::formatarMoeda($box['M00'], $box['DECIMAL']) ?><?php echo $box['SUFIXO'] ?>
                                                </div>
                                            </b>
                                        </div>

                                        <?php if($box['EVOL_MES'] != '') { ?>
                                            <small class="text-muted"><span style="border-radius:4px !important" class="label label-<?php echo ($box['EVOL_MES'] <= 0) ? 'danger' : 'success' ?>"><b><?php echo ($box['EVOL_MES'] <= 0) ? '' : '+' ?><?php echo appFunction::formatarMoeda($box['EVOL_MES'], $box['DECIMAL']) ?></b></span> do <?php echo $box['LABEL_EVOL'] ?> anterior.</small>
                                        <?php } else { ?>
                                            <small class="text-muted"><span style="border-radius:4px !important" class="label lasbel-danger"><b>&nbsp;</b></span>&nbsp;</small>
                                        <?php } ?>

                                        </div>

                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                        </div>

                        <a href="#" id="btn-ant" onClick="moverBoxAnt()"   style="font-size:30px;color:#ddd;position:absolute; z-index:1; left:10px; top:100px;" class="">
                            <i class="glyphicon glyphicon-chevron-left"></i>
                        </a>
                        
                        <a href="#" id="btn-prox" onClick="moverBoxProx()" style="font-size:30px;color:#ddd;position:absolute; z-index:1; right:10px; top:100px;" class="">
                            <i class="glyphicon glyphicon-chevron-right"></i>
                        </a>

                    </div>

                    
                    <!-- <button type="button" id="btn-prox" onClick="moverBoxProx()" style="" class="btn btn-default">></button> -->

                </div>
                </div>                            

                        <div class="padding">
                            <!-- <div class="padding">
                                <div class="row">
                                    <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 nopad">
                                        <div class="carousel carousel-showsixmoveone slide" id="carousel123">
                                            <div class="carousel-inner">
                                                <?php foreach ($boxes as $i => $box) { ?>
                                                    <div class="item <?php echo ($i == 1) ? 'active' : '' ?>">
                                                        <div class="col-lg-<?php echo $col ?> col-md-<?php echo $col ?> col-sm-<?php echo $col ?> col-xs-<?php echo $col ?> nopad">
                                                            <div class="panel panel-default">
                                                                <div class="panel-body text-center">
                                                                    <?php $tool = ($box['DESC_PILAR'] != "") ? array('icon' => '*', 'text' => 'data-html="true" data-container="body" data-toggle="popover" data-placement="top" data-content="'.$box['DESC_PILAR'].'"' )  : array('icon' => null, 'text' => null) ?>
                                                                    <span <?php echo $tool['text'] ?>><small><b><?php echo $box['PILAR'] ?><?php echo $tool['icon'] ?></b></small></span>
                                                                    <div><small><small class="text-muted">(<?php echo $box['MES'] ?>)</small></small></div>
                                                                    
                                                                    <div class="box">
                                                                        <b>
                                                                            <div class="count-up" data-start="0" data-end="<?php echo appFunction::formatarMoeda($box['M00'],2) ?>" data-num-decimal="<?php echo $box['DECIMAL'] ?>" data-duration="<?php echo rand(1, 4) ?>" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="<?php echo $box['SUFIXO'] ?>" id="mkt<?php echo rand() ?>">
                                                                                <?php echo appFunction::formatarMoeda($box['M00'], $box['DECIMAL']) ?><?php echo $box['SUFIXO'] ?>
                                                                            </div>
                                                                        </b>
                                                                    </div>

                                                                    <?php if($box['EVOL_MES'] != '') { ?>
                                                                        <small class="text-muted"><span style="border-radius:4px !important" class="label label-<?php echo ($box['EVOL_MES'] <= 0) ? 'danger' : 'success' ?>"><b><?php echo ($box['EVOL_MES'] <= 0) ? '' : '+' ?><?php echo appFunction::formatarMoeda($box['EVOL_MES'], $box['DECIMAL']) ?></b></span> do <?php echo $box['LABEL_EVOL'] ?> anterior.</small>
                                                                    <?php } else { ?>
                                                                        <small class="text-muted"><span style="border-radius:4px !important" class="label lasbel-danger"><b>&nbsp;</b></span>&nbsp;</small>
                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                            <a class="left carousel-control" href="#carousel123" data-slide="prev"><i class="glyphicon glyphicon-chevron-left"></i></a>
                                            <a class="right carousel-control" href="#carousel123" data-slide="next"><i class="glyphicon glyphicon-chevron-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div> -->

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-12">

                                    </div>
                                </div>
                            </div>

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-5 col-md-5 nopad">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default" id="panel-ranking">
                                                    <div class="panel-body">
                                                        <h5 style="margin:0px"><a data-toggle="collapse" href=".collapseExample"><span class="pull-right caret"></span></a></h5>
                                                        <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Pontuação</b></h5>
                                                        <div class="collapse in collapseExample" id="collapseExample"> 
                                                            <div class="hr"></div>

                                                            <div class="scroll-custom" style="height: 140px">
                                                                <table class="table table-condensed" style="font-size:12px">
                                                                    <tr class="active">
                                                                        <th>Pilar</th>
                                                                        <th>Valor</th>
                                                                        <th class="text-center">Nota</th>
                                                                    </tr>
                                                                    <?php foreach ($ponts as $ponto) { ?>
                                                                    
                                                                        <tr>
                                                                            <td width="30%"><?php echo $ponto['PILAR'] ?></td>
                                                                            <td width="50%" class="text-center">
                                                                                <?php
                                                                                    $v = rand(1, 110);

                                                                                    $a[0] = 'danger';
                                                                                    $a[1] = 'danger';
                                                                                    $a[2] = 'warning';
                                                                                    $a[3] = 'success';
                                                                                    $a[4] = 'primary';

                                                                                    $b[1] = 'glyphicon glyphicon-thumbs-down';
                                                                                    $b[2] = 'glyphicon glyphicon-exclamation-sign';
                                                                                    $b[3] = 'glyphicon glyphicon-thumbs-up';
                                                                                    $b[4] = 'glyphicon glyphicon-star';
                                                                                ?>
                                                                                <div class="progress">
                                                                                    <div class="progress-bar progress-bar-<?php echo $a[$ponto['PONTO']] ?>" role="progressbar" data-value="<?php echo $ponto['M00'] ?>" aria-valuenow="<?php echo $ponto['M00'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
                                                                                        <span class="progress-bar-title progress-bar-title-<?php echo ($ponto['M00'] <= 45) ? 'black' : 'white' ?>"><?php echo appFunction::formatarMoeda($ponto['M00'], $ponto['DECIMAL']) ?><?php echo $ponto['SUFIXO'] ?></span>
                                                                                    </div>
                                                                                </div>
                                                                            </td >
                                                                            <td width="20%" class="text-center">
                                                                                <span class="label label-<?php echo $a[$ponto['PONTO']] ?> <?php echo $ponto['PONTO'] ?>">
                                                                                    <?php echo $ponto['PONTO'] ?>
                                                                                </span>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </table>  
                                                            </div>
                                                        </div>
                                                    </div> 
                                                </div>


                                                    <?php if ($rnkPainel[1]['COUNT_BRASIL'] != 1) { ?>
                                                    <!-- Rankings -->
                                                    <div class="collapse in collapseExample">
                                                        <div class="row">
                                                            <?php $col = ($rnkPainel[1]['COUNT_REGIONAL'] != 1) ? array(6, 0) : array(12, -2); ?>
                                                            <div class="relogio col-lg-<?php echo $col[0] ?>  col-xs-<?php echo $col[0] ?> col-sm-<?php echo $col[0] ?>" style="padding-right:<?php echo $col[1] ?>px">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 text-center">
                                                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> <?php echo $rnkPainel[1]['LABEL_BRASIL'] ?></b></h5>
                                                                                <canvas wisdth="150" hesight="80" id="foo" class="" style="padding:0px;width: 200px; height: 103px"></canvas>    
                                                                                <div class="center-block" style="width:125px">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><?php echo $rnkPainel[1]['COUNT_BRASIL'] ?>º</div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><b><span id="preview-textfield"></span>º </b></div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4">1º</div>
                                                                                    </div>
                                                                                </div>
                                                                                <small></small>                                                                     
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <?php if ($rnkPainel[1]['COUNT_REGIONAL'] != 1) { ?>
                                                            <div class="relogio col-lg-6 col-xs-6 col-sm-6" style="padding-left:0px">
                                                                <div class="panel panel-default">
                                                                    <div class="panel-body">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 text-center">
                                                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> <?php echo $rnkPainel[1]['LABEL_REGIONAL'] ?></b></h5>

                                                                                <canvas wisdth="150" hesight="80" id="fooo" class="" style="padding:0px;width: 200px; height: 103px"></canvas>    
                                                                                <div class="center-block" style="width:125px">
                                                                                    <div class="row">
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><?php echo $rnkPainel[1]['COUNT_REGIONAL'] ?>º</div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4"><b><span id="preview-textfield2"></span>º </b></div>
                                                                                        <div class="col-lg-4 col-sm-4 col-xs-4">1º</div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-7 col-md-7 nopad">
                                        <div class="panel panel-default" id="panel-grafico">
                                            <div class="panel-body">
                                                <h5 style="margin:0px;"><a data-toggle="collapse" href=".collapseExample2"><span class="pull-right caret"></span></a></h5>
                                                <h5 style="margin:0px;"><b><span class="glyphicon glyphicon-stats"></span> Evolução dos Pilares</b><small> </small></h5>
                                                <div class="collapse in collapseExample2">
                                                    <div class="hr"></div>
                                                    <div style="height: 306px">
                                                        <div id="container" style="min-width: 310px;max-width: 800px;height: 300px;margin: 0 auto"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="padding">
                                <div class="row">
                                    <div class="col-lg-12 nopad">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <h5 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Pilares de Produtividade</b> <small><span class="text-muted"></span></small></h5>
                                                <div class="hr"></div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                    <div class="table-responsive">
                                                        <?php echo $tabelaPilares; ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    <?php //}  ?>
                        
                    </div>
                    
                    </div>
                </div>
                </div>
                </div>
            </div>
        </div>    
    </div>  
</div>  



<!-- Modal -->
<div class="modal fade" id="modal-produto" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary" >
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="ModalLabel"><b><span id="nomePilar"></span></b></h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>

<?php include('app/view/footerView.php') ?>

<Script>
var boxes = [];
var cols = 0;

var prox = 1;

function getBootstrapDeviceSize() {
    col =  $('#users-device-size').find('div:visible').first().attr('id');
    console.log($('#users-device-size').find('div:visible').first().attr('id'));
    return col;
}

function getBoxesOnLoad() {
    $(".panel-box").each(function(a, e){
        boxes[(a+1)] = e;
    })
    console.log(boxes);
}

function listarBoxes(i) {

    x = (typeof i !== 'undefined') ? i : 1;

    var tamanho = getBootstrapDeviceSize();
    var numBox = boxes.length - 1;
    var max = 6;

    
    var limiteBox = [];

    limiteBox['lg'] = 6;
    limiteBox['md'] = 4;
    limiteBox['sm'] = 3;
    limiteBox['xs'] = 3;

    var html   = '';
    var limite = limiteBox[tamanho];
    var coluna = 'col-' + tamanho + '-';
    var oldCol = coluna + (12/limite);
    var newCol = oldCol;

    if(numBox < max) {
        t = ((12 % numBox) != 0) ? (12 / (numBox - 1)) : (12 / numBox);
        newCol = coluna +  t
        limite = (numBox < limite) ? t+1 : limite;
    }

    for(i=1; i<=limite; i++) {

        if(typeof boxes[x] === 'undefined') {
            x = 1;
        }

        box = boxes[x].outerHTML;
        box = box.replace(oldCol, newCol);
    
        html = html + box;
        x++;
    }
    $('#container-box').html(html);
}

function moverBoxProx() {
    prox = ((prox+1) >= boxes.length) ? 1 : prox + 1 ;
    listarBoxes(prox);
    $('.panel-box').last().hide().fadeIn();
    //$('.panel-box').first().slideToggle({width: 'toggle'});
    
/*
    var lefty = $('.panel-box').last();
    var i = 0
    var intervalId = null;
    var max = lefty.outerWidth();

    p = $('#container-box').position();
    intervalId = setInterval(function() {
        $('#container-box').css('left', -i);
       // $('#container-box').css('left', -i + 'px');
        
        i = i + 3;
        if(i >= max) {
            clearInterval(intervalId);
            $('#container-box').css('left', 0);
            listarBoxes(prox);
            $('.panel-box').last().hide().fadeIn();
        }
    }, 3);
    */

} 

function moverBoxAnt() {
    prox = ((prox-1) <= 0) ? (boxes.length)-1 : prox - 1 ;
    listarBoxes(prox);
    $('.panel-box').first().hide().fadeIn();

    
/*
    var lefty = $('.panel-box').last();
    var i = 0
    var intervalId = null;
    var max = lefty.outerWidth();
    
    p = $('#container-box').position();
    
    intervalId = setInterval(function() {
        $('#container-box').css('left', i);
       // $('#container-box').css('left', -i + 'px');
        
        i = i + 3;
        if(i >= max) {
            clearInterval(intervalId);
            $('#container-box').css('left', 0);
            listarBoxes(prox);
            $('.panel-box').first().hide().fadeIn();
        }
    }, 3);*/
} 


$(window).resize(function () {
    cols = getBootstrapDeviceSize();
    listarBoxes(1);
});


    //$('#k').hide();
$(document).on('ready',function(){
    //abrirPainel();
    javascript();
})


function javascript() {
    
    cols = getBootstrapDeviceSize();
    getBoxesOnLoad();
    listarBoxes(0);

    $('#container-box').fadeIn();
    setInterval(moverBoxProx, 20000)

    
    $(function () {
        $('[data-toggle="tooltip"]').css("z-index", "1006");
        $('[data-toggle="tooltip"]').tooltip();
    })

    $(function () {
        $('[data-toggle="popover"]').popover({ trigger: "hover" });
    })
       
    
    $('.progress .progress-bar').css("width",function() {
      return $(this).attr("aria-valuenow") + "%";
    });
    
    /* arquivo function.js */
    startCount('.count-up');
    
    $('.panel-right').show();
    var opts = {
        angle: 0, // The span of the gauge arc
        lineWidth: 0.44, // The line thickness
        radiusScale: 1, // Relative radius
        pointer: {
            length: 0.5, // // Relative to gauge radius
            strokeWidth: 0.033, // The thickness
            color: '#000000' // Fill color
        }, staticLabels: {
            font: "10px sans-serif", // Specifies font
            labels: [100, 130, 150, 220.1, 260, 300], // Print labels at these values
            color: "#000000", // Optional: Label text color
            fractionDigits: 1  // Optional: Numerical precision. 0=round off.
        },

        limitMax: false, // If false, max value increases automatically if value > maxValue
        percentColors: [
            [0, "#c9302c"],
            [0.25, "#ec971f"],
            [0.75, "#ec971f"],
            [1.0, "#5cb85c"]],
        limitMin: false, // If true, the min value of the gauge will be fixed
        colorStart: '#000', // Colors
        colorStop: '#8FC0DA', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: false,
        highDpiSupport: false, // High resolution support

    };
    

    if (1 != <?php echo $rnkPainel[1]['COUNT_BRASIL'] ?>) {
        var target = document.getElementById('foo'); // your canvas element
        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        gauge.minValue = <?php echo -($rnkPainel[1]['COUNT_BRASIL']) ?>; // set max gauge value
        gauge.maxValue = -1;  // Prefer setter over gauge.minValue = 0
        gauge.animationSpeed = (<?php echo -($rnkPainel[1]['RNK_BRASIL']) ?> == -1) ? 1.1 : 32; // set animation speed (32 is default value)
        gauge.set(<?php echo -($rnkPainel[1]['RNK_BRASIL']) ?>); // set actual value
        gauge.setTextField(document.getElementById("preview-textfield"));

     
    }
    
    if (1 != <?php echo $rnkPainel[1]['COUNT_REGIONAL'] ?>) {
        if(1 != <?php echo $rnkPainel[1]['COUNT_BRASIL'] ?>) {
            var target2 = document.getElementById('fooo');
            var gauge2 = new Gauge(target2).setOptions(opts);
            gauge2.minValue = <?php echo -($rnkPainel[1]['COUNT_REGIONAL']) ?>; // set max gauge value
            //gauge2.minValue = -103; // set max gauge value
            gauge2.maxValue = -1;  // Prefer setter over gauge.minValue = 0
            gauge2.animationSpeed = (<?php echo -($rnkPainel[1]['RNK_REGIONAL']) ?> == -1) ? 1.1 : 32; // set animation speed (32 is default value)
            gauge2.set(<?php echo -($rnkPainel[1]['RNK_REGIONAL']) ?>); // set actual value
            gauge2.setTextField(document.getElementById("preview-textfield2"));

        }
    }
    
    chart = new Highcharts.Chart({
        chart: {
            defaultSeriesType: 'spline',
            renderTo: 'container'
        },
        legend: {
            align: 'center',
            verticalAlign: 'bottom',
            layout: 'horizontal',
            itemStyle: {
                color: '#000000',
                fontWeight: 'bold',
                fontSize: '10px'
            }

        },

        title: {
            text: null

        },
        xAxis: {

            labels: {
                style: {
                    fontSize: '9px'
                }
            },
            categories: <?php echo $dadosGrafico['CAB']; ?>
        },
        tooltip: {
            pointFormat: '<b>{series.name}: </b>{point.y:,.1f}%'
        },
        yAxis: [{
                align: 'middle',
                title: {
                    style: {
                        fontSize: '10px'
                    },
                    text: '<?php echo $dadosGrafico['EIXOS']['e0']; ?>'
                },
                labels: {

                    format: '{value}'
                },
            }, {

                title: {
                    style: {
                        fontSize: '10px'
                    },
                    text: '<?php echo $dadosGrafico['EIXOS']['e1']; ?>'
                },
                labels: {
                    format: '{value}',

                },
                opposite: true
            }],

        plotOptions: {
            series: {
                animation: true
            },
            line: {
                dataLabels: {
                    enabled: true,

                },
                enableMouseTracking: true
            }
        },
        series: <?php echo $dadosGrafico['DADOS']; ?>



    });

    resizePanelLeft();
}


$(window).ready(function() {
    startCount('.count-up');
});


</script>

<script>


    if (1 == <?php echo $rnkPainel[1]['COUNT_BRASIL'] ?>) {
        $('#panel-ranking').height($('#panel-grafico').height());
    }

    $('.btn-modal-produto').on("click",function (e) {
        e.stopPropagation();
        
        
        var idPilar = $(this).data('id');
        var setor = $(this).data('setor');
        var pilar = $(this).data('pilar');

        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>painel/listarProduto/' + setor + '/' + idPilar,
            async: false,
            beforeSend: function (xhr) {
                 
            },
            success: function (retorno) { 
                $('#modal-produto .modal-body').html(retorno);
                $('#tit-tabela').html(pilar);
                $('#modal-produto #nomePilar').html(pilar);
                $('#modal-produto').modal('show');
            }
        });
    });



    $(document).ready(function (e) {
        $(".scroll-custom").mCustomScrollbar({
            theme: "dark"
        });
    });

    carousel();

    


    $('.colpse').hide();

    $('.toggle').unbind().click(function (e) {
        e.preventDefault();
        var c = $(this).data('val');
        $('.' + c).toggle("fast", function () {
            // Animation complete.
        });
        resizePanelLeft();
    });



    




    function carousel() {
        $('#carousel123').carousel({interval: 0});

        $('#carousel123 .item').each(function () {
            var itemToClone = $(this);

            for (var i = 1; i < <?php echo count($boxes) ?>; i++) {
                itemToClone = itemToClone.next();

                // wrap around if at end of item collection
                if (!itemToClone.length) {
                    itemToClone = $(this).siblings(':first');
                }

                // grab item, clone, add marker class, add to collection
                itemToClone.children(':first-child').clone()
                        .addClass("cloneditem-" + (i))
                        .appendTo($(this));
            }
        });
    }
</script>
