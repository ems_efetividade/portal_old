<?php require_once('functions.php'); ?>
<?php 

$icon[1] = '<img height="50" src="../app/view/painel/img/gold.png" />';
$icon[2] = '<img height="50" src="../app/view/painel/img/silver.png" />';
$icon[3] = '<img height="50" src="../app/view/painel/img/bronze.png" />';



$icon[1] = 'gold';
$icon[2] = 'silver';
$icon[3] = 'bronze';

$icon['outros'] = '';
?>

<!--<script src="https://code.highcharts.com/highcharts.js"></script>-->
<style>
    .gold, .silver, .bronze {
        width: 66px;
        height: 75px;
        z-index: 1000;
        position: absolute
    }
    .gold {background-image: url(../app/view/painel/img/back-gold.png);}
    .silver {background-image: url(../app/view/painel/img/back-silver.png);}
    .bronze {background-image: url(../app/view/painel/img/back-bronze.png);}
</style>


<div class="panel panel-default">
    <div class="panel-body  " >
        <h5 style="margin:0px"><a data-toggle="collapse" href=".collapseExample"><span class="pull-right caret"></span></a></h5>
        <h4 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Gráfico Top 5</b> </small></h4>
        
        <div class="collapse in collapseExample" id="collapseExample"> 
        <div class="hr"></div>
        
        <h4 class="text-left"><small>Principais setores em: <b><?php echo $filtros[3] ?> / <?php echo $filtros[4] ?> / <?php echo $filtros[5] ?></b></small></h4>
        <br>
        <div id="container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
        </div>
        </div>
</div>    


<div class="panel panel-default">
<!--    <div class="panel-heading text-muted" >
        <h4><b>Ranking de Equipe</b><small> (<b>Pilar: </b><?php echo $criterio['PILAR'] ?> | <b>Produto: </b><?php echo $criterio['PRODUTO'] ?> | <b>Período: </b><?php echo $criterio['PERIODO'] ?>)</small></h4>
    </div>-->
<div class="panel-body  " >
    
    <div class="row">
        <div class="col-lg-10">
            <h4 style="margin:0px"><b><span class="glyphicon glyphicon-stats"></span> Ranking de setores</b> <small> - <?php echo $filtros[3] ?> / <?php echo $filtros[4] ?> / <?php echo $filtros[5] ?></small></h4>
        </div>
        <div class="col-lg-2">
            <div class="pull-right">
            <!-- Single button -->
            <div class="btn-group btn-group-sm btn-block">
              <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Exportar <span class="caret"></span>
              </button>
                
              <ul class="dropdown-menu dropdown-menu-right">
                <li><a href="<?php echo appConf::caminho ?>painel/imprimirRank"><small>Arquivo .PDF</small></a></li>
<!--                <li><a href="<?php echo appConf::caminho ?>painel/exportarRank"><small>Arquivo .CSV (Excel)</small></a></li>-->
              </ul>
                
            </div>
        </div>
        </div>
    </div>
    
</div>
        <table class="table table-hover table-striped">
<?php foreach($dados as $i => $dado) { $col = ($i<=3) ? $i : 'outros' ?>

            <tr class="<?php echo ($dado['SETOR'] == appFunction::dadoSessao('setor')) ? 'info' : '' ?>">
              
                <td class="text-center">
                    <div class="box">
                        
                        <?php echo ($i).'º' ?>
                       
                    </div>
                    
                </td>
                
                
                <td width="1%">
                    <div class="<?php echo $icon[$col] ?>"></div>
                    <img height="75" style="border-radius:10px !important" src="<?php echo fotoPerfil($dado['FOTO']) ?>" />
                    
                </td>
                <td >
                    
                <div style="font-size:15px"><b><?php echo $dado['SETOR'] ?> <?php echo $dado['NOME'] ?></b></div>
                <div class="text-muted"><small><b>SETOR: </b><?php echo $dado['NOME_SETOR'] ?></small></div>
                <div class="text-muted"><small><?php echo $dado['PERFIL'] ?> (<?php echo $dado['LINHA'] ?>)</small></div>
                <div class="text-muted"><small><b>DATA INÍCIO: </b><?php echo appFunction::formatarData($dado['DT_INICIO']) ?></small></div>
                <div>
                    <a href="<?php echo appConf::caminho ?>painel/abrirPainel/<?php echo base64_encode($dado['SETOR']) ?>">
                        <small><b><span class=""></span> Ver Painel</b></small>
                    </a></div>
                </td>
                
                <td class="text-center">
                    <div><b><?php echo $filtros[3] ?></b></div>
                    <div><?php echo $filtros[4] ?></div>
                    <div><?php echo $filtros[5] ?></div>
                </td>
                
                <td class="text-center">
                    
                    <div class="box">
                        <span class="<?php echo ($dado['VALOR'] < 0) ? 'text-danger' : '' ?> count-upP" data-start="0" data-end="<?php echo $dado['VALOR'] ?>" data-num-decimal="<?php echo $dado['DECIMAL'] ?>" data-duration="<?php echo rand(1,2.5) ?>" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="<?php echo $dado['SUFIXO'] ?>" id="mkt<?php echo rand() ?>"><?php echo appFunction::formatarMoeda($dado['VALOR'], $dado['DECIMAL']).$dado['SUFIXO'] ?></span>
                    </div>
                    
                </td>
            </tr>


  

<?php } ?>
        </table>
         
</div>
<style>

    
</style>

<script>

$('.count-up').each(function( key, item ) {

        var options = {
            useEasing:   true,
            useGrouping: (!$(this).data('group')) ? true : $(this).data('group'), 
            separator:   $(this).data('separator'), 
            decimal:     $(this).data('decimal'), 
            suffix:      $(this).data('suffix')
        };


       countUp = new CountUp(
               $(this).attr('id'),
               $(this).data('start'),
               $(this).data('end'),
               $(this).data('num-decimal'),
               $(this).data('duration'), options
            );
       countUp.start();
       
});


var obj = $.parseJSON('<?php echo $fotosGrafico ?>');

chart = new Highcharts.Chart({

    chart: {
        type: 'column',
         renderTo: 'container'
    },
    title: {
        text: null
    },
    subtitle: {
        text: null
    },
    xAxis: {
         
        categories: <?php echo $serie['cat'] ?>,
        useHTML: true,
        labels: {
            useHTML: true,
            formatter: function() {
                return '<div class="text-center"><img style="border-radius:10px !important" src="'+obj[this.value]+'" height="40" /></div><div class="text-center">'+this.value+'</div>';
            },
            // align: 'middle',
        },
        title: {
            text: null
        }
    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
           
            dataLabels: {
                enabled: true,
                format: '{point.y:.<?php echo $serie['format']['dec'] ?>f}<?php echo $serie['format']['sufix'] ?>',
                y: 55,
                borderWidth: 0,
                style: {
                    fontWeight: false,
                    textShadow: false,
                    color: '#fff',
                    textOutline: '0px'
                }
            }
        }
    },
    yAxis: {
        max: (<?php echo $maxY ?> + 7.1),
        min: 0,
        title: {
            text: null
        }
    },
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0"> </td>' +
            '<td style="padding:0"><b>{point.y:.<?php echo $serie['format']['dec'] ?>f}<?php echo $serie['format']['sufix'] ?></b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
   
    series: <?php echo $serie['serie'] ?>
});




/*
var plotLeft = Highcharts.charts[0].plotLeft;
var plotTop = Highcharts.charts[0].plotTop;

//$.each(Highcharts.charts[0].series,function(i, s){
    $.each(Highcharts.charts[0].series[0].points, function(j, p){
        
        var pointShapeArgs = p.shapeArgs;
        //alert(pointShapeArgs.y);
        //alert('Series: ' + i +', Point: ' + j + ', Left: ' + (p.plotX + plotLeft) +  ', Top: ' + (p.plotY + plotTop));
        imgW = (pointShapeArgs.width - (pointShapeArgs.width * 0.3));
        imgH = (pointShapeArgs.width + (pointShapeArgs.width * 0.1));
        
        posY = pointShapeArgs.y - imgH;
        
        //alert('col: ' + pointShapeArgs.y + ' - ' + imgH + ' = ' + posY);
        //chart.renderer.image('https://www.emsprescricao.com.br/public/profile/'+obj[j], (p.plotX + plotLeft) - imgW / 2 , posY , imgW, imgH).add();   
    });
//});
*/

//chart.renderer.image('https://www.emsprescricao.com.br/public/profile/65d026ed995ec08b98941567b0436938.jpg', 20, 20, 30, 30).add();   
 


</script>
