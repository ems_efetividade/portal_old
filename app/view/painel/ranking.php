<?php include('app/view/headerView.php'); ?>
<?php require_once('functions.php'); ?>

<link href="<?php echo appConf::caminho ?>app/view/painel/css/painel.css" rel="stylesheet" type="text/css"/>
<?php require_once('includes.php'); ?>

<script>
        (function($){
                $(window).on("load",function(){

                        $(".panel-left").mCustomScrollbar({
                                theme:"minimal"
                        });

                });
        })(jQuery);
</script>
<script>

    $('[data-toggle="popover"]').popover();

    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
    }   

    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }
    
    
    $(window).ready(function(a){
        resizePanelLeft();
    });
    
    
    
    
</script>

<style>

    .chk {
      margin:0px !important;  
    }
    
    thick-border {
        border-right: 2px solid #ddd;
    }
    
    .hr {
        margin-top:6px;
        border-bottom:1px solid #ddd;
        margin-bottom: 6px;
    }

    .alert {margin-bottom:2px; padding:2px}
    .tb td, .tb th {
        white-space: nowrap;
    }

    .table {
        margin-bottom: 0px;
        font-size: 12px
    }

    .progress {
        margin-bottom: 0px;
        height: 15px;
        position: relative;
    }

    .nopad {
        padding:0px;
    }
    .progress-bar-title {
        position: absolute;
        text-align: center;
        line-height: 15px; /* line-height should be equal to bar height */
        overflow: hidden;
        color: #FFF;
        right: 0;
        left: 0;
        top: 0;
        font-weight: 600;
    }
    
    .progress-bar-title-white {
        color: #FFF;
    }
    
    .progress-bar-title-black {
        color: #777;
    }


    html, body {

        height:100%;
        /*font-family: "open_sansregular";*/
        font-family: 'Roboto', "arial";
        /* font-family: "arial"; */
        background:  #f9f9f9;
        color: #73879C;

    }

    .panel-left { 
        background: /*#f4f4f4*/#2A3F54; 

        /*#2c3334*/


        color: #fff;
        /*border-right: 1px solid #ddd; */
    }

    .panel-right {
        padding:0px;
    }

    .padding {
        padding-right: 10px;
        padding-left: 10px;
    }

    .bg-default {
        color: #000;
        background-color: #ddd;
    }

    .panel {
        margin:2px;

    }

    .panel-body {
        padding:10px;
    }
    .alert-default {
        color: #777;
        background-color: #e5e5e5;
        border-color: #CCC;
    }

    .box {
        font-size:35px;
        font-weight: 600;
    }

    
    #nav-dash a {
        color:#73879C;
    }
    
    .th {
        font-weight: bold;
    }
    /*
    .table tbody>tr>td {
        vertical-align: middle;
    }
    */

</style>



<div class="container-fluid">
    
    <div class="row">

       <div class="col-lg-2 panel-left hidden-xs hidden-sm hidden-md" id="pnl-left" style="z-index:0">
            <?php require_once('app/view/painel/menu.php'); ?>
            <?php require_once('app/view/painel/filtros.php'); ?>
        </div>

        <div class="col-lg-12 panel-mobile  hidden-lg" style="">
            <?php require('app/view/painel/menu.php'); ?>
        </div>
        
        <div class="col-lg-10 panel-right">
            <div class="nav-menu-right">

                <?php require_once('app/view/painel/nav.php'); ?>

            </div>
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            
            

            
            <div class="padding">

                <div class="padding">
                    
                    <div class="row">
                        <div class="col-lg-12 nopad">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                    <form id="formRank" method="post" action="<?php echo appConf::caminho ?>painel/rank">
                                        <input type="hidden" name="filtros" />
                                        
                                        
                                        <div class="row">
                                            <div class="col-lg-11">
                                                
                                                <div class="row">
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Função</label>
                                                        <select class="form-control input-sm" name="cmbFuncao" id="cmbFuncao">
                                                            <option value="">::FUNÇÃO::</option>
                                                            <option value="">(TUDO)</option>
                                                            <?php foreach($perfis as $pilar) { ?>
                                                            <option value="<?php echo $pilar['ID'] ?>"><?php echo $pilar['NOME'] ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                    <?php //if(appFunction::dadoSessao('id_linha') == 0) { ?>
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Linha</label>
                                                        <select class="form-control input-sm" name="cmbLinha" id="cmbLinha">
                                                            <option value="">::LINHA::</option>
                                                            <option value="0">(TUDO)</option>
                                                            <?php foreach($linhas as $pilar) { ?>
                                                            <option value="<?php echo $pilar['ID_LINHA'] ?>"><?php echo $pilar['NOME'].' ('.$pilar['SIGLA'].')' ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                    <?php //} ?>
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Regional</label>
                                                        <select class="form-control input-sm" name="cmbRegional" id="cmbRegional">
                                                            <option value="">::REGIONAL::</option>
                                                            <option value="0">(TUDO)</option>
                                                            <?php foreach($regionais as $pilar) { ?>
                                                            <option value="<?php echo $pilar['COD_REGIONAL'] ?>"><?php echo $pilar['REGIONAL'].'' ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Selecione o Pilar</label>
                                                        <select class="form-control input-sm" name="cmbPilar" id="cmbPilar">
                                                            <option value="">::PILAR::</option>
                                                            <?php foreach($pilares as $pilar) { ?>
                                                            <option value="<?php echo $pilar['PILAR'] ?>"><?php echo $pilar['PILAR'] ?></option>
                                                            <?php } ?>

                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Selecionar o Produto</label>
                                                        <select class="form-control input-sm" name="cmbProduto" id="cmbProduto">
                                                            <option>::PRODUTO::</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-2">
                                                        <label for="exampleInputEmail1">Período</label>
                                                        <select class="form-control input-sm" name="cmbPeriodo" id="cmbPeriodo">
                                                            <option value="">::PERÍODO::</option>
                                                            <?php foreach($periodo as $per) { ?>
                                                            <option value="<?php echo $per['COLUNA'] ?>"><?php echo (isset($head[$per['COLUNA']])) ? $head[$per['COLUNA']] : $per['LABEL'] ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>

                                                    
                                                </div>
                                                
                                                
                                                
                                            </div>
                                            <div class="col-lg-1">
                                                <div clsass="col-lg-1">
                                                        <label for="exampleInputEmail1">&nbsp;</label>
                                                        <button type="button" class="btn btn-primary btn-block btn-sm" id="btn-rank">Gerar</button>
                                                    </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                    
                                    </form>
                                    
                                    
                                    
                                </div>
                            </div>
                        </div>

                    </div>
                    
                    <p></p>
                    
                    
                    
                    <div class="row">
                        
                        <div class="col-lg-12">
                            
                        </div>
                        <div class="col-lg-12 nopad" id="container-rank">
                            
                            
                            <h4 class="text-center text-muted">
                                Selecione os filtros e clique em Gerar
                            </h4>
                            
                            
                        </div>
                    </div>
                    
                </div>
                

            </div>
        </div>

    </div>
</div>



<!--<img src="<?php echo appConf::caminho ?>app/view/painel/img/loading.gif" />-->
<?php include('app/view/footerView.php') ?>
<style>
    label {
        font-size:10px;
    }
</style>

<script>
    var linha = <?php echo appFunction::dadoSessao('id_linha'); ?>;
    $('#btn-rank').unbind().click(function(e){
        var x = '';
       $.each( $('select'), function( key, value ) {
           x +=  $('option:selected', this).text() + '|';
        });
        $('input[name="filtros"]').val(x);
        
        
        
        $.ajax({
            type: "POST",
            url: $('#formRank').attr('action'),
            data: $('#formRank').serialize(),
            beforeSend: function (xhr) {
                modalAguarde('show');
               //$('#container-rank').html("Processando...");      
            },
            success: function(retorno) {
                modalAguarde('hide');
                $('#container-rank').html(retorno);
                resizePanelLeft();
            }
        });
    });
    
    
    $('#cmbLinha').unbind().change(function(e){
        var valor = $(this).val();
        linha = valor;
        
        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>painel/listarPilarRank',
            data: {
                pilar: valor,
                linha: linha
            },
            dataType: 'json',
            beforeSend: function (xhr) {
               $('#cmbPilar').html("::Processando::");         
            },
            success: function(retorno) {
                var listitems = '<option value="">SELECIONE</option>';
                
                 var divProd = '';
                $.each(retorno, function(key, value){
                    listitems = listitems +  '<option value="' + value.ID_PILAR + '">' + value.PILAR + '</option>';
                    divProd = divProd +  '<div class="radio">'+
                                                '<label>'+
                                                  '<input type="radio" name="optProduto" id="optionsRadios1" value="option1">'+value.PRODUTO + '</label>'+
                                            '</div>';
                });
                $('#cmbPilar').html(listitems);
               $('.div-produto').html(divProd);
            }
        });
        
    });
    
    
    $('#cmbPilar').unbind().change(function(e){
        var valor = $(this).val();
        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>painel/listarProdutoRank',
            data: {
                pilar: valor,
                linha: linha
            },
            dataType: 'json',
            beforeSend: function (xhr) {
               $('#cmbProduto').html("::Processando::");   
               $('#cmbPeriodo').html("::Processando::");
            },
            success: function(retorno) {
                var listitems = '<option value="">SELECIONE</option>';
                 
                var divProd = '';
                $.each(retorno.dados, function(key, value){
                    listitems = listitems +  '<option value="' + value.ID_PRODUTO + '">' + value.PRODUTO + '</option>';
                    divProd = divProd +  '<div class="radio">'+
                                                '<label>'+
                                                  '<input type="radio" name="optProduto" id="optionsRadios1" value="option1">'+value.PRODUTO + '</label>'+
                                            '</div>';
                });
                $('#cmbProduto').html(listitems);
                
                //console.log(retorno);
                
                
                var listitems1 = '<option value="">SELECIONE</option>';
                //var divPer = '';
                $.each(retorno.periodo, function(key, value){
                    //alert(value.DESC);
                    listitems1 = listitems1 +  '<option value="' + value.ID + '">' + value.DESC + '</option>';
                });
                $('#cmbPeriodo').html(listitems1);
                
                
                
            }
        });
    });

    $('.progress .progress-bar').css("width", function () {
        return $(this).attr("aria-valuenow") + "%";
    });


    $('.colpse').hide();

    $('.toggle').unbind().click(function(e){
        e.preventDefault();
        var c = $(this).data('val');
        $('.'+c).toggle( "fast", function() {
        // Animation complete.
      });
    });


$('.count-up').each(function( key, item ) {

        var options = {
            useEasing:   true,
            useGrouping: (!$(this).data('group')) ? true : $(this).data('group'), 
            separator:   $(this).data('separator'), 
            decimal:     $(this).data('decimal'), 
            suffix:      $(this).data('suffix')
        };


       countUp = new CountUp(
               $(this).attr('id'),
               $(this).data('start'),
               $(this).data('end'),
               $(this).data('num-decimal'),
               $(this).data('duration'), options
            );
       countUp.start();
       
});




    </script>
