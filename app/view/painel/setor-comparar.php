<?php

require_once('functions.php');

?>
<style>
    
    h3 {
        margin: 0px;
    }
    
</style>
<?php
$a[0] = 'danger';
$a[1] = 'danger';
$a[2] = 'warning';
$a[3] = 'success';
$a[4] = 'primary';
$a[5] = 'default';
?>

<div  id="colab<?php echo $container ?>" class="myAffix" data-spy="affix" data-offset-top="60" data-offset-bottom="200">
    <div style="background-color: #fff;">
<a href="#" class="btn-sel-colab" data-container="<?php echo $container ?>">
    <img height="100"  class="center-block" style="border-radius: 10px !important;" src="<?php echo fotoPerfil($colab['FOTO']) ?>" />
</a>

<div><b><?php echo shortName($colab['COLABORADOR']) ?></b></div>
<div class="text-muted"><small><?php echo $colab['FUNCAO'] ?> (<?php echo $colab['LINHA'] ?>)</small></div>
<div class="text-muted"><small><b>Data de Início: </b><?php echo appFunction::formatarData($colab['DT_INICIO']) ?></small></div>
<p></p>
<a class="btn btn-primary btn-block btn-xs" href="<?php echo appConf::caminho ?>painel/abrirPainel/<?php echo base64_encode($setor) ?>" target="_blank">Abrir Painel</a>
    </div>
    
</div>
<hr>

<div class="well well-sm"><b><span class="glyphicon glyphicon-stats"></span> Ranking</b></div>
<small class="text-muted"><?php echo $dados['RANKING']['LABEL_BRASIL'] ?></small>
<h3>
    <b><span class="count-up" data-start="0" data-end="<?php echo ($dados['RANKING']['COUNT_REGIONAL'] != 1) ? $dados['RANKING']['RNK_BRASIL'] :  'N/A' ?>" data-num-decimal="0" data-duration="3" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="º/<?php echo $dados['RANKING']['COUNT_BRASIL'] ?>" id="mkt<?php echo rand() ?>"><?php echo ($dados['RANKING']['COUNT_REGIONAL'] != 1) ? $dados['RANKING']['RNK_BRASIL'] :  'N/A' ?></span></b>    
</h3>

<hr>

<small class="text-muted"><?php echo $dados['RANKING']['LABEL_REGIONAL'] ?></small>
<h3>
    <b><span class="count-up" data-start="0" data-end="<?php echo ($dados['RANKING']['COUNT_REGIONAL'] != 1) ? $dados['RANKING']['RNK_REGIONAL'] :  'N/A' ?>" data-num-decimal="0" data-duration="3" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="º/<?php echo $dados['RANKING']['COUNT_REGIONAL'] ?>" id="mkt<?php echo rand() ?>"><?php echo ($dados['RANKING']['COUNT_REGIONAL'] != 1) ? $dados['RANKING']['RNK_BRASIL'] :  'N/A' ?></span></b>    
</h3>

<hr>

<!--
<div class="well well-sm"><b><span class="glyphicon glyphicon-stats"></span> Pontuação</b></div>
<?php foreach ($dados['PONTUACAO'] as $dado) { ?>
<?php $val = rand(1,110) ?>
<small class="text-muted"><?php echo $dado['PILAR'] ?></small>
<div class="progress center-block" style="width: 70%">
<div class="progress-bar progress-bar-<?php echo $a[$dado['PONTO']] ?> progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $dado['M00'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    <span class="progress-bar-title progress-bar-title-<?php echo ($dado['M00'] <= 50) ? 'black' : 'white' ?>">
        <?php echo appFunction::formatarMoeda($dado['M00'],1) ?>%
    </span>
</div>
</div>


<hr>
<?php } ?>
<SPAN></SPAN>-->




<div class="well well-sm"><b><span class="glyphicon glyphicon-stats"></span> Pontuação</b></div>
<?php foreach ($dados['PILAR_COMPARAR'] as $pilar) { ?>

<?php foreach ($dados['PONTUACAO'] as $dado) { 
    
    if($pilar['PILAR'] == $dado['PILAR']) { 
        break;        
    } else {
        $dado['PONTO'] = 5;
        $dado['M00'] = 0;
    }
    
    
}
?>

<?php $val = rand(1,110) ?>
<small class="text-muted"><?php echo $pilar['PILAR'] ?></small>

<div class="progress center-block" style="width: 70%">
<div class="progress-bar progress-bar-<?php echo $a[$dado['PONTO']] ?> progress-bar-striped" role="progressbar" aria-valuenow="<?php echo $dado['M00'] ?>" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
    <span class="progress-bar-title progress-bar-title-<?php echo ($dado['M00'] <= 50) ? 'black' : 'white' ?>">
        <?php echo appFunction::formatarMoeda($dado['M00'],$dado['DECIMAL']).$dado['SUFIXO'] ?>
    </span>
</div>
</div>
<hr>
<?php } ?>


<?php //} ?>


<!--
<div class="well well-sm"><b><span class="glyphicon glyphicon-stats"></span> Pilares</b></div>
<?php foreach ($dados['PONTUACAO'] as $dado) { ?>

<small class="text-muted"><?php echo $dado['PILAR'] ?></small>
<h3>
    <b><span class="count-up" data-start="0" data-end="<?php echo $dado['M00'] ?>" data-num-decimal="1" data-duration="3" data-easing="true" data-group="true" data-separator="." data-decimal="," data-suffix="%" id="mkt<?php echo rand() ?>">10</span></b>    
</h3>


<hr>
<?php } ?>
<SPAN></SPAN>-->

<script>
    
$('.progress .progress-bar').css("width", function () {
    return $(this).attr("aria-valuenow") + "%";
});  

//$('.progress .progress-bar .progress-bar-title').html(function () {
    //return $(this).closest(".progress-bar").attr("aria-valuenow") + "%";
//});

/* arquivo function.js */
startCount('#<?php echo $container ?> .count-up');

</script>