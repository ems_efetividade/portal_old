<?php 
    require_once('functions.php');

    $icon[0] = 'glyphicon glyphicon-circle-arrow-down';
    $icon[1] = 'glyphicon glyphicon-circle-arrow-up';
    $icon[2] = 'glyphicon glyphicon-circle-arrow-right';
?>


<div class="row">
    <div class="col-lg-6">
        <h4 class="text-muted" ><b>Indicadores de <span id="tit-tabela"></span></b></h4>
    </div>
    <div class="col-lg-6">
        <?php if($equipe['dados']) { ?>
        <!-- Nav tabs -->
        <ul class="nav nav-pills pull-right" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                    <small> 
                        <span class="glyphicon glyphicon-tag" aria-hidden="true"></span>
                        Produtos
                    </small>
                </a>
            </li>
            <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                <small>
                <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                Equipe</small></a>
            </li>
        </ul>
        <?php } ?>
    </div>
</div>


<div class="table-responsive">

    

    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">

            <table class="table table-condensed table-striped table-bordered table-hover" style="font-size: 11px;">
        
                <tr class="active">
                    <?php foreach($headers as $key => $col) { ?>
                        <th class="<?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?>">
                            <?php echo (isset($cab[$col['COLUNA']])) ? $cab[$col['COLUNA']] : $col['LABEL'] ?>
                        </th>
                    <?php } ?>
                </tr>
                
                <?php foreach($dados as $dado) { ?>
                <tr >
                    <?php foreach($headers as $key => $col) { ?>
                        <td class="c-<?php echo $dado['E_'.$col['COLUNA']] ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?> <?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?>">
                            <span class="pull-left <?php echo ($col['ICONE'] == 1 && $dado[$col['COLUNA']] != '') ? $icon[$dado['E_'.$col['COLUNA']]]  :  '' ?>"></span>

                            <?php echo ($col['NUMERICO'] == 1) ? appFunction::formatarMoeda($dado[$col['COLUNA']],$dado['DECIMAL']) :$dado[$col['COLUNA']] ?>
                            
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </table>
            
        
        </div>

        <?php if($equipe['dados']) { ?>
        <div role="tabpanel" class="tab-pane" id="profile">
        
            <table class="table table-condensed table-striped table-bordered table-hover" style="font-size: 11px;">
        
                <tr class="active">
                    <?php foreach($headers as $key => $col) { ?>
                        <th class="<?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?>">
                            <?php echo (isset($cab[$col['COLUNA']])) ? $cab[$col['COLUNA']] : $col['LABEL'] ?>
                        </th>
                    <?php } ?>
                </tr>
                
                <?php foreach($equipe['dados'] as $dado) { ?>
                <tr style="cursor:pointer" onClick="location.href = '<?php echo  appConf::caminho ?>painel/abrirPainel/<?php echo base64_encode($dado['SETOR']) ?>'">
                    <?php foreach($headers as $key => $col) { ?>
                        <td class="c-<?php echo $dado['E_'.$col['COLUNA']] ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?> <?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?>">
                            <span class="pull-left <?php echo ($col['ICONE'] == 1 && $dado[$col['COLUNA']] != '') ? $icon[$dado['E_'.$col['COLUNA']]]  :  '' ?>"></span>
                            <?php echo ($col['NUMERICO'] == 1) ? appFunction::formatarMoeda($dado[$col['COLUNA']],$dado['DECIMAL']) :$dado[$col['COLUNA']] ?>
                            
                        </td>
                    <?php } ?>
                </tr>
            <?php } ?>
            </table>
        
        </div>
        <?php } ?>
    </div>

</div>




