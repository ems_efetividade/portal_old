<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/painelImprimir.css" rel="stylesheet" type="text/css" />

<body>

<div class="report-header">
	<div class="titulo">Painel EMS Prescrição<br><small><?php echo $mesDemanda ?> - Efetividade EMS</small></div>
    <div class="linha"></div>
</div>

<div class="row">

	<div class="col-50 float-left">
    	<div class="header">Colaborador</div>
		<div class="body"><?php echo @$dadosColaborador; ?></div>
    </div>
    
    <div class="col-50">
    	<div class="header">
        	<div class="float-left" style="width:50%;">Pontuação</div>
            <div style="width:50%;text-align:center">Pontuação</div>
        </div>
        
        <div class="body">
            <div class="row pontuacao">
                <div class="col-50 float-left"><?php echo @$pontuacao; ?></div>
                <div style="text-align:center"><img src="plugins/exporting-server/files/<?php echo $graficoPontuacao ?>" width="135" height="87"  border="0"  /></div>
            </div>
        </div>
        
        <div class="row mktshare">
        
        	<div class="header">
            	<div style="width:40%" class="float-left">Demanda R$ <small>(<?php echo $mesDemanda ?>)</small></div>
                <div style="width:33%;text-align:center" class="float-left">Mkt Share <small>(<img  width="8" height="8" src="<?php echo appConf::caminho ?>public/img/flag_brazil.png" /> <?php echo $dadosShareBrasil; ?>%)</small></div>
                <div style="width:27%; text-align:center">Ranking</div>  
            </div>
            
            <div class="body">
                <div style="width:40%" class="float-left"><?php echo $mercado ?></div>
                <div style="width:32%" class="float-left"><?php echo $mktShare ?></div>
                <div style="width:28%"><?php echo $ranking ?></div>
            </div>

        </div>
        
    </div>

</div>

<div class="row">
	<div class="col">
    	<div class="header">Evolução dos Pilares</div>
        <img src="plugins/exporting-server/files/<?php echo $graficoEvolucao ?>" width="100%" />
    </div>
</div>

<div class="row">
	<div class="col">
    	<div class="header"><table id="titulo-pilares"><?php echo $cabecalhoMes ?></table></div>
        <div class="body"><?php echo $produtividade ?></div>
    </div>
</div>

</body>

