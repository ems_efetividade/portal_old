<?php
include('headerView.php');
$perfil = appFunction::dadoSessao('perfil');


?>

<style>

    body { padding-top: 70px; }
    .container { width:100%; }
    .container .panel {margin-bottom:4px }
    .container .panel-body { padding:7px; }
    .container .panel-heading {padding:5px !important}
    .container .panel-heading h5 {padding:0px !important; margin-top:3px;margin-bottom:3px;}
    h5 { margin:6px }


    @media (min-width: 1200px) {
        .right-2px { padding-right:2px;  }
        .left-2px { padding-left:2px;  }
    }

    option{
        padding: 81px;
    }

</style>
<script type="text/javascript" src="https://canvg.github.io/canvg/rgbcolor.js"></script> 
<script type="text/javascript" src="https://canvg.github.io/canvg/StackBlur.js"></script>
<script type="text/javascript" src="https://canvg.github.io/canvg/canvg.js"></script> 

<script src="<?php echo appConf::caminho ?>public/js/painel.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/exporting.src.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/highchart/js/modules/solid-gauge.js"></script>
<script>
    $(function (e) {
        graficoProdutividade('<?php echo appConf::caminho ?>painel/cabecalhoGrafico', '<?php echo appConf::caminho ?>painel/graficoProdutividade/<?php echo $setor; ?>', '<?php echo appConf::caminho ?>');
                graficoGauge();

                hPainelGrafico = $('#painel-grafico-pilares').height();
                hPainelColaborador = $('#painel-colaborador').height();
                hPainelDemanda = $('#painel-demanda').height();

                hPadrao = (hPainelDemanda + (hPainelGrafico - hPainelColaborador - hPainelDemanda)) - 6;

                $('#painel-demanda').height(hPadrao);
                $('#painel-pontuacao').height(hPadrao);

            });
            $(document).ready(function (e) {

                if (<?php echo $setor ?> == 0) {
                    $('#modalSetor').modal('show');
                }

                $('#modalDestaque').on('shown.bs.modal', function (event) {

                    $('.imprimir-avaliacao').click(function (e) {
                        var idAval = $(this).data('value');
                        
                        //alert(idAval);
                        
                        
                        $.ajax({
                            url: '<?php echo appConf::caminho ?>sistemas/PMA/avaliacao/grafico/'+idAval,
                            
                            success: function (retorno) {
                                
                                
                                
                                $('#iii').html(retorno);
                                //alert(retorno.('runscript').html());
                                eval(document.getElementById("runscript").innerHTML);
                                //eval(document.getElementById("runscript").innerHTML);
                                //console.log(retorno);
                                //alert(retorno.closest('body'));
                            }
                        });
                        
                        
                        
                        /*
                        modalAguarde('show');
                        nomeArquivo = $(this).attr('arquivo');

                        var url = cam + 'imprimir_avaliacao.php?id_avaliacao=' + $(this).attr('value');

                        $.ajax({
                            url: url,
                            success: function (retorno) {

                                $("#imprimirAvaliacaoPMA input[name='html']").val(retorno);
                                $("#imprimirAvaliacaoPMA input[name='nomeArquivo']").val(nomeArquivo);
                                $("#imprimirAvaliacaoPMA").submit();
                                setTimeout(function () {
                                    modalAguarde('hide');
                                }, 2000);
                            }
                        });
                        */
                    });
                });


                $('#tooltip').popover({
                    html: 'true'
                });

                $('#tb-dados-pilares .filho').hide();
                $('#tb-dados-pilares .pai').click(function (e) {
                    $(this).nextUntil(".pai").toggle(500);
                });

                combo();

                $('#btn-pdf').click(function (e) {
                    modalAguarde('show');

                    setTimeout(function () {

                        var chart = '';
                        var graficoEvolucao = '';

                        var chart = $('#evolucao').highcharts();
                        var chart1 = $('#container-speed').highcharts();

                        var graficoEvolucao  = exportarGraficoLinha(chart, 700, 350, '<?php echo appConf::caminho ?>');
                        var graficoPontuacao = exportarGraficoLinha(chart1, 150, 150, '<?php echo appConf::caminho ?>');

                        $('#graficoEvolucao').val(graficoEvolucao);
                        $('#graficoPontuacao').val(graficoPontuacao);

                        //alert(a);
                        //test1(chart2, 700, 700);
                        //test1(chart, 700, 350);
                        //event.preventDefault();
                        $("#imprimirPainel").submit();
                        setTimeout(function () {
                            modalAguarde('hide');
                        }, 5000);
                    }, 500);
                });
            });

            function combo() {
                $('.comboColaborador').change(function (e) {
                    container = '#' + $(this).closest('select').attr('container');
                    controller = '<?php echo appConf::caminho ?>painel/comboSetor/' + $(this).val() + '/1';
                    retorno = requisicaoAjax(controller);

                    $(container).html(retorno);
                    $('#txtSetor').val($(this).val());
                    combo();
                    $(this).find('option').css("height", "20px");
                });
            }
            function graficoGauge() {
                var valor_gauge = parseFloat(requisicaoAjax('<?php echo appConf::caminho ?>painel/graficoPontuacao/<?php echo $setor; ?>'));

                        // grfico gauge
                        var gaugeOptions = {
                            exporting: {
                                url: '<?php echo appConf::caminho ?>plugins/exporting-server/index.php',
                                //url: '<?php echo appConf::caminho ?>/painel/exportarGrafico',
                                filename: 'vai_jefff',
                                sourceWidth: 100,
                                sourceHeight: 100,
                                type: 'image/png',
                                enabled: false
                            },
                            chart: {
                                type: 'solidgauge',
                                margin: [0, 0, 0, 0],
                                spacing: [0, 0, 0, 0],
                                plotBackgroundColor: null,
                                plotBackgroundImage: null,
                                plotBorderWidth: 0,
                            },
                            title: {
                                text: ''
                            },
                            pane: {
                                startAngle: -90,
                                endAngle: 90,
                                size: '150%',
                                center: ['50%', '90%'],
                                //size: '100%',
                                //startAngle: -90,
                                //endAngle: 90,
                                background: {
                                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                    innerRadius: '60%',
                                    outerRadius: '100%',
                                    shape: 'arc'
                                }
                            },
                            tooltip: {
                                enabled: true
                            },
                            exporting: {
                                type: 'image/png',
                                enabled: false,
                                chartOptions: {
                                    solidgauge: {
                                        dataLabels: {
                                            y: 5,
                                            useHTML: true,
                                            style: {
                                                fontSize: '95px',
                                            },
                                        }
                                    }
                                }
                            },
                            // the value axis
                            yAxis: {
                                stops: [
                                    [0.0, '#DF5353'], // red
                                    [0.25, '#DF5353'], //red
                                    //[0.26, '#DDDF0D'], // yellow
                                    [0.50, '#DDDF0D'], // yellow
                                    //[0.51, '#55BF3B'], // green
                                    [0.75, '#55BF3B'], // green
                                    //[0.76, '#3B5998'], // blue
                                    [1, '#3B5998'] // blue
                                ],
                                lineWidth: 0,
                                minorTickInterval: 2,
                                tickInterval: 2,
                                tickWidth: 1,
                                title: {
                                    y: -70
                                },
                                labels: {
                                    y: 16,
                                    enabled: false
                                }
                            },
                            plotOptions: {
                                solidgauge: {
                                    dataLabels: {
                                        y: 5,
                                        borderWidth: 0,
                                        useHTML: true,
                                        style: {
                                            fontSize: '35px',
                                        },
                                    }
                                }
                            }
                        };

                        $('#pontuacao').highcharts(Highcharts.merge(gaugeOptions, {
                            yAxis: {
                                min: 0,
                                max: 4,
                                title: {
                                    text: ''
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                    name: 'Pontuação',
                                    data: [0],
                                    dataLabels: {
                                        format: '<div style="text-align:center"><span style="font-size:15px;color:' +
                                            ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                                            '<span style="font-size:12px;color:silver"></span></div>'
                                    }
                                }]
                        }));

                        // The speed gauge
                        $('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
                            yAxis: {
                                min: 0,
                                max: 4,
                                title: {
                                    text: ''
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                    name: 'Speed',
                                    data: [valor_gauge],
                                    dataLabels: {
                                        format: '<div style="text-align:center;"><span style="font-size:35px;color:' +
                                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>'
                                    },
                                    tooltip: {
                                        valueSuffix: ''
                                    }
                                }]

                        }));

                        //var valor_gauge = parseFloat(3);
                        setTimeout(function () {
                            var chartt = $('#pontuacao').highcharts();
                            var point = chartt.series[0].points[0];

                            point.update(valor_gauge, true, {
                                duration: 6,
                                easing: "linear"
                            });

                        }, 1);
                    }

                    function test1(chart, w, h) {

                        var nome_arquivo = '';
                        var rdata = '';

                        var d = new Date();
                        var n = d.getTime();
                        nome_arquivo = n.toString();

                        var obj = {}, chart;
                        obj.svg = chart.getSVG();
                        obj.type = 'image/png';
                        obj.width = w;
                        obj.scale = 2;
                        obj.async = true;
                        obj.filename = nome_arquivo;
                        //obj.sourceWidth =  w;
                        //obj.sourceHeight = h;

                        exportUrl = '<?php echo appConf::caminho ?>plugins/exporting-server/index.php';

                        $.ajax({
                            type: "POST",
                            url: exportUrl,
                            data: obj,
                            cache: false,
                            async: false,
                            crossDomain: true,
                            success: function (data) {
                                rdata = data;
                            },
                            error: function (data) {

                                //alert(data);
                                //alert("----"+data.status);
                                //alert(data.statusText);
                            }
                        });

                        var obj = {}
                        return rdata;
                    }



                    function exportar_gauge(valor) {

                        var gaugeOptions = {
                            chart: {
                                type: 'solidgauge',
                                margin: [0, 0, 0, 0]
                            },
                            title: {
                                text: ''
                            },
                            pane: {
                                center: ['50%', '85%'],
                                size: '140%',
                                startAngle: -90,
                                endAngle: 90,
                                background: {
                                    backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                                    innerRadius: '60%',
                                    outerRadius: '100%',
                                    shape: 'arc'
                                }
                            },
                            tooltip: {
                                enabled: false
                            },
                            exporting: {
                                type: 'image/png',
                                enabled: true,
                            },
                            // the value axis
                            yAxis: {
                                stops: [
                                    [0.0, '#DF5353'], // red
                                    [0.25, '#DF5353'], //red
                                    //[0.26, '#DDDF0D'], // yellow
                                    [0.50, '#DDDF0D'], // yellow
                                    //[0.51, '#55BF3B'], // green
                                    [0.75, '#55BF3B'], // green
                                    //[0.76, '#3B5998'], // blue
                                    [1, '#3B5998'] // blue
                                ],
                                lineWidth: 0,
                                minorTickInterval: 2,
                                tickInterval: 2,
                                tickWidth: 1,
                                title: {
                                    y: -70
                                },
                                labels: {
                                    y: 16,
                                    enabled: false
                                }
                            },
                            plotOptions: {
                                solidgauge: {
                                    dataLabels: {
                                        y: 60,
                                        borderWidth: 0,
                                        useHTML: true,
                                    }
                                }
                            }
                        };

                        // The speed gauge
                        $('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
                            yAxis: {
                                min: 0,
                                max: 4,
                                title: {
                                    text: ''
                                }
                            },
                            credits: {
                                enabled: false
                            },
                            series: [{
                                    name: 'Speed',
                                    data: [valor],
                                    dataLabels: {
                                        format: '<div style="text-align:center"><span style="font-size:45px;color:' +
                                                ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>'
                                    },
                                    tooltip: {
                                        valueSuffix: ''
                                    }
                                }]

                        }));

                    }
</script>
<div id="iii" style="display: none"></div>
<canvas id="canvas" width="1000px" height="600px" style="display: none"></canvas> 
<?php if ($setor != 0) { ?>

    <form id="imprimirAvaliacaoPMA" method="POST" action="<?php echo appConf::caminho ?>painel/imprimirAvaliacaoPMA">
        <input type="hidden" name="html" value="" />
        <input type="hidden" name="nomeArquivo" value="" />
    </form>



    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-xs-6"><p class="lead"><span class="glyphicon glyphicon-stats"></span> Painel EMS Prescrição</p></div>
            <div class="col-lg-6 col-xs-6">
                <ul class="nav nav-pills pull-right" id="nav-sub">
                    <li>
                        <button id="sel-colab" log="dashboard-selecionar-colaborador" type="button" class="btn btn-warning dropdown-toggle" title="Selecionar Setor" data-toggle="modal" data-target="#modalSetor"><span class="glyphicon glyphicon-user"></span></button>
                        <button id="btn-destaque" type="button" log="dashboard-destaque" class="btn btn-warning dropdown-toggle" title="Avaliações PMA" data-toggle="modal" data-target="#modalDestaque"><span class="glyphicon glyphicon-list-alt"></span></button>                        
                        <button id="btn-pdf" type="button" log="dashboard-exportar" class="btn btn-warning dropdown-toggle" title="Exportar Dashboard"><span class="glyphicon glyphicon-floppy-save"></span></button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-5">

                <div class="row">
                    <div class="col-lg-12 col-sm-12 right-2px">
                        <div class="panel panel-default" id="painel-colaborador">
                            <div class="panel-heading"><strong>Colaborador</strong></div>
                            <div class="panel-body">
                                <?php echo $dadosColaborador; ?>
                            </div>
                        </div>
                    </div>


                    <!--  </div>
                      
                      <div class="row">-->
                    <div class="col-lg-8 col-sm-12 right-2px">
                        <div class="panel panel-default" id="painel-pontuacao">
                            <div class="panel-heading"><strong>Pontuação</strong></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-7 col-xs-7">
                                        <?php echo $pontuacao ?>
                                    </div>


                                    <div class="col-lg-5 col-xs-5">
                                        <h5><small><strong><p class="text-center">Pontuação Final</p></strong></small></h5>
                                        <div id="pontuacao" style="height: 70px;width:100%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-sm-12 right-2px left-2px">
                        <div class="panel panel-default" id="painel-demanda">
                            <div class="panel-heading"><h5><strong>Demanda R$ </strong><small>(<?php echo $mesDemanda ?>)</small></h5></div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12">
                                    <?php echo $mercado ?>
                                    <?php echo $mktShare ?>
                                    <?php echo $ranking ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="col-lg-7 left-2px">

                <div class="panel panel-default" id="painel-grafico-pilares">
                    <div class="panel-heading"><strong>Gráfico</strong></div>
                    <div class="panel-body">
                        <div id="evolucao" style="width:100%;height:300px;"></div>
                    </div>
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">

                <div class="panel panel-default">
                    <div class="panel-heading"><strong>Pilares de Produtividade</strong></div>
                    <div class="panel-body">
                        <form id="imprimirPainel" method="POST" action="<?php echo appConf::caminho ?>painel/imprimirPainel">
                            <input type="hidden" name="setor" value="<?php echo $setor; ?>" />
                            <input type="hidden" id="graficoEvolucao" name="graficoEvolucao" value="" />
                            <input type="hidden" id="graficoPontuacao" name="graficoPontuacao" value="" />
                            <?php echo $produtividade; ?>
                        </form>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <?php
    } else {
        echo '<div style="height:600px;"></div>';
    }
    ?>


<!--MSGBOX DESTAQUE-->
<div class="modal fade" id="modalDestaque" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">

            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Avaliações PMA</h4>
            </div>

            <div class="modal-body" >
                <?php if ($avaliacaoPMA == "") { ?>
                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-2">
                            <img class="img-responsive" src="<?php echo appConf::caminho ?>/public/img/alert.png">
                        </div>
                        <div class="col-lg-6"><br /><br />
                            <h4 class="text-center text-danger">Não existe avaliações para este colaborador.</h4>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
            <?php } else { ?>
                <?php echo $avaliacaoPMA; ?>	 
            <?php } ?>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="fechar-selecionar"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
            </div>
        </div>
    </div>
</div>



<!--MSGBOX SELECIONAR USUARIO-->
<div class="modal fade" id="modalSetor" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Selecionar colaborador</h4>
            </div>

            <div class="modal-body" >
<?php echo $comboSetor; ?>
            </div>

            <div class="modal-footer">
                <form id="formAbrirPainel" name="formAbrirPainel" method="post" action="<?php echo appConf::caminho ?>painel/abrirPainel">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="fechar-selecionar"><span class="glyphicon glyphicon-remove"></span> Fechar</button>

                    <input type="hidden" id="txtSetor" name="setor" value="<?php appFunction::dadoSessao('setor') ?>" />

                    <button type="submit" class="btn btn-primary primary" data-dismiss="modasl" id="selecionar"><span class="glyphicon glyphicon-ok"></span> Selecionar</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div id="container-speed" style="width: 300px; height: 200px; float: left; display:none"></div>

<?php include('footerView.php') ?>