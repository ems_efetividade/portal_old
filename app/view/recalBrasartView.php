<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 24/05/2019
 * Time: 10:22
 */

require_once('/../model/mRecalBrasart.php');

class recalBrasartView
{

    public function recalBrasartView($id)
    {
        $dados;
        $model = new mRecalBrasart();
        $termos = $model->listtermoByIdColaborador($id);
        $colaborador = $model->dadosCPFColaborador($id);
        if(is_array($termos)) {
            foreach ($termos as $termo) {
                $dados[$termo['APRESENTACAO']][] = $termo;
            }
        }
        if (is_array($termos[1])) {
            $this->termo($dados, $termos, $colaborador);
            exit;
        }
    }

    public function termo($dados, $termos, $colaborador)
    {

        ?>
        <div class="container">
            <div class="row">
                <div class="col-sx-12">
                    <h3 class="text-center">
                        <br>
                        RELATÓRIO DE MONITORAÇÃO DAS DISTRIBUIDORAS PARA OS DETENTORES DE REGISTRO
                        <br>
                        <small><strong>ANEXO V RDC 55/2015</strong></small>
                        <br>
                        <strong>URGENTE:</strong> Preenchimento anexo recolhimento Brasart para devolução imediata
                    </h3>
                    <br>
                    <div class="alert alert-danger alert-dismissible text-center" role="alert">
                        Prezado(a) colaborador, em atendimento a Resolução RDC 55/2005 favor preencher os dados do anexo
                        abaixo de forma imediata.<br> Após preenchimento clicar em <strong>'Enviar Termo'</strong>.<br>
                        Você terá que preencher um termpo para cada apresentação do produto que você recebeu.
                    </div>
                    <br>
                    <a download href="https://efetividade.gruponc.net.br/public/relatorios/155913798854773498.pdf" target="_blank">
                    <h4 class="text-primary"> Instruções de Preenchimento <span class="glyphicon glyphicon-download-alt"></span></h4>
                    </a>
                    <br>
                    <?php
                    $j = 0;
                    foreach ($dados as $termo) {
                        if ($j > 0)
                            continue;
                        $j++;
                        ?>
                        <div id="termo">
                            <table border="1" cellpadding="1" cellspacing="1" style="width:100%; font-size:20px;">
                                <tbody>
                                <tr>
                                    <td colspan="2">
                                        <strong>Raz&atilde;o Social: </strong><?php echo $termo[0]['NOME'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Nome fantasia: </strong>N/A
                                    </td>
                                    <td>
                                        <strong>CNPJ: </strong><?php echo $termo[0]['CPF'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Ramo de Atividade: </strong>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Farmac&ecirc;utico Respons&aacute;vel n&deg;. CRF: </strong>N/A
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Endere&ccedil;o: </strong><?php echo $termo[0]['ENDERECO'] ?>
                                    </td>
                                    <td>
                                        <strong>Bairro: </strong> <?php echo $termo[0]['BAIRRO'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Cidade: </strong><?php echo $termo[0]['CIDADE'] ?>
                                    </td>
                                    <td>
                                        <strong>Estado: </strong><?php echo $termo[0]['ESTADO'] ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>CEP: </strong><?php echo $termo[0]['CEP'] ?>
                                    </td>
                                    <td>
                                        <strong>Telefone: </strong>
                                        <?php
                                        if ($colaborador[1]['F_PARTICULAR'] != '')
                                            echo "(" . $colaborador[1]['DDD_PARTICULAR'] . ")" . $colaborador[1]['F_PARTICULAR'];
                                        if (($colaborador[1]['F_PARTICULAR'] != '') && ($colaborador[1]['F_PARTICULAR'] != ''))
                                            echo ' - ';
                                        if ($colaborador[1]['F_PARTICULAR'] != '')
                                            echo "(" . $colaborador[0]['F_CORPORATIVO'] . ")" . $colaborador[1]['F_CORPORATIVO'];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Endere&ccedil;o
                                            Eletr&ocirc;nico: </strong><?php echo $colaborador[1]['EMAIL'] ?>
                                    </td>
                                    <td>
                                        <strong>Inscri&ccedil;&atilde;o estadual: </strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>Produto: </strong>
                                        <?php
                                        $prod = explode("MG", $termo[0]['APRESENTACAO']);
                                        if (count($prod) <= 2) {
                                            echo $prod[0] . "MG";
                                        } else if (count($prod) > 2) {
                                            for ($i = 0; $i < count($prod) - 1; $i++) {
                                                echo $prod[$i] . "MG";
                                            }
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <strong>LOTE: </strong>
                                        <?php
                                        $lotes = '';
                                        $arrayLotes = '';
                                        foreach ($termo as $lote) {
                                            $arrayLotes[] = $lote['ID'];
                                            $lotes[$lote['LOTE']] += $lote['QUANTIDADE'];
                                        }
                                        $k = 0;
                                        foreach ($lotes as $key => $lote) {
                                            if ($k > 0)
                                                echo ' - ';
                                            echo $key;
                                            $k++;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>

                                    <td>
                                        <strong>N&deg;. Registro: </strong><?php echo $termo[0]['REGISTRO']?>
                                    </td>
                                    <td>
                                        <strong>Apresenta&ccedil;&atilde;o: </strong>
                                        <?php
                                        $pos = count($prod);
                                        echo $prod[$pos - 1];
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <?php
                                    ?>
                                    <input type="hidden" id="arrayLotes" value="<?php
                                    $m = 0;
                                    foreach ($arrayLotes as $value) {
                                        if ($m > 0)
                                            echo '-';
                                        echo $value;
                                        $m++;
                                    } ?>"/>
                                    <td colspan="2">
                                        <strong>Nota Fiscal: </strong>
                                        <?php
                                        $notas = '';
                                        foreach ($termo as $nota) {
                                            $notas[$nota['NOTA_FISCAL']] = "";
                                        }
                                        $k = 0;
                                        foreach ($notas as $key => $nota) {
                                            if ($k > 0)
                                                echo ' - ';
                                            echo $key;
                                            $k++;
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Quantidade recebida no estabelecimento receptor: </strong>
                                        <?php
                                        foreach ($lotes as $key => $lote) {
                                            echo '<br>' . $key . ' - <span id="rec' . $key . '"><input class="receptor" onkeypress="return somenteNumeros(event)"  oninput="this.value = Math.abs(this.value)" id="' . $key . '" type="number" disabled value="' . $lote . '"/></span>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Quantidade em estoque no distribuidor: </strong>
                                        <?php
                                        foreach ($lotes as $key => $lote) {
                                            echo '<br>' . $key . ' - <span id="dist' . $key . '"><input class="distribuidor" onkeypress="return somenteNumeros(event)"  oninput="this.value = Math.abs(this.value)" id="' . $key . '" type="number"/></span>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Quantidade distribu&iacute;da aos estabelecimentos receptores </strong>
                                        <?php
                                        foreach ($lotes as $key => $lote) {
                                            echo '<br>' . $key . ' - <span id="est' . $key . '"><input class="estabelecimento" onkeypress="return somenteNumeros(event)" oninput="this.value = Math.abs(this.value)" id="' . $key . '" type="number"/></span>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <script>
                                    function somenteNumeros(e) {
                                        var charCode = e.charCode ? e.charCode : e.keyCode;
                                        // charCode 8 = backspace
                                        // charCode 9 = tab
                                        if (charCode != 8 && charCode != 9) {
                                            // charCode 48 equivale a 0
                                            // charCode 57 equivale a 9
                                            if (charCode < 48 || charCode > 57) {
                                                return false;
                                            }
                                        }
                                    }
                                </script>
                                <tr>
                                    <td colspan="2">
                                        <strong>Assinatura do Respons&aacute;vel T&eacute;cnico: N/A</strong>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <strong>Respons&aacute;vel Legal: </strong><span id="assinatura"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <p>
                                            <strong>Observa&ccedil;&atilde;o: </strong>
                                        </p>
                                        <p>
                                            <strong>*Campos &quot;CNPJ&quot; Informar seu CPF e &quot;Quantidade em
                                                estoque
                                                Distribuidor&quot; a quantidade dispon&iacute;vel em sua resid&ecirc;ncia
                                                para coleta</strong>
                                        </p>
                                        <p>
                                            <strong>**N/A: N&atilde;o se aplica</strong>
                                        </p>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <button onclick="enviarDados()" class="btn btn-success form-control">Enviar Termo</button>
                        <br>
                        <br>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

        <link href="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.js"></script>
        <script type="text/javascript" src="/../../public/js/Notiflix/AIO/notiflix-aio-1.4.0.min.js"></script>
        <script src="/../../public/js/anvisa.js" type="text/javascript"></script>
        <?php
    }

}