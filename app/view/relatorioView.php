<?php
	include('headerView.php');
?>

<style>
body { padding-top: 70px; }
#fileArquivo { display:none }
.progress.active .progress-bar {
  -webkit-animation: reverse progress-bar-stripes 2s linear infinite;
     -moz-animation: reverse progress-bar-stripes 2s linear infinite;
      -ms-animation: reverse progress-bar-stripes 2s linear infinite;
       -o-animation: reverse progress-bar-stripes 2s linear infinite;
          animation: reverse progress-bar-stripes 2s linear infinite;
}

.container { width:100%; }
.breadcrumb {background-color:transparent; margin-bottom:0px; padding:0px;}
.breadcrumb a {color:#428bca}
.nome-diretorio { border-bottom:1px solid #ddd; margin-bottom:10px;padding-bottom:10px;}
a { color:#000; }
a:hover { text-decoration:none;  }
.lista:hover {background-color:#eee;}
.ativo { background:#eee url(<?php echo appConf::caminho ?>public/img/arrow-right.png) no-repeat center right;  }
.menu-padrao .panel { margin-bottom:2px; }
.menu-padrao .panel-heading { padding:5px; }
.dropdown-menu {
	font-size:12px;
}
.modal-vertical-centered {
  transform: translate(0, 50%) !important;
  -ms-transform: translate(0, 50%) !important; /* IE 9 */
  -webkit-transform: translate(0, 50%) !important; /* Safari and Chrome */
}
.modal-header {
   border-top-left-radius: 5px;
   border-top-right-radius: 5px;
}
.alert{ padding:7px; }
.lista-permissao, .lista-permitido {
	overflow:auto;
	padding:5px;
	border:1px solid #ddd;

}

.lista-permissao, #divPreviewPermissao{
	height:350px;
}

.lista-permitido {
	height:400px;
	margin-top:5px;
}

#divPreviewPermissao  {
	overflow:auto;
	padding:5px;
}

iframe {
	width:100%;
	height:350px;
	border:0px;
}

iframe body {
	padding:0;
}

.menu-raiz {
	list-style:none;
	padding:0px;
}

.menu-raiz ul {
	padding:0px;
	padding-left:10px;

}

.menu-raiz ul li {

}

.menu-raiz li {
	list-style:none;
	padding:5px;

}

.folder-open, .folder-close, .no-folder {
	width:16px;
	height:15px;
	border-color:#fff;
	cursor:pointer;
	float:left;
	margin-top:4px;
}

.folder-open {
	background:url(<?php echo appConf::caminho ?>public/img/arrow-right2.png) no-repeat center center;
}

.folder-close {
	background:url(<?php echo appConf::caminho ?>public/img/arrow-down2.png) no-repeat center center;
}

.icone-folder{ float:right; margin-right:2px; }

</style>

<script>

var aguarde;

$(document).ready(function(e) {

	aguarde = requisicaoAjax('<?php echo appConf::caminho ?>relatorio/barraProgresso');

	$('#btnPesquisar').click(function(e) {
         modalAguarde('show');
    });

	$("#formManipularArquivo").submit(function() {
			$.ajax({
			 type: "POST",
			  url: $(this).attr('action'),
			  data: $(this).serialize(),
			  beforeSend: function(){
				   modalAguarde('show');
				},
			  success: function(e) {
				   modalAguarde('hide');
			   }
			})
		});


	$('#btnUpload').click(function(e) {
        $('#fileArquivo').click();

		$('#fileArquivo').change(function(e) {
			modalAguarde('show');
            $('#formUploadFile').submit();
        });
    });


	$('#btnSelecionarTudo').click(function(e) {
        $('.chk').prop('checked', true);
    });

	$('#btnRemoverSelecao').click(function(e) {
        $('.chk').prop('checked', false);
    });

	$('#btnExcluirSelecao').click(function(e) {
        $('#acao').val('excluir');
		$('#modalExcluirArquivo').modal('show');
    });




	$('#btnExcluirConteudo').click(function(e) {
        $('#acao').val('excluirTudo');
		$('#modalExcluirTotalPasta').modal('show');
    });

	$('#btnMoverConteudo').click(function(e) {
        $('#acao').val('moverTudo');
		 var dir = requisicaoAjax('<?php echo appConf::caminho ?>relatorio/raizPastaMover/');
		$('#divSelecionarPasta').html(dir);

		$('.listarSubDiretorio').click(function(e) {
			selecionarSubDiretorio($(this).attr('value'));
        });

		$('#modalSelecionarPasta').modal('show');
    });



	$('#btnMoverSelecao').click(function(e) {
		$('#acao').val('mover');
        var dir = requisicaoAjax('<?php echo appConf::caminho ?>relatorio/raizPastaMover/');
		$('#divSelecionarPasta').html(dir);

		$('.listarSubDiretorio').click(function(e) {
			selecionarSubDiretorio($(this).attr('value'));
        });

		$('#modalSelecionarPasta').modal('show');
    });


	$('#btnExcluirArquivo, #btnExcluirTotalPasta, #btnMoverArquivos, #btnMoverTudo').click(function(e) {
		$("#formManipularArquivo").submit();
    });

	$('#modalVisualizarPermissao').on('show.bs.modal', function (event) {

		$.ajax({
			type: 'POST',
			url: '<?php echo appConf::caminho ?>relatorio/visualizarPermissao',

			data: {
				permissao:getPerfil()
			},

			beforeSend: function() {
				modalAguarde('show');
			},
			complete: function(){
				modalAguarde('hide');
			},

			success: function(retorno){
				modalAguarde('hide');
				$('#divPreviewPermissao').html(retorno);
			}
		});

	});


	$('#modalEditarPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal

		var idDiretorio = button.data('id')
		var idRaiz = button.data('raiz') // Extract info from data-* attributes

		diretorio = $.parseJSON(requisicaoAjax('<?php echo appConf::caminho ?>relatorio/selecionarDiretorio/'+idDiretorio+'/'+idRaiz));

		$('#txtNomeDiretorio').val(diretorio.pasta);
		$('#txtIdDiretorio').val(diretorio.idDiretorio);
		$('#txtIdPai').val(diretorio.idRaiz);

	});

	$('#modalEditarArquivo').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idArquivo = button.data('id')

		arquivo = $.parseJSON(requisicaoAjax('<?php echo appConf::caminho ?>relatorio/selecionarArquivo/'+idArquivo));

		$('#txtIdArquivo').val(arquivo.idArquivo);
		$('#txtNomeArquivo').val(arquivo.arquivo);

	});




	$('#modalPermissaoPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var idDiretorio = button.data('id')

		htmlPermissao = requisicaoAjax('<?php echo appConf::caminho ?>relatorio/permissaoDiretorio/'+idDiretorio);

		$('#divPermissao').html(htmlPermissao);

		$('#btnAdicionarPerfil').click(function(e) {
            adicionarPerfil();
        });

		$('#btnRemoverPerfil').click(function(e) {
            removerPerfil();
        });

		$('#txtPesquisarPerfil').keyup(function(e) {
				var val = $(this).val().toUpperCase();
				if(val == "") {
					$(".lista-permissao .check-usuario").closest('.checkbox').show();
				} else {

					$(".lista-permissao .check-usuario").closest('.checkbox').hide();
	                $(".lista-permissao .check-usuario[usuario*='"+val+"'").closest('.checkbox').show();
				}
            });
	});


	$('#modalExcluirPasta').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget); // Button that triggered the modal
		var idDiretorio = button.data('id');
		$('#formExcluirDiretorio #txtIdDiretorio').val(idDiretorio);

	});

	$('#btnSalvarDiretorio').click(function(e) {
		$("#formSalvarDiretorio").submit();
    });

	$("#formSalvarDiretorio").submit(function(event) {
		enviarFormulario($(this), '', '');
		event.preventDefault();
	});



	$('#btnExcluirDiretorio').click(function(e) {
		$("#formExcluirDiretorio").submit();
    });

	$("#formExcluirDiretorio").submit(function(event) {
		enviarFormulario($(this), '', '');
		event.preventDefault();
	});

	$('#btnSalvarArquivo').click(function(e) {
		$("#formSalvarArquivo").submit();
    });

	$("#formSalvarArquivo").submit(function(event) {
		enviarFormulario($(this), '', '');
		event.preventDefault();
	});

	$('#btnSalvarPermissao').click(function(e) {



		$.ajax({
			type: 'POST',
			url: '<?php echo appConf::caminho ?>relatorio/salvarPermissao',

			data: {
				permissao:getPerfil(),
				idPasta:$('#idPastaPermissao').val()
			},

			beforeSend: function() {
				modalAguarde('show');
			},
			complete: function(){
				modalAguarde('hide');
			},

			success: function(retorno){
				modalAguarde('hide');
			}
		});


    });


	$('.menuPadrao').click(function(e) {
		menu($(this).attr('value'));

		alterarIconeMenu($(this));

		$('#sub'+$(this).attr('value')).toggle();
	});

	$('.abrir-diretorio').click(function(e) {
        abrirDiretorio($(this).attr('diretorio'));
    });


	//menu();



});

function abrirDiretorio(idDiretorio, pagina) {



  $.ajax({

	  type: 'POST',
	  url: '<?php echo appConf::caminho ?>relatorio/abrir2',

	  data: {
		  idDiretorio:idDiretorio,
		  pagina:pagina
	  },

	  beforeSend: function() {
		 // modalAguarde('show');
		 $('.conteudo-diretorio').html(aguarde);
	  },
	  complete: function(){
		  //modalAguarde('hide');
	  },

	  success: function(retorno){
		$('.conteudo-diretorio').html(retorno);

		$('.abrir-diretorio').click(function(e) {
            abrirDiretorio($(this).attr('diretorio'));
			//alert($(this).attr('diretorio'),0);
        });

	  }
  });
}

function menu(id) {

	   //abrirDiretorio(id,'');
		html = requisicaoAjax('<?php echo appConf::caminho ?>relatorio/subPasta/'+id);

	   $('#sub'+id).html(html);

		$('#sub'+id + ' .subPasta').click(function(e) {

			alterarIconeMenu($(this));

			menu($(this).attr('value'));

			$('#sub'+$(this).attr('value')).toggle();

			$('.abrir-diretorio').click(function(e) {
				abrirDiretorio($(this).attr('diretorio'));
			});

		});
}

function alterarIconeMenu(no) {
	classe = $(no).attr('class');

	if($(no).hasClass('folder-open')) {
		$(no).removeClass('folder-open');
		$(no).addClass('folder-close');
	} else {
		$(no).removeClass('folder-close');
		$(no).addClass('folder-open');
	}

	//$(no).attr('src', llink);
}

function getPerfil() {
	var array_final = [];

	$('.lista-permitido .check-usuario').each(function(index, element) {
		var array_add = {};

		array_add.id_perfil = $(this).attr('id_perfil');
		array_add.id_setor  = $(this).attr('id_setor');
		array_add.id_linha  = $(this).attr('id_linha');

		array_final.push(array_add);

	});

	return array_final;
}

function selecionarSubDiretorio(id) {
	subDir = $.parseJSON(requisicaoAjax('<?php echo appConf::caminho ?>relatorio/selecionarPastaMover/'+id));
	$('#divSubDiretorio').html(subDir.sub);
	$('#local').html(subDir.diretorio);
	$('#idPastaMover').val(id);

	$('#divSubDiretorio a').click(function(e) {
		selecionarSubDiretorio($(this).attr('value'));
	});
}

function adicionarPerfil() {
	var html='';
	$('.lista-permissao .check-usuario').each(function(index, element) {
		if($(this).prop('checked') == true) {
			htmlCheck = $(this).closest('.checkbox').html();
			text = $(this).attr('usuario');

			if($(".lista-permitido .check-usuario[usuario*='"+text+"'").length == 0) {
				html = html + '<div class="checkbox">' + htmlCheck +'</div>';
			}
		}
	});

	$('.lista-permissao .check-usuario').prop('checked', false);
	$('.lista-permitido').append(html);
}

function removerPerfil() {
	var html='';
	$('.lista-permitido .check-usuario').each(function(index, element) {
		if($(this).prop('checked') == true) {
			$(this).closest('.checkbox').remove();
		}
	});

}





</script>

<div class="container">

	<div class="row">
    	<div class="col-lg-3 col-xs-6">
        	<p class="lead"><span class="glyphicon glyphicon-stats"></span> Relatórios</p>
        </div>

        <div class="col-lg-6 col-xs-6">
           <?php echo $barraPesquisa ?>
        </div>

        <div class="col-lg-3 col-xs-6">
        	<?php echo $barraAdmin ?>
        </div>
    </div>


	<div class="row">
    	<div class="col-lg-3 arvore-diretorio" style="min-height:400px;border-right:1px solid #ddd">
			<?php echo $diretorio ?>
        </div>

        <div class="col-lg-9 conteudo-diretorio" style="min-height:400px;">

        	<div class="nome-diretorio">
            <div class="row">


            	<div class="col-lg-8">
                	<div>
                    	<div style="font-size:20px;"><img style="padding-right:5px;" src="<?php echo appConf::caminho ?>/public/img/closedfolder.png" class="pull-left" /><strong><?php echo $nomeDiretorio; ?></strong></div>
                        <div><small><?php echo $caminhoDiretorio; ?></small></div>
                    </div>
                </div>

                <div class="col-lg-4">



                </div>

            </div>

            </div>



            <div class="row">
            	<div class="col-lg-12">

                	<?php if ($idPasta != '') { ?>

                	<div class="row" >
                    	<div class="col-lg-5" style="border-right:1px solid #ddd"><small><strong>Nome</strong></small></div>
						<div class="col-lg-2" style="border-right:1px solid #ddd"><small><strong>Data</strong></small></div>
						<div class="col-lg-1" style="border-right:1px solid #ddd"><small><strong>Tipo</strong></small></div>
						<div class="col-lg-2" style="border-right:1px solid #ddd"><small><strong>Tamanho</strong></small></div>
                        <div class="col-lg-2" style="border-right:1px solid #ddd"><div><small><strong>Ação</strong></small></div></div>
                    </div>

                	<br />

                	<div id="conteudo-diretorio">
                	<?php echo $idPasta ?>
                    <?php echo $paginacao ?>
                    </div>

                    <?php } else { ?>
                    	<p class="text-center">Não existe arquivos nesta pasta.</p>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>

</div>


<!--MODAL EDITAR ARQUIVO-->
<div class="modal fade" id="modalEditarArquivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-primary">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Editar Arquivo</h4>
      </div>
      <div class="modal-body">

        <div class="form-group">
            <label for="exampleInputEmail1">Nome do Arquivo</label>
            <form id="formSalvarArquivo" method="post" action="<?php echo appConf::caminho ?>relatorio/salvarArquivo">
            	<input type="hidden" id="txtIdArquivo" name="txtIdArquivo" value="0" />
            	<input type="text" class="form-control" id="txtNomeArquivo" name="txtNomeArquivo" placeholder="">
            </form>
          </div>

        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-md" id="btnSalvarArquivo"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>


<!--MODAL EDITAR/ADICIONAR NOVA PASTA-->
<div class="modal fade" id="modalEditarPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-primary">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-file"></span> Criar/Editar Diretório</h4>
      </div>
      <div class="modal-body">

        <div class="form-group">
            <label for="exampleInputEmail1">Nome do Diretório</label>
            <form id="formSalvarDiretorio" method="post" action="<?php echo appConf::caminho ?>relatorio/salvardiretorio">
            	<input type="hidden" id="txtIdDiretorio" name="txtIdDiretorio" value="0" />
            	<input type="hidden" id="txtIdPai" name="txtIdPai" value="0" />
            	<input type="text" class="form-control" id="txtNomeDiretorio" name="txtNomeDiretorio" placeholder="">
            </form>
          </div>

        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-md" id="btnSalvarDiretorio"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>


<!--MODAL EXCLUIR PASTA-->
<div class="modal fade" id="modalExcluirPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-warning">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>
      <div class="modal-body">

      	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img src="<?php echo appConf::caminho ?>public/img/warning.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja Excluir essa pasta e todas as suas subpastas?</h4>
            </div>
        </div>



        <div class="form-group">
            <form id="formExcluirDiretorio" method="post" action="<?php echo appConf::caminho ?>relatorio/excluirDiretorio">
            	<input type="hidden" id="txtIdDiretorio" name="txtIdDiretorio" value="0" />
            </form>
          </div>

        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" id="btnExcluirDiretorio"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>


<!--MODAL EXCLUIR ARQUIVO-->
<div class="modal fade" id="modalExcluirArquivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-danger">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>
      <div class="modal-body">

      	<p>Deseja excluir os arquivos selecionados?</p>
        <p><strong>Atenção: </strong>Essa ação não poderá ser defeita.</p>

        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnExcluirArquivo"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>

<!--MODAL EXCLUIR CONTEUDO PASTA-->
<div class="modal fade" id="modalExcluirTotalPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-vertical-centeredd">
    <div class="modal-content">
      <div class="modal-header btn-danger">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>
      <div class="modal-body">

      	<p>Deseja excluir TODOS os arquivos desta pasta?</p>
        <p><strong>Atenção: </strong>Essa ação não poderá ser defeita.</p>

        <div class="alert alert-danger" role="alert" id="alertErro" style="display:none">

        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnExcluirTotalPasta"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>




<!--MODAL SELECIONAR PASTA-->
<div class="modal fade" id="modalSelecionarPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Diretório</h4>
      </div>

      <div class="modal-body">
      	<div id="divSelecionarPasta"></div>
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-md" data-dismiss="modal" id="btnMoverArquivos"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>




<!--MODAL PERMISSAO-->
<div class="modal fade" id="modalPermissaoPasta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content ">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><strong><span class="glyphicon glyphicon-user"></span> Permissões de Acesso</strong></h5>
      </div>

      <div class="modal-body">
      	<div id="divPermissao"></div>
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>

        <button type="button" class="btn btn-primary btn-md" data-dismiss="modal" id="btnSalvarPermissao"><span class="glyphicon glyphicon-ok"></span> Salvar Permissões</button>
      </div>
    </div>
  </div>
</div>

<!--MODAL ver PERMISSAO-->
<!--<div class="modal fade" id="modalVisualizarPermissao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-default">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Permissões de Acesso - Usuários Permitidos</h5>
      </div>

      <div class="modal-body">
      	<div id="divPreviewPermissao"></div>
      </div>


      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>
-->





<?php include('footerView.php') ?>