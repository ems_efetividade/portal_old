<?php include('headerView.php');?>


<div class="container" id="dados-home">
    <br>
    <div class="tpanel panel-tdefault">
        <div class="panel-bdody">
            <div class="center-block" style="width: 90%">
            <img class="img-responsive center-block" src="<?php echo appConf::caminho ?>public/img/logo_transformadores.png" />
            <br>
            
                
                <h3><b>REGULAMENTO TRANSFORMADORES EMS</b></h3>
                <hr>
                
                <p><b>1.	Do Objeto </b></p>
                
                <p>A presente campanha tem como objetivo criar e selecionar um grupo de colaboradores para a campanha Transformadores EMS, que tem como função multiplicar a mensagem institucional da EMS, levar para o marketing e demais áreas de apoio o feedback sobre os projetos em andamento, aumentar o contato com as bases, relacionamento com médicos, realizar apresentações institucionais, organizar eventos relacionados ao tema, participação de reuniões do projeto e promover a participação desse público nas melhorias, inovações e criação de novos projetos. </p>
                <p>A campanha “Transformadores EMS” terá a duração da campanha de um ano no período correspondente de 29/01/2018 a 31/12/2018.</p>
                <br>

                <p><b>2.	Da Participação</b></p>
                <p>2.1 Esta campanha é dirigida a todos os colaboradores da EMS interno e externo da área de Prescrição Médica e Hospitalar</p>

                <p>2.2 Para participar, o colaborador deverá se eleger ao projeto durante o período de inscrição;</p>

                <p>2.3. Os profissionais deverão fazer sua inscrição através do PORTAL EMS no período de 29/01/2018 a 09/02/2018;</p>

                <p>2.4. Os profissionais serão selecionados a partir de critérios pré-estabelecidos descritos no item 4;</p>

                <p>2.5 A participação nessa campanha implica no total reconhecimento e aceitação das condições descritas nesse regulamento. </p>
                <br>
                <p><b>3. Da Elegibilidade</b></p>
                <p>3.1 Para ser elegível um Transformador EMS, todos os colaboradores da EMS Prescrição interessados deverão efetuar o seu cadastro no PORTAL EMS Prescrição e os colaboradores da EMS Hospitalar deverão enviar a ficha de aceite pelo e-mail: ems.prescricao@ems.com.br </p>
                <p>3.2	Serão selecionados colaboradores de todo o Brasil.</p>





<br>
                    <p><b>4. Da Seleção</b></p>
                    <p>Os colaboradores que se inscreverem irão ser selecionados através de um comitê decisório que será composto por Diretores, Gerentes Nacionais e Pessoas e Gestão. </p>
<br>
                    <p><b>5. Das Atribuições</b></p>

                    <p>O grupo de Transformadores EMS tem como função multiplicar a mensagem institucional da EMS, levar para o marketing e demais áreas de apoio o feedback sobre os projetos em andamento, aumentar o contato com as bases, relacionamento com médicos, realizar apresentações institucionais, organizar eventos relacionados ao tema, participação de reuniões do projeto e promover a participação desse público nas melhorias, inovações e criação de novos projetos. O Transformador irá participar de reuniões estratégicas que visam discutir boas práticas, novos projetos e pensar no futuro da empresa com foco em inovação, pensamento criativo e pensar diferente. Atua como um embaixador da marca, seu trabalho é garantir que uma imagem de marca positiva e com credibilidade chegue ao público com iniciativas criativas, inovadoras, ousadas e uma abordagem de acordo com o proposito da empresa: Cuidar de Pessoas. Os Transformadores terão a oportunidade de conhecer mais de perto e ter um grande conhecimento importante sobre a EMS. </p>
<br>
                    <p><b>6. Disposições Gerais</b></p>

                    <p>O grupo de Transformadores da EMS será divulgado através de um e-mail marketing e por uma divulgação geral para todos os colaboradores;</p>

                    <p>O “Transformador EMS” participará ao longo do ano de encontros na EMS, em Hortolândia, que visam capacitação institucional e discussão de projetos;</p>

                    <p>Por se tratar de uma campanha institucional e de livre inscrição e participação, os colaboradores selecionados não serão comtemplados com alteração de cargo e bônus salarial;</p>

                    <p>Qualquer situação não prevista nesse regulamento, questões complementares ou de interpretações, serão decididas pelo grupo EMS.</p>

<br>
                    <p><b>7. Cronograma Geral</b></p>
                    <b>
                    <table class="table table-bordered">
                        <tr>
                            <td>Período de Inscrição</td>
                            <td>29/01 a 09/02 de 2018</td>
                        </tr>
                        
                        <tr>
                            <td>Processo de seleção dos Transformadores</td>
                            <td>19/02 a 23/02 de 2018</td>
                        </tr>
                    </table>
                    </b>
                    	
                    <p><b><u><i> * As datas informadas estão sujeitas para alteração de acordo com a decisão da empresa. </i></u></b></p>
                    <br><br><br>
                
                
<!--                <div style="max-height: 400px;overflow: auto;">
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <p>
                    Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum. Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus, elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc. Nullam arcu. Aliquam consequat. Curabitur augue lorem, dapibus quis, laoreet et, pretium ac, nisi. Aenean magna nisl, mollis quis, molestie eu, feugiat in, orci. In hac habitasse platea dictumst.
                </p>
                <p>
                    Fusce convallis, mauris imperdiet gravida bibendum, nisl turpis suscipit mauris, sed placerat ipsum urna sed risus. In convallis tellus a mauris. Curabitur non elit ut libero tristique sodales. Mauris a lacus. Donec mattis semper leo. In hac habitasse platea dictumst. Vivamus facilisis diam at odio. Mauris dictum, nisi eget consequat elementum, lacus ligula molestie metus, non feugiat orci magna ac sem. Donec turpis. Donec vitae metus. Morbi tristique neque eu mauris. Quisque gravida ipsum non sapien. Proin turpis lacus, scelerisque vitae, elementum at, lobortis ac, quam. Aliquam dictum eleifend risus. In hac habitasse platea dictumst. Etiam sit amet diam. Suspendisse odio. Suspendisse nunc. In semper bibendum libero.
                </p>
                <p>
                    Proin nonummy, lacus eget pulvinar lacinia, pede felis dignissim leo, vitae tristique magna lacus sit amet eros. Nullam ornare. Praesent odio ligula, dapibus sed, tincidunt eget, dictum ac, nibh. Nam quis lacus. Nunc eleifend molestie velit. Morbi lobortis quam eu velit. Donec euismod vestibulum massa. Donec non lectus. Aliquam commodo lacus sit amet nulla. Cras dignissim elit et augue. Nullam non diam. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. In hac habitasse platea dictumst. Aenean vestibulum. Sed lobortis elit quis lectus. Nunc sed lacus at augue bibendum dapibus.
                </p>
                </div>-->
  
                <hr>
                
                <h2 class="text-info text-center"><strong>Você quer ser um transformador?</strong></h2>
                <br>
                
                <a style="font-size: 30px" href="#" id="btnInscrever" class="btn-block btn btn-lg btn-success">
                    Clique aqui para se candidatar!
                </a>
                <br>
                
                
                
                <p class="text-center"><a  href="<?php echo appConf::caminho ?>"> Voltar para Home</a></p>
            </div>
            
            
            
        </div>
</div>
</div>
<!-- Modal -->
<div class="modal fade" id="modalOK" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
<!--      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>-->
      <div class="modal-body">
          <h1 class="text-center text-success"><b>
              <span class="glyphicon glyphicon-thumbs-up"></span>
              Sucesso!</b>
          </h1>
          <h3 class="text-center text-muted">
              Sua inscrição foi realizada com sucesso!
          </h3>
          <h5 class="text-muted text-center">Estamos felizes com o seu propósito de transformar a marca EMS</h5>
      </div>
      <div class="modal-footer">
          <a href="<?php echo appConf::caminho ?>" class="btn btn-default" >Fechar</a>
      </div>
    </div>
  </div>
</div>

    
    <script>
    
    
    $('#btnInscrever').unbind().click(function(e){
        btn = $(this);
        var btnTexto = btn.html();
        
        e.preventDefault();

               
        
        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>home/transformadores/inscrever',

            beforeSend: function (xhr) {
               btn.html('Aguarde, estamos fazendo a sua inscrição...');         
            },

            success: function(u) {
                btn.html("Sucesso!!!"); 
                //alert(u);
                $('#modalOK').modal('show');
            }

        });
        
    });
    
    
    
    </script>


<?php include('footerView.php') ?>    