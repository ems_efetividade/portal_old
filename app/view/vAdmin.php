<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 13/09/2018
 * Time: 15:36
 */
require_once('headerView.php');
require_once('/../../lib/appConf.php');
require_once('/../controller/cSenhaAdmin.php');

class vAdmin extends appConf
{
    public function main()
    {
        $this->content();
        $this->footer();
    }

    public function content()
    {
        $control = new cSenhaAdmin();
        $lisUsersAdmin = $control->listObj();
        ?>
        <script src="<?php echo appConf::caminho ?>plugins/ckeditor/ckeditor.js"></script>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <br>

                    <h3>Painel Administrador</h3>
                    <p>Painel de Admnistrador - Portal Efetividade </p>
                    <h6 class="alert alert-danger"><strong>Atenção!</strong>
                        Este módulo de sistema executa tarefas especificas a usuários avançados.<br>
                        Não utilize nenhum módulo o qual você não seja designado
                    </h6>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <fieldset>
                        <legend>
                            Atualizar Prescription Share Online
                        </legend>
                        <div class="row">
                            <div class="col-sm-12">
                                <label for="pxfile" class="btn btn-info">Arquivo</label>
                                <small>O arquivo deve ser um CSV com Email e Nome, ou somente E-mail</small>
                                <input type="file" name="pxfile" id="pxfile" style="display:none;">
                                <br>
                                <br>
                                <label class="btn btn-success" id="subirArqPx">Enviar Carga</label>
                                <button class="btn btn-danger" id="limparPx">Limpar Base Atual</button>
                                <button class="btn btn-success" id="atualizarPx">Rodar Carga</button>
                            </div>
                        </div>
                    </fieldset>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <fieldset>
                        <legend>
                            Senha Mestre
                        </legend>
                        <div class="row">
                            <div class="col-sm-4">
                                <input id="txtSenhaCpf" type="text" placeholder="CPF" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <button id="btnSenhaVerificar" class="form-control btn-info">Verificar</button>
                            </div>
                            <div class="col-sm-6">
                                <input id="txtSenhaUsuario" type="text" placeholder="Usuário" readonly
                                       class="form-control">
                            </div>
                            <br>
                            <br>
                            <div class="col-sm-4">
                                <input id="txtSenhaData" type="date" placeholder="Expirar" class="form-control">
                            </div>
                            <div class="col-sm-2">
                                <button id="btnSenhaGerar" class="form-control btn-success">Gerar</button>
                            </div>
                            <div class="col-sm-6">
                                <input id="txtSenhaSenha" type="text" placeholder="Senha" readonly class="form-control">
                            </div>
                        </div>
                        <div style="overflow: auto; height: 250px">
                            <table id="tabela_senhas" class="table table-hover text-primary">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Usuário</th>
                                    <th>Expira em:</th>
                                    <th></th>
                                <tr>
                                </thead>
                                <tbody>
                                <?php
                                foreach ($lisUsersAdmin as $o) {
                                    ?>
                                    <tr>
                                        <td><?php echo $o->getId() ?></td>
                                        <td><?php echo $o->getNomeUsuarioById() ?></td>
                                        <td
                                            <?php
                                            if ($o->getSituacaoSenha()) {
                                                echo 'class="text-success"';
                                            } else {
                                                echo 'class="text-danger"';
                                            }
                                            ?>
                                        >
                                            <?php
                                            echo $o->getDataExpirar();
                                            ?>
                                        </td>
                                        <td>
                                            <button class="btn btn-info" id="<?php echo $o->getId() ?>">Log
                                            </button>
                                            <?php
                                            if ($o->getSituacaoSenha()) {
                                                ?>
                                                <button class="btn btn-warning" id="<?php echo $o->getId() ?>">Travar
                                                </button>
                                                <?php
                                            }
                                            ?>

                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12" style="cursor: pointer; ">
                    <fieldset>
                        <legend>
                            Procedimentos DBA
                        </legend>
                        <span class="btn btn-info" id="btnListarAlteracao">Listar Alterações</span>
                        <span class="btn btn-info" id="btnPendencias">Verificar Usuários Pendentes</span>
                        <span class="btn btn-warning" id="btnAcessos">Criar Acessos</span>
                        <hr>
                        <div style="overflow: auto; height: 250px">
                            <table id="tabela_retorno" class="table table-hover text-primary">

                            </table>
                        </div>
                    </fieldset>
                    <hr>
                </div>

                <div class="col-sm-12" style="cursor: pointer; display: ;">
                    <fieldset>
                        <legend>
                            Reset Senha <strong>Prescrição</strong>
                        </legend>
                        <form id="frm_resetar">
                            <label>Informe o setor para resetar a senha</label>
                            <input type="text" name="setor" id="setor" class="form-control">
                            <small>Um e-mail será enviado automáticamente ao usuário</small>
                            <br><br>
                            <a class="btn btn-success" id="btn_resetar">Resetar</a>
                        </form>
                    </fieldset>
                    <hr>
                </div>
                <div class="col-sm-12" style="cursor: pointer;">
                    <fieldset>
                        <legend>
                            Disparador de E-mail
                        </legend>
                        <form id="frm_email">
                            <label>Selecione o arquivo com a lista dos e-mails</label>
                            <br>
                            <label for="emailfile" class="btn btn-info">Arquivo</label>
                            <small>O arquivo deve ser um CSV com Email e Nome, ou somente E-mail</small>
                            <label class="btn btn-success" style="float: right" id="btnEmail">Enviar</label>
                            <br>
                            <br>
                            <input type="file" name="emailfile" id="emailfile" style="display:none;">
                            <label>Assunto</label>
                            <input type="text" name="assunto" id="assunto" class="form-control">
                            <label>Nome do Remetente</label>
                            <input type="text" name="nomeremetente" id="nomeremetente" class="form-control">
                            <label>Remetente</label>
                            <input type="text" name="remetente" id="remetente" class="form-control">
                            <label>CCo</label>
                            <input type="text" name="cco" id="cco" class="form-control">
                            <br>
                            <label>Email</label>
                            <textarea class="ckeditor" name="txtTexto" id="txtTexto" rows="1"></textarea>
                            <br><br>
                        </form>
                    </fieldset>
                    <hr>
                </div>
            </div>
            <br>
        </div>
        <script src="/../../public/js/admin.js"></script>
        <link href="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/../../public/js/Notiflix/Minified/notiflix-1.4.0.min.js"></script>
        <script type="text/javascript" src="/../../public/js/Notiflix/AIO/notiflix-aio-1.4.0.min.js"></script>
        <?php
    }

    public function footer()
    {
        require_once('footerView.php');
    }


}