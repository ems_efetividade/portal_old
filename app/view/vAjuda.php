<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 27/08/2018
 * Time: 11:44
 */

require_once('/../controller/cAjudaController.php');

class vAjuda
{
    public function viewAjudaFerramentas(){
        session_start();
        $control = new cAjudaController();
        $_POST['idsistemaacoplado'] = $_SESSION['sistema_atual'];
        $lista = $control->listObj();
        if(!is_array($lista)){
            ?>
            <div class="row">
                <div class="col-sm-12 text-center">
                    <br>
                    <div class="alert alert-info">Infelizmente ainda não temos nenhuma informação cadastrada aqui<br>
                        Que tal entrar em contato através do nosso e-mail? <i>efetividade@ems.com.br</i>
                    </div>
                </div>
            </div>
            <?php
            return;
        }
        ?>

            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <h3>Dúvidas Frequentes </h3>
                </div>
            </div>
            <hr>
        <script>
            var notificacao = new Notification("Portal Efetividade",{
                icon:'https://efetividade.gruponc.net.br/public/img/iconsFerramentas/logoEfet.png',
                body:'Relatório de Produtividade Postado',
            });
        </script>
            <div class="row">

                <?php
                foreach ($lista as $o) {
                    $avaliacao = $control->getAvaliacao($o->getId());
                    ?>
                    <div class="col-sm-12">
                        <div class="panel panel-default text-left" style="box-shadow: 1px 1px #a0a0a0;">
                            <div class="panel-heading ">
                                <h4 class="panel-title">
                                    <span class="glyphicon glyphicon-chevron-right">&nbsp;</span>
                                    <a onclick="logClick(<?php echo $o->getId() ?>)" data-toggle="collapse"
                                       data-parent="#accordion"
                                       style="font-size: 19px; text-decoration: none"
                                       href="#collapse<?php echo $o->getId() ?>"><?php echo $o->getTitulo() ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $o->getId() ?>" class="panel-collapse collapse">
                                <div class="panel-body"><?php echo $o->getResposta() ?></div>
                                <div class="panel-body text-right">
                                    <h5 class="<?php echo $avaliacao ?>" id="pergunta<?php echo $o->getId() ?>">Este item foi útil para você?
                                        <span onclick="rating(<?php echo $o->getId() ?>,1)"
                                              style=" cursor:pointer;"
                                              class="glyphicon glyphicon-thumbs-up text-success">
                                        </span>
                                        <span onclick="rating(<?php echo $o->getId() ?>,0)"
                                              style=" cursor:pointer;"
                                              class="glyphicon glyphicon-thumbs-down text-danger">
                                        </span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <hr>
        <script src="../../public/js/ajuda.js"></script>
        <?php
    }

    public function main()
    {
        require_once('headerView.php');
        $_POST['idsistemaacoplado'] = '';
        $control = new cAjudaController();
        $lista = $control->listObj();
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <h3>Dúvidas Frequentes </h3>
                </div>
            </div>
            <hr>

            <div class="row">

                <?php
                foreach ($lista as $o) {
                    $avaliacao = $control->getAvaliacao($o->getId());
                    ?>
                    <div class="col-sm-12">
                        <div class="panel panel-default text-left" style="box-shadow: 1px 1px #a0a0a0;">
                            <div class="panel-heading ">
                                <h4 class="panel-title">
                                    <span class="glyphicon glyphicon-chevron-right">&nbsp;</span>
                                    <a onclick="logClick(<?php echo $o->getId() ?>)" data-toggle="collapse"
                                       data-parent="#accordion"
                                       style="font-size: 19px; text-decoration: none"
                                       href="#collapse<?php echo $o->getId() ?>"><?php echo $o->getTitulo() ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $o->getId() ?>" class="panel-collapse collapse">
                                <div class="panel-body"><?php echo $o->getResposta() ?></div>
                                <div class="panel-body text-right">
                                    <h5 class="<?php echo $avaliacao ?>" id="pergunta<?php echo $o->getId() ?>">Este item foi útil para você?
                                        <span onclick="rating(<?php echo $o->getId() ?>,1)"
                                              style=" cursor:pointer;"
                                              class="glyphicon glyphicon-thumbs-up text-success">
                                        </span>
                                        <span onclick="rating(<?php echo $o->getId() ?>,0)"
                                              style=" cursor:pointer;"
                                              class="glyphicon glyphicon-thumbs-down text-danger">
                                        </span>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <hr>
        </div>
        <script src="../../public/js/ajuda.js"></script>
        <?php
        require_once('footerView.php');
    }
}