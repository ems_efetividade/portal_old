<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 06/11/2018
 * Time: 17:52
 */

class vAssinaturas
{
    public function vAssinaturas()
    {
        require_once('headerView.php');
        if ($_SESSION['nivel_admin'] < 2) {
            session_destroy();
            header("Refresh:0");
        }
        $modulo = $_POST['modulo'];
        ?>
        <style>
            #ctn_explorer {
                margin-top: 0px;
                margin-left: 0px;
                padding: 0;
            }
        </style>
        <div id="ctn_explorer">
            <div class="col-md-2"
                 style="background: rgb(42, 63, 84);
                 color: rgb(255, 255, 255);
                 height: 750px;
                 paddin:0;
                 margin:0;">
                <br>
                <img width="90" class="center-block" src="/../../public/img/iconsFerramentas/logoEfet.png">

                <h5 class="text-center">
                    EFETIVIDADE
                </h5>

                <hr>
                <h4>
                    <span class="glyphicon glyphicon-file"></span> Documentos
                </h4>
                <div style="overflow-x: auto;">
                    &nbsp;&nbsp;<a style="text-decoration: none;color: white"
                                   href="<?php echo appConf::caminho ?>cAssinaturasDocumentos/viewUpload"><span
                                class="glyphicon glyphicon-upload"></span>&nbsp;Carregar Documentos</a><br>
                    &nbsp;&nbsp;<a style="text-decoration: none;color: white"
                                   href="<?php echo appConf::caminho ?>cAssinaturasDocumentos/viewPermissoes"><span
                                class="glyphicon glyphicon-cog"></span>&nbsp;Permissões</a><br>
                    &nbsp;&nbsp;<a style="text-decoration: none;color: white"
                                   href="<?php echo appConf::caminho ?>cAssinaturasDocumentos/viewAssinadas"><span
                                class="glyphicon glyphicon-pencil"></span>&nbsp;Assinaturas</a>
                </div>
            </div>
            <div class="col-md-10" style="height: 750px; overflow: auto;">
                <?php
                if ($modulo == 1) {
                    ?>
                    <br>
                    <legend>Enviar Documentos</legend>
                    <span class="btn btn-info" id="btnEscolher">Escolher Arquivos</span>
                    <script>
                        $('#btnEscolher').click(function () {
                            $('#termo').click();
                        })
                    </script>
                    <?php
                    $this->formUpload();
                } else if ($modulo == 2) {
                    $this->formPermissoes();
                } else if ($modulo == 3) {
                    $this->formDocumentosAssinados();
                }
                ?>
            </div>
        </div>
        <script src="/../../public/js/assinaturas.js"></script>
        <?php
        $this->rodape();
    }

    public function formDocumentosAssinados()
    {
        require_once('/../controller/cAssinaturasDocumentosController.php');
        $control = new cAssinaturasDocumentosController();
        $idUsuario = $_POST['id_usuario'];
        $listUsuario = $control->listUsuarios();
        if ($idUsuario > 0) {
            $lista = $control->listarAssinadosByIdEmpresa($idUsuario);
        }
        ?>
        <form method="post">
            <br><br>
            <label for="id_usuario">Nome Colaborador</label>
            <select class="form-control" id="id_usuario" name="id_usuario">
                <?php
                foreach ($listUsuario as $usuario) {
                    ?>
                    <option value="<?php echo $usuario['ID_COLABORADOR'] ?>"><?php echo $usuario['NOME'] ?></option>
                <?php } ?>
            </select>
            <br>
            <input class="btn btn-info" type="submit" value="BUSCAR"/>

        </form>
        <br><br>
        <table class="table table-responsive">
            <tr>
                <th>
                    Arquivo
                </th>
            </tr>
            <?php
            foreach ($lista as $value) {
                ?>
                <tr>
                    <td>
                        <a download href="../../public/politicas/assinadas/<?php echo $value; ?>">
                            <img width="25" src="/../../public/img/ext/pdf.png"/>
                            <?php echo $value; ?></a>
                    </td>
                </tr>
                <?php
            }
            ?>
        </table>
        <?php
    }

    public function formPermissoes()
    {
        require_once('/../controller/cAssinaturasDocumentosController.php');
        $control = new cAssinaturasDocumentosController();
        $array_permissoes = $control->carregarPermissoes();
        $lista = $control->listObj();
        if (!is_array($lista)) {
            ?>
            <br>
            <div class="container">
                <br>
                <p class=" alert alert-danger">Nenhum Documento Encontrado</p>
            </div>
            <?php
            return;
        }
        ?>
        <br>

        <legend>Permissões de Documentos</legend>
        <table class="table table-striped table-hover">
            <?php
            foreach ($lista as $o) {
                ?>
                <tr>
                    <td>
                        <div class="row">
                            <div class="col-sm-12">
                                <?php
                                echo "<b>" . $o->getId() . "</b> - ";
                                echo $o->getNome();
                                ?>
                            </div>
                            <?php
                            foreach ($array_permissoes as $permissoes => $array) {
                                ?>
                                <div class="col-sm-3">
                                    <label><?php echo $permissoes ?></label>
                                    <select multiple
                                            name="<?php echo $o->getId() . $permissoes ?>[]"
                                            id="<?php echo $o->getId() . $permissoes ?>"
                                            class="form-control"
                                            style="overflow: auto">
                                        <?php
                                        foreach ($array as $key => $value) {
                                            ?>
                                            <option value="<?php echo $value['ID'] ?>">
                                                <?php echo $value['NOME'] ?>
                                            </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-sm-4">
                                <label>Inicio</label>
                                <input type="date" class="form-control" name="<?php echo $o->getId() ?>inicio"
                                       id="<?php echo $o->getId() ?>inicio">
                            </div>
                            <div class="col-sm-4">
                                <label>Término</label>
                                <input type="date" class="form-control" name="<?php echo $o->getId() ?>expira"
                                       id="<?php echo $o->getId() ?>expira">
                            </div>
                            <div class="col-sm-4" style="float: right"><br>
                                <button class="btn btn-success "
                                        onclick="salvarLinha('<?php echo $o->getId() ?>')">Atualizar
                                </button>
                                <button class="btn btn-danger "
                                        onclick="excluir('<?php echo $o->getId() ?>')">Excluir
                                </button>
                            </div>
                        </div>
                    </td>
                </tr>
                <?php
            }
            ?>

        </table>
        <br>
        <br>
        <br>
        <?php
    }

    public function formUpload()
    {
        ?>
        <form id="formUpload" action="<?php echo appConf::caminho ?>cAssinaturasDocumentos/salvarArquivos"
              enctype="multipart/form-data">
            <div class="row text-left ">
                <div class="col-sm-7 text-left opl_div" style="display: none">
                    <div class="form-group form-group-sm">
                        <label for="termo"> Arquivo</label>
                        <label for="termo" class="form-control" id="nomeTermo">
                        </label>
                    </div>
                </div>
                <div class="col-sm-1 text-left opl_div">
                    <div class="form-group form-group-sm">
                        <label for="termo" style="color:white;">.</label><br>
                        <label for="termo" class="">
                        </label>
                        <input required style="display: none;" type="file" onchange="gerarListaUpload()"
                               multiple="multiple" name="termo" id="termo">
                    </div>
                </div>

                <div class="col-sm-12" style="height:450px;overflow-x: scroll">
                    <table class="table tb_div table-responsive table-condensed" id="arquivos"
                           style="display:none;">
                        <thead>
                        <tr class="active">
                            <th class="text-left"> #</th>
                            <th class="text-left"> Nome</th>
                            <th class=""> Tipo</th>
                            <th class=""> Tamanho</th>
                            <th class=""> Status</th>
                        </tr>
                        </thead>
                    </table>
                </div>

                <div style="display:;" id="barraProgresso" class="col-sm-12 text-left">
                    <br>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success progress-bar-striped" id="barra"
                             role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                            <span id="textobarra">AGUARDE...</span>
                        </div>
                    </div>
                    <i id="contadorArqu" class="text-center">0 de 0 arquivo(s) enviado(s) </i><i id="tempo"></i>
                    <!--                    <img id="centro" style="align-content:center!important;" src="https://media.giphy.com/media/3oEdv5FBPbfMJbraBG/giphy.gif">-->
                </div>
                <div class="col-sm-12 text-right">
                    <br>
                    <span onclick="subirArquivos()" id="btnSubirArqu" class="btn btn-success">Confirmar</span>
                </div>
            </div>
        </form>
        <?php
    }

    public function rodape()
    {
        ?>
        <div id="home-rodape" style="position:fixed;bottom:0;width:100%;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h5>
                            <p class="text-center">
                                <small>Portal Efetividade - Grupo NC</small>
                            </p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
}