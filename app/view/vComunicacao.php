<?php
/**
 * Created by PhpStorm.
 * User: jeanc
 * Date: 25/09/2018
 * Time: 22:45
 */
require_once("headerView.php");

class vComunicacao
{

    public function vComunicacao()
    {
        $perfil = appFunction::dadoSessao('perfil');
        $nivelAdmin = appFunction::dadoSessao('nivel_admin');
        ?>
        <link href="/../../sistemas/AMOSTRAS/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/../../sistemas/AMOSTRAS/plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet"
              type="text/css"/>
        <link href="/../../sistemas/AMOSTRAS/public/css/fonts.css" rel="stylesheet" type="text/css"/>
        <div class="container">
        <div class="row">
        <div class="col-lg-12 col-xs-12">
        <br>
        <h3 class="lead"><span class="glyphicon glyphicon-align-justify"></span>
            Comunicação
        </h3>
        <ul class="nav nav-tabs ">
            <li role="presentation" class="<?php
            if ($_POST['idsistemaacoplado'][1] == 'noticias') {
                echo 'active';
            } ?>">
            <a href="<?php echo appConf::caminho ?>comunicacao/noticias"><span
                        class="glyphicon glyphicon-file"></span>
                Artigos/Notícias
            </a>
        </li>
        <li role="presentation" class="<?php
        if ($_POST['idsistemaacoplado'][1] == 'informativo') {
            echo 'active';
        } ?>">
            <a href="<?php echo appConf::caminho ?>comunicacao/informativo"><span
                        class="glyphicon glyphicon-info-sign"></span> Informativos
            </a>
            </li>
            <li role="presentation" class="<?php
            if ($_POST['idsistemaacoplado'][1] == 'popup') {
                echo 'active';
            } ?>">
                <a href="<?php echo appConf::caminho ?>comunicacao/popup"><span
                            class="glyphicon glyphicon-comment"></span> Popup
                </a>
            </li>
            </ul>
            </div>
            </div>
            </div>
            <br>
            <?php
            if ($_POST['idsistemaacoplado'][1] == '') {
                echo '<div style="height: 250px"></div>';
                require_once('footerView.php');
            }
        }

    }