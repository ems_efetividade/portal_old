<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 12/09/2018
 * Time: 17:24
 */

class vFaleConosco
{
    public function vFaleConosco()
    {
        require_once('headerView.php');

        ?>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <br>
                    <br>
                    <br>
                    <legend><strong>Canais de Atendimento -&nbsp;Efetividade</strong></legend>

                    <p style="font-size: 15px">
                        Estamos trabalhando constantemente em melhorias de processos e ferramentas, e para que isso aconteça você é muito importante. Através dos nossos canais de atendimento, você pode enviar suas dúvidas, solicitações ou sugestões, e você ainda poderá contar com uma equipe técnica que esta cada dia mais especializada em melhorar sua experiência profissional e pessoal, trazendo sempre informações essenciais de forma clara e objetiva.
                    </p>

                    <br>
                    <p><strong>Canal de Atendimento: </strong><a
                                href="mailto:efetividade@ems.com.br">efetividade@ems.com.br</a>
                    </p>

                    <p><strong>Assuntos que devem ser tratados no e-mail Efetividade:</strong></p>

                    <ul>
                        <li>Relat&oacute;rios de Produtividade, Prescri&ccedil;&atilde;o e Demanda;</li>
                        <li>Dúvidas e suporte Portal Efetividade</li>
                        <li>Sistema Mercanet</li>
                        <li>Sistema WRS</li>
                        <li>Dúvidas Objetivo/Prêmio</li>
                        <li>Suporte LINCE</li>
                        <li>Painel HB20</li>
                        <li>PMA/RAC</li>
                    </ul>

                    <p>&nbsp;</p>

                    <p><strong>Atendimento Close-Up</strong></p>
                    <p><strong>Departamento Respons&aacute;vel: </strong>Efetividade</p>
                    <p><strong>Canal de Suporte: </strong> 0800 770 8608</p>
                    <ul>
                        <li>Aplicativo SFNET</li>
                        <li>Sistema WebApp</li>
                        <li>Sistema GerReps</li>
                    </ul>

                    <p>&nbsp;</p>
                    <p><strong>TABM&Eacute;DIA / Amostras Gr&aacute;tis</strong></p>

                    <p><strong>Departamento Respons&aacute;vel: </strong>Planejamento Estrat&eacute;gico</p>

                    <p><strong>Canal de Suporte: </strong><a href="mailto:planejamento.estrategico@ems.com.br">planejamento.estrategico@ems.com.br</a>
                    </p>

                    <p>&nbsp;</p>

                    <p><strong>E-mail / RDV / Frota / Acesso Automidia / IPAD / Celular</strong></p>

                    <p><strong>Departamento Respons&aacute;vel:</strong> CSI Automidia</p>

                    <p><strong>Canal de Suporte:</strong><a href="http://automidia.csintegrado.com.br/"><strong>http://automidia.csintegrado.com.br</strong></a>
                        ou pelo telefone: <strong>0800 703 8337</strong></p>
                    <br>
                    <br>                    <br>
                    <br>
                </div>
            </div>
        </div>
        <?php
        require_once('footerView.php');
    }

}