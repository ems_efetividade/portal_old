<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 03/10/2018
 * Time: 09:35
 */
require_once('/../controller/cHomeController.php');
require_once('/../view/abs/gridView.php');
include_once('headerView.php');

class vHome extends gridView
{
    /**
     * vHome constructor.
     */
    public function vHome()
    {
        $control = new cHomeController();
        $pilares = $control->listPilaresBySetor();
        $informativos = $control->listInformativos();
        $noticias = $control->listNoticias();
        var_dump($noticias);
        ?>
        <style>
            .highcharts-background {
                fill: #f7f7f7;
            }
        </style>
        <script src="/../../public/js/home.js"></script>
        <link rel="stylesheet" href="public/css/homeView.css"/>
        <div class="container" style="width: 95%">
            <div class="row">
                <br>
                <div class="col-sm-12">
                    <h3>Olá <?php echo appFunction::tratarNome($_SESSION['nome']); ?>
                        , seja bem-vindo!</h3>
                </div>
                <?php
                foreach ($pilares as $pilar) {
                    ?>
                    <div class="col-xs-6 col-md-3">
                        <div class="panel" style="background-color: #f7f7f7">
                            <div class="panel-body text-center">
                                <span><small><b><?php echo $pilar['PILAR'] ?></b></small></span>
                                <div>
                                    <small>
                                        <small class="text-muted">(Jul/18)</small>
                                    </small>
                                </div>
                                <b>
                                    <div>
                                        <?php
                                        $ponto = strpos($pilar['M00'], '.') + 1;
                                        echo substr($pilar['M00'], 0, $ponto + $pilar['DECIMAL']) . "%"
                                        ?>
                                    </div>
                                </b>

                                <small class="text-muted">
                                    <span style="border-radius:4px !important"
                                          class="label
                                        <?php
                                          $ponto = strpos($pilar['EVOL_MES'], '.') + 1;
                                          $valor = substr($pilar['EVOL_MES'], 0, $ponto + $pilar['DECIMAL']);
                                          if ($valor > 0) {
                                              echo ' label-success ';
                                          } else {
                                              echo ' label-danger ';
                                          }
                                          ?>
                                           "><b>
                                          <?php echo $valor ?></b>
                                    </span>&nbsp; do mes anterior.
                                </small>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div class="col-sm-7 text-block" style="height: 560px">
                        <h1><?php echo $noticias[2]['TITULO']; ?></h1>
                    <img max-width="100%" max-height="60%" src="<?php echo $noticias[2]['IMAGEM']; ?>">
                    <p><?php echo $noticias[2]['RESUMO']; ?></p>
                </div>
                <div class="col-sm-5">
                    <h4 style="display: none" class="header-box header-text-color"><span
                                class="glyphicon glyphicon-stats"></span>
                        Projeção
                        <small class="pull-right">Atualizado em <span id="graf1-atualizacao"></span></small>
                    </h4>
                    <div id="grafico-home1" style="width:100%;height:260px;"></div>
                    <h4 id="graf1-texto"></h4>
                    <span style="margin-top:11px;position:relative;">
                                <a href="#infoCarousel" data-slide="prev" class="nav-informativo btn btn-danger btn-sm"
                                   acao="prev">
                                    <span class="glyphicon glyphicon-chevron-left">
                                    </span>
                                </a>
                                <a href="#infoCarousel" data-slide="next" class="nav-informativo btn btn-danger btn-sm"
                                   style="z-index:999;" acao="next">
                                    <span class="glyphicon glyphicon-chevron-right">
                                    </span>
                                </a>
                        </span>
                    <div id="infoCarousel" class="carousel slide" data-ride="carousel" style="height: 200">
                        <div class="carousel-inner">
                            <?php
                            $i = 0;
                            foreach ($informativos as $informativo) {
                                ?>
                                <div class="item
                                <?php if ($i == 0) {
                                    echo ' active';
                                } ?>">
                                    <h3><?php echo $informativo['TITULO']; ?></h3>
                                    <?php echo $informativo['TEXTO']; ?>
                                </div>
                                <?php
                                $i++;
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="row">
                <div class="col-sm-12">
                    <h3 class="my-4">Related Projects</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <a href="#">
                        <img width="200px" class="img-fluid" src="http://placehold.it/500x300" alt="">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#">
                        <img width="200px" class="img-fluid" src="http://placehold.it/500x300" alt="">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#">
                        <img width="200px" class="img-fluid" src="http://placehold.it/500x300" alt="">
                    </a>
                </div>
                <div class="col-sm-3">
                    <a href="#">
                        <img width="200px" class="img-fluid" src="http://placehold.it/500x300" alt="">
                    </a>
                </div>
            </div>
        </div>
        <footer class=" bg-dark">
            <?php require_once('footerView.php') ?>
        </footer>
        <?php
    }
}