<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 13/09/2018
 * Time: 15:36
 */
require_once('headerView.php');

class vJuridico
{
    public function main()
    {
        $this->content();
        $this->footer();
    }

    public function content()
    {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <br>
                    <br>
                    <h3>Jurídico </h3>
                    <p>Painel de Relatórios </p>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-4" style="cursor: pointer;">
                    <legend>
                        <fieldset>
                            Arquivos Baixados
                        </fieldset>
                    </legend>
                </div>
                <div class="col-sm-4" style="cursor: pointer;">
                    <legend>
                        <fieldset>
                            Log de Acessos
                        </fieldset>
                    </legend>
                </div>
                <div class="col-sm-4" style="cursor: pointer;">
                    <legend>
                        <fieldset>
                            Documentos Assinados
                        </fieldset>
                    </legend>
                </div>
            </div>
                <div class="col-sm-12" >
                    <div class="container" style="min-height: 350px">
                        <div class="row">
                            <div class="col-sm-12">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function footer()
    {
        require_once('footerView.php');
    }


}