<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 29/07/2019
 * Time: 23:55
 */
require_once('headerView.php');

class vMeeting
{

    public function vMeeting()
    {

        ?>
        <style>
            .container {
                border: 2px solid #dedede;
                background-color: #f1f1f1;
                border-radius: 5px;
                padding: 10px;
                width: 99%;
                margin: 10px 1px;
            }

            .darker {
                border-color: #ccc;
                background-color: #ddd;
            }

            .container::after {
                content: "";
                clear: both;
                display: table;
            }

            .container img {
                float: left;
                max-width: 60px;
                width: 100%;
                margin-right: 20px;
                border-radius: 50%;
            }

            .container img.right {
                float: right;
                margin-left: 20px;
                margin-right: 0;
            }

            .time-right {
                float: right;
                color: #aaa;
            }

            .time-left {
                float: left;
                color: #999;
            }
        </style>
        <body style="background-image: url('/../../public/img/back.png'); background-size: cover;">
        <div class="row" style="padding: 10px">
            <div class="col-md-7" style="height: 430px;margin-top: 54px">
                <h3 style="color: white;">Assunto - Sistema SIGNO &nbsp;&nbsp;&nbsp;<small style="color: white;">Horário
                        das 10:30 às 12:30
                    </small>
                </h3>
                <iframe src="https://www.youtube.com/embed/oXTaB378Fp4" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
            <div class="col-md-5" style="max-width: 600px">
                <img src="/../../public/img/webmeeting.png" width="100%"/>
                <div style="height: 430px; overflow: auto; border: solid white" id="chat">

                </div>
            </div>
            <div class="col-md-7" style="width:56.2%; border: 1px solid white;margin:10px 14px 0px 14px ">
                <br>
                <div class="row" style="color:white; font-size: 17px">
                    <div class="col-md-1"></div>
                    <div class="col-md-4"><span class="glyphicon glyphicon-star"></span> Total de Visitantes: 000</div>
                    <div class="col-md-3"><span class="glyphicon glyphicon-user"></span> Online Agora: 000</div>
                    <div class="col-md-4"><span class="glyphicon glyphicon-time"></span> Tempo Total: 00:00</div>
                </div>
                <br>
            </div>
            <div class="col-md-5" style="padding: 10px 15px 0px 15px">
                <textarea class="form-control" rows="2" style="padding: 12px" id="txtMensagem"></textarea>
                <div class="row pull-right" style="color:white; font-size: 40px;margin-top: 5px">
                    <div class="col-md-12">
                        <button onclick="EnviarMensagem()"
                                class="btn btn-success" style="border-radius: 5px!important;">ENVIAR
                        </button>

                        <input type="hidden" id="idColaborador" value="<?php echo $_SESSION['id_colaborador'] ?>"/>
                        <input type="hidden" id="nomeColaborador" value="<?php echo $_SESSION['nome'] ?>"/>
                    </div>
                </div>
            </div>
        </div>
        </body>

        <script>
            ultimoID = 0;
            setTimeout(function () {
                buscarMensagens();
            }, 3000);
            function buscarMensagens() {
                formData = new FormData();
                formData.append('ultimo', ultimoID);
                $.ajax({
                    type: "POST",
                    url: 'https://' + window.location.host + '/admin/listarMensagem',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (retorno) {
                        if (retorno === 0) {
                            setTimeout(function () {
                               buscarMensagens();
                            }, 2000);
                            return;
                        }
                        retorno = $.parseJSON(retorno);
                        for (prop in retorno) {
                            console.log(retorno[prop].ID)
                            if(parseInt(retorno[prop].ID)>ultimoID) {
                                console.log(retorno[prop].ID)
                                ultimoID = retorno[prop].ID
                            }
                            mensagem = retorno[prop].MENSAGEM;
                            foto = retorno[prop].FK_USER;
                            nome = retorno[prop].NOME;
                            hora = retorno[prop].HORA;
                            listarMensagens(mensagem,foto,nome,hora);
                        }
                        setTimeout(function () {
                           buscarMensagens();
                        }, 2000);
                    }
                });
            }

            function listarMensagens(mensagem,foto,nome,hora){
                $.ajax({
                    type: "POST",
                    url: 'https://' + window.location.host + '/admin/fotoColaborador/'+foto,
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (retorno) {
                        chat = document.getElementById('chat').innerHTML;
                        mensagemf = '<div class="container" id="+ mensagem +">' +
                            '<img src="'+retorno+'" alt="Avatar" style="width:100%;">' +
                            '<strong><h5>' + nome + '</h5></strong>' +
                            '<p>' + mensagem + '</p>' +
                            '<span class="time-right">'+hora+'</span>' +
                            '</div>';
                        document.getElementById('chat').innerHTML =  chat+mensagemf
                    }
                });
            }

            function EnviarMensagem() {
                var data = new Date();
                var hora    = data.getHours();          // 0-23
                var min     = data.getMinutes();        // 0-59
                chat = document.getElementById('chat').innerHTML;
                mensagem = document.getElementById('txtMensagem').value;
                nomeColaborador = document.getElementById('nomeColaborador').value;
                idColaborador = document.getElementById('idColaborador').value;
                formData = new FormData();
                formData.append('mensagem', mensagem);
                formData.append('nomeColaborador', nomeColaborador);
                formData.append('idColaborador', idColaborador);
                formData.append('hora', hora+':'+min);
                $.ajax({
                    type: "POST",
                    url: 'https://' + window.location.host + '/admin/gerarMensagem',
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (retorno) {
                        document.getElementById('txtMensagem').value = '';
                    }
                });

            }
        </script>
        <?php

    }

}