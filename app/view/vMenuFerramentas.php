<?php
/**
 * Created by PhpStorm.
 * User: j0021877
 * Date: 12/09/2018
 * Time: 14:09
 */
require_once('/../model/mControleHeader.php');

class vMenuFerramentas
{
    public function montaModal()
    {
        $header = new mControleHeader();
        $permitidos = $header->listPermissoesEmpresas();
        $escritos = 0;
        ?>
        <div class="row">
            <?php
            $_POST['super'] = 0;
            $_POST['ativo'] = 1;
            $menus = $header->listObj();
            ?>
            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12 visible-xs'>
                <legend class="text-center title-side ">
                    Sistemas
                </legend>
            </div>
            <?php
            if (is_array($permitidos))
                foreach ($menus as $sub_item) {
                    if ($sub_item->getFkFerramenta() == 0) {
                        continue;
                    }
                    if (!in_array($sub_item->getId(), $permitidos)) {
                        continue;
                    }
                    ?>
                    <div class='col-xs-4 col-sm-3 col-md-4 col-lg-4 text-center quadro-icon visible-xs'
                         style="height:110px;">
                        <br>
                        <a class="text-icon" href="<?php echo appConf::caminho . $sub_item->getUrl() ?>">
                            <img width="58"
                                 src='/../../public/img/iconsFerramentas/<?php echo $sub_item->getImagem() ?>'></img><br>
                            <label><?php echo $sub_item->getCampo() ?></label>
                            <br>
                        </a>
                        <br>
                    </div>
                    <?php
                }
            $_POST['super'] = 3;
            $_POST['ativo'] = 1;
            $menus = $header->listObj();
            if (is_array($menus)) {
                ?>
                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                    <legend class="text-center title-side">
                        Ferramentas
                    </legend>
                </div>
                <?php
                if (is_array($permitidos))
                    foreach ($menus as $item) {
                        $_POST['super'] = $item->getId();
                        if (!in_array($item->getId(), $permitidos)) {
                            continue;
                        }
                        $escritos++;
                        $sub_menus = $header->listObj();
                        if (is_array($sub_menus)) {
                            ?>
                            <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                            <div class="panel panel-default">
                                <div class="panel-heading"><?php echo $item->getCampo() ?></div>
                            </div>
                            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10
                     col-xl-offset-1 col-sm-offset-1 col-md-offset-1 col-lg-offset-1">
                                <br>
                            </div>
                            </div><?php
                            if (is_array($permitidos))
                                foreach ($sub_menus as $sub_item) {
                                    if (!in_array($sub_item->getId(), $permitidos)) {
                                        continue;
                                    }
                                    $escritos++;
                                    if ($sub_item->getFkFerramenta() == "-2") {
                                        ?>
                                        <div class='col-xs-4 col-sm-3 col-md-4 col-lg-4 text-center quadro-icon'
                                             style="height:110px;">
                                            <a class="text-icon" target="_blank"
                                               href="<?php echo $sub_item->getUrl() ?>">
                                                <img width="58"
                                                     src='/../../public/img/iconsFerramentas/<?php echo $sub_item->getImagem() ?>'></img><br>
                                                <label><?php echo $sub_item->getCampo() ?></label>
                                            </a>
                                        </div>
                                        <?php
                                    } else if ($sub_item->getFkFerramenta() == '-1') {
                                        ?>
                                        <div class='col-xs-4 col-sm-3 col-md-4 col-lg-4 text-center quadro-icon'
                                             style="height:110px;">
                                            <a class="text-icon"
                                               href="<?php echo appConf::caminho . $sub_item->getUrl() ?>">
                                                <img width="58"
                                                     src='/../../public/img/iconsFerramentas/<?php echo $sub_item->getImagem() ?>'></img><br>
                                                <label><?php echo $sub_item->getCampo() ?></label>
                                            </a>
                                        </div>
                                        <?php
                                    } else {
                                        ?>
                                        <div class='col-xs-4 col-sm-3 col-md-4 col-lg-4 text-center quadro-icon'
                                             style="height: 110px;">
                                            <a class="text-icon"
                                               href="<?php echo appConf::caminho . "ferramenta/abrir/" . $sub_item->getFkFerramenta() . "/" . $sub_item->getCampo() ?>">
                                                <img width="58"
                                                     src='/../../public/img/iconsFerramentas/<?php echo $sub_item->getImagem() ?>'></img><br>
                                                <label><?php echo $sub_item->getCampo() ?></label>
                                            </a>
                                        </div>

                                        <?php
                                    }
                                }
                        }
                    }
                ?>
                <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>
                    <br>
                    <br>
                    <br>
                    <hr>
                </div>
            <?php } ?>
        </div>
        <?php
        return $escritos;
    }

    public function menuUsuario()
    {
        ?>
        <div class="row" style="margin-bottom: 0px; padding-bottom: 0px">
            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">
            </div>
            <div class="col-xs-4 col-sm-5 col-md-5 col-lg-5">
                <div class="circleImgGrd">
                    <img src="<?php echo appFunction::fotoColaborador(appFunction::dadoSessao('id_colaborador')) ?>"/>
                </div>
            </div>
            <div class="col-xs-7 col-sm-6 col-md-6 col-lg-6">
                <br>
                <h5><?php echo appFunction::tratarNome($_SESSION['nome']); ?><br>
                    <?php echo $_SESSION['setor'] ?></h5>
                <small><?php echo $_SESSION['unidade_neg'] ?></small>
                <div class="text-muted">
                    <small><?php echo $_SESSION['linha'] ?></small>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <hr>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a style="text-decoration: none!important; font-size: 14px; color: #0f0f0f"
               href="<?php echo appConf::caminho ?>meusDados"
               class="w3-bar-item  ">
                <span class="glyphicon glyphicon glyphicon-user ">
                </span> Meus dados
            </a>
        </div>
        <?php
        if($_SESSION['perfil']!=4){
        ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a style="text-decoration: none!important; font-size: 14px; color: #0f0f0f"
               href="<?php echo appConf::caminho ?>colaboradores" class="w3-bar-item  ">
                <img width="13" style="margin: 0px" src="/../../public/img/iconsFerramentas/users.png">
                </img>Colaboradores
            </a>
        </div>
        <?php
    }
    ?>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a style="text-decoration: none!important; font-size: 14px; color: #0f0f0f"
               href="<?php echo appConf::caminho ?>contato" class="w3-bar-item  ">
                <span class="glyphicon glyphicon glyphicon-phone ">
                </span> Fale Conosco
            </a>
        </div>
        <div style="text-decoration: none; font-size: 14px" class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <a style="text-decoration: none!important; font-size: 14px; color: #0f0f0f"
               href="<?php echo appConf::caminho ?>ajuda"
               class="w3-bar-item ">
                        <span class="glyphicon glyphicon glyphicon-question-sign ">
                        </span> Ajuda
            </a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right"
             style="margin-bottom: 0px!important; padding-bottom: 0px!important;">
            <hr>
            <button id="btnSairSistema"
                    class="btn btn-danger btn-sm" data-toggle="modal"
                    data-target="#modalLogout"
                    modulo="logout">
                        <span class="glyphicon glyphicon glyphicon-off ">
                        </span> Logout
            </button>
        </div>
        <?php
    }

}