<?php
/**
 * Created by PhpStorm.
 * User: J0021877
 * Date: 10/07/2018
 * Time: 12:23
 */
require_once('/../view/abs/gridView.php');
include_once('headerView.php');
include_once('/../controller/cRelatoriosController.php');

class vRelatorios extends gridView
{

    /**
     * função desativada
     * @return mixed
     */
    public function permissoes()
    {
        require_once('/../../sistemas/ControleNivel/app/controller/cControleHeaderNivel.php');
        $control_nivel = new cControleHeaderNivel();
        $_POST['fkperfil'] = $_SESSION['id_perfil'];
        $_POST['fkferramenta'] = $_SESSION['sistema_atual'];
        $permissao = $control_nivel->loadObj();
        return $permissao->nivelAcesso();
    }

    /**
     * construtor inicial da página, deve ser substituido por um router
     * vRelatorios constructor.
     */
    public function vRelatorios()
    {
        // $permissao = $this->permissoes();
        $this->content();
        $this->rodape();
    }

    public function rodape()
    {
        ?>
        <div id="home-rodape" style="position:fixed;bottom:0;width:100%;">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h5>
                            <p class="text-center">
                                <small>Portal Efetividade - Grupo NC</small>
                            </p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function arquivos($pasta)
    {
        $control = new cRelatoriosController();
        $arquivos = $control->listRelatoriosByPasta($pasta);
        if (is_array($arquivos)) {
            ?>
            <ul style="text-decoration:none;"><?php
                foreach ($arquivos as $arquivo) {
                    ?>
                    <li id="<?php echo $arquivo['ID'] ?>">
                        <img width="25" src="/public/img/ext/pdf.png"/>
                        <b><?php echo $arquivo['NOME'] . ".pdf" ?></b>
                    </li>
                    <?php
                }
                ?>
            </ul>
            <?php
        }
    }

    public function content()
    {
        $control = new cRelatoriosController();
        ?>
        <script>
            $(document).ready(function () {
                var idPasta = <?php echo $_SESSION['pasta_atual']?>;
                var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/carregaPasta/' + idPasta);
                document.getElementById("txtIdPasta").value = idPasta;
                document.getElementById("conteudoPasta").innerHTML = retorno;
            })
            function verificarmover(){
                if (idsMover.length > 0) {
                    $('#qtdArquivos').text(idsMover.length.toString());
                    $('#msgMover').show('fade');
                    setTimeout(function () {
                        verificarmover();
                    }, 1000);
                }else{
                    setTimeout(function () {
                        verificarmover();
                    }, 1000);
                }
            }
            $(document).ready(function () {
                setTimeout(function () {
                    verificarmover();
                }, 1000);
            });
        </script>
        <style>
            ul {
                list-style-type: none;
                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
            }

            #ctn_explorer {
                margin-top: 0px;
                margin-left: 0px;
                padding: 0;
                height: 100%;
                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
            }
            ::-webkit-scrollbar {
                width: 6px
            }

            ::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3)
            }

            ::-webkit-scrollbar-thumb {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, .3)
            }
            body{
                 font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif
             }
        </style>
        <div id="ctn_explorer">
            <div id="lateral" class="col-xs-4 col-sm-3"
                 style="background: rgb(42, 63, 84); color: rgb(255, 255, 255); min-height: 100%; paddin:0;margin:0; padding-bottom: 50px;">
                <br>
                <img width="90" class="center-block" src="/../../public/img/iconsFerramentas/logoEfet.png">

                <h5 class="text-center">
                    EFETIVIDADE
                </h5>
                <hr>
                <h4>
                    <span class="glyphicon glyphicon-stats"></span> Diretórios
                </h4>
                <div style="overflow: auto;">
                    <?php
                    $control->carregarListaDiretório();
                    ?>
                </div>
            </div>
            <div id="conteudoPasta">
                <?php
                $control->carregaPasta(0);
                ?>
            </div>
        </div>
        <script src="/../../public/js/relatorios.js"></script>
        <?php
        $modalUser = array(" Usuários", "closeUser", "Usuarios", "", "user");
        $modalUserEditar = array(" Usuários", "closeUserEditar", "UsuariosEditar", "warning", "userEditar");
        $modalExcluir = array("Excluir", "closeExcluir", "ExcluirSelecionado", "danger", "remove");
        $modalExcluirPasta = array("Excluir", "closeExcluirPasta", "ExcluirPastaSelecionada", "danger", "remove", "Deseja realmente excluir esta pasta e todos os arquivos?", 1);
        $modalRenomearPasta = array(" Renomear Pasta", "closeRenomearPasta", "RenomearPastaSelecionada", "primary", "edit", "Ocorreu um erro, por favor recarregue a página.", 0);
        $modalRenomearArqu = array(" Renomear Arquivo", "closeRenomearArqu", "RenomearArquSelecionada", "primary", "edit", "Ocorreu um erro, por favor recarregue a página.", 0);
        $modalPermissao = array(" Permissões", "closePermissaoPasta", "permissaoPasta", "warning", "user", "Ocorreu um erro, por favor recarregue a página.", 0);
        formsHtml::modalCadastrar();
        formsHtml::modalVisualizar("Enviar Arquivos");
        formsHtml::modalDefault($modalExcluir);
        formsHtml::modalDefault($modalExcluirPasta);
        formsHtml::modalDefault($modalRenomearPasta);
        formsHtml::modalDefault($modalRenomearArqu);
        formsHtml::modalDefault($modalPermissao);
        formsHtml::modalDefault($modalUser);
        formsHtml::modalDefault($modalUserEditar);
    }
}