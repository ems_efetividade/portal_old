<?php
require_once ('/abs/gridView.php');

class vSenhaAdmin extends gridView{

    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        $cols = array('ID','SENHA','FKUSUARIO','DATAEXPIRAR','DATAIN');
        $colsPesquisaNome = array('ID','SENHA','FKUSUARIO','DATAEXPIRAR','DATAIN');
        $colsPesquisa = array('id','senha','fkUsuario','dataExpirar','dataIn');
        $array_metodo = array('Id','Senha','FkUsuario','DataExpirar','DataIn');
        gridView::setGrid_titulo('SenhaAdmin');
        gridView::setGrid_sub_titulo('SenhaAdmin');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('SenhaAdmin');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }
}