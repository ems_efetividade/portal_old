<?php
require_once('lib/appConf.php');
require_once('lib/appFunction.php');
require_once('app/model/loginModel.php');
require_once('app/model/ferramentaModel.php');
require_once('sistemas/AceiteDeDocumentos/app/model/mDocumento.php');
require_once('/../model/mControleHeader.php');
class vnavbar{

    public function vnavbar(){
        $header = new mControleHeader();
        loginModel::validarSessao();
        $aceite = new mDocumento();
         ?>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="#">Navbar</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Dropdown
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link disabled" href="#">Disabled</a>
                    </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
        <?php
    }
/*
 * <div class="collapse navbar-collapse" id="navbar-collapse-1">
                <ul class="nav navbar-nav pull-right" id="menu-padrao">
                    <?php
                    $_POST['super'] = 0;
                    $_POST['ativo'] = 1;
                    $menus = $header->listObj();;
                    foreach ($menus

                    as $menu){
                    $_POST['super'] = $menu->getId();
                    $sub = $header->listObj();
                    if (is_array($sub)) { ?>
                    <li class="dropdown">
                        <a href="#" data-toggle='dropdown' data-tip='tooltip' data-placement='auto'
                           title='<?php echo $menu->getCampo(); ?>' class="dropdown-toggle">
                    <span class="glyphicon glyphicon-<?php echo $menu->getIcone(); ?>">
                    </span>
                            <b class="caret"></b>
                        </a>
                        <ul class="dropdown-menu ">
                            <?php
                            foreach ($sub

                            as $item){
                            $_POST['super'] = $item->getId();
                            $sub2 = $header->listObj();
                            if (is_array($sub2)) {
                            ?>
                            <li class="dropdown dropdown-submenu  ">
                                <a class="dropdown-toggle " data-toggle="dropdown" href="#">
                                    <span class="glyphicon glyphicon-<?php echo $item->getIcone(); ?>">&nbsp;</span>
                                    <?php echo $item->getCampo(); ?>
                                    <ul class="dropdown-menu pull-left">
                                </a>
                                <?php
                                foreach ($sub2

                                as $item2){
                                $link2 = appFunction::criarUrl(appConf::caminho . 'ferramenta/abrir/' . $item2->getFkFerramenta() . '/' . $item2->getCampo())
                                ?>
                            <li>
                                <a onclick="guardaMenu(<?php echo $item2->getID(); ?>)" class="dropdown-toggle "
                                   href="<?php echo $link2 ?>">
                                    <span class="glyphicon glyphicon-<?php echo $item->getIcone(); ?>">&nbsp;</span>
                                    <?php echo $item2->getCampo(); ?>
                                </a>
                            </li>
                        <?php
                        }
                        ?>
                        </ul>
                    </li>
                <?php
                } else {
                    $link = appFunction::criarUrl(appConf::caminho . 'ferramenta/abrir/' . $item->getFkFerramenta() . '/' . $item->getCampo())
                    ?>
                    <li class="">
                        <a
                                onclick="guardaMenu(<?php echo $item->getID(); ?>)" class="dropdown-toggle"
                                href="<?php echo $link ?>"><span
                                    class="glyphicon glyphicon-<?php echo $item->getIcone() ?>">&nbsp;</span> <?php echo $item->getCampo() ?>
                        </a>
                    </li>
                    <?php
                }
                }
                ?>
                </ul>
                </li>
                <?php
                }
                else {
                    ?>
                    <li>
                    <a data-tip='tooltip' data-placement='auto' title='<?php echo $menu->getCampo(); ?>'
                       onclick="guardaMenu(<?php echo $menu->getID(); ?>)"
                       href="<?php echo appConf::caminho ?><?php echo $menu->getUrl(); ?>" modulo="home"
                       class="link">
                <span class="glyphicon glyphicon-<?php echo $menu->getIcone(); ?>">
                </span>
                        <?php echo $menu->getCampo(); ?>
                    </a>
                    </li><?php
                }
                }

                ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img height="20"
                             src="<?php echo appFunction::fotoColaborador(appFunction::dadoSessao('id_colaborador')) ?>"/>
                        <?php echo appFunction::dadoSessao('nome'); ?>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li>
                            <a href="<?php echo appConf::caminho ?>configuracao" modulo="usuario" class="link">
                        <span class="glyphicon glyphicon glyphicon-cog">
                        </span> Configurações
                            </a>
                        </li>
                        <li><a href="#" modulo="contato" class="link" data-toggle="modal" data-target="#modalContato"><span
                                        class="glyphicon glyphicon glyphicon-cog"></span> Fale Conosco</a></li>
                        <li role="presentation" class="divider">
                        </li>
                        <li>
                            <a href="#" class="link" data-toggle="modal" data-target="#modalLogout" modulo="logout">
                        <span class="glyphicon glyphicon glyphicon-off">
                        </span> Sair do sistema
                            </a>
                        </li>
                    </ul>
                </li>
                </ul>
            </div>
 * */

}