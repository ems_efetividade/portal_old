<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/*
require_once('sExemplo.php');

echo '<h1>Testes</h1><br>';

$exemplo = new sExemplo();

$exemplo->setId(1);
$exemplo->setNome("Jefferson Maion de Oliveira");
$exemplo->setEmail("jefmaion@hotmail.com");

$exemplo->selecionar();

 */

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
require './src/Exception.php';
require './src/PHPMailer.php';
require './src/SMTP.php';


use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$mail = new PHPMailer();
$mail->IsSMTP();
$mail->SMTPSecure = "tls";
$mail->Host = "emsmail.ems.com.br"; // Endereço do servidor SMTP
$mail->Port = 25;

//Remetente
$mail->From = 'noreply@ems.com.br';
$mail->FromName = 'EMS';

//Destinatários
$mail->AddAddress("jefferson.oliveira@ems.com.br", 'Gustavo Pagotto');

$mail->IsHTML(true);

$mail->Subject = 'Teste Email';
$msg = 'TESTE';
$mail->Body = $msg;

print_r($mail->Send());
