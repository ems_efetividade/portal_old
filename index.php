<?php
// configuração de exibição de erros no PHP em modo desenvolvimento.
// Upload de arquivos de no maximo 500 mb
ini_set('upload_max_filesize', '500M');
ini_set('post_max_size', '500M');
ini_set('max_input_time', -1);
ini_set('max_execution_time', 0);

// Segurança de cookies
ini_set('session.cookie_secure', 1);
ini_set('session.cookie_httponly', 1);
ini_set('session.use_only_cookies', 1);

require_once('lib/appConexao.php');

class Framework
{

    protected static $controller = 'homeController';
    protected static $method = 'main';
    protected static $params = [];

    protected static $controller_path = 'app/controller/';

    public function __construct()
    {
    }

    static function parseUrl()
    {

        if (isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url']), FILTER_SANITIZE_URL));
        }
    }

    static function start()
    {


        // pega a url e gera um array
        $url = self::parseUrl();
        //guarda sistema atual
        self::checkMetodo($url);
        // verifica se existe o controlador
        if (file_exists(self::$controller_path . $url[0] . 'Controller.php')) {
            self::$controller = $url[0] . 'Controller';
            unset($url[0]);
        }


        // inclui o controlador na página
        require_once self::$controller_path . self::$controller . '.php';

        //instancia a classe do controlador
        self::$controller = new self::$controller;

        //verifica se existe o metodo na classe do controlador
        if (isset($url[1])) {
            if (method_exists(self::$controller, $url[1])) {
                self::$method = $url[1];
                unset($url[1]);
            }
        }

        // verifica se existe parametros
        self::$params = $url ? array_values($url) : [];
        ?>
        <?php
        call_user_func_array([self::$controller, self::$method], self::$params);
    }

    static function checkMetodo($url)
    {

        if (isset($url[0])) {
            $cn = new appConexao();

            $url[1] = (isset($url[1])) ? $url[1] : 'index';

            $query = "procAddMetodo '" . $url[0] . "', '" . $url[1] . "'";
            $cn->executar($query);
        }
    }

}

@Framework::start();


?>