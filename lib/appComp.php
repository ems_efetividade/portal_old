<?php
class appComp {

    private $folder;

    public function get($comp, $data = [], $string=false) {

        $folder =  $_SERVER['DOCUMENT_ROOT'] . "/app/components/";

        
        if(file_exists($folder . "{$comp}.comp.php")) {
            
   
            
            foreach($data as $chave => $item) {
                $$chave = $item;
            }

           //include $folder . "{$comp}.comp.php";

            
            ob_start();
            require($folder . "{$comp}.comp.php");

            if($string) {
                return ob_get_clean();
            }
            echo ob_get_clean();
        }
    }


}

function replaceTags($preg, $conf, $pattern='') {
    $flat = array();
    
    foreach($conf as $key => $var) {

        if(is_array($var)) {
            $flat[$key] =  replaceTags($preg, $var, $pattern);
        } else {

            $parametro = '';
            preg_match($preg, $var, $parametro);
        
            while(isset($parametro[0])) {
                $conf[$key] = str_replace($parametro[0], $pattern[$parametro[1]], $conf[$key]);
                preg_match($preg, $conf[$key], $parametro);
            }

            $flat[$key] = $conf[$key];
        }
    }

    return $flat;
}

?>