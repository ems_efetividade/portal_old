<?php
define("DS", DIRECTORY_SEPARATOR);
define("PORTAL_ROOT", getcwd() . DS);
define('PORTAL_URL', $_SERVER['HTTP_HOST']);
define('SERVER_URL', $_SERVER['HTTP_HOST']);
define('PORTAL_FOLDER', '/');
class appConf {
	const caminho = 'https://' . PORTAL_URL . PORTAL_FOLDER;
	const caminho_fisico = PORTAL_ROOT;
	const user_default_pass = '123';
	const user_login_time = 18000; //1 hor
	const folder_img_noticia = 'public/noticia/img/';
	const folder_img_noticia_fisico = 'public\\noticia\\img\\';
    const folder_relatorios = PORTAL_ROOT.'public/relatorios/';
    const folder_relatorios_fisico = PORTAL_URL.'\\public\\relatorios\\';
    const folder_politicas = PORTAL_ROOT.'public/politicas/';
    const folder_politicas_fisico = PORTAL_URL.'\\public\\politicas\\';
	const folder_foto_noticia = 'public/noticia/album/';
	const folder_foto_noticia_fisico = 'public\\noticia\\album\\';
	const folder_profile = 'public/profile/';
	const folder_profile_fisico = 'public\\profile\\';
	const folder_popup = 'public/popup/';
	const folder_popup_fisico = 'public\\popup\\';
	const folder_report = PORTAL_ROOT.'ARQUIVOS_PORTAL\\';
}
?>