<?php


/*define("DS", DIRECTORY_SEPARATOR);

define("ROOT", getcwd() . DS);


define("APP_PATH",          ROOT . 'app' . DS);

define("PUBLIC_PATH",       ROOT . "public" . DS);


define("LIB_PATH",          APP_PATH . "lib" . DS);

define("CONTROLLER_PATH",   APP_PATH . "controller" . DS);

define("MODEL_PATH",        APP_PATH . "model" . DS);

define("VIEW_PATH",         APP_PATH . "view" . DS);

define("ARQUIVOS_PATH",     APP_PATH . "uploads" . DS);

define("UPLOAD_PATH",       PUBLIC_PATH . "uploads" . DS);


define('APP_HOST', 'http://'. $_SERVER['HTTP_HOST']);



*/

/*
define("EMS_SERVER", "https://172.20.9.131/");
define("EMS_HTDOCS", "");
define("EMS_DB_CONNECT", $_SERVER['DOCUMENT_ROOT']."/".EMS_HTDOCS."lib/appConexao.php");
*/

require_once('appSanitize.php');
require_once('appComp.php');

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE ^ E_DEPRECATED);

define('EMS_HTTP', 'https://');
define('EMS_URL', EMS_HTTP.$_SERVER['HTTP_HOST']);
define("EMS_HTDOCS", "");
define("EMS_DB_CONNECT", $_SERVER['DOCUMENT_ROOT']."/".EMS_HTDOCS."lib/appConexao.php");
define("EMS_FOTO_PROFILE", $_SERVER['DOCUMENT_ROOT']."\\".EMS_HTDOCS."public\\profile\\");
