<script>
function exportar_gauge(valor) {
	
    var gaugeOptions = {
	
	    chart: {
	        type: 'solidgauge',
			margin: [0, 0, 0, 0],
			events: {
            load: function () {
					var ch = this;
					setTimeout(function(){
						//ch.exportChart();
					},1);
            	}
        	}
	    },
	    title :{
				text: ''	
			},
	    
	    pane: {
	    	center: ['50%', '85%'],
	    	size: '140%',
	        startAngle: -90,
	        endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
	    },

	    tooltip: {
	    	enabled: false
	    },
		exporting: {
			type: 'image/png',
			enabled: true,	
		},
	       
	    // the value axis
	    yAxis: {
			stops: [
				[0.0, '#DF5353'], // red
				[0.25, '#DF5353'], //red
				[0.26, '#DDDF0D'], // yellow
				[0.50, '#DDDF0D'], // yellow
				[0.51, '#55BF3B'], // green
				[0.75, '#55BF3B'], // green
				[0.76, '#3B5998'], // blue
				[1, '#3B5998'] // blue
        	],

			lineWidth: 0,
            minorTickInterval: 2,
			tickInterval:2,
            tickWidth: 1,
	        title: {
                y: -70
	        },
            labels: {
                y: 16,
				enabled: false
            }        
	    },
        
        plotOptions: {

            solidgauge: {
                dataLabels: {
                    y: 60,
                    borderWidth: 0,
                    useHTML: true,
                }
            }
        }
    };
    
    // The speed gauge
    $('#container-speed').highcharts(Highcharts.merge(gaugeOptions, {
        yAxis: {
	        min: 0,
	        max: 4,
	        title: {
	            text: ''
	        }       
	    },

	    credits: {
	    	enabled: false
	    },
	
	    series: [{
	        name: 'Speed',
	        data: [valor],
	        dataLabels: {
	        	format: '<div style="text-align:center"><span style="font-size:55px;color:' + 
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>'
	        },
	        tooltip: {
	            valueSuffix: ''
	        }
	    }]
	
	}));

}
</script>
<div id="container-speed" style="width: 300px; height: 200px; float: left"></div>
