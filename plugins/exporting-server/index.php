<?php
/**
 * This file is part of the exporting module for Highcharts JS.
 * www.highcharts.com/license
 * 
 *  
 * Available POST variables:
 *
 * $filename  string   The desired filename without extension
 * $type      string   The MIME type for export. 
 * $width     int      The pixel width of the exported raster image. The height is calculated.
 * $svg       string   The SVG source code to convert.
 */


// Options
//define ('BATIK_PATH', 'C:\xampp\htdocs\portal_px\exporting-server\batik-rasterizer.jar');

require_once('../../lib/appConf.php');
require_once('java_path.php');

$name = (isset($_POST['name'])) ? $_POST['name'] :  '';
$img = @$_POST['base64'];
$img = @str_replace('data:image/png;base64,', '', $img);
$img = @str_replace(' ', '+', $img);
$fileData = base64_decode($img);
//saving
$fileName = ($name == '') ? md5(round(microtime(true) * 1000)) : $name;
$path =  PORTAL_ROOT."files/".$fileName.'.png';
file_put_contents($path, $fileData);


$img = @$_POST['rnk1'];
$img = @str_replace('data:image/png;base64,', '', $img);
//$img = @str_replace(' ', '+', $img);
$fileData = base64_decode($img);
//saving

$path =  PORTAL_ROOT."files/".$fileName.'_rnk1.png';
file_put_contents($path, $fileData);

$img = @$_POST['rnk2'];
$img = @str_replace('data:image/png;base64,', '', $img);
//$img = @str_replace(' ', '+', $img);
$fileData = base64_decode($img);
//saving

$path =  PORTAL_ROOT."files/".$fileName.'_rnk2.png';
file_put_contents($path, $fileData);

echo $fileName;

/*

define ('BATIK_PATH', PORTAL_ROOT.'batik-rasterizer.jar');


//$java = "C:\ProgramData\Oracle\Java\javapath\java.exe";

//echo '<b>'.BATIK_PATH.'</b>';

///////////////////////////////////////////////////////////////////////////////
ini_set('magic_quotes_gpc', 'off');

$type = $_POST['type'];
$svg = (string) $_POST['svg'];
$filename = (string) $_POST['filename'];

// prepare variables
if (!$filename or !preg_match('/^[A-Za-z0-9\-_ ]+$/', $filename)) {
	$filename = 'chart';
}
if (get_magic_quotes_gpc()) {
	$svg = stripslashes($svg);	
}

// check for malicious attack in SVG
if(strpos($svg,"<!ENTITY") !== false || strpos($svg,"<!DOCTYPE") !== false){
	exit("Execution is topped, the posted SVG could contain code for a malicious attack");
}

$tempName = md5(rand());

// allow no other than predefined types
if ($type == 'image/png') {
	$typeString = '-m image/png';
	$ext = 'png';
	
} elseif ($type == 'image/jpeg') {
	$typeString = '-m image/jpeg';
	$ext = 'jpg';

} elseif ($type == 'application/pdf') {
	$typeString = '-m application/pdf';
	$ext = 'pdf';

} elseif ($type == 'image/svg+xml') {
	$ext = 'svg';

} else { // prevent fallthrough from global variables
	$ext = 'txt';
}

$outfile = PORTAL_ROOT."temp/".$tempName.".$ext";

if (isset($typeString)) {
	
	// size
	$width = '';
	if ($_POST['width']) {
		$width = (int)$_POST['width'];
		if ($width) $width = "-w $width";
	}

	// generate the temporary file
	if (!file_put_contents("temp/$tempName.svg", $svg)) { 
		die("Couldn't create temporary file. Check that the directory permissions for
			the /temp directory are set to 777.");
	}
	
	// Troubleshoot snippet
	//	$command = "java -jar ". BATIK_PATH ." $typeString -d $outfile $width temp/$tempName.svg 2>&1"; 
	//	$output = shell_exec($command);
	//	echo "Command: $command <br>";
	//	echo "Output: $output";
	//	die;
	
	// do the conversion
	$output = shell_exec($java_path." -jar ". BATIK_PATH ." $typeString -d $outfile $width  ".PORTAL_ROOT."temp/$tempName.svg");
	
	// catch error
	if (!is_file($outfile) || filesize($outfile) < 10) {
		echo "<pre>$output</pre>";
		echo "Error while converting SVG. ";
		//echo "java -jar ". BATIK_PATH ." $typeString -d $outfile $width  ".PORTAL_ROOT."temp/$tempName.svg";
		
		
		$command = "java -jar ". BATIK_PATH ." $typeString -d $outfile $width  ".PORTAL_ROOT."temp/$tempName.svg 2>&1"; 
		$output = shell_exec($command);
		echo "Command: $command <br>";
		echo "Output: $output";
		die;
		
		
		if (strpos($output, 'SVGConverter.error.while.rasterizing.file') !== false) {
			echo "
			<h4>Debug steps</h4>
			<ol>
			<li>Copy the SVG:<br/><textarea rows=5>" . htmlentities(str_replace('>', ">\n", $svg)) . "</textarea></li>
			<li>Go to <a href='http://validator.w3.org/#validate_by_input' target='_blank'>validator.w3.org/#validate_by_input</a></li>
			<li>Paste the SVG</li>
			<li>Click More Options and select SVG 1.1 for Use Doctype</li>
			<li>Click the Check button</li>
			</ol>";
		}
	} 
	
	// stream it
	else {
		//header("Content-Disposition: attachment; filename=\"$filename.$ext\"");
		//header("Content-Type: $type");
		//echo file_get_contents($outfile);
		
		copy($outfile, 'files/'.$filename.'.'.$ext);
		//echo 'files/'.$filename.'.'.$ext;
		echo $filename.'.'.$ext;
		
		
	}
	
	// delete it
	unlink("temp/".$tempName.".svg");
	unlink($outfile);

// SVG can be streamed directly back
} else if ($ext == 'svg') {
	header("Content-Disposition: attachment; filename=\"$filename.$ext\"");
	header("Content-Type: $type");
	echo $svg;
	
} else {
	echo "Invalid type";
}
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 * 
 */
?>

