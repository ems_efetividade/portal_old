$(document).ready(function () {
    var allRows = null;
    var qntColunas = 0;
    var assunto = null;
    var nomeRemetente = null;
    var remetente = null;
    var CCo = null;
    var Mensagem = null;

    Notiflix.Loading.Init({});
    Notiflix.Notify.Init({
        position: 'right-bottom'
    });
    Notiflix.Report.Init({});
    Notiflix.Confirm.Init({});

    $('#subirArqPx').click(function () {
        subirPXBanco(1);
        Notiflix.Loading.Pulse('Aguarde...');
    });


    function encode_utf8(s) {
        return unescape(encodeURIComponent(s));
    }

    function decode_utf8(s) {
        return decodeURIComponent(escape(s));
    }
    let sql;
    let formData = new FormData();
    function subirPXBanco(indice) {
        var formData = new FormData();
        if (allRows[indice] == null) {
            Notiflix.Loading.Remove();
            Notiflix.Notify.Success('Processo finalizado');
            setTimeout(function () {
                location.reload();
            }, 1000);
            return;
        }
        var cabecalho = allRows[0].split(";");
        sql = '';
        for (j = indice; j < indice + 100; j++) {
            if (allRows[j] == null) {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Processo finalizado');
                setTimeout(function () {
                    location.reload();
                }, 1000);
                return;
            }
            linha = allRows[j].split(";");
            sql += 'INSERT INTO PS_BASE (';
            for (i = 0; i < cabecalho.length; i++) {
                if (i > 0) {
                    sql += ',' + cabecalho[i];
                } else {
                    sql += cabecalho[i];
                }
            }
            sql += ') VALUES (';
            sql = sql.replace(/"/g, '');
            for (k = 0; k < linha.length; k++) {
                if (k > 0) {
                    sql += encode_utf8(',' + linha[k].replace(/"/g, "'"));
                } else {
                    sql += encode_utf8(linha[k].replace(/"/g, "'"));
                }
            }
            sql += ');';
            final = j;
        }
        console.log(final);
        formData.append('sql',sql)
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/Admin/subirPXBanco',
            data: formData,
            async: true,
            contentType: false,
            processData: false,
            success: function (retorno) {
                if(final==1200){
                    Notiflix.Loading.Remove();
                    Notiflix.Notify.Success('Processo finalizado');
                    setTimeout(function () {
                        location.reload();
                    }, 1000);
                    return;
                }
                subirPXBanco(final+1);
            }
        });
    }

    window.onload = function () {
        //Check the support for the File API support
        if (window.File && window.FileReader && window.FileList && window.Blob) {
            var fileSelected = document.getElementById('pxfile');
            fileSelected.addEventListener('change', function (e) {
                Notiflix.Loading.Pulse('Aguarde...');
                //Set the extension for the file
                var fileExtension = /text.*/;
                //Get the file object
                var fileTobeRead = fileSelected.files[0];
                //Check of the extension match
                if (fileTobeRead.type.match(fileExtension)) {
                    //Initialize the FileReader object to read the 2file
                    var fileReader = new FileReader();
                    fileReader.onload = function (e) {
                        successFunction(fileReader.result);
                    }
                    fileReader.readAsText(fileTobeRead);
                } else {
                    alert("Por favor selecione arquivo texto");
                }

            }, false);
        } else {
            alert("Arquivo(s) não suportado(s)");
        }
    }


    $('#atualizarPx').click(function () {
        Notiflix.Loading.Pulse('Executando...');
        var caminho = 'https://' + window.location.host + '/Admin/rodarBase';
        setTimeout(function () {
            retorno = requisicaoAjax(caminho);
            if (retorno == 0) {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Processo finalizado');
                setTimeout(function () {
                    location.reload();
                }, 1000);
            }
        }, 1000);

    });

    $('#limparPx').click(function () {
        Notiflix.Loading.Pulse('Executando...');
        var caminho = 'https://' + window.location.host + '/Admin/limparBasePx';
        setTimeout(function () {
            retorno = requisicaoAjax(caminho);
            if (retorno == 0) {
                Notiflix.Loading.Remove();
                Notiflix.Notify.Success('Processo finalizado');
            }
        }, 1000);

    });

    $('#btnEmail').click(function () {
        assunto = document.getElementById('assunto').value;
        nomeRemetente = document.getElementById('nomeremetente').value;
        remetente = document.getElementById('remetente').value;
        CCo = document.getElementById('cco').value;
        Mensagem = CKEDITOR.instances['txtTexto'].getData();
        dispararEmails(0);
    });


    $('#btnSenhaVerificar').click(function () {
        var cpf = document.getElementById('txtSenhaCpf').value;
        var caminho = 'https://' + window.location.host + '/Admin/loadNomeUsuarioByCpf/' + cpf;
        var retorno = requisicaoAjax(caminho);
        document.getElementById('txtSenhaUsuario').value = retorno;
    });


    $('#btnSenhaGerar').click(function () {
        var cpf = document.getElementById('txtSenhaCpf').value;
        var data = document.getElementById('txtSenhaData').value;
        var caminho = 'https://' + window.location.host + '/Admin/gerarSenha/' + cpf + '/' + data;
        var retorno = requisicaoAjax(caminho);
        document.getElementById('txtSenhaSenha').value = retorno;
    });

    $('#btn_resetar').click(function () {
        var setor = document.getElementById('setor').value;
        var caminho = 'https://' + window.location.host + '/Admin/resetar/' + setor;
        var retorno = requisicaoAjax(caminho);
        msgBox(retorno);
    });

    if (window.File && window.FileReader && window.FileList && window.Blob) {
        var fileSelected = document.getElementById('emailfile');
        fileSelected.addEventListener('change', function (e) {
            var txtExt = "text/plain";
            var csvExt = "application/vnd.ms-excel";
            var fileTobeRead = fileSelected.files[0];
            if (fileTobeRead.type.match(csvExt) || fileTobeRead.type.match(txtExt)) {
                var fileReader = new FileReader();
                fileReader.onload = function (e) {
                    successFunction(fileReader.result);
                };
                fileReader.readAsText(fileTobeRead);
            } else {
                alert("Formato do arquivo incorreto");
            }
        }, false);
    }

    $('#btnListarAlteracao').click(function () {
        var formData = new FormData();
        var tabela = document.getElementById("tabela_retorno");
        tabela.innerHTML = "";
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/Admin/listarAlteracoes/1',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                var row = tabela.insertRow(0);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);
                cell1.innerHTML = "#";
                cell2.innerHTML = "Nome";
                cell3.innerHTML = "Setor";
                cell4.innerHTML = "Setor_old";
                cell5.innerHTML = "E-mail";
                cell6.innerHTML = "Data";
                for (var i in obj) {
                    row = tabela.insertRow(1);
                    cell1 = row.insertCell(0);
                    cell2 = row.insertCell(1);
                    cell3 = row.insertCell(2);
                    cell4 = row.insertCell(3);
                    cell5 = row.insertCell(4);
                    cell6 = row.insertCell(5);
                    cell1.innerHTML = obj[i].id;
                    cell2.innerHTML = obj[i].NOME;
                    cell3.innerHTML = obj[i].SETOR;
                    cell4.innerHTML = obj[i].SETOR_OLD;
                    cell5.innerHTML = obj[i].EMAIL;
                    cell6.innerHTML = obj[i].DATA_IN;
                }
            }
        });
    });

    $('#btnPendencias').click(function () {
        var formData = new FormData();
        var tabela = document.getElementById("tabela_retorno");
        tabela.innerHTML = "";
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/Admin/listarPendenciasLogin',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                var obj = $.parseJSON(retorno);
                var row = tabela.insertRow(0);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                cell1.innerHTML = "Nome";
                cell2.innerHTML = "E-mail";
                for (var i in obj) {
                    row = tabela.insertRow(1);
                    cell1 = row.insertCell(0);
                    cell2 = row.insertCell(1);
                    cell1.innerHTML = obj[i].NOME;
                    cell2.innerHTML = obj[i].EMAIL;
                }
            }
        });
    });

    $('#btnAcessos').click(function () {
        var formData = new FormData();
        var tabela = document.getElementById("tabela_retorno");
        tabela.innerHTML = "";
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/Admin/criarNovosAcessos',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                console.log(retorno);
                //document.write(retorno)
                // var obj = $.parseJSON(retorno);
                // var row = tabela.insertRow(0);
                // var cell1 = row.insertCell(0);
                // var cell2 = row.insertCell(1);
                // cell1.innerHTML = "Nome";
                // cell2.innerHTML = "E-mail";
                // for (var i in obj) {
                //     row = tabela.insertRow(1);
                //     cell1 = row.insertCell(0);
                //     cell2 = row.insertCell(1);
                //     cell1.innerHTML = obj[i].NOME;
                //     cell2.innerHTML = obj[i].EMAIL;
                // }
            }
        });
    });

    function successFunction(data) {
        Notiflix.Loading.Remove();
        allRows = data.split(/\r?\n|\r/);
        var Colunas = allRows[0].split(';');
        qntColunas = Colunas.length;
    }

    function dispararEmails(indice) {
        var formData = new FormData();
        if (allRows[indice] == null) {
            alert('operação Finalizada');
            return;
        }
        var linha = allRows[indice].split(";");
        formData.append('assunto', assunto);
        formData.append('nomeRemetente', nomeRemetente);
        formData.append('remetente', remetente);
        formData.append('CCo', CCo);
        formData.append('Mensagem', Mensagem);
        if (qntColunas > 1) {
            if (qntColunas > 2) {
                formData.append('nomeDestino', linha[0]);
                formData.append('emailDestino', linha[1]);
                formData.append('quantidade', qntColunas);
                for (i = 2; i < qntColunas; i++) {
                    formData.append('col' + qntColunas, linha[i]);
                }
                formData.append('emailDestino', linha[1]);
            }
            formData.append('nomeDestino', linha[0]);
            formData.append('emailDestino', linha[1]);
        } else {
            formData.append('emailDestino', linha[0]);
        }
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/Admin/disparador',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                console.log(retorno);
                dispararEmails(indice + 1);
            }
        });
    }

});
