function logClick(id) {
    var elemento = document.getElementById('collapse' + id);
    if (elemento.className == 'panel-collapse collapse') {
        requisicaoAjax('https://' + window.location.host + '/cAjuda/salvarLog/' + id);
    }
}

function rating(id,nota) {
    var parametro = id + '-' + nota;
    var elemento = document.getElementById('pergunta' + id);
    requisicaoAjax('https://' + window.location.host + '/cAjuda/salvarRating/' + parametro);
    elemento.classList.remove("text-success");
    elemento.classList.remove("text-danger");
    if(nota==1){
        elemento.classList.add("text-success");
    }
    if(nota==0){
        elemento.classList.add("text-danger");
    }
}


