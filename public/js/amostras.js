var resultado;
var colunas;
var indice = 0;
var tempo = new Number();
var tempo = 0;
var tempoProcessado = 0;
var namesPost = [];
var posicoesPost = [];

if (window.File && window.FileReader && window.FileList && window.Blob) {
    var fileSelected = document.getElementById('termo');
    fileSelected.addEventListener('change', function (e) {
        modalAguarde();
        var txtExt = "text/plain";
        var csvExt = "application/vnd.ms-excel";
        var fileTobeRead = fileSelected.files[0];
        if (fileTobeRead.type.match(csvExt) || fileTobeRead.type.match(txtExt)) {
            var fileReader = new FileReader();
            fileReader.onload = function (e) {
                successFunction(fileReader.result);
            }
            fileReader.readAsText(fileTobeRead);
        }
        else {
            alert("Por favor selecione arquivo texto");
            modalAguarde("hide");
        }
    }, false);
}

$(document).ready(function () {
    $('.selectColunas').click(function () {
        $('#colunas_' + this.id).val(this.firstChild.value);
    });
});

function successFunction(data) {
    modalAguarde("hide");
    var allRows = data.split(/\r?\n|\r/);
    resultado = allRows;
    colunas = allRows[0].split(';');
    var optionsList = '<select class="form-control">';
    for (var i in colunas) {
        optionsList += '<option value="' + i + '">' + colunas[i] + '</option>';
    }
    optionsList += '</select>';
    $('.colunasCheck').html(optionsList);
    $('.apontadores').show();
    $('#btn-enviar-excel').show('fade');
}

function capturarCampos() {
    var campos  = $('#camposUpload');
    var limite = (campos[0].childNodes.length)-1;
    for(var i=0;i<=limite;i++){
        if((i%2)===1) {
            namesPost.push(campos[0].childNodes[i].childNodes[1].childNodes[1].name);
            posicoesPost.push(campos[0].childNodes[i].childNodes[1].childNodes[1].value);
        }
    }
}

$('#btn-enviar-excel').click(function () {
    $("#barraProgresso").show('fade');
    startCountdown();
    capturarCampos();
    $('.apontadores').hide();
    inserir(indice + 1);
});

function inserir(indice) {
    var allRows = resultado;
    var preco = (allRows.length) - 1;
    var formData = new FormData();
    var linha = allRows[indice].split(";");
    for(var i =0;i<=namesPost.length;i++) {
        formData.append(namesPost[i], linha[posicoesPost[i]]);
    }
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/AMOSTRAS/RecebimentoAmostrasProduto/controlSwitch/inserir',
        data: formData,
        contentType: false,
        processData: false,
        success: function (retorno) {
            document.getElementById("quantidadeIns").value = parseInt(indice);
            var porcentagem = indice;
            var total = (porcentagem * 100) / preco;
            document.getElementById("barra").style.width = total + "%";
            document.getElementById("contador").innerText = indice + " de " + preco + " Linha(s) inseridas."
            if (parseInt(total) < 100) {
                document.getElementById("textobarra").innerText = parseInt(total) + "%";
                inserir(indice+1);
            } else {
                var tempoFinal = document.getElementById("tempo").innerText;
                document.getElementById("textobarra").innerText = "Finalizado em " + tempoFinal;
                $('#tempo').hide();
            }
        }
    });
}

$("#termo").change(function () {
    input = document.getElementById("termo");
    nome = "Não há arquivo selecionado. Selecionar arquivo...";
    if (input.files.length > 0)
        nome = input.files[0].name;
    document.getElementById("nomeTermo").innerText = nome;
});

function startCountdown() {
    var min = parseInt(tempo / 60);
    var seg = tempo % 60;
    if (min < 10) {
        min = "0" + min;
        min = min.substr(0, 2);
    }
    if (seg <= 9) {
        seg = "0" + seg;
    }
    tempoProcessado = ' 00:' + min + ':' + seg;
    document.getElementById("tempo").innerText = tempoProcessado;
    setTimeout('startCountdown()', 1000);
    tempo++;

}