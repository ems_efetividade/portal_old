Notiflix.Report.Init({});
Notiflix.Loading.Init({});
Notiflix.Notify.Init({backOverlay: true, distance: "5%", timeout: 2000});

function buscarTermo() {
    return document.getElementById('termo').innerHTML;
}

function enviarDados() {
    arrayLotes = document.getElementById('arrayLotes').value;
    estabelecimento = document.getElementsByClassName('estabelecimento');
    receptor = document.getElementsByClassName('receptor');
    distribuidor = document.getElementsByClassName('distribuidor');
    for (i = 0; i < receptor.length; i++) {
        if (distribuidor[i].value == '') {
            Notiflix.Report.Warning('Atenção!', 'Quantidade em estoque no distribuidor do lote ' + distribuidor[i].id + ' não informada', 'Fechar');
            return;
        }
        if (estabelecimento[i].value == '') {
            Notiflix.Report.Warning('Atenção!', 'Quantidade distribuída aos estabelecimentos receptores do lote ' + estabelecimento[i].id + ' não informada', 'Fechar');
            return;
        }
        total = parseInt(distribuidor[i].value) + parseInt(estabelecimento[i].value)
        if ((total > receptor[i].value) || (total < receptor[i].value)) {
            Notiflix.Report.Failure('Atenção!', 'A soma do valor informado no lote ' + estabelecimento[i].id + ' deve ser igual a quantidade de amostras recebidas', 'Fechar');
            return;
        }
    }
    valorD = new Array();
    idD = new Array();
    valorE = new Array();
    idE = new Array();
    valorR = new Array();
    idR = new Array();
    for (i = 0; i < receptor.length; i++) {
        valorD.push(distribuidor[i].value);
        idD.push(distribuidor[i].id);
        valorE.push(estabelecimento[i].value);
        idE.push(estabelecimento[i].id);
        valorR.push(receptor[i].value);
        idR.push(receptor[i].id);
    }
    maximo = receptor.length;
    for (i = 0; i < maximo; i++) {
        document.getElementById('dist' + idD[i]).innerHTML = '<span>' + valorD[i] + '</span>';
        document.getElementById('est' + idE[i]).innerHTML = '<span>' + valorE[i] + '</span>';
        document.getElementById('rec' + idR[i]).innerHTML = '<span>' + valorR[i] + '</span>';
    }
    document.getElementById('assinatura').innerHTML = '<span> Assinatura Digital </span>';
    termo = buscarTermo();
    Notiflix.Loading.Standard('Enviando Dados...');
    formData = new FormData();
    formData.append('termo', termo);
    formData.append('arrayLotes', arrayLotes);
    setTimeout(function () {
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/cTermoAnvisa/salvarTermo',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                Notiflix.Loading.Remove();
                if (retorno == 0) {
                    Notiflix.Notify.Success('Termo Gerado com Sucesso!\nAguarde...');
                    setTimeout(function () {
                        Notiflix.Loading.Standard('Redirecionando...');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    }, 2000);
                }
            }
        });
    }, 2000);
}