function subirArquivos() {
    var input = document.getElementById("termo");
    if (input.files.length == 0) {
        msgBox("Nenhum arquivo selecionado!")
        return;
    }
    arquivosUploadTotal = input.files.length;
    tabelaIndice = document.getElementById("arquivos").lastElementChild.children;
    upload(input, 0);
}

var arquivosUpload = 0;
var arquivosUploadTotal = 0;

function upload(file, indice) {
    if (file.files[indice] == null) {
        return;
    }
    var formData = new FormData();
    formData.append('nome', file.files[indice].name);
    formData.append('arquivo', file.files[indice]);
    $('#btnSubirArqu').fadeOut(1000);
    $.ajax({
        type: "POST",
        url: $('#formUpload').attr('action'),
        data: formData,
        contentType: false,
        processData: false,
        success: function (retorno) {
            console.log(retorno);
            if (parseInt(retorno) == 1) {
                arquivosUpload += 1;
                var total = (arquivosUpload * 100) / arquivosUploadTotal;
                document.getElementById("barra").style.width = total + "%";
                $("#arq" + (indice + 1)).addClass('alert alert-success');
                $("#arqStatus" + (indice + 1)).text("Enviado");
                document.getElementById("contadorArqu").innerHTML = arquivosUpload + ' de ' + arquivosUploadTotal + ' arquivo(s) enviado(s)';
                if (arquivosUpload == arquivosUploadTotal) {
                    arquivosUploadTotal = 0;
                    arquivosUpload = 0;
                    modalAguarde();
                    setTimeout(function () {
                        modalAguarde('hide');
                        if (chave) {
                            msgBox('Processo Finalizado com falhas.<br>Envie os arquivos novamente ou entre em contato com o suporte do sistema.');
                            chave = false;
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
            } else if (parseInt(retorno) == 2) {
                chave = true;
                arquivosUpload += 1;
                var total = (arquivosUpload * 100) / arquivosUploadTotal;
                document.getElementById("barra").style.width = total + "%";
                $("#arq" + (indice + 1)).addClass('alert alert-danger');
                $("#arqStatus" + (indice + 1)).text("Falha");
                document.getElementById("contadorArqu").innerHTML = arquivosUpload + ' de ' + arquivosUploadTotal + ' arquivo(s) enviado(s)';
                if (arquivosUpload == arquivosUploadTotal) {
                    arquivosUploadTotal = 0;
                    arquivosUpload = 0;
                    modalAguarde();
                    setTimeout(function () {
                        modalAguarde('hide');
                        if (chave) {
                            msgBox('Processo Finalizado com falhas.<br>Envie os arquivos novamente ou entre em contato com o suporte do sistema.');
                            chave = false;
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
            }
            upload(file, indice + 1);
        }
    });

}


function formatarTamanho(tamanho) {
    var kb = 1024;
    var mb = 1048576;
    var gb = 1073741824;
    var tb = 1099511627776;
    if (tamanho < kb) {
        return (tamanho.toFixed(2) + " bytes");
    } else if (tamanho >= kb && tamanho < mb) {
        var kilo = tamanho / kb;
        return (kilo.toFixed(2) + " KB");
    } else if (tamanho >= mb && tamanho < gb) {
        var mega = tamanho / mb;
        return (mega.toFixed(2) + " MB");
    } else if (tamanho >= gb && tamanho < tb) {
        var giga = tamanho / gb;
        return (giga.toFixed(2) + " GB");
    }
}

function gerarListaUpload() {
    var input = document.getElementById("termo");
    if (input.files.length == 0) {
        $('#modalVisualizar').hide();
        return;
    }
    $('.opl_div').hide("slow");
    $('.tb_div').show("slow");

    if (input.files.length > 0) {
        var indice = input.files.length;
        for (i = 0; i < input.files.length; i++) {
            var table = document.getElementById("arquivos");
            var row = table.insertRow(1);
            var cell5 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            row.id = "arq" + indice;
            cell1.innerHTML = input.files[i].name;
            cell2.innerHTML = input.files[i].type;
            cell3.innerHTML = formatarTamanho(input.files[i].size);
            cell4.innerHTML = '<span id="arqStatus' + indice + '">Aguardando.</span>';
            cell5.innerHTML = indice--;
        }
    }
}

function excluir(id) {
    var formData = new FormData();
    formData.append('id', id);
    $.ajax(
        {
            type: "POST",
            url: 'https://' + window.location.host + '/cAssinaturasDocumentos/controlSwitch/excluir',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                msgBox(retorno);
            }
        });
}

function salvarLinha(id) {
    expira = document.getElementById(id + 'expira').value;
    inicio = document.getElementById(id + 'inicio').value;
    empresa = document.getElementById(id + 'Empresa');
    qtdEmpresa = empresa.childElementCount - 1;
    empresas = ''
    for (i = 0; i <= qtdEmpresa; i++) {
        if (empresa[i].selected)
            empresas += empresa[i].value + '|';
    }
    unidade = document.getElementById(id + 'Unidade');
    qtdUnidade = unidade.childElementCount - 1;
    unidades = ''
    for (i = 0; i <= qtdUnidade; i++) {
        if (unidade[i].selected)
            unidades += unidade[i].value + '|';
    }
    linha = document.getElementById(id + 'Linha');
    qtdlinha = linha.childElementCount - 1;
    linhas = ''
    for (i = 0; i <= qtdlinha; i++) {
        if (linha[i].selected)
            linhas += linha[i].value + '|';
    }
    perfil = document.getElementById(id + 'Perfil');
    qtdPerfil = perfil.childElementCount - 1;
    perfis = ''
    for (i = 0; i <= qtdPerfil; i++) {
        if (perfil[i].selected)
            perfis += perfil[i].value + '|';
    }
    var formData = new FormData();
    formData.append('id', id);
    formData.append('inicio',inicio);
    formData.append('expira',expira);
    formData.append('empresa',empresas);
    formData.append('unidade',unidades);
    formData.append('linha',linhas);
    formData.append('perfil',perfis);
    $.ajax(
        {
            type: "POST",
            url: 'https://' + window.location.host + '/cAssinaturasDocumentos/guardar',
            data: formData,
            contentType: false,
            processData: false,
            success: function (retorno) {
                msgBox(retorno);
            }
        });
}


