var serverUrl = 'https://' + window.location.host
var serverChart = serverUrl + '/home/getDadosGraficoHome/';

$(document).ready(function (e) {
    if ($('.imagemPopup').attr('src') != "") {
        $('#modalPopupView').modal('show');
    }
    $('#width').html(screen.width + 'x' + screen.height);
    graficosHome();
    var a = $('#panel-grafico').height() + 3;
    var b = $('#panel-colab').height() + 20;
    c = (a + b);
    $('#panel-info').height(c);
    $('#carousel-informativo .item').height((c) - 80);
});

$(window).ready(function (e) {
    $('#tabs a').click(function (e) {
        chart = $(this).attr('chart');
        $('#' + chart).html("");
        setTimeout(function () {
            eval(chart + "()");
        }, 300);
    });
    $('.nav-informativo').click(function (e) {
        $('#carousel-informativo').carousel($(this).attr('acao'));
    });
});

function chart1() {

    $('#chart1').html();
    $.ajax({
        type: "POST",
        url: serverChart + '2',
        dataType: 'json',
        success: function (u) {
            $('#chart1').highcharts({
                chart: {
                    type: 'column'
                },
                title: {
                    text: u.titulo
                },
                subtitle: {
                    text: u.subtitulo
                },
                xAxis: {
                    categories: u.categories
                },
                yAxis: [{
                    min: 0,
                    title: {
                        text: u.texto_esquerda
                    }
                }, {
                    title: {
                        text: u.texto_direita,
                        style: {
                            color: '#1E90FF'
                        }
                    },
                    opposite: true
                }],
                tooltip: {
                    valueDecimals: 3,
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>R$ {point.y}</b><br/>',
                    shared: true
                },
                plotOptions: {
                    column: {
                        grouping: false,
                        shadow: false,
                        borderWidth: 0
                    }
                }, exporting: {
                    enabled: false
                },
                series: u.serie
            });
        }
    });

};

function chart2() {
    $.ajax({
        type: "POST",
        dataType: 'json',
        url: serverChart + '3',
        success: function (u) {
            $('#graf2-texto').html(u.texto);
            $('#chart2').highcharts({
                chart: {
                    type: 'column',
                    plotBackgroundImage: 'http://charmingprince.webege.com/images/fundo.png',
                },
                title: {
                    text: u.titulo
                },
                subtitle: {
                    text: u.subtitulo
                },
                xAxis: {
                    categories: u.categories
                },
                yAxis: {
                    title: {
                        text: u.texto_esquerda
                    }
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">Evolução TOP 5</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> <br/>'
                },
                exporting: {
                    enabled: false
                },
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    colors: u.serie[0].colors,
                    data: u.serie[0].data
                }],
            });
        }
    });
}

function chart4() {
    $.ajax({
        type: "POST",
        url: serverChart + '4',
        dataType: 'json',
        success: function (u) {
            $('#graf4-texto').html(u.texto);
            $('#chart4').highcharts({
                title: {
                    text: u.titulo,
                    x: -20
                },
                subtitle: {
                    text: u.subtitulo,
                    x: -20
                },
                xAxis: {
                    categories: u.categories
                },
                yAxis: {
                    title: {
                        text: u.texto_esquerda
                    },
                    plotLines: [{
                        value: 0,
                        width: 1,
                        color: '#808080'
                    }]
                },
                exporting: {
                    enabled: false
                },
                tooltip: {
                    pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b> {point.y}</b><br/>',
                    shared: true,
                    valueSuffix: ' %'
                },
                legend: {
                    layout: 'vertical',
                    align: 'right',
                    verticalAlign: 'middle',
                    borderWidth: 0
                },
                series: u.serie
            });
        }
    });
}

function graficosHome() {
    chart1();
    $.ajax({
        type: "POST",
        url: serverChart + '1',
        dataType: 'json',
        success: function (u) {
            if (u.serie) {
                $('#graf1-atualizacao').html(u.atualizacao);
                $('#graf1-texto').html(u.texto);
                $('#grafico-home1').highcharts({
                    chart: {
                        type: u.tipo
                    },
                    title: {
                        text: u.titulo
                    },
                    subtitle: {
                        text: u.subtitulo
                    },
                    xAxis: {
                        categories: u.categories
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: u.texto_esquerda
                        }
                    },
                    plotOptions: {
                        line: {
                            dataLabels: {
                                enabled: false
                            },
                            enableMouseTracking: true
                        }
                    },
                    exporting: {
                        enabled: false
                    },
                    tooltip: {
                        pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>R$ {point.y}</b> milhões<br/>',
                        shared: true,
                        valueDecimals: 3
                    },
                    series: u.serie
                });
            }
        }
    });
}

$(document).ready(function (e) {
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //alert(e.target.result);
                $('#fotoConv').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

    function alerta(texto, classe) {
        $('#alerta').hide();
        $('#alerta').removeClass('alert-danger');
        $('#alerta').removeClass('alert-info');
        $('#alerta').removeClass('alert-success');
        $('#alerta').addClass(classe);
        $('#alerta').html(texto);
        $('#alerta').show();
    }

    $('#btnSelFoto').click(function (e) {
        $('#alerta').hide();
        $('#foto').click();
    });

    $('#foto').change(function (e) {
        alerta('<span class="glyphicon glyphicon-refresh"></span> Aguarde, fazendo o upload da sua foto!', 'alert-info');
        readURL(this);
        $.ajax({
            type: "POST",
            url: $('#formFoto').attr('action'),
            data: new FormData($('#formFoto')[0]),
            contentType: false,
            processData: false,
            success: function (retorno) {
                dados = $.parseJSON(retorno);
                if ($.trim(dados.erro) == "OK") {
                    alerta('<span class="glyphicon glyphicon-ok"></span> Sucesso! Sua foto foi enviada com sucesso!', 'alert-success');
                    setTimeout(function () {
                        location.reload();
                    }, 3000);
                } else {
                    alerta('<b><span class="glyphicon glyphicon-remove"></span> Ops!</b> ' + dados.erro, 'alert-danger');
                }
            },
        });
    });
});