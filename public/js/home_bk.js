$(window).ready(function(e) {
	
   	$('#tabs a').click(function(e) {
		
		chart = $(this).attr('chart');
		
		$('#'+chart).html("");
		setTimeout(function() {
			eval(chart + "()");
		  }, 300);
    	
	});
	
	
	$('.nav-informativo').click(function(e) {
        $('#carousel-informativo').carousel($(this).attr('acao'));
    });
	
	
	
});

function chart1() {
	$('#chart1').html();
	$('#chart1').highcharts({
       chart: {
            type: 'column'
        },
        title: {
            text: 'Vendas EMS Prescrição - Preço Fábrica em Reais (R$)'
        },
        subtitle: {
            text: '2017 - Mês a Mês em Reais(000)'
        },
        xAxis: {
            categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
                'Jul', 'Aug', 'Set', 'Out', 'Nov', 'Dez']
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Vendas '
            }
        }, {
            title: {
                text: '2017',
                style: {
                    color: '#1E90FF'
                }
            },
            opposite: true
        }],
        tooltip: {
           valueDecimals: 3,
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>R$ {point.y}</b><br/>',
            shared: true
        },
        plotOptions: {
            column: {
                grouping: false,
                shadow: false,
                borderWidth: 0
            }
        },exporting: {
			enabled:false
		},
        series: [{
            name: 'Objetivo 2017',
            color: '#BF0B23',
            data: [89.016,96.974,129.946,80.978,149.966,147.053,137.978,149.948,126.060,148.932,139.047,143.990
]
        }, {
            name: 'Realizado 2017',
            color: '#1E90FF',
            data: [90.134,104.131,136.226,92.504,146.071,141.188,112.669,137.691,128.168]
        }]
    });
};

function chart2() {
	Highcharts.data({
        csv: document.getElementById('tsv').innerHTML,
        itemDelimiter: '\t',
        parsed: function (columns) {

            var brands = {},
                brandsData = [],
                versions = {},
                drilldownSeries = [];

            // Parse percentage strings
            columns[1] = $.map(columns[1], function (value) {
                if (value.indexOf('%') === value.length - 1) {
                    value = parseFloat(value);
                }
                return value;
            });

            $.each(columns[0], function (i, name) {
                var brand,
                    version;

                if (i > 0) {

                    // Remove special edition notes
                    name = name.split(' -')[0];

                    // Split into brand and version
                    version = name.match(/([0-9]+[\.0-9x]*)/);
                    if (version) {
                        version = version[0];
                    }
                    brand = name.replace(version, '');

                    // Create the main data
                    if (!brands[brand]) {
                        brands[brand] = columns[1][i];
                    } else {
                        brands[brand] += columns[1][i];
                    }

                    // Create the version data
                    if (version !== null) {
                        if (!versions[brand]) {
                            versions[brand] = [];
                        }
                        versions[brand].push(['v' + version, columns[1][i]]);
                    }
                }

            });

            $.each(brands, function (name, y) {
                brandsData.push({
                    name: name,
                    y: y,
                    drilldown: versions[name] ? name : null
                });
            });
            $.each(versions, function (key, value) {
                drilldownSeries.push({
                    name: key,
                    id: key,
                    data: value
                });
            });

            // Create the chart
            $('#chart2').highcharts({
                chart: {
                    type: 'column',
                    plotBackgroundImage: 'http://charmingprince.webege.com/images/fundo.png',
                   
                },
                title: {
                    text: 'Evolução TOP 5  Nacionais'
                },
                subtitle: {
                    text: 'Agosto 2016 x 2017 - PMB em Reais(000)'
                },
                xAxis: {
                    type: 'category'
                    
                    
                    
                },
                yAxis: {
                    max: 20,
                    title: {
                        text: 'Percentual'
                    }
					
                },
                legend: {
                    enabled: false
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        }
                    }
                },

                tooltip: {
                    headerFormat: '<span style="font-size:11px">Evolução TOP 5</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> <br/>'
                },
			exporting: {
						enabled:false
					},
                series: [{
                    name: 'Brands',
                    colorByPoint: true,
                    colors: ['#000000','#00008B','#CD2990','#FFD700','#00B2EE ','#9ACD32'],
                    data: brandsData
                }],
                
            });
            
        }
    });
}

function chart4() {
	$('#chart4').highcharts({
      title: {
            text: 'Prescription Share por Linha',
            x: -20 //center
        },
        subtitle: {
            text: 'Ago 2016 x Ago 2017',
            x: -20
        },
        xAxis: {
            categories: ['Ago/16','Set/16','Out/16','Nov/16','Dez/16','Jan/16','Fev/17','Mar/17','Abr/17','Mai/17','Jun/17','Jul/17','Ago/17']
        },
        yAxis: {
            title: {
                text: 'Percentual (%)'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
		exporting: {
			enabled:false
		},
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b> {point.y}</b><br/>',
            shared: true,
			valueSuffix: ' %'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Linha Sirius',
            data: [14.54,15.09,14.56,14.47,14.16,13.82,13.44,13.47,13.76,13.88,13.69,14.03,14.26


]
        }, {
            name: 'Linha Sinapse',
            data: [13.30,13.34,13.49,13.64,13.31,12.97,12.72,12.58,12.76,12.71,12.89,13.25,13.42


]
        }, {
            name: 'Linha Saúde',
            data: [15.02,14.38,14.10,14.21,14.20,14.40,14.27,13.46,12.76,12.59,12.40,12.84,13.30


]
        }, {
            name: 'Linha Vital',
            data: [17.64,18.34,17.94,17.77,17.34,16.68,16.44,16.23,16.62,16.74,16.72,17.04,17.44


]
        },{
            name: 'Linha Fênix',
            data: [16.58,16.64,16.63,16.96,17.09,16.70,16.42,16.39,17.01,17.30,17.40,17.63,17.88


]
        }, {
            name: 'Linha Clínica',
            data: [22.08,22.37,22.72,22.60,22.05,21.28,20.38,20.46,20.64,20.47,20.63,20.70,20.52


]
        }]
    });	
}

function graficosHome() {
	chart1();


	$('#grafico-home1').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ' Demanda EMS Prescrição'
        },
        subtitle: {
            text: '2016, 2017 e Projeção Último Mês - PMB em Reais(000)'
        },
        xAxis: {
            categories: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez']
        },
        yAxis: {
			min: 0,
            title: {
                text: 'Reais (R$)'
            }
			        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: false
                },
                enableMouseTracking: true
            }
        },
		exporting: {
			enabled:false
		},
        tooltip: {
            pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>R$ {point.y}</b> milhões<br/>',
            shared: true,
			valueDecimals: 3
        },

        series: [{
            name: 'EMS Prescrição 2016',
            data: [83.131,88.750,107.115,100.394,106.389,108.299,109.864,116.604,113.663,112.167,110.604,116.336
],
            color: '#1E90FF',
   
          }, {
                name: 'EMS Prescrição 2017',
                data: [ 105.413,102.833,128.070,107.550, 123.987,124.387,124.013,132.996

],
              color: '#0000ff',
              
            }, {
                name: 'Objetivo 2017',
                data: [106.877,108.092,134.547,107.289,128.166,131.924,131.692,141.735,130.052,139.716,138.398,141.333
],
              color: '#FF0000',
              
            }]
        });
		
		
	
}