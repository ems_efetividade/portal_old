$(document).ready(function (e) {
    $('ul.dropdown-menu [data-toggle=dropdown]').on('click', function (event) {
        event.preventDefault();
        event.stopPropagation();
        $(this).parent().siblings().removeClass('open');
        $(this).parent().toggleClass('open');
    });
});

function dropNav(id) {
    var x = document.getElementById(id);
    if (x.className.indexOf("w3-show") == -1) {
        x.className += " w3-show";
    } else {
        x.className = x.className.replace(" w3-show", "");
    }
}

function botaoAjuda() {

    var css = '<style>.btnAjuda {\n' +
        '    background-color: #428bca ; \n' +
        '    border: none;\n' +
        '    color: white;\n' +
        '    text-align: center;\n' +
        '    text-decoration: none;\n' +
        '    font-size: 16px;\n' +
        '    -webkit-transition-duration: 0.4s; /* Safari */\n' +
        '    transition-duration: 0.4s;\n' +
        '    cursor: pointer;\n' +
        '    border-radius: 200%;' +
        '    max-width: 50px;' +
        '    max-height: 50px;' +
        '    min-width: 50px;' +
        '    min-height: 50px;' +
        '    float: right;' +
        '    position: absolute;' +
        '    position: fixed;' +
        '    float: bottom;' +
        '    bottom: 45px;' +
        '    right: 25px;' +
        '    z-index: 100;' +
        '}\n' +
        '\n' +
        '.btnAjuda {\n' +
        '    background-color:  rgba(66,139,202,0); \n' +
        '    font-size: 30px;\n' +
        '    color: rgba(66,139,202,0.5); \n' +
        '}\n' +
        '\n' +
        '.btnAjuda:hover {\n' +
        '    background-color:  rgba(255,255,255,0); \n' +
        '    font-size: 50px;\n' +
        '    color: #2f4368; \n' +
        '}' +
        '</style>';
    var btn = "<button data-toggle='modal' data-target='#modalAjuda' data-toggle='tooltip' data-placement='left' title='Precisa de Ajuda?' class='btnAjuda'>" +
        "<span  class='glyphicon glyphicon-question-sign'></span>" +
        "</button>";
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-tip="tooltip"]').tooltip();
        $('[data-tip="tooltip"]').mouseleave(function () {
            $('[data-tip="tooltip"]').tooltip('hide');
        });

    });
    document.getElementById("botaoAjudaPadrao").innerHTML = css + btn;

}

function chamaAjuda() {
    var id = localStorage.getItem('idMenu');
    alert(id);
}

$(document).ready(function () {

    $('[data-toggle="popover"]').popover({
        trigger: 'manual',
        html: true,
        title: 'Título popover',
        content: $('#div-popover').html()
    }).click(function (e) {
        e.preventDefault();
        $(this).popover('show');
    });
});

function trocarEmpresa(id) {
    requisicaoAjax('https://' + window.location.host + '/login/controlarEmpresa/' + id);
    modalAguarde();
    setTimeout(function () {
        location.reload();
    }, 1000);
}

//### função que chama uma pagina em ajax
function requisicaoAjax(caminho) {
    var requisicao;
    //aguarde(1);
    $.ajaxSetup({async: false});
    $.post(caminho, function (retorno) {
        requisicao = retorno
    }).fail(function (xhr, textStatus, errorThrown) {
        //alert('ERRO: '+ xhr.responseText + ' ' +caminho);
    });
    $.ajaxSetup({async: true});
    //aguarde(0);
    return requisicao;
}

function msgBox(texto) {
    $('#modalMsgBox #msgboxTexto').html(texto);
    $('#modalMsgBox').modal('show');
}

function enviarFormulario(form, callback, msgbox) {

    $.ajax({
        type: "POST",
        url: form.attr('action'),
        data: form.serialize(),
        success: function (u) {

            if (u == '') {
                window.location.href = $(location).attr('href');
            } else {
                msgBox(u);
                $('#alertErro').html(u).show();
                setTimeout(function () {
                    $('#alertErro').hide();
                }, 5000);
            }
        }
    })
}

function modalAguarde(ctrl) {
    $('#modalAguarde').modal({
        backdrop: 'static',
        keyboard: false
    });
    $('#modalAguarde').modal(ctrl);
}

$(document).ready(function () {
    $('[data-toggle="popover"]').popover();
});

function w3_open() {
    var windowWidth = window.innerWidth;
    if (windowWidth <= 768) {
        document.getElementById("mySidebar").style.width = "100%";
    } else {
        document.getElementById("mySidebar").style.width = "30%";
    }
    document.getElementById("mySidebar").style.display = "block";
    var btnAbrir = document.getElementById("btnAbrir");
    var btnFechar = document.getElementById("btnFechar");
    $(btnAbrir).hide();
    $(btnFechar).show();
}

function w3_close() {
    document.getElementById("mySidebar").style.display = "none";
    var btnAbrir = document.getElementById("btnAbrir");
    var btnFechar = document.getElementById("btnFechar");
    $(btnAbrir).show();
    $(btnFechar).hide();
}

var verificardor = false;

function openRightMenu() {

    if (verificardor) {
        document.getElementById("rightMenu").style.display = "none";
        verificardor = !verificardor;
        return;
    }
    var windowWidth = window.innerWidth;
    var elemento = document.getElementById("rightMenu");
    if (windowWidth <= 768) {
        document.getElementById("rightMenu").style.width = "100%";
    } else if (windowWidth <= 1315) {
        document.getElementById("rightMenu").style.width = "35%";
    } else {
        document.getElementById("rightMenu").style.width = "25%";
    }
    document.getElementById("rightMenu").style.height = "380px";
    document.getElementById("rightMenu").style.display = "block";
    verificardor = !verificardor;
}
