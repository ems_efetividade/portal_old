// JavaScript Document


function graficoProdutividade(header, dataJson, host) {

    var cabecalho = requisicaoAjax(header);

    var json = dataJson;
    
    //alert(host + 'plugins/exporting-server/index.php');

    $.getJSON(dataJson, function(json) {
        


            chart = new Highcharts.Chart({
                    exporting: {
                            url: host + 'plugins/exporting-server/index.php',
                            //url: '<?php echo appConf::caminho ?>/painel/exportarGrafico',
                            filename: 'vai_jeff',
                            sourceWidth: 700,
                            sourceHeight: 350,
                            type: 'image/png',
                            enabled: false
                    },
                    chart: {
                            defaultSeriesType: 'spline',
                            renderTo: 'evolucao'
                    },
                    title: {
                            text: 'Evolução dos Pilares'
                    },
                    xAxis: {
                    labels: {

                    },
                    categories: jQuery.parseJSON(cabecalho)
            },
            yAxis: [{
                    title: {
                            text: 'Produtividade / Cob. Objetivo'
                    },
                    labels: {
            format: '{value}',
        },
            },{
                    title: {
                            text: 'Presc. Share / Market Share'
                    },
        labels: {
            format: '{value}',
        },
                    opposite: true
            }],
            plotOptions: {
                    series:{
                            animation:true
                    },
                    line: {
                            dataLabels: {
                                    enabled: true
                            },
                            enableMouseTracking: true
                    }
            },
            series: json

            });
    });	


}



function exportarGraficoLinha(chart, w, h, urlExport) {
	
	var exportUrl = urlExport+'plugins/exporting-server/index.php';
	
	

	var nome_arquivo = '';
	var rdata = '';
		
	var d = new Date();
	var n = d.getTime();
	nome_arquivo = n.toString();
		
    var obj = {},chart;
    obj.svg = chart.getSVG();
    obj.type = 'image/png';
    obj.width = w; 
	//obj.scale = 2;
    obj.async = true;
	obj.filename = nome_arquivo;
	obj.sourceWidth =  w;
   	obj.sourceHeight = h;

     $.ajax({
            type: "POST",
            url: exportUrl,
            data: obj,
            cache:false,
            async:false,
            crossDomain:true,

        success: function (data) {
			//alert(data);
			rdata =  data;
			return rdata;
        },
        error: function(data) {
			//alert(data);
        	//alert("----"+data.status);
         	//alert(data.statusText);
    	}
    });

	var obj = {}
	return rdata;
}  


