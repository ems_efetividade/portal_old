var arrayIdUser = [];
var arrayLinhas = [];
var pastaParaExcluir = 0;
var arquivosUpload = 0;
var arquivosUploadTotal = 0;
var tabelaIndice;
var arrayIdUserEditar = [];
var chave = false;
var idsMover = [];

function buscaNomesEditar() {
    var parametro = document.getElementById("nomeUsuarioEditar").value;
    parametro = parametro.replace(" ", "*");
    retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/listUsuariosByNomeSetorMatricula/' + parametro);
    obj = $.parseJSON(retorno);
    options = '';
    for (var i in obj) {
        options += '<option data-setor="' + obj[i].SETOR + '" data-nome="' + obj[i].NOME + '" class="listaUserSelectEditar" value="' + obj[i].ID_COLABORADOR + '">' + obj[i].MATRICULA + ' - ' + obj[i].SETOR + ' - ' + obj[i].NOME + ' - ' + obj[i].EMAIL + '</option>';
    }
    document.getElementById("listaUserSelectEditar").innerHTML = options;
}

function adicionaIdListaEditar() {
    var lista = document.getElementsByClassName('listaUserSelectEditar');
    for (var i in lista) {
        if (lista[i].selected) {
            if (arrayIdUserEditar.indexOf(lista[i].value) > -1) {
                continue;
            }
            arrayIdUserEditar.push(lista[i].value);
        }
    }
    atualizaListaUsersEditar();
    carregaLinhasTabela();
}

function voltarHome() {
    requisicaoAjax('https://' + window.location.host + '/cRelatorios/limpaAtual');
    location.href = 'https://' + window.location.host + '/diretorio';
}

function atualizaListaUsersEditar() {
    var lista = "";
    for (var i in arrayIdUserEditar) {
        if (i > 0) {
            lista += "-";
        }
        lista += arrayIdUserEditar[i].toString();
    }
    document.getElementById('userPermitidos').value = lista;
}

function deletaLinhaTabelaEditar(linha) {
    id = linha.getAttribute("data-id");
    posicao = arrayIdUserEditar.indexOf(id);
    linha.parentNode.parentNode.remove();
    arrayIdUserEditar.splice(posicao, 1);
    atualizaListaUsersEditar();
}

function carregaLinhasTabela() {
    var options = '<tr class="active">\n' +
        '                             <th width="60px">ID</th>\n' +
        '                             <th width="100px">Login</th>\n' +
        '                             <th>Nome</th>\n' +
        '                             <th width="20px">#</th>\n' +
        '                         </tr>';
    for (var i = 0; i < arrayIdUserEditar.length; i++) {
        var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/loadUsuariosByID/' + arrayIdUserEditar[i]);
        obj = $.parseJSON(retorno);
        options += '<tr><td>' + obj[1].ID_COLABORADOR + '</td><td>' + obj[1].SETOR + '</td><td>' + obj[1].NOME + '</td><td><span data-id="45" onclick="deletaLinhaTabelaEditar(this)" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></span></td></tr>'
    }
    var table = document.getElementById("userAdicionadosEditar");
    table.innerHTML = options;
}

function carregaAtualGridEditar() {
    arrayIdUserEditar = [];
    var valor = document.getElementById('userPermitidos').value;
    if (valor != '') {
        valor = valor.split('-');
    }
    if (Array.isArray(valor)) {
        for (var i = 0; i < valor.length; i++) {
            arrayIdUserEditar.push(valor[i]);
        }
    }
    carregaLinhasTabela();
}

function carregaUsuariosEditar() {
    $('#closeUserEditar').hide();
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/usuariosFormEditar/');
    document.getElementById("UsuariosEditarContent").innerHTML = retorno;
    carregaAtualGridEditar();
}

function marcarLinha(linha) {
    var id = linha.id.split("td");
    id = "tr" + id[1];
    var classe = $('#' + id).attr('class');
    $('#' + id).addClass("alert-danger");
    if (classe == 'tr-linha alert-danger') {
        $('#' + id).removeClass("alert-danger");
    }
}

function marcarTrs() {
    var linhas = document.getElementsByClassName('tr-linha');
    $(linhas).addClass("alert-danger");
}

function desmarcarTrs() {
    var linhas = document.getElementsByClassName('tr-linha');
    $(linhas).removeClass("alert-danger");
}

function excluirIncluirArquivos(param) {
    var formData = new FormData();
    for (var i = 0; i < param.length; i++) {
        formData.append("id[]", param[i]);
    }
    modalAguarde();
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/cRelatorios/excluir',
        data: formData,
        contentType: false,
        processData: false,
        success: function (retorno) {
            console.log(retorno);
            if (parseInt(retorno) == 1)
                location.reload();
        }
    });
}

$(document).ready(function () {
    $("#confirmaExcluirPastaSelecionada").click(function () {
        retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/excluirPasta/' + pastaParaExcluir);
        console.log(retorno);
        modalAguarde();
        pastaParaExcluir = 0;
        setTimeout(function () {
            location.reload()
        }, 2000);
    });
});

function baixarArquivo(id) {
    arquivo = document.getElementById('arquivo_baixar' + id);
    requisicaoAjax('https://' + window.location.host + '/cRelatorios/salvarLog/' + id);
    arquivo.click();

}

function excluirSelecao() {
    var linhas = $('.alert-danger');
    if (linhas.length < 1) {
        msgBox('Nenhum arquivo Selecionado');
        return;
    }
    $('#btnChamaModal').click();
    $('#closeExcluir').hide();
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formExcluirArquivos');
    document.getElementById('ExcluirSelecionadoContent').innerHTML = retorno;
    return;

}

function confirmaExclusao() {
    var linhas = $('.alert-danger');
    var ids_excluir = [];
    for (var i = 0; i < linhas.length; i++) {
        linhas[i] = linhas[i].id.split('tr');
        ids_excluir.push(linhas[i][1]);
    }
    excluirIncluirArquivos(ids_excluir);
}

function guardarIdsMover() {
    if(idsMover.length > 0){
        msgBox('Existem outros arquivos na área de tranferência<br>Cancele ou finalize a transferência anterior antes de continuar');
        return;
    }
    var linhas = $('.alert-danger');
    for (var i = 0; i < linhas.length; i++) {
        linhas[i] = linhas[i].id.split('tr');
        idsMover.push(linhas[i][1]);
    }
    if(idsMover.length == 0){
        msgBox('Nenhum arquivo selecionado para mover.');
        return;
    }
}

function upload(file, indice) {
    if (file.files[indice] == null) {
        return;
    }
    var pasta = document.getElementById("txtIdPasta").value;
    var identi = document.getElementById("identificador").value;
    var formData = new FormData();
    var now = new Date
    var stamp = +'' + now.getMinutes() + now.getSeconds() + '' + now.getMilliseconds();
    formData.append('nome', file.files[indice].name);
    formData.append('pasta', pasta);
    formData.append('identificador', identi);
    formData.append('arquivo', file.files[indice]);
    formData.append('tamanho', file.files[indice].size);
    formData.append('tipo', file.files[indice].type);
    formData.append('metadados', file.files[indice].lastModified+""+stamp);
    $('#btnSubirArqu').fadeOut(1000);
    $.ajax({
        type: "POST",
        url: $('#formUpload').attr('action'),
        data: formData,
        contentType: false,
        processData: false,
        success: function (retorno) {
            console.log(retorno);
            if (parseInt(retorno) == 1) {
                arquivosUpload += 1;
                var total = (arquivosUpload * 100) / arquivosUploadTotal;
                document.getElementById("barra").style.width = total + "%";
                $("#arq" + (indice + 1)).addClass('alert alert-success');
                $("#arqStatus" + (indice + 1)).text("Enviado");
                document.getElementById("contadorArqu").innerHTML = arquivosUpload + ' de ' + arquivosUploadTotal + ' arquivo(s) enviado(s)';
                if (arquivosUpload == arquivosUploadTotal) {
                    arquivosUploadTotal = 0;
                    arquivosUpload = 0;
                    modalAguarde();
                    setTimeout(function () {
                        modalAguarde('hide');
                        if (chave) {
                            msgBox('Processo Finalizado com falhas.<br>Envie os arquivos novamente ou entre em contato com o suporte do sistema.');
                            chave = false;
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
            } else if (parseInt(retorno) == 2) {
                chave = true;
                arquivosUpload += 1;
                var total = (arquivosUpload * 100) / arquivosUploadTotal;
                document.getElementById("barra").style.width = total + "%";
                $("#arq" + (indice + 1)).addClass('alert alert-danger');
                $("#arqStatus" + (indice + 1)).text("Falha");
                document.getElementById("contadorArqu").innerHTML = arquivosUpload + ' de ' + arquivosUploadTotal + ' arquivo(s) enviado(s)';
                if (arquivosUpload == arquivosUploadTotal) {
                    arquivosUploadTotal = 0;
                    arquivosUpload = 0;
                    modalAguarde();
                    setTimeout(function () {
                        modalAguarde('hide');
                        if (chave) {
                            msgBox('Processo Finalizado com falhas.<br>Envie os arquivos novamente ou entre em contato com o suporte do sistema.');
                            chave = false;
                        } else {
                            location.reload();
                        }
                    }, 2000);
                }
            }
            upload(file, indice + 1);
        }
    });

}

function subirArquivos() {
    var input = document.getElementById("termo");
    if (input.files.length == 0) {
        msgBox("Nenhum arquivo selecionado!")
        return;
    }
    arquivosUploadTotal = input.files.length;
    tabelaIndice = document.getElementById("arquivos").lastElementChild.children;
    upload(input, 0);
}

function gerarListaUpload() {
    var input = document.getElementById("termo");
    if (input.files.length == 0) {
        $('#modalVisualizar').hide();
        return;
    }
    $('.opl_div').hide("slow");
    $('.tb_div').show("slow");

    if (input.files.length > 0) {
        var indice = input.files.length;
        for (i = 0; i < input.files.length; i++) {
            var table = document.getElementById("arquivos");
            var row = table.insertRow(1);
            var cell5 = row.insertCell(0);
            var cell1 = row.insertCell(1);
            var cell2 = row.insertCell(2);
            var cell3 = row.insertCell(3);
            var cell4 = row.insertCell(4);
            row.id = "arq" + indice;
            cell1.innerHTML = input.files[i].name;
            cell2.innerHTML = input.files[i].type;
            cell3.innerHTML = formatarTamanho(input.files[i].size);
            cell4.innerHTML = '<span id="arqStatus' + indice + '">Aguardando.</span>';
            cell5.innerHTML = indice--;
        }
    }
}

function formatarTamanho(tamanho) {
    var kb = 1024;
    var mb = 1048576;
    var gb = 1073741824;
    var tb = 1099511627776;
    if (tamanho < kb) {
        return (tamanho.toFixed(2) + " bytes");
    } else if (tamanho >= kb && tamanho < mb) {
        var kilo = tamanho / kb;
        return (kilo.toFixed(2) + " KB");
    } else if (tamanho >= mb && tamanho < gb) {
        var mega = tamanho / mb;
        return (mega.toFixed(2) + " MB");
    } else if (tamanho >= gb && tamanho < tb) {
        var giga = tamanho / gb;
        return (giga.toFixed(2) + " GB");
    }
}

function modalCadastrar() {
    retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formNovaPasta');
    document.getElementById('cadastrarContent').innerHTML = retorno;
}

function modalUpload() {
    retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formUpload');
    document.getElementById("visualizarrContent").innerHTML = retorno;
    $("#termo").trigger("click");
    $('#btnFecharModalVisualizar').hide();
}

function excluirPasta(id) {
    pastaParaExcluir = id;
}

function confirmaEdicao(id) {
    nome = document.getElementById('txtEditarNome').value;
    modalAguarde();
    nome = nome.replace(/ /gi, "*");
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/atualizaPasta/' + nome + '/' + id);
    setTimeout(function () {
        location.reload()
    }, 2000);
}

function confirmaEdicaoArqu(id) {
    nome = document.getElementById('txtEditarNomeArqu').value;
    requisicaoAjax('https://' + window.location.host + '/cRelatorios/atualizaArqu/' + nome + '/' + id);
    setTimeout(function () {
        location.reload();
    }, 2000);
}

function renomearPasta(nome, id) {
    $('#closeRenomearPasta').hide();
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formRenomearPasta/' + nome + '/' + id);
    document.getElementById("RenomearPastaSelecionadaContent").innerHTML = retorno;
}

function renomearArqu(nome, id) {
    nome = nome.split('.');
    $('#closeRenomearArqu').hide();
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formRenomearArqu/' + nome[0] + '/' + id);
    document.getElementById("RenomearArquSelecionadaContent").innerHTML = retorno;
}

function permissoesPasta(nome, id) {
    $('#closePermissaoPasta').hide();
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/formPermissaoPasta/' + nome + '/' + id);
    document.getElementById("permissaoPastaContent").innerHTML = retorno;
}

function capturaIdPasta(e) {
    var idPasta = e.id;
    idPasta = idPasta.split('_');
    if(idPasta[1]==='doc'){
        var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/carregaPasta/' + idPasta[1]);
    }else {
        var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/carregaPasta/' + idPasta[1]);
        document.getElementById("txtIdPasta").value = idPasta[1];
        document.getElementById("conteudoPasta").innerHTML = retorno;
    }
}

$('.pasta').click(function () {
    controlPasta(this);
});

function controlPasta(pasta) {
    seletor = 'super_' + pasta.id;
    pastas = $('.' + seletor);
    if (pastas.length == 0) {
        return;
    }
    if (pastas.is(':hidden')) {
        pastas.show('fade');
        return;
    }
    pastas.hide('fade');
    fechaSubs(pastas);
}

function fechaSubs(pastas) {
    if (pastas.length > 0) {
        for (i = 0; i < pastas.length; i++) {
            seletor = 'super_' + pastas[i].id;
            pasta = $('.' + seletor);
            if (pasta.length == 0) {
                continue;
            }
            if (!pasta.is(':hidden')) {
                pasta.hide('fade');
            }
        }
        fechaSubs(pasta);
    }
}

function carregaUsuarios() {
    $('#closeUser').hide();
    retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/usuariosForm');
    document.getElementById("UsuariosContent").innerHTML = retorno;
    montaGrid();
}

function carregaSetores() {
    $('#closeSetor').hide();
    retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/setoresForm');
    document.getElementById("SetoresContent").innerHTML = retorno;
}

function buscaNomes() {
    var parametro = document.getElementById("nomeUsuario").value;
    parametro = parametro.replace(" ", "*");
    var retorno = requisicaoAjax('https://' + window.location.host + '/cRelatorios/listUsuariosByNomeSetorMatricula/' + parametro);
    obj = $.parseJSON(retorno);
    options = '';
    for (var i in obj) {
        options += '<option data-setor="' + obj[i].SETOR + '" data-nome="' + obj[i].NOME + '" class="listaUserSelect" value="' + obj[i].ID_COLABORADOR + '">' + obj[i].MATRICULA + ' - ' + obj[i].SETOR + ' - ' + obj[i].NOME + ' - ' + obj[i].EMAIL + '</option>';
    }
    document.getElementById("listaUserSelect").innerHTML = options;
}

function deletaLinhaTabela(linha) {
    id = linha.getAttribute("data-id");
    posicao = arrayIdUser.indexOf(id);
    linha.parentNode.parentNode.remove();
    arrayIdUser.splice(posicao, 1);
    arrayLinhas.splice(posicao, 1);
    atualizaListaUsers();
}

function atualizaListaUsers() {
    var lista = "";
    for (var i in arrayIdUser) {
        if (i > 0)
            lista += "-";
        lista += arrayIdUser[i].toString();
    }
    document.getElementById('userPermitidos').value = lista;
}

function montaGrid() {
    var table = document.getElementById("userAdicionados");
    if (arrayLinhas.length == 0)
        return;
    for (j = 1; j < $("#userAdicionados tr").length;) {
        table.deleteRow(j);
    }
    for (var i in arrayLinhas) {
        var row = table.insertRow(1);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        cell1.innerHTML = arrayLinhas[i].value;
        cell2.innerHTML = arrayLinhas[i].getAttribute("data-setor");
        cell3.innerHTML = arrayLinhas[i].getAttribute("data-nome");
        cell4.innerHTML = '<span data-id="' + arrayLinhas[i].value + '" onclick="deletaLinhaTabela(this)" class="btn btn-sm btn-danger"><span class="glyphicon glyphicon-remove"></span></span>';
    }
}

function adicionaIdLista() {
    var lista = document.getElementsByClassName('listaUserSelect');
    for (var i in lista) {
        if (lista[i].selected) {
            if (arrayIdUser.indexOf(lista[i].value) > -1)
                continue;
            arrayIdUser.push(lista[i].value);
            arrayLinhas.push(lista[i]);
        }
    }
    atualizaListaUsers();
    montaGrid();
}