var setorFinal = 0;
var cpfFinal = 0;
var matriculaFinal = 0;
var tipoFinal = 0;

function confirmarResetar() {
    codigo = document.getElementById('codigo').value;
    senha = document.getElementById('senha').value;
    repetir = document.getElementById('repetir').value;
    mensagem = document.getElementById('mensagem');
    mensagemContent = document.getElementById('mensagemContent');
    var regex = '[^a-zA-Z0-9]+';
    if (senha.length <= 5) {
        mensagemContent.innerHTML = "A senha deve conter mais de 5 caracteres!";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (!senha.match(regex)) {
        mensagemContent.innerHTML = "A senha deve conter letras e símbolos <br>(@,#,$,*)";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (!senha.match(/[a-z]+/)) {
        mensagemContent.innerHTML = "A senha deve conter maiúsculas e minúsculas";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (!senha.match(/[A-Z]+/)) {
        mensagemContent.innerHTML = "A senha deve conter maiúsculas e minúsculas";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (!senha.match(/[A-Z]+/)) {
        mensagemContent.innerHTML = "A senha deve conter maiúsculas e minúsculas";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (senha != repetir) {
        mensagemContent.innerHTML = "A senha e a confirmação não podem ser diferentes!";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    id = requisicaoAjax('https://' + window.location.host + '/reset/verificarCodigo/' + codigo);
    if (id == 0) {
        mensagemContent.innerHTML = "Código Inválido";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    if (id == 'e') {
        mensagemContent.innerHTML = "Código Expirado!<br>Solicite novamente a alteração da senha";
        $(mensagem).show('fade');
        setTimeout(function () {
            $(mensagem).hide('fade');
        }, 3000);
        return;
    }
    formData = new FormData();
    formData.append("senha", senha);
    formData.append("id", id);
    $.ajax({
        type: "POST",
        data: formData,
        url: 'https://' + window.location.host + '/reset/resetar/',
        contentType: false,
        processData: false,
        success: function (resposta) {
            if (resposta == 0) {
                $(mensagem).removeClass('alert-danger');
                $(mensagem).addClass('alert-success');
                mensagemContent.innerHTML = "Senha alterada com Sucesso!<br>Aguarde, redirecionando.";
                $(mensagem).show('fade');
                setTimeout(function () {
                    $(mensagem).hide('fade');
                }, 3000);
                setTimeout(function () {
                    location.href = 'https://' + window.location.host + "/login/logar";
                }, 4000);
            }
        }
    });
}

function validarCpf(strCPF) {
    strCPF = strCPF.value;
    strCPF = strCPF.replace(/\.|\-/g, "");
    msg = document.getElementById('msgCPF');
    $(msg).hide('fade');
    if (TestaCPF(strCPF)) {
        colaborador = document.getElementById('colaboradorGroup');
        cpf = document.getElementById('cpfGroup');
        $(cpf).hide('fade');
        $(colaborador).show('fade');
    } else if (strCPF.length == 11) {
        $(msg).show('fade');
    }
}

function enviarSolicitacao() {
    aguarde = document.getElementById('aguarde');
    $(aguarde).show('fade');
    if (tipoFinal == 1) {
        var parametro = cpfFinal + '@' + setorFinal;
        retorno = requisicaoAjax('https://' + window.location.host + '/reset/resetarCpf/' + parametro);
    } else if (tipoFinal == 2) {
        var parametro = cpfFinal + '@' + matriculaFinal;
        retorno = requisicaoAjax('https://' + window.location.host + '/reset/resetarMatricula/' + parametro);
    }
    if (retorno == 2) {
        erro = document.getElementById('falha');
        setTimeout(function () {
            $(aguarde).hide('slow');
        }, 1000);
        setTimeout(function () {
            $(erro).show('slow');
        }, 1000);
        setTimeout(function () {
            $(erro).hide('slow');
        }, 3500);
        return;
    }
    setTimeout(function () {
        $(aguarde).hide('slow');
        dados = document.getElementById('dadosConfirmar');
        $(dados).hide('slow');
    }, 1000);
    setTimeout(function () {
        mensagem = "Procedimento realizado com sucesso!<br><br> Enviamos um e-mail para <br><b>";
        mensagem += retorno + "</b><br><br>Acesse agora mesmo sua caixa de entrada e finalize a solicitação<br>";
        botao = '                                    <br><a id="btReiniciar" type="button"\n' +
            '                                            href="../login/logar"\n' +
            '                                            class="btn btn-lg btn-block btn-primary">Voltar\n' +
            '                                    </a>';
        document.getElementById('retorno').innerHTML = mensagem + botao;
    }, 1500);
}

function verificarDados() {
    var btconfirmar = document.getElementById('setorGroup');
    $(btconfirmar).hide('fade');
    btconfirmar = document.getElementById('matriculaGroup');
    $(btconfirmar).hide('fade');
    btconfirmar = document.getElementById('btConfirmar');
    $(btconfirmar).hide('fade');
    matriculaFinal = document.getElementById('matricula').value;
    setorFinal = document.getElementById('setor').value;
    cpfFinal = document.getElementById('cpf').value;
    if (setorFinal != '') {
        document.getElementById('setorLabel').innerHTML = 'Setor: ' + setorFinal;
    } else if (matriculaFinal != '') {
        document.getElementById('setorLabel').innerHTML = 'Matrícula: ' + matriculaFinal;
    } else {
        document.getElementById('setorLabel').innerHTML = 'Ocorreu um erro.';
    }
    document.getElementById('CPFLabel').innerHTML = "CPF: " + cpfFinal;
    btconfirmar = document.getElementById('dadosConfirmar');
    $(btconfirmar).show('fade');
}

function contaCampos(tipo, campos) {
    if (tipo == 1) {
        if (campos.value.length > 7) {
            btconfirmar = document.getElementById('btConfirmar');
            $(btconfirmar).show('fade');
        }
    } else if (tipo == 2) {
        if (campos.value.length > 3) {
            btconfirmar = document.getElementById('btConfirmar');
            $(btconfirmar).show('fade');
        }
    }
}

function tipoColaborador(tipo) {
    tipoFinal = tipo;
    colaborador = document.getElementById('colaboradorGroup');
    $(colaborador).hide('fade');
    if (tipo == 1) {
        colaborador = document.getElementById('setorGroup');
    } else {
        colaborador = document.getElementById('matriculaGroup');
    }
    $(colaborador).show('fade');
}

function TestaCPF(strCPF) {
    if (strCPF.length < 11) {
        return;
    }
    var Soma;
    var Resto;
    Soma = 0;
    if (strCPF == "00000000000") {
        return false;
    }
    for (var i = 1; i <= 9; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (11 - i);
        Resto = (Soma * 10) % 11;
    }

    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(9, 10))) {
        return false;
    }

    Soma = 0;
    for (var i = 1; i <= 10; i++) {
        Soma = Soma + parseInt(strCPF.substring(i - 1, i)) * (12 - i);
        Resto = (Soma * 10) % 11;
    }

    if ((Resto == 10) || (Resto == 11)) {
        Resto = 0;
    }
    if (Resto != parseInt(strCPF.substring(10, 11))) {
        return false;
    }
    return true;
}

function MascaraCPF(cpf) {
    if (mascaraInteiro(cpf) == false) {
        event.returnValue = false;
    }
    return formataCampo(cpf, '000.000.000-00', event);
}

function mascaraInteiro() {
    if (event.keyCode < 48 || event.keyCode > 57) {
        event.returnValue = false;
        return false;
    }
    return true;
}

function formataCampo(campo, Mascara, evento) {
    var boleanoMascara;

    var Digitato = evento.keyCode;
    exp = /\-|\.|\/|\(|\)| /g
    campoSoNumeros = campo.value.toString().replace(exp, "");

    var posicaoCampo = 0;
    var NovoValorCampo = "";
    var TamanhoMascara = campoSoNumeros.length;
    ;

    if (Digitato != 8) { // backspace
        for (i = 0; i <= TamanhoMascara; i++) {
            boleanoMascara = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                || (Mascara.charAt(i) == "/"))
            boleanoMascara = boleanoMascara || ((Mascara.charAt(i) == "(")
                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " "))
            if (boleanoMascara) {
                NovoValorCampo += Mascara.charAt(i);
                TamanhoMascara++;
            } else {
                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo);
                posicaoCampo++;
            }
        }
        campo.value = NovoValorCampo;
        return true;
    } else {
        return true;
    }
}