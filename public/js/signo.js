verificadorMedico = 0;
tipoAtividade = 1;
Notiflix.Notify.Init({});
Notiflix.Confirm.Init({});
Notiflix.Loading.Init({
    svgColor: "#00639A ",
    zindex: 999000,
});
Notiflix.Report.Init({
    zindex: 999001,
});

function TrocarResponsavel(id) {
    Notiflix.Loading.Circle("Aguarde...");
    modal = document.getElementById('resumoAtividade');
    modall = document.getElementById('resumoAtividademd');
    $(modal).show();
    $(modall).show();
    modal = document.getElementById('NomeResponsavel');
    empresa = document.getElementById('idEmpresaR');
    formData = new FormData();
    formData.append('idEmpresa', id);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/nomeResponsavel',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        success: function (retorno) {
            modal.innerHTML = retorno;
            empresa.value = id;
            Notiflix.Loading.Remove(300);
        }
    });
}

function salvarResposavel() {
    Notiflix.Loading.Circle("Aguarde...");
    responsavel = document.getElementById('responsavelId').value;
    empresa = document.getElementById('idEmpresaR').value;
    formData = new FormData();
    formData.append('idResponsavel', responsavel);
    formData.append('idEmpresa', empresa);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/alterarResponsavel',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
        },
        success: function (retorno) {
            location.reload();
        }
    });
}

function salvarPma() {
    contatos = [];
    data = document.getElementById('dataAgenda').value;
    resp = document.getElementById('responsavelId').value;
    respNome = document.getElementById('responsavel').value;
    obser = document.getElementById('observacao').value;
    empresa = document.getElementById('idEmpresa').value;
    lista = document.getElementsByClassName('listMedicos');
    for (i = 0; i < lista.length; i++)
        if (lista[i].classList.contains('alert-success')) {
            contatos.push(lista[i].id.substring(3));
        }
    if (contatos.length == 0) {
        Notiflix.Report.Failure('Atenção', 'Selecione um contato para executar a atividade', 'Fechar');
        return;
    }
    if (data == '') {
        Notiflix.Report.Failure('Atenção', 'Informe a data da atividade', 'Fechar');
        return;
    }
    if (resp == '') {
        Notiflix.Report.Failure('Atenção', 'Informe o responsável pela atividade', 'Fechar');
        return;
    }
    if (obser == '') {
        Notiflix.Report.Failure('Atenção', 'Descreva a atividade na observação', 'Fechar');
        return;
    }
    var formData = new FormData();
    formData.append('idEmpresa', empresa);
    formData.append('data', data);
    formData.append('obser', obser);
    formData.append('responsavelId', resp);
    formData.append('contatos', contatos);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/informacoesContatos',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            retorno = $.parseJSON(retorno);
            switch (tipoAtividade) {
                case 1:
                    texto = '<h3>Telefonema</h3>';
                    break;
                case 2:
                    texto = '<h3>Email</h3>';
                    break;
                case 3:
                    texto = '<h3>Reunião</h3>';
                    break;

            }
            texto += '<p><strong>Responsável: </strong>' + respNome + '</p>';

            tratada = data.split('-');
            texto += '<p><strong>Data: </strong>' + tratada[2] + '/' + tratada[1] + '/' + tratada[0] + '</p>';
            texto += '<p>' + obser + '</p>';
            texto += '<br>';
            for (var prop in retorno) {
                texto += '<b>Contato: </b>';
                texto += retorno[prop][0];
                texto += ' ';
                texto += retorno[prop][1];
                texto += ' ';
                texto += retorno[prop][2];
                texto += '<br>';
            }
            setTimeout(function () {
                modal = document.getElementById('resumoAtividade');
                $(modal).fadeIn();
                modal = document.getElementById('resumoAtividademd');
                $(modal).fadeIn();
                modal = document.getElementById('resumoAtividademsg');
                modal.innerHTML = texto;
                Notiflix.Loading.Remove(300);
            }, 700);
        }
    });
    return;
}

function salvarAtividade() {
    contatos = [];
    data = document.getElementById('dataAgenda').value;
    resp = document.getElementById('responsavelId').value;
    empresa = document.getElementById('idEmpresa').value;
    msg = document.getElementById('resumoAtividademsg').innerHTML;
    lista = document.getElementsByClassName('listMedicos');
    for (i = 0; i < lista.length; i++) {
        if (lista[i].classList.contains('alert-success')) {
            contatos.push(lista[i].id.substring(3));
        }
    }
    var formData = new FormData();
    formData.append('data', data);
    formData.append('resp', resp);
    formData.append('idEmpresa', empresa);
    formData.append('msg', msg);
    formData.append('contatos', contatos);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/salvarPma',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            tipoAtividade = 1;
            location.reload();
        }
    });
}

function CancelaResposavel() {
    modal = document.getElementById('resumoAtividade');
    $(modal).fadeOut();
    modal = document.getElementById('resumoAtividademd');
    $(modal).fadeOut();
}

function CancelaAtividade() {
    setTimeout(function () {
        modal = document.getElementById('resumoAtividademsg');
        modal.innerHTML = '';
    }, 700);
    modal = document.getElementById('resumoAtividade');
    $(modal).fadeOut();
    modal = document.getElementById('resumoAtividademd');
    $(modal).fadeOut();
}

function alternaBotao(elemento) {
    elementos = document.getElementsByClassName("btnatividades");
    for (i = 0; i < elementos.length; i++) {
        elementos[i].classList.remove("active");
    }
    if (elemento == 'mailer') {
        tipoAtividade = 2;
        document.getElementById('mailer').classList.add("active");
        document.getElementById('titulo').innerText = "Agendar Email"
    }
    if (elemento == 'phone') {
        tipoAtividade = 1;
        document.getElementById('phone').classList.add("active");
        document.getElementById('titulo').innerText = "Agendar Telefonema"
    }
    if (elemento == 'visit') {
        tipoAtividade = 3;
        document.getElementById('visit').classList.add("active");
        document.getElementById('titulo').innerText = "Agendar Visita"
    }
}

function marcarMedico(div) {
    elemento = document.getElementById(div.id)
    if (elemento.classList.contains('alert-success')) {
        elemento.classList.remove("alert-success")
        return;
    }
    elemento.classList.add("alert-success");
}

function Cadastrar(id) {
    var formData = new FormData();
    if (id > 0) {
        formData.append('idEmpresa', id);
    }
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/cadastrar',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 500);
            setTimeout(function () {
                $('#conteudoView').fadeIn();
                Notiflix.Loading.Remove();
            }, 700);
        }
    });
}

function ViewContatos() {
    var formData = new FormData();
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/contatos',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            modalAguarde();
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 500);
            setTimeout(function () {
                $('#conteudoView').fadeIn();
                modalAguarde('hide');
            }, 700);
        }
    });
}

function InserirAtividade(id) {
    var formData = new FormData();
    formData.append('id', id);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/agendar',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 500);
            setTimeout(function () {
                $('#conteudoView').fadeIn();
                Notiflix.Loading.Remove();
            }, 700);
        }
    });
}

function ExcluirEmpresa(id) {
    var formData = new FormData();
    Notiflix.Confirm.Show('Excluir Empresa', 'Deseja realmente excluir esta empresa?', 'Sim', 'Não', function () {
        Notiflix.Loading.Circle('Aguarde...');
        formData.append('id', id);
        $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/excluir',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
            },
            success: function (retorno) {
                setTimeout(function () {
                    location.reload();
                }, 700);
            }
        });
    });
}

function Atividades() {
    var formData = new FormData();
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/atividades',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 500);
            setTimeout(function () {
                Notiflix.Loading.Remove();
                $('#conteudoView').fadeIn();
            }, 700);
        }
    });
}

function FichaMedico(crm, nome, esp, periodo) {
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/NewPrescriptionShareOnline/home/fichaMedicoForever',
        data: {
            crm: crm,
            nome: nome,
            esp: esp,
            periodo: periodo
        },
        beforeSend: function () {

        },
        success: function (retorno) {
            $('#modalFichaMedico .modal-body').html(retorno);
            $('#modalFichaMedico').modal('show');
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-crm', crm);
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-periodo', periodo);
            $('#modalFichaMedico').modal('show');
            $('#btn-exportar-ficha-medico').unbind().click(function (s) {
                s.preventDefault();
                window.location.href = 'https://' + window.location.host + '/sistemas/NewPrescriptionShareOnline/home/exportarFichaMedico/' + crm + "/" + periodo;
            });
        }
    });
}

function VisualizarEmpresa(id) {
    var formData = new FormData();
    formData.append('id', id);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/visualizarEmpresa',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            modalAguarde();
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 500);
            setTimeout(function () {
                modalAguarde('hide');
                $('#conteudoView').fadeIn();
            }, 700);
        }
    });
}

function Contatos(idEmpresa) {
    responsavel = document.getElementById('responsavelId').value;
    nMedicos = document.getElementById('numeroMedicos').value;
    nPacientes = document.getElementById('numeroPacientes').value;
    consultasDia = document.getElementById('pacientesdia').value;
    valorConsulta = document.getElementById('valorConsulta').value;
    porcAtendimento = document.getElementById('porcAtendimento').value;
    AtendeParticular = document.getElementById('AtendeParticular').checked;
    valueConsultoria = document.getElementById('valueConsultoria').value;
    valueSituacao = document.getElementById('valueSituacao').value;
    consultoria = document.getElementById('consultoria').checked;
    objetivo = document.getElementById('objetivo').value;
    previsao = document.getElementById('previsao').value;
    editar = document.getElementById('empEditar').value;

    if (objetivo == '') {
        Notiflix.Report.Failure('Atenção', 'Informe o objetivo', 'Fechar');
        return;
    }
    if (previsao == '') {
        Notiflix.Report.Failure('Atenção', 'Informe a data prevista para o objetivo', 'Fechar');
        return;
    }
    if (consultoria) {
        if (valueConsultoria < 1) {
            Notiflix.Report.Failure('Atenção', 'Informe a consultoria do CNPJ', 'Fechar');
            return;
        }
        if (valueSituacao < 1) {
            Notiflix.Report.Failure('Atenção', 'Informe a situação da consultoria do CNPJ', 'Fechar');
            return;
        }
    }
    if (responsavel.length == '') {
        Notiflix.Report.Failure('Atenção', 'Informe um responsável pelo CNPJ', 'Fechar');
        return;
    }
    if (nMedicos.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo quantidade de médicos não informado', 'Fechar');
        return;
    }
    if (nPacientes.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo pacientes por mês não informado', 'Fechar');
        return;
    }
    if (consultasDia.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo consultas por dia não informado', 'Fechar');
        return;
    }
    if (AtendeParticular) {
        if (valorConsulta.length == '') {
            Notiflix.Report.Failure('Atenção', 'Campo valor da consulta não informado', 'Fechar');
            return;
        }
        if (porcAtendimento.length == '') {
            Notiflix.Report.Failure('Atenção', 'Campo % de atendimento não informado', 'Fechar');
            return;
        }
    }

    //combos
    convenios = document.getElementById('convenios').checked;
    plataforma = document.getElementsByClassName('plataforma');
    relacionamento = document.getElementsByClassName('relacionamento btn-success');
    especialidade = document.getElementsByClassName('especialidade');
    convenio = document.getElementsByClassName('convenio');

    if (relacionamento.length == 0) {
        Notiflix.Report.Failure('Atenção', 'Informe o relacionamento com o CNPJ', 'Fechar');
        return;
    }
    relacionamento = relacionamento[0].id;
    arrayPlataforma = [];
    for (i = 0; i < plataforma.length; i++) {
        if ($("#" + plataforma[i].id).prop("checked")) {
            arrayPlataforma.push(plataforma[i].value);
        }
    }
    if (arrayPlataforma.length == 0) {
        Notiflix.Report.Failure('Atenção', 'Informe as plataformas do CNPJ', 'Fechar');
        return;
    }
    arrayEspecialidade = [];
    for (i = 0; i < especialidade.length; i++) {
        if ($("#" + especialidade[i].id).prop("checked")) {
            arrayEspecialidade.push(especialidade[i].value);
        }
    }
    if (arrayEspecialidade.length == 0) {
        Notiflix.Report.Failure('Atenção', 'Informe as especialidades do CNPJ', 'Fechar');
        return;
    }
    arrayConvenio = [];
    if (convenios) {
        for (i = 0; i < convenio.length; i++) {
            if ($("#" + convenio[i].id).prop("checked")) {
                arrayConvenio.push(convenio[i].value);
            }
        }
        if (arrayConvenio.length == 0) {
            Notiflix.Report.Failure('Atenção', 'Informe os convenios do CNPJ', 'Fechar');
            return;
        }
    }
    var formData = new FormData();
    formData.append('relacionamento', relacionamento);
    formData.append('responsavel', responsavel);
    formData.append('nMedicos', nMedicos);
    formData.append('nPacientes', nPacientes);
    formData.append('consultasDia', consultasDia);
    if (AtendeParticular) {
        formData.append('valorConsulta', valorConsulta);
        formData.append('porcAtendimento', porcAtendimento);
    } else {
        formData.append('valorConsulta', '');
        formData.append('porcAtendimento', '');
    }

    if (consultoria) {
        formData.append('consultoria', valueConsultoria);
        formData.append('situacao', valueSituacao);
    } else {
        formData.append('consultoria', '');
        formData.append('situacao', '');
    }
    formData.append('arrayPlataforma', arrayPlataforma);
    formData.append('arrayEspecialidade', arrayEspecialidade);
    formData.append('arrayConvenio', arrayConvenio);
    formData.append('idEmpresa', idEmpresa);
    formData.append('objetivo', objetivo);
    formData.append('previsao', previsao);
    formData.append('editar', editar);
    $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/usuarios',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                Notiflix.Loading.Circle('Aguarde...');
            },
            success: function (retorno) {
                $('#conteudoView').fadeOut();
                setTimeout(function () {
                    document.getElementById('conteudoView').innerHTML = retorno;
                }, 700);
                setTimeout(function () {
                    Notiflix.Loading.Remove();
                    $('#conteudoView').fadeIn();
                }, 900);
            }
        }
    );
}

function Ficha(id) {
    var formData = new FormData();
    if (id > 0) {
        formData.append('idEmpresa', id);
    }
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/ficha',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 700);
            setTimeout(function () {
                Notiflix.Loading.Remove();
                $('#conteudoView').fadeIn();
            }, 900);
        }
    });
}

function FichaVisualizar(id) {
    var formData = new FormData();
    if (id > 0) {
        formData.append('id', id);
    }
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/fichaVisualizar',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            modalAguarde();
        },
        success: function (retorno) {
            $('#conteudoView').fadeOut();
            setTimeout(function () {
                document.getElementById('conteudoView').innerHTML = retorno;
            }, 600);
            setTimeout(function () {
                modalAguarde('hide');
                $('#conteudoView').fadeIn();
            }, 800);
        }
    });
}

function DadosAdicionais(idEmp) {
    //obrigatorios
    cnpj = document.getElementById('txtCnpj').value;
    cep = document.getElementById('txtcep').value;
    numero = document.getElementById('txtnumero').value;
    email = document.getElementById('txtemail').value;
    telefone = document.getElementById('txttelefone').value;
    razao = document.getElementById('txtrazaosocial').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    fantasia = document.getElementById('txtfantasia').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    cidade = document.getElementById('txtcidade').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    bairro = document.getElementById('txtbairro').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    logradouro = document.getElementById('txtlogradouro').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    estado = document.getElementById('txtestado').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();

    if (cep.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo CEP não informado', 'Fechar');
        return;
    }
    if (numero.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Número não informado', 'Fechar');
        return;
    }
    if (email.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo E-mail não informado', 'Fechar');
        return;
    }
    if (telefone.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Telefone não informado', 'Fechar');
        return;
    }
    if (razao.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Razão Social não informado', 'Fechar');
        return;
    }
    if (fantasia.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Nome Fantasia não informado', 'Fechar');
        return;
    }
    if (estado.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo UF não informado', 'Fechar');
        return;
    }
    if (logradouro.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Endereço não informado', 'Fechar');
        return;
    }
    if (bairro.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Bairro não informado', 'Fechar');
        return;
    }
    if (cidade.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Cidade não informado', 'Fechar');
        return;
    }
    if (cnpj.length == '') {
        Notiflix.Report.Failure('Atenção', 'Campo CNPJ não informado', 'Fechar');
        return;
    }

    //não obrigatorios
    complemento = document.getElementById('txtcomplemento').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
    whatsapp = document.getElementById('whatsapp').innerText;

    //trata Whatsapp
    if (whatsapp == "✓ WhatsApp")
        whatsapp = 1;
    else
        whatsapp = 0;

    var formData = new FormData();
    formData.append('whatsapp', whatsapp);
    formData.append('complemento', complemento);
    formData.append('cnpj', cnpj);
    formData.append('cidade', cidade);
    formData.append('bairro', bairro);
    formData.append('logradouro', logradouro);
    formData.append('estado', estado);
    formData.append('fantasia', fantasia);
    formData.append('razao', razao);
    formData.append('telefone', telefone);
    formData.append('email', email);
    formData.append('numero', numero);
    formData.append('cep', cep);
    if (idEmp > 0) {
        formData.append('idEmpresa', idEmp);
    }
    $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/adicionais',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                Notiflix.Loading.Circle('Aguarde...');
            },
            success: function (retorno) {
                if (retorno == 1) {
                    Notiflix.Report.Failure('Atenção', 'Ocorreu um erro ao salvar.', 'Fechar');
                    return;
                } else {
                    $('#conteudoView').fadeOut();
                    setTimeout(function () {
                        document.getElementById('conteudoView').innerHTML = retorno;
                    }, 700);
                    setTimeout(function () {
                        Notiflix.Loading.Remove();
                        $('#conteudoView').fadeIn();
                    }, 900);
                }
            }
        }
    );
}

function contagemTempo() {
    Notiflix.Loading.Change(segundos + ' segundos restantes.');
    segundos = segundos - 1;
    if (segundos != -1) {
        setTimeout(function () {
            contagemTempo();
        }, 1000);
    }
}

function solicitarAjax(valor) {
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/buscaCNPJ/' + valor,
        crossDomain: true,
        dataType: "text",
        contentType: false,
        processData: false,
        beforeSend: function () {
            Notiflix.Loading.Circle('Aguarde...');
        },
        success: function (retorno) {
            if (retorno == 1) {
                Notiflix.Report.Failure('Aviso', 'CNPJ já cadastrado.\nEm caso de duvidas entre em contato com seu gerente.', 'Fechar');
                Notiflix.Loading.Remove();
                return;
            }
            if (retorno === "Too many requests, please try again later.") {
                Notiflix.Report.Failure('Aviso', '"Número de consultas excedidas<br>Aguarde, sua solicitação será reenviada em alguns segundos."', 'Fechar');
                segundos = 45;
                Notiflix.Loading.Change(segundos + ' segundos restantes.');
                contagemTempo();
                setTimeout(function () {
                    solicitarAjax(valor);
                }, 45000);
            } else {
                retorno = $.parseJSON(retorno);
                if (retorno.status === "ERROR") {
                    Notiflix.Report.Failure('Aviso', retorno, 'Fechar');
                } else {
                    document.getElementById('avancarInicio').style.display = '';
                    document.getElementById('txtrazaosocial').value = retorno.nome;
                    document.getElementById('txtfantasia').value = retorno.fantasia;
                    document.getElementById('txtcep').value = retorno.cep;
                    document.getElementById('txtcidade').value = retorno.municipio;
                    document.getElementById('txtnumero').value = retorno.numero;
                    document.getElementById('txtbairro').value = retorno.bairro;
                    document.getElementById('txtemail').value = retorno.email;
                    document.getElementById('txtlogradouro').value = retorno.logradouro;
                    Notiflix.Loading.Remove();
                }
            }
        }
    });
}

function buscarDados() {
    valor = document.getElementById('txtCnpj').value;
    valor = valor.replace(/\./g, '');
    valor = valor.replace(/\//g, '');
    valor = valor.replace(/-/g, '');
    if (valor.length != 14) {
        Notiflix.Report.Failure('Aviso', 'CNPJ Inválido', 'Fechar');
        return;
    }
    if (!validarCNPJ(valor)) {
        Notiflix.Report.Failure('Aviso', 'CNPJ Inválido', 'Fechar');
        return;
    }
    solicitarAjax(valor);
}

function validarCNPJ(cnpj) {
    cnpj = cnpj.replace(/[^\d]+/g, '');
    if (cnpj == '') return false;
    if (cnpj.length != 14)
        return false;
    // Elimina CNPJs invalidos conhecidos
    if (cnpj == "00000000000000" ||
        cnpj == "11111111111111" ||
        cnpj == "22222222222222" ||
        cnpj == "33333333333333" ||
        cnpj == "44444444444444" ||
        cnpj == "55555555555555" ||
        cnpj == "66666666666666" ||
        cnpj == "77777777777777" ||
        cnpj == "88888888888888" ||
        cnpj == "99999999999999")
        return false;
    // Valida DVs
    tamanho = cnpj.length - 2;
    numeros = cnpj.substring(0, tamanho);
    digitos = cnpj.substring(tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(0))
        return false;
    tamanho = tamanho + 1;
    numeros = cnpj.substring(0, tamanho);
    soma = 0;
    pos = tamanho - 7;
    for (i = tamanho; i >= 1; i--) {
        soma += numeros.charAt(tamanho - i) * pos--;
        if (pos < 2)
            pos = 9;
    }
    resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
    if (resultado != digitos.charAt(1))
        return false;
    return true;
}

function marcarWhatsapp() {
    botao = $("#whatsapp");
    check = '&check; WhatsApp';
    if (botao[0].innerText == "WhatsApp") {
        botao.addClass('btn-success');
        botao.html(check);
    } else {
        botao.removeClass('btn-success');
        botao.text("WhatsApp");
    }
    return false;
}

function Relacionamento(btn) {
    elementos = document.getElementsByClassName("relacionamento");
    for (i = 0; i < elementos.length; i++) {
        elementos[i].classList.remove("btn-success");
    }
    botao = btn.id;
    document.getElementById(botao).classList.add("btn-success");
}

function Particular(criterio) {
    if (criterio == 1) {
        document.getElementById('btnSim').classList.add("btn-success");
        document.getElementById('btnNao').classList.remove("btn-danger");
        document.getElementById("valorConsulta").disabled = false;
        document.getElementById("porcAtendimento").disabled = false;
    }
    if (criterio == 2) {
        document.getElementById('btnSim').classList.remove("btn-success");
        document.getElementById('btnNao').classList.add("btn-danger");
        document.getElementById("valorConsulta").disabled = true;
        document.getElementById("porcAtendimento").disabled = true;
    }
}

function Convenios(checkBox) {
    if (checkBox.checked == true) {
        document.getElementById("DivcheckboxPlano").style.display = "";
    } else {
        document.getElementById("DivcheckboxPlano").style.display = "none";
    }
}

function BuscaCrm() {
    Notiflix.Loading.Circle('Aguarde...');
    crm = document.getElementById('crm').value;
    uf = document.getElementById('estadoMedico').value;
    if (uf == '') {
        Notiflix.Report.Failure('Atenção', 'Informe o estado do CRM', 'Fechar');
        Notiflix.Loading.Remove();
        return;
    }
    crm = crm.trim();
    adicionar = 7 - crm.length;
    for (var i = 0; i < adicionar; i++) crm = '0' + crm;
    crm = crm.slice(0, 7);
    if (crm == 0000000) {
        Notiflix.Report.Failure('Atenção', 'Informe o número do CRM', 'Fechar');
        Notiflix.Loading.Remove();
        return;
    }
    retorno = requisicaoAjax('https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/buscarCrm/' + uf + crm);
    document.getElementById('crm').value = crm;
    if (retorno == '') {
        Notiflix.Loading.Remove();
        Notiflix.Report.Failure('Atenção', 'Médico não encontrado, cadastro sujeito a aprovação!', 'Fechar');
        verificadorMedico = 1;
        return;
    }
    verificadorMedico = 2;
    Notiflix.Loading.Remove();
    document.getElementById('txtnomecontato').value = retorno;
    document.getElementById('txtnomecontato').disabled = true;
}

function buscaNome() {
    var filtro = document.getElementById('responsavel');
    var tabela = document.getElementById('lista');
    combo = document.getElementById('comboNomes');
    var nomeFiltro = filtro.value.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    if (nomeFiltro == '') {
        for (var i = 0; i < tabela.rows.length; i++) {
            tabela.rows[i].style.display = 'none';
        }
        combo.style.display = 'none';
        return;
    }
    for (var j = 0; j < tabela.rows.length; j++) {
        var conteudoCelula = tabela.rows[j].cells[0].innerText;
        var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
        tabela.rows[j].style.display = corresponde ? '' : 'none';
        combo.style.display = 'block';
    }
}

function buscaNomeNovo() {
    var filtro = document.getElementById('responsavel');
    var tabela = document.getElementById('listanovo');
    combo = document.getElementById('comboNomes');
    var nomeFiltro = filtro.value.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    if (nomeFiltro == '') {
        for (var i = 0; i < tabela.rows.length; i++) {
            tabela.rows[i].style.display = 'none';
        }
        combo.style.display = 'none';
        return;
    }
    for (var j = 0; j < tabela.rows.length; j++) {
        var conteudoCelula = tabela.rows[j].cells[0].innerText;
        var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
        tabela.rows[j].style.display = corresponde ? '' : 'none';
        combo.style.display = 'block';
    }
}

function SelecionarNome(nome, id) {
    combo = document.getElementById('comboNomes');
    filtro = document.getElementById('responsavel');
    resp = document.getElementById('responsavelId');
    filtro.value = nome;
    resp.value = id;
    tabela = document.getElementById('lista');
    combo.style.display = 'none';
}

function CheckConsultas(checkBox) {
    elementos = document.getElementsByClassName('consultasCheck');
    if (checkBox.checked == true) {
        elementos[0].disabled = false;
        elementos[1].disabled = false;
        elementos[2].disabled = false;
        elementos[3].disabled = false;
        elementos[0].value = "";
        elementos[1].value = "";
        elementos[2].value = "";
        elementos[3].value = "";
        elementos[0].style.display = "block";
        elementos[1].style.display = "block";
        elementos[2].style.display = "block";
        elementos[3].style.display = "block";
    } else {
        elementos[0].disabled = true;
        elementos[1].disabled = true;
        elementos[2].disabled = true;
        elementos[3].disabled = true;
        elementos[0].value = "";
        elementos[1].value = "";
        elementos[2].value = "";
        elementos[3].value = "";
        elementos[0].style.display = "none";
        elementos[1].style.display = "none";
        elementos[2].style.display = "none";
        elementos[3].style.display = "none";
    }
}

function Consultoria(checkBox) {
    situacao = document.getElementById('selectSituacao');
    consultoria = document.getElementById('selectConsultoria');
    if (checkBox.checked == true) {
        situacao.style.display = "block";
        consultoria.style.display = "block";
    } else {
        situacao.style.display = "none";
        consultoria.style.display = "none";
    }
}

function buscaGridEmp() {
    filtro = document.getElementById('empresa');
    tabela = document.getElementById('lista');
    nomeFiltro = filtro.value.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    for (var i = 1; i < tabela.rows.length; i++) {
        tabela.rows[i].style.display = (tabela.rows[i].cells[0].innerText.toLowerCase().indexOf(nomeFiltro) >= 0) ? '' : 'none';

    }
    document.getElementById('cidade').value = '';
    document.getElementById('txtCnpj').value = '';
    document.getElementById('ugn').value = '';
}

function buscaGridCid() {
    filtro = document.getElementById('cidade');
    tabela = document.getElementById('lista');
    nomeFiltro = filtro.value.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    for (var i = 1; i < tabela.rows.length; i++) {
        var conteudoCelula = tabela.rows[i].cells[2].innerText;
        var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
        tabela.rows[i].style.display = corresponde ? '' : 'none';
    }
    document.getElementById('empresa').value = '';
    document.getElementById('txtCnpj').value = '';
    document.getElementById('ugn').value = '';
}

function buscaGridCnpj() {
    filtro = document.getElementById('txtCnpj');
    tabela = document.getElementById('lista');
    nomeFiltro = filtro.value;
    for (var i = 1; i < tabela.rows.length; i++) {
        var conteudoCelula = tabela.rows[i].cells[1].innerText;
        var corresponde = conteudoCelula.indexOf(nomeFiltro) >= 0;
        tabela.rows[i].style.display = corresponde ? '' : 'none';
    }
    document.getElementById('cidade').value = '';
    document.getElementById('empresa').value = '';
    document.getElementById('ugn').value = '';
}

function buscaGridUgn() {
    filtro = document.getElementById('ugn');
    tabela = document.getElementById('lista');
    nomeFiltro = filtro.value.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, "");
    for (var i = 1; i < tabela.rows.length; i++) {
        var conteudoCelula = tabela.rows[i].cells[3].innerText;
        var corresponde = conteudoCelula.toLowerCase().indexOf(nomeFiltro) >= 0;
        tabela.rows[i].style.display = corresponde ? '' : 'none';
    }
    document.getElementById('cidade').value = '';
    document.getElementById('empresa').value = '';
    document.getElementById('txtCnpj').value = '';
}

function SalvarContato() {
    idEmpresa = document.getElementById('idEmpresa').value;
    dataNasc = document.getElementById('dataNasc').value;
    tipoContato = document.getElementById('tipoContato').value;
    decisor = document.getElementById('decisor').checked;
    medico = document.getElementById('medico').checked;
    residente = document.getElementById('residente').checked;
    estadoMedico = document.getElementById('estadoMedico').value;
    crm = document.getElementById('crm').value;
    whatsapp = document.getElementById('whatsapp').innerText;
    telefone = document.getElementById('txtTelefone').value;
    email = document.getElementById('txtemail').value;
    nome = document.getElementById('txtnomecontato').value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();

    if (tipoContato == '') {
        Notiflix.Report.Failure('Atenção', 'Campo Tipo de Contato não informado', 'Fechar');
        return;
    }
    if (decisor) {
        decisor = 1;
    } else {
        decisor = 0;
    }
    if (medico) {
        medico = 1;
    } else {
        medico = 0;
    }
    if (residente) {
        residente = 1;
    } else {
        residente = 0;
    }
    if (medico == 1) {
        if (verificadorMedico == 0) {
            Notiflix.Report.Failure('Atenção', 'Por favor clique em Buscar Médico e valide as informações', 'Fechar');
            return;
        }
    }
    if (nome.length == 0) {
        Notiflix.Report.Failure('Atenção', 'Campo Nome não informado', 'Fechar');
        return;
    }
    if (whatsapp == '✓ WhatsApp') {
        whatsapp = 1;
    } else {
        whatsapp = 0;
    }
    alert(decisor);
    formData = new FormData();
    formData.append('tipoContato', tipoContato);
    formData.append('idEmpresa', idEmpresa);
    formData.append('decisor', decisor);
    formData.append('medico', medico);
    formData.append('residente', residente);
    formData.append('estadoMedico', estadoMedico);
    formData.append('crm', crm);
    formData.append('whatsapp', whatsapp);
    formData.append('telefone', telefone);
    formData.append('email', email);
    formData.append('nome', nome);
    formData.append('dataNasc', dataNasc);
    formData.append('verificadorMedico', verificadorMedico);
    $.ajax({
            type: "POST",
            url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/adicionarContato',
            data: formData,
            contentType: false,
            processData: false,
            beforeSend: function () {
                Notiflix.Loading.Circle('Aguarde...');
            },
            success: function (retorno) {
                alert(retorno);
                return;
                if (retorno == 1) {
                    Notiflix.Report.Failure('Atenção', 'Ocorreu um erro ao salvar.', 'Fechar');
                }
                tabela = document.getElementById('tableContatos');
                row = tabela.getElementsByTagName('tr');
                for (i = row.length; i > 1; i--) {
                    tabela.deleteRow(i - 1);
                }
                retorno = $.parseJSON(retorno);
                for (i = 0; i < retorno.length; i++) {
                    gerarTabela('tableContatos', retorno[i]);
                }
                document.getElementById('dataNasc').value = '';
                document.getElementById('tipoContato').value = '';
                document.getElementById('decisor').checked = false;
                document.getElementById('medico').checked = false;
                document.getElementById('residente').checked = false;
                document.getElementById('estadoMedico').value = '';
                document.getElementById('crm').value = '';
                document.getElementById('whatsapp').innerText = 'WhatsApp';
                document.getElementById('whatsapp').classList.remove("btn-success");
                document.getElementById('txtTelefone').value = '';
                document.getElementById('txtemail').value = '';
                document.getElementById('txtnomecontato').value = '';
                document.getElementById('txtnomecontato').disabled = false;
                $('.isMedico').fadeOut();
                $('#selectTipo').find('.placeholder').text('Tipo Contato');
                $('#selectUf').find('.placeholder').text('UF');
                verificadorMedico = 0;
                setTimeout(function () {
                    Notiflix.Loading.Remove();
                }, 900);
            }
        }
    );
}

function gerarTabela(idTabela, dados) {
    crm = '';
    re = / /gi;
    dados.crm = dados.crm.replace(re, '');
    if (dados.crm.toString().length > 1) {
        crm = "CRM: ";
    }
    if (dados.residente == 1) {
        dados.residente = 'Sim';
    } else {
        dados.residente = 'Não';
    }
    if (dados.decisor == 1) {
        dados.decisor = 'Sim';
    } else {
        dados.decisor = 'Não';
    }
    if (dados.dataNasc == '1900-01-01') {
        dataNasc = '';
    } else {
        dataNasc = dados.dataNasc.split('-');
        dataNasc = dataNasc[2] + '/' + dataNasc[1] + '/' + dataNasc[0];
    }
    tr = '<tr>' +
        '<td>' +
        '' + dados.nome + '<br><b>' + crm + '</b>' + dados.uf + dados.crm + '</td>' +
        '<td>' +
        '' + dados.decisor + '</td>' +
        '<td>' +
        '' + dados.contatoTipo + '</td>' +
        '<td>' +
        '' + dados.residente + '</td>' +
        '<td>' +
        '' + dados.email + '<br>\n' +
        '' + dados.telefone + '</td>\n' +
        '<td>\n' +
        '' + dataNasc + '</td>\n' +
        '<td class="text-right">\n' +
        '<div class="btn-group btn-group-sm" role="group">\n' +
        '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">AÇÃO\n' +
        '<span class="caret"></span></button>\n' +
        '<ul class="dropdown-menu dropdown-menu-right">\n' +
        '<li><a href="#" onclick="EditarContato(' + dados.id + ')"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Editar</a>\n' +
        '</li>\n' +
        '<li><a href="#" onclick="ExcluirContato(' + dados.id + ')"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Excluir</a>\n' +
        '</li>\n' +
        '</ul>\n' +
        '</div>\n' +
        '</td>\n' +
        '</tr>';
    tabela = document.getElementById(idTabela);
    row = tabela.insertRow(1);
    row.innerHTML = tr;
}

function AbrirSelect(elemento) {
    var parent = $(elemento).closest('.select');
    if (!parent.hasClass('is-open')) {
        parent.addClass('is-open');
        $('.select.is-open').not(parent).removeClass('is-open');
    } else {
        parent.removeClass('is-open');
    }
}

function Selecionar(elemento) {
    var parent = $(elemento).closest('.select');
    parent.removeClass('is-open').find('.placeholder').text($(elemento).text());
    parent.addClass('is-open');
    $('.select.is-open').not(parent).removeClass('is-open');
    parent.find('input[type=hidden]').attr('value', $(elemento).attr('data-value'));
}

function isMedico(elemento) {
    if (elemento.checked) {
        $('.isMedico').fadeIn();
        return;
    }
    $('.isMedico').fadeOut();
}

function EditarContato(id) {
    Notiflix.Loading.Circle('Aguarde...');
    var formData = new FormData();
    idEmpresa = document.getElementById('idEmpresa').value;
    formData.append('idContato', id);
    formData.append('idEmpresa', idEmpresa);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/editarContato',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
        }, success: function (retorno) {
            if (retorno == 1) {
                alert('Ocorreu um erro ao salvar.');
            }
            tabela = document.getElementById('tableContatos');
            row = tabela.getElementsByTagName('tr');
            for (i = row.length; i > 1; i--) {
                tabela.deleteRow(i - 1);
            }
            retorno = $.parseJSON(retorno);
            for (i = 0; i < retorno.length; i++) {
                if (retorno[i].id == id) {
                    document.getElementById('dataNasc').value = '';
                    document.getElementById('tipoContato').value = '';
                    document.getElementById('decisor').checked = false;
                    document.getElementById('medico').checked = false;
                    document.getElementById('residente').checked = false;
                    document.getElementById('estadoMedico').value = '';
                    document.getElementById('crm').value = '';
                    document.getElementById('whatsapp').innerText = 'WhatsApp';
                    document.getElementById('whatsapp').classList.remove("btn-success");
                    document.getElementById('txtTelefone').value = '';
                    document.getElementById('txtemail').value = '';
                    document.getElementById('txtnomecontato').value = '';
                    $('.isMedico').fadeOut();
                    $('#selectTipo').find('.placeholder').text('Tipo Contato');
                    $('#selectUf').find('.placeholder').text('Uf');
                    document.getElementById('dataNasc').value = retorno[i].dataNasc;
                    document.getElementById('tipoContato').value = retorno[i].contatoTipo;
                    document.getElementById('decisor').checked = retorno[i].decisor;
                    if (retorno[i].crm > 0) {
                        document.getElementById('medico').checked = true;
                        $('.isMedico').fadeIn();
                        document.getElementById('estadoMedico').value = retorno[i].uf;
                        document.getElementById('crm').value = retorno[i].crm;
                        $('#selectUf').find('.placeholder').text(retorno[i].uf);
                        document.getElementById('residente').checked = retorno[i].residente;
                    }
                    if (retorno[i].whatsapp == 1) {
                        document.getElementById('whatsapp').innerText = '✓ WhatsApp';
                        document.getElementById('whatsapp').classList.add("btn-success");
                    }
                    document.getElementById('txtTelefone').value = retorno[i].telefone;
                    document.getElementById('txtemail').value = retorno[i].email;
                    document.getElementById('txtnomecontato').value = retorno[i].nome;
                    $('#selectTipo').find('.placeholder').text(retorno[i].contatoTipo);
                    continue;
                }
                gerarTabela('tableContatos', retorno[i]);
            }
        }
    });
    setTimeout(function () {
        Notiflix.Loading.Remove();
    }, 900);
}

function ExcluirContato(id) {
    Notiflix.Loading.Circle('Aguarde...');
    var formData = new FormData();
    idEmpresa = document.getElementById('idEmpresa').value;
    formData.append('idContato', id);
    formData.append('idEmpresa', idEmpresa);
    $.ajax({
        type: "POST",
        url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/controlSwitch/excluirContatos',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
        }, success: function (retorno) {
            if (retorno == 1) {
                alert('Ocorreu um erro ao salvar.');
            }
            tabela = document.getElementById('tableContatos');
            row = tabela.getElementsByTagName('tr');
            for (i = row.length; i > 1; i--) {
                tabela.deleteRow(i - 1);
            }
            retorno = $.parseJSON(retorno);
            for (i = 0; i < retorno.length; i++) {
                gerarTabela('tableContatos', retorno[i]);
            }
        }

    });
    setTimeout(function () {
        Notiflix.Loading.Remove();
    }, 900);
}

function gerarTabela(idTabela, dados) {
    crm = '';
    re = / /gi;
    dados.crm = dados.crm.replace(re, '');
    if (dados.crm.toString().length > 1) {
        crm = "CRM: ";
    }
    if (dados.residente == 1) {
        dados.residente = 'Sim';
    } else {
        dados.residente = 'Não';
    }
    if (dados.decisor == 1) {
        dados.decisor = 'Sim';
    } else {
        dados.decisor = 'Não';
    }
    if (dados.dataNasc == '1900-01-01') {
        dataNasc = '';
    } else {
        dataNasc = dados.dataNasc.split('-');
        dataNasc = dataNasc[2] + '/' + dataNasc[1] + '/' + dataNasc[0];
    }
    whats = '';
    if (dados.whatsapp == 1) {
        whats = '<img width="14"src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></td>';
    }
    tr = '<tr>' +
        '<td>' +
        '' + dados.nome + '<br><b>' + crm + '</b>' + dados.uf + dados.crm + '</td>' +
        '<td>' +
        '' + dados.decisor + '</td>' +
        '<td>' +
        '' + dados.contatoTipo + '</td>' +
        '<td>' +
        '' + dados.residente + '</td>' +
        '<td>' +
        '' + dados.email + '<br>\n' +
        '' + dados.telefone + ' ' + whats + '<td>' +
        '' + dataNasc + '</td>\n' +
        '<td class="text-right">\n' +
        '<div class="btn-group btn-group-sm" role="group">\n' +
        '<button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">AÇÃO\n' +
        '<span class="caret"></span></button>\n' +
        '<ul class="dropdown-menu dropdown-menu-right">\n' +
        '<li><a href="#" onclick="EditarContato(' + dados.id + ')"><span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Editar</a>\n' +
        '</li>\n' +
        '<li><a href="#" onclick="ExcluirContato(' + dados.id + ')"><span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Excluir</a>\n' +
        '</li>\n' +
        '</ul>\n' +
        '</div>\n' +
        '</td>\n' +
        '</tr>';
    tabela = document.getElementById(idTabela);
    row = tabela.insertRow(1);
    row.innerHTML = tr;
}