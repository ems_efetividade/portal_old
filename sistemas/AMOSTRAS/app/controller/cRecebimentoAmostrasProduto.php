<?php
require_once('lib/appController.php');
require_once('app/model/mRecebimentoAmostrasProduto.php');
require_once('app/view/formsRecebimentoAmostrasProduto.php');

class cRecebimentoAmostrasProduto extends appController {

    private $modelRecebimentoAmostrasProduto = null;

    public function __construct(){
        $this->modelRecebimentoAmostrasProduto = new mRecebimentoAmostrasProduto();
    }

    public function main(){
        $this->render('vRecebimentoAmostrasProduto');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsRecebimentoAmostrasProduto();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsRecebimentoAmostrasProduto();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsRecebimentoAmostrasProduto();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            case 'excel':
                $forms = new formsRecebimentoAmostrasProduto();
                $colunas = $this->listarColunasTabela();
                $forms->formExcel($colunas);
                break;
            case 'inserir':
                echo '1';
                $this->save();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function listarColunasTabela(){
        $metodos = get_class_methods('mRecebimentoAmostrasProduto');
        foreach ($metodos as $metodo){
            if(substr($metodo, 0,3)=="set"){
                $atributo = substr($metodo, 3);
                if($atributo!='Id' && $atributo!='DataIn'){
                    for($i=0;$i<=strlen($atributo);$i++){
                        if(ctype_upper($atributo[$i])&&$i!=0){
                            $posicoes[] = $i;
                        }
                    }
                    if(is_array($posicoes)) {
                        foreach ($posicoes as $posicao) {
                            $antes = substr($atributo, 0, $posicao);
                            $depois = substr($atributo, $posicao);
                            $atributo = $antes . " " . $depois;
                        }
                    }
                    $posicoes = null;
                    $colunas[]= $atributo;
                }
            }
        }
        return $colunas;
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $this->modelRecebimentoAmostrasProduto->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $this->modelRecebimentoAmostrasProduto->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        return $this->modelRecebimentoAmostrasProduto->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        return $this->modelRecebimentoAmostrasProduto->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $this->modelRecebimentoAmostrasProduto->updateObj();
    }
}
