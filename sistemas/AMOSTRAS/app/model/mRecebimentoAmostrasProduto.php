<?php
require_once('lib/appConexao.php');

class mRecebimentoAmostrasProduto extends appConexao implements gridInterface
{

    private $id;
    private $codigo;
    private $nome;
    private $lote;
    private $loteNf;
    private $dataValidade;
    private $controleEspecial;
    private $quantidade;
    private $setor;
    private $dataIn;

    public function __construct()
    {
        $this->id;
        $this->codigo;
        $this->nome;
        $this->lote;
        $this->loteNf;
        $this->dataValidade;
        $this->controleEspecial;
        $this->quantidade;
        $this->setor;
        $this->dataIn;
    }

    public function getQuantidade()
    {
        return $this->quantidade;
    }

    public function setQuantidade($quantidade)
    {
        $this->quantidade = $quantidade;
    }

    public function getSetor()
    {
        return $this->setor;
    }

    public function setSetor($setor)
    {
        $this->setor = $setor;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCodigo()
    {
        return $this->codigo;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getLote()
    {
        return $this->lote;
    }

    public function getLoteNf()
    {
        return $this->loteNf;
    }

    public function getDataValidade()
    {
        return $this->dataValidade;
    }

    public function getControleEspecial()
    {
        return $this->controleEspecial;
    }

    public function getDataIn()
    {
        return $this->dataIn;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setCodigo($Codigo)
    {
        $this->codigo = $Codigo;
    }

    public function setNome($Nome)
    {
        $this->nome = $Nome;
    }

    public function setLote($Lote)
    {
        $this->lote = $Lote;
    }

    public function setLoteNf($LoteNf)
    {
        $this->loteNf = $LoteNf;
    }

    public function setDataValidade($DataValidade)
    {
        $this->dataValidade = $DataValidade;
    }

    public function setControleEspecial($ControleEspecial)
    {
        $this->controleEspecial = $ControleEspecial;
    }

    public function setDataIn($DataIn)
    {
        $this->dataIn = $DataIn;
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from RECEBIMENTO_AMOSTRAS_PRODUTO";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($codigo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CODIGO LIKE '%$codigo%' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if (strcmp($lote, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOTE LIKE '%$lote%' ";
            $verif = true;
        }
        if (strcmp($loteNf, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOTE_NF LIKE '%$loteNf%' ";
            $verif = true;
        }
        if (strcmp($dataValidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_VALIDADE IN ('$dataValidade') ";
            $verif = true;
        }
        if (strcmp($controleEspecial, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CONTROLE_ESPECIAL LIKE '%$controleEspecial%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $quantidade = explode(',',$_POST['quantidade']);
        $quantidade = $quantidade[0];
        $setor = $_POST['setor'];
        $controleEspecial = 0;
        $sql = "INSERT INTO RECEBIMENTO_AMOSTRAS_PRODUTO ([CODIGO],[NOME],[LOTE],[LOTE_NF],[DATA_VALIDADE],[CONTROLE_ESPECIAL]) values";
        $sql .= " ('$codigo','$nome','$lote','$loteNf','$dataValidade',$controleEspecial)";
        $this->executar($sql);
        $sql = "select TOP 1 ID from RECEBIMENTO_AMOSTRAS_PRODUTO ORDER BY ID DESC";
        $retorno = $this->executarQueryArray($sql);
        $id_produto = $retorno[1][0];
        $sql = "INSERT INTO RECEBIMENTO_AMOSTRAS_PRODUTO_SETOR (QUANTIDADE,SETOR,ID_PRODUTO) VALUES ($quantidade,'$setor',$id_produto)";
        $this->executar($sql);
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == '+') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == '-') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (" . $sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= " . $atual;
        $paginacao .= " AND row <= " . $max . " ";
        return $paginacao;
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM RECEBIMENTO_AMOSTRAS_PRODUTO WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = '$id' ";
            $verif = true;
        }
        if (strcmp($codigo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CODIGO = '$codigo' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NOME = '$nome' ";
            $verif = true;
        }
        if (strcmp($lote, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "LOTE = '$lote' ";
            $verif = true;
        }
        if (strcmp($loteNf, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "LOTE_NF = '$loteNf' ";
            $verif = true;
        }
        if (strcmp($dataValidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_VALIDADE IN ('$dataValidade') ";
            $verif = true;
        }
        if (strcmp($controleEspecial, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CONTROLE_ESPECIAL = '$controleEspecial' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CODIGO ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "LOTE ";
        $sql .= ",";
        $sql .= "LOTE_NF ";
        $sql .= ",";
        $sql .= "DATA_VALIDADE ";
        $sql .= ",";
        $sql .= "CONTROLE_ESPECIAL ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM RECEBIMENTO_AMOSTRAS_PRODUTO ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($codigo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CODIGO LIKE '%$codigo%' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if (strcmp($lote, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOTE LIKE '%$lote%' ";
            $verif = true;
        }
        if (strcmp($loteNf, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOTE_NF LIKE '%$loteNf%' ";
            $verif = true;
        }
        if (strcmp($dataValidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_VALIDADE IN ('$dataValidade') ";
            $verif = true;
        }
        if (strcmp($controleEspecial, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CONTROLE_ESPECIAL LIKE '%$controleEspecial%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  RECEBIMENTO_AMOSTRAS_PRODUTO";
        $sql .= " SET ";
        $sql .= "CODIGO = '$codigo' ";
        $sql .= " , ";
        $sql .= "NOME = '$nome' ";
        $sql .= " , ";
        $sql .= "LOTE = '$lote' ";
        $sql .= " , ";
        $sql .= "LOTE_NF = '$loteNf' ";
        $sql .= " , ";
        $sql .= "DATA_VALIDADE = '$dataValidade' ";
        $sql .= " , ";
        $sql .= "CONTROLE_ESPECIAL = '$controleEspecial' ";
        $sql .= " , ";
        $sql .= "DATA_IN = '$dataIn' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $codigo = $_POST['codigo'];
        $nome = $_POST['nome'];
        $lote = $_POST['lote'];
        $loteNf = $_POST['lotenf'];
        $dataValidade = $_POST['datavalidade'];
        $controleEspecial = $_POST['controleespecial'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CODIGO ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "LOTE ";
        $sql .= ",";
        $sql .= "LOTE_NF ";
        $sql .= ",";
        $sql .= "DATA_VALIDADE ";
        $sql .= ",";
        $sql .= "CONTROLE_ESPECIAL ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM RECEBIMENTO_AMOSTRAS_PRODUTO ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($codigo, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CODIGO LIKE '%$codigo%' ";
            $verif = true;
        }
        if (strcmp($nome, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if (strcmp($lote, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "LOTE LIKE '%$lote%' ";
            $verif = true;
        }
        if (strcmp($loteNf, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "LOTE_NF LIKE '%$loteNf%' ";
            $verif = true;
        }
        if (strcmp($dataValidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_VALIDADE IN ('$dataValidade') ";
            $verif = true;
        }
        if (strcmp($controleEspecial, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CONTROLE_ESPECIAL LIKE '%$controleEspecial%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {

        $o = new mRecebimentoAmostrasProduto();
        $o->setId($param[0]);
        $o->setCodigo($param[1]);
        $o->setNome($param[2]);
        $o->setLote($param[3]);
        $o->setLoteNf($param[4]);
        $o->setDataValidade($param[5]);
        $o->setControleEspecial($param[6]);
        $o->setDataIn($param[7]);
        $o->setQuantidade($param[8]);
        $o->setSetor($param[9]);
        return $o;

    }
}