<?php
class formsRecebimentoAmostrasProduto{

    public function formEditar($objeto){
        ?>
    <form id="formEditar" action="<?php print appConf::caminho ?>RecebimentoAmostrasProduto/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="codigo">Código</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getCodigo()?>" name="codigo" id="codigo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">Nome Produto</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
        </div>
        </form>
        <?php
    }

    public function formExcel($colunas){
        ?>
    <form id="formExcel" action="<?php print appConf::caminho ?>RecebimentoAmostrasProduto/controlSwitch/upload">
        <div class="row">
            <input type="hidden" name="id" value="">
            <div class="col-sm-8 text-left">
                <div class="form-group form-group-sm">
                    <label for="termo" >Arquivo</label>
                    <label  for="termo"  class="form-control"  id="nomeTermo" ></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label for="termo" style="color: white;">.</label><br>
                    <label for="termo" class="form-control btn btn-default  btn-block  text-center">
                        <label for="termo" id="btnTermo" style="font-size: 18px" class="glyphicon glyphicon-cloud-upload"> </label>
                    </label>
                    <input required style="display: none;" type="file"  name="termo" id="termo" >
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm text-right">
                    <label for="termo" style="color: white;">.</label><br>
                    <span style="display: none;" id="btn-enviar-excel" class="btn btn-success form-control">Enviar</span>
                </div>
            </div>
            <div style="display:none;" id="barraProgresso"  class="col-sm-12 text-left">
                <div class="progress">
                    <div class="progress-bar progress-bar-success progress-bar-striped" id="barra" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:100%">
                        <span id="textobarra">AGUARDE...</span>
                    </div>
                </div>
                <i id="contador" class="text-center">0 de 0 linha(s) inseridas </i><i id="tempo"></i><br>
                <style>
                    #centro {
                        margin-left: 20%;
                        border-radius: 50%;
                    }
                </style>
                <img id="centro" style="align-content:center!important;" src="https://media.giphy.com/media/3oEdv5FBPbfMJbraBG/giphy.gif"/>
            </div>
            <div class="col-sm-12 text-left apontadores" style="display: none">
                <div class="form-group form-group-sm " >
                    <h5>Indique as colunas do arquivo</h5>
                </div>
            </div>
            <div id="camposUpload">
            <?php
            $i= 0;
            foreach ($colunas as $coluna)
            {
                ?>
                  <div class="col-sm-6 text-left apontadores" style="display: none">
                    <div class="form-group form-group-sm ">
                    <input type="hidden" id="colunas_<?php echo $i?>" name="<?php echo strtolower(str_replace(" ","",$coluna)) ?>">
                        <label><?php echo $coluna?></label>
                        <div id="<?php echo $i?>" class="colunasCheck selectColunas">
                        </div>
                    </div>
                  </div>
                <?php
                $i++;
            }
            ?>
            </div>
            <input type="hidden" id="quantidadeIns" value="0">
            <script src="/../../../../public/js/amostras.js">
        </div>
        </form>
        <?php
    }

    public function formVisualizar($objeto){
        ?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="codigo">Código</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getCodigo()?>" name="codigo" id="codigo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">Nome Produto</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar(){?>
        <form id="formSalvar" action="<?php print appConf::caminho ?>RecebimentoAmostrasProduto/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="codigo">Código</label>
                        <input required type="text" class="form-control " name="codigo" id="codigo" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="nome">Nome Produto</label>
                        <input required type="text" class="form-control " name="nome" id="nome" >
                    </div>
                </div>
            </div>
        </form><?php 
    }
}
