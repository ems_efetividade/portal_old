<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('lib/appConf.php');
require_once('lib/appSetup.php');
require_once('lib/appFunction.php');

?>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"/>
<head-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
<title>
    <?php print appConf::nomeProjeto ?>
</title>
<!--Plugin javascript do jquery e bootstrap-->

<script src="/../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
<script src="<?php echo appConf::caminho ?>plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo appConf::caminho ?>public/js/jquery.mask.js"></script>
<script src="<?php echo appConf::caminho ?>public/js/index.js"></script>
<link rel="shortcut icon" href="<?php echo appConf::caminho ?>public/img/liferay.ico" type="image/x-icon" />
<!--Folhas de estilo do bootstrap, das fontes e do index-->
        
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css" />
        
<style>
    /*VACINA PRO IPAD EM LANDSCAPE*/
    @media all and (min-width: 768px) and (max-width: 1024px) {
        .navbar-header {
            float: none;
        }
        .navbar-left,
        .navbar-right {
            float: none !important;
        }
        .navbar-toggle {
            display: block;
        }
        .navbar-collapse {
            border-top: 1px solid transparent;
            box-shadow: inset 0 1px 0 rgba(255, 255, 255, 0.1);
        }
        .navbar-fixed-top {
            top: 0;
            border-width: 0 0 1px;
        }
        .navbar-collapse.collapse {
            display: none!important;
        }
        .navbar-nav {
            float: none!important;
            margin-top: 7.5px;
        }
        .navbar-nav>li {
            float: none;
        }
        .navbar-nav>li>a {
            padding-top: 10px;
            padding-bottom: 10px;
        }
        .collapse.in {
            display: block !important;
        }
    }
</style>
</head>
<body>
<!--Barra de Navegação-->
<nav class="navbar navbar-default navbar-fixed-top nav-caollapse" role="navigation" style="font-size:14px;">
    <div class="navbar-header">
        <a class="navbar-brand" href="#"><span class="glyphicon fa glyphicon-file"></span>&nbsp;<?php print appConf::nomeProjeto ?></a>
    </div>
</nav>
<!--MODAL Aguarde-->
<div class="modal fade" style="z-index:100000;" id="modalAguarde" data-basckdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content ">

            <div class="modal-body">
                <p class="text-center"><strong>Aguarde...</strong></p>
                <h5 class="text-center"><small>Gerando o seu protocolo</small></h5>
                <div class="progress">
                    <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                        <span class="sr-only"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal modal-center" style="z-index:10000000;" id="modalMsgBox" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content ">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <!--<div class="col-lg-3">
            	<p class="text-center text-danger" style="font-size:500%;"><span class="glyphicon glyphicon-remove"></span><p>
            </div>-->
                    <div class="col-lg-12">
                        <h5><p class="text-center" id="msgboxTexto"></p></h5>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
            </div>
        </div>
    </div>
</div>