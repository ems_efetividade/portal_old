<?php
require_once('header.php');

class vRecebimentoAmostrasProduto extends gridView{


    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        $cols = array('ID','CODIGO','NOME','LOTE','LOTENF','DATAVALIDADE','CONTROLEESPECIAL','DATAIN');
        $colsPesquisaNome = array('ID','CODIGO','NOME','LOTE','LOTENF','DATAVALIDADE','CONTROLEESPECIAL','DATAIN');
        $colsPesquisa = array('id','codigo','nome','lote','loteNf','dataValidade','controleEspecial','dataIn');
        $array_metodo = array('Id','Codigo','Nome','Lote','LoteNf','DataValidade','ControleEspecial','DataIn');
        gridView::setGrid_titulo('Cadastro de Amostras');
        gridView::setGrid_sub_titulo('Listagem de Amostras Cadastradas para Sistema de Upload de Termos');
        gridView::setGrid_objeto('RecebimentoAmostrasProduto');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_botao_topo(array(".XLS","excel","btn_excel"," btn-info","open-file"));
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();

    }

}