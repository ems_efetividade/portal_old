<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('lib/appConf.php');

class appConexao extends appConf {

    private $local;
    private $user;
    private $senha;
    private $banco;
    private $numRows;
    private $cn;
	
        function appConexao() {
        $this->local = appConf::dbLocal;
        $this->user  = appConf::dbUser;
        $this->senha = appConf::dbPass;
        $this->banco = appConf::dbName;
    }
        
    public function getNumRows() {
        return $this->numRows;
    }

    private function abrir() {
        $cx = odbc_connect(appConf::dbLocal, appConf::dbUser, appConf::dbPass);
        if($cx === false) {
            echo "erro: ".odbc_errormsg().'';
            echo "<br>";
            echo $this->local." - ".$this->user." - ".$this->senha;
            die();
        } else {
            $this->cn = odbc_connect(appConf::dbLocal, appConf::dbUser, appConf::dbPass);
        }
    }
	
    // executa uma query e não retorna nada
    public function executar($str) {
        $this->abrir();
        $retorno = odbc_exec($this->cn, utf8_decode($str));
        $this->fechar();
        return $retorno;
    }
	
    // executa uma query e retorna um recordset em forma de array
    public function executarQueryArray($str) {
        $this->abrir();
        $arr    = array();
        $rs     = odbc_exec($this->cn, $str) or die($str);
        $x      = 1;
        //echo $str.'<br>';
        while (odbc_fetch_row($rs)) {
            for ($y = 1; $y <= odbc_num_fields($rs); $y++)  {
                $arr[$x][odbc_field_name($rs, $y)] = utf8_encode(odbc_result($rs,$y));
                //$arr[$x][odbc_field_name($rs, $y)] = odbc_result($rs,$y);
            }
            $x++;
        }
        if ($x > 1) {
            return $arr;
            $this->numRows = odbc_num_rows($rs);
        }
        $this->fechar();
    }
        
    public function query($query) {
        return odbc_exec($this->cn, $query);
    }
	
    //Fecha o banco
    private function fechar() {
        odbc_close($this->cn);
    }

}

?>