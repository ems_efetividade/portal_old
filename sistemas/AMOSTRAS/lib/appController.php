<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('lib/appView.php');
require_once('lib/appFunction.php');

class appController extends appView {
	
    public $visao = null;
	
    function appController() {
        $this->visao = new appView();
    }

    public function main() {
        die('main do appController');
    }
	
    public function location($url) {
        header('Location: '.$url);
    }
        
    public function reload() {
        header('Location: '.$_SERVER['HTTP_REFERER']);
    }
}

?>