<?php 

require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/mDocumento.php');
require_once('app/model/mDocumentoResposta.php');
require_once('app/model/mColaboradorSetor.php');

require_once('../../app/model/perfilModel.php');
require_once('../../app/model/linhaModel.php');

class cAdmin extends appController {
    
    private $documento = null;
    private $resposta = null;
    private $perfil = null;
    private $linha = null;
    
    public function __construct() {
        $this->documento = new mDocumento();
        $this->perfil = new perfilModel();
        $this->resposta = new mDocumentoResposta();
        $this->linha = new linhaModel();
    }
    
    
    public function main() {
        
        
        $this->set('documentos', $this->documento->listar());
        $this->set('menu1', 'active');
        $this->render('admin/vHome');
    }
    
    
    public function editar($idDocumento=0) {
        
        $this->documento->setIdDocumento($idDocumento);
        $this->documento->selecionar();
        
        $this->set('menu1', 'active');
        $this->set('documento', $this->documento);
        $this->set('perfis', $this->perfil->listar());
        $this->set('linhas', $this->linha->listar());
        $this->render('admin/vFormDocumento');
        
    }
    
    public function salvarDocumento() {
        
        $idDoc = appSanitize::filter($_POST['txtIdDocumento']);
        $nomeDoc = appSanitize::filter($_POST['txtNomeDocumento']);
        $nomeArq = appSanitize::filter($_POST['txtNomeArquivo']);
        $ativo = isset($_POST['txtAtivo']) ? appSanitize::filter($_POST['txtAtivo']) : 0;
        $dataInicio = (appSanitize::filter($_POST['txtDataInicio']) != "") ? appFunction::formatarData(trim(appSanitize::filter($_POST['txtDataInicio']))).' '.appSanitize::filter($_POST['cmbHoraInicio']) : '';
        $dataExpirar = (appSanitize::filter($_POST['txtDataExpirar']) != "") ? appFunction::formatarData(trim(appSanitize::filter($_POST['txtDataExpirar']))).' '.appSanitize::filter($_POST['cmbHoraExpirar']) : '';
                
        $arquivo = (file_exists($_FILES['filDocumento']['tmp_name'])) ? $_FILES['filDocumento'] : '';
        $permissao = appSanitize::filter($_POST['chkPerfil']);
        $permissaoLinha = appSanitize::filter($_POST['chkLinha']);
        
        $final = array();
        foreach($permissao as $a) {
            $x['ID_PERFIL'] = $a;
            
            if($permissaoLinha) {
                foreach($permissaoLinha as $b) {
                    $x['ID_LINHA']  = $b;
                    $final[] = $x;
                }
            } else {
                $x['ID_LINHA']  = 0;
                $final[] = $x;
            }
          
        }
        
        
        if(!$permissao) {
            $x['ID_PERFIL'] = 0;
            foreach($permissaoLinha as $b) {
                $x['ID_LINHA']  = $b;
                $final[] = $x;
            }
        }
        
     
        $this->documento->setIdDocumento($idDoc);
        $this->documento->setTitulo($nomeDoc);
        $this->documento->setNomeArquivo($nomeArq);
        $this->documento->setAtivo($ativo);
        $this->documento->setDataInicio($dataInicio);
        $this->documento->setDataExpirar($dataExpirar);
        
        $this->documento->setArquivo($arquivo);
        $this->documento->setPerfil($final);
        
        $retorno = $this->documento->salvar();
        
        if($retorno != '') {
            $html = '<h4 class="text-danger"><b>Houve alguns erros ao inserir os dados</b><br></h4>';
            
            if(is_array($retorno)) {
                $html .= '<ul>';
                foreach($retorno as $erro) {
                    $html .= '<li >'.$erro.'</li>';
                }
                $html .= '</ul>';
                
            } else {
                $html .= '<p>'.$retorno.'</p>';
            }
            
            echo $html;
        }

    }
    
    public function verDocumento($idDocumento) {
        $this->documento->setIdDocumento($idDocumento);
        $this->documento->selecionar();
        
        $this->set('menu1', 'active');
        $this->set('doc', $this->documento);
        $this->render('admin/vVerDocumento');
        
    }
    
    public function excluirDocumento($idDocumento) {
        $this->documento->setIdDocumento($idDocumento);
        echo $this->documento->excluir();
    }
    
    public function excluirResposta($idResposta) {
        $this->resposta->setIdDocResposta($idResposta);
        echo $this->resposta->excluir();
    }    
    
    public function pesquisarResposta() {
        $campo = appSanitize::filter($_POST['cmbCampo']);
        $criterio = appSanitize::filter($_POST['txtCriterio']);
        
        $resultado = $this->resposta->pesquisar($campo, $criterio);
        
        if(count($resultado) > 0) {
            $html = '<table class="table table-striped">'
                    . '<tr>'
                    . '<th>Colaborador</th>'
                    . '<th>Documento</th>'
                    . '<th>Data Assinatura</th>'
                    . '<th></th>'
                    . '</tr>';

            foreach($resultado as $resp) {

                $html .= '
                    <tr>
                        <td><small>'.$resp['SETOR'].' '. $resp['NOME'].'</small></td>
                        <td><small>'.$resp['DOCUMENTO'].'</small></td>
                        <td><small>'.appFunction::formatarData($resp['DATA_CADASTRO']).'</small></td>
                        <td>
                            <a class="btn btn-sm btn-primary" href='.appConf::caminho.'home/gerarArquivoPDF/'.$resp['ID_DOCUMENTO'].' ><span class="glyphicon glyphicon-save"></span></a>
                            <a class="btn btn-sm btn-danger" href="#" data-toggle="modal" data-target="#modalExcluir" data-id="'.$resp['ID_DOCUMENTO'].'"><span class="glyphicon glyphicon-remove"></span></a>
                        </td>
                    </tr>';

            }
            $table .= '</table>';
        } else {
            $html = '<h4 class="text-center">Nenhum resultado encontrado com os critérios selecionados.</h4>';
        }
        echo $html;
    }
    
    public function assinaturas() {
        $this->set('respostas', $this->resposta->pesquisar());
        $this->set('menu2', 'active');
        $this->render('admin/vAssinaturas');
    }
    
}