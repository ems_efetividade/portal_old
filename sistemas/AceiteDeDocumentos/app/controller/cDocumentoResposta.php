<?php 

require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/mDocumentoResposta.php');

class cDocumentoResposta extends appController {
    
    private $cResposta = null;
    
    public function __construct() {
        $this->cResposta = new mDocumentoResposta;
    }
    
    public function comboResposta() {
        $this->cResposta->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
        $respostas = $this->cResposta->listar();
        
        if(count($respostas) > 0) {
            
            $combo = '<select class="form-control" id="selPolitica"><option value="#">Selecione o documento/politica</option>';

            foreach($respostas as $resp) {
                $combo .= '<option value="'.$resp->getIdDocResposta().'">'.$resp->getProtocolo().' '. $resp->documento->getTitulo().'</option>';
            }

            $combo .= '</select>';

            echo $combo;
        } else {
            echo '<h4 class="text-center text-info">Não existem políticas para ser impressas.</h4>';
        }
    }
    
    public function gerarLinks() {
        $this->cResposta->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
        $respostas = $this->cResposta->listar();
        
        if(count($respostas) > 0) {
            
            $link = '';

            foreach($respostas as $resp) {
                 //$combo .= '<option value="'.$resp->getIdDocResposta().'">'.$resp->getProtocolo().' '. $resp->documento->getTitulo().'</option>';
                 $link .= '<p><a href="'.appConf::caminho.'home/gerarArquivoPDF/'.$resp->getIdDocResposta().'">'.$resp->documento->getTitulo().'</a></p>';
            }

            

            echo $link;
        } else {
            //echo '<h4 class="text-center text-info">Não existem políticas para ser impressas.</h4>';
        }
    }
}