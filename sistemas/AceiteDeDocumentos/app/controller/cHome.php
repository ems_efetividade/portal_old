<?php 
validarSessao();
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/mDocumento.php');
require_once('app/model/mDocumentoResposta.php');
require_once('app/model/mColaboradorSetor.php');
require_once('app/model/mColaborador.php');
require_once('app/model/mSetor.php');

class cHome extends appController {
    
    private $cDocumento = null;
    private $cResp = null;
    
    private $idColaborador = null;
    
    public function __construct() {
        
        //$this->cDocumento = new mDocumento();
       // $this->cResp =  new mDocumentoResposta();
        
       // $this->idColaborador =  appFunction::dadoSessao('id_colaborador');
    }
    
    public function main() {
       // $docs = $this->cDocumento->listarDocumentosAssinar($this->idColaborador);
        //echo count($docs);
      //  if(count($docs) > 0){
            
            
      //      $this->set('documento', $docs[0]);

      //      //$this->set('titulo', $docs[0]->getTitulo());
     //       $this->set('texto',  $this->substituirTags($docs[0]->getTexto(), $this->idColaborador));
     //       //$this->set('idDoc',  $docs[0]->getIdDocumento());
     //       $this->render('vHome');
        
     //   } else {
            //print_r($docs);
     //       //header('Location: '.EMS_URL);
   //         header('Location: '.EMS_URL);
    //    }
        
        //$this->render('vHome');
    }
    

    public function exibirPDF($idDocumento) {
        $this->cDocumento->setIdDocumento($idDocumento);
        $this->cDocumento->selecionar();
        
        $file = PASTA_PDF.utf8_decode($this->cDocumento->getNomeArquivo());
        
        
        
        header('Content-type: application/pdf');
        header('Content-Disposition: inline; filename="' . $this->cDocumento->getNomeArquivo() . '"');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($file));
        header('Accept-Ranges: bytes');

        @readfile($file);

    }
    
    public function mains() {

        $docs = $this->cDocumento->listarDocFaltante($this->idColaborador);
        
        if(count($docs) > 0){
            $this->set('documento', $docs[0]);    

            $this->set('titulo', $docs[0]->getTitulo());
            $this->set('texto',  $this->substituirTags($docs[0]->getTexto()));
            $this->set('idDoc',  $docs[0]->getIdDocumento());
            $this->render('vHome');
        
        } else {
            header('Location: '.EMS_URL);
        }
    }
    
    public function validarCaptcha() {
        
        $resposta = base64_decode($_POST['txtResposta']);
        $captcha = $_POST['txtCaptcha'];
        $idDoc = $_POST['txtIdDoc'];
        
        $this->cDocumento->setIdDocumento($idDoc);
        $this->cDocumento->selecionar();
        
        $protocolo = rand().appFunction::dadoSessao('setor');
        $nomeArquivo = str_replace(" ", "_", $protocolo."_".$this->cDocumento->getTitulo().'_'.appFunction::dadoSessao('nome')).'.pdf';
        
        session_start();
       
        if($_SESSION['captcha'] == $captcha){
            $this->cResp->setIdColaborador($this->idColaborador);
            $this->cResp->setIdDocumento($idDoc);
            $this->cResp->setResposta($resposta);
            $this->cResp->setSetor(appFunction::dadoSessao('setor'));
            $this->cResp->setProtocolo($protocolo);
            $this->cResp->setArquivo($nomeArquivo);
            
            echo $this->cResp->salvar();
        } else {
            echo 'erro';
        }
    }
    
    
    public function gerarArquivoPDF($idResposta=0, $baixar='') {
        
        // seta as respostas
        $resp = array('S' => 'Estou de acordo com os termos.',
                      'N' => 'Não estou de acordo com os termos.');

        $this->cResp->setIdDocResposta($idResposta);
        $this->cResp->selecionar();
        
        $nomeArquivo = str_replace(" ", "_", $this->cResp->getProtocolo()."_".$this->cResp->documento->getTitulo().'_'.appFunction::dadoSessao('nome')).'.pdf';
        $this->set('titulo', $this->cResp->documento->getTitulo());
        $this->set('texto', $this->substituirTags($this->cResp->documento->getTexto(), $this->cResp->getIdColaborador()));
        $this->set('resposta', $this->cResp->getResposta());
        $this->set('textoResposta', $resp[$this->cResp->getResposta()]);
        $this->set('data', appFunction::formatarData($this->cResp->getDataCadastro()));
        $this->set('protocolo', $this->cResp->getProtocolo());
            
            
        if($this->cResp->documento->getExtensao() != 'pdf') {  
            $this->set('resposta', $this->cRest);
            $html = $this->substituirTags($this->renderToString('vPDF'), $this->cResp->getIdColaborador(), $this->cResp->getSetor());
            $this->fn_gerarPDF($html, '', $nomeArquivo, $baixar);
        } else {
            $arquivo = utf8_decode(PASTA_PDF.$this->cResp->documento->getNomeArquivo());
            $html = $this->substituirTags($this->renderToString('vResposta'), $this->cResp->getIdColaborador(), $this->cResp->getSetor());
            $this->fn_gerarPDF2($html, $arquivo, $nomeArquivo, $baixar);
        }
 
        
        if($baixar != '') {
            header('Location: '.appConf::caminho);
        }
    }
    
    
    public function fn_gerarPDF2($html, $arquivo, $nomeArquivo, $salvarEm='') {
        ob_start();

        $pdf = new mPDF();
        

        $pdf->SetImportUse();
        $pageCount = $pdf->SetSourceFile($arquivo);
        for($i = 1; $i <= $pageCount; $i++) {
         $pdf->AddPage();
         $tpl = $pdf->ImportPage($i);
         $pdf->UseTemplate($tpl);
        }

        $pdf->AddPage();
        $pdf->WriteHTML($html);
        
        $t = ($salvarEm == '') ? 'D' : 'F';

        $pdf->Output($salvarEm.$nomeArquivo, $t);
        
        // output as a file
        //$pdf->Output();
        //exit();
    }  
    
    private function fn_gerarPDF($html='', $css='', $nomeArquivo='', $salvarEm='') {
        ob_start();
        $html = urldecode($html);
        $nome_arquivo = $nomeArquivo;
        $arquivo_css = $css;
        
        //$stylesheet = file_get_contents('css/css_imprimir_print.css');
        $pdf = new mPDF();
        $pdf->allow_charset_conversion=true;
        $pdf->showImageErrors = true;
        $pdf->charset_in='iso-8859-1';
        $pdf->SetDisplayMode('fullpage');
        $pdf->SetFooter('{PAGENO}');
        //$pdf->WriteHTML($stylesheet,1);
       
        
        $pdf->WriteHTML($html);

        $t = ($salvarEm == '') ? 'D' : 'F';

        $pdf->Output($salvarEm.$nome_arquivo, $t);
        //exit();
    }
    
    public function gerarPDF1($idResposta) {

        //$this->cResp->setIdDocResposta($idResposta);
        //$this->cResp->selecionar();
        
        //if(!file_exists(appConf::caminho.'/public/pdf/dominio/'.utf8_decode($this->cResp->getArquivo()))) {
         //   $this->salvarPDF($idResposta);
        //}

        //header('Content-Type: application/pdf');
        //header('Content-disposition: attachment;filename='.$this->cResp->getArquivo());
        //readfile(appConf::caminho.'/public/pdf/dominio/'.$this->cResp->getArquivo());
        
        
        $this->salvarPDF($idResposta, 1);
        /*
        $resp = array('S' => 'Estou de acordo com os termos.',
                      'N' => 'Não estou de acordo com os termos.');
        
        
        $this->cResp->setIdDocResposta($idResposta);
        $this->cResp->selecionar();
        
        $nomeArquivo = str_replace(" ", "_", $this->cResp->getProtocolo()."_".$this->cResp->documento->getTitulo().'_'.appFunction::dadoSessao('nome'));
        $this->set('titulo', $this->cResp->documento->getTitulo());
        $this->set('texto', $this->substituirTags($this->cResp->documento->getTexto()));
        $this->set('resposta', $this->cResp->getResposta());
        $this->set('textoResposta', $resp[$this->cResp->getResposta()]);
        $this->set('data', appFunction::formatarData($this->cResp->getDataCadastro()));
        $this->set('protocolo', $this->cResp->getProtocolo());

        
        $html = utf8_decode($this->substituirTags($this->renderToString('vPDF')));
        appFunction::gerarPDF($html, '', $nomeArquivo);
       */
    }
    
    public function gerarPDF($idResposta) {
        $this->salvarPDF($idResposta, 1);
    }
    public function  salvarPDF($idResposta, $download='') {
        
        $resp = array('S' => 'Estou de acordo com os termos.',
                      'N' => 'Não estou de acordo com os termos.');
        
        $this->cResp->setIdDocResposta($idResposta);
        $this->cResp->selecionar();
        
        $nomeArquivo = ($this->cResp->getArquivo() != "") ? utf8_decode($this->cResp->getArquivo()) : utf8_decode($this->cResp->documento->getTitulo()).".pdf" ;
        //$nomeArquivo = 'Documento.pdf';
        
        $documento = 'C:\xampp\htdocs\projetos\confirmacaoDocs\docs\Politica_de_Premiacao_2017_Gerente_Distrital-08.12.2016.pdf';        
        
        $this->set('texto', $this->substituirTags($this->cResp->documento->getTexto()));
        $this->set('textoResposta', $resp[$this->cResp->getResposta()]);
        $this->set('data', appFunction::formatarData($this->cResp->getDataCadastro()));
        $this->set('protocolo', $this->cResp->getProtocolo());
        
        $html = $this->substituirTags($this->renderToString('vResposta'));
        
        $baixar = ($download == '') ? PASTA_PDF.'dominio\\' : '';
        
        $this->fn_gerarPDF2($html, $documento,$nomeArquivo,$baixar);
        
        if($baixar != '') {
            header('Location: '.appConf::caminho);
        }
    }
    
    public function salvarPDFs($idResposta, $download='') {
        
        $resp = array('S' => 'Estou de acordo com os termos.',
                      'N' => 'Não estou de acordo com os termos.');
        
        
        $this->cResp->setIdDocResposta($idResposta);
        $this->cResp->selecionar();
        
        $nomeArquivo = ($this->cResp->getArquivo() != "") ? utf8_decode($this->cResp->getArquivo()) : utf8_decode($this->cResp->documento->getTitulo()).".pdf" ;

        //$nomeArquivo = utf8_decode(str_replace(" ", "_", $this->cResp->getProtocolo()."_".$this->cResp->documento->getTitulo().'_'.appFunction::dadoSessao('nome')));
        $nomeArquivo = ($this->cResp->getArquivo() != "") ? utf8_decode($this->cResp->getArquivo()) : utf8_decode($this->cResp->documento->getTitulo()).".pdf" ;
        $this->set('titulo', $this->cResp->documento->getTitulo());
        $this->set('texto', $this->substituirTags($this->cResp->documento->getTexto()));
        $this->set('resposta', $this->cResp->getResposta());
        $this->set('textoResposta', $resp[$this->cResp->getResposta()]);
        $this->set('data', appFunction::formatarData($this->cResp->getDataCadastro()));
        $this->set('protocolo', $this->cResp->getProtocolo());

        
        $html = $this->substituirTags($this->renderToString('vPDF'));
        
        $download = ($download == '') ? PASTA_PDF.'dominio\\' : '';
        
        $this->fn_gerarPDF($html, '', $nomeArquivo, $download);
        
        if($download != '') {
            header('Location: '.appConf::caminho);
        }
    }    
    
    public function getCaptcha() {
        


        // Establish image factors:
        $font_size = 25; // Font size is in pixels.
        $font_file = ROOT."public\\css\\fonts\\times_new_yorker.ttf"; // This is the path to your font file.
        $background = ROOT."public\\img\\white-wave.png";
        $char = 'ABCDEFGHJKLMNPRSTUVWXYZabcdefghjkmnprstuvwxyz23456789';

        
        $length = mt_rand(4, 5);
        while( strlen($codigoCaptcha) < $length ) {
           $codigoCaptcha .= substr($char, mt_rand() % (strlen($char)), 1);
        }

        $_SESSION['captcha'] = $codigoCaptcha;

        $imagemCaptcha = imagecreatefrompng($background);
        $corCaptcha = imagecolorallocate($imagemCaptcha,0,0,0);

        list($bg_width, $bg_height, $bg_type, $bg_attr) = getimagesize($background);
        
        $type_space = imagettfbbox($font_size, 0, $font_file, $codigoCaptcha);
        // Determine image width and height, 10 pixels are added for 5 pixels padding:
        $image_width = abs($type_space[4] - $type_space[0]) + 10;
        $image_height = abs($type_space[5] - $type_space[1]) + 10;
        
        $text_pos_x_max = ($bg_width) - ($image_width);
        $text_pos_y_max = ($bg_height) - ($image_height / 2) - 10;
        $text_pos_y_min = $bg_height;
        
        if ($text_pos_y_min > $text_pos_y_max) {
            $temp_text_pos_y = $text_pos_y_min;
            $text_pos_y_min = $text_pos_y_max;
            $text_pos_y_max = $temp_text_pos_y;
        }
        imagettftext($imagemCaptcha, 
                     $font_size, 
                     0, 
                     mt_rand(0, $text_pos_x_max), 
                     mt_rand($text_pos_y_min, $text_pos_y_max), 
                     $corCaptcha, 
                     $font_file, 
                     $codigoCaptcha);
        
        header("Content-type: image/png");
        imagepng($imagemCaptcha);
        imagedestroy($imagemCaptcha);
        
        
        
        
    }
    
    public function substituirTags($texto, $idColaborador, $setor='') {
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($idColaborador);
        $col->selecionarPorColaborador();
        
        
        $colab = new mColaborador();
        $colab->setIdColaborador($idColaborador);
        $colab->selecionar();
        
        $tags = array('{ID}','{NOME}','{LINHA}','{SETOR}','{CARGO}');
        
        $valorTag = array(
                      '{ID}'    => $col->getID(),
                      '{NOME}'  => $colab->getNome(),
                      '{LINHA}' => utf8_decode($col->getLinha()),
                      '{SETOR}' => $setor,
                      '{CARGO}' => $col->getPerfil()
                     );
        
        
        foreach($tags as $tag) {
            $texto = str_replace($tag, $valorTag[$tag], $texto);
        }
        
        return $texto;
        
    }
    
    public function downloadPDF($idArquivo) {
        
    }
    
}