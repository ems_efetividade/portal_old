<?php

require_once('lib/appConexao.php');

class mColaborador extends appConexao {
	
        private $idColaborador;
	private $id;
	private $funcao;
	private $nome;
        private $dataInicioSetor;
        private $fone;
        private $email;
	
	
	private $fotoColaborador;
	
	function __construct() {
            $this->id = 0;
            $this->idColaborador = '';
            $this->nome = '';
	}
	
	public function setId($valor) {
		$this->id = $valor;	
	}
	
	public function setIdColaborador($valor) {
		$this->idColaborador = $valor;
	}
	
	
	public function setFuncao($valor) {
		$this->funcao = $valor;
	}
	
	public function setNome($valor) {
		$this->nome = $valor;
	}
	
	
	
	
	
	
	public function getId() {
		return $this->id;	
	}
	
	public function getIdColaborador() {
		return $this->idColaborador;
	}
	
	public function getFuncao() {
		return $this->funcao;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
        public function getDataInicioSetor() {
            return $this->dataInicioSetor;
        }
        
	public function getFoto() {
            return $this->fotoColaborador;
	}
        
        public function getFone() {
            return $this->fone;
        }
        
        public function getEmail() {
            return $this->email;
        }
	
		
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT ID_COLABORADOR, ID, NOME, CARGO, EMAIL FROM colaborador WHERE ID_COLABORADOR = ".$this->idColaborador);

		if(count($rs) > 0) {
		
                    $rsFoto = $this->executarQueryArray("SELECT FOTO FROM FOTO_PERFIL WHERE ID_COLABORADOR = ".$this->idColaborador);
                    $rsFone = $this->executarQueryArray("SELECT * FROM TELEFONE WHERE ID_COLABORADOR = ".$this->idColaborador);

                    $this->idColaborador = $rs[1]['ID_COLABORADOR'];
                    $this->id = $rs[1]['ID'];
                    $this->funcao = $rs[1]['CARGO'];
                    $this->nome = $rs[1]['NOME'];
                    $this->email = $rs[1]['EMAIL'];


                    //dados foto
                    $this->fotoColaborador = $rsFoto[1]['FOTO'];
                    
                    for($i=1;$i<=count($rsFone);$i++) {
                        $this->fone .= '('.$rsFone[$i]['DDD'].') '.$rsFone[$i]['FONE'].'&nbsp;&nbsp;';
                    }
		
		}
	}
        
        public function selecionarDataInicioSetor($idSetor) {
            $rs = $this->executarQueryArray("SELECT DT_INICIO FROM HISTORICO_COLABORADOR_SETOR WHERE ID_COLABORADOR = ".$this->idColaborador." AND ID_SETOR = ".$idSetor);
            
            $this->dataInicioSetor = $rs[1]['DT_INICIO'];
            
        }
        
        public function comboColaborador($setor){
            $query = "SELECT     
                            COLABORADOR.ID_COLABORADOR,
                            COLABORADOR.NOME
                    FROM         
                            HISTORICO_COLABORADOR_SETOR 
                    INNER JOIN
                            SETOR ON HISTORICO_COLABORADOR_SETOR.ID_SETOR = SETOR.ID_SETOR 
                    INNER JOIN
                            COLABORADOR ON HISTORICO_COLABORADOR_SETOR.ID_COLABORADOR = COLABORADOR.ID_COLABORADOR 
                    WHERE
                            SETOR.SETOR = '".$setor."'
                    ORDER BY COLABORADOR.NOME ASC";

            $rs = $this->executarQueryArray($query);

            $option = '<option value="0">:: SELECIONE O COLABORADOR ::</option>';

            foreach($rs as $row) {
                $option .= '<option value="'.$row['ID_COLABORADOR'].'">'.$row['NOME'].'</option>';
            }

            return '<select class="form-control" name="cmbColaborador">'.$option.'</select>';

        
    }
	

	
	
}