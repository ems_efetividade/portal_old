<?php

require_once('lib/appConexao.php');

class mColaboradorSetor extends appConexao {
    
    private $ID;
    private $idSetor;
    private $idColaborador;
    private $nivel;
    private $nivelPerfil;
    private $setor;
    private $colaborador;
    private $perfil;
    private $linha;
    private $foneCorp;
    private $fonePart;
    private $email;
    private $foto;
    
   
    
    public function __construct() {
        $this->ID = 0;
        $this->idSetor = 0;
        $this->idColaborador = 0;
        $this->nivel = 0;
        $this->nivelPerfil = 0;
        $this->setor = '';
        $this->colaborador = '';
        $this->perfil = '';
        $this->linha = '';
        $this->foneCorp = '';
        $this->fonePart = '';
        $this->email = '';
        $this->foto = '';
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }
    
    public function getID() {
        return $this->ID;
    }    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getNivelPerfil() {
        return $this->nivelPerfil;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getColaborador() {
        return $this->colaborador;
    }
    
    public function getPerfil() {
        return $this->perfil;
    }
    
    public function getLinha() {
        return $this->linha;
    }
    
    public function getFoneCorp() {
        return $this->foneCorp;
    }
    
    public function getFonePart() {
        return $this->fonePart;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getFoto() {
        return $this->foto;
    }
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_SETOR, ID_COLABORADOR, ID, NIVEL, NIVEL_PERFIL, SETOR, NOME, PERFIL, LINHA, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, FOTO FROM vw_colaboradorSetor WHERE ID_SETOR = ".$this->idSetor));
    }
    
    public function selecionarPorColaborador() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_SETOR, ID_COLABORADOR, ID, NIVEL, NIVEL_PERFIL, SETOR, NOME, PERFIL, LINHA, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, FOTO FROM vw_colaboradorSetor WHERE ID_COLABORADOR = ".$this->idColaborador));
    }
    
    public function selecionarEquipe() {
        $rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = ".$this->idSetor);
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->ID            = $rs[1]['ID'];
            $this->idSetor       = $rs[1]['ID_SETOR'];
            $this->idColaborador = $rs[1]['ID_COLABORADOR'];
            $this->nivel         = $rs[1]['NIVEL'];
            $this->nivelPerfil   = $rs[1]['NIVEL_PERFIL'];
            $this->setor         = $rs[1]['SETOR'];
            $this->colaborador   = $rs[1]['NOME'];
            $this->perfil        = $rs[1]['PERFIL'];
            $this->linha         = $rs[1]['LINHA'];
            $this->foneCorp      = $rs[1]['FONE_CORPORATIVO'];
            $this->fonePart      = $rs[1]['FONE_PARTICULAR'];
            $this->email         = $rs[1]['EMAIL'];
            $this->foto          = $rs[1]['FOTO'];
        }
    }
    
}