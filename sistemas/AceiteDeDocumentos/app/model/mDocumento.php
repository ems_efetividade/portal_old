<?php
require_once('lib/appConexao.php');
require_once('mDocumentoPermissao.php');

class mDocumento extends appConexao {
    
    private $extPermitidas = ['pdf', 'txt', 'html', 'htm'];
    
    private $idDocumento;
    private $titulo;
    private $texto;
    private $ativo;
    private $nomeArquivo;
    private $extensao;
    private $dataInicio;
    private $dataExpirar;
    
    private $arquivo;
    private $perfil;
    
    public $permissao;
    
    public function __construct() {
        $this->idDocumento = 0;
        $this->titulo = '';
        $this->texto = '';
        $this->ativo = 1;
        $this->extensao = '';
        $this->arquivo = '';
        $this->perfil = '';
        $this->nomeArquivo = '';
        $this->dataInicio = '';
        $this->dataExpirar  = '';
        
        $this->permissao = new mDocumentoPermissao();
        
        $this->criarColuna();
    }
    
    public function setIdDocumento($valor) {
        $this->idDocumento = $valor;
    }
    
    public function setTitulo($valor) {
        $this->titulo = utf8_decode($valor);
    }
    
    public function setTexto($valor) {
        $this->texto = utf8_decode($valor);
    }
    
    public function setAtivo($valor) {
        $this->ativo = $valor;
    }
    
    public function setArquivo($valor) {
        $this->arquivo = $valor;
    }
    
    public function setNomeArquivo($valor) {
        $this->nomeArquivo = utf8_decode($valor);
    }    
    
    public function setPerfil($perfil) {
        $this->perfil = $perfil;
    }
    
    public function setDataInicio($dataInicio) {
        $this->dataInicio = $dataInicio;
    }   
    
    public function setDataExpirar($dataExpirar) {
        $this->dataExpirar = $dataExpirar;
    }     
    
    
    public function getIdDocumento() {
        return $this->idDocumento;
    }
    
    public function getTitulo() {
        return $this->titulo;
    }
    
    public function getTexto() {
        return $this->texto;
    }    
    
    public function getAtivo() {
        return $this->ativo;
    }
    
    public function getNomeArquivo() {
        return $this->nomeArquivo;
    }    
    
    public function getExtensao() {
       return $this->extrairExtensao($this->nomeArquivo);
    }

    public function getDataInicio() {
        return $this->dataInicio;
    }   
    
    public function getDataExpirar() {
        return $this->dataExpirar;
    }      
    
    public function getTempoExpirar() {
        
        
        if(strtotime(date('Y-m-d H:i:s')) <  strtotime($this->dataExpirar)) {
        
        $DataFuturo = $this->dataExpirar;
        $DataAtual = date('Y-m-d H:i:s');

        $date_time  = new DateTime( $DataAtual );
        $diff       = $date_time->diff( new DateTime( $DataFuturo ) );
        //echo $diff->format( '%y year(s), %m month(s), %d day(s), %H hour(s), %i minute(s) and %s second(s)' ); 
        
        return $diff->d.'d '.$diff->h. 'h'.$diff->i.'m';
        } else {
            return 'Expirado';
        }
        // 0 year(s), 0 month(s), 0 day(s), 00 hour(s), 20 minute(s) and 0 second(s)
    }
    
    
    
    public function salvar() {
        
        //verifica se é um novo documento
        if($this->idDocumento == 0) {
            $query = "INSERT INTO DC_DOCUMENTO (TITULO, ARQUIVO, ATIVO, DATA_INICIO, DATA_EXPIRAR) VALUES ('".$this->titulo."', '".$this->nomeArquivo."', ".$this->ativo.", '".$this->dataInicio."', '".$this->dataExpirar."')";
            $query .= ' ; SELECT @@IDENTITY AS ID_DOCUMENTO';
            
            // valida os campos
            $validar = $this->validar();
            
            // ser nao houver erro...
            if(count($validar) <= 0) {
                
                //faz o upload do arquivo
                $moveu = move_uploaded_file($this->arquivo['tmp_name'], PASTA_PDF.utf8_decode($this->arquivo['name']));

                // se o upload deu certo...
                if($moveu) {
                    
                    //executa a query e retorno o ID do documento criado
                    $rs = $this->executarQueryArray($query);                    
                    $idDoc = $rs[1]['ID_DOCUMENTO'];
                    //print_r(sqlsrv_errors());
                } else {
                    return 'houve um erro ao salvar o arquivo - '.$this->arquivo['error'];
                }
            } else {
                return $validar;
                return false;
            }
        
        // se for uma alteração de documento
        } else {
            $query = "UPDATE DC_DOCUMENTO SET TITULO = '".$this->titulo."', ARQUIVO = '".$this->nomeArquivo."', ATIVO = ".$this->ativo.", DATA_INICIO = '".$this->dataInicio."', DATA_EXPIRAR = '".$this->dataExpirar."' WHERE ID_DOCUMENTO = ".$this->idDocumento;
            
            $validar = $this->validar();
            
            if(count($validar) <= 0) {
            
                $d = new mDocumento();
                $d->setIdDocumento($this->idDocumento);
                $d->selecionar();

                if(isset($this->arquivo['tmp_name'])) {
                //if($d->getNomeArquivo() != $this->nomeArquivo) {
                    $moveu = 1;
                    if(isset($this->arquivo['tmp_name'])) {

                        if(file_exists(PASTA_PDF.utf8_decode($d->getNomeArquivo()))) {
                            unlink(PASTA_PDF.utf8_decode($d->getNomeArquivo()));
                        }

                        $moveu = move_uploaded_file($this->arquivo['tmp_name'], PASTA_PDF.utf8_decode($this->arquivo['name']));
                    }
                    if($moveu) {
                        $rs = $this->executarQueryArray($query);
                        $idDoc = $this->idDocumento;
                    } else {
                        return 'houve um erro ao salvar o arquivo - '.$moveu;
                    }
                } else {
                    $rs = $this->executarQueryArray($query);
                    $idDoc = $this->idDocumento;
                }
            } else {
                return $validar;
                return false;
            }
        }
        
        
        if(count($this->perfil) > 0) {
            $this->permissao->setIdDocumento($idDoc);
            $this->permissao->excluir();
            foreach($this->perfil as $arr) {
                $this->permissao->setIdPerfil($arr['ID_PERFIL']);
                $this->permissao->setIdLinha($arr['ID_LINHA']);
                $this->permissao->salvar();
            }
           
        }
        
        
    }
    
    public function excluir() {
        
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS TOTAL FROM DC_DOC_RESPOSTA WHERE ID_DOCUMENTO = ".$this->idDocumento);
        if($rs[1]['TOTAL'] == 0) {
            
            $d = new mDocumento();
            $d->setIdDocumento($this->idDocumento);
            $d->selecionar();
            
            if(file_exists(PASTA_PDF.utf8_decode($d->getNomeArquivo()))) {
                unlink(PASTA_PDF.utf8_decode($d->getNomeArquivo()));
            }
            
            $this->executar("DELETE FROM DC_DOCUMENTO WHERE ID_DOCUMENTO = ".$this->idDocumento);            
            $this->executar("DELETE FROM DC_DOCUMENTO_PERMISSAO WHERE ID_DOCUMENTO = ".$this->idDocumento);            
        } else {
            return 'Não é possível excluir esse documento. Já existem assinaturas para ele.';
        }
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT ID_DOCUMENTO, TITULO,  ATIVO, ARQUIVO, DATA_INICIO, DATA_EXPIRAR FROM DC_DOCUMENTO WHERE ID_DOCUMENTO = ".$this->idDocumento);
        
        if(count($rs)>0){
            $this->idDocumento  = $rs[1]['ID_DOCUMENTO'];
            $this->titulo       = $rs[1]['TITULO'];
            $this->ativo        = $rs[1]['ATIVO'];
            $this->nomeArquivo  = $rs[1]['ARQUIVO'];
            $this->dataInicio   = $rs[1]['DATA_INICIO'];
            $this->dataExpirar  = $rs[1]['DATA_EXPIRAR'];
            
            $this->permissao->setIdDocumento($this->idDocumento);
            
            if($this->nomeArquivo != '') {
                
                $d = explode('.', $rs[1]['ARQUIVO']);
                $extensao = strtolower($d[count($d)-1]);
                
                if($extensao != 'pdf') {
                    if(file_exists(PASTA_PDF . utf8_decode($this->nomeArquivo))) {
                        
                    $encod = mb_detect_encoding(PASTA_PDF.utf8_decode($this->nomeArquivo)); 
                        
                    $file = fopen(PASTA_PDF.utf8_decode($this->nomeArquivo),"r");
                    while(! feof($file)){
                        
                        $linha = fgets($file);
                        if(trim($linha) != "") {
                            
                            $row = ($encod == 'UTF-8') ? $linha : utf8_decode($linha);
                            $this->texto .= "<p>".$row. "</p>";
                        }
                    }
                    fclose($file);
                }
                    
                } else {
                    $this->texto = '';
                }
            }
        }
    }
    
    private function extrairExtensao($arquivo) {
        $dados = explode(".", $arquivo);
        return strtolower($dados[count($dados)-1]);
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("SELECT ID_DOCUMENTO FROM DC_DOCUMENTO ORDER BY ID_DOCUMENTO DESC");
        
        if(count($rs)>0){
            $arr = [];
            foreach($rs as $row) {
                
                $doc = new mDocumento();
                $doc->setIdDocumento($row['ID_DOCUMENTO']);
                $doc->selecionar();
                
                $arr[] = $doc;
            }
            return $arr;
            
        }
    }
    
    public function validar() {
        $erro = [];
        
        if($this->titulo == "") {
            $erro[] = "O Nome do Documento não pode estar vazio.";
        }
        
        if(!isset($this->arquivo['tmp_name']) && $this->nomeArquivo == '') {
            $erro[] = "O arquivo para ser assinado não foi selecionado."; 
        }
        
        if(count($this->perfil) == 0) {
            $erro[] = "Selecionar pelo menos um perfil para assinar o documento.";
        }
        
        if($this->dataInicio == "") {
            $erro[] = "Selecionar a data de Inicio de assinatura do documento.";
        }
        
        if($this->dataExpirar == "") {
            $erro[] = "Selecionar a data de Expiração de assinatura do documento.";
        }     
        
        if(strtotime($this->dataInicio) > strtotime($this->dataExpirar)) {
            $erro[] = "A data de Inicio deve ser menor que a data de Expiração.";
        }    
        
        if(strtotime($this->dataExpirar) < strtotime($this->dataInicio)) {
            $erro[] = "A data de Expiração deve ser maior que a data de Inicio";
        }         
        
        
        if(strtotime($this->dataInicio) == strtotime($this->dataExpirar)) {
            $erro[] = "As datas de Inicio e Expiração devem ser diferentes.";
        }
        
        if(!in_array($this->extrairExtensao($this->nomeArquivo), $this->extPermitidas)) {
            $erro[] = "Extensão não permitida. (Permitido apenas arquivos do tipo <b>".  implode(", ", $this->extPermitidas) . '</b>)';
        }
        
        return $erro;
    }
    
    
    public function listarDocumentosAssinar($idUsuario) {
        $documentos = array();
        $query = "SELECT A.ID_DOCUMENTO FROM (
                    /* SELECIONA DOCS PELO PERFIL */
                    SELECT DC.ID_DOCUMENTO, V.ID_COLABORADOR FROM DC_DOCUMENTO_PERMISSAO D
                    INNER JOIN vw_colaboradorSetor V ON V.ID_PERFIL = D.ID_PERFIL AND D.ID_LINHA = 0
                    INNER JOIN DC_DOCUMENTO DC ON DC.ID_DOCUMENTO = D.ID_DOCUMENTO
                    WHERE DC.ATIVO = 1 AND (GETDATE() BETWEEN DC.DATA_INICIO AND DC.DATA_EXPIRAR) 

                    UNION ALL

                    /* SELECIONA DOCS PELO PERFIL E LINHA */
                    SELECT DC.ID_DOCUMENTO, V.ID_COLABORADOR FROM DC_DOCUMENTO_PERMISSAO D
                    INNER JOIN vw_colaboradorSetor V ON V.ID_PERFIL = D.ID_PERFIL AND D.ID_LINHA = V.ID_LINHA
                    INNER JOIN DC_DOCUMENTO DC ON DC.ID_DOCUMENTO = D.ID_DOCUMENTO
                    WHERE DC.ATIVO = 1 AND (GETDATE() BETWEEN DC.DATA_INICIO AND DC.DATA_EXPIRAR) 

                    UNION ALL

                    /* SELECIONA DOCS PELA LINHA */
                    SELECT DC.ID_DOCUMENTO, V.ID_COLABORADOR FROM DC_DOCUMENTO_PERMISSAO D
                    INNER JOIN vw_colaboradorSetor V ON D.ID_LINHA = V.ID_LINHA AND D.ID_PERFIL = 0
                    INNER JOIN DC_DOCUMENTO DC ON DC.ID_DOCUMENTO = D.ID_DOCUMENTO
                    WHERE DC.ATIVO = 1 AND (GETDATE() BETWEEN DC.DATA_INICIO AND DC.DATA_EXPIRAR)                    

                ) A LEFT JOIN DC_DOC_RESPOSTA R ON R.ID_COLABORADOR = A.ID_COLABORADOR AND 
                R.ID_DOCUMENTO = A.ID_DOCUMENTO
                WHERE R.ID_DOC_RESPOSTA IS NULL AND A.ID_COLABORADOR = ".$idUsuario." GROUP BY A.ID_DOCUMENTO";
                        //echo $query;
        
        $rs = $this->executarQueryArray($query);
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                
                $doc = new mDocumento();
                $doc->setIdDocumento($dado['ID_DOCUMENTO']);
                $doc->selecionar();
                
                $documentos[] = $doc;
                
            }
        }
        
        return $documentos;
    }
    
    public function getTotalRespostas() {
        $query = "SELECT A.ID_DOCUMENTO, A.TOTAL, COUNT(R.ID_DOCUMENTO) AS ASSINADO FROM (
                    SELECT P.ID_DOCUMENTO, COUNT(V.ID_PERFIL) AS TOTAL FROM DC_DOCUMENTO_PERMISSAO P
                    INNER JOIN vw_colaboradorSetor V ON V.ID_PERFIL = P.ID_PERFIL
                    GROUP BY P.ID_DOCUMENTO
                    ) A LEFT JOIN DC_DOC_RESPOSTA R ON R.ID_DOCUMENTO = A.ID_DOCUMENTO
                    WHERE A.ID_DOCUMENTO = ".$this->idDocumento."
                    GROUP BY A.ID_DOCUMENTO, A.TOTAL";
        
        return $this->executarQueryArray($query);
    }
    
    public function listarDocFaltante($idUsuario) {
        $documentos = array();
        //$rs = $this->executarQueryArray("SELECT D.ID_DOCUMENTO FROM (
        //                                        SELECT ID_DOCUMENTO FROM DC_DOC_RESPOSTA WHERE ID_COLABORADOR  = ".$idUsuario." GROUP BY ID_DOCUMENTO
        //                                ) A RIGHT JOIN DC_DOCUMENTO D ON D.ID_DOCUMENTO = A.ID_DOCUMENTO
        //                                WHERE A.ID_DOCUMENTO IS NULL AND D.ATIVO = 1");
        
        $rs = $this->executarQueryArray("WITH DOCUMENTOS AS (
                                        SELECT G.ID_COLABORADOR, G.ID_DOCUMENTO FROM (
                                                SELECT     DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                                FROM         DC_DOCUMENTO_PERMISSAO INNER JOIN
                                                                                          vw_colaboradorSetor ON DC_DOCUMENTO_PERMISSAO.ID_PERFIL = vw_colaboradorSetor.ID_PERFIL INNER JOIN
                                                                                          DC_DOCUMENTO ON DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO = DC_DOCUMENTO.ID_DOCUMENTO
                                                WHERE     (DC_DOCUMENTO.ATIVO = 1)
                                                GROUP BY DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR

                                                UNION ALL
                                                SELECT     DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                                FROM         DC_DOCUMENTO_PERMISSAO INNER JOIN
                                                                                          vw_colaboradorSetor ON DC_DOCUMENTO_PERMISSAO.ID_PERFIL = vw_colaboradorSetor.ID_PERFIL AND 
                                                                                          DC_DOCUMENTO_PERMISSAO.ID_LINHA = vw_colaboradorSetor.ID_LINHA INNER JOIN
                                                                                          DC_DOCUMENTO ON DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO = DC_DOCUMENTO.ID_DOCUMENTO
                                                WHERE     (DC_DOCUMENTO.ATIVO = 1)
                                                GROUP BY DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                                UNION ALL
                                                SELECT     DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                                FROM         DC_DOCUMENTO_PERMISSAO INNER JOIN
                                                                                          vw_colaboradorSetor ON DC_DOCUMENTO_PERMISSAO.ID_LINHA = vw_colaboradorSetor.ID_LINHA
                                                GROUP BY DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR

                                                UNION ALL
                                                SELECT     DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                                FROM         DC_DOCUMENTO_PERMISSAO INNER JOIN
                                                                                          vw_colaboradorSetor ON DC_DOCUMENTO_PERMISSAO.ID_SETOR = vw_colaboradorSetor.ID_SETOR INNER JOIN
                                                                                          DC_DOCUMENTO ON DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO = DC_DOCUMENTO.ID_DOCUMENTO
                                                WHERE     (DC_DOCUMENTO.ATIVO = 1)
                                                GROUP BY DC_DOCUMENTO_PERMISSAO.ID_DOCUMENTO, vw_colaboradorSetor.ID_COLABORADOR
                                        ) G GROUP BY G.ID_COLABORADOR, G.ID_DOCUMENTO )


                                        SELECT D.ID_DOCUMENTO FROM (
                                                SELECT ID_DOCUMENTO FROM DC_DOC_RESPOSTA WHERE ID_COLABORADOR  = ".$idUsuario." GROUP BY ID_DOCUMENTO
                                        ) A RIGHT JOIN DOCUMENTOS D ON D.ID_DOCUMENTO = A.ID_DOCUMENTO WHERE A.ID_DOCUMENTO IS NULL AND
                                        D.ID_COLABORADOR = ".$idUsuario." 
                                        GROUP BY D.ID_DOCUMENTO");
        
       
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                
                $doc = new mDocumento();
                $doc->setIdDocumento($dado['ID_DOCUMENTO']);
                $doc->selecionar();
                
                $documentos[] = $doc;
                
            }
        }
        
        return $documentos;
    }
    
    private function criarColuna() {
        $this->executar("IF NOT EXISTS(
                            SELECT TOP 1 1
                            FROM INFORMATION_SCHEMA.COLUMNS
                            WHERE 
                              [TABLE_NAME] = 'DC_DOCUMENTO'
                              AND [COLUMN_NAME] = 'DATA_INICIO')
                          BEGIN
                            ALTER TABLE [DC_DOCUMENTO] ADD [DATA_INICIO] DATETIME
                            ALTER TABLE [DC_DOCUMENTO] ADD [DATA_EXPIRAR] DATETIME
                          END");
    }
    
}