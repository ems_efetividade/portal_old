<?php

require_once('lib/appConexao.php');

class mDocumentoPermissao extends appConexao {
    
    private $idDocumento;
    private $idPerfil;
    private $idLinha;
    
    public function __construct() {
        $this->idPerfil = 0;
        $this->idLinha = 0;
    }
    
    function setIdDocumento($idDocumento) {
        $this->idDocumento = $idDocumento;
    }

    function setIdPerfil($idPerfil) {
        $this->idPerfil = $idPerfil;
    }
    
    function setIdLinha($idLinha) {
        $this->idLinha = $idLinha;
    }    
    
    function getIdDocumento() {
        return $this->idDocumento;
    }

    function getIdPerfil() {
        return $this->idPerfil;
    }
    
    function getIdLinha() {
        return $this->idLinha;
    }    

    
    public function salvar() {
        $this->executarQueryArray("INSERT INTO DC_DOCUMENTO_PERMISSAO (ID_DOCUMENTO, ID_PERFIL, ID_LINHA) VALUES (".$this->idDocumento.", ".$this->idPerfil.", ".$this->idLinha.")");
        //return "INSERT INTO DC_DOCUMENTO_PERMISSAO (ID_DOCUMENTO, ID_PERFIL, ID_LINHA) VALUES (".$this->idDocumento.", ".$this->idPerfil.", ".$this->idLinha.")<br>";
        
        
        /*
        $this->executar("DELETE FROM DC_DOCUMENTO_PERMISSAO WHERE ID_DOCUMENTO = ".$this->idDocumento);
        
        if(count($this->idPerfil) > 0) {
            foreach($this->idPerfil as $idPerfil) {
                $this->executarQueryArray("INSERT INTO DC_DOCUMENTO_PERMISSAO (ID_DOCUMENTO, ID_PERFIL) VALUES (".$this->idDocumento.", ".$idPerfil.")");
            }    
        }
         */
    }
    
    public function excluir() {
        $this->executar("DELETE FROM DC_DOCUMENTO_PERMISSAO WHERE ID_DOCUMENTO = ".$this->idDocumento);
    }
    
    public function selecionar() {
        $rs =  $this->executarQueryArray("SELECT ID_PERFIL, ID_LINHA FROM DC_DOCUMENTO_PERMISSAO WHERE ID_DOCUMENTO = ".$this->idDocumento); 
        $arr = [];
        if($rs) {
            foreach($rs as $row) {
                $perfil[] = $row[0];
                $linha[]  = $row[1];
                
            }
            $arr['PERFIL'] = $perfil;
            $arr['LINHA'] = $linha;
        }
        return $arr;
        
    }

    
}