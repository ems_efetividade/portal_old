<?php

require_once('lib/appConexao.php');
require_once('mDocumento.php');
require_once('mColaborador.php');

class mDocumentoResposta extends appConexao {
    
    private $idDocResposta;
    private $idDocumento;
    private $idColaborador;
    private $setor;
    private $resposta;
    private $protocolo;
    private $dataCadastro;
    private $arquivo;
    
    public $documento;
    public $colaborador;
    
    public function __construct() {
        $this->idDocResposta = 0;
        $this->idDocumento = 0;
        $this->idColaborador = 0;
        $this->setor = '';
        $this->resposta = '';
        $this->protocolo = '';
        $this->dataCadastro = '';
        $this->arquivo = '';
        
        $this->documento = new mDocumento();
        $this->colaborador = new mColaborador();
    }
    
    public function setIdDocResposta($valor) {
        $this->idDocResposta = $valor;
    }
    
    public function setIdDocumento($valor) {
        $this->idDocumento = $valor;
    }    
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }   
    
    public function setSetor($valor) {
        $this->setor = $valor;
    }    

    public function setResposta($valor) {
        $this->resposta = $valor;
    }
    
    public function setProtocolo($valor) {
        $this->protocolo = $valor;
    }    
    
    public function setArquivo($valor) {
        $this->arquivo = $valor;
    }
    
    
    public function getIdDocResposta() {
        return $this->idDocResposta;
    }
    
    public function getIdDocumento() {
        return $this->idDocumento;
    }    
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }   
    
    public function getSetor() {
        return $this->setor;
    }    

    public function getResposta() {
        return $this->resposta;
    }
    
    public function getProtocolo() {
        return $this->protocolo;
    }    
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }    
    
    public function getArquivo() {
        return $this->arquivo;
    }
    
    public function salvar() {
        
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS T FROM DC_DOC_RESPOSTA WHERE ID_DOCUMENTO = ".$this->idDocumento." AND ID_COLABORADOR = ".$this->idColaborador."");
        
        if($rs[1]['T'] == 0) {
        
        $query = "INSERT INTO [DC_DOC_RESPOSTA]
                        ([ID_DOCUMENTO]
                        ,[ID_COLABORADOR]
                        ,[SETOR]
                        ,[RESPOSTA]
                        ,[PROTOCOLO]
                        ,[DATA_CADASTRO]
                        ,[ARQUIVO])
                  VALUES
                        (".$this->idDocumento."
                        ,".$this->idColaborador."
                        ,'".$this->setor."'
                        ,'".$this->resposta."'
                        ,'".$this->protocolo."'
                        ,'".date('Y-m-d H:i:s')."'
                        ,'".$this->arquivo."')";
                        
        
        $this->executar($query);

        }
        
        $rs = $this->executarQueryArray("SELECT MAX(ID_DOC_RESPOSTA) AS ID FROM DC_DOC_RESPOSTA WHERE ID_COLABORADOR = ".$this->idColaborador);
        return $rs[1]['ID'];
        
    }   
    
    
    public function selecionar() {
        $query = "SELECT * FROM DC_DOC_RESPOSTA WHERE ID_DOC_RESPOSTA = ".$this->idDocResposta;
        
        $rs = $this->executarQueryArray($query);
        
        if(count($rs) > 0) {
            $this->idColaborador = $rs[1]['ID_COLABORADOR'];
            $this->dataCadastro = $rs[1]['DATA_CADASTRO'];
            $this->idDocResposta = $rs[1]['ID_DOC_RESPOSTA'];
            $this->idDocumento = $rs[1]['ID_DOCUMENTO'];
            $this->protocolo = $rs[1]['PROTOCOLO'];
            $this->resposta = $rs[1]['RESPOSTA'];
            $this->setor = $rs[1]['SETOR'];
            $this->arquivo = $rs[1]['ARQUIVO'];
            
            $this->documento =  new mDocumento();
            $this->documento->setIdDocumento($this->idDocumento);
            $this->documento->selecionar();
                    
            $this->colaborador = new mColaborador();
            $this->colaborador->setIdColaborador($this->idColaborador);
            $this->colaborador->selecionar();
        }
    }
    
    public function listar() {
        $respostas = array();
        
        $rs = $this->executarQueryArray("SELECT ID_DOC_RESPOSTA FROM DC_DOC_RESPOSTA WHERE ID_COLABORADOR = ".$this->idColaborador);
        
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                
                $resp = new mDocumentoResposta();
                $resp->setIdDocResposta($dado['ID_DOC_RESPOSTA']);
                $resp->selecionar();
                
                $respostas[] = $resp;
                
            }
        }
        
        return $respostas;
    }
    
    public function excluir() {
        $this->executar("DELETE FROM DC_DOC_RESPOSTA WHERE ID_DOC_RESPOSTA = ".$this->idDocResposta);
    }
    
    public function pesquisar($campo='', $criterio='') {
        /*
        $query = "SELECT 
                        R.ID_DOC_RESPOSTA, 
                        R.SETOR, 
                        C.NOME,
                        D.TITULO AS DOCUMENTO
                FROM 
                    DC_DOC_RESPOSTA R 
                INNER JOIN COLABORADOR C ON C.ID_COLABORADOR = R.ID_COLABORADOR
                INNER JOIN DC_DOCUMENTO D ON D.ID_DOCUMENTO = R.ID_DOCUMENTO
                WHERE 
                    ".$campo." LIKE '%".utf8_decode($criterio)."%' ORDER BY DATA_CADASTRO DESC";
        
        */
        
        
        $where = '';
        $top = '';
        
        if ($criterio ===  '') {
            $top = ' TOP 200 ';
        } else {
            $where = "WHERE ".$campo." LIKE '%".utf8_decode($criterio)."%'";
        }
        
        $query = "SELECT     ".$top."
                    R.ID_DOC_RESPOSTA AS ID_DOCUMENTO, 
                    R.SETOR, 
                    C.NOME, 
                    D.TITULO AS DOCUMENTO, 
                    R.DATA_CADASTRO
                FROM         
                        COLABORADOR C
                INNER JOIN DC_DOC_RESPOSTA R ON C.ID_COLABORADOR = R.ID_COLABORADOR 
                INNER JOIN DC_DOCUMENTO D ON R.ID_DOCUMENTO = D.ID_DOCUMENTO
                ".$where."
                ORDER BY R.DATA_CADASTRO DESC";
        //echo $query;
        
        //echo $query;
        $rs = $this->executarQueryArray($query);
        
        $respostas = array();
        
        
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                /*
                $resp = new mDocumentoResposta();
                $resp->setIdDocResposta($dado['ID_DOC_RESPOSTA']);
                $resp->selecionar();
                */
                $respostas[] = $dado;
                
            }
        }
        
        return $respostas;
        
        /*
        $rs = $this->executarQueryArray($query);
        
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                
                $resp = new mDocumentoResposta();
                $resp->setIdDocResposta($dado['ID_DOC_RESPOSTA']);
                $resp->selecionar();
                
                $respostas[] = $resp;
                
            }
        }
        
        return $respostas;

         */
    }
    
    public function listarTudo() {
        $respostas = array();
        
        $query = "SELECT     TOP 200
                    R.ID_DOC_RESPOSTA AS ID_DOCUMENTO, 
                    R.SETOR, 
                    C.NOME, 
                    D.TITULO AS DOCUMENTO, 
                    R.DATA_CADASTRO
                FROM         
                        COLABORADOR C
                INNER JOIN DC_DOC_RESPOSTA R ON C.ID_COLABORADOR = R.ID_COLABORADOR 
                INNER JOIN DC_DOCUMENTO D ON R.ID_DOCUMENTO = D.ID_DOCUMENTO
                ORDER BY R.DATA_CADASTRO DESC";

        $rs = $this->executarQueryArray($query);
        
        if(count($rs) > 0) {
            foreach($rs as $dado) {
                /*
                $resp = new mDocumentoResposta();
                $resp->setIdDocResposta($dado['ID_DOC_RESPOSTA']);
                $resp->selecionar();
                */
                $respostas[] = $dado;
                
            }
        }
        
        return $respostas;
    }
    
    

}