<?php

require_once('lib/appConexao.php');

class mSetor extends appConexao {
    
    private $idSetor;
    private $idPerfil;
    private $nivel;
    private $setor;
    private $senha;
    private $nome;
    private $idLinha;
    
    public function __construct() {
        $this->idSetor = 0;
        $this->idLinha = 0;
        $this->idPerfil = 0;
        $this->nivel = 0;
        $this->senha = '';
        $this->setor = '';
        $this->nome = '';
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdPerfil($valor) {
        $this->idPerfil = $valor;
    }
    
    public function setIdLinha($valor) {
        $this->idLinha = $valor;
    }
    
    public function setNivel($valor) {
        $this->nivel = $valor;
    }
    
    public function setSenha($valor) {
        $this->senha = $valor;
    }
    
    public function setSetor($valor) {
        $this->setor = $valor;
    }
    
    public function setNome($valor) {
        $this->nome = $valor;
    }
    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdPerfil() {
        return $this->idPerfil;
    }
    
    public function getIdLinha() {
        return $this->idLinha;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getSenha() {
        return $this->senha;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getNome() {
        return $this->nome;
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM SETOR WHERE ID_SETOR = ".$this->idSetor);
        $this->popularVariaveis($rs);
    }
    
    public function selecionarEquipe() {
        $rs = $this->executarQueryArray("SELECT ID_SETOR FROM SETOR WHERE NIVEL = ".$this->idSetor);
        
        if(count($rs) > 0) {
            
            $arrSetor = array();
            foreach($rs as $row) {
                $setor = new mSetor();
                
                $setor->idSetor($row['ID_SETOR']);
                $setor->selecionar();
                $arrSetor[] = $setor;
            }
            return $arrSetor;
        }
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idSetor  = $rs[1]['ID_SETOR'];
            $this->idLinha  = $rs[1]['ID_LINHA'];
            $this->idPerfil = $rs[1]['ID_PERFIL'];
            $this->nivel    = $rs[1]['NIVEL'];
            $this->senha    = $rs[1]['SENHA'];
            $this->setor    = $rs[1]['SETOR'];
            $this->nome     = $rs[1]['NOME'];
        }
    }

}