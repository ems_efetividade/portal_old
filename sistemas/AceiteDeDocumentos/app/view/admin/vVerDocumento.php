<?php require_once('includes.php'); ?>

<style>
    
    .progress { margin-bottom: 0px; height: 15px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            
            <div class="row">
                <div class="col-lg-9">
                    <h2><b><span class="glyphicon glyphicon-file"></span> Visualizar Documento</b> <small> - <?php echo $doc->getTitulo(); ?></small></h2>
                </div>
                <div class="col-lg-3 text-right">
                    <br>
                    <p>
                        <a href="<?php echo appConf::caminho ?>admin" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
                    </p>
                </div>
            </div>
            
            
            
            <hr>
             <div class="panel panel-default">
                <div class="panel-body">
            
            <?php if($doc->getExtensao() == 'pdf') { ?>
                    <embed src="<?php echo appConf::caminho.'public/documento/'.$doc->getNomeArquivo(); ?>" width="100%" height="500" type='application/pdf'>
                <?php } else { ?>
                    
                    <div class="doc-texto">
                        <?php print utf8_encode($doc->getTexto()); ?>
                    </div>
                    
                <?php } ?>
            
            
              </div>
            </div>
        </div>
    </div>
</div>
<script>resizePanelLeft();</script>