<?php include('app/view/header.php');?>

<style>
    
    body {
        padding-top:50px;
    }
    
    .arrow, arrow-small {
        background: url(<?php print appConf::caminho ?>docs/dominio/img/arrow-right.jpg) no-repeat;
        background-size: 30px 30px;
        background-position: left center; 
        padding-left:30px;
    }
    
    .arrow-small {
        background: url(<?php print appConf::caminho ?>docs/dominio/img/arrow-right.jpg) no-repeat;
        background-size: 10px 10px;
        background-position: left center; 
        padding-left:30px;
    }
    
    .bold { font-weight: bold }
    .center { text-align: center }
    .padding { margin-left:50px; }
    .mark { background-color: #ff0 }
    
    
   
   
    .doc-texto {
        misn-height: 325px;
        overflow: auto;
        
    }
    
    .doc-texto p {
        line-height: 170%;
    }

ul {
	list-style-type:none;
} 


li {
        text-align: justify;    
        
    }
li ul {
    
    list-style:none;
}

.list-style {
    list-style:disc;
}
table {
    margin-top:20px;
    margin-bottom:20px;
    width: 100%;
}
    table td {
        border-collapse: collapse;
        padding:8px;
        border:1px solid #ddd;
    }
    
#embedPDF {
-webkit-overflow-scrolling: touch !important;
overflow: scroll !important;
}    
</style>




<div style="margins-top:20px;"></div>



<div class="container">
    
    <div class="row">
        <div class="col-lg-12">

<!--            <h5>
                <div class="alert alert-gray" role="alert"> <strong><span class="glyphicon glyphicon-info-sign"></span> Atenção:</strong> Leia o documento e dê a sua resposta para continuar a acessar o portal. </div>
            </h5>-->
            
            <h2><strong><span class="glyphicon glyphicon-file"></span> <?php print $documento->getTitulo(); ?></strong></h2>
            <h4 class="bg-info" style="padding:10px;"><small class="text-info "><strong>Atenção: </strong>Leia atentamente todo o documento abaixo e dê a sua resposta no final da página. </small></h4>
            <hr>
            

            
            <div class="panel panel-default">
                <div class="panel-body">
                    
                    
                    
                <?php if($documento->getExtensao() == 'pdf') { ?>
<!--                    <p class="text-center"><small>Se você não consegue ver o arquivo completo, <a href="<?php echo appConf::caminho.'home/exibirPDF/'.$documento->getIdDocumento(); ?>" target="_bank">Clique Aqui</a></small></p>-->
                    <embed id="embedPDF" src="<?php echo  appConf::caminho.'home/exibirPDF/'.$documento->getIdDocumento(); ?>" width="100%" height="500" type='application/pdf' style="overflow: scroll">
                    <p class="text-center"><small>Se você não consegue ver o arquivo completo, <a href="<?php echo appConf::caminho.'home/exibirPDF/'.$documento->getIdDocumento(); ?>" target="_bank">Clique Aqui</a></small></p>
                    
                <?php } else { ?>
                    
                    <div class="doc-texto">
                        <?php print utf8_encode($texto); ?>
                    </div>
                    
                <?php } ?>
                </div>
            </div>
          
            
            
        </div>
    </div>
    
    <div class="row" id="buttons">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <button type="button" data-ans="<?php echo base64_encode('N'); ?>" class="btn-block btn-lg btn-danger btn-responder" daata-toggle="modal" daata-target="#modalRespostaa" ><span class="glyphicon glyphicon-thumbs-down"></span> Li e NÃO estou de acordo!</button>            
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
            <button type="button" data-ans="<?php echo base64_encode('S'); ?>" class="btn btn-lg btn-block btn-success btn-responder"><span class="glyphicon glyphicon-thumbs-up"></span> Li e ESTOU de acordo!</button>
        </div>
    </div>
    
    <br>
    
</div>    




<div class="modal fade" id="modalRespostaa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modadl-sm" role="document">
    <div class="modal-content ">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar</h4>
      </div>
      <div class="modal-body">
          <div class="row" >
              <div class="col-lg-6">
                  <div class="text-success" id="S">
                   <div class="panel panel-success">
                <div class="panel-body bg-success">   
                    <p class="text-center"><?php print $titulo; ?></p>
                   <h1 class="text-center"><span class="glyphicon glyphicon-thumbs-up"></span></h1>
                   <h3 class="text-center"><b>Li e ESTOU de acordo </b></h3>
                </div></div>
                  </div>
                <div class="text-danger" id="N">
                    <div class="panel panel-danger">
                <div class="panel-body bg-danger">  
                    <p class="text-center"><?php print $titulo; ?></p>
                   <h1 class="text-center"><span class="glyphicon glyphicon-thumbs-down"></span></h1>
                   <h2 class="text-center"><b>Li e NÃO ESTOU de acordo </b></h2>
                  </div>          
                    </div></div>
              </div>
              <div class="col-lg-6">
                  <h5 class="text-center"><small>Digite o código abaixo</small></h5>
                  <div id="captcha">
                      <p><img src="<?php echo appConf::caminho ?>home/getCaptcha" class="center-block  img-responsive" alt="código captcha"></p>
                        </div>
                  <form id="formResposta" action="<?php echo appConf::caminho ?>home/validarCaptcha" method="post">

                    <div class="row">
                        <div class="col-lg-2"></div>
                        <div class="col-lg-8">
                    <div class="form-group">
                        <input type="hidden" name="txtResposta" value="" />
                        <input type="hidden" name="txtIdDoc" value="<?php print $documento->getIdDocumento() ?>" />
                        
                        <input type="text" class="form-control" name="txtCaptcha" placeholder="">
                        <h5 class="text-center"><small><a href="#" id="btnRecarregarCaptcha">Recarregar imagem</a></small></h5>
                      </div>
                        </div>
                        <div class="col-lg-2"></div>
                    </div>
                    </form>
              </div>
              <div class="col-lg-12">
                  <h5 class="bg-warnisng text-center" style="padding:5px;"><small class="text-gray "><strong>Atenção: </strong>Esta operação não poderá ser cancelada ou alterada. </small></h5>
              </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnConfirmar"><span class="glyphicon glyphicon-ok"></span> Confirmar</button>
      </div>
    </div>
  </div>
</div>
<!--<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />-->
<!--<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />-->

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>-->
<!--<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.js"></script> -->

<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script>
    
    
$(document).ready(function(e) {
    
    redimensionarDiv();
    
    $('.btn-responder').click(function(e) {
        resposta = $(this).attr('data-ans');
        
        $('input[name="txtResposta"]').val(resposta);
        $('#btnRecarregarCaptcha').click();
        $('input[name="txtCaptcha"]').val('');
        
        $('#N').hide();
        $('#S').hide();
        
        
        if(resposta == '<?php echo base64_encode('S'); ?>') {
            $('#S').show();
        } else {
            $('#N').show();
        }
        
        $('#modalRespostaa').modal('show');
        
    });
    
    $('#btnRecarregarCaptcha').click(function(e) {
       img = $('#captcha img');
       
       $('#captcha img').attr('src', img.attr('src')).fadeIn(200);   
       $('input[name="txtCaptcha"]').val('');
       return false;
    });
    
    window.onresize = function(event) {
        redimensionarDiv();
    }
    
    $('#formResposta').keypress(function(e){
        if (e.keyCode == 13) {               
            e.preventDefault();
            return false;
          }
        //$('#btnConfirmar').click();
    })
    
    
    $('#btnConfirmar').click(function(e) {
        
       $.ajax({
            type: "POST",
            url: $('#formResposta').attr('action'),
            data: $('#formResposta').serialize(),

            success: function(e) {

               if(e == "erro") {
                   $('input[name="txtCaptcha"]').val('');
                   $('#btnRecarregarCaptcha').click();
               } else {
                   $('#modalResposta').modal('hide');
                   modalAguarde('show');
                   //alert('<?php print appConf::caminho ?>home/gerarPDF/'+e);
                   window.location = '<?php print appConf::caminho ?>home/gerarArquivoPDF/'+e;
                   //window.location = '<?php print appConf::caminho ?>home/';
                   setTimeout(redirecionar, 5000);
               }
            }
       }) 
    });
    
});    
    
function redimensionarDiv() {
    vph = $(window).innerHeight() - 325;
    $('.doc-texto').css({'height': vph + 'px'});
}    

function modalAguarde(ctrl) {
    $('#modalAguarde').modal(ctrl);

    $('#modalAguarde').on('shown.bs.modal', function (e) {
      $('body').css('padding', '0px');
    });

    $('#modalAguarde').on('hidden.bs.modal', function (e) {
      $('body').css('padding', '0px');
    })
}

/*

$(document).ready(function(e) {
    
    redimensionarDiv();
    
    /*
    $('.btn-responder').click(function(e) {
        resposta = $(this).attr('data-ans');
        
        $('input[name="txtResposta"]').val(resposta);
        $('#btnRecarregarCaptcha').click();
        $('input[name="txtCaptcha"]').val('');
        
        $('#N').hide();
        $('#S').hide();
        
        /*
        if(resposta == '<?php echo base64_encode('S'); ?>') {
            $('#S').show();
        } else {
            $('#N').show();
        }
        
        $('#modalResposta').modal('show');
        
    });
    
    
    
    $('#btnRecarregarCaptcha').click(function(e) {
       img = $('#captcha img');
       
       $('#captcha img').attr('src', img.attr('src')).fadeIn(200);   
       $('input[name="txtCaptcha"]').val('');
       return false;
    });
    
    window.onresize = function(event) {
        redimensionarDiv();
    }
    
    $('#formResposta').keypress(function(e){
        if (e.keyCode == 13) {               
            e.preventDefault();
            return false;
          }
        //$('#btnConfirmar').click();
    })
    
    
    $('#btnConfirmar').click(function(e) {
        
       $.ajax({
            type: "POST",
            url: $('#formResposta').attr('action'),
            data: $('#formResposta').serialize(),

            success: function(e) {
               if(e == "erro") {
                   $('input[name="txtCaptcha"]').val('');
                   $('#btnRecarregarCaptcha').click();
               } else {
                   $('#modalResposta').modal('hide');
                   modalAguarde('show');
                   //alert('<?php print appConf::caminho ?>home/gerarPDF/'+e);
                   window.location = '<?php print appConf::caminho ?>home/gerarPDF/'+e;
                   setTimeout(redirecionar, 5000);
               }
            }
       }) 
    });
    
    
    $('#modalRespostaa').on('shown.bs.modal', function (event) {
        alert('f');
    });
    
    //$('#modalResposta').on('show.bs.modal', function (event) {
        //alert('f');
        //var button = $(event.relatedTarget); // Button that triggered the modal
       /// var recipient = button.data('ans'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      
        
         //alert('recipient');
        
        
        
        //alert('recipient: '+recipient + ' / ' + '<?php echo base64_encode('S'); ?>');
        
        //if(recipient == '<?php echo base64_encode('S'); ?>') {
       //     $('#S').show();
       // } else {
       //    $('#N').show();
       // }
        
        
      //});
      
      
    
});

*/

function redirecionar() {
    window.location = '<?php print appConf::caminho ?>';
}


</script>