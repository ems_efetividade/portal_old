<style>
    
    .arrow, arrow-small {
        background: url(<?php echo ROOT ?>public/img/arrow-right.jpg) no-repeat;
        background-size: 50px 50px;
        background-position: left center; 
        padding-left:30px;
    }
    
    .arrow-small {
        background: url(<?php echo ROOT ?>public/img/arrow-right.jpg) no-repeat;
        background-size: 30px 30px;
        background-position: left center; 
        padding-left:30px;
    }
    
    .bold { font-weight: bold }
    .h2 { font-size:20px; }
    .h3 { font-size:17px; }
    .h5 { font-size:15px; }
    .center { text-align: center }
    .padding { margin-left:50px; }
    .mark { background-color: #ff0 }
    
    p {
        margin:0px;
        margin-bottom: 12px;
        font-size: 12px;
    }
    
    li {
        
        margin: 15px;
    }
    
    body {
        font-family: arial; 
        font-size: 12px;
    }

    .resposta {
        width: 100%;
        padding:20px;
        text-align: center;
        font-size: 20px;
        font-weight: bold;
    }
    
    .resposta-S {
        background-color: #dff0d8;
        color: #3c763d;
        border:1px solid #3c763d;
    }
    
    .resposta-N {
        background-color: #f2dede;
        color:#a94442;
        border:1px solid #a94442;
    }
    
    .titulo {
        color: #555;
        width: 95%;
    } 
    .logo {
        text-align: right;
        width: 5%;
    } 
    
    /*
    
    
    h3 {
        margin:0px;
    }
    
    body {
        font-family: arial;
        font-size: 13px;
        
    }
    
    p, ol {
        margin:0px;
        margin-bottom: 5px;
        
    }

       
    
    
    ul {
	list-style-type:none;
        font-size:13px;
    } 
    
    li {
       line-height: 390%;
       padding-bottom: 5px;
    }
    
    div {
        /*margin-bottom:30px;
    }
    
    li ul {
        list-style:none;
    }
    .list-style {
        list-style:disc;
    }
    
    ismg {
        max-height: 200px;
    }
    
    .tbl {
        margin-top:20px;
        margin-bottom:20px;
        width: 100%;
        border-collapse: collapse;
        font-size:13px;
    }
    .tbl td {
       
        padding:5px;
        border:1px solid #ddd;
    }
    
    */
</style>

<table>
    <tr>
        <td class="titulo"><h2><strong> <?php print utf8_decode($titulo); ?></strong></h2></td>
        <td class="logo"><img src="../../public/img/ems.png" /></td>
    </tr>
</table>

<hr />

<?php print $texto; ?>

<br>

<HR>
<br>
<br>

<div class="resposta resposta-<?php print $resposta ?>">
    <?php print $textoResposta; ?>
</div>

<p style="text-align:center; font-size:12px"><b>Data: </b> <?php print $data; ?>
    &nbsp;&nbsp;&nbsp;&nbsp;<b>Protocolo: </b> <?php print $protocolo; ?>
&nbsp;&nbsp;&nbsp;&nbsp;<b>Setor: </b> {SETOR}
&nbsp;&nbsp;&nbsp;&nbsp;<b>Colaborador: </b> {NOME}</p>
