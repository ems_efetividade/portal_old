Eu, {NOME}, ID {ID} alocado na linha {LINHA}, setor {SETOR}, sob o cargo de {CARGO} declaro que participei da <strong>CONVENÇÃO 2016 - EMS PRESCRIÇÃO</strong>, no período de <strong>18/01/2016</strong> a <strong>22/01/2016</strong>.           

Informo ainda que retornei antes do encerramento das minhas férias, prevista para o período de <b>21/12/2015</b> a <b>21/01/2016</b>, e estou de acordo em compensar estes dias, na semana do Carnaval 2016, conforme descrito abaixo: 
<br>                     
18/01/2016 - Compensar em 08/02/2016 (segunda-feira antes do Carnaval             
19/01/2016 - Compensar em 10/02/2016 (quarta-feira de cinzas)  
20/01/2016 - Compensar em 11/02/2016 (quinta-feira)              
21/01/2016 - Compensar em 12/02/2016 (sexta-feira)              
<br>          
                           
Se por motivo do meu deslocamento, foi necessário viajar no dia 17/01/2016, este dia foi compensado em 25/01/2016 (segunda-feira).                         
Desta forma, me comprometo a cumprir o regulamento acima, previamente validado pela EMS Prescrição, Gestão de Capital Humano e Jurídico.