<?php


/* CONFIGURAÇÕES DO PROJETO */
require_once('..\\..\\lib\\appGlobalVar.php');
//require_once('appSanitize.php');


define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);
define('SERVER_URL', $_SERVER['HTTP_HOST']);
define("APP_FOLDER", '/sistemas/AdminGeral/');
define("APP_NAME",   "Administração Portal EMS");

define('APP_URL', APP_HOST.APP_FOLDER);
define('APP_CSS', APP_URL.'public/css/');
define('APP_IMG', APP_URL.'public/img/');
define('APP_JS',  APP_URL.'public/js/');

require_once HELPER_PATH . 'Functions.php';

?>