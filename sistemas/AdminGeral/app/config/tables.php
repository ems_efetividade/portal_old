<?php
$cn = new appConexao();
$rsTabela = $cn->executarQueryArray("SELECT T.*, S.create_date AS DATA_CREATE, S.modify_date AS DATA_MOD FROM INFORMATION_SCHEMA.TABLES T
INNER JOIN sys.tables S ON S.name = T.TABLE_NAME
WHERE T.TABLE_TYPE = 'BASE TABLE' ORDER BY T.TABLE_NAME ASC");

$tabelas = [];
foreach($rsTabela as $tabela) {
    $arr['NOME']   = $tabela['TABLE_NAME'];
    $arr['CREATE'] = $tabela['DATA_CREATE'];
    $arr['MOD']    = $tabela['DATA_MOD'];
    
    $rsColuna = $cn->executarQueryArray("SELECT C.TABLE_NAME, C.COLUMN_NAME, C.DATA_TYPE, C.IS_NULLABLE
                                    from INFORMATION_SCHEMA.COLUMNS C
                                    WHERE C.TABLE_NAME = '".$arr['NOME']."'
                                    ");

    
    $arrCol = [];
    foreach($rsColuna as $x => $col) {
        $a['NOME'] = $col['COLUMN_NAME'];
        $a['TIPO'] = $col['DATA_TYPE'];
        $a['ATTR'] = ($x == 1) ? 'PK' : 'NULL';
        
        $arrCol[] = $a;
    }
    $arr['COLUNAS'] = $arrCol;
    
    $tabelas[] = $arr;
}
/*
echo '<pre>';
print_r($tabelas);
echo '</pre>';
*/




//
//$tabelas = array(
//    /* TABELA PERFIL */
//    array(
//            'NOME' => 'PERFIL',
//            'COLUNAS' => array(
//                array('NOME' => 'ID_PERFIL', 'TIPO' => 'INTEGER'      , 'ATTR' => 'PK'),
//                array('NOME' => 'PERFIL',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'NIVEL',    'TIPO' => 'INT' , 'ATTR' => 'NULL'),
//                array('NOME' => 'ADMIN',    'TIPO' => 'INT' , 'ATTR' => 'NULL')
//            )
//
//    ),
//    
//    /* TABELA FUNÇÃO */
//    array(
//            'NOME' => 'FUNCAO',
//            'COLUNAS' => array(
//                array('NOME' => 'ID_FUNCAO', 'TIPO' => 'INTEGER'      , 'ATTR' => 'PK'),
//                array('NOME' => 'ID_PERFIL', 'TIPO' => 'INTEGER' , 'ATTR' => 'NULL'),
//                array('NOME' => 'FUNCAO',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL')
//            )
//
//    ),
//    
//    /* TABELA LINHA */
//    array(
//            'NOME' => 'LINHA',
//            'COLUNAS' => array(
//                array('NOME' => 'ID_LINHA', 'TIPO' => 'INTEGER'      , 'ATTR' => 'PK'),
//                array('NOME' => 'NOME',     'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'SIGLA',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'CODIGO',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL')
//            )
//
//    ),
//    
//    /* TABELA LINHA */
//    array(
//            'NOME' => 'SETOR',
//            'COLUNAS' => array(
//                array('NOME' => 'ID_SETOR', 'TIPO' => 'INTEGER'      , 'ATTR' => 'PK'),
//                array('NOME' => 'ID_LINHA',     'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'ID_PERFIL',     'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'NIVEL',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'SETOR',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'SENHA',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL'),
//                array('NOME' => 'NOME',    'TIPO' => 'VARCHAR(200)' , 'ATTR' => 'NULL')
//            )
//
//    )
//);
