<?php



class cMain extends Controller {
    
    private $db;
    
    public function __construct() {

        if (!isset($_SESSION['id_setor'])) {
            header('Location: '.EMS_URL);
            exit();
        }

        parent::__construct();
        $this->db = new mDatabase;
    }
    
    public function main() {
        
        $tables     = $this->db->getStructure();
        $procedures = $this->db->getProcedures('P');
        $views = $this->db->getProcedures('V');
        
        $panel = $this->renderToString('main/vPanelSQL');
        
        $this->set('panel', $panel);
        $this->set('tables', $tables);
        $this->set('procedures', $procedures);
        $this->set('views', $views);
        $this->render('main/vMain');
    }
    
    public function open($tableName) {
        $columns = $this->db->getFields($tableName);
        $data    = $this->db->openTable($tableName);
        
        echo $this->getTable($columns, $data);
    }
    
    public function execute() {
        $inicio = microtime(true); 
        $query = $_POST['query'];

        $rs = $this->db->execute($query);
        
        
        $erro = $this->parseError();
        
        if($erro != "") {
            echo $erro;
            exit();
        }
        
        if($rs) {
            $colunas = array_keys($rs[1]);
            $arrCol = [];
            foreach ($colunas as $col) {
                if (!is_numeric($col)) {
                    $arrCol[] = $col;
                }
            }
        }
        
        echo $this->getTable($arrCol, $rs, $inicio);

    }
    
    private function parseError() {
        if (sqlsrv_errors()) {
            $errors = sqlsrv_errors();
            
            $html = '';
            foreach($errors as $erro) {
                $html .= '<div class="alert alert-danger"><p> <b>Erro '.$erro['code'].': </b> '.str_replace('[Microsoft][ODBC Driver 11 for SQL Server][SQL Server]', '', $erro['message']).'</p></div>';
            }
           
            return $html;
        }
    }
    
    public function getPanelSQL() {
        echo $this->renderToString('main/vPanelSQL');
    }
    
    public function getProcCode() {
        $proc = $_POST['proc'];
        echo $this->db->getProcedureCode($proc);
    }
    
    private function getTable($columns, $data, $inicio) {
        if (count($data) > 0) {
            /*
            $html = '';
            $html .= '<table style="font-size:12px" class="table table-bordered table-condensed table-striped">';
            $html .= '<tr><th class="text-center"><a href=#" class="btn-export"><span class="glyphicon glyphicon-download"></span></a></th>';

            foreach($columns as $col) {
                $html .= '<th><small>'.$col.'</small></th>';
            }
            $html .= '</tr>';

                foreach($data as $i => $row) {
                    $html .= '<tr>';
                    $html .= '<td style="background:#f9f9f9" class="text-center"><small><small>'.$i.'</small></small></td>';
                    foreach($columns as $col) {
                        $html .= '<td style="white-space:nowrap;"><small>'.$row[$col].'</small></td>';
                    }
                    $html .= '</tr>';
                }

            $html .= '</html>';
             */
            
            $this->set('data', $data);
            $this->set('columns', $columns);
            $this->set('inicio', $inicio);
            $html = $this->renderToString('main/vTable');
            
            
        } else {
            echo '<h5 class="text-center text-muted">Nenhum registro encontrado</h5>';
        }
        
        return $html;
    }
    
    public function export($a) {
        $query = $_POST['query'];
        $rs = $this->db->execute($query);
        
        if($rs) {
            
            $colunas = array_keys($rs[1]);
            $arrCol = [];
            foreach ($colunas as $col) {
                if (!is_numeric($col)) {
                    $arrCol[] = $col;
                }
            }
            
            
            $nomeArquivo = 'SQLQuEry_'.date('YmdHis').'.csv';
            $df = fopen(PUBLIC_PATH .$nomeArquivo, 'w');

            fputcsv($df, $arrCol, ';');
            foreach ($rs as $i => $row) {

                $a = [];
                foreach ($arrCol as $col) {
                    $a[$col] = utf8_decode($row[$col]);
                }

              fputcsv($df, $a, ';');
            }
            fclose($df);
            echo $nomeArquivo;
            
        }
        
    }
    
    public function download($file) {
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$file.'"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize(PUBLIC_PATH .$file));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile(PUBLIC_PATH .$file);

        unlink(PUBLIC_PATH .$file);
    }
    
    
}