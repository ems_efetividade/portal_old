<?php

class cTabela extends Controller {
    
    private $tabela;
    private $db;
    
    public function __construct() {
        parent::__construct();
        $this->tabela = new mTabela();
        $this->db = new mDatabase;
    }
    
    
    public function main() {
        //$this->set('tabelas', $this->tabela->listar());
        //$this->render('main/tabela/vListaTabela');
        
        $tables = $this->db->getStructure();
        $html = '';
        foreach($tables as $table) {
            $html .= '<li>'.$table['NAME'].''; 
            $html .= '<ul>';
            
            foreach($table['COLUMNS'] as $column) {
                $html .= '<li>'.$column['NAME'].'</li>';
            }
            $html .= '</ul></li>';
        }  
        
        $this->set('estrutura', $html);
        $this->render('main/tabela/vListaTabela');
        
       
    }
    
    public function mostrar($idTabela, $pagina='') {
        $this->tabela->setId($idTabela);
        $this->tabela->selecionar();
        
        
        $this->set('paginacao', $this->tabela->paginacao($pagina));
        $this->set('tabela', $this->tabela);
        $this->render('main/tabela/vDadosTabela');
    }
    
    public function form($idTabela, $idPK) {
        $this->tabela->setId($idTabela);
        $this->tabela->selecionar();
        
        $this->set('tabela', $this->tabela);
        $this->set('dados', $this->tabela->getRow($idPK));
        $this->render('main/tabela/vFormTabela');
    }
    
    public function salvar() {

        $idTabela = $_POST['TABLE'];
        
        $this->tabela->setId($idTabela);
        $this->tabela->selecionar();
        echo $this->tabela->execute($_POST);
        
        $this->redirect(APP_URL.'tabela/mostrar/'.$idTabela);
    }
    
    public function excluir($idTabela, $idPK) {
        $this->tabela->setId($idTabela);
        $this->tabela->selecionar();
        $this->tabela->delete($idPK);
        
        $this->redirect(APP_URL.'tabela/mostrar/'.$idTabela);
    }
    
}