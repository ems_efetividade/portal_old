<?php

class mCalendario {
    
    private $dataInicio;
    private $dataFinal;
    private $nomeMes;
    
    public function __construct($dataInicio, $dataFinal) {
        $this->dataInicio = $dataInicio;
        $this->dataFinal  = $dataFinal;
        
        $this->nomeMes[1]  = "Janeiro";
        $this->nomeMes[2]  = "Fevereiro";
        $this->nomeMes[3]  = "Março";
        $this->nomeMes[4]  = "Abril";
        $this->nomeMes[5]  = "Maio";
        $this->nomeMes[6]  = "Junho";
        $this->nomeMes[7]  = "Julho";
        $this->nomeMes[8]  = "Agosto";
        $this->nomeMes[9]  = "Setembro";
        $this->nomeMes[10] = "Outubro";
        $this->nomeMes[11] = "Novembro";
        $this->nomeMes[12] = "Dezembro";
    }

    public function gerarCalendario() {
        /*
        $dia = 1;
        $h='<tr>';
        while(checkdate($this->mes, $dia, $this->ano)) {
            $semana = getdate(mktime( 0, 0, 0, $this->mes, $dia, $this->ano));
            
            $h .= $this->ano.'-'.$this->mes.'-'.$dia.' - '.$semana['wday'].'<br>';
            $dia++;
        }
        return $h;
         */
        
        //$de = '2017-03-15';
        //$ate = '2017-04-22';
        
        echo date('m', strtotime($this->dataInicio));
        
        while(strtotime($this->dataInicio) <= strtotime($this->dataFinal)) {
            //$semana = getdate(mktime( 0, 0, 0, $this->mes, $dia, $this->ano));
            echo 'd - '.$this->dataInicio.'<br>';
            $this->dataInicio = date('Y-m-d', strtotime($this->dataInicio. ' +1 day'));
        }
    }
    
    
    
    
    
    
}