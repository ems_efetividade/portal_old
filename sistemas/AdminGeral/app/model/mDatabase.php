<?php

class mDatabase extends Model {
    
    public function getStructure() {
        
        $rs = $this->getTables();
        $arrTable = array();
        
        foreach($rs as $row) {
            $arr = array();
            $arr['NAME'] = $row['TABLE_NAME'];

            
            $rsFields = $this->getFields($row['TABLE_NAME']);
            
            $colunas = array_keys($rsFields[1]);
            $arrCol = [];
            foreach ($colunas as $col) {
                if (!is_numeric($col)) {
                    $arrCol[] = $col;
                }
            }
            
            
            foreach($rsFields as $field) {
                $arrField = array();
                
                foreach($arrCol as $col) {
                    $arrField[$col] = $field[$col];
                }
                $arr['COLUMNS'][] = $arrField;
            }
            $arrTable[] = $arr;
        }
        
        return $arrTable;
    }
    
    public function getTables() {
        $rs = $this->cn->executarQueryArray("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' ORDER BY TABLE_NAME");
        return $rs;
    }
    
    public function getFields($tableName) {
        $rs = $this->cn->executarQueryArray("SELECT
                                                    A.COLUMN_NAME,
                                                    A.DATA_TYPE,
                                                    A.IS_NULLABLE,
                                                    A.CHARACTER_MAXIMUM_LENGTH,
                                                    A.NUMERIC_PRECISION,
                                                    A.ORDINAL_POSITION,
                                                    CASE WHEN (C.COLUMN_NAME = A.COLUMN_NAME) THEN 1 ELSE 0 END AS PK
                                            FROM INFORMATION_SCHEMA.COLUMNS A
                                            LEFT JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS B ON A.TABLE_NAME = B.TABLE_NAME
                                            LEFT JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE C ON
                                            C.CONSTRAINT_NAME = B.CONSTRAINT_NAME AND
                                            C.TABLE_NAME = B.TABLE_NAME AND
                                            B.CONSTRAINT_TYPE = 'PRIMARY KEY'
                                            WHERE A.TABLE_NAME = '".$tableName."'");
        return $rs;
    }
    
    public function getProcedures($p='V') {
        $rs = $this->cn->executarQueryArray("SELECT NAME
                                                FROM dbo.sysobjects
                                               WHERE (type = '".$p."')
                                               order by name asc");
        
        return $rs;
    }
    
    public function getProcedureCode($proc) {
        $rs = $this->cn->executarQueryArray("SELECT OBJECT_DEFINITION (OBJECT_ID('".$proc."')) AS CODE");
        return $rs[1]['CODE'];
    }
    
    public function openTable($tableName) {
        return $this->cn->executarQueryArray("SELECT TOP 1000 * FROM [".$tableName."]");
    }
    
    public function numReg($tableName) {
        $rs = $this->cn->executarQueryArray("SELECT COUNT(*) AS C FROM [".$tableName."]");
        return $rs[1]['C'];
    }
    
    public function execute($query) {
        //return $this->cn->executarQueryArray($query);
		return $this->cn->executarQueryArray(utf8_decode($query));
        
    }
    
}