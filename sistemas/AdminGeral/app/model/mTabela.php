<?php

class mTabela extends Model {
    
    private $id;
    private $nome;
    private $dataCriacao;
    private $dataModificado;
    
    
    private $colunas;
    
    private $tabelas;
    
    public function __construct() {
        parent::__construct();
        
        require_once (CONFIG_PATH.'tables.php');
              
        
        $this->nome = '';
        $this->colunas = [];
        
        $this->tabelas = $tabelas;
    }
    function getDataCriacao() {
        return $this->dataCriacao;
    }

    function getDataModificado() {
        return $this->dataModificado;
    }

        
    function getId() {
        return $this->id;
    }
    
    function getNome() {
        return $this->nome;
    }

    function getColunas() {
        return $this->colunas[0];
    }

    function setId($id) {
        $this->id = $id;
    }
    
    function setNome($nome) {
        $this->nome = $nome;
    }
    function setDataCriacao($dataCriacao) {
        $this->dataCriacao = $dataCriacao;
    }

    function setDataModificado($dataModificado) {
        $this->dataModificado = $dataModificado;
    }

    function setColunas($colunas) {
        $this->colunas[] = $colunas;
    }

    public function selecionar() {

        $this->nome           = $this->tabelas[$this->id]['NOME'];
        $this->dataCriacao    = $this->tabelas[$this->id]['CREATE'];
        $this->dataModificado = $this->tabelas[$this->id]['MOD'];
        
        $this->colunas[] = $this->tabelas[$this->id]['COLUNAS'];
    }
    
    
    public function getCount() {
        $query = "SELECT COUNT(*) AS TOTAL FROM [".$this->nome."]";
        $rs = $this->cn->executarQueryArray($query);
        return $rs[1]['TOTAL'];
    }
    
    public function getData($pag='') {
        $query = $this->getSelect($pag);
        return $this->cn->executarQueryArray($query);
    }
    
    public function getRow($id) {
        $query = $this->getSelectRow($id);
        return $this->cn->executarQueryArray($query);
    }
    
    public function execute($data) {
        if($data['PK'] == 0) {
            $query = $this->getInsert($data);
        } else {
            $query = $this->getUpdate($data);
        }
        //echo $query;
        $this->cn->executarQueryArray($query);
    }
    
    public function delete($id) {
        $this->cn->executarQueryArray($this->getDelete($id));
    }
    
    public function getInsert($data) {      
        $colunas = $this->tabelas[$this->id]['COLUNAS'];
        $sql = 'INSERT INTO ['.$this->nome.'] ';

        foreach($colunas as $coluna) {
            
            if($coluna['ATTR'] != 'PK') {
                $arrCol[] = $coluna['NOME'];
                $arrVal[] = "'".utf8_decode(trim($data[$coluna['NOME']]))."'";
            }
        }
        
        $cols = '( '.implode(', ', $arrCol) .') VALUES ';
        $vals = '( '.implode(', ', $arrVal) .');';
        
        $sql .= $cols.$vals;
        
        return $sql;
    }
    
    public function getUpdate($data) {      
        $colunas = $this->tabelas[$this->id]['COLUNAS'];
        $sql = 'UPDATE ['.$this->nome.'] SET ';

        foreach($colunas as $coluna) {
            
            if($coluna['ATTR'] == 'PK') {
                $pk = $coluna['NOME'].' = '.$data['PK'];
            } else {
                $arrVal[] = $coluna['NOME']. " = '".  utf8_decode(trim($data[$coluna['NOME']]))."'";
            }
        }
        
        $vals = ''.implode(', ', $arrVal) .' WHERE '.$pk.';';
        
        $sql .= $vals;
        
        return $sql;
    }

    public function getDelete($id) {
        
        $colunas = $this->tabelas[$this->id]['COLUNAS'];
        foreach($colunas as $coluna) {
            
            if($coluna['ATTR'] == 'PK') {
                $col = $coluna['NOME'] . ' = ' . $id;
            }
        }
        
        return 'DELETE FROM ['.$this->nome.'] WHERE '.$col;
    }
    
    public function getSelect($pag='') {
        $colunas = $this->tabelas[$this->id]['COLUNAS'];
        $over = '';
        foreach($colunas as $i => $coluna) {
            $arrCol[] = $coluna['NOME'];
            $arrVal[] = "''";
            
            if($over == '') {
                $over = ($i == 0) ? $coluna['NOME'] : '';
            }
        }
        
        $paginacao = $this->paginacao($pag);
        
        //if ($pag == '') {
            $limit = ' WHERE A.INDICE BETWEEN '.$paginacao['INICIO'].' AND '.$paginacao['FIM'].';';
       // }
            
            //echo 'SELECT A.* FROM (SELECT ROW_NUMBER() OVER (ORDER BY '.$over.' ASC) AS INDICE, '.implode(', ', $arrCol).' FROM ['.$this->nome.']) A '.$limit;
       
        return 'SELECT A.* FROM (SELECT ROW_NUMBER() OVER (ORDER BY '.$over.' ASC) AS INDICE, '.implode(', ', $arrCol).' FROM ['.$this->nome.']) A '.$limit;
        
    }
    
    public function getSelectRow($id) {
        $colunas = $this->tabelas[$this->id]['COLUNAS'];

        foreach($colunas as $coluna) {
            if($coluna['ATTR'] == 'PK') {
                $key = ' WHERE '.$coluna['NOME'].' = '.$id;
            }
            
            $arrCol[] = $coluna['NOME'];
        }
        
        return 'SELECT '.implode(', ', $arrCol).' FROM ['.$this->nome.'] '.$key;
    }    
    
    public function listar() {
        
        
        $arr = [];
        foreach($this->tabelas as $i => $tabela) {
           
           $t = new mTabela();
           $t->setId($i);
           $t->setNome($tabela['NOME']);
           $t->setDataCriacao($tabela['CREATE']);
           $t->setDataModificado($tabela['MOD']);
           $t->setColunas($tabela['COLUNAS'][0]);
            
           
           
           $arr[] = $t;
        }
        
        return $arr;
        
    }
    
    public function paginacao($pag='') {
        $max = 20;
        $pagina = ($pag == '') ? 1 : $pag;
        $total = $this->getCount();
        
        $paginas = ceil(($total / $max));
        
        $inicio = (($max * $pagina) - $max);
        $fim = $inicio + $max;
        
        $arr['INICIO'] = $inicio;
        $arr['FIM'] = $fim;
        $arr['PAGINA'] = $pagina;
        $arr['PAGINAS'] = $paginas;
        
        return $arr;
        
        
    }
    
}

