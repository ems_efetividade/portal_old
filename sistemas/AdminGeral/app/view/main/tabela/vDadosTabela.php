<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    
    .progress { margin-bottom: 0px; height: 15px;}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH.'main/vMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            
            <h3 class="lead">
                Gerenciamento de tabelas - <?php echo $tabela->getNome(); ?> <small>Dados da tabela</small>
            </h3>
            <hr>
            
            <p>
                <a href="<?php echo APP_URL ?>tabela/form/<?php echo $tabela->getId(); ?>/0" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> Adicionar Registro
                </a>
                
            </p>
            
            <small>
            <table class="table table-condensed table-striped">
                <?php
                    $colunas = $tabela->getColunas();
                    $dados = $tabela->getData($paginacao['PAGINA']);
                ?>
                <tr>
                    <th>INDICE</th>
                    <?php foreach($colunas as $coluna) { ?>
                    <th><?php echo $coluna['NOME'] ?></th>
                    <?php } ?>
                    <th>Ação</th>
                </tr>
                
                <?php
                    
                    
                    foreach($dados as $i => $dado) {
                        $pk = '';
                ?>
                <tr>
                    <td><?php echo fnFormatarMoedaBRL(($dado['INDICE']),0) ?></td>
                    <?php 
                        foreach($colunas as  $coluna) { 
                            if($pk == '') {
                               $pk = ($coluna['ATTR'] == 'PK') ? $dado[$coluna['NOME']] : 0;
                            }
                    ?>
                    
                    <td><?php echo $dado[$coluna['NOME']] ?></td>
                    <?php } ?>
                    <td>
                        <a href="<?php echo APP_URL ?>tabela/form/<?php echo $tabela->getId(); ?>/<?php echo $pk; ?>">Editar </a>
                        <a href="<?php echo APP_URL ?>tabela/excluir/<?php echo $tabela->getId(); ?>/<?php echo $pk; ?>">Excluir </a>
                    </td>
                </tr>
                
                    <?php } ?>
                
            </table>
            </small>
            
            <?php for($i=1;$i<=$paginacao['PAGINAS'];$i++) { ?>
            <a href="<?php echo APP_URL ?>tabela/mostrar/<?php echo $tabela->getId(); ?>/<?php echo ($i); ?>"><?php echo ($i) ?></a>
            <?php } ?>
        </div>
    </div>
</div>

<script>
    resizePanelLeft();
</script>