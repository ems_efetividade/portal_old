<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    
    .progress { margin-bottom: 0px; height: 15px;}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH.'main/vMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            
            <h3 class="lead">
                Gerenciamento de tabelas <small><?php echo $tabela->getNome(); ?></small>
            </h3>
            <hr>
            
            
            <form action="<?php echo APP_URL ?>tabela/salvar" method="POST">
                <input type="hidden" class="form-control" name="TABLE" value="<?php echo $tabela->getId() ?>">
                <div class="row">
            <?php
                $colunas = $tabela->getColunas();
                
               
                    
                    foreach($colunas as $coluna) {
                       if($coluna['ATTR'] != 'PK') {
            ?>
                    <div class="col-lg-12">
                        <div class="form-group">
                        <label for="exampleInputEmail1"><?php echo $coluna['NOME'] ?></label>
                        <input type="text" class="form-control" name="<?php echo $coluna['NOME'] ?>" value="<?php echo $dados[1][$coluna['NOME']] ?>">
                      </div>
                    </div>
                
                    <?php } else { ?>
                        
                    <input type="hidden" class="form-control" name="PK" value="<?php echo $dados[1][$coluna['NOME']] ?>">
                        
                   <?php }
                    
                       } ?>
                    
                
                </div>
                
                <p>
                    <a href="<?php echo APP_URL ?>tabela/mostrar/<?php echo $tabela->getId(); ?>" class="btn btn-default">Voltar</a>
                    <button type="submit" class="btn btn-success">Salvar</button>
                </p>
                
            </form>
        </div>
    </div>
</div>

<script>
    resizePanelLeft();
</script>