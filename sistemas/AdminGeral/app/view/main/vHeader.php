<!doctype html>
<html lang='en-EN'>

<head>
    <title><?php echo APP_NAME; ?></title>

    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    
    
    
    <script src="/../../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
    <link href="<?php echo APP_URL ?>framework/libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo APP_URL ?>framework/libraries/bootstrap/js/bootstrap.min.js"></script>
    
    <link href="<?php echo APP_CSS ?>fonts.css" rel="stylesheet">
    <link href="<?php echo APP_CSS ?>default.css" rel="stylesheet" type="text/css"/>

</head>

<body>
    
<script>
    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
        
        
    }
</script>