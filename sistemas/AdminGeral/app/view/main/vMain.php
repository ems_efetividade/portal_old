<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    
    .progress { margin-bottom: 0px; height: 15px;}
    #tab-main.nav>li>a { padding: 5px 10px; font-size: 12px; }
    
    
    .editor {

    padding: 10px;

    color: black;
    font-size: 14px;
    font-family: monospace;
}
  
.statement {
    color: blue;
    font-weight: bold;
}
.variable {
    color: red;
}

</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-3 panel-left" style="overflow: auto; ">
            <br>
            <img class="center-block" src="../../../../../public/img/logo_ems2.png" alt=""/>
            <h4 class="text-center">
                <small><?php echo APP_NAME ?></small>
            </h4>
            <hr>
            <small>
                <ul class="list-unstyled">
                    <li><a data-toggle="collapse" href="#table" aria-expanded="false" aria-controls="table"><b>TABELAS</b>
                    <ul style="margin-left:10px" class="list-unstyled collapse" id="table">

                        <?php foreach($tables as $table) { ?>
                        <li>

                            <a data-toggle="collapse" href="#_<?php echo str_replace(" ", "-", $table['NAME']) ?>" aria-expanded="false" aria-controls="_<?php echo str_replace(" ", "-",$table['NAME']) ?>">
                                <small><small><span class="glyphicon glyphicon-plus"></span> </small></small>
                            </a>
                            <a href="#" data-table="<?php echo $table['NAME'] ?>" class="open-table"><?php echo $table['NAME'] ?></a>

                            <ul style="margin-left:10px" class="list-unstyled collapse" id="_<?php echo str_replace(" ", "-", $table['NAME']) ?>">
                                <?php foreach($table['COLUMNS'] as $columns) { ?>
                                <li>
                                    <span class="glyphicon <?php echo (($columns['PK'] == 0) ? 'glyphicon-list-alt' : 'glyphicon glyphicon-asterisk') ?>"></span> 
                                    <?php echo $columns['COLUMN_NAME'] ?> <span class="text-muted">(<?php echo (($columns['PK'] == 0) ? '' : 'PK, ') ?><?php echo $columns['DATA_TYPE'].(($columns['CHARACTER_MAXIMUM_LENGTH'] != NULL) ? ' ('.$columns['CHARACTER_MAXIMUM_LENGTH'].'), ' : ', ') ?><?php echo (($columns['IS_NULLABLE'] == 'NO') ? 'not null' : 'null') ?>)</span>
                                </li>
                                <?php } ?>
                            </ul>

                        </li>
                        <?php } ?>
                        <?php //echo $estrutura; ?>
                    </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#views" aria-expanded="false" aria-controls="views"><b>VIEWS</b></a>
                        <ul style="margin-left:10px" class="list-unstyled collapse" id="views">
                            <?php foreach($views as $view) { ?>
                            <li>
                                <a href="#" data-table="<?php echo $view['NAME'] ?>" class="open-table"><?php echo $view['NAME'] ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                    <li>
                        <a data-toggle="collapse" href="#procs" aria-expanded="false" aria-controls="procs"><b>PROCEDURES</b></a>
                        <ul style="margin-left:10px" class="list-unstyled collapse" id="procs">
                            <?php foreach($procedures as $proc) { ?>
                            <li>
                                <a href="#" data-proc="<?php echo $proc['NAME'] ?>" class="open-proc"><?php echo $proc['NAME'] ?></a>
                            </li>
                            <?php } ?>
                        </ul>
                    </li>
                </ul>    
                
                
            
                </small>
        </div>
        <div class="col-lg-9" style="padding:0px">
            
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <!-- Brand and toggle get grouped for better mobile display -->
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                  </div>

                  <!-- Collect the nav links, forms, and other content for toggling -->
                  <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                      <li>
                          <a href="#" class="add-tab">
                              <span class="glyphicon glyphicon-list-alt"></span>
                              Nova Aba
                          </a>
                      </li>
                      <li>
                          <a href="#" id="btnExecute">
                              <span class="glyphicon glyphicon-ok"></span>
                              Executar</a>
                      </li>
                      <li>
                          <a href="#" class="btn-export">
                              <span class="glyphicon glyphicon-download"></span>
                              Exportar</a>
                      </li>
                      
                    </ul>
                    
                    
                  </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
              </nav>
            
            
            
            
            
            
            
            
            
            
            
            <div style="padding-left:15px; padding-right: 15px;">
                
                <div>

                <!-- Nav tabs -->
                <ul id="tab-main" class="nav nav-tabs" role="tablist">
                  <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
                </ul>

                <!-- Tab panes -->
                <div id="tab-content-main" class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="home">
                        <?php echo $panel ?>
                    </div>
                </div>

              </div>
                
                
                
                
                
                
                
                
            </div>
        </div>
    </div>
</div>

<script>
    resizePanelLeft();
    
    $('#btnExecute').click(function(e) {
        var sql = $('#tab-content-main .active .txt').val();
        var tab   = $('#tab-content-main .active').attr('id');
        
        query(sql, tab);
    });
    
    
    
    $('.open-table').click(function(e) {
        var d = new Date();
        var n = d.getTime();
    
        var table = $(this).data('table');
        var sql = 'SELECT TOP 1000 * FROM ['+table+']';
        
        addTab(table, sql);
        
        query(sql, table);
     
    });
    
    $('.open-proc').click(function(e) {
        var proc = $(this).data('proc');
        var d = new Date();
        var n = d.getTime();
        
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>main/getProcCode/',
            data :{
                proc:proc
            },

            beforeSend: function() {

            },

            success: function(retorno) {
               addTab(n, retorno);
            }
        }); 
        
        
    });
    
    $('.add-tab').click(function(e) {
        var d = new Date();
        var n = d.getTime();
        addTab(n, '');
        
    });
    
    /*
$(".editor").on("keydown keyup", function(e){
    statement(e, this);
    
});    

$(".editor").change(function(e) {
    statement(e, this);
})

function statement(e, obj) {
 if (e.keyCode == 32){
        var text = $(obj).text().replace(/[\s]+/g, " ").trim();
        var word = text.split(" ");
        var newHTML = "";

        $.each(word, function(index, value){
            switch(value.toUpperCase()){
                case "SELECT":
                case "FROM":
                case "WHERE":
                case "LIKE":
                case "BETWEEN":
                case "NOT LIKE":
                case "FALSE":
                case "NULL":
                case "FROM":
                case "TRUE":
                case "NOT IN":
                    newHTML += "<span class='statement'>" + value + "&nbsp;</span>";
                    break;
                default: 
                    newHTML += "<span class='other'>" + value + "&nbsp;</span>";
                    break;
            }
        });
        
        
        
      	$(obj).html(newHTML);
        setEndOfContenteditable(obj);
        
        //// Set cursor postion to end of text
        
        var child = $(obj).children();
        var range = document.createRange();
        var sel = window.getSelection();
        range.setStart(child[child.length - 1], 1);
        range.collapse(true);
        sel.removeAllRanges();
        sel.addRange(range);
        $(obj)[0].focus(); 
        
    }   
}
*/
function executar(tabela) {
    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/open/'+tabela,


        beforeSend: function() {
            $('#result').html('Executando...');
        },

        success: function(retorno) {
            $('#result').html(retorno);
        }
    }); 
}


function query(sql, tab) {
    


    $('#'+tab+ ' .result').html('<h5 class="text-center text-muted">Executando...</h5>');
    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/execute/',
        data :{
            query:sql
        },

        beforeSend: function() {
            
        },

        success: function(retorno) {
            $('#'+tab+ ' .result').html(retorno);
            
            $('#'+tab+ ' .btn-export').click(function(e) {
                var sql = $('#tab-content-main .active .txt').val();

                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>main/export/'+sql,
                    data :{
                        query:sql
                    },

                    beforeSend: function() {

                    },

                    success: function(retorno) {
                        if($.trim(retorno) != "") {
                            location.href = '<?php echo APP_URL ?>main/download/'+retorno;
                        }
                    }
                }); 
            });
            
            
            
        },
        
    }); 
}

function addTab(name, data) {
    n = name;

    if($("#"+n).length) {

        $('#tab-main a[href="#'+n+'"]').tab('show');
        return n;
        
    } else {

        

        $.post('<?php echo APP_URL ?>main/getPanelSQL/', function( retorno ) {
            var tab = '<li role="presentation"><a href="#'+n+'" aria-controls="'+n+'" role="tab" data-toggle="tab">SQLQuery ['+n+'] <span data-id="'+n+'" class="remove glyphicon glyphicon-remove"></span></a></li>';
            var content = '<div role="tabpanel" class="tab-pane" id="'+n+'">'+retorno+'</div>';

            $('#tab-main').append(tab);
            $('#tab-content-main').append(content);
            $('#tab-content-main #'+n+' .txt').val(data);
            $('#tab-main a[href="#'+n+'"]').tab('show');


            $('.remove').click(function(e) {
                $('#'+$(this).data('id')).remove();
                $(this).closest('li').remove();
                $('#tab-main a:last').tab('show');
            });

            return n;
        });
    
    }
/*
    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/getPanelSQL/',
        success: function(retorno) {

            var tab = '<li role="presentation"><a href="#'+n+'" aria-controls="'+n+'" role="tab" data-toggle="tab">Query '+$('#tab-main li').length+' <span class="remove glyphicon glyphicon-remove"></span></a></li>';
            var content = '<div role="tabpanel" class="tab-pane" id="'+n+'">'+retorno+'</div>';

            $('#tab-main').append(tab);
            $('#tab-content-main').append(content);
            $('#tab-content-main #'+n+' .txt').val(data);
            $('#tab-main a[href="#'+n+'"]').tab('show');
            
        }
    });
    return n;
    */
}
function setEndOfContenteditable(contentEditableElement)
{
    var range,selection;
    if(document.createRange)//Firefox, Chrome, Opera, Safari, IE 9+
    {
        range = document.createRange();//Create a range (a range is a like the selection but invisible)
        range.selectNodeContents(contentEditableElement);//Select the entire contents of the element with the range
        range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
        selection = window.getSelection();//get the selection object (allows you to change selection)
        selection.removeAllRanges();//remove any selections already made
        selection.addRange(range);//make the range you have just created the visible selection
    }
    else if(document.selection)//IE 8 and lower
    { 
        range = document.body.createTextRange();//Create a range (a range is a like the selection but invisible)
        range.moveToElementText(contentEditableElement);//Select the entire contents of the element with the range
        range.collapse(false);//collapse the range to the end point. false means collapse to end rather than the start
        range.select();//Select the range (make it the visible selection
    }
}
    
</script>