<div class="panel panel-default">
    <div class="panel-body">
       
        <div style="max-height: 290px;overflow:auto">

            <table style="font-size:12px" class="table table-bordered table-condensed table-striped">
                <tr>
                    <th class="text-center">
                        <a href=#" class="btn-export">
                            <span class="glyphicon glyphicon-download"></span>
                        </a>
                    </th>

                    <?php foreach($columns as $col) { ?>
                    <th>
                        <small><?php echo $col; ?></small>
                    </th>
                    <?php } ?>
                </tr>

                <?php foreach($data as $i => $row) { ?>
                <tr>
                    <td style="background:#f9f9f9" class="text-center">
                        <small>
                            <small>
                                <?php echo $i; ?>
                            </small>
                        </small>
                    </td>

                    <?php foreach($columns as $col) { ?>
                    <td style="white-space:nowrap;">
                        <small>
                            <?php echo $row[$col]; ?>
                        </small>
                    </td>
                    <?php } ?>
                <?php } ?>
                </tr>
            </table>
            
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-lg-6">
                <span class="glyphicon glyphicon-ok-circle text-success"></span>
                <span class="text-muted">
                    <small>Query executada com sucesso!</small>
                </span>
            </div>
            <div class="col-lg-3 text-right text-muted">
                
            </div>
            <div class="col-lg-3  text-right  text-muted">
                <small>
                    <?php 
                        $time_end = microtime(true); 
                        $execution_time = ($time_end - $inicio);
                        $t = round($execution_time);

                        echo  sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);    ?> | <?php echo count($data) ?> linha(s).</small>
            </div>
        </div>
        
    </div>
</div>