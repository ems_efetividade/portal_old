<?php



class Controller extends View {
    
    protected $loader;
    
    public function __construct() {
        $this->loader = new Loader();
    }
    

    
    public function redirect($url) {
        header("Location: " .$url);
    }
    
    public function varDebug($var) {
        echo '<pre>';
        echo print_r($var);
        echo '<pre>';
    }
    
}