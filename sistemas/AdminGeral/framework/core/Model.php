<?php


class Model {
    
    public $cn;
    
    public function __construct() {    
       $this->cn = new appConexao();
    }
    
    public function createArrObjects($rs, $class) {
        
        $objects = array();
        if($rs) {
        foreach($rs as $row) {
            $object = new $class();
            $object->popular($row);
            $objects[] = $object;
        }
        }
        return $objects;
        
    }
    
}