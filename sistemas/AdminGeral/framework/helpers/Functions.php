<?php

function fnFormatarData($data) {
    if($data != "") {

        $dataHora = explode(" ", $data);

        if(count($dataHora) > 0) {
            $campos = explode("-", $dataHora[0]);
            $hora = explode(".", $dataHora[1]);
        } else {
            $campos = explode("-", $data);
            $hora = '';
        }

        if(count($campos) == 1) {
                $campos = explode("/", $data);
                return trim($campos[2])."-".trim($campos[1])."-".trim($campos[0]);
        } else {
                return trim($campos[2])."/".trim($campos[1])."/".trim($campos[0]).' '.$hora[0];
        }
    }
}

function fnFormatarMoedaBRL($valor=0, $decimal=2) {
    $valor  = ($valor != null) ? $valor : 0;
    return number_format($valor, $decimal, ",", ".");
}

function fnFormatarMoedaSQL($valor) {
    return str_replace(",", ".", str_replace(".", "", $valor));
}

function fnFormatarURL($url) {
    return strtolower(str_replace(' ', '-', $url));
}

function fnNomeMes($data='', $abrev=0) {
    $mes['01'] = "Janeiro";
    $mes['02'] = "Fevereiro";
    $mes['03'] = "Março";
    $mes['04'] = "Abril";
    $mes['05'] = "Maio";
    $mes['06'] = "Junho";
    $mes['07'] = "Julho";
    $mes['08'] = "Agosto";
    $mes['09'] = "Setembro";
    $mes['10'] = "Outubro";
    $mes['11'] = "Novembro";
    $mes['12'] = "Dezembro";
    
    $arr = explode('-', $data);
    
    return ($abrev == 0) ? $mes[$arr[1]].'/'.$arr[0] : substr($mes[$arr[1]],0,3).'/'.$arr[0];
}