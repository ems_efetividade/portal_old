<?php
require_once('lib/appController.php');
require_once('app/model/mAtendimentoPortal.php');
require_once('app/view/formsAtendimentoPortal.php');

class cAtendimentoPortal extends appController {

    private $modelAtendimentoPortal = null;

    public function __construct(){
        $this->modelAtendimentoPortal = new mAtendimentoPortal();
    }

    public function main(){
        $this->render('vAtendimentoPortal');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsAtendimentoPortal();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsAtendimentoPortal();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsAtendimentoPortal();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $_POST['resolvido'] = utf8_decode("Não visualizado");
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $_POST['aceita'] = "Aguardando";
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $_POST['atribuidopara'] = utf8_decode("Não Atribuido");
        $acompanhamento = $_POST['acompanhamento'];

        $this->modelAtendimentoPortal->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $this->modelAtendimentoPortal->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        return $this->modelAtendimentoPortal->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        return $this->modelAtendimentoPortal->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $this->modelAtendimentoPortal->updateObj();
    }
}
