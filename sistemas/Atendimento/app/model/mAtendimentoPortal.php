<?php
require_once('lib/appConexao.php');

class mAtendimentoPortal extends appConexao implements gridInterface {

    private $id;
    private $tipo;
    private $resgistradaPor;
    private $descricao;
    private $anexo;
    private $dataRegistro;
    private $setor;
    private $nomeUsuario;
    private $resolvido;
    private $correcoes;
    private $dataConclusao;
    private $aceita;
    private $motivoNaoAceitacao;
    private $atribuidoPara;
    private $acompanhamento;

    public function __construct(){
        $this->id;
        $this->tipo;
        $this->resgistradaPor;
        $this->descricao;
        $this->anexo;
        $this->dataRegistro;
        $this->setor;
        $this->nomeUsuario;
        $this->resolvido;
        $this->correcoes;
        $this->dataConclusao;
        $this->aceita;
        $this->motivoNaoAceitacao;
        $this->atribuidoPara;
        $this->acompanhamento;
    }

    public function getId(){
        return $this->id;
    }

    public function getTipo(){
        return $this->tipo;
    }

    public function getResgistradaPor(){
        return $this->resgistradaPor;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function getAnexo(){
        return $this->anexo;
    }

    public function getDataRegistro(){
        return $this->dataRegistro;
    }

    public function getSetor(){
        return $this->setor;
    }

    public function getNomeUsuario(){
        return $this->nomeUsuario;
    }

    public function getResolvido(){
        return $this->resolvido;
    }

    public function getCorrecoes(){
        return $this->correcoes;
    }

    public function getDataConclusao(){
        return $this->dataConclusao;
    }

    public function getAceita(){
        return $this->aceita;
    }

    public function getMotivoNaoAceitacao(){
        return $this->motivoNaoAceitacao;
    }

    public function getAtribuidoPara(){
        return $this->atribuidoPara;
    }

    public function getAcompanhamento(){
        return $this->acompanhamento;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setTipo($Tipo){
        $this->tipo = $Tipo;
    }

    public function setResgistradaPor($ResgistradaPor){
        $this->resgistradaPor = $ResgistradaPor;
    }

    public function setDescricao($Descricao){
        $this->descricao = $Descricao;
    }

    public function setAnexo($Anexo){
        $this->anexo = $Anexo;
    }

    public function setDataRegistro($DataRegistro){
        $this->dataRegistro = $DataRegistro;
    }

    public function setSetor($Setor){
        $this->setor = $Setor;
    }

    public function setNomeUsuario($NomeUsuario){
        $this->nomeUsuario = $NomeUsuario;
    }

    public function setResolvido($Resolvido){
        $this->resolvido = $Resolvido;
    }

    public function setCorrecoes($Correcoes){
        $this->correcoes = $Correcoes;
    }

    public function setDataConclusao($DataConclusao){
        $this->dataConclusao = $DataConclusao;
    }

    public function setAceita($Aceita){
        $this->aceita = $Aceita;
    }

    public function setMotivoNaoAceitacao($MotivoNaoAceitacao){
        $this->motivoNaoAceitacao = $MotivoNaoAceitacao;
    }

    public function setAtribuidoPara($AtribuidoPara){
        $this->atribuidoPara = $AtribuidoPara;
    }

    public function setAcompanhamento($Acompanhamento){
        $this->acompanhamento = $Acompanhamento;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $sql = "select count(id) from ATENDIMENTO_PORTAL";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tipo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TIPO LIKE '%$tipo%' ";
            $verif = true;
        }
        if(strcmp($resgistradaPor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESGISTRADA_POR LIKE '%$resgistradaPor%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($anexo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ANEXO LIKE '%$anexo%' ";
            $verif = true;
        }
        if(strcmp($dataRegistro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_REGISTRO LIKE '%$dataRegistro%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($nomeUsuario, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NOME_USUARIO LIKE '%$nomeUsuario%' ";
            $verif = true;
        }
        if(strcmp($resolvido, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESOLVIDO LIKE '%$resolvido%' ";
            $verif = true;
        }
        if(strcmp($correcoes, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CORRECOES LIKE '%$correcoes%' ";
            $verif = true;
        }
        if(strcmp($dataConclusao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CONCLUSAO LIKE '%$dataConclusao%' ";
            $verif = true;
        }
        if(strcmp($aceita, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ACEITA LIKE '%$aceita%' ";
            $verif = true;
        }
        if(strcmp($motivoNaoAceitacao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="MOTIVO_NAO_ACEITACAO LIKE '%$motivoNaoAceitacao%' ";
            $verif = true;
        }
        if(strcmp($atribuidoPara, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATRIBUIDO_PARA LIKE '%$atribuidoPara%' ";
            $verif = true;
        }
        if(strcmp($acompanhamento, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ACOMPANHAMENTO LIKE '%$acompanhamento%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        //$_POST = appSanitize::filter($_POST);

        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $sql = "INSERT INTO ATENDIMENTO_PORTAL ([TIPO],[RESGISTRADA_POR],[DESCRICAO],[ANEXO],[DATA_REGISTRO],[SETOR],[NOME_USUARIO],[RESOLVIDO],[CORRECOES],[DATA_CONCLUSAO],[ACEITA],[MOTIVO_NAO_ACEITACAO],[ATRIBUIDO_PARA],[ACOMPANHAMENTO])";
        $sql .=" VALUES ('$tipo','$resgistradaPor','$descricao','$anexo','$dataRegistro','$setor','$nomeUsuario','$resolvido','$correcoes','$dataConclusao','$aceita','$motivoNaoAceitacao','$atribuidoPara','$acompanhamento')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $sql = "DELETE FROM ATENDIMENTO_PORTAL WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($tipo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TIPO = '$tipo' ";
            $verif = true;
        }
        if(strcmp($resgistradaPor, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="RESGISTRADA_POR = '$resgistradaPor' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DESCRICAO = '$descricao' ";
            $verif = true;
        }
        if(strcmp($anexo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ANEXO = '$anexo' ";
            $verif = true;
        }
        if(strcmp($dataRegistro, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DATA_REGISTRO = '$dataRegistro' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="SETOR = '$setor' ";
            $verif = true;
        }
        if(strcmp($nomeUsuario, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="NOME_USUARIO = '$nomeUsuario' ";
            $verif = true;
        }
        if(strcmp($resolvido, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="RESOLVIDO = '$resolvido' ";
            $verif = true;
        }
        if(strcmp($correcoes, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="CORRECOES = '$correcoes' ";
            $verif = true;
        }
        if(strcmp($dataConclusao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DATA_CONCLUSAO = '$dataConclusao' ";
            $verif = true;
        }
        if(strcmp($aceita, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ACEITA = '$aceita' ";
            $verif = true;
        }
        if(strcmp($motivoNaoAceitacao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="MOTIVO_NAO_ACEITACAO = '$motivoNaoAceitacao' ";
            $verif = true;
        }
        if(strcmp($atribuidoPara, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATRIBUIDO_PARA = '$atribuidoPara' ";
            $verif = true;
        }
        if(strcmp($acompanhamento, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ACOMPANHAMENTO = '$acompanhamento' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "TIPO ";
        $sql .= ",";
        $sql .= "RESGISTRADA_POR ";
        $sql .= ",";
        $sql .= "DESCRICAO ";
        $sql .= ",";
        $sql .= "ANEXO ";
        $sql .= ",";
        $sql .= "DATA_REGISTRO ";
        $sql .= ",";
        $sql .= "SETOR ";
        $sql .= ",";
        $sql .= "NOME_USUARIO ";
        $sql .= ",";
        $sql .= "RESOLVIDO ";
        $sql .= ",";
        $sql .= "CORRECOES ";
        $sql .= ",";
        $sql .= "DATA_CONCLUSAO ";
        $sql .= ",";
        $sql .= "ACEITA ";
        $sql .= ",";
        $sql .= "MOTIVO_NAO_ACEITACAO ";
        $sql .= ",";
        $sql .= "ATRIBUIDO_PARA ";
        $sql .= ",";
        $sql .= "ACOMPANHAMENTO ";
        $sql .= " FROM ATENDIMENTO_PORTAL ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tipo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TIPO LIKE '%$tipo%' ";
            $verif = true;
        }
        if(strcmp($resgistradaPor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESGISTRADA_POR LIKE '%$resgistradaPor%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($anexo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ANEXO LIKE '%$anexo%' ";
            $verif = true;
        }
        if(strcmp($dataRegistro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_REGISTRO LIKE '%$dataRegistro%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($nomeUsuario, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NOME_USUARIO LIKE '%$nomeUsuario%' ";
            $verif = true;
        }
        if(strcmp($resolvido, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESOLVIDO LIKE '%$resolvido%' ";
            $verif = true;
        }
        if(strcmp($correcoes, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CORRECOES LIKE '%$correcoes%' ";
            $verif = true;
        }
        if(strcmp($dataConclusao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CONCLUSAO LIKE '%$dataConclusao%' ";
            $verif = true;
        }
        if(strcmp($aceita, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ACEITA LIKE '%$aceita%' ";
            $verif = true;
        }
        if(strcmp($motivoNaoAceitacao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="MOTIVO_NAO_ACEITACAO LIKE '%$motivoNaoAceitacao%' ";
            $verif = true;
        }
        if(strcmp($atribuidoPara, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATRIBUIDO_PARA LIKE '%$atribuidoPara%' ";
            $verif = true;
        }
        if(strcmp($acompanhamento, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ACOMPANHAMENTO LIKE '%$acompanhamento%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];
        $sql = "UPDATE  ATENDIMENTO_PORTAL";
        $sql .=" SET ";
        $sql .="TIPO = '$tipo' ";
        $sql .=" , ";
        $sql .="RESGISTRADA_POR = '$resgistradaPor' ";
        $sql .=" , ";
        $sql .="DESCRICAO = '$descricao' ";
        $sql .=" , ";
        $sql .="ANEXO = '$anexo' ";
        $sql .=" , ";
        $sql .="DATA_REGISTRO = '$dataRegistro' ";
        $sql .=" , ";
        $sql .="SETOR = '$setor' ";
        $sql .=" , ";
        $sql .="NOME_USUARIO = '$nomeUsuario' ";
        $sql .=" , ";
        $sql .="RESOLVIDO = '$resolvido' ";
        $sql .=" , ";
        $sql .="CORRECOES = '$correcoes' ";
        $sql .=" , ";
        $sql .="DATA_CONCLUSAO = '$dataConclusao' ";
        $sql .=" , ";
        $sql .="ACEITA = '$aceita' ";
        $sql .=" , ";
        $sql .="MOTIVO_NAO_ACEITACAO = '$motivoNaoAceitacao' ";
        $sql .=" , ";
        $sql .="ATRIBUIDO_PARA = '$atribuidoPara' ";
        $sql .=" , ";
        $sql .="ACOMPANHAMENTO = '$acompanhamento' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tipo = $_POST['tipo'];
        $resgistradaPor = $_POST['resgistradapor'];
        $descricao = $_POST['descricao'];
        $anexo = $_POST['anexo'];
        $dataRegistro = $_POST['dataregistro'];
        $setor = $_POST['setor'];
        $nomeUsuario = $_POST['nomeusuario'];
        $resolvido = $_POST['resolvido'];
        $correcoes = $_POST['correcoes'];
        $dataConclusao = $_POST['dataconclusao'];
        $aceita = $_POST['aceita'];
        $motivoNaoAceitacao = $_POST['motivonaoaceitacao'];
        $atribuidoPara = $_POST['atribuidopara'];
        $acompanhamento = $_POST['acompanhamento'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "TIPO ";
        $sql .= ",";
        $sql .= "RESGISTRADA_POR ";
        $sql .= ",";
        $sql .= "DESCRICAO ";
        $sql .= ",";
        $sql .= "ANEXO ";
        $sql .= ",";
        $sql .= "DATA_REGISTRO ";
        $sql .= ",";
        $sql .= "SETOR ";
        $sql .= ",";
        $sql .= "NOME_USUARIO ";
        $sql .= ",";
        $sql .= "RESOLVIDO ";
        $sql .= ",";
        $sql .= "CORRECOES ";
        $sql .= ",";
        $sql .= "DATA_CONCLUSAO ";
        $sql .= ",";
        $sql .= "ACEITA ";
        $sql .= ",";
        $sql .= "MOTIVO_NAO_ACEITACAO ";
        $sql .= ",";
        $sql .= "ATRIBUIDO_PARA ";
        $sql .= ",";
        $sql .= "ACOMPANHAMENTO ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM ATENDIMENTO_PORTAL ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tipo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TIPO LIKE '%$tipo%' ";
            $verif = true;
        }
        if(strcmp($resgistradaPor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESGISTRADA_POR LIKE '%$resgistradaPor%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($anexo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ANEXO LIKE '%$anexo%' ";
            $verif = true;
        }
        if(strcmp($dataRegistro, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_REGISTRO LIKE '%$dataRegistro%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($nomeUsuario, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="NOME_USUARIO LIKE '%$nomeUsuario%' ";
            $verif = true;
        }
        if(strcmp($resolvido, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESOLVIDO LIKE '%$resolvido%' ";
            $verif = true;
        }
        if(strcmp($correcoes, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CORRECOES LIKE '%$correcoes%' ";
            $verif = true;
        }
        if(strcmp($dataConclusao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_CONCLUSAO LIKE '%$dataConclusao%' ";
            $verif = true;
        }
        if(strcmp($aceita, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ACEITA LIKE '%$aceita%' ";
            $verif = true;
        }
        if(strcmp($motivoNaoAceitacao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="MOTIVO_NAO_ACEITACAO LIKE '%$motivoNaoAceitacao%' ";
            $verif = true;
        }
        if(strcmp($atribuidoPara, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATRIBUIDO_PARA LIKE '%$atribuidoPara%' ";
            $verif = true;
        }
        if(strcmp($acompanhamento, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ACOMPANHAMENTO LIKE '%$acompanhamento%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mAtendimentoPortal();
        $o->setId($param[0]);
        $o->setTipo($param[1]);
        $o->setResgistradaPor($param[2]);
        $o->setDescricao($param[3]);
        $o->setAnexo($param[4]);
        $o->setDataRegistro($param[5]);
        $o->setSetor($param[6]);
        $o->setNomeUsuario($param[7]);
        $o->setResolvido($param[8]);
        $o->setCorrecoes($param[9]);
        $o->setDataConclusao($param[10]);
        $o->setAceita($param[11]);
        $o->setMotivoNaoAceitacao($param[12]);
        $o->setAtribuidoPara($param[13]);
        $o->setAcompanhamento($param[14]);

        return $o;

    }
}