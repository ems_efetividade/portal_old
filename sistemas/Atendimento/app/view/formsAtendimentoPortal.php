<?php
class formsAtendimentoPortal{

    public function formEditar($objeto){?>
    <form id="formEditar" action="<?php print appConf::caminho ?>AtendimentoPortal/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="tipo">TIPO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getTipo()?>" name="tipo" id="tipo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="resgistradaPor">RESGISTRADAPOR</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getResgistradaPor()?>" name="resgistradapor" id="resgistradaPor" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="descricao">DESCRICAO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getDescricao()?>" name="descricao" id="descricao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="anexo">ANEXO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAnexo()?>" name="anexo" id="anexo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataRegistro">DATAREGISTRO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getDataRegistro()?>" name="dataregistro" id="dataRegistro" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="setor">SETOR</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getSetor()?>" name="setor" id="setor" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nomeUsuario">NOMEUSUARIO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getNomeUsuario()?>" name="nomeusuario" id="nomeUsuario" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="resolvido">RESOLVIDO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getResolvido()?>" name="resolvido" id="resolvido" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="correcoes">CORRECOES</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getCorrecoes()?>" name="correcoes" id="correcoes" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataConclusao">DATACONCLUSAO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getDataConclusao()?>" name="dataconclusao" id="dataConclusao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="aceita">ACEITA</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAceita()?>" name="aceita" id="aceita" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="motivoNaoAceitacao">MOTIVONAOACEITACAO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getMotivoNaoAceitacao()?>" name="motivonaoaceitacao" id="motivoNaoAceitacao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="atribuidoPara">ATRIBUIDOPARA</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAtribuidoPara()?>" name="atribuidopara" id="atribuidoPara" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="acompanhamento">ACOMPANHAMENTO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAcompanhamento()?>" name="acompanhamento" id="acompanhamento" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($objeto){?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="tipo">TIPO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getTipo()?>" name="tipo" id="tipo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="resgistradaPor">RESGISTRADAPOR</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getResgistradaPor()?>" name="resgistradapor" id="resgistradaPor" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="descricao">DESCRICAO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getDescricao()?>" name="descricao" id="descricao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="anexo">ANEXO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getAnexo()?>" name="anexo" id="anexo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataRegistro">DATAREGISTRO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getDataRegistro()?>" name="dataregistro" id="dataRegistro" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="setor">SETOR</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getSetor()?>" name="setor" id="setor" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nomeUsuario">NOMEUSUARIO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getNomeUsuario()?>" name="nomeusuario" id="nomeUsuario" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="resolvido">RESOLVIDO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getResolvido()?>" name="resolvido" id="resolvido" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="correcoes">CORRECOES</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getCorrecoes()?>" name="correcoes" id="correcoes" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataConclusao">DATACONCLUSAO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getDataConclusao()?>" name="dataconclusao" id="dataConclusao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="aceita">ACEITA</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getAceita()?>" name="aceita" id="aceita" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="motivoNaoAceitacao">MOTIVONAOACEITACAO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getMotivoNaoAceitacao()?>" name="motivonaoaceitacao" id="motivoNaoAceitacao" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="atribuidoPara">ATRIBUIDOPARA</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getAtribuidoPara()?>" name="atribuidopara" id="atribuidoPara" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="acompanhamento">ACOMPANHAMENTO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getAcompanhamento()?>" name="acompanhamento" id="acompanhamento" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar(){
        $today = date("d/m/Y");
        ?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>AtendimentoPortal/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataRegistro">Data Registro</label>
                        <input required readonly type="text" class="form-control " value="<?php echo $today;?>" name="dataregistro" id="dataRegistro" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="resgistradaPor">Registrada Por:</label>
                        <input required type="text" readonly class="form-control " value="<?php echo $_SESSION['nome']?>" name="resgistradapor" id="resgistradaPor" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="setor">Setor/Usuário</label>
                        <input required type="text" class="form-control " value="<?php echo $_SESSION['setor'] ?>" name="setor" id="setor" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="tipo">Categoria</label>
                        <select class="form-control" name="tipo">
                            <option>Duvida</option>
                            <option>Erro de Sistema</option>
                            <option>Solicitacao</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="tipo">Sistema</label>
                        <select class="form-control" name="nomeusuario">
                            <option>Cota</option>
                            <option>Demanda
                            </option>
                            <option>Prêmio
                            </option>
                            <option>LINCE
                            </option>
                            <option>P4D
                            </option>
                            <option>HB20
                            </option>
                            <option>GerRep
                            </option>
                            <option>Portal
                            </option>
                            <option>Painel
                            </option>
                            <option>ADV/AMV
                            </option>
                        </select>
                    </div>
                </div>


                <div class="col-sm-7 text-left">
                    <div class="form-group form-group-sm">
                        <label for="termo" >Arquivo</label>
                        <input  for="termo" readonly  class="form-control" name="anexo" id="nomeTermo" ></input>
                    </div>
                </div>
                <script>
                    $("#termo").change(function () {
                        input = document.getElementById("termo");
                        nome = "Não há arquivo selecionado. Selecionar arquivo...";
                        console.log(input.files.length);
                        if(input.files.length > 0)
                            nome = input.files[0].name;
                        document.getElementById("nomeTermo").value = nome;
                    });
                </script>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="termo" style="color: white;">.</label><br>
                        <label for="termo" class="form-control btn-default text-center">
                            <label for="termo" id="btnTermo" style="font-size: 18px" class="glyphicon glyphicon-cloud-upload"> </label>
                        </label>
                        <input required style="display: none;" type="file" name="" id="termo" >
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <label for="descricao">Descrição</label>
                        <textarea required type="text" class="form-control " name="descricao" id="descricao" ></textarea>
                    </div>
                </div>
            </div>
        </form><?php
    }
}
