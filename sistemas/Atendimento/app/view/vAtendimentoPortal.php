<?php
require_once('header.php');

class vAtendimentoPortal extends gridView{


    public function __construct(){
        $this->viewGrid();
    }


    public function viewGrid() {

        $cols = array('ID','Categoria','Sistema','Data','Status','Aceito','Responsável');
        $array_metodo = array('Id','Tipo','NomeUsuario','DataRegistro','Resolvido','Aceita','AtribuidoPara');
        $colsPesquisaNome = array('ID','TIPO','RESGISTRADAPOR','DATAREGISTRO','SETOR','NOMEUSUARIO','RESOLVIDO','CORRECOES','DATACONCLUSAO','ACEITA','MOTIVONAOACEITACAO','ATRIBUIDOPARA','ACOMPANHAMENTO');
        $colsPesquisa = array('id','tipo','resgistradaPor','descricao','anexo','dataRegistro','setor','nomeUsuario','resolvido','correcoes','dataConclusao','aceita','motivoNaoAceitacao','atribuidoPara','acompanhamento');
        gridView::setGrid_titulo('AtendimentoPortal');
        gridView::setGrid_sub_titulo('AtendimentoPortal');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('AtendimentoPortal');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }

}