<?php
require_once('lib/appController.php');

require_once('app/model/mFatura.php');
require_once('app/model/mCartao.php');
require_once('app/model/mColaboradorSetor.php');
require_once('app/model/mUsuarioAdmin.php');

require_once('app/model/mSetor.php');
require_once('app/model/mAtualizar.php');
require_once('app/model/mProjeto.php');
require_once('app/model/mProduto.php');

require_once('app/model/mNotaFiscal.php');

require_once('app/model/mMoveCategoria.php');

require_once('app/controller/cFatura.php');

class cAdmin extends appController {
    
    private $fatura;
    private $cartao;
    private $colaborador;
    private $atualizar;
    private $projeto;
    private $produto;
    private $notaFiscal;
    
    private $maxRegPag;
    private $pastaUpload = ROOT.'\\public\\upload\\';
    
    public function __construct() {
        $this->fatura = new mFatura();
        $this->cartao = new mCartao();
        $this->colaborador = new mColaboradorSetor();
        $this->atualizar = new mAtualizar();
        $this->projeto = new mProjeto();
        $this->produto = new mProduto();
        $this->notaFiscal = new mNotaFiscal();
        
        $this->maxRegPag = 11;
    }
    
    public function main() {
       
        $this->render('admin/vHome');
    }
    
    
    public function dashboard() {
        
    }
    
    public function nfs() {
        //echo $this->notaFiscal->listarTudo();
        $this->set('fatura', $this->fatura);
        $this->set('nfs', $this->notaFiscal->listarTudo());
        $this->render('admin/vListagemNFS');
    }
    
    public function reprovar() {
        $_POST = appSanitize::filter($_POST);
        
        $idFatura = $_POST['idFatura'];
        $obs = $_POST['observacao'];
        $despesas = isset($_POST['chkReprovar']) ? $_POST['chkReprovar'] : [];
        
        $this->fatura->setIdFatura($idFatura);
        if(count($despesas) > 0) {
            foreach($despesas as $idDespesa) {
         
                echo $this->fatura->reprovarDespesa($idDespesa, appFunction::dadoSessao('setor') , $obs);
            }
        }
        
    }
    
    public function listar($pagina=0, $limparFiltro=1) {
           
        if($limparFiltro == 1) {
           $this->limparFiltro();
        }
            
        $de = ((($pagina * $this->maxRegPag) - $this->maxRegPag) <= 0) ? 1 : (($pagina * $this->maxRegPag) - $this->maxRegPag)+1;
        $ate = ($pagina == 0) ? $this->maxRegPag : ($pagina * $this->maxRegPag);
        
        $criterios = $this->getFilterSession();
        
        $faturas = $this->fatura->listarTudo($de, $ate, $criterios);
        $totalRegistros = $this->fatura->getNumRows($criterios);
        
        $this->set('cartao', $this->cartao);
        $this->set('fatura', $this->fatura);
        $this->set('listaFaturas', $faturas);
        $this->set('cmbPesquisa', $this->campoPesquisaFatura($criterios['campos']));
        $this->set('criterios', $criterios);
        
        
        $this->set('paginacao', $this->paginacao($pagina, $totalRegistros, 'listar'));
        $this->set('pagina', $pagina);
        $this->set('menu1', 'active');
        $this->render('admin/vListagem');
    }
    
    public function visualizarFatura($idFatura=0, $pagina=0) {
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        
        $this->cartao->setIdCartao($this->fatura->getIdCartao());
        $this->cartao->selecionar();
        
        $cat = new mMoveCategoria();
        $categorias = $cat->listar();
        
        $this->set('categorias', $categorias);
        $this->set('cartao', $this->cartao);
        $this->set('fatura', $this->fatura);
        $this->set('pagina', $pagina);
        $this->set('menu1', 'active');
        $this->render('admin/vVisualizarFatura');

    }
    
    public function filtrar() {
        $_POST = appSanitize::filter($_POST);
                
        $criterios['mesFatura'] = (isset($_POST['cmbMesFatura'])) ? $_POST['cmbMesFatura'] : 0;
        $criterios['idStatus']  = (isset($_POST['cmbStatusFatura'])) ? $_POST['cmbStatusFatura'] : 0;
        $criterios['campos']    = (isset($_POST['cmbCampos'])) ? $_POST['cmbCampos'] : 0;
        $criterios['recebido'] = (isset($_POST['cmbRecebido']) ? $_POST['cmbRecebido'] : '');
        $criterios['criterio']  = (isset($_POST['txtCriterio'])) ? $_POST['txtCriterio'] : '';
        
        
        
        $this->setFilterSession($criterios);
        
        $this->listar(0,0);
    }
    
    public function limparFiltro() {
        @session_start();
        unset($_SESSION['filter']);
    }
    
    public function verificarCodFatura($idFatura=0) {
        
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        
        $this->cartao->setIdCartao($this->fatura->getIdCartao());
        $this->cartao->selecionar();
        
        
        $html = '<table class="table table-condensed table-striped">
                    <tr>
                        <td><strong>Cartão:</strong></td>
                        <td>'.appFunction::camuflarNroCartao($this->cartao->getNumCartao()).'</td>
                    </tr>
                    <tr>
                        <td><strong>Portador:</strong></td>
                        <td>'.$this->cartao->setor->getSetor().' '.$this->cartao->colaborador->getNome().'</td>
                    </tr>                            
                    <tr>
                        <td><strong>Mês:</strong></td>
                        <td>'.$this->fatura->getMesFatura().'</td>
                    </tr>
                    <tr>
                        <td><strong>Valor:</strong></td>
                        <td>R$ '.appFunction::formatarMoedaBRL($this->fatura->getTotalFatura()).'</td>
                    </tr>
                    <tr>
                        <td><strong>Status:</strong></td>
                        <td>'.$this->fatura->statusFatura->getStatus().'</td>
                    </tr>
                </table>
                

                <div class="form-group form-group-sm">
                    <label for="exampleInputEmail1">Data do Recebimento</label>
                    <input type="email" name="txtDataConfirmacao" class="form-control" id="exampleInputEmail1" value="'.date('d/m/Y').'">
                </div>';
        
        echo $html;
        
    }
    
    public function confirmarRecebimento() {
        $idFatura     = $_POST['txtIdFatura'];
        $dataCadastro = appFunction::formatarData($_POST['txtDataConfirmacao']);
        
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->setDataRecebimento($dataCadastro);
        $this->fatura->confirmarRecebimento();
        
        $this->reload();
    }
    
    public function campoPesquisaFatura($sel='') {
        
        $campos[0][0] = "";
        $campos[0][1] = ":: CAMPOS ::";
        
        $campos[1][0] = "REPLICATE('0', 5 - LEN(F.ID_FATURA)) + RTrim(F.ID_FATURA)";
        $campos[1][1] = "CÓD. FATURA";
        
        $campos[2][0] = "S.SETOR";
        $campos[2][1] = "SETOR";
        
        $campos[3][0] = "V.NOME";
        $campos[3][1] = "COLABORADOR";
     
        
        foreach ($campos as $campo) {
            $opt .= '<option value="'.$campo[0].'" '.(($sel == $campo[0]) ? "selected" : "") .' >'.$campo[1].'</option>';
        }
        
        return '<select class="form-control input-sm" name="cmbCampos">'
                    .$opt.'
                </select>';
        
    }    
    
    public function filtrarCartao() {
        $_POST = appSanitize::filter($_POST);
        
        $criterios['criterio'] = (isset($_POST['txtCriterio'])) ? $_POST['txtCriterio'] : '';
        $this->setFilterSession($criterios);
        $this->cartao(0,0);
    }
    
    public function exportarLista() {
        $dados   = $this->fatura->exportar($this->getFilterSession());
        
        //print_r($dados);
        $this->exportar($dados, 'ListaDeFaturas');
    }
    
    public function exportarCartao() {
        $dados   = $this->cartao->exportar($this->getFilterSession());
        $this->exportar($dados, 'ListaDeCartoes');
    }    
    
    
    
    
    public function cartao($pagina=0, $limparFiltro=1) {
        
         if($limparFiltro == 1) {
           $this->limparFiltro();
        }
        
        $limite = $this->calcularPagina($pagina);
        

        $criterios = $this->getFilterSession();
        
        $cartoes        = $this->cartao->listar($limite['de'], $limite['ate'], $criterios);
        $totalRegistros = $this->cartao->getNumRows($criterios);
        

        
        $this->set('totalCartoes', $this->cartao->getTotalCartoes());
        $this->set('cartoes', $cartoes);
        $this->set('menu2', 'active');
        $this->set('criterios', $criterios);
        $this->set('paginacao', $this->paginacao($pagina, $totalRegistros, 'cartao'));
        $this->render('admin/vCartao');
    }
    

    
    public function formCartao($idCartao=0) {
        
        $setor = new mSetor();
        
        $this->cartao->setIdCartao($idCartao);
        $this->cartao->selecionar();
        
        $this->set('cartao', $this->cartao);
        $this->set('colaborador', $this->colaborador);
        $this->set('comboSetor', $setor->comboSetor($this->cartao->getIdSetor()));
        $this->set('comboColaborador', $this->colaborador->comboColaboradorHistorico($this->cartao->getIdColaborador(), 'cmbColaborador', $this->cartao->getIdSetor()));
        
        $this->render('admin/vCartaoForm');
    }
    
    public function comboColaboradorHistorico($idSetor) {
        $combo = $this->colaborador->comboColaboradorHistorico('', 'cmbColaborador', $idSetor);
        echo $combo;
    }
    
    public function salvarCartao() {
        //print_r($_POST);
        
        $idCartao = appSanitize::filter($_POST['txtIdCartao']);
        $idSetor = appSanitize::filter($_POST['cmbSetor']);
        $idColaborador = appSanitize::filter($_POST['cmbColaborador']);
        $portador = appSanitize::filter($_POST['txtPortador']);
        $numCartao = appSanitize::filter($_POST['txtNumCartao']);
        $limite = (appSanitize::filter($_POST['txtLimite']) == NULL) ? 'NULL' : appFunction::formatarMoedaSQL(appSanitize::filter($_POST['txtLimite']));
        $validade = (appSanitize::filter($_POST['txtValidade']) == NULL) ? 'NULL' : "'".appFunction::formatarData(appSanitize::filter($_POST['txtValidade']))."'";
        $status = appSanitize::filter($_POST['cmbStatus']);
        
        $this->cartao->setIdCartao($idCartao);
        $this->cartao->setIdColaborador($idColaborador);
        $this->cartao->setIdSetor($idSetor);
        $this->cartao->setLimite($limite);
        $this->cartao->setNumCartao($numCartao);
        $this->cartao->setPortador($portador);
        $this->cartao->setValidade($validade);
        $this->cartao->setStatus($status);
        
        echo $this->cartao->salvar();
        
        //$this->reload();
    }
    
    public function excluirCartao($idCartao) {        
        $this->cartao->setIdCartao($idCartao);
        echo $this->cartao->excluir();
    }
    
    public function listaColaborador() {
        
        //$colaboradores = $this->colaborador->listar();
        
        $adms = new mUsuarioAdmin();
        
        $colaboradores = $adms->listarColaboradores();
        
        
        $this->set('colaboradores', $colaboradores);
        $this->render('admin/vListaColaborador');
    }
    
    
    public function atualizar() {
        $this->set('menu4', 'active');
        $this->set('tabelaMes', $this->tabelaMesFatura());
        $this->set('arquivos', $this->listarArquivoUpload());
        $this->render('admin/vAtualizar');
 
    }
    
    public function executarUploadETL($exc=0) {
        
        $maxQuery = 1;
        $cQuery = 1;
        $query = "" ;
        $arquivos = $this->listarArquivoUpload();
        $dados = [];
        
        $xl = '';
        
        if(count($arquivos) > 0) {
        
            $this->atualizar->limparTabelaBase();
            

            
            if($exc == 1) {
                $exp = explode("_", $arquivos[0]['arquivo']);
                $dataFatura = str_replace('.csv', '', $exp[2]);
                $this->atualizar->excluirFaturas($dataFatura);
            }
            
            

            foreach($arquivos as $arquivo) {

                $exp = explode("_", $arquivo['arquivo']);
                $dataFatura = str_replace('.csv', '', $exp[2]);

                $csv = array_map('str_getcsv', file($this->pastaUpload.$arquivo['arquivo']));
                $values = '';

                foreach($csv as $u => $data) {

                    if($u != 0) {

                        $dados = explode(";", $data[0]);
                        $values = '';
                        $values .= '(';

                        for($i=0;$i<=24;$i++) {
                            $x = trim($dados[$i]);
                            $x = ($i == 0) ? appFunction::formatarData($x) : $x;
                            $values .= "'" . (($x == '') ? null : $x) . "'" . ',';
                        }

                        $values .= "'".$dataFatura."'";

                        $values .= ')';

                        //echo $values;

                        $cQuery++;


                        //if($cQuery == $maxQuery) {
                        //    $cQuery = 1;
                            //$values = substr($values, 0, (strlen($values)-1));
                            $retorno = $this->atualizar->inserirTabelaBase($values);
                            if($retorno != "") {
                                echo $retorno;
                                break;
                            }

                       // }
                    }
                }     

                if($retorno != "") {
                    echo $retorno;
                    break;
                }
            }


            if($retorno == '') {
                $this->atualizar->atualizarNovasFaturas();
                echo "Faturas atualizadas com sucesso!";
            }
        
        }
        /*
        if($cQuery <= $maxQuery) {
            $values = substr($values, 0, (strlen($values)-1));
            echo $this->atualizar->inserirTabelaBase($values);
        }*/
        //echo $xl;
        
    }
    
    public function tabelaMesFatura() {
        $meses = $this->fatura->getMesesFatura();
        
        $html = '<table class="table table-bordered table-striped">
                <tr>
                    <th>Faturas no sistema</th>
                </tr>';
        
        foreach($meses as $mes) {
            $html .= '<tr><td>'.$mes->getMesFatura().'</td></tr>';
        }
        
        $html .= '</table>';
        
        return $html;
    }
    
    public function listarArquivoUpload() {
        $pasta = $this->pastaUpload;
        $arquivos = array();
        
        $diretorio = dir($pasta); 
        while($arquivo = $diretorio -> read()){
            if ($arquivo != '.' && $arquivo != '..'){
                
                if(!is_dir($pasta.$arquivo)) {
                
                $arq['arquivo'] = $arquivo;
                $arq['data'] = date ("d/m/Y H:i:s", fileatime($pasta.$arquivo));
                $arq['tamanho'] = appFunction::formatarTamanho(filesize($pasta.$arquivo));
                
                $arquivos[] = $arq;
                
                }
                
            }
        }
        $diretorio -> close();
        
        return $arquivos;
    }
    
    public function uploadFatura() {
        
        $pasta = $this->pastaUpload;
        
        $arquivos  = isset($_FILES['arqFaturas']) ? $_FILES['arqFaturas'] : [];
        $numArquivos = count($_FILES['arqFaturas']['name']);
        
        for($i=0;$i<$numArquivos;$i++) {
           $mover = move_uploaded_file($arquivos['tmp_name'][$i], $pasta.$arquivos['name'][$i]);
        }
    }
    
    public function excluirArquivosUpload() {
        $pasta = $this->pastaUpload;
        
        $arquivos = $_POST['chkArquivo'];
        
        if(count($arquivos) > 0) {
            foreach($arquivos as $arquivo) {
                unlink($pasta.$arquivo);
            }
        }
        
        unset($arquivos);
        $this->reload();
    }
    
    
    public function calcularPagina($pagina=0) {
        $limit['de'] = ((($pagina * $this->maxRegPag) - $this->maxRegPag) <= 0) ? 1 : (($pagina * $this->maxRegPag) - $this->maxRegPag)+1;
        $limit['ate'] = ($pagina == 0) ? $this->maxRegPag : ($pagina * $this->maxRegPag);
        
        return $limit;
    }
    
    
    
    
    
    public function paginacao($pagina=0, $totalRegistros=0, $action='', $limpar=0) {
        //$this->cadastro->setIdSetorCadastro(fn_dadoSessao('id_setor'));
        
        $total = $totalRegistros;
        $max = $this->maxRegPag;
        $paginas = ceil($total / $max);
        
        $pagina = ($pagina == 0) ? 1 : $pagina;
        $html = '';
        
        //if ($paginas > $pagina) {
        
        $prox =  ($paginas == $pagina) ? 1 : $pagina + 1;
        $ant = $pagina - 1;
        
        $html = '<small><nav>
                    <ul class="pager">
                        <li><a href="'.AppConf::root.'admin/'.$action.'/1/'.$limpar.'">Primeiro</a></li>
                        <li><a href="'.AppConf::root.'admin/'.$action.'/'.$ant.'/'.$limpar.'">Anterior</a></li>
                        <li><small>'.$pagina.' de '.$paginas.'</small></li>
                        <li><a href="'.AppConf::root.'admin/'.$action.'/'.$prox.'/'.$limpar.'">Próximo</a></li>
                        <li><a href="'.AppConf::root.'admin/'.$action.'/'.$paginas.'/'.$limpar.'">Último</a></li>
                    </ul>
                </nav></small>';
        
        //}
        
        return $html;
        
    }
    
    
    public function relatorio() {
        $setor = new mSetor();
        
        
        $this->set('linhas', $setor->listarLinha());
        
        $this->set('fatura', $this->fatura);
        $this->set('menu6', 'active');
        $this->render('admin/vRelatorio');
    }
    
    public function exportarRelatorio($opcao, $mesFaturaDE, $mesFaturaATE, $linha=0) {
        
        switch ($opcao) {
            
            case 1: {
                $dados = $this->fatura->exportarTotalLinha($mesFaturaDE, $mesFaturaATE);
                $nomeArquivo = 'TotalLinhas';
                break;
            }
            
            case 2: {
                $dados = $this->fatura->lancamento->despesa->exportarLista($mesFaturaDE, $mesFaturaATE);
                $nomeArquivo = 'ListaDeFaturas';
                break;
            }
            
            case 3: {
                $setor = new mSetor();
                $sigla = $setor->getSiglaLinha($linha);
                
                $dados = $this->fatura->lancamento->despesa->exportarListaMove($mesFaturaDE, $mesFaturaATE, $linha);
                $nomeArquivo = 'ListaDespesasMoveDistrital_'.$sigla;
                break;
            }
            
        }
        
        //print_r($dados);
        
        $this->exportar($dados, $nomeArquivo);
        
    }
    
    
    
    public function usuario() {
        
        $admin = new mUsuarioAdmin();
        $adminLogado = new mUsuarioAdmin();

        $adminLogado->setIdSetor(appFunction::dadoSessao('id_setor'));
        $adminLogado->selecionarSetor();
        
        $this->set('logado', $adminLogado);
        $this->set('usuarios', $admin->listar());
        $this->set('menu7', 'active');
        $this->render('admin/vUsuario');
    }
    
    public function usuarioForm($id=0) {
        $admin = new mUsuarioAdmin();
        
        //if($id != 0) {
            $admin->setIdUsuario($id);
            $admin->selecionar();
        //}
        
        $this->set('usuario', $admin);
        $this->render('admin/vUsuarioForm');
    }
    
    public function excluirUsuario($id) {
         $admin = new mUsuarioAdmin();
         $admin->setIdUsuario($id);
         $admin->excluir();
    }
    
    public function salvarUsuario() {
        $idColaborador = $_POST['txtIdColaborador'];
        $idSetor = $_POST['txtIdSetor'];
        $addUsuario = (isset($_POST['chkAddUsuario'])) ? 1 : 0;
        $idUsuario = $_POST['txtIdUsuario'];
        
        $user = new mUsuarioAdmin();
        
        $user->setIdUsuario($idUsuario);
        $user->setIdSetor($idSetor);
        $user->setIdColaborador($idColaborador);
        $user->setAddUsuario($addUsuario);
        $user->salvar();
        
        $this->reload();
    }
    
    

    
    
    
    
    public function projeto() {
        
        $this->set('projetos', $this->projeto->listar());
        $this->set('menu8', 'active');
        $this->render('admin/vProjeto');
    }
    
    public function projetoForm($idProjeto) {
        $this->projeto->setIdProjeto($idProjeto);
        $this->projeto->selecionar();
        
        $this->set('projeto', $this->projeto);
        echo $this->renderToString('admin/vProjetoForm');
    }
    
    public function projetoSalvar() {
        
        $idProjeto = $_POST['txtIdProjeto'];
        $descricao = $_POST['txtDescricao'];
        
        $this->projeto->setIdProjeto($idProjeto);
        $this->projeto->setDescricao($descricao);
        echo $this->projeto->salvar();
    }
    
    public function projetoExcluir($idProjeto) {
        $this->projeto->setIdProjeto($idProjeto);
        echo $this->projeto->excluir();
    }
    
    
    public function produto() {
        
        
        $BUS = $this->produto->listarBU();

        foreach($BUS as $bu) {
            $arr[$bu['ID_UN_NEGOCIO']] = $bu['EMPRESA'].' - '.$bu['UN_NEGOCIO'];
        }
        
        $this->set('produtos', $this->produto->listar());
        $this->set('bu', $arr);
        $this->set('menu9', 'active');
        $this->render('admin/vProduto');
    }
    
    public function produtoForm($idProduto) {
        $this->produto->setIdProduto($idProduto);
        $this->produto->selecionar();
        
        $this->set('produto', $this->produto);
        $this->set('BUS', $this->produto->listarBU());
        echo $this->renderToString('admin/vProdutoForm');
    }
    
    public function produtoSalvar() {
        
        $idProduto = $_POST['txtIdProduto'];
        $descricao = $_POST['txtDescricao'];
        $idBU      = $_POST['cmbBU'];
        
        $this->produto->setIdProduto($idProduto);
        $this->produto->setIdBU($idBU);
        $this->produto->setProduto($descricao);


        echo $this->produto->salvar();
    }
    
    public function produtoExcluir($idProduto) {
        $this->produto->setIdProduto($idProduto);
        echo $this->produto->excluir();
    }
    
    
    public function reprovarFatura() {
        $idFatura     = $_POST['txtIdFatura'];
        
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->reprovar(appFunction::dadoSessao('setor'));
        $this->reload();
    }
    
    
    
    private function setFilterSession($array='') {
        @session_start();
        unset($_SESSION['filter']);
        $_SESSION['filter'] = $array;
    }
    
    private function getFilterSession() {
        @session_start();
        return $_SESSION['filter'];
    }
    
    
    public function exportar($dados, $nomeDoArquivo='exportacao') {
        
        $d = [];
        for($i=1; $i<=count($dados); $i++) {
            $a = array_keys($dados[$i]);
            for($x=0; $x<=count($a)-1; $x++) {
                if(!is_numeric($a[$x])) {
                    $d[$i][$a[$x]] = $dados[$i][$a[$x]];
                }
            }
        }
        $dados = $d;
        
        $arrColuna = array_keys($dados[1]);
        
        $arquivoCSV = $nomeDoArquivo.'_'.date('d_m_y_H-i-s').'.csv';

        
        $caminho =  getcwd()."\\..\\..\\public\\img\\";
        $df = fopen($caminho.$arquivoCSV, 'w');
        
//        $linha = '';
//        
//        foreach($arrColuna as $coluna) {
//            $linha .=  '"'.$coluna.'";';
//        }
//        
//        $linha .= PHP_EOL;
//        fwrite($df, $linha);
//        
//        for($i=1; $i<=count($dados); $i++) {
//            $linha = '';
//            for($x=0; $x<=count($arrColuna)-1; $x++) {
//                $dados[$i][$arrColuna[$x]] = str_replace(PHP_EOL, '', $dados[$i][$arrColuna[$x]]);
//                $linha .=  '"'.$dados[$i][$arrColuna[$x]].'";';
//            }
//            
//            $linha .= PHP_EOL;
//            fwrite($df, $linha);
//        }
 //       fclose($df);
        
        
        fputcsv($df, array_keys(reset($dados)),';');
        
        //$html = '<table>';
        

        for($i=1; $i<=count($dados); $i++) {
            for($x=0; $x<=count($arrColuna)-1; $x++) {
                   $dados[$i][$arrColuna[$x]] = str_replace(PHP_EOL, '', $dados[$i][$arrColuna[$x]]); 
                
            }
        }

        
        foreach($dados as $row) {
            fputcsv($df, $row, ';', '"');
        }
         
        
        //foreach($ok as $row) {
       //     fwrite($df, $row);
        //}

        
        
        fclose($df);
        
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="'.$arquivoCSV);
        header('Content-Type: application/x-msexcel');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($caminho.'\\'.$arquivoCSV));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile($caminho.$arquivoCSV);
        
        
        unlink($caminho.'\\'.$arquivoCSV);
        
        
//        $arquivo = 'planilha.xls';
//
//        // Configurações header para forçar o download
//        header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
//        header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
//        header ("Cache-Control: no-cache, must-revalidate");
//        header ("Pragma: no-cache");
//        header ("Content-type: application/x-msexcel");
//        header ("Content-Disposition: attachment; filename=\"{$arquivo}\"" );
//        header ("Content-Description: PHP Generated Data" );
//        // Envia o conteúdo do arquivo
//        echo $html;
//        exit;
    }

    
}