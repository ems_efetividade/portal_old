<?php
require_once('lib/appController.php');

require_once('app/model/mLancamento.php');
require_once('app/model/mCartao.php');
require_once('app/model/mDespesa.php');
require_once('app/model/mFatura.php');
require_once('app/model/mStatusFatura.php');
require_once('app/model/mStatusDespesaAtual.php');
require_once('app/model/mColaboradorSetor.php');

require_once('app/model/mFaturaAtual.php');

class cAprovacao extends appController {
    
    private $fatura       = null;
    private $statusFatura = null;
    private $despesa      = null;
    private $cartao       = null;
    private $colaborador  = null;
    private $faturaAtual = null;
    
    public function __construct() {
        $this->fatura       = new mFatura();
        $this->statusFatura = new mStatusFatura();
        $this->despesa      =  new mDespesa();
        $this->cartao       = new mCartao();
        $this->colaborador  = new mColaboradorSetor();
        $this->faturaAtual = new mFaturaAtual();
    }    
    
    public function main() {
        //$this->listarFaturaEquipe();
        $this->listarFaturaEquipeAtual();
    }
    
    
    public function listarFaturaEquipeAtual($perfil=0, $mesFatura=0, $status=0) {
        
        
        $this->colaborador->setIdSetor(appFunction::dadoSessao('id_setor'));
        $this->colaborador->selecionar();
        
        $mesFatura = ($mesFatura == 0) ? $this->fatura->getMaxMesFatura() : $mesFatura;     
        
        $equipeAtual    = $this->faturaAtual->listarFaturaEstruturaAtual(appFunction::dadoSessao('id_setor'), $mesFatura);
        $equipePendente = $this->faturaAtual->listarFaturaEstruturaPendente(appFunction::dadoSessao('id_setor'), $mesFatura);
        
        
        foreach($equipeAtual as $dado) {
            $soma = $this->faturaAtual->listarTotalEquipe($dado['ID_SETOR'], $mesFatura);
            $dado['ACAO'] = '';
            $dado['SETOR'] = (count($soma) > 0) ? '<span class="caret"></span> '.$dado['SETOR'] : '<span class="no-caret">&nbsp;</span> '.$dado['SETOR'];
            
            if($dado['ID_FATURA'] != NULL) {
                $this->fatura->setIdFatura($dado['ID_FATURA']);
                $this->fatura->selecionar();
                $dado['ACAO'] = ($dado['ID_FATURA'] != NULL) ? $this->fatura->botoesFatura($dado['ID_SETOR']) : '';
            }
            
            $tabela .= $this->tabelaAtual($dado, 'subA', $dado['ID_PERFIL']);
        }
        
        $tabela2='';
        if(count($equipePendente) > 0) {
            foreach($equipePendente as $dado) {
                //$soma = $this->faturaAtual->listarTotalEquipe($dado['ID_SETOR'], $mesFatura);
                $dado['ACAO'] = '';


                if($dado['ID_FATURA'] != NULL) {
                    $this->fatura->setIdFatura($dado['ID_FATURA']);
                    $this->fatura->selecionar();
                    $dado['ACAO'] = ($dado['ID_FATURA'] != NULL) ? $this->fatura->botoesFatura($dado['ID_SETOR']) : '';
                }

                $tabela2 .= $this->tabelaAtual($dado, 'subAa', $dado['ID_PERFIL']);
            }
        }
        $this->set('mesFatura', $mesFatura);
        $this->set('fatura', $this->fatura);
        $this->set('listaAtual', $tabela);
        //$this->set('listaPendente', $tabela2);
        $this->render('aprovacao/vAprovacaoLista3');
    }
    
    public function listarEquipeAtual($idSetor, $mesFatura=0) {
        $this->colaborador->setIdSetor($idSetor);
        $this->colaborador->selecionar();
        
        $equipeAtual = $this->faturaAtual->listarFaturaEstruturaAtual($idSetor, $mesFatura);
 
        
        
        foreach($equipeAtual as $dado) {
            $soma = $this->faturaAtual->listarTotalEquipe($dado['ID_SETOR'], $mesFatura);
           
            
            $dado['ACAO'] = '';
            $dado['SETOR'] = (count($soma) > 0) ? '<span class="caret"></span> '.$dado['SETOR'] : '<span class="no-caret">&nbsp;</span> '.$dado['SETOR'];
            if($dado['ID_FATURA'] != NULL) {
                $this->fatura->setIdFatura($dado['ID_FATURA']);
                $this->fatura->selecionar();
                $dado['ACAO'] = ($dado['ID_FATURA'] != NULL) ? $this->fatura->botoesFatura($dado['ID_SETOR']) : '';
            }
            
            $tabela .= $this->tabelaAtual($dado, 'subA', $dado['ID_PERFIL']);
        }
        echo $tabela;
    }
    
    public function somarFaturaEquipeAtual($idSetor, $mesFatura=0) {
        $soma = $this->faturaAtual->listarTotalEquipe($idSetor, $mesFatura);
        
        $aprovar = $this->faturaAtual->listarTotalAprovarLote(appFunction::dadoSessao('id_setor'), $idSetor, $mesFatura);
        $botao = '';
        
        if($aprovar > 0) {
            $botao = '<a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalAprovarFaturaLote" data-id-setor="'.$idSetor.'" ><span class="glyphicon glyphicon-ok"></span>&nbsp;</a>';
        }
        
        foreach($soma as $i=> $dado) {
            
                       
            //$this->fatura->setIdFatura($dado['ID_FATURA']);
            $dado['NOME'] = '<span class="caret"></span> TOTAL DA EQUIPE ('.$dado['SETOR'].')';
            $dado['ID_SETOR'] = $idSetor;
            $dado['SETOR'] = '';
            $dado['ACAO'] = $botao;
            $tabela .= $this->tabelaAtual($dado, 'subSomaA', $dado['ID_PERFIL'], 'bold');
        }
        echo $tabela;
        
    }
    
    public function tabelaAtual($dados, $class="subA", $idPerfilLogado=0, $classeOpcional='') {
        $espaco[1][2] = '';
        $espaco[1][3] = 'tab1';
        $espaco[1][4] = 'tab2';

        $espaco[2][3] = '';
        $espaco[2][4] = 'tab1';     
        $espaco[3][4] = '';
        
        $background[1] = 'c3';
        $background[2] = 'c2';
        $background[3] = 'c1';

        $this->statusFatura->setIdStatusFatura($dados['STATUS']);
        $this->statusFatura->selecionar();
        
        $status = '';
        if($dados['STATUS'] > 0) {
            $status = $this->statusFatura->getStatus();
        }
        
         $clsAtual = [];
        if(isset($dados['ATUAL']) && $dados['ATUAL'] == 0) {
            $clsAtual[0] = 'text-muted';
            $clsAtual[1] = 'drawed';
        }
        
        $recebido = ($dados['DATA_RECEBIMENTO'] != '') ? '<span class="glyphicon glyphicon-ok"></span>' : '<span class="glyphicon glyphicon-remove"></span>' ;
        
       
        
        $html = '<div class="row border '.$clsAtual[0].' '.$background[$idPerfilLogado].'" >'
                . '<div class="col-lg-3 '.$class.'  vcenter" val="'.$dados['ID_SETOR'].'">'
                . '<span class="'.$clsAtual[1].' '.$classeOpcional.'">'.$dados['SETOR'].' '.$dados['NOME'] .'</span>'
                . '<span class=" loading pull-right"><img src="'.AppConf::root.'public/images/loading.gif" ></span>'
                . '</div>'
                
                . '<div class="col-lg-5 vcenter">'
                    . '<div class="row">'
                        . '<div class="col-lg-2 vcenter text-center '.$clsAtual[1].'">'.$dados['MES_FATURA'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.appFunction::formatarMoedaBRL($dados['TOTAL'],2).'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.appFunction::formatarMoedaBRL($dados['MARKET'],2).'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.appFunction::formatarMoedaBRL($dados['PESSOAL'],2).'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.appFunction::formatarMoedaBRL($dados['OUTRAS'],2).'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.appFunction::formatarMoedaBRL($dados['NAO_JUSTIFICADAS'],2).'</div>'
                    . '</div>'
                . '</div>'
                . '<div class="col-lg-2 vcenter text-center">'.$status.'</div>'
                . '<div class="col-lg-1 vcenter text-center">'.$recebido.'</div>'
                . '<div class="col-lg-1 vcenter text-right">'.$dados['ACAO'].'</div>'
                . '</div>'
                . '<div id="'.$class.$dados['ID_SETOR'].'"></div>';
       
        return $html;
      
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function listarFaturaEquipe($perfil=0, $mesFatura=0, $status=0) {
        $this->cartao->setIdColaborador(appFunction::dadoSessao('id_colaborador'));
        $this->cartao->selecionarPorColaborador();
        
        $mesFatura = ($mesFatura == 0) ? $this->fatura->getMaxMesFatura() : $mesFatura;        

        
        
        $this->set('perfil', $perfil);
        $this->set('mesFatura', $mesFatura);
        $this->set('status', $status);
        
        $this->set('cartao', $this->cartao);
        $this->set('colaborador', $this->colaborador);
        $this->set('fatura', $this->fatura);
        
        $this->set('faturas', $this->fatura->listarFaturaEquipe(appFunction::dadoSessao('id_colaborador'), $perfil, $mesFatura, $status));
        $col = $this->colaborador->listarEquipe(appFunction::dadoSessao('id_setor'));
        //$this->set('colaborador', $this->colaborador->listarEquipe(appFunction::dadoSessao('id_setor')));
        
        $this->set('listaAtual', $this->listarFaturaColaborador($col, $mesFatura));
        $this->set('lista', $this->listarFaturaColaborador($col, $mesFatura));
        //$this->set('faturas', $this->fatura->listarFaturaEquipe2(appFunction::dadoSessao('id_setor'), $perfil));
        //$this->set('colabs', $this->listarEquipe(appFunction::dadoSessao('id_setor'), '', appFunction::dadoSessao('id_setor')));
        
        $this->render('aprovacao/vAprovacaoLista2');
    }
    
    
    public function somaFatura($idSetor, $echo=0, $mesFatura=0) {
        $background[1]['back'] = 'c1';
        $background[2]['back'] = 'c2';
        $background[3]['back'] = 'c3';
        
        $this->colaborador->setIdSetor($idSetor);
        $this->colaborador->selecionar();
        

        $total = $this->fatura->totalFaturaEquipe($idSetor, $mesFatura);  //appFunction::dadoSessao('id_setor')
        $numFaturasAprovar = $this->fatura->faturasAprovar($idSetor, appFunction::dadoSessao('id_setor'), $mesFatura);
        
        $botao = '';
        if($numFaturasAprovar > 0 || $this->colaborador->getIdColaborador() == '') {
            $botao = '<a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalAprovarFaturaLote" data-id-setor="'.$idSetor.'" ><span class="glyphicon glyphicon-ok"></span>&nbsp;</a>';
        }
        
        //echo $echo;
        
        if($total[1]['TOTAL'] > 0) {
		
            $dados['ID_SETOR']          = $idSetor;
            $dados['SETOR']             = '<span class="caret"></span> <b>TOTAL EQUIPE '.$this->colaborador->getSetor().'</b>';
            $dados['MES_FATURA']        = '&nbsp;';
            $dados['TOTAL_FATURA']      = '<b>'.appFunction::formatarMoedaBRL($total[1]['TOTAL'],2).'</b>';
            $dados['MARKET']            = '<b>'.appFunction::formatarMoedaBRL($total[1]['MARKET'],2).'</b>';
            $dados['PESSOAL']           = '<b>'.appFunction::formatarMoedaBRL($total[1]['PESSOAL'],2).'</b>';
            $dados['OUTRAS']            = '<b>'.appFunction::formatarMoedaBRL($total[1]['OUTRAS'],2).'</b>';
            $dados['NAO_JUSTIFICADAS']  = '<b>'.appFunction::formatarMoedaBRL($total[1]['NAO_JUSTIFICADAS'],2).'</b>';
            $dados['STATUS']            =  '&nbsp;';
            $dados['FISICO']            =  '&nbsp;';
            $dados['ACAO']              = $botao;

            $tabela = $this->tabelaFaturaEquipe($dados, 'subSoma', '', 'tab1', $background[$this->colaborador->getNivelPerfil()]['back']);
            
            if($echo == 0) { 
                echo $tabela;
            } else {
               return $tabela;
            }
        }
    }
    
    public function listarFaturaColaborador($colaboradores, $mesFatura) {
        
        $espaco[1][2] = '';
        $espaco[1][3] = 'tab1';
        $espaco[1][4] = 'tab2';

        $espaco[2][3] = '';
        $espaco[2][4] = 'tab1';     
        
        $espaco[3][4] = '';
        
        $background[1]['back'] = 'c1';
        $background[2]['back'] = 'c2';
        $background[3]['back'] = 'c3';

        
        
        $tabela = '';
        $perfilLogado = appFunction::dadoSessao('perfil');
        //echo $perfilLogado;
        
        
        foreach($colaboradores as $colab) {
            
            $clsTR = ($colab->getNivelPerfil() == 4) ? '' : 'active';
            
            $existeSoma = $this->somaFatura($colab->getIdSetor(), 1, $mesFatura);
            
            $caret = ($existeSoma == '') ? '<span style="margin-left:10px;">&nbsp</span>' : '<span class="caret"></span> ';

            $f = $this->fatura->selecionarFaturaAtual($colab->getIdColaborador(), $mesFatura);
            $colaborador = ($colab->getNivelPerfil() < 1) ? '<strong>'.$colab->getSetor() .' '. $colab->getColaborador().'</strong>' : $colab->getSetor() .' '. $colab->getColaborador();

            $dados['ID_SETOR']  = $colab->getIdSetor();
            $dados['SETOR']     = $caret.$colaborador;
            $dados['MES_FATURA'] = $f->getMesFatura();
            $dados['TOTAL_FATURA'] = appFunction::formatarMoedaBRL($f->getTotalFatura(),2);
            $dados['MARKET'] = appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(1),2);
            $dados['PESSOAL'] = appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(2),2);
            $dados['OUTRAS'] = appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(3),2);
            $dados['NAO_JUSTIFICADAS'] = appFunction::formatarMoedaBRL($f->getTotalFatura() - ($f->lancamento->getTotalDespesa(1)+$f->lancamento->getTotalDespesa(2)+$f->lancamento->getTotalDespesa(3)+$f->lancamento->getTotalDespesa(4)),2);
            $dados['STATUS'] =  $f->statusFatura->getStatus();
            $dados['FISICO'] =  ($f->getDataRecebimento() == '') ? '<span class="glyphicon glyphicon-remove"></span>' : '<span class="text-success glyphicon glyphicon-ok"></span>' ;
            $dados['ACAO'] = ($f->getTotalFatura() == 0) ? '' : $f->botoesFatura($colab->getIdSetor());

            $tabela .= $this->tabelaFaturaEquipe($dados, 'sub', $clsTR, $espaco[$perfilLogado][$colab->getNivelPerfil()], $background[$colab->getNivelPerfil()]['back']);
        }
        
        return $tabela;
        
    }
    
    public function listarEquipe($idSetor, $mesFatura=0) {
       $colabs = $this->colaborador->listarEquipe($idSetor);
       echo $this->listarFaturaColaborador($colabs, $mesFatura);
    }
    
    public function tabelaFaturaEquipe($dados, $class="sub", $clsTR='', $classTD='', $background='') {       

        
        $html = '<div class="row border '.$background.'" >'
                . '<div class="col-lg-3 '.$class.'  vcenter" val="'.$dados['ID_SETOR'].'">'
                . '<span class="'.$classTD.'">'.$dados['SETOR'] .'</span>'
                . '<span class="loading pull-right"><img src="'.AppConf::root.'public/images/loading.gif" ></span>'
                . '</div>'
                
                . '<div class="col-lg-5 vcenter">'
                    . '<div class="row">'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['MES_FATURA'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['TOTAL_FATURA'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['MARKET'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['PESSOAL'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['OUTRAS'].'</div>'
                        . '<div class="col-lg-2 vcenter text-center">'.$dados['NAO_JUSTIFICADAS'].'</div>'
                    . '</div>'
                . '</div>'
                . '<div class="col-lg-2 vcenter text-center">'.$dados['STATUS'].'</div>'
                . '<div class="col-lg-1 vcenter text-center">'.$dados['FISICO'].'</div>'
                . '<div class="col-lg-1 vcenter text-right">'.$dados['ACAO'].'</div>'
                . '</div>'
                . '<div id="'.$class.$dados['ID_SETOR'].'"></div>';
       
        return $html;
    }
    
    
    public function abrirAprovacao($idFatura) {
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        
        $this->set('fatura', $this->fatura);
        $this->render('aprovacao/vAprovacaoFatura');
    }
    
    public function aprovarDespesa() {
        
        $_POST = appSanitize::filter($_POST);
        
        $idStatus = $_POST['txtIdStatus'];
        $descricao = $_POST['txtObservacao'];
        $despesas = $_POST['chkIdDespesa'];
      
        $status = new mStatusDespesaAtual();
        $status->setObservacao($descricao);
        $status->setIdStatusDespesa($idStatus);
        
        foreach($despesas as $despesa) {
            $status->setIdDespesa($despesa);
            $status->salvar();
        }
        $this->reload();
    }
    
    public function aprovarFatura($idFatura) {
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        $this->fatura->aprovar(appFunction::dadoSessao('setor'));
        
        $this->cartao->setIdCartao($this->fatura->getIdCartao());
        $this->cartao->selecionar();
        
        
        //$texto = 'Caro '.$this->cartao->colaborador->getColaborador().', a sua fatura do mês de '.$this->fatura->getMesFatura().' foi aprovada.';
        
        //appFunction::enviarEmail('george@localhost.com', 'Aprovação de Fatura', $texto, 'Cartão de Marketing - Ems Prescrição');
        
        $this->location(AppConf::root.'aprovacao');
    }
    
    public function aprovarTudo($idSetor, $mesFatura) {
        $this->fatura->aprovarEmLote($idSetor, appFunction::dadoSessao('id_setor'), $mesFatura);
        $this->location(AppConf::root.'aprovacao');
    }
    
    public function abrirFaturaAprovar($idFatura) {

        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();

        $this->set('fatura', $this->fatura);
        
        $this->set('lancamentoMarket', $this->despesa->listarPorTipo(1, $idFatura));
        $this->set('lancamentoOutros', $this->despesa->listarOutrasDespesas(2, $idFatura));
        
        $this->set('totalMarket',  $this->fatura->getTotais('market'));
        $this->set('totalPessoal', $this->fatura->getTotais('pessoal'));
        $this->set('totalNaoRec',  $this->fatura->getTotais('nao_reconhecido'));
        $this->set('totalNaoJust', $this->fatura->getTotais('nao_justificada'));
        $this->set('totalFatura',  $this->fatura->getTotais('fatura'));
        $this->set('statusFatura', $this->fatura->getStatus());
        
         $this->render('vAprovarFatura');
    }  
    
    
    
    
    
    

    
    
   
}

?>