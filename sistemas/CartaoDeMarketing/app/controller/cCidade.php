<?php
require_once('lib/appController.php');

require_once('app/model/mCidade.php');

class cCidade extends appController {
    

    private $cidade = null;
    
    public function __construct() {
        $this->cidade = new mCidade();
    }
    
    public function comboCidade($idUF, $nome='') {

       echo $this->cidade->comboCidade($idUF,0, $nome);
    }
    

    
}    