<?php
require_once('lib/appController.php');
require_once('app/model/mLancamento.php');
require_once('app/model/mMoveCategoria.php');

class cLancamento extends appController {
    
    private $lancamento = null;
    
    public function __construct() {
        $this->lancamento = new mLancamento();
    }
    
    public function visualizarLancamento($idLanc) {
        $this->lancamento->setIdLancamento($idLanc);
        $this->lancamento->selecionar();
        
        $cats = new mMoveCategoria();
        
        $this->set('categorias', $cats->listar());
        $this->set('lancamento', $this->lancamento);
        $this->render('fatura/vDespesaVisualizar');
    }
    
}