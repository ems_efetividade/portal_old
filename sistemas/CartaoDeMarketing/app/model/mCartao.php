<?php

require_once('lib/appConexao.php');

require_once('app/model/mColaborador.php');
require_once('app/model/mSetor.php');
require_once('app/model/mFatura.php');

class mCartao extends appConexao {
    
    private $idCartao;
    private $idColaborador;
    private $idSetor;
    private $numCartao;
    private $portador;
    private $limite;
    private $validade;
    private $status;
    
    public $colaborador;
    public $setor;
    
    public $fatura;
    
    
    public function __construct() {
        $this->idCartao = 0;
        $this->idColaborador = 0;
        $this->idSetor = 0;
        $this->numCartao = '';
        $this->portador = '';
        $this->limite = 0;
        $this->validade = '';
        $this->status = 0;
        
        $this->colaborador = new mColaborador();
        $this->setor = new mSetor();
        
        $this->fatura = new mFatura();
    }
    
    public function setIdCartao($valor) {
        $this->idCartao = trim($valor);
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setNumCartao($valor) {
        $this->numCartao = trim($valor);
    }
    
    public function setPortador($valor) {
        $this->portador = $valor;
    }
    
    public function setLimite($valor) {
        $this->limite = $valor;
    }
    
    public function setValidade($valor) {
        $this->validade  = $valor;
    }
    
    public function setStatus($valor) {
        $this->status = $valor;
    }
    
    
    
    public function getIdCartao() {
        return $this->idCartao;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getNumCartao() {
        return $this->numCartao;
    }
    
    public function getPortador() {
        return $this->portador;
    }
    
    public function getLimite() {
        return $this->limite;
    }
    
    public function getValidade() {
        return $this->validade ;
    }
    
    public function getStatus() {
        return $this->status;
    }
    
    public function getNomeStatus() {
       return ($this->status == 0) ? '<span class="label label-default">INATIVO</span>' : '<span class="label label-success">ATIVO</span>';
    }
    
    public function getTotalCartoes() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS TOTAL FROM CM_CARTAO");
        return $rs[1]['TOTAL'];
    }
    
    private function validar() {
        $retorno = null;
        
        if($this->numCartao == '') {
            $retorno = 'Por favor, digitar o número do cartao!';
            return $retorno;
        }
        
        if($this->portador == '') {
            $retorno = 'Por favor, digitar o nome do portador do cartão!';
            return $retorno;
        }   
        
        if($this->idColaborador == 0) {
            $retorno = 'Por favor, selecionar o setor proprietário do cartão!';
            return $retorno;
        }     
        
        
        if($this->idCartao == 0) {
            $rs = $this->executarQueryArray("SELECT COUNT(*) AS C FROM CM_CARTAO WHERE CARTAO = '".$this->numCartao."'");
            if($rs[1]['C'] > 0) {
                $retorno = 'Esse número de cartão já existe!';
                return $retorno;
            }
        }
        
        
        
    }
    
    public function salvar() {
        $validar = $this->validar();
        if($validar == '') {
        
        if($this->idCartao == 0) {
            $query = "INSERT INTO [CM_CARTAO]
                            ([ID_COLABORADOR]
                            ,[ID_SETOR]
                            ,[CARTAO]
                            ,[PORTADOR]
                            ,[LIMITE]
                            ,[VALIDADE]
                            ,[STATUS_CARTAO])
                      VALUES
                            (".$this->idColaborador."
                            ,'".$this->idSetor."'
                            ,'".$this->numCartao."'
                            ,'".$this->portador."'
                            ,".$this->limite."
                            ,".$this->validade."
                            ,".$this->status.")";
        } else {
            $query = "UPDATE [CM_CARTAO]
                    SET [ID_COLABORADOR] = ".$this->idColaborador."
                       ,[ID_SETOR] = ".$this->idSetor."
                       ,[CARTAO] = '".$this->numCartao."'
                       ,[PORTADOR] = '".$this->portador."'
                       ,[LIMITE] = ".$this->limite."
                       ,[VALIDADE] = ".$this->validade."
                       ,[STATUS_CARTAO] = ".$this->status."
                  WHERE [ID_CARTAO] = ".$this->idCartao."";
        }
        
        
        $c = new mCartao();
        $c->setIdCartao($this->idCartao);
        $c->selecionar();
                
        $this->executar($query);
        
        if($c->getNumCartao() != $this->numCartao) {
            $this->executar("EXEC proc_cm_cargaFatura");
        }
        
        } else {
            return $validar;
        }
        
    }
    
    public function excluir() {
        $c = new mCartao();
        $c->setIdCartao($this->idCartao);
        $c->selecionar();
        
        $numFatura = count($c->fatura->listarFaturas());
        
        if($numFatura == 0) {
            $this->executar("DELETE FROM CM_CARTAO WHERE ID_CARTAO = ".$this->idCartao);
            return '';
        } else {
            return 'Não é possível excluir este cartão pois já existem faturas relacionadas a ele.';
        }
        
    }
    
    
    public function selecionar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_cartaoSelecionar ".$this->idCartao ."");
        $this->popularVariaveis($rs);
    }

    
    public function selecionarPorColaborador() {
        $rs = $this->executarQueryArray("EXEC proc_cm_cartaoSelecionarPorColaborador ".$this->idColaborador." ");
        //$this->popularVariaveis($rs);  
        
        
        
        $cartoes = array();
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $cartao = new mCartao();
                $cartao->setIdCartao($row['ID_CARTAO']);
                $cartao->selecionar();

                $cartoes[] = $cartao;
            }
        }
        
        return $cartoes;
    }
    
    public function getNumRows($criterios) {
        $filtro = ($criterios['criterio'] == "") ? "" : " WHERE CARTAO + ' ' + PORTADOR LIKE '%".$criterios['criterio']."%'";
        $query = "SELECT COUNT(*) AS ROWS FROM CM_CARTAO ".$filtro." ";
        $rs = $this->executarQueryArray($query);
        return $rs[1]['ROWS'];
    }
    
    public function exportar($criterios) {
        
        $filtro = ($criterios['criterio'] == "") ? "" : " WHERE C.CARTAO + ' ' + C.PORTADOR LIKE '%".$criterios['criterio']."%'";
        
        $query = "SELECT 
                    V.SETOR,
                    V.NOME AS COLABORADOR,
                    C.PORTADOR,
                    CONVERT(VARCHAR, C.CARTAO) AS CARTAO,
                    V.LINHA,
                    V.PERFIL,
                    CASE WHEN C.STATUS_CARTAO = 0 THEN 'INATIVO' ELSE 'ATIVO' END AS STATUS
                FROM CM_CARTAO C INNER JOIN vw_colaboradorSetor V ON V.ID_COLABORADOR = C.ID_COLABORADOR ".$filtro." ";
        
        
        $query = "SELECT     S.SETOR, COL.NOME AS COLABORADOR, C.PORTADOR, CONVERT(VARCHAR, C.CARTAO) AS CARTAO, L.NOME AS LINHA, P.PERFIL, 
                                        CASE WHEN C.STATUS_CARTAO = 0 THEN 'INATIVO' ELSE 'ATIVO' END AS STATUS
                  FROM         PERFIL AS P RIGHT OUTER JOIN
                                        SETOR AS S INNER JOIN
                                        CM_CARTAO AS C ON S.ID_SETOR = C.ID_SETOR LEFT OUTER JOIN
                                        COLABORADOR AS COL ON C.ID_COLABORADOR = COL.ID_COLABORADOR LEFT OUTER JOIN
                                        LINHA AS L ON S.ID_LINHA = L.ID_LINHA ON P.ID_PERFIL = S.ID_PERFIL
                                        
                  ORDER BY S.SETOR";
        
        return $this->executarQueryArray($query);
    }
    
    public function listar($de=1, $ate=20, $criterios) {
        
		/*
        $filtro = ($criterios['criterio'] == "") ? "" : " WHERE S.SETOR + ' ' + C.CARTAO + ' ' + C.PORTADOR LIKE '%".$criterios['criterio']."%'";
		*/
		
		$filtro = ($criterios['criterio'] == "") ? "" : " WHERE C.CARTAO + ' ' + C.PORTADOR LIKE '%".$criterios['criterio']."%'";

        $query = "SELECT V.ID_CARTAO FROM (
                        SELECT
                                ROW_NUMBER() OVER (ORDER BY ID_CARTAO DESC) AS RANK,
                                C.ID_CARTAO
                        FROM CM_CARTAO C LEFT JOIN COLABORADOR V on V.ID_COLABORADOR = C.ID_COLABORADOR
                        LEFT JOIN SETOR S ON S.ID_SETOR = C.ID_SETOR ".$filtro."
                ) V WHERE V.RANK BETWEEN ".$de." AND ".$ate."";
        
        $rs = $this->executarQueryArray($query);
        
        $cartoes = array();
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $cartao = new mCartao();
                $cartao->setIdCartao($row['ID_CARTAO']);
                $cartao->selecionar();

                $cartoes[] = $cartao;
            }
        }
        
        return $cartoes;
    }
    
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idCartao = $rs[1]['ID_CARTAO'];
            $this->idColaborador = $rs[1]['ID_COLABORADOR'];
            $this->idSetor = NULL + $rs[1]['ID_SETOR'];
            $this->numCartao = $rs[1]['CARTAO'];
            $this->portador = $rs[1]['PORTADOR'];
            $this->limite = $rs[1]['LIMITE'];
            $this->validade = $rs[1]['VALIDADE'];
            $this->status = $rs[1]['STATUS_CARTAO'];
            
            $this->colaborador->setIdColaborador($this->idColaborador);
            $this->colaborador->selecionar();
            
            $this->setor->setIdSetor($this->idSetor);
            $this->setor->selecionar();
            
            $this->fatura->setIdCartao($this->idCartao);
        }
    }
    
}

?>