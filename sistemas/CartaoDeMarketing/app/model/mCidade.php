<?php

require_once('lib/appConexao.php');

class mCidade extends appConexao {
    
    private $idUF;
    private $idCidade;
    private $cidade;
    private $uf;
    
    public function __construct() {
        $this->idUF = 0;
        $this->idCidade = 0;
        $this->cidade = '';
        $this->uf = '';
    }
    
    public function setIdUF($valor) {
        $this->idUF = $valor;
    }

    
    public function setIdCidade($valor) {
        $this->idCidade = $valor;
    }    
    
    
    public function getIdUF() {
        return $this->idUF;
    }
    
    public function getIdCidade() {
        return $this->idCidade;
    }
    
    public function getCidade() {
        return $this->cidade;
    }
    
    public function getUF() {
        $rs = $this->executarQueryArray("SELECT SIGLA FROM ESTADO WHERE ESTADOID = ".$this->idUF);
        return $rs[1]['SIGLA'];
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CIDADE WHERE CIDADEID = ".$this->idCidade);
        $this->popularVariaveis($rs);
    }

    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idCidade = $rs[1]['CIDADEID'];
            $this->idUF = $rs[1]['ESTADOID'];
            $this->cidade = $rs[1]['NOME'];
        }
    }
    
    public function comboUF($selecionar=0, $nomeCombo='cmbUF') {
        $rs = $this->executarQueryArray("SELECT * FROM ESTADO");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ESTADOID'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ESTADOID'].'" '.$marcar.'>'.$row['SIGLA'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    
    public function comboCidade($idUF, $selecionar=0, $nomeCombo='cmbCidade') {
        $rs = $this->executarQueryArray("SELECT * FROM CIDADE WHERE ESTADOID = ".$idUF." ORDER BY NOME ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        if(count($rs) > 0) {
        foreach($rs as $row) {
            
            $marcar = ($row['CIDADEID'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['CIDADEID'].'" '.$marcar.'>'.$row['NOME'].'</option>';
        }
         }
        $select .= $option.'</select>';
        
        return $select;
        
       
        
        
    }
    
}

?>