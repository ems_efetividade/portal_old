<?php

require_once('lib/appConexao.php');

class mColaboradorSetor extends appConexao {
    
    private $idSetor;
    private $idColaborador;
    private $nivel;
    private $nivelPerfil;
    private $setor;
    private $colaborador;
    private $perfil;
    private $linha;
    private $foneCorp;
    private $fonePart;
    private $email;
    private $foto;
    
    public function __construct() {
        $this->idSetor = 0;
        $this->idColaborador = 0;
        $this->nivel = 0;
        $this->nivelPerfil = 0;
        $this->setor = '';
        $this->colaborador = '';
        $this->perfil = '';
        $this->linha = '';
        $this->foneCorp = '';
        $this->fonePart = '';
        $this->email = '';
        $this->foto = '';
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdColaborador($valor=0) {
        $this->idColaborador = $valor;
    }
    
    public function setColaborador($valor='') {
        $this->colaborador = $valor;
    }
    
    public function setSetor($valor='') {
        $this->setor = $valor;
    }   
    
    public function setPerfil($valor='') {
        $this->perfil = $valor;
    }    
    
    public function setLinha($valor='') {
        $this->linha = $valor;
    }    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getNivelPerfil() {
        return $this->nivelPerfil;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getColaborador() {
        return $this->colaborador;
    }
    
    public function getPerfil() {
        return $this->perfil;
    }
    
    public function getLinha() {
        return $this->linha;
    }
    
    public function getFoneCorp() {
        return $this->foneCorp;
    }
    
    public function getFonePart() {
        return $this->fonePart;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getFoto() {
        return $this->foto;
    }
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT 
	V.ID_SETOR, 
	V.ID_COLABORADOR, 
	V.NIVEL, 
	V.NIVEL_PERFIL, 
	V.SETOR, 
	V.NOME, 
	V.PERFIL, 
	V.LINHA, 
	V.FONE_CORPORATIVO, 
	V.FONE_PARTICULAR, 
	V.EMAIL, 
	V.FOTO,
	C.ID_COLABORADOR AS ID_COLAB
        FROM vw_colaboradorSetorHistorico V LEFT OUTER JOIN CM_CARTAO C ON C.ID_SETOR = V.ID_SETOR AND C.ID_COLABORADOR = V.ID_COLABORADOR WHERE C.STATUS_CARTAO = 1 AND C.ID_SETOR = ".($this->idSetor+0)));
    }
    
    public function selecionarPorColaborador() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_SETOR, ID_COLABORADOR, NIVEL, NIVEL_PERFIL, SETOR, NOME, PERFIL, LINHA, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, FOTO FROM vw_colaboradorSetor WHERE ID_COLABORADOR = ".($this->idColaborador+0)));
    }
    
    public function selecionarEquipe() {
        $rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = ".$this->idSetor);
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }
    
    public function listar() {
        $query = "SELECT ID_COLABORADOR, ID_SETOR, SETOR, NOME, PERFIL, LINHA FROM vw_colaboradorSetor WHERE NIVEL_PERFIL <= 4 ORDER BY SETOR ASC";
        
        $rs = $this->executarQueryArray($query);
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              
              $setor->setIdColaborador($row['ID_COLABORADOR']);
              $setor->setSetor($row['SETOR']);
              $setor->setColaborador($row['NOME']);
              $setor->setPerfil($row['PERFIL']);
              $setor->setLinha($row['LINHA']);
              $setor->setIdSetor($row['ID_SETOR']);
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }
    
    public function listarHistorico() {
        $query = "SELECT ID_COLABORADOR, SETOR, NOME, PERFIL, LINHA FROM vw_colaboradorSetorHistorico WHERE NIVEL_PERFIL <= 4 ORDER BY SETOR ASC";
        
        $rs = $this->executarQueryArray($query);
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              
              $setor->setIdColaborador($row['ID_COLABORADOR']);
              $setor->setSetor($row['SETOR']);
              $setor->setColaborador($row['NOME']);
              $setor->setPerfil($row['PERFIL']);
              $setor->setLinha($row['LINHA']);
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }    
    
    public function listarGerentes($perfil) {
        //echo "SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL_PERFIL = '".$perfil."' ORDER BY SETOR ASC";
        //echo "SELECT * FROM vw_colaboradorSetor WHERE NIVEL_PERFIL = '".$perfil."' AND ID_UN_NEGOCIO = ".appFunction::dadoSessao('id_un_negocio')."  ORDER BY SETOR ASC";
       // $query = "SELECT * FROM vw_colaboradorSetor WHERE ID_UN_NEGOCIO = 1 AND NIVEL_PERFIL = '".$perfil."'   ORDER BY SETOR ASC";
        $query = "EXEC proc_cm_colaboradorListarGerentes ".$perfil.", ".appFunction::dadoSessao('id_un_negocio')." ";
        $rs = $this->executarQueryArray($query);
        
        

        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $a[1] = $row;
              $setor = new mColaboradorSetor();
              //$setor->setIdSetor($row['ID_SETOR']);
              //$setor->selecionar();
              $setor->popularVariaveis($a);
              
              $arr[] = $setor;
          }

          return $arr;
            
        }
    }
    
    
    public function listarEquipeAtual($idSetor) {
        $query = "SELECT "
                . "ID_SETOR, "
                . "ID_COLABORADOR, "
                . "NIVEL, "
                . "NIVEL_PERFIL, "
                . "SETOR, "
                . "NOME, "
                . "PERFIL, "
                . "LINHA, "
                . "FONE_CORPORATIVO, "
                . "FONE_PARTICULAR, "
                . "EMAIL, "
                . "FOTO FROM vw_colaboradorSetor WHERE NIVEL = ".($idSetor+0);
        
        
        return $this->executarQueryArray($query);
        /*
        $rs = $this->executarQueryArray($query);
        $arr = array();
         
         
        if(count($rs) > 0) {
            
         
          
          foreach($rs as $row) {
              $arr[1] = $row;
              $setor = new mColaboradorSetor();
              $setor->popularVariaveis($arr);
              
              $arr[] = $setor;
          }
          
          
            
        }
        return $arr;
        */
        
    }
    
    public function listarEquipe($idSetor) {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = '".$idSetor."' ORDER BY SETOR ASC");
        $rs = $this->executarQueryArray("
        WITH G AS (
                SELECT 
		V.SETOR, 
		V.ID_SETOR, 
		V.NIVEL, 
		V.ID_PERFIL, 
		ISNULL(V.ID_COLABORADOR, C.ID_COLABORADOR) AS ID_COLABORADOR
		
	FROM 
		vw_colaboradorSetor  V
		 LEFT OUTER JOIN CM_CARTAO C ON C.ID_SETOR = V.ID_SETOR
		WHERE V.NIVEL = ".$idSetor."
                UNION ALL
                SELECT V.SETOR, V.ID_SETOR, V.NIVEL, V.ID_PERFIL, V.ID_COLABORADOR FROM vw_colaboradorSetor V 
                INNER JOIN G ON G.ID_SETOR = V.NIVEL
        ), V AS (

	/* pegando REPS E GDS */
	SELECT G.* FROM CM_FATURA F
		INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
		INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
	WHERE 
		G.ID_PERFIL = 1
		
	UNION ALL

	SELECT G.* FROM (	
		SELECT G.* FROM CM_FATURA F
			INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
			INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
		WHERE 
			G.ID_PERFIL = 1	
	) A	INNER JOIN G ON G.ID_SETOR = A.NIVEL

	UNION ALL

	/* pegando GDS E GR */
	SELECT G.* FROM CM_FATURA F
		INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
		INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
	WHERE 
		G.ID_PERFIL = 2
		
	UNION ALL

	SELECT G.* FROM (	
		SELECT G.* FROM CM_FATURA F
			INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
			INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
		WHERE 
			G.ID_PERFIL = 2
	) A	INNER JOIN G ON G.ID_SETOR = A.NIVEL
        
    UNION ALL
	
	/* pegando GDS E GR */
	SELECT G.* FROM CM_FATURA F
		INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
		INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
	WHERE 
		G.ID_PERFIL = 3
		
	UNION ALL

	SELECT G.* FROM (	
		SELECT G.* FROM CM_FATURA F
			INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
			INNER JOIN G ON G.ID_COLABORADOR = C.ID_COLABORADOR
		WHERE 
			G.ID_PERFIL = 3
	) A	INNER JOIN G ON G.ID_SETOR = A.NIVEL

)

SELECT ID_SETOR, SETOR FROM V WHERE NIVEL = ".$idSetor." GROUP BY ID_SETOR, SETOR ORDER BY SETOR ASC");
         $arr = array();
         
         
        if(count($rs) > 0) {
            
         
          
          foreach($rs as $row) {
              
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              $arr[] = $setor;
          }
          
          
            
        }
        return $arr;
    }
    
    
    public function comboColaborador($selecionar=0, $nomeCombo='cmbColaborador') {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE PERFIL = '".$perfil."' ORDER BY SETOR ASC");
        $rs = $this->executarQueryArray("SELECT ID_COLABORADOR, SETOR, NOME FROM vw_colaboradorSetor ORDER BY SETOR ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $colab) {
            
            $marcar = ($colab['ID_COLABORADOR'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$colab['ID_COLABORADOR'].'" '.$marcar.'>'.$colab['SETOR'].' '.$colab['NOME'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function comboColaboradorHistorico($selecionar=0, $nomeCombo='cmbColaboradorHistorico', $idSetor=0) {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE PERFIL = '".$perfil."' ORDER BY SETOR ASC");
        $rs = $this->executarQueryArray("SELECT ID_COLABORADOR, NOME FROM vw_colaboradorSetorHistorico WHERE ID_SETOR = ".$idSetor." ORDER BY NOME ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        if(count($rs) > 0) {
        foreach($rs as $colab) {
            
            $marcar = ($colab['ID_COLABORADOR'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$colab['ID_COLABORADOR'].'" '.$marcar.'>'.$colab['NOME'].'</option>';
        }
        
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function comboGerentes($perfil, $selecionar=0, $nomeCombo='cmbGerente') {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE PERFIL = '".$perfil."' ORDER BY SETOR ASC");
        $rs = $this->listarGerentes($perfil);
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $colab) {
            
            $marcar = ($colab->getIdColaborador()."|".$colab->getIdSetor() == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$colab->getIdColaborador()."|".$colab->getIdSetor().'" '.$marcar.'>'.$colab->getSetor().' '.$colab->getColaborador().'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function comboPerfil($selecionar=0, $nomeCombo='cmbPerfil') {
        $rs = $this->executarQueryArray("SELECT * FROM PERFIL WHERE ID_PERFIL <= 4 and NIVEL > ".appFunction::dadoSessao('perfil')." ORDER BY ID_PERFIL DESC");
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option value="0">:: PERFIL (TODOS) ::</option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_PERFIL'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_PERFIL'].'" '.$marcar.'>'.$row['PERFIL'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    protected function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idSetor       = $rs[1]['ID_SETOR'];
            $this->idColaborador = ($rs[1]['ID_COLABORADOR'] == NULL) ? $rs[1]['ID_COLAB'] : $rs[1]['ID_COLABORADOR'];
            $this->nivel         = $rs[1]['NIVEL'];
            $this->nivelPerfil   = $rs[1]['NIVEL_PERFIL'];
            $this->setor         = $rs[1]['SETOR'];
            $this->colaborador   = $rs[1]['NOME'];
            $this->perfil        = $rs[1]['PERFIL'];
            $this->linha         = $rs[1]['LINHA'];
            $this->foneCorp      = $rs[1]['FONE_CORPORATIVO'];
            $this->fonePart      = $rs[1]['FONE_PARTICULAR'];
            $this->email         = $rs[1]['EMAIL'];
            $this->foto          = $rs[1]['FOTO'];
        }
    }
    
}