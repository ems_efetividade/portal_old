<?php

require_once('lib/appConexao.php');

class mDashboard extends appConexao {
    
    private $arrMes = array(1 => 'Jan',
                            2 => 'Fev',
                            3 => 'Mar',
                            4 => 'Abr',
                            5 => 'Mai',
                            6 => 'Jun',
                            7 => 'Jul',
                            8 => 'Ago',
                            9 => 'Set',
                            10 => 'Out',
                            11 => 'Nov',
                            12 => 'Dez');
    
    public function getBoxes() {
        $query = "SELECT 
                        SUM(CASE WHEN S.ID_STATUS_FATURA = 1 THEN 1 ELSE 0 END) AS AGUARDANDO_LANC,
                        SUM(CASE WHEN S.ID_STATUS_FATURA IN (2,6,7) THEN 1 ELSE 0 END) AS AGUARDANDO_APROVA,
                        SUM(CASE WHEN S.ID_STATUS_FATURA = 3 THEN 1 ELSE 0 END) AS APROVADO,
                        SUM(CASE WHEN S.ID_STATUS_FATURA = 4 THEN 1 ELSE 0 END) AS REPROVADO
                FROM 
                        CM_FATURA F INNER JOIN CM_STATUS_ATUAL S ON S.ID_FATURA = F.ID_FATURA WHERE S.ORDEM = 1";
        
        
        return $this->executarQueryArray($query);
        
    }
    
    public function getGraficoLinha() {
        
        $d['mes'] = 6;
        $d['ano'] = 2016;
        
        $mes = $d['mes'];
        $ano = substr($d['ano'], 2, 2);
        
        $query  = '';
        for($i=12; $i>=1 ;$i--) {
            $query .= "SUM(CASE WHEN F.MES_FATURA = ".$mes.$ano." THEN L.VALOR ELSE 0 END) AS '".$this->arrMes[$mes]."/".$ano."',";
            $ano = (($mes-1) == 0) ? $ano-1 : $ano;
            $mes = (($mes-1) == 0) ? 12 : $mes-1;
        }
        $query = substr($query, 0, strlen($query)-1);
        
        $queryFinal = "SELECT     
                        ".$query."
                FROM
                        CM_FATURA F
                INNER JOIN CM_LANCAMENTO L ON L.ID_FATURA = F.ID_FATURA";
        
        return $this->executarQueryArray($queryFinal);
        
        //echo $queryFinal;
    }
    
}