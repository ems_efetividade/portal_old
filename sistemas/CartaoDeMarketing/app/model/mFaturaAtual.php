<?php
require_once('lib/appConexao.php');


class mFaturaAtual extends appConexao {

    public function listarFaturaEstruturaAtual($idSetor=0, $mesFatura=0) {
        $query = "EXEC proc_cm_listarEstruturaAtual '".$idSetor."', '".$mesFatura."'";        
        return $this->executarQueryArray($query);
    }
    
    public function listarTotalEquipe($idSetor=0, $mesFatura=0) {
        return $this->executarQueryArray("EXEC proc_cm_faturaTotalEquipe2 '".$idSetor."', '".$mesFatura."'");
    }
    
    public function listarTotalAprovarLote($idSetorLogado=0, $idSetor=0, $mesFatura=0) {
        $query = "EXEC proc_cm_listarTotalAprovarLote '".$idSetorLogado."','".$idSetor."','".$mesFatura."'";
        $rs = $this->executarQueryArray($query);
        return $rs[1]['APROVAR'];
    }

    public function listarFaturaEstruturaPendente($idSetor=0, $mesFatura=0) {
        $query = "WITH LX AS (
                        SELECT * FROM SETOR WHERE ID_SETOR = ".$idSetor."
                        UNION ALL
                        SELECT B.* FROM SETOR B INNER JOIN LX ON LX.ID_SETOR = B.NIVEL
                )

                SELECT 
                                A.ID_FATURA,
                                SS.ID_SETOR,
                                SS.ID_PERFIL,
                                SS.SETOR, 
                                V.NOME, 
                                A.NOME_MES AS MES_FATURA,
                                ISNULL(SUM(CASE WHEN A.ID_TIPO_DESPESA = 1 THEN A.VALOR ELSE 0 END),0) AS MARKET,
                                ISNULL(SUM(CASE WHEN A.ID_TIPO_DESPESA = 2 THEN A.VALOR ELSE 0 END),0) AS PESSOAL,
                                ISNULL(SUM(CASE WHEN A.ID_TIPO_DESPESA = 3 THEN A.VALOR ELSE 0 END),0) AS OUTRAS,
                                ISNULL((SUM(A.VALOR) - SUM(CASE WHEN A.ID_TIPO_DESPESA IN (1,2,3) THEN A.VALOR ELSE 0 END)),0) AS NAO_JUSTIFICADAS,
                                ISNULL(SUM(A.VALOR),0) AS TOTAL,
                                A.STATUS,
                                A.DATA_RECEBIMENTO

                                FROM (
                                        SELECT
                                                        C.ID_CARTAO,
                                                        C.CARTAO,
                                                        C.PORTADOR,
                                                        C.ID_COLABORADOR,
                                                        C.ID_SETOR,
                                                        F.ID_FATURA,
                                                        F.MES_FATURA,
                                                        NM.NOME_MES,
                                                        L.ESTABELECIMENTO,
                                                        D.ID_TIPO_DESPESA,
                                                        SF.ID_STATUS_FATURA AS STATUS,
                                                        F.DATA_RECEBIMENTO,
                                                        L.VALOR
                                        FROM
                                                        CM_CARTAO C
                                        INNER JOIN CM_FATURA F ON C.ID_CARTAO = F.ID_CARTAO
                                        INNER JOIN CM_LANCAMENTO L ON L.ID_FATURA = F.ID_FATURA
                                        LEFT  JOIN CM_DESPESA D ON D.ID_LANCAMENTO = L.ID_LANCAMENTO
                                        INNER JOIN CM_STATUS_ATUAL SA ON SA.ID_FATURA = F.ID_FATURA
                                        INNER JOIN CM_STATUS_FATURA SF ON SA.ID_STATUS_FATURA = SF.ID_STATUS_FATURA
                                        INNER JOIN CM_NOME_MES NM ON NM.MES = F.MES_FATURA
                                        LEFT OUTER JOIN vw_colaboradorSetor V ON C.ID_SETOR = V.ID_SETOR AND C.ID_COLABORADOR = V.ID_COLABORADOR
                                        WHERE F.MES_FATURA = ".$mesFatura." AND SA.ORDEM = 1 AND V.ID_SETOR IS NULL AND C.ID_SETOR IS NOT NULL
                ) A INNER JOIN COLABORADOR V ON V.ID_COLABORADOR = A.ID_COLABORADOR
                INNER JOIN SETOR SS ON SS.ID_SETOR  = A.ID_SETOR
                        INNER JOIN LX ON LX.ID_SETOR = SS.NIVEL
                        WHERE A.STATUS <> 3
                        GROUP BY
                        SS.SETOR, V.NOME, A.NOME_MES, A.STATUS, A.DATA_RECEBIMENTO, SS.ID_SETOR, SS.ID_PERFIL, A.ID_FATURA
                        ORDER BY SS.SETOR ASC";
        
        
        return $this->executarQueryArray($query);
    }
    
}