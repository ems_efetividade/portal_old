<?php

require_once('lib/appConexao.php');

class mMove extends appConexao {
    
    private $idDespesa;
    private $idCategoria;
    private $qtd;
    private $valor;
    
    public function __connstruct() {
        $this->idDespesa = 0;
        $this->idCategoria = 0;
        $this->qtd = 0;
        $this->valor = 0;
    }
    
    public function setIdDespesa($idDespesa) {
        $this->idDespesa = $idDespesa;
    }

    public function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    public function setQtd($qtd) {
        $this->qtd = $qtd;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function getIdDespesa() {
        return $this->idDespesa;
    }

    public function getIdCategoria() {
        return $this->idCategoria;
    }

    public function getQtd() {
        return $this->qtd;
    }

    public function getValor() {
        return $this->valor;
    }

    public function salvar() {
        $qy = "EXEC proc_cm_moveSalvar '".$this->getIdDespesa()."', '".$this->getIdcategoria()."', '".$this->getQtd()."', '".$this->getValor()."'";
        $this->executarQueryArray($qy);
    }
    
    public function excluir() {
        $this->executarQueryArray("EXEC proc_cm_moveExcluir '".$this->getIdDespesa()."'");
    }
    
}