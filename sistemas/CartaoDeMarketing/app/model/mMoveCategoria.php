<?php
require_once('lib/appConexao.php');


class mMoveCategoria extends appConexao {
    
    private $idCategoria;
    private $idPai;
    private $descricao;
    
    public function __construct() {
        $this->setIdCategoria(0);
        $this->setIdPai(0);
        $this->setDescricao('');
    }
    
    function setIdCategoria($idCategoria) {
        $this->idCategoria = $idCategoria;
    }

    function setIdPai($idPai) {
        $this->idPai = $idPai;
    }

    function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    function getIdCategoria() {
        return $this->idCategoria;
    }

    function getIdPai() {
        return $this->idPai;
    }

    function getDescricao() {
        return $this->descricao;
    }

    public function selecionar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_moveCategoriaSelecionar '".$this->getIdCategoria()."'");
        $this->popular($rs[1]);
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_moveCategoriaListarGrupo");
        $arr = [];
        foreach($rs as $row) {
            $cat = new mMoveCategoria();
            $cat->setIdCategoria($row['ID_CATEGORIA']);
            $cat->selecionar();
            
            $arr[] = $cat;
        }
        return $arr;
    }
    
    public function listarSub() {
        $rs = $this->executarQueryArray("EXEC proc_cm_moveCategoriaListarSub '".$this->getIdCategoria()."'");
        $arr = [];
        foreach($rs as $row) {
            $cat = new mMoveCategoria();
            $cat->setIdCategoria($row['ID_CATEGORIA']);
            $cat->selecionar();
            $arr[] = $cat;
        }
        return $arr;
    }
    
    public function listarResposta($idDespesa=0) {
        $rs = $this->executarQueryArray("EXEC proc_cm_moveCategoriaListarTotal '".$this->getIdCategoria()."', '".$idDespesa."'");
        return $rs;
    }

    
    private function popular($row) {
        $this->setIdCategoria($row['ID_CATEGORIA']);
        $this->setIdPai($row['ID_PAI']);
        $this->setDescricao($row['DESCRICAO']);
    }
    
}