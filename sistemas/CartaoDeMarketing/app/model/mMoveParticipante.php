<?php

require_once('lib/appConexao.php');

class mMoveParticipante extends appConexao {
    private $idDespesa;
    private $id;
    private $setor;
    private $nome;
    
    public function __construct() {
        $this->idDespesa = 0;
        $this->id = '';
        $this->setor = '';
        $this->nome = '';
    }
    
    public function setIdDespesa($idDespesa) {
        $this->idDespesa = $idDespesa;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function setSetor($setor) {
        $this->setor = $setor;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function getIdDespesa() {
        return $this->idDespesa;
    }

    public function getId() {
        return $this->id;
    }

    public function getSetor() {
        return $this->setor;
    }

    public function getNome() {
        return $this->nome;
    }

    public function salvar() {
        $qy = "EXEC proc_cm_moveParticipanteSalvar '".$this->getIdDespesa()."', '".$this->getId()."', '".$this->getSetor()."', '".$this->getNome()."'";
        $this->executarQueryArray($qy);
    }
    
    public function excluir() {
        $qy = "EXEC proc_cm_moveParticipanteExcluir '".$this->getIdDespesa()."'";
        $this->executarQueryArray($qy);
    }
    
    public function listar() {
        $qy = "EXEC proc_cm_moveParticipanteListar '".$this->getIdDespesa()."'";
        $rs = $this->executarQueryArray($qy);
        
        $a = [];
        if($rs) {
            foreach($rs as $row) {
                $c = new mMoveParticipante();
                $c->popular($row);
                $a[] = $c;
            }
        }
        return $a;
    }
    
    protected function popular($row) {
        $this->setIdDespesa($row['ID_DESPESA']);
        $this->setId($row['ID']);
        $this->setSetor($row['SETOR']);
        $this->setNome($row['NOME']);
    }
}