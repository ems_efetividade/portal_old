<?php

require_once('lib/appConexao.php');

class mProduto extends appConexao {
    
    private $idProduto;
    private $idBU;
    private $produto;
    
    public function __construct() {
        $this->idProduto = 0;
        $this->produto = '';
        $this->idBU = 0;
    }
    
    public function setIdProduto($valor) {
        $this->idProduto = $valor;
    }    
    
    public function setProduto($valor) {
        $this->produto = $valor;
    }

    public function setIdBU($idBU) {
        $this->idBU = $idBU;
    }
    

    public function getIdProduto() {
        return $this->idProduto;
    }    

    
    public function getProduto() {
        return $this->produto;
    }

    public function getIdBU() {
        return $this->idBU;
    }

    
    public function salvar() {
        //$query = "INSERT INTO CM_PRODUTO_DESPESA (ID_PRODUTO, ID_DESPESA, PARTICIPACAO) VALUES (".$this->idProduto.", ".$this->idDespesa.", ".$this->participacao.")";
        //$this->executar($query);
        if($this->idProduto == 0) {
            $query = "INSERT INTO CM_PRODUTO (PRODUTO, ID_UN_NEGOCIO) VALUES ('".utf8_decode($this->produto)."', ".$this->getIdBU().")";
        } else {
            $query = "UPDATE CM_PRODUTO SET PRODUTO = '".utf8_decode($this->produto)."', ID_UN_NEGOCIO = ".$this->getIdBU()." WHERE ID_PRODUTO = ".$this->idProduto;
        }
        
        $this->executar($query);
    }
    
    public function excluir() {
        $this->executar("DELETE FROM CM_PRODUTO WHERE ID_PRODUTO = ".$this->idProduto." AND ID_PRODUTO NOT IN (SELECT ID_PRODUTO FROM CM_PRODUTO_DESPESA GROUP BY ID_PRODUTO)");
    }

    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PRODUTO WHERE ID_PRODUTO = ".$this->idProduto);
        
        if(count($rs) > 0) {
            $this->idProduto = $rs[1]['ID_PRODUTO'];
            $this->idBU = $rs[1]['ID_UN_NEGOCIO'];
            $this->produto = $rs[1]['PRODUTO'];
        }
    }

    
    public function listar() {
        $produtos = array();
        $rs = $this->executarQueryArray("SELECT ID_PRODUTO FROM CM_PRODUTO ORDER BY  PRODUTO, ID_UN_NEGOCIO ASC");
        
        if (count($rs) > 0) {
            foreach($rs as $row) {
                
                $produto = new mProduto;
                $produto->setIdProduto($row['ID_PRODUTO']);
                $produto->selecionar();
                
                $produtos[] = $produto;
            }
            return $produtos;
        }
        
    }

    public function listarBU() {
        $rs = $this->executarQueryArray("EXEC proc_cm_produtoListarBu");
        return $rs;
    }
    
    public function comboProduto($sel=0, $nomeCombo='comboProdutoss') {
        $query = "EXEC proc_cm_produtoListar ".appFunction::dadoSessao('id_un_negocio')."";
        $produtos = $this->executarQueryArray($query);

        //var_dump($produtos);
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($produtos as $produto) {
            $option .= '<option value="'.$produto['ID_PRODUTO'].'">'.$produto['PRODUTO'].'</option>';
        }
        
        return $select.$option.'</select>';
        
    }
    
}

?>