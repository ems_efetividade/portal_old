<?php

require_once('lib/appConexao.php');

class mProjeto extends appConexao {
    
    private $idProjeto;
    private $descricao;
    private $planejado;
    
    public function __construct() {
        $this->idProjeto = 0;
        $this->descricao = '';
        $this->planejado = 0;
    }
    
    public function setIdProjeto($valor) {
        $this->idProjeto = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $valor;
    }
    
    public function getIdProjeto() {
        return $this->idProjeto;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getPlanejado() {
        return $this->planejado;
    }    
    
    public function salvar() {
        if($this->idProjeto == 0) {
            $query = "INSERT INTO CM_PROJETO (DESCRICAO, PLANEJADO) VALUES ('".utf8_decode($this->descricao)."', 0)";
        } else {
            $query = "UPDATE CM_PROJETO SET DESCRICAO = '".utf8_decode($this->descricao)."' WHERE ID_PROJETO = ".$this->idProjeto;
        }
        
        $this->executar($query);
    }
    
    public function excluir() {
        $this->executar("DELETE FROM CM_PROJETO WHERE ID_PROJETO = ".$this->idProjeto." AND ID_PROJETO NOT IN (SELECT ID_PROJETO FROM CM_DESPESA GROUP BY ID_PROJETO)");
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PROJETO WHERE ID_PROJETO = ".$this->idProjeto);
        
        
        if(count($rs) > 0) {
            $this->idProjeto = $rs[1]['ID_PROJETO'];
            $this->descricao = $rs[1]['DESCRICAO'];
            $this->planejado = $rs[1]['PLANEJADO'];
        }
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PROJETO");
        $a = array();
        foreach($rs as $row) {
            $c = new mProjeto();
            $c->setIdProjeto($row['ID_PROJETO']);
            $c->setDescricao($row['DESCRICAO']);
            
            $a[] = $c;
        }
        return $a;
    }
    
    public function comboProjeto($selecionar=0, $nomeCombo='cmbProjeto') {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PROJETO");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_PROJETO'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_PROJETO'].'" '.$marcar.'>'.$row['DESCRICAO'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    
}

?>