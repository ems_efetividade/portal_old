<?php

require_once('lib/appConexao.php');
require_once('app/model/mStatusFatura.php');
require_once('app/model/mColaboradorSetor.php');

class mStatusAtual extends appConexao {
    
    private $idStatusAtual;
    private $idFatura;
    private $idStatusFatura;
    private $descricao;
    private $dataCadastro;
    private $ordem;
    
    private $idSetorAprovador;
    private $idColaboradorAprovador;
    
    public $statusAtual;
    public $colaborador;
    public $setor;
    
    public function __construct() {
        $this->idStatusAtual = 0;
        $this->idFatura = 0;
        $this->idStatusFatura = 0;
        $this->descricao = '';
        $this->dataCadastro = '';
        $this->ordem = 0;
        
        $this->statusAtual = new mStatusFatura();
        $this->colaborador = new mColaboradorSetor();
        
    }
    
    public function setIdStatusAtual($valor) {
        $this->idStatusAtual = $valor;
    }    
    
    public function setIdFatura($valor) {
        $this->idFatura = $valor;
    }
    
    public function setIdStatusFatura($valor) {
        $this->idStatusFatura = $valor;
    } 
    
    public function setDescricao($valor) {
        $this->descricao = $this->removerAspas(utf8_decode($valor));
    } 
    
    public function setDataCadastro($valor) {
        $this->dataCadastro = $valor;
    }
    
    public function setOrdem($valor) {
        $this->ordem = $valor;
    }
    
    public function setIdSetorAprovador($valor) {
        $this->idSetorAprovador = $valor;
    }
    
    public function setIdColaboradorAprovador($valor) {
        $this->idColaboradorAprovador = $valor;
    }    
    

    
    public function getIdStatusAtual() {
        return $this->idStatusAtual;
    } 
    
    public function getIdFatura() {
        return $this->idFatura;
    }
    
    public function getIdStatusFatura() {
        return $this->idStatusFatura;
    } 
    
    public function getDescricao() {
        return $this->adicionarAspas($this->descricao);
    } 
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }
    
    public function getOrdem() {
        return $this->ordem;
    } 
    
    public function getStatus() {
        return $this->statusAtual->getStatus();
    }
    
    public function getIdSetorAprovador() {
        return $this->idSetorAprovador;
    }
    
    public function getIdColaboradorAprovador() {
        return $this->idColaboradorAprovador;
    }     
    
    public function getSetorAprovador() {
        return $this->colaborador->getSetor();
    }
    
    public function getColaboradorAprovador() {
        return $this->colaborador->getColaborador();
    }     
    
    
    public function salvar() {
        
        if($this->idStatusAtual == 0) {
            $query = "INSERT INTO CM_STATUS_ATUAL "
                    . "(ID_FATURA, "
                    . "ID_STATUS_FATURA, "
                    . "DESCRICAO, "
                    . "DATA_CADASTRO, "
                    . "ID_SETOR_APROVADOR, "
                    . "ID_COLABORADOR_APROVADOR, "
                    . "ORDEM) "
                        . "VALUES "
                    . "(".$this->idFatura.", "
                    . "".$this->idStatusFatura.", "
                    . "'".$this->descricao."', "
                    . "'".date('Y-m-d H:i:s')."', "
                    . "'".$this->idSetorAprovador."', "
                    . "'".$this->idColaboradorAprovador."', "
                    . "1);";
        }
        
        $this->executar("UPDATE CM_STATUS_ATUAL SET ORDEM = 0 WHERE ID_FATURA = ".$this->idFatura);
        $this->executarQueryArray($query);
    }
    
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT TOP 1 * FROM CM_STATUS_ATUAL WHERE ID_FATURA = ".$this->idFatura." ORDER BY DATA_CADASTRO DESC"));
    }
    
    public function selecionarPorId() {
        $this->popularVariaveis($this->executarQueryArray("SELECT  * FROM CM_STATUS_ATUAL WHERE ID_STATUS_ATUAL = ".$this->idStatusAtual.""));
    }
    
    private function removerAspas($texto) {
        return str_replace("'", "`", $texto);
    }
    
    private function adicionarAspas($texto) {
        return str_replace("`", "'", $texto);
    }
    
    private function popularVariaveis($rs) {
        
        if(count($rs) > 0) {
            $this->idStatusAtual          = $rs[1]['ID_STATUS_ATUAL'];
            $this->idFatura               = $rs[1]['ID_FATURA'];
            $this->idStatusFatura         = $rs[1]['ID_STATUS_FATURA'];
            $this->descricao              = $rs[1]['DESCRICAO'];
            $this->dataCadastro           = $rs[1]['DATA_CADASTRO'];
            $this->idSetorAprovador       = $rs[1]['ID_SETOR_APROVADOR'];
            $this->idColaboradorAprovador = $rs[1]['ID_COLABORADOR_APROVADOR'];
            $this->ordem                  = 1;
            
            $this->statusAtual = new mStatusFatura();
            $this->statusAtual->setIdStatusFatura($this->idStatusFatura);
            $this->statusAtual->selecionar();
            
            $this->colaborador = new mColaboradorSetor();
            $this->colaborador->setIdColaborador($this->idColaboradorAprovador);
            $this->colaborador->selecionarPorColaborador();
            
            $this->setor = new mColaboradorSetor();
            $this->setor->setIdSetor($this->idSetorAprovador);
            $this->setor->selecionar();
                    
        }
        
    }
    
    public function listarTodosStatusFatura() {
        $qy = "SELECT ID_STATUS_ATUAL FROM CM_STATUS_ATUAL WHERE ID_FATURA = ".$this->idFatura."  AND ID_SETOR_APROVADOR IS NOT NULL ORDER BY DATA_CADASTRO ASC ";
        $rs = $this->executarQueryArray($qy);

        $status = array();
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $st = new mStatusAtual();
                $st->setIdStatusAtual($row['ID_STATUS_ATUAL']);
                $st->selecionarPorId();
                
                $status[] = $st;
            }
        }
        
        return $status;
    }
   
    
}