<?php

require_once('lib/appConexao.php');

class mStatusDespesa extends appConexao {
    
    private $idStatusDespesa;
    private $descricao;
    
    private $labelStatus;
    
    public function __construct() {
        $this->idStatusFatura = 0;
        $this->descricao = '';
        
        $this->labelStatus[1][0] = 'label label-default';
        $this->labelStatus[1][1] = 'glyphicon glyphicon-hourglass';
        
        $this->labelStatus[2][0] = 'label label-success';
        $this->labelStatus[2][1] = 'glyphicon glyphicon-ok';
        
        $this->labelStatus[3][0] = 'label label-danger';
        $this->labelStatus[3][1] = 'glyphicon glyphicon-remove';
    }
    
    public function setIdStatusDespesa($valor) {
        $this->idStatusDespesa = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $valor;
    }
    
    public function getIdStatusDespesa() {
        return $this->idStatusDespesa;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getStatus() {
        return '<span class="'.$this->labelStatus[$this->idStatusDespesa][0].'"><span class="'.$this->labelStatus[$this->idStatusDespesa][1].'"></span> '.$this->descricao.'</span>';
    }
    
    public function salvar() {
        
    }
    
    public function excluir() {
        
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_STATUS_DESPESA WHERE ID_STATUS_DESPESA = ".$this->idStatusDespesa);
        
        
        if(count($rs) > 0) {
            $this->idStatusFatura = $rs[1]['ID_STATUS_DESPESA'];
            $this->descricao = $rs[1]['DESCRICAO'];
        }
    }
    
    public function comboStatusDespesa($selecionar=0, $nomeCombo='cmbStatusDespesa') {
        $rs = $this->executarQueryArray("SELECT * FROM CM_STATUS_DESPESA");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_STATUS_DESPESA'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_STATUS_DESPESA'].'" '.$marcar.'>'.$row['DESCRICAO'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    
}

?>