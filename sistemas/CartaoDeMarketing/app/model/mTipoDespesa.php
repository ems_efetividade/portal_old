<?php

require_once('lib/appConexao.php');

class mTipoDespesa extends appConexao {
    
    private $idTipoDespesa;
    private $descricao;
    
    public function __construct() {
        $this->idTipoDespesa = 0;
        $this->descricao = '';
    }
    
    public function setIdTipoDespesa($valor) {
        $this->idTipoDespesa = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $valor;
    }
    
    public function getIdTipoDespesa() {
        return $this->idTipoDespesa;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function salvar() {
        
    }
    
    public function excluir() {
        
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_TIPO_DESPESA WHERE ID_TIPO_DESPESA = ".$this->idTipoDespesa);
        
        
        if(count($rs) > 0) {
            $this->idTipoDespesa = $rs[1]['ID_TIPO_DESPESA'];
            $this->descricao = $rs[1]['DESCRICAO'];
        }
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_TIPO_DESPESA WHERE MOVE = 1");
        
        $arr = [];
        
        foreach($rs as $row) {
            $a = new mTipoDespesa();
            $a->setIdTipoDespesa($row['ID_TIPO_DESPESA']);
            $a->selecionar();
            $arr[] = $a;
        }
        
        return $arr;
        
    }
    
    public function comboTipoDespesa($selecionar=0, $nomeCombo='cmbTipoDespesa') {
        $rs = $this->executarQueryArray("SELECT * FROM CM_TIPO_DESPESA WHERE ID_TIPO_DESPESA > 1 AND MOVE = 0");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_TIPO_DESPESA'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_TIPO_DESPESA'].'" '.$marcar.'>'.$row['DESCRICAO'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    
}

?>