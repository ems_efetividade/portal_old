<?php
require_once('app/model/mUsuarioAdmin.php');
$userAdmin = new mUsuarioAdmin();
$userAdmin->setIdSetor(appFunction::dadoSessao('id_setor'));
$userAdmin->selecionarSetor();

?>

<br>

<div class="panael panael-default">
<div class="panel-abody">
<img class="center-block" height="90" src="<?php echo EMS_URL ?>\\..\\public\img\iconsFerramentas\logoEfet.png" alt="">
<h4 class="text-center" style="color:#fff">
    <small><?php echo APP_NAME ?></small>
</h4>
</div>
</div>

<hr>

<small>
<ul class="nav nav-pills nav-stacked nav-pills-stacked-example"> 
<!--    <li role="presentation" class="<?php echo $menu5 ?>"><a href="<?php print AppConf::root ?>dashboard"><span class="glyphicon glyphicon-upload"></span>  Dash</a></li> -->
    <li role="presentation" class="<?php echo $menu1 ?>"><a href="<?php print AppConf::root ?>admin/listar/0/1"><span class="glyphicon glyphicon-file"></span>  Faturas</a></li> 
    <li role="presentation" class="<?php echo $menu2 ?>"><a href="<?php print AppConf::root ?>admin/cartao/0/1"><span class="glyphicon glyphicon-credit-card"></span>  Cartões</a></li> 
<!--    <li role="presentation" class="<?php echo $menu3 ?>"><a href="<?php print AppConf::root ?>admin/baixa"><span class="glyphicon glyphicon-ok"></span>  Recebimento Físico</a></li> -->
    
    <li role="presentation" class="<?php echo $menu6 ?>"><a href="<?php print AppConf::root ?>admin/relatorio"><span class="glyphicon glyphicon-download"></span>  Exportar</a></li> 
    <?php if($userAdmin->getAddUsuario() == 1) { ?>
    <li role="presentation" class="<?php echo $menu8 ?>"><a href="<?php print AppConf::root ?>admin/projeto"><span class="glyphicon glyphicon-asterisk"></span>  Projetos</a></li> 
    <li role="presentation" class="<?php echo $menu9 ?>"><a href="<?php print AppConf::root ?>admin/produto"><span class="glyphicon glyphicon-asterisk"></span>  Produtos</a></li> 
    <li role="presentation" class="<?php echo $menu4 ?>"><a href="<?php print AppConf::root ?>admin/atualizar"><span class="glyphicon glyphicon-refresh"></span>  Upload Faturas</a></li>
    <!-- <li role="presentation" class="<?php echo $menu4 ?>"><a href="<?php print AppConf::root ?>admin/nfs"><span class="glyphicon glyphicon-refresh"></span>  Notas Fiscais</a></li> -->
    <li role="presentation" class="<?php echo $menu7 ?>"><a href="<?php print AppConf::root ?>admin/usuario"><span class="glyphicon glyphicon-user"></span>  Usuarios Admin</a></li>
    <?php } ?>
    
</ul>
</small>    