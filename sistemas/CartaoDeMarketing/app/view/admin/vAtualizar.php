<?php require_once('includes.php'); ?>

<style>
    
    #arqFaturas { display: none; }
    
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            
            <div class="row">
                <div class="col-lg-7">
                    <h3><span class="glyphicon glyphicon-user"></span> Upload de Faturas</h3>
                </div>
                
                
            </div>
            <div class="alert alert-warning" role="alert" style="padding:10px"><small><strong>Atenção!</strong> O nome dos arquivos devem começar com a palavra <strong>fatura_</strong>, seguido da <strong>LINHA</strong> e do <strong>MÊS</strong> no formato MMAAAA, trocando os espaços por <strong>_</strong> . <strong>Exemplo: fatura_cardio_062016.</strong></small></div>
            
            <hr>
            
            
            
            <div class="row">
                <div class="col-lg-10">
                    
                    <a href="#" id="btnAddArquivo" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-file"></span> Adicionar Arquivos</a>
                    <a href="#" id="btnExcluirArquivo" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-trash"></span> Excluir Arquivos Selecionados</a>
                </div>
                
                <div class="col-lg-2 text-right">
                    
                </div>
                
            
            </div>
            
            
            <br>
            
            <form id="formFaturas" action="<?php echo AppConf::root ?>admin/uploadFatura" method="post" enctype="multipart/form-data">
                <input type="file" id="arqFaturas" name="arqFaturas[]" multiple="multiple" />
            </form>
            
            
            <div class="row">
                
                <div class="col-lg-10">
            
                    <form id="formExcluirArquivo" action="<?php echo AppConf::root ?>admin/excluirArquivosUpload" method="post">
                    <small>
                    <table class="table table-bordered table-striped">
                        <tr>
                            <th class="text-center"><input  type="checkbox" id="checkAll" value="" /></th>
                            <th>Nome do Arquivo</th>
                            <th>Data do Arquivo</th>
                            <th>Tamaho do Arquivo</th>

                        </tr>
                        <?php foreach($arquivos as $arquivo) { ?>
                        <tr>
                            <td class="text-center"><input type="checkbox" class="files" name="chkArquivo[]" value="<?php echo $arquivo['arquivo']; ?>" /> </td>
                            <td><?php echo $arquivo['arquivo']; ?></td>
                            <td><?php echo $arquivo['data']; ?></td>
                            <td><?php echo $arquivo['tamanho']; ?></td>                    
                        </tr>
                        <?php } ?>
                    </table>
                    </small>
                    </form>
                    
                    <h5>
                        <div class="checkbox" style="margin-bottom:0px;">
                            <label>
                                <input type="checkbox" id="chkExcFatura" name="chkExcluirFatura" value="1"> <b>Ao atualizar, excluir as faturas existentes desse período. </b>
                            </label>
                            
                        </div>
                    <small>(ao marcar essa opção, se houver faturas já lançadas referente ao mês dos arquivos, eles serão apagados.)</small>
                    </h5>
                    <br>
                    <a href="#" id="btnExecutarETL"  class="btn btn-primary"><span class="glyphicon glyphicon-file"></span> Atualizar Faturas</a>
                </div>
                
                <div class="col-lg-2">
                    <small>
                        <div id="tabelaMes">
                            <?php echo $tabelaMes; ?>
                        </div>
                    </small>
                </div>
                
            </div>
            
            
            
        </div>
    </div>
</div>

<br>
<br>

    <!--MODAL Aguarde-->
<div class="modal modal-center fade" style="z-index:100000;" id="modalAguarde" tabindex="-1" role="dialog" data-backdrop="true" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
      
      <div class="modal-body">
      	<p class="text-center"><strong>Processando...</strong></p>
        <h5 class="text-center"><small>(Esta operação pode demorar alguns minutos.)</small></h5>
        
        <div class="progress">
          <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span class="sr-only"></span>
          </div>
        </div>
        
      </div>

    </div>
  </div>
</div>
    
    
    
    <div class="modal fade modal-center" style="z-index:10000000;" id="modalMsg" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
       <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
      </div>
      
      <div class="modal-body">
      
      	<div class="row">
            <div class="col-lg-12">
            	<h5><p class="text-center" id="msgTexto"></p></h5>
            </div>
        </div>
      
      	
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>

    </div>
  </div>
</div>

<script>

$(document).ready(function(e) {
    
    
    
    $("#checkAll").click(function(){
        $('.files:input:checkbox').not(this).prop('checked', this.checked);
    });    

    $('#btnExcluirArquivo').unbind().click(function(e) {
        $('#formExcluirArquivo').unbind().submit();
    });    
    
    $('#btnAddArquivo').unbind().click(function(e) {
        $('#arqFaturas').click();
    });    
    
    $('#btnExecutarETL').unbind().click(function(e) {
        
        var exc = ($('#chkExcFatura').prop('checked')) ? 1 : 0;
        
        
                
               
        $.ajax({
            type: "POST",
            url: '<?php echo AppConf::root ?>admin/executarUploadETL/'+exc,
            beforeSend: function (xhr) {
                modalAguarde('show');      
            },
            success: function(retorno) {
                //console.log(retorno);
                //alert(retorno);
                modalAguarde('hide');
                $('#modalMsg .modal-body').html(retorno);
                $('#modalMsg').modal('show');
                
            }
        });
        
    });
    
    $('#arqFaturas').unbind().change(function(e){

        
        $.ajax({
            type: "POST",
            url: $('#formFaturas').attr('action'),
            data: new FormData($('#formFaturas')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                
                if(retorno != '') {
                    
                    modalAguarde('hide');
                    msgBox(retorno);
                } else {
                    location.reload();
                }

            },
            beforeSend: function (a) {
               modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
            }
        });
        
        
    });
    
});

</script>