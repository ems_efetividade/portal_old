<?php require_once('app/view/admin/includes.php'); ?>

<style>

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-ok"></span>  Recebimento Físico <small>(<?php echo appFunction::formatarMoedaBRL($totalCartoes,0) ?>  cartão(ões))</small></h3>
                    <h5></h5>
                </div>
            </div>
            
            <hr>
            
        </div>
    </div>
</div>




<div class="modal fade" id="modalEditarCartao"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Cartão</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarCartao"><span class="glyphicon glyphicon-ok"></span> Salvar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalPesquisarColaborador"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body max-height">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSelecionar"><span class="glyphicon glyphicon-ok"></span> Selecionar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalExcluirCartao"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body max-height">
                <p>Deseja excluir este cartão?</p>
                    
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-danger btn-sm" id="btnExcluirCartao"><span class="glyphicon glyphicon-trash"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>


