<?php require_once('includes.php'); ?>
<?php 
for($i=0;$i<3;$i++) {
    

$dados .= "{
            name: 'Dado".$i."',
            data: [null, null, null, null, null, 6, 11, 32, 110, 235, 369, 640,
                ".rand().", 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
                27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
                26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
                24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586,
                22380, 21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950,
                10871, 10824, 10577, 10527, 10475, 10421, 10358, 10295, 10104]
        },";
}

$dados = substr($dados, 0, strlen($dados)-1);
?>
<style>
    .panel-left { background: #f9f9f9; }
    .panel {border-radius: 0px;}
</style>

<!--
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
-->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <h1>Dashboard</h1>
            <hr>
            
            <div class="row">
                
                
                
                
                
                
                
                <div class="col-lg-3">
                    <div class="panel panel-success">
                        <div class="panel-body list-group-item-success">
                            
                            <div class="row text-success">
                                <div class="col-lg-4">
                                    <p style="margin-bottom: 14px;margin-top: 14px">
                                    <span style="font-size: 500%" class="glyphicon glyphicon-ok"></span>
                                    </p>
                                </div>
                                
                                <div class="col-lg-8">
                                    <p class="text-right" style="font-size: 300%"><?php echo $box[1]['APROVADO'] ?></p>
                                    <p class="text-right">faturas aprovadas</p>
                                </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
                
                <div class="col-lg-3">
                    <div class="panel panel-danger">
                        <div class="panel-body list-group-item-danger">
                            
                            <div class="row text-danger">
                                <div class="col-lg-4">
                                    <p style="margin-bottom: 14px;margin-top: 14px">
                                    <span style="font-size: 500%" class="glyphicon glyphicon-remove"></span>
                                    </p>
                                </div>
                                
                                <div class="col-lg-8">
                                    <p class="text-right" style="font-size: 300%"><?php echo $box[1]['REPROVADO'] ?></p>
                                    <p class="text-right">faturas reprovadas</p>
                                </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
<div class="col-lg-3">
                    <div class="panel panel-default">
                        <div class="panel-body list-group-item-info">
                            
                            <div class="row text-info">
                                <div class="col-lg-4">
                                    <p style="margin-bottom: 14px;margin-top: 14px">
                                    <span style="font-size: 500%" class="glyphicon glyphicon-list-alt"></span>
                                    </p>
                                </div>
                                
                                <div class="col-lg-8">
                                    <p class="text-right" style="font-size: 300%"><b><?php echo $box[1]['AGUARDANDO_APROVA'] ?></b></p>
                                    <p class="text-right">faturas cadastradas</p>
                                </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>                
                
                <div class="col-lg-3">
                    <div class="panel panel-warning">
                        <div class="panel-body list-group-item-warning">
                            
                            <div class="row text-warning">
                                <div class="col-lg-4">
                                    <p style="margin-bottom: 14px;margin-top: 14px">
                                    <span style="font-size: 500%" class="glyphicon glyphicon-alert"></span>
                                    </p>
                                </div>
                                
                                <div class="col-lg-8">
                                    <p class="text-right" style="font-size: 300%"><?php echo $box[1]['AGUARDANDO_LANC'] ?></p>
                                    <p class="text-right">faturas à justificar</p>
                                </div>
                                
                                
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="row">
                
                <div class="col-lg-8">
                    
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <div id="container" style="min-width: 310px; height: 300px; margin: 0 auto"></div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-lg-4">
                    
                    <div class="panel panel-default">
                        <div class="panel-body">
                            
                        </div>
                    </div>
                    
                </div>
                
            </div> 
            
            
        </div>
    </div>
</div>
<?php print_r($graf); ?>
<script>

$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: <?php echo $graf['xAxis'] ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: <?php echo $graf['data'] ?>
    });
});

</script>