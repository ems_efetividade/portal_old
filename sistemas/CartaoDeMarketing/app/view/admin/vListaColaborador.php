<script>

$(document).ready(function(e){
    
    $('#search').keyup(function () {
      var searchitem = $('#search').val();
      if (searchitem == '' || searchitem == null || searchitem == undefined)           {
          $('#table tbody tr').show();
      }
      else {
          searchitem = searchitem.toUpperCase();
          $('#table tbody tr').hide();
          $('#table tbody tr').each(function () {
              if ($(this).text().indexOf(searchitem) > -1) {
                  $(this).show();
              }
          });
      }
  });
    
});
</script>
<input type="text" id="search" name="txtCriterio" class="form-control input-sm" value="" autocomplete="off" placeholder="">
<br>
<div style="max-height: 300px; overflow-x: auto">
<small><small>
    
    <table id="table" class="table table-condensed table-striped">
        <tr>
            <th>Colaborador</th>
            <th>Perfil</th>
            
        </tr>
        <?php foreach($colaboradores as $colab) { ?>
        
        <tr>
            <td>
                <input type="radio" name="optionsRadios" id="optionsRadios1" value="<?php echo $colab['ID_COLABORADOR'] ?>" id-setor="<?php echo $colab['ID_SETOR'] ?>" setor="<?php echo $colab['SETOR'].' '.$colab['NOME']; ?>">
                <?php echo $colab['SETOR'].' '.$colab['NOME']; ?></td>
            <td><?php echo $colab['PERFIL']; ?></td>
            
        </tr>
        
        <?php } ?>
        
    </table>
    
</small></small>
</div>