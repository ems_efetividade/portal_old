<?php// require_once('app/view/header.php'); ?>
<?php require_once('app/view/admin/includes.php'); ?>

<style>
    .table.label {display: block; padding:4px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <h3><span class="glyphicon glyphicon-file"></span> Arquivo de Notas Fiscais</h3>
            <hr>
            
            
            <form class="form-inline ">
                <div class="form-group form-group-sm">
                  <?php echo $fatura->comboMesFatura() ?>
                </div>
                <div class="form-group">
                  <input type="email" class="form-control" id="exampleInputEmail2" placeholder="jane.doe@example.com">
                </div>
                <button type="submit" class="btn btn-default">Send invitation</button>
            </form>
            
            <table class="table table-condensed table-striped" style="font-size: 11px;">
                <tr>
                    <th>Data Despesa</th>
                    <th>Fatura</th>
                    <th>Setor</th>
                    <th>Estabelecimento</th>
                    <th>Nota Fiscal</th>
                </tr>
                <?php foreach($nfs as $nf) { ?>
                <tr>
                    <td><?php echo $nf['DATA_LANCAMENTO'] ?></td>
                    <td><?php echo $nf['MES_FATURA'] ?></td>
                    <td><?php echo $nf['SETOR'] ?></td>
                    <td><?php echo $nf['ESTABELECIMENTO'] ?></td>
                    
                    
                    <td>
                        <a class="btnVerNF" data-file="<?php echo  appFunction::arquivoNF($nf['ARQUIVO_NF_MD5']);  ?>" data-type="<?php echo $nf['TIPO'] ?>" href="#">
                            <?php echo substr($nf['ARQUIVO_NF'], 0, 40) ?>
                        </a>
                    </td>
                    <td><?php echo appFunction::formatarTamanho(filesize(fileFolderRoot.$nf['ARQUIVO_NF_MD5'])) ?></td>
                </tr>
                <?php } ?>
            </table>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalVisualizarNF"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-ok-circle"></span> Confirmar Recebimento de Fatura</h5>
            </div>
            
            <div class="modal-body">
                <object id="obj" type="#"  data="#"  width="100%" hseight="400" >
                    <a href="#">Ver PDF</a> <-- Para navegadores que não suportam object -->
                </object>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarConfirmacao"><span class="glyphicon glyphicon-ok"></span> Confirmar Recebimento</a>
            </div>
            
        </div>
    </div>
</div>

<script>

$('.btnVerNF').unbind().click(function(e) {
    var arquivo = $(this).data('file');
    var tipo = $(this).data('type');
    var height = '';
    
    
    
    if(tipo == 'application/pdf') {
        height = '400px';
    }
    
    $('#obj').attr('data', arquivo);
    $('#obj').attr('type', tipo);
    $('#obj').attr('height', height);
    
    $('#modalVisualizarNF').modal('show');
});

</script>