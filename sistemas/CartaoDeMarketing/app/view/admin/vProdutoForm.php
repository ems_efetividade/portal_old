
<form id="formProduto" action="<?php echo AppConf::root ?>admin/produtoSalvar" method="post">

    
    <input type="hidden" name="txtIdProduto" value="<?php print $produto->getIdProduto(); ?>">   

    <div class="row">

        <div class="col-lg-12">
            <div class="form-group">
                <label for="exampleInputEmail1">Descrição do Produto</label>
                <input type="text" class="form-control input-sm" name="txtDescricao" value="<?php echo $produto->getProduto() ?>">
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <label for="exampleInputEmail1">Descrição do Produto</label>
                <select class="form-control input-sm" name="cmbBU" id="cmbBU">
                    <option value="0"></option>
                    <?php foreach($BUS as $bu) { ?>
                     <option value="<?php echo $bu['ID_UN_NEGOCIO'] ?>" <?php echo (($produto->getIdBU() == $bu['ID_UN_NEGOCIO']) ? 'selected' : '') ?>><?php echo $bu['EMPRESA'] ?> - <?php echo $bu['UN_NEGOCIO'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </div>

    </div>

</form>