<?php require_once('app/view/admin/includes.php'); ?>

<style>

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            <h3><span class="glyphicon glyphicon-credit-card"></span> Projetos </h3>
            <hr>
            
            <p>
                <button class="btn btn-success" data-toggle="modal" data-target="#modalEditarProjeto" data-id="0">
                    <span class="glyphicon glyphicon-plus"></span>
                    Novo Projeto
                </button>
            </p>
            
            <table class="table table-condensed table-striped">
                <tr>
                    <th>Projeto</th>
                    <th></th>
                </tr>
                
                <?php foreach($projetos as $projeto) { ?>
                <tr>
                    <td><small><?php echo $projeto->getDescricao() ?></small></td>
                    <td class="text-right">
                        <button class="btn btn-warning btn-xs btnEditar" data-toggle="modal" data-target="#modalEditarProjeto" data-id="<?php echo $projeto->getIdProjeto() ?>"><small>Editar</small></button>
                        <button class="btn btn-danger btn-xs btnExcluir" data-toggle="modal" data-target="#modalExcluirProjeto"  data-id="<?php echo $projeto->getIdProjeto() ?>"><small>Excluir</small></button>
                    </td>
                </tr>
                <?php } ?>    
            </table>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalEditarProjeto"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Projeto</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarProjeto"><span class="glyphicon glyphicon-ok"></span> Salvar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalExcluirProjeto"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Projeto</h5>
            </div>
            
            <div class="modal-body">
                <h4>Deseja excluir esse projeto?</h4>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-danger btn-sm" id="btnExcluirProjeto"><span class="glyphicon glyphicon-remove"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {
    $('#modalEditarProjeto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        
        formulario = requisicaoAjax('<?php echo AppConf::root ?>admin/projetoForm/'+id);
        $('#modalEditarProjeto .modal-body').html(formulario);
        
        $('#btnSalvarProjeto').unbind().click(function(e) {
            //$('#formCartao').submit();
            
            $.ajax({
                type: "POST",
                url: $('#formProjeto').attr('action'),
                data: $('#formProjeto').serialize(),
                success: function(html) {
                    if(html != '') {
                        msgBox(html);
                    } else {
                        location.reload();
                    }
                }
            });
            
            
        });
        
        
        
    });
    
    
    $('#modalExcluirProjeto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
                
        $('#btnExcluirProjeto').unbind().click(function(e) {
            //$('#formCartao').submit();
            $.ajax({
                type: "POST",
                url: '<?php echo AppConf::root ?>admin/projetoExcluir/'+id,
                success: function(html) {
                    if($.trim(html) != '') {
                        msgBox(html);
                    } else {
                        location.reload();
                    }
                }
            });
            
            
        });
        
        
        
    });
   
});
</script>
