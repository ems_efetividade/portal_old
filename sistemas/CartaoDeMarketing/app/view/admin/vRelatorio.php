<?php require_once('includes.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            
            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-download"></span> Exportar Base </h3>
                    
                    
                    
                </div>
                
                <div class="col-lg-6 text-right">
                   
                    
                    
                    
                </div>
            </div>
            
            <hr>
            
            <div class="row">
                <div class="col-lg-6 col-md-offset-3">


                    <div class="row">
                        <div class="col-lg-6">
                            <small>Mes Inicial</small>
                            <?php echo $fatura->comboMesFatura(0, 'comboMesDE'); ?>
                        </div>

                        <div class="col-lg-6">
                        <small>Mes Final</small>
                        <?php echo $fatura->comboMesFatura(0, 'comboMesATE'); ?>
                        </div>
                    </div>

                    
                </div>
                
                <div class="col-lg-6 col-md-offset-3">
                    
                    
                    <br>
                    
                    <a href="#" option="1" linha="0" class="exportarRelatorio btn btn-default btn-block"><span class="glyphicon glyphicon-file"></span> Base de conferência</a>
                    <a href="#" option="2" linha="0" class="exportarRelatorio btn btn-default btn-block"><span class="glyphicon glyphicon-file"></span> Listagem de Lançamentos</a>
                    <!--                    <a href="#" option="3" linha="0" class="exportarRelatorio btn btn-default btn-block"><span class="glyphicon glyphicon-file"></span> Listagem Move/Distrital</a>-->
                    
                    
                    
                </div>
                
                <div class="col-lg-6 col-md-offset-3">
                    <p></p>
                    <div class="btn-group" style="width: 100%" >
                        <button type="button"  class="btn btn-default btn-block dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <span class="glyphicon glyphicon-file"></span> Listagem Move/Distrital <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu btn-block">
                            <?php foreach($linhas as $linha) { ?>
                          <li><a href="#" option="3" linha="<?php echo $linha['ID_LINHA'] ?>" class="exportarRelatorio"><small><?php echo $linha['NOME'] ?></small></a></li>
                            <?php } ?>
                        </ul>
                      </div>
                </div>
            </div>
            
            
        </div>
    </div>
</div>

<script>

$(document).ready(function(e) {
    
    $('.exportarRelatorio').click(function(e) {
        url = '<?php echo AppConf::root ?>admin/exportarRelatorio/' + $(this).attr('option')+'/'+$('select[name="comboMesDE"]').val()+'/'+$('select[name="comboMesATE"]').val()+'/'+$(this).attr('linha');
        //alert(url);
        location.href = url;

        /*
          $.ajax({
            type: "POST",
            url: "<?php echo AppConf::root ?>admin/exportarRelatorio/",
            data: {
                opcao: $(this).attr('option'),
                linha: $(this).attr('linha'),
                cmbDE: $('select[name="comboMesDE"]').val(),
                cmbATE: $('select[name="comboMesATE"]').val()
            },
            success: function (response) {
                
            }
        });
        
         */
    });
        
    
    
});

</script>