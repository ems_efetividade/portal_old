<?php 
    require_once('app/view/header.php'); 
    $marketing[] = $fatura->lancamento->listarPorTipo(1);
    $marketing[] = $fatura->lancamento->listarPorTipo(2);
    $outros = $fatura->lancamento->listarPorTipo(0);
?>

<script>
$(document).ready(function(e){
    
    
    if(<?php echo $fatura->getDespesasPendentes() ?> == 0) {
        //$('#modalAprovarFatura').modal('show');
    }

    $('.btnMarcar').unbind().click(function(e){
         $('.chkDespesa').trigger('click');
    });
    
    $('#modalSetarStatus').on('show.bs.modal', function (event) {
        checked = $('input:checkbox:checked').length;
        
        if(checked > 0) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var idStatus = button.data('value'); // Extract info from data-* attributes
       $('input[name="txtIdStatus"]').val(idStatus);
        } else {
            
            msgBox('Selecione ao menos uma despesa');
            return false;
        }
   });
   
    $('#modalVisualizarDespesa').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>lancamento/visualizarLancamento/'+idLancamento);
        $('#modalVisualizarDespesa .modal-body').html(formulario); 
        
    });    
    
$('#btnSalvarDespesa').unbind().click(function(e){
    
    $('#formStatus').unbind().submit();
    
//            $.ajax({
//                type: "POST",
//                url: '<?php print AppConf::root ?>despesa/salvarStatusDespesa/'+funcao,
//                data:$('#formStatus').serialize(),
//                success: function(e) {
//                    if(e == "") {
//                        location.reload();
//                   } else {
//                       alert(e);
//                   }
//                }
//            });
        });    
    
});   

</script>    
<form id="formStatus" action="<?php print AppConf::root ?>aprovacao/aprovarDespesa" method="post">
<input type="hidden" name="txtIdStatus" value="" />
<div class="container">


    
    
    <div class="row">
        <div class="col-lg-7">
            <h1 class="roboto">Aprovar Fatura </h1>
        </div>
        <div class="col-lg-5">
            <?php if($fatura->getDespesasPendentes() == 0) { ?>

            <button type="button" class="btn btn-lg btn-success pull-right" data-toggle="modal" data-target="#modalAprovarFatura"><small> <span class="glyphicon glyphicon-ok"></span> Finalizar Lançamento</small></button>

    <?php } ?>
        </div>
    </div> 
    
    <hr>
    



    <div class="row">
    
        <div class="col-lg-7">
            <p>
                <button type="button" class="btn btn-default btn-sm btnMarcar"><span class="glyphicon glyphicon-check" ></span>&nbsp;Marcar/Desmarcar Todos</button>
                <button type="button" class="btn btn-success btn-sm btnAprovar" data-value="2" data-function="0" data-toggle="modal" data-target="#modalSetarStatus"><span class="glyphicon glyphicon-ok"></span>&nbsp;Aprovar Selecionados</button>
                <button type="button" class="btn btn-danger btn-sm btnReprovar"  data-value="3" data-function="0" data-toggle="modal" data-target="#modalSetarStatus"><span class="glyphicon glyphicon-remove"></span>&nbsp;Reprovar Selecionados</button>
            </p>
        </div>
        <div class="col-lg-5">
            
                       
            
        </div>
    
    </div>
    
    

    <div class="panel panel-default">
        <div class="panel-body">
            <?php if(count($marketing) > 0) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><span class="glyphicon glyphicon-ok"></span> Despesas do Cartão</strong>
                        </div>
                    </div>
                    <small>
                        <table class="table table-condensed table-bordered table-hover table-striped">
                           <tr class="active">
                                <th width="1%"></th>
                                <th width="5%">Data</th>
                                <th width="30%">Estabelecimento</th>
                                <th width="10%" class="text-center">Valor</th>
                                <th class="text-center">Tipo</th>
                                <th class="text-center">Ação</th>
                                <th>Projeto</th>
                                <th class="text-right">Nº Médicos Part.</th>
                                <th class="text-center">Status Aprovação</th>
                                <th class="text-center">Descrição Aprovação</th>
                                <th class="text-right"></th>
                            </tr>

                            <?php foreach($marketing as $mkt) { ?>    
                            <?php foreach($mkt as $i => $lancamento) { ?>    
                            <tr>
                                <td class="text-center"><input type="checkbox" class="chkDespesa" name="chkIdDespesa[]" value="<?php print $lancamento->despesa->getIdDespesa() ?>"></td>
                                <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
                                <td><?php print $lancamento->getEstabelecimento(); ?></td>
                                <td><?php print $lancamento->getValor(); ?></td>
                                <td><?php print $lancamento->despesa->tipoDespesa->getDescricao(); ?></td>
                                <td><?php print $lancamento->despesa->acao->getDescricao(); ?></td>
                                <td><?php print $lancamento->despesa->projeto->getDescricao(); ?></td>
                                <td><?php print $lancamento->despesa->medico->getNumMedicos(); ?></td>
                                
                                <td class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
                                <td><?php print $lancamento->despesa->status->getObservacao(); ?></td>
                                <td class="text-center">
                                    <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalVisualizarDespesa" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } ?>
                        </table>
                    </small>
                </div>
            </div>
            <?php } ?>
           <?php if(count($outros) > 0) { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><span class="glyphicon glyphicon-ok"></span> Despesas do Cartão</strong>
                        </div>
                    </div>
                    <small>
                        <table class="table table-condensed table-bordered table-hover table-striped">
                            <tr class="active">
                                <th width="1%"></th>
                                <th width="5%">Data</th>
                                <th width="30%">Estabelecimento</th>
                                <th width="10%" class="text-center">Valor</th>
                                <th>Tipo</th>
                                <th>Descricao</th>
                                <th class="text-center">Status Aprovação</th>
                                <th class="text-center"></th>
                                <th class="text-center"></th>
                            </tr>

                            <?php foreach($outros as $i => $lancamento) { ?>    
                            <tr>
                                <td class="text-center"><input type="checkbox" class="chkDespesa" name="chkIdDespesa[]" value="<?php print $lancamento->despesa->getIdDespesa() ?>"></td>
                                <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
                                <td><?php print $lancamento->getEstabelecimento(); ?></td>
                                <td class="text-center"><?php print $lancamento->getValor(); ?></td>
                                <td><?php print $lancamento->despesa->tipoDespesa->getDescricao(); ?></td>
                                <td><?php print $lancamento->despesa->getDescricao(); ?></td>
                                <td class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
                                <td><?php print $lancamento->despesa->status->getObservacao(); ?></td>
                                <td class="text-center">
<!--                                    <a href="#" class="btn btn-success btn-xs" data-toggle="modal" data-target="#modalMarketing" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-thumbs-up"></span>&nbsp;</a>
                                    <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirDespesa" data-id-despesa="<?php print $lancamento->despesa->getIdDespesa(); ?>"><span class="glyphicon glyphicon-thumbs-down"></span>&nbsp;</a>-->
                                    <a href="#" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </small>
                </div>
            </div>
            <?php } ?>
            
            <div class="row">
                <div class="col-lg-12">
                    <?php print $tabelaOutros; ?>
                </div>
            </div>
            
            
            
        </div>
    </div>
    
    <?php // if(count($lancamentos) == 0) { ?>
    <?php if($fatura->getDespesasPendentes() == 0) { ?>
    <div class="panel panel-default">
        <div class="panel-body text-right">
            <button type="button" class="btn btn-lg btn-success" data-toggle="modal" data-target="#modalAprovarFatura"><small> <span class="glyphicon glyphicon-ok"></span> Finalizar Lançamento</small></button>
        </div>
    </div>
    <?php } ?>

</div>





<div class="modal fade" id="modalSetarStatus" style="z-index: 10000" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><strong><span class="glyphicon glyphicon-usd"></span> Status da Despesa</strong></h5>
            </div>
            
            <div class="modal-body ">
                <small>
                <div class="form-group">
                      <label for="exampleInputEmail1">Observação para a(s) despesa(s) selecionada(s).</label>
                      <textarea class="form-control input-sm" rows="5" name="txtObservacao"><?php print $detalhe ?></textarea>
                </div>
                </small>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary btn-sm" id="btnSalvarDespesa"><span class="glyphicon glyphicon-ok"></span> Salvar Status</button>
            </div>
            
        </div>
    </div>
</div>
   
   


<div class="modal fade" id="modalDetalheDespesa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-default">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-usd"></span> Detalhe da Despesa</h5>
            </div>
            
            <div class="modal-body max-height">
                
            </div>
            
            <div class="modal-footer">
                
                
                <button type="button" class="btn btn-success btn-sm" data-value="2" data-toggle="modal" data-function="1" data-target="#modalSetarStatus"><span class="glyphicon glyphicon-ok"></span> Aprovar</button>
                <button type="button" class="btn btn-danger btn-sm"  data-value="3" data-toggle="modal" data-function="1" data-target="#modalSetarStatus"><span class="glyphicon glyphicon-remove"></span> Reprovar</button>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
            </div>
            
        </div>
    </div>
</div>   
   
   
   
</form>


<div class="modal fade" id="modalAprovarFatura"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body text-center">
                <h4 class="text-primary"><strong>Deseja salvar as aprovações/reprovações feitas nessa fatura?</strong></h4>
                <h5 class="text-danger">Essa operação não poderá ser desfeita!</h5>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="<?php print AppConf::root ?>aprovacao/aprovarFatura/<?php print $fatura->getIdFatura() ?>" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-ok"></span> Salvar Aprovações</a>
            </div>
            
        </div>
    </div>
</div>






<!--MODAL FORM MARKETING-->
<div class="modal fade" id="modalVisualizarDespesa" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-folder-open"></span> Visualizar Despesa</h5>
            </div>
            <div class="modal-body max-height">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
<!--                <button type="button" class="btn btn-primary" id="btnSalvarDespesa"><span class="glyphicon glyphicon-ok"></span> Salvar</button>-->
            </div>
        </div>
    </div>
</div>
