<?php
    require_once('app/view/header.php');
    //require_once('plugins/mail/cMail.php');
?>
<script>
$(document).ready(function(e) {
    
    $('.loading').hide();
    
    
    $('#modalAprovarFaturaLote').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget) // Button that triggered the modal
        var idSetor = (button.data('id-setor') === undefined) ? 0 : parseInt(button.data('id-setor')); // Extract info from data-* attributes
        
        //SELECIONA A FOTO DA NOTA FISCAL
        $('#btnSalvarTudo').unbind().click(function(e){
            url = '<?php echo AppConf::root ?>aprovacao/aprovarTudo/'+idSetor+'/'+<?php echo $mesFatura ?>;
            alert(url);
            location.href = url;
        });
    });
    
    
    $('#cmbMesFatura').change(function(e) {
        modalAguarde('show');
        url = '<?php echo AppConf::root ?>aprovacao/listarFaturaEquipe/0/'+$('#cmbMesFatura').val()+'/0';
        location.href = url;

    });
    
    $('#btnMarcar').click(function(e){
        $('.chkDespesa').trigger('click');
    });  
    

    
    abrirNivel();
    
    
  
});

function abrirNivel() {
    $('.sub').unbind().click(function(e) {

        idSetor = $(this).attr('val');
        $('.loading', this).show();

        if($('#sub'+idSetor).html() == "") {
            
            //html = requisicaoAjax('<?php echo AppConf::root ?>aprovacao/somaFatura/'+idSetor);
            //$('#sub'+idSetor).html(html);
            
            $.ajax({
                type: "POST",
                url: '<?php echo AppConf::root ?>aprovacao/somaFatura/'+idSetor+'/0/'+$('#cmbMesFatura').val(),
                success: function(html) {
                   
                    $('#sub'+idSetor).html(html);
                     $('.loading').hide();
                    
                    
                    
                    
                    
                    
                    $('.subSoma').unbind().click(function(e) {
                        $('.loading', this).show();
                        idSetor = $(this).attr('val');
                        if($('#subSoma'+idSetor).html() == "") {

                            //html = requisicaoAjax('<?php echo AppConf::root ?>aprovacao/listarEquipe/'+idSetor);
                           // $('#subSoma'+idSetor).html(html);
                           $.ajax({
                                type: "POST",
                                url: '<?php echo AppConf::root ?>aprovacao/listarEquipe/'+idSetor+'/'+$('#cmbMesFatura').val(),
                                success: function(html) {

                                    $('#subSoma'+idSetor).html(html);
                                    $('.loading').hide();
                                    abrirNivel();
                                },
                                beforeSend: function (a) {
                                   //$('.subSoma .loading').html('aqui');
                                  // $('#subSoma'+idSetor).html('Aguarde');

                                }
                            });
                        } else {
                            $('#subSoma'+idSetor).html("");
                            $('.loading').hide();
                        }
                    
                    });
  
                },
                beforeSend: function (a) {
                
                   //$('#sub'+idSetor).html('aqui');
                }
            });
            
            
        } else {
           $('#sub'+idSetor).html(""); 
           $('.loading').hide();
        }
        
        
        
        
        abrirNivel();
    });
    
    
}
</script>
<style>
.tb-fatura {margin-bottom:0px;}
.tab1 {padding-left: 20px;}
.tab2 {padding-left: 40px;}
.tab3 {padding-left: 60px;}

.c1 {background-color: #f1f1f1;}
.c2 {background-color: #f4f4f4;}
.c3 {background-color: #f8f8f8;}

.border { border-bottom: 1px solid #ddd; }
.vcenter {
    display: inline-block;
    vertical-align: middle;
    float: none;
    padding-top: 3px;
    padding-bottom: 3px;
}
</style>

<div class="container">
<!--    <a href="<?php print AppConf::root ?>aprovacao/enviarEmail" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-ok"></span> Salvar Aprovações</a>-->
    
    
    <div class="row">
    <div class="col-lg-7">
        <h1 style="font-family:Roboto-Thin"><span class="glyphicon glyphicon-user"></span> Faturas da Equipe</h1>
    </div>
    
    <div class="col-lg-5">
        <div class="row"> &nbsp;
            
<!--            <div class="col-lg-12">
                <p>
                    <div class="input-group input-group-sm">
                      <input type="text" id="txtCriterioBusca" class="form-control" value="" placeholder="Pesquisar pelo Nome...">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button" id="btnPesquisar">Pesquisar</button>
                      </span>
                    </div> /input-group 
                </p>
              </div>-->
        </div>
    </div>
</div>
    <hr>
    
    <div class="row">
        
<!--        <div class="col-lg-4">
            <button class="btn btn-default btn-sm" type="button" id="btnMarcar"><span class="glyphicon glyphicon-check"></span> Marcar/Desmarcar Todos</button>
            <button class="btn btn-success btn-sm" type="button" id="btnAprovarLote"><span class="glyphicon glyphicon-ok"></span> Aprovar</button>
            <button class="btn btn-danger  btn-sm" type="button" id="btnReprovarLote"><span class="glyphicon glyphicon-remove"></span> Reprovar</button>
        </div>-->

<!--        <div class="col-lg-3">
            <div class="form-group form-group-sm">
                 <?php //echo  $colaborador->comboPerfil($perfil); ?>
            </div>
        </div>
        -->
        <div class="col-lg-2">
            <div class="form-group form-group-sm">
                <?php echo  $fatura->comboMesFatura($mesFatura); ?>
            </div>
        </div>   
        
<!--        <div class="col-lg-3">
            <div class="form-group form-group-sm">
                <?php echo  $fatura->statusFatura->statusAtual->comboStatusFatura($status); ?>
            </div>
        </div>        -->

    </div>    
    
    <div class="pdanel padnel-default">
        <form action="<?php print AppConf::root ?>aprovacao/salvarTudo" method="post">        
        <div class="padnel-body">
            
            
            
            <small>
<!--                <table class="table table-stripsed table-condensed">
                    <tr >
                        <th width="30%">Cartão</th>
                        <th width="10%">Mês Fatura</th>
                        <th width="5%">Total da Fatura</th>
                        <th width="5%">Market</th>
                        <th width="5%">Pessoal</th>
                        <th width="5%">Outras</th>
                        <th width="5%">Não Justificadas</th>
                        <th width="20%" class="text-center">Status</th>
                        <th width="10%">Ação</th>
                    </tr>
</table> --><strong>
                <div class="row border" >
                    <div class="col-lg-3">Setor</div>
                    <div class="col-lg-5">
                        <div class="row">
                            <div class="col-lg-2"><p class="text-center">Mês Fatura</p></div>
                            <div class="col-lg-2"><p class="text-center">Total Fatura</p></div>
                            <div class="col-lg-2"><p class="text-center">Market</p></div>
                            <div class="col-lg-2"><p class="text-center">Pessoal</p></div>
                            <div class="col-lg-2"><p class="text-center">Outras</p></div>
                            <div class="col-lg-1 text-center"><p >Não Justificadas</p></div>
                       </div>
                    </div>
                    <div class="col-lg-2 text-center"><p class="text-center">Status</p></div>
                    <div class="col-lg-1 text-center"><p class="text-center">Receb. Fisico</p></div>
                    <div class="col-lg-1 text-right"><p class="text-center">&nbsp;</p></div>
                </div></strong>
                <?php echo $lista; ?>
            </small>
            <br>

    
        </div>
        </form>
    </div>  
    
</div>





<div class="modal fade" id="modalAprovarFaturaLote"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body text-center">
                <h4 class="text-primary"><strong>Deseja aprovar TODAS as faturas da equipe?</strong></h4>
<!--                <h5 class="text-danger">Essa operação não poderá ser desfeita!</h5>-->
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarTudo"><span class="glyphicon glyphicon-ok"></span> Salvar Aprovações</a>
            </div>
            
        </div>
    </div>
</div>