<div class="row">
    <div class="col-lg-8">
        <h2 class="roboto" style="margin-top: 0px;"><?php print $lancamento->getDataLancamento() ?> <?php print $lancamento->getEstabelecimento() ?></h2>
    </div>
    
    <div class="col-lg-4 text-right">
        <h2 class="roboto" style="margin-top: 0px;">R$ <?php print $lancamento->getValor() ?></h2>
    </div>
</div>

<hr>




<div class="row">
    <div class="col-lg-7">
        <p class="lead">Dados da Despesa</p>
        <small>
            <table class="table table-condensed table-bordered table-striped">
                <tr>
                    <td><strong>Status:</strong></td>
                    <td>
                        <?php print $lancamento->despesa->status->getStatus() ?>
                        <small><?php print $lancamento->despesa->status->getObservacao() ?></small>
                    </td>
                </tr>            
                <tr>
                    <td><strong>Tipo de Despesa:</strong></td>
                    <td><?php print $lancamento->despesa->tipoDespesa->getDescricao() ?></td>
                </tr>
                <tr>
                    <td><strong>Distrito:</strong></td>
                    <td><?php print $lancamento->despesa->getSetorPag().' '.$lancamento->despesa->getColaboradorPag() ?></td>
                </tr>                

                <?php if ($lancamento->despesa->getDespesaMkt() == 1) { ?>

                <tr>
                    <td><strong>Projeto:</strong></td>
                    <td><?php print $lancamento->despesa->projeto->getDescricao() ?></td>
                </tr>       

                <tr>
                    <td><strong>Ação:</strong></td>
                    <td><?php print $lancamento->despesa->acao->getDescricao() ?></td>
                </tr>

                <tr>
                    <td><strong>Cidade:</strong></td>
                    <td><?php print $lancamento->despesa->cidade->getCidade() ?>/<?php print $lancamento->despesa->cidade->getUF() ?></td>
                </tr>   
                <tr>
                    <td><strong>Planjeado:</strong></td>
                    <td>R$ <?php print $lancamento->despesa->getPlanejado() ?></td>
                </tr>  
                <tr>
                    <td><strong>Realizado:</strong></td>
                    <td>R$ <?php print $lancamento->despesa->getRealizado() ?></td>
                </tr>              
                <tr>
                    <td><strong>Local do Evento:</strong></td>
                    <td><?php print $lancamento->despesa->getLocalEvento() ?></td>
                </tr>  

                <?php } ?>
                
                <?php if ($lancamento->despesa->getIdTipoDespesa() == 5 || $lancamento->despesa->getIdTipoDespesa() == 4) { ?>


                <tr>
                    <td><strong>Cidade:</strong></td>
                    <td><?php print $lancamento->despesa->cidade->getCidade() ?>/<?php print $lancamento->despesa->cidade->getUF() ?></td>
                </tr>   
                <tr>
                    <td><strong>Local do Evento:</strong></td>
                    <td><?php print $lancamento->despesa->getLocalEvento() ?></td>
                </tr> 
                <?php } ?>
                
                <tr>
                    <td><strong>Descrição/Observação:</strong></td>
                    <td><?php print $lancamento->despesa->getDescricao() ?></td>
                </tr>  
                
                <?php if($lancamento->despesa->getJustificativaFimSemana() != '') { ?>
                <tr>
                    <td><strong>Justificativa Fim de Semana:</strong></td>
                    <td><?php print $lancamento->despesa->getJustificativaFimSemana() ?></td>
                </tr>
                <?php } ?>
            </table>
        </small>
    </div>
    <div class="col-lg-5">
        
        
        <?php if ($lancamento->despesa->getDespesaMkt() == 1) { ?>
<div class="row">
    <div class="col-lg-12">
        <p class="lead">Produtos Participantes</p>
<small>
        <table class="table table-condensed table-bordered table-striped">
            <tr>
                <td><strong>Produto:</strong></td>
                <td><strong>% Participação:</strong></td>
            </tr>
            <?php 
                $produtos = $lancamento->despesa->produto->listar();
                $soma = 0;
                foreach($produtos as $produto) {              
            ?>
            <tr>
                <td><?php print $produto->produto->getProduto() ?></td>
                <td><?php print $produto->getParticipacao() ?></td>
            </tr>       
                <?php } ?>
                     
        </table>
        </small>
        
        
    </div>
    <div class="col-lg-12">
        
        <p class="lead">Médicos/PDV's Participantes</p>
<small>
        <table class="table table-condensed table-bordered table-striped">
            <tr>
                <td><strong>Médico/CNPJ</strong></td>
                <td><strong>Nome</strong></td>
            </tr>
            <?php
                $medicos = $lancamento->despesa->medico->listar();
                foreach($medicos as $medico) {
            ?>
            <tr>
                <td><?php print $medico->getCRM() ?></td>
                <td><?php print $medico->getNome() ?></td>
            </tr>       
                <?php } ?>
                     
        </table>
        </small>
        
    </div>
</div>
<?php } ?>
        
        
        
<?php if ($lancamento->despesa->getIdTipoDespesa() == 5 || $lancamento->despesa->getIdTipoDespesa() == 4) { ?>
        <p class="lead">Participantes</p>
        <table class="table table-condensed table-bordered table-striped">
        <?php foreach ($lancamento->despesa->moveParticipante->listar() as $part) { ?>
            <tr>
                <td><?php print $part->getId() ?></td>
                <td><?php print $part->getSetor().' '. $part->getNome() ?></td>
            </tr>
        <?php } ?>
        </table>
    
<?php } ?>
        
        
        
        
<!--        <div class="panel panel-default">
            <div class="panel-body">
                
            </div>
        </div>-->
    </div>
    
    
    
    
</div>


<?php if ($lancamento->despesa->getIdTipoDespesa() == 5 || $lancamento->despesa->getIdTipoDespesa() == 4) { ?>
        
        
        <p class="lead">Investimentos</p>
        <table class="table table-condensed table-bordered table-stsriped">
            <tr class="active">
                <th></th>
                <th class="text-center">QTD</th>
                <th class="text-center">TOTAL</th>
                <th class="text-right">MEDIA</th>
            </tr>
        <?php $total = 0; foreach ($categorias as $cat) { $dados = $cat->listarResposta($lancamento->despesa->getIdDespesa());   ?>
            <tr class="active">
                <td><b><?php print $cat->getDescricao() ?></b></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-right"><b><?php print appFunction::formatarMoedaBRL($dados[1]['TOTAL'],2) ?></b></td>
            </tr>
            <?php foreach ($cat->listarSub() as $cat2) { $dados2 = $cat2->listarResposta($lancamento->despesa->getIdDespesa()); $total = $total + ($dados2[1]['QTD']*$dados2[1]['VALOR']) ?>
                <tr>
                    <td><small>&nbsp;&nbsp;&nbsp;<?php print $cat2->getDescricao() ?></small></td>
                    <td class="text-center"><small><?php print appFunction::formatarMoedaBRL($dados2[1]['QTD'],0) ?></small></td>
                    <td class="text-center"><small><?php print appFunction::formatarMoedaBRL($dados2[1]['VALOR'],2) ?></small></td>
                    <td class="text-right"><small><?php print appFunction::formatarMoedaBRL($dados2[1]['TOTAL'],2) ?></small></td>
                </tr>
            <?php } ?>
            
        <?php } ?>
<!--            <tr class="active">
                <td><b>TOTAL GERAL</b></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-right"><b><?php print appFunction::formatarMoedaBRL($total,2) ?></b></td>
            </tr>-->
        </table>
<?php } ?>

<div class="row">
    <div class="col-lg-12">
    <p class="lead">Cupom Fiscal</p>
    
    <?php $NFs = $lancamento->despesa->notaFiscal->listar();
        foreach($NFs as $nf) { ?>
        <?php print $nf->getArquivoNF() ?>
        <object type="<?php print $nf->getTipo() ?>"  data="<?php echo appFunction::arquivoNF($nf->getArquivoMD5()) ?>"  width="100%" height="400" >
            <a href="<?php echo appFunction::arquivoNF($nf->getArquivoMD5()) ?>">Ver PDF</a> <-- Para navegadores que não suportam object -->
        </object>
        <br>
        <br>
        <br>
    <?php } ?>
<!--        <table class="table table-condensed table-bordered table-striped">
                <tr>
                    <td><img class="img-responsive" src="<?php print appFunction::arquivoNF($lancamento->despesa->getArquivoNFMD5()) ?>"</td>
                    <td>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                         <?php print $lancamento->despesa->getArquivoNFMD5().'-'.appFunction::arquivoNF($lancamento->despesa->getArquivoNFMD5()) ?> 
                        
                    </td>
                </tr>
        </table>-->
        </div>
    </div>




