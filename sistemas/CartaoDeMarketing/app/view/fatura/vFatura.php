<?php require_once('app/view/header.php'); ?>

<script>
$(document).ready(function(e) { 
    $('#modalMove').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>despesa/formularioMove/'+idLancamento);
        
        $('#modalMove .modal-body').html(formulario); 
    });

    $('#modalMarketing').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        var idDespesa = (button.data('id-despesa') === undefined) ? 0 : parseInt(button.data('id-despesa')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>despesa/formularioMarketing/'+idLancamento+'/'+idDespesa);
        $('#modalMarketing .modal-body').html(formulario); 
    });
    
    $('#modalDistrital').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        var idDespesa = (button.data('id-despesa') === undefined) ? 0 : parseInt(button.data('id-despesa')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>despesa/formularioDistrital/'+idLancamento+'/'+idDespesa);
        $('#modalDistrital .modal-body').html(formulario); 
    });    
    
    $('#modalOutros').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>despesa/formularioOutros/'+idLancamento);
        $('#modalOutros .modal-body').html(formulario); 
        
        //SELECIONA A FOTO DA NOTA FISCAL
        $('#btnSelNF').unbind().click(function(e) {
            $('#arqNF').unbind().click();
            $('#arqNF').unbind().change(function(e) {
                arquivo = $(this).val();
                arquivo = arquivo.split(/(\\|\/)/g).pop();
                $('input[name="txtArqNF"]').val(arquivo);
            });
        });
    });    
    
    
    $('#modalVisualizarDespesa').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print AppConf::root ?>lancamento/visualizarLancamento/'+idLancamento);
        $('#modalVisualizarDespesa .modal-body').html(formulario); 
        
    });    
    
    $('#modalExcluirDespesa').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idDespesa = (button.data('id-despesa') === undefined) ? 0 : parseInt(button.data('id-despesa'));
        
        $('#btnExcluirDespesa').attr('href', '<?php print AppConf::root ?>despesa/excluir/'+idDespesa);
    });
    
    
    $('#btnSalvarMove').unbind().click(function(e) {
        $.ajax({
            type: "POST",
            url: $('#formMove').attr('action'),
            data: new FormData($('#formMove')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                //modalAguarde('hide');
               //alert(retorno);
                //console.log(retorno);
                if(retorno.trim() != '') {
                    
                    modalAguarde('hide');
                    msgBox(retorno);
                } else {
                    location.reload();
                }

            },
            beforeSend: function (a) {
               modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
            }
        });

    });
    
    
    $('#btnSalvarDespesa').unbind().click(function(e) {
        $.ajax({
            type: "POST",
            url: $('#formMarketing').attr('action'),
            data: new FormData($('#formMarketing')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                
                if(retorno.trim() != '') {
                    
                    modalAguarde('hide');
                    msgBox(retorno);
                } else {
                    location.reload();
                }

            },
            beforeSend: function (a) {
               modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
            }
        });

    });
    
    
    $('#btnSalvarOutros').unbind().click(function(e) {
        $.ajax({
            type: "POST",
            url: $('#formOutros').attr('action'),
            data: new FormData($('#formOutros')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                (retorno != '') ? msgBox(retorno) : location.reload();
            },
            beforeSend: function (a) {
               //$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
            }
        });

    });  
    
    
    if(<?php echo count($lancamentos) ?> == 0 && <?php echo $fatura->getDespesasPendentes() ?> > 0 && <?php echo $fatura->statusFatura->getIdStatusFatura() ?> == 1) {
        $('#modalSalvarFatura').modal('show');
    }

});
</script>

<div class="container-fluid">

    <div class="row">
        <div class="col-lg-7">
            <h1 class="roboto">Justificar Fatura <!-- <a href="" class="btn btn-xs btn-default pull-right">Voltar</a>--></h1>
        </div>
        <div class="col-lg-5">
            <?php  /* echo $fatura->statusFatura->statusAtual->getIdStatusFatura();  */ if (count($lancamentos) == 0 && ($fatura->getDespesasPendentes() > 0 || $fatura->statusFatura->getIdStatusFatura() == 1)) { ?>

            <button class="btn btn-lg btn-success pull-right" data-toggle="modal" data-target="#modalSalvarFatura"><small> <span class="glyphicon glyphicon-ok"></span> Finalizar Lançamento</small></button>

    <?php } ?>
        </div>
    </div> 
   
   
    <hr>
    
    
    <div style="padding-left:5px;padding-right: 5px;">
        <div class="row">
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-info">
                        <h5 class="text-center">Total da Fatura</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></b></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-info">
                        <h5 class="text-center">Despesas Marketing</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(1),2) ?></b></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-info">
                        <h5 class="text-center">Move/Distrital</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL(($fatura->lancamento->getTotalDespesa(5)+$fatura->lancamento->getTotalDespesa(4)),2) ?></b></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-warning">
                        <h5 class="text-center">Despesas Pessoais</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(2),2) ?></b></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-warning">
                        <h5 class="text-center">Não Reconhecidas</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(3),2) ?></b></h3>
                    </div>
                </div>
            </div>
            <div class="col-lg-2">
                <div class="panel panel-default">
                    <div class="panel-body text-danger">
                        <h5 class="text-center">Não Justificadas</h5>
                        <h3 class="text-center"><b>R$ <?php print appFunction::formatarMoedaBRL(($fatura->getTotalFatura()-($fatura->lancamento->getTotalDespesa(2)+$fatura->lancamento->getTotalDespesa(4)+$fatura->lancamento->getTotalDespesa(1)+$fatura->lancamento->getTotalDespesa(3))),2) ?></b></h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <?php  /* echo $fatura->statusFatura->statusAtual->getIdStatusFatura();  if (count($lancamentos) == 0 && ($fatura->statusFatura->getIdStatusFatura() == 1 || $fatura->statusFatura->getIdStatusFatura() == 4)) { */ ?>
    

    <div class="panel panel-default">
        <div class="panel-body">
            
            
            <?php if(count($lancamentos) > 0) { ?>
            
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <strong><span class="glyphicon glyphicon-ok"></span> Despesas do Cartão <?php echo appFunction::camuflarNroCartao($fatura->getNumCartao()); ?></strong>
                        </div>
                    </div>
                    <small>
                        <table class="table table-condensed table-bordered table-hover table-striped">
                            <tr class="active">
                                <th></th>
                                <th>Data</th>
                                <th>Estabelecimento</th>
                                <th class="text-center">Deb/Cred.</th>
                                <th class="text-center">Autorização</th>
                                <th>Valor</th>
                                <th class="text-right">Tipo de Despesa</th>
                            </tr>

                            <?php foreach($lancamentos as $i => $lancamento) { ?>    
                            <tr>
                                <td><?php print $i+1; ?></td>
                                <td><?php print $lancamento->getDataLancamento(); ?></td>
                                <td><?php print $lancamento->getEstabelecimento(); ?></td>
                                <td class="text-center"><?php print $lancamento->getTipoTransacao(); ?></td>
                                <td class="text-center"><?php print $lancamento->getAutorizacao(); ?></td>
                                <td><b><?php print $lancamento->getValor(); ?></b></td>
                                <td class="text-right">
                                    <button class="btn btn-xs btn-success" data-toggle="modal" data-target="#modalMove" data-id-lancamento="<?php print $lancamento->getIdLancamento() ?>"><small>Move/Distrital</small></button>
                                    <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#modalMarketing" data-id-lancamento="<?php print $lancamento->getIdLancamento() ?>"><small>Marketing</small></button>
                                    <button class="btn btn-xs btn-info" data-toggle="modal" data-target="#modalOutros" data-id-lancamento="<?php print $lancamento->getIdLancamento() ?>"><small>Outros</small></button>
<!--                                    <button class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modalDistrital" data-id-lancamento="<?php print $lancamento->getIdLancamento() ?>"><small>Outros</small></button>-->
                                </td>
                            </tr>
                            <?php } ?>
                        </table>
                    </small>
                </div>
            </div>
            <?php } ?>
            
            
            <div class="row">
                <div class="col-lg-12">
                    <?php print $tabelaMkt; ?>
                </div>
            </div>
            


            <div class="row">
                <div class="col-lg-12">
                    <?php print $tabelaOutros; ?>
                </div>
            </div>
            
            
            
        </div>
    </div>
    
    <?php  /* echo $fatura->statusFatura->statusAtual->getIdStatusFatura(); */ if (count($lancamentos) == 0 && ($fatura->statusFatura->getIdStatusFatura() == 1 || $fatura->statusFatura->getIdStatusFatura() == 4)) { ?>
    <div class="panel panel-default">
        <div class="panel-body text-right">
            <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#modalSalvarFatura"><small> <span class="glyphicon glyphicon-ok"></span> Finalizar Lançamento</small></button>
        </div>
    </div>
    <?php } ?>

</div>

<!--MODAL MOVE-->
<div class="modal fade" id="modalMove" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel">Move/Distrital</h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnSalvarMove"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
        </div>
    </div>
</div>



<!--MODAL FORM MARKETING-->
<div class="modal fade" id="modalMarketing" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static" >
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel">Despesa de Marketing</h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnSalvarDespesa"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
        </div>
    </div>
</div>

<!--MODAL FORM MARKETING-->
<div class="modal fade" id="modalVisualizarDespesa" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-folder-open"></span> Visualizar Despesa</h5>
            </div>
            <div class="modal-body max-height">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnSalvarDespesa"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
        </div>
    </div>
</div>





<div class="modal fade" id="modalOutros" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><strong><span class="glyphicon glyphicon-usd"></span> Outras Despesas / Excessões</strong></h4>
            </div>
            
            <div class="modal-body">

            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary btn-sm" id="btnSalvarOutros"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalExcluirDespesa" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
             
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="glyphicon glyphicon-trash    "></span> Excluir
            </div>
            
            <div class="modal-body">
                <h3>Deseja excluir essa despesa?</h3>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="<?php print AppConf::root ?>despesa/excluir/" class="btn btn-danger" id="btnExcluirDespesa"><span class="glyphicon glyphicon-ok"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>



<div class="modal fade" id="modalSalvarFatura" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><strong><span class="glyphicon glyphicon-usd"></span> Finalizar Lançamentos</strong></h5>
            </div>
            
            <div class="modal-body text-center">
                <h4>Deseja finalizar o lançamentos dessa fatura?</h4>
                <h5>Depois de salva, não será possível realizar alterações.</h5>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="<?php print AppConf::root ?>fatura/salvar/<?php print $fatura->getIdFatura() ?>" class="btn btn-primary btn-sm" id="btnSalvarOutros"><span class="glyphicon glyphicon-ok"></span> Salvar</a>
            </div>
            
        </div>
    </div>
</div>




<div class="modal fade" id="modalDistrital" role="dialog" aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel"><strong><span class="glyphicon glyphicon-usd"></span> Distrital</strong></h4>
            </div>
            
            <div class="modal-body">

            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary btn-sm" id="btnSalvarOutros"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
            
        </div>
    </div>
</div>





<div class="modal fade" id="modalDistrito" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel">Selecionar Distrito</h5>
      </div>
      <div class="modal-body">
<!--      	<p><input type="text" class="form-control input-sm" id="txtPesquisaDistrito" value=""></p>-->
          <div id="listaGerentes" style="max-height: 300px; overflow: auto;"></div>
      </div>
      <div class="modal-footer">
      
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-sm" id="btnSelecionarDistrito">Selecionar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-default">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><strong>Adicionar Médico</strong></h5>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-primary btn-sm">Selecionar</button>
      </div>
    </div>
  </div>
</div>


