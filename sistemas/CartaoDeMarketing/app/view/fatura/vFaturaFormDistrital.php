<?php
//Verifica se a data de lancamento é fim de semana. Sabado = 6 e Domingo = 0;
$fds = date("w", strtotime(appFunction::formatarData($dataNF)));
//$fds = 0;
$col = 12;
?>
<style>
    #listaDesc {
        position: absolute;
        width: 95%;
        z-index: 1000;
        height: 200px;
        
    }
    #listaDesc .panel-body {
        height: 200px;
        overflow:auto;
    }

    
    #listaDesc table {
        margin-bottom: 0px;
    }
    
    #listaDesc table td {
       cursor: pointer;
    }
</style>  
<script>

$('#btnListarDistrito').click(function(e) {
    
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>despesa/listaGerentes',
        success: function(tabela) {
            
            $('#modalDistrito #listaGerentes').html(tabela);
            
            $('#btnSelecionarDistrito').click(function(e) {
                checked = $('input[name=optionsRadios]:checked');
                texto = checked.attr('text');
                valor = checked.val();

                $('input[name="txtDistrito"]').val(valor);
                $('input[name="txtNomeDistrito"]').val(texto);

                $('#modalDistrito').modal('hide');
            });
            
            modalAguarde('hide');
            $('#modalDistrito').modal('show');
        },
        beforeSend: function (a) {
           modalAguarde('show');
        }
    });
});



$('select[name="cmbUF"]').change(function(e) {
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>cidade/comboCidade/'+$(this).val(),
        success: function(cidade) {
            $('#comboCidade').html(cidade);
        },
        beforeSend: function (a) {
           $('#comboCidade').html('<select class="form-control input-sm"><option>Listando...</option></select>');
        }
    });
});

$('#btnAddNF').click(function(e) {

    nome = $.now();
    

    file = '<input type="file" id="arqNF'+nome+'" name="arqNF[]" style="display: none" accept=".pdf" />';
    
    $('#formMarketing').append(file);
    $('#arqNF'+nome).click();
    $('#arqNF'+nome).change(function(e) {
        
        if(this.files[0].size <= 2000000) {
        
        
            arquivo = $(this).val().split(/(\\|\/)/g).pop();
            tr = '<tr><td>'+arquivo+'</td><td><button type="button" class="btn btn-xs btn-danger btnExcluirFile" file="arqNF'+nome+'"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
            $('#tbNF').append(tr);

            $('.btnExcluirFile').click(function(e) {
                fileExcluir = $(this).attr('file');

                $(this).closest('tr').remove();
                $('#'+fileExcluir).remove();
            });
        } else {
            msgBox('Tamanho de arquivo excedido. Máximo 2 MB!');
        }
    });
});

$('.btnExcluirNF').click(function(e) {
    nf = $(this).attr('nf');
    excluir = $('input[name="txtNFExcluir"]').val();
    excluir = excluir +  nf + ';';
    
    $('input[name="txtNFExcluir"]').val(excluir);
    
    $(this).closest('tr').remove();
    
});

$('#btnAddProduto').click(function(e) {
    
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>despesa/tbAdicionarProduto/',
        data: {
            comboProduto: $('#comboProduto').val(),
            txtParticipacao: $('#cmbPart').val(),
            parcial: $('#barra-progresso').attr('aria-valuenow')
        },
        success: function(linha) {
            var existe = 0;
            obj = $.parseJSON(linha);
            
            if(obj.erro == "") {
                
                produto = $('#comboProduto :selected').text();
                $('#tabelaProduto td').each(function(e){
                    if(produto == $(this).html()) {
                      existe = 1;
                      return false;
                    }
                });

                if(existe == 0) {
                
                    $('#tabelaProduto').append(obj.dado);
                    barraProgresso( $('#cmbPart').val());
                    botoes_DOM();

                } else {
                    msgBox('Esse produto já existe na tabela');
                }

            } else {
                msgBox(obj.erro);
            }
            
            //$('input[name="txtPart"]').val('');
             $('#cmbPart')[0].selectedIndex = 0;
            $('#comboProduto')[0].selectedIndex = 0;
            
        }
    });
});

botoes_DOM();

$('#btnAddMedico').click(function(e) {
    
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>despesa/tbAdicionarMedico/',
        data: {
            comboTipoDoc: $('#cmbTipoDoc').val(),
            txtCRM: $('#txtCRM').val(),
            txtMedico: $('#txtMedico').val(),
        },
        success: function(linha) {
            $('#tabelaMedico').append(linha);
            
            $('#txtCRM').val('');
            $('#txtMedico').val('');
            botoes_DOM();
            

        }
    });
});

$('#cmbTipoDoc').change(function(e){
    opt = $(this).val().split('|');
    $('#labelDoc').html(opt[0]);
    $('#labelNome').html(opt[1]);
});

$('input, select, textarea').focusin(function(e) {
    $(this).css({'background-color' : '#FFFFEE'});
});
$('input, select, textarea').focusout(function(e) {
    $(this).css({'background-color' : '#FFFFFF'});
});

$('#listaDesc').hide();
$('#txtCRM').keyup(function(e) { 
    
    criterio = $(this).val();

    $('#listaDesc').hide();
    
    if($('#cmbTipoDoc option:selected').text() == 'CRM') {
    
        if($(this).val() != "") {
            $('#listaDesc').show();

            $.ajax({
                type: "POST",
                url: '<?php print AppConf::root; ?>despesa/pesquisarMedico/',
                data: {
                    crm:$(this).val(),
                },
                beforeSend: function (xhr) {
                    $('#tabelaDesc').html('<small>Pesquisando...</small>');           
                },
                success: function(linha) {
                   $('#tabelaDesc').html(linha);


                   $('#listaDesc td').click(function(e) {
                        crm = $(this).attr('crm');
                        nome = $(this).attr('nome');

                        $('#txtCRM').val(crm);
                        $('#txtMedico').val(nome);

                        $('#listaDesc').hide();
                        $('#tabelaDesc').html("");

                    }); 
                }
            });

        }
    
    }
});


$('.data').mask('00/00/0000');
$('.money').mask('000.000.000.000.000,00', {reverse: true});
$('.number').mask('000');
$('.justNumber').mask('0#');

function botoes_DOM() {
    $('.excluir-produto').unbind('click').click(function(e) {
        valor = -($(this).attr('valor'));
        $(this).closest('tr').remove();
        barraProgresso(valor);
        return false;
    });
    
    $('.excluir-medico').click(function(e) {
        $(this).closest('tr').remove();
    });    
}
function barraProgresso(valor) {
    barra = $('#barra-progresso');
    valorBarra = parseInt(barra.attr('aria-valuenow')) + parseInt(valor);

    barra.attr('aria-valuenow', valorBarra);
    barra.css('width', valorBarra+'%');
    barra.html(valorBarra+'%');
    }            
</script>
<style>    
.tab-pane {
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    padding:10px;
}

label {
    font-size: 11px;
}
</style>


<div class="row">
    <div class="col-lg-8">
        <h2 class="roboto" style="margin-top: 0px;"><?php print $lancamento->getEstabelecimento() ?></h2>
    </div>
    
    <div class="col-lg-4 text-right">
        <h4><?php print $lancamento->despesa->status->getStatus() ?></h4>
    </div>
</div>


<hr style="margin-top:0px">

<form id="formMarketing" enctype="multipart/form-data" action="<?php print AppConf::root ?>despesa/salvar" method="post">

<!--<input type="file" id="arqNF" name="arqNF" style="display: none" />-->

<input type="hidden" name="txtIdTipoDespesa" value="1" />
<input type="hidden" name="txtDespesaMkt" value="1" />
<input type="hidden" name="txtIdDespesa" value="<?php print $lancamento->despesa->getIdDespesa() ?>" />
<input type="hidden" name="txtIdLancamento" value="<?php print $lancamento->getIdLancamento() ?>" />
<input type="hidden" name="txtNFExcluir" value="" />



  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Dados do Lançamento</a></li>
    <li role="presentation"><a href="#nf" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Notas Fiscais</a></li>
<!--    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Produtos Participantes</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plus"></span> Médicos Participantes</a></li>-->
  </ul>

  <!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <div class="row">
            <div class="col-lg-12">
                <small>
                    <div class="row">
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Data NF</label>
                                <input type="text" class="form-control input-sm data" name="txtDataNF" value="<?php print $lancamento->getDataLancamento() ?>">
                            </div>
                        </div>                 

                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nº NF</label>
                                <input type="text" class="form-control input-sm justNumber" name="txtNF" value="<?php print $lancamento->despesa->getNotaFiscal() ?>">
                            </div>
                        </div>
                        
                        <div class="col-lg-8">
                            <div class="form-group">
                                <input type="hidden" name="txtDistrito" value="<?php echo $idSetorPag ?>|<?php echo $idColabPag ?>" />
                                <label for="exampleInputEmail1">Distrito</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="txtNomeDistrito" value="<?php echo $setorPag ?> <?php echo $colabPag ?>" disabled="" style="text-transform:none">
                                  <span class="input-group-btn">
                                      <button class="btn btn-default btn-sm" type="button"   id="btnListarDistrito"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                                  </span>
                                </div>
                            </div>
                            
                            
<!--                            <div class="form-group">
                                <label for="exampleInputEmail1">Distrito</label>
                                <?php print $comboGerente; ?>
                            </div>-->
                        </div>                        

<!--                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Arquivo NF</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="txtArqNF" value="<?php print $lancamento->despesa->getArquivoNF(); ?>" disabled="" style="text-transform:none">
                                    <input type="hidden" class="form-control input-sm" name="txtNovoArqNF" value="">
                                  <span class="input-group-btn">
                                      <button class="btn btn-default btn-sm" type="button"  id="btnSelNF"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                                  </span>
                                </div>
                            </div>
                        </div> -->


                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tipo de Ação</label>
                                <?php print $acao->comboAcao($lancamento->despesa->getIdAcao()); ?>
                            </div>
                        </div> 

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Projeto</label>
                                <?php print $projeto->comboProjeto($lancamento->despesa->getIdProjeto()); ?>
                            </div>
                        </div> 

                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Planejado</label>
                                <input type="text" class="form-control input-sm money" name="txtPlanejado" value="<?php print $lancamento->despesa->getPlanejado() ?>">
                            </div>
                        </div> 

                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Realizado</label>
                                <input type="text" class="form-control input-sm money" value="<?php print $lancamento->getValor() ?>" disabled="disabled">
                                <input type="hidden" class="form-control input-sm money" name="txtRealizado" value="<?php print $lancamento->getValor() ?>">
                            </div>
                        </div> 

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Local do Evento</label>
                                <input type="text" class="form-control input-sm" name="txtLocalEvento" value="<?php print $lancamento->despesa->getLocalEvento() ?>">
                            </div>
                        </div> 

                        <div class="col-lg-2">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Estado</label>
                              <?php print $UF->comboUF($lancamento->despesa->cidade->getIdUF()); ?>
                          </div>
                        </div>

                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cidade</label>
                                <div id="comboCidade">
                                    <?php print $UF->comboCidade($lancamento->despesa->cidade->getIdUF(), $lancamento->despesa->getIdCidade()); ?>
                                </div>
                            </div>
                        </div> 

                        <?php if($fds == 0 or $fds == 6) { $col = 6;   ?>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Justificativa Fim de Semana</label>
                                <textarea class="form-control input-sm" rows="3" name="txtJustificativaFDS"><?php print $lancamento->despesa->getJustificativaFimSemana() ?></textarea>
                            </div>
                        </div>                 
                         <?php } ?>

                        <div class="col-lg-<?php echo $col; ?>">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Detalhe da Ação</label>
                                <textarea class="form-control input-sm" rows="3" name="txtDetalhe"><?php print $lancamento->despesa->getDescricao() ?></textarea>
                            </div>
                        </div> 
                    </div>
                </small>
            </div>
        </div>
    </div> 
    
    <div role="tabpanel" class="tab-pane" id="nf">
        <p><button id="btnAddNF" type="button" class="btn btn-sm btn-success">Adicionar Nota Fiscal</button></p>
        <div style="height: 180px; overflow-x: auto;">
            <small>
                <table class="table table-condensed table-striped" id="tbNF">
                    <tr>
                        <th>Arquivo</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php $NFs = $lancamento->despesa->notaFiscal->listar();
                    foreach($NFs as $nf) { ?>
                    <tr>
                        <td><?php echo $nf->getArquivoNF(); ?></td>
                        <td><button type="button" class="btn btn-xs btn-danger btnExcluirNF" nf="<?php echo $nf->getArquivoMD5(); ?>"><span class="glyphicon glyphicon-remove"></span></button></td>
                    </tr>
                    
                    <?php } ?>
                    
                </table>
            </small>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="profile">
        
        <div class="row">
            <div class="col-lg-8">
                <div class="form-group form-group-sm">
                    <label for="exampleInputEmail1">Produto</label>
                    <?php print $lancamento->despesa->produto->comboProduto() ?>
                </div>
            </div>
            
            <div class="col-lg-2">
                <div class="form-group form-group-sm">
                    <label for="exampleInputEmail1">Participação</label>
<!--                    <input type="text" class="form-control input-sm number" name="txtPart" value="">-->
                    <select class="form-control input-sm" id="cmbPart">
                        <?php for($i=1;$i<=100;$i++) { ?>
                        <option value="<?php echo $i ?>"><?php echo $i ?>%</option>
                        <?php } ?>
                      </select>
                </div>
            </div>
            
            <div class="col-lg-2">
                <div class="form-group form-group-sm">
                    <br>
                    <button id="btnAddProduto" type="button" class="btn-block btn btn-sm btn-success">Adicionar Produto</button>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-lg-12">
              <div style="height: 130px; overflow-x: auto;">
                <small>
                    <table class="table table-striped table-condensed" id="tabelaProduto" >
                        <tr>
                            <td><strong>Produto</strong></td>
                            <td><strong>Part. %</strong></td>
                            <td><strong>Excluir</strong></td>
                        </tr>
                        <?php
                            $produtos = $lancamento->despesa->produto->listar();
                            $soma = 0;
                            foreach($produtos as $produto) {
                                $soma = $soma + $produto->getParticipacao();
                        ?>
                        
                                                <tr>
                            <td><?php print $produto->produto->getProduto() ?></td>
                            <td><?php print $produto->getParticipacao() ?></td>
                            <td>
                                <input type="hidden" name="txtValorPart[]" value="<?php print $produto->getIdProduto() ?>|<?php print $produto->getParticipacao() ?>" />
                                <button type="button" class="btn btn-danger btn-xs excluir-produto" valor="<?php print $produto->getParticipacao() ?>"><small>excluir</small></button>
                            </td>
                        </tr>
                        
                        <?php } ?>
                    </table>
                </small>

            </div>
          </div>
            <div class="col-lg-12">
                <hr style="margin-top: 0px">
                <small>Participação dos produtos</small>
                <div class="progress">
                    <div id="barra-progresso" class="progress-bar progress-bar-striped" role="progressbar" aria-valuenow="<?php print $soma ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php print $soma ?>%;">
                    <?php print $soma ?>%
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div role="tabpanel" class="tab-pane" id="messages">

        <div class="row">
            <div class="col-lg-2">
                <div class="form-group">
                    <label for="exampleInputEmail1">Tipo</label>
                    <select class="form-control input-sm" name="cmbTipoDoc" id="cmbTipoDoc">
                        <option value="CRM|Nome do Médico|M">CRM</option>
                      <option value="CNPJ|Nome da Farmácia|F">PDV</option>
                  </select>
                </div>
            </div>
            
<!--            <div class="col-lg-2">
                <div class="form-group">
                    <label for="exampleInputEmail1" id="labelDoc">CRM</label>
                    <input type="text" class="form-control input-sm" id="txtCRM">
                </div>
            </div>-->
            
            <div class="col-lg-8">
                
                <div class="row">
                    <div class="col-lg-4">
                        
                        <div class="form-group">
                            <label for="exampleInputEmail1" id="labelDoc">CRM</label>
                            <input type="text" class="form-control input-sm" id="txtCRM" autocomplete="off">
                        </div>
                        
                    </div>
                    <div class="col-lg-8">
                        
                         <div class="form-group">
                            <label for="exampleInputEmail1" id="labelNome">Nome do Médico</label>
                            <input type="text" class="form-control input-sm" id="txtMedico">
                        </div>
                        
                    </div>
                </div>

                <div id="listaDesc">
                    <div class="panel panel-default">
                        <div class="panel-body" id="tabelaDesc">
                                
                        </div>
                    </div>
                </div>
                
            </div>
            
            <div class="col-lg-2">
                <div class="form-group form-group-sm">
                    <br>
                    <button id="btnAddMedico" style="width:100%" type="button" class="btn btn-success"><small>Adicionar Médico</small></button>
                </div>
            </div>
        </div>
        
        
        <div class="row">
            <div class="col-lg-12">
                

                <div style="height: 150px; overflow-x: auto;">
                    <small><small>
                        <table class="table table-striped table-condensed" id="tabelaMedico" >
                            <tr>
                                <td><strong>CRM/CNPJ</strong></td>
                                <td><strong>Médico/PDV</strong></td>
                                <td><strong>Excluir</strong></td>
                            </tr>
                            <?php
                                $medicos = $lancamento->despesa->medico->listar();
                                foreach($medicos as $medico) {
                            ?>
                            <tr>
                                <td><?php print $medico->getCRM() ?></td>
                                <td><?php print $medico->getNome() ?></td>
                                <td>
                                    <input type="hidden" name="txtMedico[]" value="<?php print $medico->getCRM() ?>|<?php print $medico->getNome() ?>|<?php print $medico->getTipo() ?>" />
                                    <button type="button" class="btn btn-danger btn-xs excluir-medico"><small>excluir</small></button>
                                </td>
                            </tr>
                            <?php } ?>                            
                         </table>
                    </small></small>
                </div>
            </div>
        </div>
    </div>
</div>

</form>

