<?php
//Verifica se a data de lancamento é fim de semana. Sabado = 6 e Domingo = 0;
$fds = date("w", strtotime(appFunction::formatarData($lancamento->getDataLancamento())));
//$fds = 0;
$col = 12;
?>
<style>
    #listaDesc {
        position: absolute;
        width: 95%;
        z-index: 1000;
        height: 200px;
        
    }
    #listaDesc .panel-body {
        height: 200px;
        overflow:auto;
    }

    
    #listaDesc table {
        margin-bottom: 0px;
    }
    
    #listaDesc table td {
       cursor: pointer;
    }
	
    .tab-pane{
        max-height: 280px;
        overflow-x: hidden;
    }

</style>  
<script>

$('.txtQtd, .txtValor').unbind().keyup(function(e) {
    id = $(this).attr('cat');
    pai= $(this).attr('pai');
    
    var qtd   = ($('#txtQtd'+id).val()   == '') ? 0 : $('#txtQtd'+id).val();
    var valor = ($('#txtValor'+id).val() == '') ? 0 : $('#txtValor'+id).val();

    valor = removeMask(valor);
    valor = parseFloat(valor);
    
    var total = parseFloat((valor / qtd)).toFixed(2);
    $('#txtTotal'+id).val(total).unmask().mask("#.##0,00", {reverse: true});
    
    somaTotal = 0;
    somaQtd = 0;
    somaValor = 0;
   
    $('input[pai="'+pai+'"].txtQtd').each(function(e){
        somaQtd = somaQtd + parseInt((($(this).val() == '') ? 0 : $(this).val()));
    });
    $('input[pai="'+pai+'"].txtValor').each(function(e){
        somaValor = somaValor + parseFloat(($(this).val() == '') ? 0 : removeMask($(this).val()));
    });    
    
    somaTotal = somaValor / somaQtd;
    $('#txtTotal'+pai).val(somaTotal.toFixed(2)).unmask().mask("#.##0,00", {reverse: true});
    
    
    somaTotal = 0;
    somaQtd = 0;
    somaValor = 0;
    
    $('.txtQtd').each(function(e){
        somaQtd = somaQtd + parseInt((($(this).val() == '') ? 0 : $(this).val()));
    });
    $('.txtValor').each(function(e){
        somaValor = somaValor + parseFloat(($(this).val() == '') ? 0 : removeMask($(this).val()));
    });
    
    //somaTotal = somaValor / somaQtd;
    //$('#txtTotalGeral').val(somaTotal.toFixed(2)).unmask().mask("#.##0,00", {reverse: true});
    //alert(somaValor + '/' + somaQtd + ' = ' + somaTotal);
   /*
    $('.txtTotal').each(function(e) {
        valor = $(this).val();
        valor = removeMask(valor);
        valor = parseFloat(valor);
        
        if($(this).attr('pai') == pai) {
            somaTotal = somaTotal + valor;
        }
    });
    */
    
    
    
 /*
    totalGeral = 0
    $('.txtTotal').each(function(e) {
        valor = $(this).val();
        valor = removeMask(valor);
        valor = parseFloat(valor);
        
        if($(this).attr('pai') == 0) {
            totalGeral = totalGeral + valor;
        }
    });
    $('#txtTotalGeral').val(totalGeral.toFixed(2)).unmask().mask("#.##0,00", {reverse: true});
    */
});

function removeMask(value) {
    d = value.toString();
    //alert(d.length);
    x = '';
    for(i=0;i<d.length;i++) {
        
        if(d.charAt(i) !== '.') {
            x = x + d.charAt(i);
            x = x.replace(',','.');
        }
    }
    
    return x;
}


$('#btnAddPart').unbind().click(function(e) {
    $.ajax({
        type: "POST",
        url: '<?php echo AppConf::root ?>despesa/adicionarPart',
        data: {
            txtID:    $('input[name="txtID"]').val(),
            txtSetor: $('input[name="txtSetor"]').val(),
            txtNome:  $('input[name="txtNome"]').val()
        },
        success: function(retorno) {
            modalAguarde('hide');
            $('#tbPart').append(retorno);
            
            $('input[name="txtID"]').val('');
            $('input[name="txtSetor"]').val('');
            $('input[name="txtNome"]').val('');
    
            $('.btnExcluirPart').unbind().click(function(e) {
                $(this).closest('tr').remove();
            });

        },
        beforeSend: function (a) {
           modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
        }
    });
});


 $('.btnExcluirPart').unbind().click(function(e) {
    $(this).closest('tr').remove();
});

$('#btnListarDistritoM').click(function(e) {
    
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>despesa/listaGerentes',
        success: function(tabela) {
            
            $('#modalDistrito #listaGerentes').html(tabela);
            
            $('#btnSelecionarDistrito').click(function(e) {
                checked = $('input[name=optionsRadios]:checked');
                texto = checked.attr('text');
                valor = checked.val();

                $('input[name="txtDistrito"]').val(valor);
                $('input[name="txtNomeDistrito"]').val(texto);

                $('#modalDistrito').modal('hide');
            });
            
            modalAguarde('hide');
            $('#modalDistrito').modal('show');
        },
        beforeSend: function (a) {
           modalAguarde('show');
        }
    });
});



$('select[name="cmbUFM"]').change(function(e) {
    $.ajax({
        type: "POST",
        url: '<?php print AppConf::root; ?>cidade/comboCidade/'+$(this).val()+'/cmbCidadeM',
        success: function(cidade) {
            $('#comboCidadeM').html(cidade);
        },
        beforeSend: function (a) {
           $('#comboCidadeM').html('<select class="form-control input-sm"><option>Listando...</option></select>');
        }
    });
});

$('#btnAddNFM').click(function(e) {

    nome = $.now();
    
    file = '<input type="file" id="arqNFM'+nome+'" name="arqNF[]" style="display: none" asccept=".pdf" accept="image/jpeg,image/bmp,image/gif,image/png,application/pdf,image/x-eps" />';
    
    $('#formMove').append(file);
    $('#arqNFM'+nome).click();
    $('#arqNFM'+nome).change(function(e) {
        
        if(this.files[0].size <= 2000000) {
        
        
            arquivo = $(this).val().split(/(\\|\/)/g).pop();
            tr = '<tr><td>'+arquivo+'</td><td><button type="button" class="btn btn-xs btn-danger btnExcluirFile" file="arqNFM'+nome+'"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
            $('#tbNFM').append(tr);

            $('.btnExcluirFile').click(function(e) {
                fileExcluir = $(this).attr('file');

                $(this).closest('tr').remove();
                $('#'+fileExcluir).remove();
            });
        } else {
            msgBox('Tamanho de arquivo excedido. Máximo 2 MB!');
            $('#arqNFM'+nome).remove();
        }
    });
});

$('.btnExcluirNF').click(function(e) {
    nf = $(this).attr('nf');
    excluir = $('input[name="txtNFExcluir"]').val();
    excluir = excluir +  nf + ';';
    
    $('input[name="txtNFExcluir"]').val(excluir);
    
    $(this).closest('tr').remove();
    
});



botoes_DOM();




$('input, select, textarea').focusin(function(e) {
    $(this).css({'background-color' : '#FFFFEE'});
});
$('input, select, textarea').focusout(function(e) {
    $(this).css({'background-color' : '#FFFFFF'});
});




$('.data').mask('00/00/0000');
$('.money').mask("#.##0,00", {reverse: true});
$('.number').mask('000');
$('.justNumber').mask('0#');

function botoes_DOM() {
    $('.excluir-produto').unbind('click').click(function(e) {
        valor = -($(this).attr('valor'));
        $(this).closest('tr').remove();
        barraProgresso(valor);
        return false;
    });
    
    $('.excluir-medico').click(function(e) {
        $(this).closest('tr').remove();
    });    
}


</script>
<style>    
.tab-pane {
    border-bottom: 1px solid #ddd;
    border-left: 1px solid #ddd;
    border-right: 1px solid #ddd;
    padding:10px;
    
}

.bold {font-weight: bold}
label {
    font-size: 11px;
}
</style>


<div class="row">
    <div class="col-lg-8">
        <h2 class="roboto" style="margin-top: 0px;"><?php print $lancamento->getEstabelecimento() ?></h2>
    </div>
    
    <div class="col-lg-4 text-right">
        <h4><?php print $lancamento->despesa->status->getStatus() ?></h4>
    </div>
</div>


<hr style="margin-top:0px">

<form id="formMove" enctype="multipart/form-data" action="<?php print AppConf::root ?>despesa/salvarMove" method="post">

<!--<input type="file" id="arqNF" name="arqNF" style="display: none" />-->

<input type="hidden" name="txtDespesaMkt" value="2" />
<input type="hidden" name="txtIdDespesa" value="<?php print $lancamento->despesa->getIdDespesa() ?>" />
<input type="hidden" name="txtIdLancamento" value="<?php print $lancamento->getIdLancamento() ?>" />
<input type="hidden" name="txtNFExcluir" value="" />



  <!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active"><a href="#homeM" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Dados do Lançamento</a></li>
    <li role="presentation"><a href="#cats" aria-controls="cats" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-th-list"></span> Categorias</a></li>
    <li role="presentation"><a href="#nfM" aria-controls="nfM" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Notas Fiscais/Foto</a></li>
    <li role="presentation"><a href="#participantes" aria-controls="participantes" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Participantes</a></li>
    
<!--    <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Produtos Participantes</a></li>
    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-plus"></span> Médicos Participantes</a></li>-->
  </ul>

  <!-- Tab panes -->
<div class="tab-content">
    <div role="tabpanel" class="tab-pane" id="participantes">
        
       
            
        <div class="row">
            <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="txtID" value="" placeholder="ID...">
            </div>
            <div class="col-lg-2">
                <input type="text" class="form-control input-sm" name="txtSetor" value="" placeholder="Setor">
            </div>
            <div class="col-lg-6">
                <input type="text" class="form-control input-sm" name="txtNome" value="" placeholder="Nome do participante...">
            </div>
            <div class="col-lg-2">
                <button type="button" class="btn btn-success btn-sm btn-block" id="btnAddPart"><span class="glyphicon glyphicon-plus"></span> Adicionar</button>
            </div>
        </div>

        <HR>
        
        <table class="table table-condensed table-striped" id="tbPart" style="font-size:12px;">
            <tr>
                <th>ID</th>
                <th>Setor</th>
                <th>Participante</th>
                <th>&nbsp;</th>
            </tr>
            <?php echo $tbPart ?>
        </table>
    </div>
    
    
    <div role="tabpanel" class="tab-pane" id="cats">
        <div class="row">
            <div class="col-lg-12">
                
                
                <table class="table table-condensed" style="font-size:13px">
                    <tr class="active">
                        <th>Categoria</th>
                        <th>Qtd</th>
                        <th>Valor Total</th>
                        <th>Média</th>
                    </tr>
                    <?php echo $listaCategoria ?>
<!--                    <tr class="active">
                        <td><h4><b>TOTAL</b></h4></td>
                        <td></td>

                        <td colspan="2">
                            <b>
                            <input type="text" style="text-align:right" class="form-control " id="txtTotalGeral"   value="0,00" disabled></b>
                        </td>
                    </tr>-->
                </table>
                
            </div>
            
        </div>
    </div>
    
    <div role="tabpanel" class="tab-pane active" id="homeM">
        <div class="row">
            <div class="col-lg-12">
                <small>
                    <div class="row">
                        
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tipo de Ação</label>
                                <select class="form-control input-sm" name="cmbTipoDespesa" id="cmbTipoDespesa">
                                    <option></option>
                                <?php foreach($lancamento->despesa->tipoDespesa->listar() as $tipo) { ?>
                                
                                    
                                    <option <?php echo (($lancamento->despesa->getIdTipoDespesa() == $tipo->getIdTipoDespesa()) ? 'selected'  : ''); ?> value="<?php echo $tipo->getIdTipoDespesa() ?>"><?php echo $tipo->getDescricao() ?></option>
                                
                                <?php } ?>
                                    </select>
                                <?php //print $lancamento->despesa->tipoDespesa->comboTipoDespesa($lancamento->despesa->getIdTipoDespesa()) ?>
                            </div>
                        </div> 
                        
                        
                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Data NF</label>
                                <input type="text" class="form-control input-sm data" name="txtDataNF" value="<?php print $lancamento->getDataLancamento() ?>">
                            </div>
                        </div>                 

                        <div class="col-lg-2">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Nº NF</label>
                                <input type="text" class="form-control input-sm" name="txtNF" value="<?php print $lancamento->despesa->getNotaFiscal() ?>">
                            </div>
                        </div>
                        
                        <div class="col-lg-5">
                            <div class="form-group">
                                <input type="hidden" name="txtDistrito" value="<?php echo $idSetorPag ?>|<?php echo $idColabPag ?>" />
                                <label for="exampleInputEmail1">Distrito</label>
                                <div class="input-group">
                                    <input type="text" class="form-control input-sm" name="txtNomeDistrito" value="<?php echo $setorPag ?> <?php echo $colabPag ?>" disabled="" style="text-transform:none">
                                  <span class="input-group-btn">
                                      <button class="btn btn-default btn-sm" type="button"   id="btnListarDistritoM"><span class="glyphicon glyphicon-search"></span>&nbsp;</button>
                                  </span>
                                </div>
                            </div>
                            
                            

                        </div>                        



<!--                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tipo de Ação</label>
                                <?php print $acao->comboAcao($lancamento->despesa->getIdAcao()); ?>
                            </div>
                        </div> 

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Projeto</label>
                                <?php print $projeto->comboProjeto($lancamento->despesa->getIdProjeto()); ?>
                            </div>
                        </div> -->

  

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Local do Evento</label>
                                <input type="text" class="form-control input-sm" name="txtLocalEvento" value="<?php print $lancamento->despesa->getLocalEvento() ?>">
                            </div>
                        </div> 

                        <div class="col-lg-2">
                          <div class="form-group">
                              <label for="exampleInputEmail1">Estado</label>
                              <?php print $UF->comboUF($lancamento->despesa->cidade->getIdUF(), 'cmbUFM'); ?>
                          </div>
                        </div>

                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Cidade</label>
                                <div id="comboCidadeM">
                                    <?php print $UF->comboCidade($lancamento->despesa->cidade->getIdUF(), $lancamento->despesa->getIdCidade(), 'cmbCidadeM'); ?>
                                </div>
                            </div>
                        </div> 

                        <?php if($fds == 0 or $fds == 6) { $col = 6;   ?>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Justificativa Fim de Semana</label>
                                <textarea class="form-control input-sm" rows="3" name="txtJustificativaFDS"><?php print $lancamento->despesa->getJustificativaFimSemana() ?></textarea>
                            </div>
                        </div>                 
                         <?php } ?>

                        <div class="col-lg-<?php echo $col; ?>">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Detalhe da Ação</label>
                                <textarea class="form-control input-sm" rows="3" name="txtDetalhe"><?php print $lancamento->despesa->getDescricao() ?></textarea>
                            </div>
                        </div> 
                    </div>
                </small>
            </div>
        </div>
    </div> 
    
    <div role="tabpanel" class="tab-pane" id="nfM">
        <p><button id="btnAddNFM" type="button" class="btn btn-sm btn-success">Adicionar Nota Fiscal/Foto</button></p>
        <div style="height: 180px; overflow-x: auto;">
            <small>
                <table class="table table-condensed table-striped" id="tbNFM">
                    <tr>
                        <th>Arquivo</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php $NFs = $lancamento->despesa->notaFiscal->listar();
                    foreach($NFs as $nf) { ?>
                    <tr>
                        <td><?php echo $nf->getArquivoNF(); ?></td>
                        <td><button type="button" class="btn btn-xs btn-danger btnExcluirNF" nf="<?php echo $nf->getArquivoMD5(); ?>"><span class="glyphicon glyphicon-remove"></span></button></td>
                    </tr>
                    
                    <?php } ?>
                    
                </table>
            </small>
        </div>
    </div>

    
    
    
</div>

</form>

