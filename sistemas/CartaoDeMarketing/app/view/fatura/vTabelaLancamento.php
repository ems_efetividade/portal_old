<?php

$pendente = 0;

if (count($lanc) > 0) { ?>

<div class="panel panel-default">
    <div class="panel-heading">
        <strong><span class="glyphicon glyphicon-ok"></span> Despesas de Marketing</strong>
    </div>
</div>

<small>
    <table class="table table-condensed table-bordered table-hover table-striped">
        <tr class="active">
            <th width="1%"></th>
            <th width="1%">Data</th>
            <th width="28%">Estabelecimento</th>
            <th width="10%" class="text-center">Valor</th>
            <th width="20%">Ação</th>
            <th width="12%">Projeto</th>
            <th width="8%">Nº Médicos</th>
            <th width="10%" class="text-center">Status</th>
            <th width="10%" class="text-right"></th>
        </tr>

        <?php foreach($lanc as $i => $lancamento) { ?>    
        <tr>
            <td class="text-center"><?php print  $i+1 ; ?></td>
            <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
            <td><?php print $lancamento->getEstabelecimento(); ?></td>
            <td class="text-center"><b><?php print $lancamento->getValor(); ?></b></td>
            <td><?php print $lancamento->despesa->acao->getDescricao(); ?></td>
            <td><?php print $lancamento->despesa->projeto->getDescricao(); ?></td>
            <td class="text-center"><?php print $lancamento->despesa->medico->getNumMedicos(); ?></td>
            <td  class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
            <td  class="text-center">
                <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalVisualizarDespesa" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                <?php if ($fatura->statusFatura->getIdStatusFatura() != 2 && ($lancamento->despesa->status->getIdStatusDespesa() == 3 || $lancamento->despesa->status->getIdStatusDespesa() == 1)) { ?>
                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalMarketing" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a>
                <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirDespesa" data-id-despesa="<?php print $lancamento->despesa->getIdDespesa(); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                <?php } //$fatura->statusFatura->getIdStatusFatura() != 2 ?>
            </td>
        </tr>
        <?php }  ?>
    </table>
</small>
<?php }  ?>


<?php if (count($move) > 0) { ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <strong><span class="glyphicon glyphicon-ok"></span> Move/Distrital</strong>
    </div>
</div>
<small>
    <table class="table table-condensed table-bordered table-hover table-striped">
        <tr class="active">
            <th width="1%"></th>
            <th width="1%">Data</th>
            <th width="28%">Estabelecimento</th>
            <th width="10%" class="text-center">Valor</th>
            <th width="12%">Tipo</th>
            <th width="28%">Descricao</th>
            <th width="10%" class="text-center">Status</th>
            <th width="10%" class="text-center"></th>
        </tr>

        <?php 
            foreach($move as $i => $lancamento) {   ?>    
        <tr>
            <td class="text-center"><?php print $i+1; ?></td>
            <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
            <td><?php print $lancamento->getEstabelecimento(); ?></td>
            <td class="text-center"><b><?php print $lancamento->getValor(); ?></b></td>
            <td><?php print $lancamento->despesa->tipoDespesa->getDescricao(); ?></td>
            <td><?php print $lancamento->despesa->getDescricao(); ?></td>
            <td class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
            <td class="text-center">
                <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalVisualizarDespesa" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                <?php if ( $fatura->statusFatura->getIdStatusFatura() != 2 && ($lancamento->despesa->status->getIdStatusDespesa() == 3 || $lancamento->despesa->status->getIdStatusDespesa() == 1)) { ?>
                
                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalMove" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a>
                
                <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirDespesa" data-id-despesa="<?php print $lancamento->despesa->getIdDespesa(); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                <?php } ?>
            </td>
        </tr>
        <?php  }  ?>
    </table>
</small>
<?php  }  ?>



<?php if (count($outros) > 0) { ?>
<div class="panel panel-default">
    <div class="panel-heading">
        <strong><span class="glyphicon glyphicon-ok"></span> Outras Despesas</strong>
    </div>
</div>
<small>
    <table class="table table-condensed table-bordered table-hover table-striped">
        <tr class="active">
            <th width="1%"></th>
            <th width="1%">Data</th>
            <th width="28%">Estabelecimento</th>
            <th width="10%" class="text-center">Valor</th>
            <th width="12%">Tipo</th>
            <th width="28%">Descricao</th>
            <th width="10%" class="text-center">Status</th>
            <th width="10%" class="text-center"></th>
        </tr>

        <?php 
            foreach($outros as $i => $lancamento) {   ?>    
        <tr>
            <td class="text-center"><?php print $i+1; ?></td>
            <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
            <td><?php print $lancamento->getEstabelecimento(); ?></td>
            <td class="text-center"><b><?php print $lancamento->getValor(); ?></b></td>
            <td><?php print $lancamento->despesa->tipoDespesa->getDescricao(); ?></td>
            <td><?php print $lancamento->despesa->getDescricao(); ?></td>
            <td class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
            <td class="text-center">
                <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#modalVisualizarDespesa" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                <?php if ( $fatura->statusFatura->getIdStatusFatura() != 2 && ($lancamento->despesa->status->getIdStatusDespesa() == 3 || $lancamento->despesa->status->getIdStatusDespesa() == 1)) { ?>
                
                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalOutros" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a>
                
                <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirDespesa" data-id-despesa="<?php print $lancamento->despesa->getIdDespesa(); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                <?php } ?>
            </td>
        </tr>
        <?php  }  ?>
    </table>
</small>
<?php  }  ?>