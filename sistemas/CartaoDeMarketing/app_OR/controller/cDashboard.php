<?php
require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/mDashboard.php');


class cDashboard extends appController {
    
    private $dash;
    
    
    public function __construct() {
        $this->dash = new mDashboard();
    }
    
    public function main() {
        $this->set('box', $this->dash->getBoxes());
        $this->set('graf', $this->dadosGraficoLinha());

        $this->render('admin/vDash');
    }
    
    public function dadosGraficoLinha() {
        
        $dados = $this->dash->getGraficoLinha();
        
        $arr = array();
        $arr2 = array();
        $xAxis = 'Jun/16';
        
        foreach($dados as $dado) {
            $a['name'] = $dado['LINHA'];
            $a['data'] = array($dado['TOTAL']);
            $arr[] = $a;
            $arr2[] = $dado['LINHA'];
        }
        
        $retorno['xAxis'] = json_encode($arr2);
        $retorno['data'] = json_encode($arr, JSON_NUMERIC_CHECK);
        
        return $retorno;
        
    }
    
}