<?php
require_once('lib/appController.php');
require_once('app/model/mDespesa.php');
require_once('app/model/mProduto.php');
require_once('app/model/mProdutoDespesa.php');
require_once('app/model/mMedico.php');
require_once('app/model/mLancamento.php');
require_once('app/model/mColaboradorSetor.php');
require_once('app/model/mNotaFiscal.php');

require_once('app/model/mAcao.php');
require_once('app/model/mCidade.php');
require_once('app/model/mProjeto.php');

class cDespesa extends appController {
    
    private $despesa = null;
    private $lancamento = null;
    
    private $acao        = null;
    private $cidade      = null;
    private $projeto     = null;
    private $produto     = null;
    private $medico      = null;
    private $colaborador = null;
    private $NF          = null;
    
    
    public function __construct() {
        $this->despesa = new mDespesa();
        $this->produto = new mProduto();
        $this->lancamento = new mLancamento();
        
        $this->acao     = new mAcao();
        $this->cidade   = new mCidade();
        $this->projeto  = new mProjeto();
        $this->medico   = new mMedico();
        $this->colaborador = new mColaboradorSetor();
        $this->nf = new mNotaFiscal();
    }
    
    public function formularioMarketing($idLancamento=0) {
        
        $this->lancamento->setIdLancamento($idLancamento);
        $this->lancamento->selecionar();
        
        $idSetorPag = ($this->lancamento->despesa->getIdSetorPag()       == 0) ? appFunction::dadoSessao('id_setor') : $this->lancamento->despesa->getIdSetorPag();
        $idColabPag = ($this->lancamento->despesa->getIdColaboradorPag() == 0) ? appFunction::dadoSessao('id_colaborador') : $this->lancamento->despesa->getIdColaboradorPag();
        
        
        $this->colaborador->setIdColaborador($idColabPag);
        $this->colaborador->selecionarPorColaborador();
        $colabPag = $this->colaborador->getColaborador();   
            
        $this->colaborador->setIdSetor($idSetorPag);
        $this->colaborador->selecionar();
        $setorPag = $this->colaborador->getSetor();    
        
         
         
        
        
        
        $this->set('lancamento', $this->lancamento);
        $this->set('acao', $this->acao);
        $this->set('projeto', $this->projeto);
        $this->set('UF', $this->cidade);
        $this->set('comboProduto', $this->produto->comboProduto());
        
        $this->set('idSetorPag',$idSetorPag);
        $this->set('idColabPag',$idColabPag);
        $this->set('setorPag',$setorPag);
        $this->set('colabPag',$colabPag);        
        //$this->set('comboGerente', $this->colaborador->comboGerentes('GERENTE DISTRITAL'));
        
        $this->render('fatura/vFaturaFormMarket');
    }
    
    public function formularioOutros($idLancamento=0) {
        $this->lancamento->setIdLancamento($idLancamento);
        $this->lancamento->selecionar();
        
        $this->set('lancamento', $this->lancamento);
        $this->render('fatura/vFaturaFormOutros');
    }   
    
    public function formularioDistrital($idLancamento=0) {
        
        $this->lancamento->setIdLancamento($idLancamento);
        $this->lancamento->selecionar();
        
        $idSetorPag = ($this->lancamento->despesa->getIdSetorPag()       == 0) ? appFunction::dadoSessao('id_setor') : $this->lancamento->despesa->getIdSetorPag();
        $idColabPag = ($this->lancamento->despesa->getIdColaboradorPag() == 0) ? appFunction::dadoSessao('id_colaborador') : $this->lancamento->despesa->getIdColaboradorPag();
        
        
        $this->colaborador->setIdColaborador($idColabPag);
        $this->colaborador->selecionarPorColaborador();
        $colabPag = $this->colaborador->getColaborador();   
            
        $this->colaborador->setIdSetor($idSetorPag);
        $this->colaborador->selecionar();
        $setorPag = $this->colaborador->getSetor();    
        

        $this->set('lancamento', $this->lancamento);
        $this->set('acao', $this->acao);
        $this->set('projeto', $this->projeto);
        $this->set('UF', $this->cidade);
        $this->set('comboProduto', $this->produto->comboProduto());
        
        $this->set('idSetorPag',$idSetorPag);
        $this->set('idColabPag',$idColabPag);
        $this->set('setorPag',$setorPag);
        $this->set('colabPag',$colabPag);        
        //$this->set('comboGerente', $this->colaborador->comboGerentes('GERENTE DISTRITAL'));
        
        $this->render('fatura/vFaturaFormDistrital');
    }    
    
    public function salvar() {
        //print_r($_POST);
        
        $_POST = appSanitize::filter($_POST);
        
        $numArquivos = count($_FILES['arqNF']['name']);
        $nfExcluir = $_POST['txtNFExcluir'];
        
        

        //print_r($_FILES['arqNF']);
        
        
        $idDespesa          = isset($_POST['txtIdDespesa'])     ? $_POST['txtIdDespesa']     : 0;
        $idTipoDespesa      = isset($_POST['txtIdTipoDespesa']) ? $_POST['txtIdTipoDespesa'] : 0;
        $idLancamento       = isset($_POST['txtIdLancamento'])  ? $_POST['txtIdLancamento']  : 0;
        $idCidade           = isset($_POST['cmbCidade'])        ? $_POST['cmbCidade']        : 0;
        $projeto            = isset($_POST['cmbProjeto'])       ? $_POST['cmbProjeto']       : 0;
        $planejado          = isset($_POST['txtPlanejado'])     ? $_POST['txtPlanejado']     : 0;
        $realizado          = isset($_POST['txtRealizado'])     ? $_POST['txtRealizado']     : 0;
        $NF                 = $_POST['txtNF'];
        $dataNF             = $_POST['txtDataNF'];
        $acao               = $_POST['cmbAcao'];
        $localEvento        = $_POST['txtLocalEvento'];
        $detalhe            = $_POST['txtDetalhe'];
        $justificativaFDS   = $_POST['txtJustificativaFDS'];
        $despesaMkt         = $_POST['txtDespesaMkt'];
        $arqNovoNF          = $_POST['txtNovoArqNF'];
        $distrito           = $_POST['txtDistrito'];
               
        $produtos        = (isset($_POST['txtValorPart'])) ? $_POST['txtValorPart'] : array();
        $medicos         = (isset($_POST['txtMedico'])) ? $_POST['txtMedico'] : array() ;        
        
        $arquivoNF      = (isset($_FILES['arqNF'])) ? $_FILES['arqNF']['name'] : '';
        

        $dadosDistrito = explode('|', $distrito);
        $this->despesa = new mDespesa();
        
        $this->despesa->setIdDespesa($idDespesa);
        $this->despesa->setIdTipoDespesa($idTipoDespesa);
        $this->despesa->setIdLancamento($idLancamento);
        $this->despesa->setNotaFiscal($NF);
        $this->despesa->setDataNotaFiscal($dataNF);
        $this->despesa->setIdAcao($acao);
        $this->despesa->setIdProjeto($projeto);
        $this->despesa->setPlanejado($planejado);
        $this->despesa->setRealizado($realizado);
        $this->despesa->setLocalEvento($localEvento);
        $this->despesa->setIdCidade($idCidade);
        $this->despesa->setDescricao($detalhe);
        $this->despesa->setArquivoNF($arquivoNF);
        $this->despesa->setJustificativaFimSemana($justificativaFDS);
        $this->despesa->setDespesaMkt($despesaMkt);
        $this->despesa->setIdSetorPag($dadosDistrito[0]);
        $this->despesa->setIdColaboradorPag($dadosDistrito[1]);
        
        foreach($produtos as $produto) {
            
            $dadosProduto = explode('|', $produto);
            
            $prod = new mProdutoDespesa;
            $prod->setIdProduto($dadosProduto[0]);
            $prod->setParticipacao($dadosProduto[1]);
            
            $this->despesa->setProdutos($prod);
        }
        
        foreach($medicos as $medico) {
            
            $dadosMedico = explode('|', $medico);
            
            $med = new mMedico;
            $med->setCRM($dadosMedico[0]);
            $med->setNome($dadosMedico[1]);
            $med->setTipo($dadosMedico[2]);
            
            $this->despesa->setMedicos($med);
        }
        
        /*
        if($arquivoNF != "") {
            $ext = explode('.', $arquivoNF);
            $arquivoNFMD5 = md5(rand().$arquivoNF.date('Y-m-d H:i:s')).'.'.$ext[count($ext)-1];
            $mover = move_uploaded_file($_FILES['arqNF']['tmp_name'], appConf::nfFolderRoot.$arquivoNFMD5);
            
            if($idDespesa != 0) {
                $d = new mDespesa();
                $d->setIdDespesa($idDespesa);
                $d->selecionar();
                
                if($d->getArquivoNFMD5() != ""){
                    unlink(appConf::nfFolderRoot.$d->getArquivoNFMD5());
                }
            }
        }
*/
        if($numArquivos > 0) {
            $arquivos = $_FILES['arqNF'];

            for($i=0;$i<$numArquivos;$i++) {
                                
                $nf = new mNotaFiscal();
                $nf->setArquivoNF($arquivos['name'][$i]);
                $nf->setArquivoTemp($arquivos['tmp_name'][$i]);
                $nf->setTipo($arquivos['type'][$i]);
                //$nf->setArquivoMD5($arquivoNFMD5);
                
                
                $this->despesa->setNotasFiscais($nf);
                
            }
        }
        
        if($nfExcluir != "") {
            $NFs = explode(';', $nfExcluir);
            
            for($i=0;$i<count($NFs)-1;$i++) {
                
                $nf = new mNotaFiscal();
                $nf->setIdDespesa($idDespesa);
                $nf->setArquivoMD5($NFs[$i]);
                
                $this->despesa->setNotasFiscaisExcluir($nf);
            }
        }
       
        
        

        
       $this->despesa->setArquivoNFMD5($arquivoNFMD5);

       echo $this->despesa->salvar();
        
          
         
        
    }
    
    public function excluir($idDespesa) {
        $this->despesa->setIdDespesa($idDespesa);
        $this->despesa->excluir();
        $this->location($_SERVER['HTTP_REFERER']);
    }
    
        
    public function listaGerentes() {
        //'GERENTE DISTRITAL';
        
    	
        $perfil = (appFunction::dadoSessao('perfil') == 4) ? 3 : appFunction::dadoSessao('perfil');
        
        $gerentes = $this->colaborador->listarGerentes($perfil);

        foreach($gerentes as $gerente) {
            $tabela .= '<tr>'
                    . '<td><input type="radio" name="optionsRadios" text="'.$gerente->getSetor().' '.$gerente->getColaborador().'" value="'.$gerente->getIdSetor().'|'.$gerente->getIdColaborador().'"></td>'
                    . ' <td>'.$gerente->getSetor().' '.$gerente->getColaborador().'</td>'
                    . '</tr>';
        }
        
        echo '<small><small><table class="table table-condensed table-striped">'.$tabela.'</table></small></small>';
    }
    
    
    public function tbAdicionarProduto() {
        $_POST = appSanitize::filter($_POST);
        
        $idProduto     = $_POST['comboProduto'];
        $participacao  = $_POST['txtParticipacao'];
        $parcial       = $_POST['parcial'];
        
        $erro = '';
        if(($parcial + $participacao) > 100) {
            $erro = 'Você já atingiu 100% dos produtos';
        }
        
        $this->produto->setIdProduto($idProduto);
        $this->produto->selecionar();
        
        $linha = '<tr>
                    <td>'.$this->produto->getProduto().'</td>
                    <td>
                        '.$participacao.'%
                    </td>
                    <td>
                    <input type="hidden" name="txtValorPart[]" value="'.$idProduto.'|'.$participacao.'" />
                    <button type="button" class="btn btn-danger btn-xs excluir-produto" valor="'.$participacao.'"><small>excluir</small></button>
                    </td>    
                </tr>';
        
        $arr['erro'] = $erro;
        $arr['dado'] = $linha;
       
        echo json_encode($arr);
        
        
    }
    
    public function tbAdicionarMedico() {
        
        $_POST = appSanitize::filter($_POST);
        
        $documento = $_POST['comboTipoDoc'];
        $CRM       = $_POST['txtCRM'];
        $medico    = $_POST['txtMedico'];
        
        $tipo = explode("|", $documento);
        
        $linha = '<tr>
                    <td>'.$CRM.'</td>
                    <td>
                        '.$medico.'
                    </td>
                    <td>
                    <input type="hidden" name="txtMedico[]" value="'.$CRM.'|'.$medico.'|'.$tipo[2].'" />
                    <button type="button" class="btn btn-danger btn-xs excluir-medico"><small>excluir</small></button>
                    </td>    
                </tr>';
       
        echo $linha;
    }
    
    public function pesquisarMedico() {
        $_POST = appSanitize::filter($_POST);
        $crm = $_POST['crm'];
        $medicos = $this->medico->pesquisarMedicoBase($crm);
        
        if (count($medicos) > 0) {
            
            $linha = '';
            
            foreach($medicos as $medico) {
                $linha .= '<tr>
                            <td crm="'.$medico->getCRM().'" nome="'.$medico->getNome().'">'.$medico->getCRM().'</td>
                            <td crm="'.$medico->getCRM().'" nome="'.$medico->getNome().'">'.$medico->getNome().'</td>
                            <td crm="'.$medico->getCRM().'" nome="'.$medico->getNome().'">'.$medico->getEsp().'</td>
                        </tr>';
            }
            

            
            echo '<small>
                  <small>
                    <table class="table table-condensed table-striped table-hover" id="tb-pesquisa-medico">
                        '.$linha.'
                    </table>
                </small>
                </small>';
            
          
            
        } else {
            echo '<small>Médico não encontrado</small>';
        }
    }
    
    private function salvarImagemNF($arquivo) {
        $nomeArquivo = $arquivo['tmp_nome'];
    }
    
    public function downloadNF($arquivo) {
        
        
    }
    
}