<?php
require_once('lib/appController.php');

require_once('app/model/mCartao.php');
require_once('app/model/mFatura.php');
require_once('app/model/mAcao.php');
require_once('app/model/mCidade.php');
require_once('app/model/mProjeto.php');
require_once('app/model/mProduto.php');
require_once('app/model/mDespesa.php');


class cFatura extends appController {

    private $fatura     = null;
    private $acao       = null;
    private $cidade     = null;
    private $projeto    = null;
    private $produto    = null;
    private $despesa    = null;
    private $cartao     = null;
    
    public function __construct() {
        $this->fatura = new mFatura();
        
        $this->acao     = new mAcao();
        $this->cidade   = new mCidade();
        $this->projeto  = new mProjeto();
        $this->produto  = new mProduto();
        $this->despesa  = new mDespesa();
        $this->cartao   = new mCartao();
    }
    
    public function abrirFatura($idFatura) {
        
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        
        $lancOutros = array();
        
        $lancMkt    = $this->fatura->lancamento->listarPorTipo(1);
        $lancOutros = $this->fatura->lancamento->listarPorTipo(0);
        //$lancOutros[] = $this->fatura->lancamento->listarPorTipo(3);
        
        
        $this->set('fatura', $this->fatura);
        $this->set('lanc', $lancMkt);
        $this->set('outros', $lancOutros);
        $tbMKT = $this->renderToString('fatura/vTabelaLancamento');
        
       // $this->set('lanc', $lancOutros);
        
        $this->set('tabelaMkt', $tbMKT);
        $this->set('tabelaOutros', $tbOutros);
        
        $this->set('fatura', $this->fatura);
        $this->set('totalFatura', $this->fatura->getTotalFatura());
        $this->set('lancamentos', $this->fatura->lancamento->listarNaoJustificados());
        $this->render('fatura/vFatura');
    }
    
    public function salvar($idFatura) {
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->salvar(appFunction::dadoSessao('setor'));
        
        $this->location(appConf::root);
    }

    public function imprimirFatura($idFatura=0) {
        //$css = appConf::root.'plugins/bootstrap/css/bootstrap.min.css';
        
        $this->fatura->setIdFatura($idFatura);
        $this->fatura->selecionar();
        
        $this->cartao->setIdCartao($this->fatura->getIdCartao());
        $this->cartao->selecionar();
        
        $this->set('fatura', $this->fatura);
        $this->set('cartao', $this->cartao);
        
        //$this->render('fatura/vFaturaImprimir');
        $html = utf8_decode($this->renderToString('fatura/vFaturaImprimir'));
        
        //echo $html;
        
        appFunction::gerarPDF($html, $css, 'Fatura_'.$this->cartao->setor->getSetor().'_'.$this->cartao->colaborador->getNome(), 'A4-L');
    }
    

    
}