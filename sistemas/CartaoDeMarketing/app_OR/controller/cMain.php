<?php
require_once('lib/appController.php');
require_once('lib/appFunction.php');
require_once('app/model/mCartao.php');
require_once('app/model/mUsuarioAdmin.php');

class cMain extends appController {
    
    private $idColaborador = 0; //67; //1139;   //413; //977  /495
    private $idCartao = 0; //16; // 215;  // 71;
    
    private $cartao = null;
    private $userAdmin;

    public function __construct() {
        $this->cartao = new mCartao();
        $this->userAdmin = new mUsuarioAdmin();
    }
    
    public function main() {

        $this->idColaborador = appFunction::dadoSessao('id_colaborador');
        
        $this->userAdmin->setIdSetor(appFunction::dadoSessao('id_setor'));
        $this->userAdmin->selecionarSetor();
        
        if ($this->userAdmin->getIdUsuario() > 0) {
            
            $this->location(appConf::root.'admin/listar');
        } else {
            $this->minhasFaturas();
        }

    }
    
    public function minhasFaturas() {
        
        $this->cartao->setIdColaborador($this->idColaborador);
        //$this->cartao->selecionarPorColaborador();
        
        //$this->set('cartao', $this->cartao);
        //$this->set('faturas', $this->cartao->fatura->listarFaturas());
        //$this->render('vMain');
        
        
        $this->set('cartoes', $this->cartao->selecionarPorColaborador());
        //$this->set('faturas', $this->cartao->fatura->listarFaturas());
        $this->render('vMain');
    }
    
    private function listarFaturaEquipe($idColaborador=0) {
        $this->setor->setIdColaborador($idColaborador);
        $this->setor->selecionarPorColaborador();
       
        $equipe = $this->setor->selecionarEquipe();
        $tabela = '';
        
        foreach($equipe as $setor) {
            $tabela .= $this->fatura->listarFaturas($setor->getIdColaborador());
        }
        
        return $tabela;
    }
    
    private function listarFatEquipe($idColaborador) {
        return $this->fatura->listarFaturaEquipe($idColaborador);
    }
    
}

?>