<?php

require_once('lib/appConexao.php');

class mAcao extends appConexao {
    
    private $idAcao;
    private $descricao;
    
    public function __construct() {
        $this->idAcao = 0;
        $this->descricao = '';
    }
    
    public function setIdAcao($valor) {
        $this->idAcao = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $valor;
    }
    
    public function getIdAcao() {
        return $this->idAcao;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function salvar() {
        
    }
    
    public function excluir() {
        
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_ACAO WHERE ID_ACAO = ".$this->idAcao);
        
        
        if(count($rs) > 0) {
            $this->idAcao = $rs[1]['ID_ACAO'];
            $this->descricao = $rs[1]['DESCRICAO'];
        }
    }
    
    public function comboAcao($selecionar=0, $nomeCombo='cmbAcao') {
        $rs = $this->executarQueryArray("SELECT * FROM CM_ACAO ORDER BY DESCRICAO ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_ACAO'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_ACAO'].'" '.$marcar.'>'.$row['DESCRICAO'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    
}

?>