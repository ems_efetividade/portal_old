<?php

require_once('lib/appConexao.php');

class mAtualizar extends appConexao {
    
    
    public function limparTabelaBase() {
        $this->executar('TRUNCATE TABLE [CM_BASE_FATURA]');
    }
    
    public function atualizarNovasFaturas() {
        $this->executar('EXEC proc_cm_cargaFatura');
        $erro = sqlsrv_errors();
        
        if(is_array($erro)) {
           echo $erro[0]['message'].'<br><br>'; 
        }
    }
    
    public function  inserirTabelaBase($dados) {
        $query = "INSERT INTO [CM_BASE_FATURA]
                ([DATA]
                ,[NOME_DO_PORTADOR]
                ,[NUM._FUNCIONAL]
                ,[NUMERO_DO_CARTAO]
                ,[CONTA_MAE]
                ,[TIPO_TRANSACAO]
                ,[DESCRICAO]
                ,[CRED/DEB]
                ,[NOME_DA_MOEDA]
                ,[COD_MOEDA]
                ,[RAMO_DO_ESTAB]
                ,[LOCAL]
                ,[VALOR_DA_TRANSACAO]
                ,[VALOR_EM_DOLARES]
                ,[VALOR_EM_REAIS]
                ,[NOME_DA_COMPANHIA_AEREA]
                ,[NOME_DO_PASSAGEIRO]
                ,[BILHETE1]
                ,[BILHETE2]
                ,[BILHETE3]
                ,[BILHETE4]
                ,[CODIGO_IATA]
                ,[CENTRO_DE_CUSTO]
                ,[NUMERO_DA_AUTORIZACAO]
                ,[CNPJ]
                ,[FATURA])
          VALUES " . $dados;
        
        //return $query;
        
        $this->executar($query);
        $erro = sqlsrv_errors();
        
        if(is_array($erro)) {
           echo $erro[0]['message'].'<br>'.$query.'<br><br>'; 
        }
        
        //echo $query;
        
    }
    
    public function excluirFaturas($mesFatura=0) {
        $query  = "DELETE DA FROM CM_STATUS_DESPESA_ATUAL DA
                    INNER JOIN
                    CM_DESPESA D ON D.ID_DESPESA = DA.ID_DESPESA 
                    INNER JOIN CM_LANCAMENTO L ON L.ID_LANCAMENTO = D.ID_LANCAMENTO 
                    INNER JOIN CM_FATURA F ON F.ID_FATURA = L.ID_FATURA 
                    WHERE 
                            F.MES_FATURA = ".$mesFatura."

                    DELETE D FROM CM_DESPESA D 
                    INNER JOIN CM_LANCAMENTO L ON L.ID_LANCAMENTO = D.ID_LANCAMENTO 
                    INNER JOIN CM_FATURA F ON F.ID_FATURA = L.ID_FATURA 
                    WHERE 
                            F.MES_FATURA = ".$mesFatura."

                    DELETE L FROM CM_LANCAMENTO L 
                    INNER JOIN CM_FATURA F ON F.ID_FATURA = L.ID_FATURA 
                    WHERE F.MES_FATURA = ".$mesFatura."

                    DELETE FROM CM_FATURA WHERE MES_FATURA = ".$mesFatura."
                    ";
        
       
        //echo $query;
         $this->executar($query);
    }
    
}