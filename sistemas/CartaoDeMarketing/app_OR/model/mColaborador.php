<?php

require_once('lib/appConexao.php');

class mColaborador {
    
    private $idColaborador;
    private $nome;
    private $endereco;
    private $nro;
    private $complemento;
    private $bairro;
    private $cidade;
    private $uf;
    private $cep;
    private $email;
    private $rg;
    private $cpf;
    private $matricula;
    private $id;
    private $camiseta;
    private $sapato;
    private $fumante;
    private $estadoCivil;
    private $nascimento;
    private $sexo;
    private $cargo;
    private $dtCadastro;
    private $senha;
    private $status;
    private $aeroporto;
    private $carro;
    private $placa;
    
    private $foneCorp;
    private $foneRes;
    private $fonePart;
    
    private $foto;
    
    private $cn;
    
    public function __construct() {
        $this->idColaborador = '';
        $this->nome = '';
        $this->endereco = '';
        $this->nro = '';
        $this->complemento = '';
        $this->bairro = '';
        $this->cidade = '';
        $this->uf = '';
        $this->cep = '';
        $this->email = '';
        $this->rg = '';
        $this->cpf = '';
        $this->matricula = '';
        $this->id = '';
        $this->camiseta = '';
        $this->sapato = '';
        $this->fumante = '';
        $this->estadoCivil = '';
        $this->nascimento = '';
        $this->sexo = '';
        $this->cargo = '';
        $this->dtCadastro = '';
        $this->senha = '';
        $this->status = '';
        $this->aeroporto = '';
        $this->carro = '';
        $this->placa = '';
        
        $this->foneCorp = '';
        $this->fonePart = '';
        $this->foneRes = '';
        
        $this->foto = '';
        
    }
    
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor; 
    }
    
    public function setNome($valor) {
        $this->nome = $valor; 
    }
    
    public function setEndereco($valor) {
        $this->endereco = $valor; 
    }
    
    public function setNro($valor) {
        $this->nro = $valor; 
    }
    
    public function setComplemento($valor) {
        $this->complemento = $valor;  
    }
    
    public function setBairro($valor) {
        $this->bairro = $valor; 
    }
    
    public function setCidade($valor) {
        $this->cidade = $valor; 
    }
    
    public function setUf($valor) {
        $this->uf = $valor; 
    }
    
    public function setCep($valor) {
        $this->cep = $valor;     
    }
    
    public function setEmail($valor) {
        $this->email = $valor;     
    }
    
    public function setRg($valor) {
        $this->rg = $valor; 
    }
    
    public function setCpf($valor) {
        $this->cpf = $valor; 
    }
    
    public function setMatricula($valor) {
        $this->matricula = $valor; 
    }
    
    public function setId($valor) {
        $this->id = $valor;        
    }
    
    public function setCamiseta($valor) {
        $this->camiseta = $valor;         
    }
    
    public function setSapato($valor) {
        $this->sapato = $valor;        
    }
    
    public function setFumante($valor) {
        $this->fumante = $valor; 
    }
    
    public function setEstadocivil($valor) {
        $this->estadoCivil = $valor; 
    }
    
    public function setNascimento($valor) {
        $this->nascimento = $valor; 
    }
    
    public function setSexo($valor) {
        $this->sexo = $valor; 
    }
    
    public function setCargo($valor) {
        $this->cargo = $valor; 
    }
    
    public function setDtcadastro($valor) {
        $this->dtCadastro = $valor; 
    }
    
    public function setSenha($valor) {
        $this->senha = $valor; 
    }
    
    public function setStatus($valor) {
        $this->status = $valor; 
    }
    
    public function setAeroporto($valor) {
        $this->aeroporto = $valor; 
    }
    
    public function setCarro($valor) {
        $this->carro = $valor; 
    }
    
    public function setPlaca($valor) {
        $this->placa = $valor; 
    }

    
    public function getIdColaborador() {
        return $this->idColaborador; 
    }
    
    public function getNome() {
        return $this->nome; 
    }
    
    public function getEndereco() {
        return $this->endereco;  
    }
    
    public function getNro() {
        return $this->nro; 
    }
    
    public function getComplemento() {
        return $this->complemento; 
    }
    
    public function getBairro() {
        return $this->bairro; 
    }
    
    public function getCidade() {
        return $this->cidade; 
    }
    
    public function getUf() {
        return $this->uf; 
    }
    
    public function getCep() {
        return $this->cep; 
    }
    
    public function getEmail() {
        return $this->email; 
    }
    
    public function getRg() {
        return $this->rg; 
    }
    
    public function getCpf() {
        return $this->cpf; 
    }
    
    public function getMatricula() {
        return $this->matricula; 
    }
    
    public function getId() {
        return $this->id; 
    }
    
    public function getCamiseta() {
        return $this->camiseta;  
    }
    
    public function getSapato() {
        return $this->sapato; 
    }
    
    public function getFumante() {
        return $this->fumante; 
    }
    
    public function getEstadocivil() {
        return $this->estadoCivil; 
    }
    
    public function getNascimento() {
        return $this->nascimento; 
    }
    
    public function getSexo() {
        return $this->sexo; 
    }
    
    public function getCargo() {
        return $this->cargo;  
    }
    
    public function getDtcadastro() {
        return $this->dtCadastro; 
    }
    
    public function getSenha() {
        return $this->senha;  
    }
    
    public function getStatus() {
        return $this->status;  
    }
    
    public function getAeroporto() {
        return $this->aeroporto;  
    }
    
    public function getCarro() {
        return $this->carro; 
    }
    
    public function getPlaca() {
        return $this->placa; 
    }
    
    public function getFoneResidencial() {
        return $this->foneRes;
    }
    
    public function getFoneCorp() {
        return $this->foneCorp;
    }
    
    public function getFoneParticular() {
        return $this->fonePart;
    }
    
    public function getFoto() {        
        return $this->foto;
    }

    public function selecionar() {
        $this->cn = new appConexao();
        $rs = $this->cn->executarQueryArray("SELECT * FROM COLABORADOR WHERE ID_COLABORADOR = ".$this->idColaborador);
        unset($this->cn);
        
        $this->popularVariaveis($rs);
    }

    
    
    private function popularVariaveis($rs) {
        $this->idColaborador = $rs[1]['ID_COLABORADOR'];
        $this->nome          = $rs[1]['NOME'];
        $this->endereco = $rs[1]['ENDERECO'];
        $this->nro = $rs[1]['NRO'];
        $this->complemento = $rs[1]['COMPLEMENTO'];
        $this->bairro = $rs[1]['BAIRRO'];
        $this->cidade = $rs[1]['CIDADE'];
        $this->uf = $rs[1]['UF'];
        $this->cep = $rs[1]['CEP'];
        $this->email = $rs[1]['EMAIL'];
        $this->rg = $rs[1]['RG'];
        $this->cpf = $rs[1]['CPF'];
        $this->matricula = $rs[1]['MATRICULA'];
        $this->id = $rs[1]['ID'];
        $this->camiseta = $rs[1]['CAMISETA'];
        $this->sapato = $rs[1]['SAPATO'];
        $this->fumante = $rs[1]['FUMANTE'];
        $this->estadoCivil = $rs[1]['ESTADO_CIVIL'];
        $this->nascimento = $rs[1]['NASCIMENTO'];
        $this->sexo = $rs[1]['SEXO'];
        $this->cargo = $rs[1]['CARGO'];
        $this->dtCadastro = $rs[1]['DT_CADASTRO'];
        $this->senha = $rs[1]['SENHA'];
        $this->status = $rs[1]['STATUS'];
        $this->aeroporto = $rs[1]['AEROPORTO'];
        $this->carro = $rs[1]['CARRO'];
        $this->placa = $rs[1]['PLACA'];
        
        $this->cn = new appConexao();
        $rs = $this->cn->executarQueryArray("SELECT
                                                    MAX(CASE WHEN TIPO = 'R' THEN '(' + DDD + ') ' + FONE ELSE NULL END) AS FONE_RES,
                                                    MAX(CASE WHEN TIPO = 'C' THEN '(' + DDD + ') ' + FONE ELSE NULL END) AS FONE_CORP,
                                                    MAX(CASE WHEN TIPO = 'P' THEN '(' + DDD + ') ' + FONE ELSE NULL END) AS FONE_PART
                                            FROM
                                                    TELEFONE
                                            WHERE ID_COLABORADOR = ".($this->idColaborador+0)."");
        
        
        $this->foneCorp = $rs[1]['FONE_CORP'];
        $this->fonePart = $rs[1]['FONE_PART'];
        $this->foneRes = $rs[1]['FONE_RES'];
        
        
        $rs = $this->cn->executarQueryArray("SELECT FOTO FROM FOTO_PERFIL WHERE ID_COLABORADOR = ".($this->idColaborador+0));
        
        $this->foto = $rs[1]['FOTO'];
        
        unset($this->cn);
        
    }
    

    
}