<?php

require_once('lib/appConexao.php');
require_once('app/model/mLancamento.php');
require_once('app/model/mProdutoDespesa.php');
require_once('app/model/mMedico.php');
require_once('app/model/mTipoDespesa.php');
require_once('app/model/mAcao.php');
require_once('app/model/mProjeto.php');
require_once('app/model/mCidade.php');
require_once('app/model/mStatusDespesaAtual.php');
require_once('app/model/mColaboradorSetor.php');
require_once('app/model/mNotaFiscal.php');

class mDespesa extends appConexao {
    
    private $idDespesa;
    private $idTipoDespesa;
    private $idLancamento;
    private $idProjeto;
    private $idAcao;
    private $NF;
    private $dataNF;
    private $planejado;
    private $realizado;
    private $localEvento;
    private $idCidade;
    private $descricao;
    private $dataJustificativa;
    private $arquivoNF;
    private $arquivoNfMD5;
    private $arquivo;
    private $justificativaFds;
    private $despesaMkt;
    private $idSetorPag;
    private $idColabPag;
    
    private $setorPag;
    private $colabPag;
    
    private $arrProdutos;
    private $arrMedicos;
    private $arrNF;
    private $arrNFExcluir;
    
    
    public $tipoDespesa;
    public $acao;
    public $projeto;
    public $produto;
    public $medico;
    public $lancamento;
    public $cidade;
    public $status;
    public $colaborador;
    public $notaFiscal;
    
    public function __construct() {
        $this->idDespesa         = 0;
        $this->idTipoDespesa     = 0;
        $this->idLancamento      = 0;
        $this->idProjeto         = 0;
        $this->idAcao            = 0;
        $this->NF                = '';
        $this->dataNF            = '';
        $this->planejado         = 0;
        $this->realizado         = 0;
        $this->localEvento       = '';
        $this->idCidade          = 0;
        $this->descricao         = '';
        $this->dataJustificativa = '';
        $this->arquivoNF         = '';
        $this->arquivoNfMD5      = '';
        $this->justificativaFds  = '';
        $this->despesaMkt        = 0;
        $this->idSetorPag        = 0;
        $this->idColabPag        = 0;
        
        $this->arrMedicos   = array();
        $this->arrProdutos  = array();
        $this->arrNF        = array();
        
        $this->produto      = new mProdutoDespesa();
        $this->medico       = new mMedico();
        $this->acao         = new mAcao();
        $this->projeto      = new mProjeto();
        $this->status       = new mStatusDespesaAtual();
        $this->cidade       = new mCidade();
        $this->tipoDespesa  = new mTipoDespesa();
        $this->colaborador  = new mColaboradorSetor();
        $this->notaFiscal   = new mNotaFiscal();
    }
    
    public function setIdDespesa($valor) {
        $this->idDespesa = $valor;
    }
    
    public function setIdTipoDespesa($valor) {
        $this->idTipoDespesa = $valor;
    }
    
    public function setIdLancamento($valor) {
        $this->idLancamento = $valor;
    }
    
    public function setIdProjeto($valor) {
        $this->idProjeto = $valor;
    }
    
    public function setIdAcao($valor) {
        $this->idAcao = $valor;
    }
    
    public function setNotaFiscal($valor) {
        $this->NF = $valor;
    }
    
    public function setDataNotaFiscal($valor) {
        $this->dataNF = appFunction::formatarData($valor);
    }
    
    public function setPlanejado($valor) {
        $this->planejado = appFunction::formatarMoedaSQL($valor);
    }
    
    public function setRealizado($valor) {
        $this->realizado = appFunction::formatarMoedaSQL($valor);
    }
    
    public function setLocalEvento($valor) {
        $this->localEvento = $this->removerAspas(utf8_decode($valor));
    }
    
    public function setIdCidade($valor) {
        $this->idCidade = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $this->removerAspas(utf8_decode(trim($valor)));
    }
    
    public function setArquivoNF($arquivo) {
        $this->arquivoNF = $arquivo;
    }
    
    public function setArquivoNFMD5($valor) {
        $this->arquivoNfMD5 = $valor;
    }
    
    public function setJustificativaFimSemana($valor) {
        $this->justificativaFds = $this->removerAspas(utf8_decode($valor));
    }
    
    public function setDespesaMkt($valor) {
        $this->despesaMkt = $valor;
    }
    
    public function setProdutos($arr) {
        $this->arrProdutos[] = $arr;
    }
    
    public function setMedicos($arr) {
        $this->arrMedicos[] = $arr;
    }   
    
    public function setIdSetorPag($valor) {
        $this->idSetorPag = $valor;
    }  
    
    public function setIdColaboradorPag($valor) {
        $this->idColabPag = $valor;
    } 
    
    
    public function setNotasFiscais($valor) {
        $this->arrNF[] = $valor;
    }
    
    public function setNotasFiscaisExcluir($valor) {
        $this->arrNFExcluir[] = $valor;
    }    
    
    
    public function getIdDespesa() {
        return $this->idDespesa;
    }
    
    public function getIdTipoDespesa() {
        return $this->idTipoDespesa;
    }
    
    public function getIdLancamento() {
        return $this->idLancamento;
    }
    
    public function getIdProjeto() {
        return $this->idProjeto;
    }
    
    public function getIdAcao() {
        return $this->idAcao;
    }
    
    public function getNotaFiscal() {
        return $this->NF;
    }
    
    public function getJustificativaFimSemana() {
        return $this->justificativaFds;
    }
    
    public function getDataNotaFiscal() {
        return appFunction::formatarData($this->dataNF);
    }
    
    public function getPlanejado() {
        return appFunction::formatarMoedaBRL($this->planejado,2);
    }
    
    public function getRealizado() {
        return appFunction::formatarMoedaBRL($this->realizado,2);
    }
    
    
    public function getLocalEvento() {
        return $this->adicionarAspas($this->localEvento);
    }
    
    public function getIdCidade() {
        return $this->idCidade;
    }
    
    public function getDescricao() {
        return $this->adicionarAspas($this->descricao);
    }
    
    public function getDataJustificativa() {
        return $this->dataJustificativa;
    }
    
    public function getArquivoNF() {
        return $this->arquivoNF;
    }
    
    public function getArquivoNFMD5() {
        return $this->arquivoNfMD5;
    }
    
    public function getDespesaMkt() {
        return $this->despesaMkt;
    }
    
    public function getIdSetorPag() {
        return $this->idSetorPag;
    }  
    
    public function getIdColaboradorPag() {
        return $this->idColabPag;
    }  
    
    public function getSetorPag() {
        return $this->setorPag;
    }  
    
    public function getColaboradorPag() {
        return $this->colabPag;
    }   

    
    
      
    private function validarDados() {
        $validar = '';
        
        if($this->idTipoDespesa == 1) {
            
            if($this->NF == 0 || $this->NF == "") {
                $validar = 'Por favor, digitar o número da nota fiscal';
                return $validar;
            }
            
            if($this->dataNF == '') {
                $validar = 'Por favor, digitar a da nota fiscal';
                return $validar;
            }
            
            if($this->idAcao == 0) {
                $validar = 'Por favor, selecionar a ação';
                return $validar;
            }
            
            if($this->idProjeto == 0) {
                $validar = 'Por favor, selecione o projeto';
                return $validar;
            }
            
            if($this->planejado == 0) {
                $validar = 'Por favor, digite o valor planejado';
                return $validar;
            }
            
            if($this->realizado == 0) {
                $validar = 'Por favor, digite o valor realizado';
                return $validar;
            }
            
            if($this->localEvento == '') {
                $validar = 'Por favor, digite o local do evento';
                return $validar;
            }
            
            if($this->idCidade == 0) {
                $validar = 'Por favor, digite a cidade';
                return $validar;
            }
            
            
            if(count($this->arrProdutos) == 0) {
                $validar = 'Por favor, indique os produtos para essa despesa';
                return $validar;
            } else {
                $soma = 0;
                foreach($this->arrProdutos as $produto) {
                    $soma = $soma + $produto->getParticipacao();
                }
                
                if($soma <> 100) {
                    $validar = 'A soma dos produtos deve ser 100%';
                    return $validar;
                }
            }
            
            
            
            
            
            if(count($this->arrMedicos) == 0) {
                $validar = 'Por favor, indique os médicos para essa despesa';
                return $validar;
            }
            
            
            
            
        } else {
            if($this->idTipoDespesa == 0) {
                $validar = 'Por favor, selecione o tipo de despesa.';
                return $validar;
            }
            
            if($this->descricao == '') {
                $validar = 'Por favor, digite uma descrição!';
                return $validar;
            }            
        }
    }
    
    public function salvar() {
        $validar = $this->validarDados();
        if($validar == '') {
            
            $query = "EXEC proc_cm_despesaSalvar '".$this->idDespesa."' , '".$this->idTipoDespesa."'
                ,'".$this->idLancamento."' 
                ,'".$this->idProjeto."'
                ,'".$this->idAcao."'
                ,'".$this->idCidade."'
                ,'".$this->NF."'
                ,'".$this->dataNF."'
                ,'".$this->planejado."'
                ,'".$this->realizado."'
                ,'".$this->localEvento."'                        
                ,'".$this->descricao."'
                ,'".$this->arquivoNF."'
                ,'".$this->arquivoNfMD5."'
                ,'".$this->justificativaFds."'
                ,'".$this->despesaMkt."'
                ,'".$this->idSetorPag."'
                ,'".$this->idColabPag."'";
            $rs = $this->executarQueryArray($query); 
            
            $idDespesa = $rs[1]['ID'];
            
            $this->status->setIdDespesa($idDespesa);
            $this->status->setIdStatusDespesa(1);
            $this->status->salvar();
            
            if(count($this->produto) > 0) {
                $this->produto->setIdDespesa($idDespesa);
                $this->produto->excluirPorDespesa();

                foreach($this->arrProdutos as $produto) {
                    $produto->setIdDespesa($idDespesa);
                    $produto->salvar();
                }
            }

            if(count($this->medico) > 0) {
                $this->medico->setIdDespesa($idDespesa);
                $this->medico->excluirPorDespesa();

                foreach($this->arrMedicos as $medico) {
                    $medico->setIdDespesa($idDespesa);
                    $medico->salvar();
                }
            }
            
            if(count($this->arrNF) > 0) {
                foreach($this->arrNF as $nf) {
                    $nf->setIdDespesa($idDespesa);
                    $nf->salvar();
                }
            }
            
            if(count($this->arrNFExcluir) > 0) {
                foreach($this->arrNFExcluir as $nf) {
                    //$nf->setIdDespesa($idDespesa);
                    $nf->excluir();
                }
            }            
            
        } else {
            return $validar;
        }

    }
    
    public function excluir() {
        
        $des = new mDespesa();
        $des->setIdDespesa($this->idDespesa);
        $des->selecionar();
        
        $this->executar("EXEC proc_cm_despesaExcluir ".$this->idDespesa);
        
        //if(file_exists(appConf::nfFolderRoot.$des->getArquivoNFMD5())) {
        //    unlink(appConf::nfFolderRoot.$des->getArquivoNFMD5());
        //}
        
        $this->notaFiscal->setIdDespesa($this->idDespesa);
        $this->notaFiscal->excluirTudo();
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_despesaSelecionar ".$this->idDespesa);
        $this->popularVariaveis($rs);
    }
    
    public function selecionarPorLancamento() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_DESPESA WHERE ID_LANCAMENTO = ".$this->idLancamento);
        $this->popularVariaveis($rs);
    }
    
    public function listar($tipo=0, $idFatura) {
        
        $rs = $this->executarQueryArray("proc_cm_despesaListar ".$tipo.", ".$idFatura);
        
        $despesas = array();
        
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $despesa = new mDespesa;
                $despesa->setIdDespesa($row['ID_DESPESA']);
                $despesa->selecionar();

                $despesas[] = $despesa;
            }
        }
        
        return $despesas;
    }
    
    public function listarPorTipo($idTipo=0, $idFatura) {
        $rs = $this->executarQueryArray("SELECT A.ID_DESPESA FROM CM_DESPESA A INNER JOIN CM_LANCAMENTO B ON B.ID_LANCAMENTO = A.ID_LANCAMENTO WHERE A.ID_TIPO_DESPESA = ".$idTipo." AND B.ID_FATURA = ".$idFatura);
        
        
        $despesas = array();
        
        if(count($rs) > 0) {
        foreach($rs as $row) {
            $despesa = new mDespesa;
            $despesa->setIdDespesa($row['ID_DESPESA']);
            $despesa->selecionar();
            
            $despesas[] = $despesa;
        }
        
        
        }
        
        
    }
    
    public function listarOutrasDespesas($idTipo=0, $idFatura) {
        $rs = $this->executarQueryArray("SELECT A.ID_DESPESA FROM CM_DESPESA A INNER JOIN CM_LANCAMENTO B ON B.ID_LANCAMENTO = A.ID_LANCAMENTO WHERE A.ID_TIPO_DESPESA > 1 AND B.ID_FATURA = ".$idFatura);
        $despesas = array();
        
        if(count($rs) > 0) {
        foreach($rs as $row) {
            $despesa = new mDespesa;
            $despesa->setIdDespesa($row['ID_DESPESA']);
            $despesa->selecionar();
            
            $despesas[] = $despesa;
        }
        
        return $despesas;
        }
        
    }
    
    private function removerAspas($texto) {
        return str_replace("'", "`", $texto);
    }
    
    private function adicionarAspas($texto) {
        return str_replace("`", "'", $texto);
    }    
    
    public function exportarLista($mesFatura) {
        return $this->executarQueryArray("EXEC proc_cm_exportarListaLancamento ".$mesFatura."");
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idDespesa            = $rs[1]['ID_DESPESA'];
            $this->idTipoDespesa        = $rs[1]['ID_TIPO_DESPESA'];
            $this->idLancamento         = $rs[1]['ID_LANCAMENTO'];
            $this->idProjeto            = $rs[1]['ID_PROJETO'];
            $this->idAcao               = $rs[1]['ID_ACAO'];
            $this->NF                   = $rs[1]['NF'];
            $this->dataNF               = $rs[1]['DATA_NF'];
            $this->planejado            = $rs[1]['PLANEJADO'];
            $this->realizado            = $rs[1]['REALIZADO'];
            $this->estabelecimento      = $rs[1]['ESTABELECIMENTO'];
            $this->localEvento          = $rs[1]['LOCAL_EVENTO'];
            $this->idCidade             = $rs[1]['ID_CIDADE'];
            $this->descricao            = $rs[1]['DESCRICAO'];
            $this->dataJustificativa    = $rs[1]['DATA_JUSTIFICATIVA'];
            $this->arquivoNF            = $rs[1]['ARQUIVO_NF'];
            $this->arquivoNfMD5         = $rs[1]['ARQUIVO_NF_MD5'];
            $this->justificativaFds     = $rs[1]['JUSTIFICATIVA_FDS'];
            $this->despesaMkt           = $rs[1]['DESPESA_MKT'];
            $this->idSetorPag           = $rs[1]['ID_SETOR_PAG'];
            $this->idColabPag           = $rs[1]['ID_COLAB_PAG'];
            
            
            $this->colaborador->setIdColaborador($this->idColabPag);
            $this->colaborador->selecionarPorColaborador();
            $this->colabPag = $this->colaborador->getColaborador();   
            
            $this->colaborador->setIdSetor($this->idSetorPag);
            $this->colaborador->selecionar();
            $this->setorPag = $this->colaborador->getSetor();              
            
            //$this->lancamento = new mLancamento();
            //$this->lancamento->setIdLancamento($this->idLancamento);
            //$this->lancamento->selecionar();
            
            $this->tipoDespesa = new mTipoDespesa();
            $this->tipoDespesa->setIdTipoDespesa($this->idTipoDespesa);
            $this->tipoDespesa->selecionar();
            
            $this->acao->setIdAcao($this->idAcao);
            $this->acao->selecionar();
            
            $this->projeto->setIdProjeto($this->idProjeto);
            $this->projeto->selecionar();
            
            $this->medico = new mMedico();
            $this->medico->setIdDespesa($this->idDespesa);
            
            
            $this->produto->setIdDespesa($this->idDespesa);  
            
            $this->cidade = new mCidade();
            $this->cidade->setIdCidade($this->idCidade);
            $this->cidade->selecionar();
            
            
            $this->status->setIdDespesa($this->idDespesa);
            $this->status->selecionar();
            
            
            $this->notaFiscal->setIdDespesa($this->idDespesa);

            
            
        }
    }
    
    
    public function excluirArquivoNF($id=0) {
        
        if($id != '') {
            $des = new mDespesa();
            $des->setIdDespesa($id);
            $des->selecionar();

            if($des->getArquivoNFMD5() != "") {
                if(file_exists(appConf::nfFolderRoot.$des->getArquivoNFMD5())) {
                    unlink(appConf::nfFolderRoot.$des->getArquivoNFMD5());
                }
            }
        }
    }
}

?>