<?php
require_once('lib/appConexao.php');

require_once('app/model/mLancamento.php');
require_once('app/model/mStatusAtual.php');

class mFatura extends appConexao {
    
    private $idFatura;
    private $idCartao;
    private $mesFatura;
    private $dataRecebimento;
    private $dataCadastroRecebimento;
    
    private $totalFatura;
    private $arrMes = array(1 => 'Jan',
                            2 => 'Fev',
                            3 => 'Mar',
                            4 => 'Abr',
                            5 => 'Mai',
                            6 => 'Jun',
                            7 => 'Jul',
                            8 => 'Ago',
                            9 => 'Set',
                            10 => 'Out',
                            11 => 'Nov',
                            12 => 'Dez');
    
    public $lancamento;
    public $statusFatura;
    
    public function __construct() {
        $this->idFatura = 0;
        $this->idCartao = 0;
        $this->mesFatura = 0;
        $this->dataRecebimento = '';
        $this->dataCadastroRecebimento = '';
        
        $this->lancamento = new mLancamento();
        $this->statusFatura = new mStatusAtual();
    }
    
    public function setIdFatura($valor) {
        $this->idFatura = $valor;
    }
       
    public function setIdCartao($valor) {
        $this->idCartao = $valor;
    }
    
    public function setMesfatura($valor) {
        $this->mesFatura = $valor;
    }
    
    public function setDataRecebimento($valor) {
        $this->dataRecebimento = $valor;
    }
    
    public function getIdFatura() {
        return $this->idFatura;
    }
    
    public function getIdCartao() {
        return $this->idCartao;
    }
    
    public function getDataRecebimento() {
        return $this->dataRecebimento;
    }
    
    public function getCadastroDataRecebimento() {
        return $this->dataCadastroRecebimento;
    }    
    
    public function getMesFatura() {
        if($this->mesFatura != "") {
            if(strlen($this->mesFatura) == 5) {
                $mes = $this->arrMes[substr($this->mesFatura, 0, 1)].'/'.substr($this->mesFatura, 1, 4);
            } else {
                $mes = $this->arrMes[substr($this->mesFatura, 0, 2)].'/'.substr($this->mesFatura, 2, 4);
            }

            return $mes;
        }
    }
    
    public function getTotalFatura() {
        return $this->totalFatura;
    }
    
    public function getMaxMesFatura() {
        $rs = $this->executarQueryArray("select MAX(mes_fatura) AS MES_FATURA from CM_FATURA");
        return $rs[1]['MES_FATURA'];
    }
    
    public function getDespesasPendentes() {
        $query = 'SELECT
                        COUNT(*) AS PENDENTE  FROM
                CM_FATURA F 
                INNER JOIN CM_LANCAMENTO L ON L.ID_FATURA = F.ID_FATURA
                INNER JOIN CM_DESPESA D ON D.ID_LANCAMENTO = L.ID_LANCAMENTO
                INNER JOIN CM_STATUS_DESPESA_ATUAL DS ON DS.ID_DESPESA = D.ID_DESPESA
                WHERE DS.ATIVO = 1 AND F.ID_FATURA = '.$this->idFatura.' AND DS.ID_STATUS_DESPESA = 1';
        
        $rs = $this->executarQueryArray($query);
        return $rs[1]['PENDENTE'];
        
    }    
    
    public function getNumRows($criterios) {
        $preQuery = '';
        
        if($criterios['mesFatura'] != 0) {
            $preQuery .= ' AND F.MES_FATURA = '.$criterios['mesFatura'];
        }
        
        if($criterios['idStatus'] != 0) {
            $preQuery .= ' AND A.ID_STATUS_FATURA = '.$criterios['idStatus'];
        }
        
        if($criterios['recebido'] != 0) {
            $preQuery .= ' AND F.DATA_RECEBIMENTO IS '.$criterios['recebido'];
        }        
        
        if($criterios['campos'] != '') {
            $preQuery .= " AND ".$criterios['campos']." LIKE '%".$criterios['criterio']."%'";
        }
        
//        if($criterios['criterio'] != '') {
//            $preQuery .= " AND V.SETOR + ' ' + V.NOME LIKE '%".$criterios['criterio']."%'";
//        }
        
        $query = "SELECT 
                    COUNT(*) AS ROWS
                    FROM CM_FATURA F 
                    INNER JOIN  CM_STATUS_ATUAL A ON A.ID_FATURA = F.ID_FATURA
                    INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
                    INNER JOIN COLABORADOR V ON V.ID_COLABORADOR = C.ID_COLABORADOR
                    INNER JOIN SETOR S ON S.ID_SETOR = C.ID_SETOR
                    WHERE 
                            A.ORDEM = 1 ".$preQuery."";
        
        $rs = $this->executarQueryArray($query);
        return $rs[1]['ROWS'];
    }
    
    public function getTotalStatusFatura() {
        $qy = "SELECT 
                        S.DATA_CADASTRO, 
                        SF.DESCRICAO ,
                        ST.SETOR + ' ' + C.NOME AS APROVADO_POR
                FROM CM_STATUS_ATUAL S 
                INNER JOIN CM_STATUS_FATURA SF ON SF.ID_STATUS_FATURA = S.ID_STATUS_FATURA
                INNER JOIN SETOR ST ON ST.ID_SETOR = S.ID_SETOR_APROVADOR
                INNER JOIN COLABORADOR C ON C.ID_COLABORADOR = S.ID_COLABORADOR_APROVADOR
                WHERE S.ID_FATURA = ".$this->idFatura." AND ID_SETOR_APROVADOR IS NOT NULL";
        return $this->executarQueryArray($qy);
    }
    
    public function getNumTotalFaturas() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS NUM FROM CM_FATURA");
        return $rs[1]['NUM'];
    }
    
    public function salvar($idSetor) {
        $this->executar("EXEC proc_cm_faturaSalvar ".$this->idFatura.", ".$idSetor."");
        //$this->executar("EXEC proc_cm_faturaAprovar ".$this->idFatura.", '".$idSetor."'");
    }
    
    public function aprovar($idSetor='') {
        $this->executar("EXEC proc_cm_faturaAprovar ".$this->idFatura.", '".$idSetor."', 0");
    }
    
    public function aprovarEmLote($idSetor=0, $idSetorLogado=0, $mesFatura=0) {
        $this->executar("EXEC proc_cm_faturaAprovarEmLote '".$idSetor."', '".$idSetorLogado."', '".$mesFatura."'");
    }
    
    public function confirmarRecebimento() {
        $and = '';
        
        $f = new mFatura();
        $f->setIdFatura($this->idFatura);
        
        $f->selecionar();
        
        if($f->getCadastroDataRecebimento() == null) {
            $and = ", DATA_CADASTRO_RECEBIMENTO = '".date('Y-m-d H:i:s')."'";
        }
        
        $query = "UPDATE CM_FATURA SET DATA_RECEBIMENTO = '".$this->dataRecebimento."' ".$and." WHERE ID_FATURA = ".$this->idFatura;
        
        $this->executar($query);

        
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_FATURA WHERE ID_FATURA = ".$this->idFatura);
        
        $this->popularVariaveis($rs);
    }
    
    public function listarFaturas() {
        $rs = $this->executarQueryArray("SELECT ID_FATURA FROM CM_FATURA WHERE ID_CARTAO = ".$this->idCartao." ORDER BY MES_FATURA DESC");
        $faturas = array();
        
        if(count($rs) > 0) {
            foreach($rs as $i => $row) {
                $fatura = new mFatura();
                $fatura->setIdFatura($rs[$i]['ID_FATURA']);
                $fatura->selecionar();
                $faturas[] = $fatura;

            }
        }
        return $faturas;
    }
    
    public function listarTudo($de=1, $ate=20, $criterios='') {
        
        $preQuery = '';
        
        if($criterios['mesFatura'] != 0) {
            $preQuery .= ' AND F.MES_FATURA = '.$criterios['mesFatura'];
        }
        
        if($criterios['idStatus'] != 0) {
            $preQuery .= ' AND A.ID_STATUS_FATURA = '.$criterios['idStatus'];
        }
        
        if($criterios['recebido'] != '') {
            $preQuery .= ' AND F.DATA_RECEBIMENTO IS '.$criterios['recebido'];
        }           
        
        if($criterios['campos'] != '') {
            $preQuery .= " AND ".$criterios['campos']." LIKE '%".$criterios['criterio']."%'";
        }
        
//        if($criterios['criterio'] != '') {
//            $preQuery .= " AND V.SETOR + ' ' + V.NOME LIKE '%".$criterios['criterio']."%'";
//        }
        
        $query = "SELECT A.* FROM (
                    SELECT 
                            ROW_NUMBER() OVER (ORDER BY A.DATA_CADASTRO DESC) AS INDICE,
                            F.ID_FATURA
                    FROM CM_FATURA F 
                    INNER JOIN  CM_STATUS_ATUAL A ON A.ID_FATURA = F.ID_FATURA
                    INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
                    INNER JOIN COLABORADOR V ON V.ID_COLABORADOR = C.ID_COLABORADOR
                    INNER JOIN SETOR S ON S.ID_SETOR = C.ID_SETOR
                    WHERE 
                            A.ORDEM = 1 ".$preQuery."
            ) A WHERE A.INDICE BETWEEN ".$de." AND ".$ate." GROUP BY A.ID_FATURA, A.INDICE";
        
        
        
        $rs = $this->executarQueryArray($query);
        $faturas = array();
        
        if(count($rs) > 0) {
            foreach($rs as $i => $row) {
                $fatura = new mFatura();
                $fatura->setIdFatura($rs[$i]['ID_FATURA']);
                $fatura->selecionar();
                $faturas[] = $fatura;

            }
        }
        return $faturas;
        
    }
    
    public function exportar($criterios) {
        $preQuery = '';
        
        if($criterios['mesFatura'] != 0) {
            $preQuery .= ' AND F.MES_FATURA = '.$criterios['mesFatura'];
        }
        
        if($criterios['idStatus'] != 0) {
            $preQuery .= ' AND A.ID_STATUS_FATURA = '.$criterios['idStatus'];
        }
        
        if($criterios['criterio'] != '') {
            $preQuery .= " AND V.SETOR + ' ' + V.NOME LIKE '%".$criterios['criterio']."%'";
        }
        
        $query = "SELECT 
                        V.SETOR,
                        V.NOME AS COLABORADOR,
                        V.EMAIL,
                        V.FONE_CORPORATIVO,
                        C.CARTAO,
                        F.MES_FATURA,
                        SUM(L.VALOR) AS TOTAL_FATURA,
                        SF.DESCRICAO,
						CONVERT(DATE, F.DATA_RECEBIMENTO) AS RECEBIDO
                FROM CM_FATURA F 
                INNER JOIN CM_STATUS_ATUAL A ON A.ID_FATURA = F.ID_FATURA
                INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
                INNER JOIN vw_colaboradorSetor V ON V.ID_COLABORADOR = C.ID_COLABORADOR
                INNER JOIN CM_LANCAMENTO L ON L.ID_FATURA = F.ID_FATURA
                INNER JOIN CM_STATUS_FATURA SF ON SF.ID_STATUS_FATURA = A.ID_STATUS_FATURA
                WHERE 
                        A.ORDEM = 1 ".$preQuery."
                GROUP BY
                        V.SETOR,
                    V.NOME,
                    V.EMAIL,
                    C.CARTAO,
                    V.FONE_CORPORATIVO,
                    F.MES_FATURA,
                    SF.DESCRICAO,
					F.DATA_RECEBIMENTO";
       
        return $this->executarQueryArray($query);
        
    }
    
    public function exportarTotalLinha($mesFatura) {
        return $this->executarQueryArray("EXEC proc_cm_exportarTotalLancamento ".$mesFatura."");
    }
    
    public function getMesesFatura() {
        $rs = $this->executarQueryArray("select TOP 12 MES_FATURA from cm_fatura GROUP BY MES_FATURA ORDER BY MES_FATURA DESC");
        $a = array();
        foreach($rs as $dado) {
            $f = new mFatura();
            $f->setMesfatura($dado['MES_FATURA']);
            $a[] = $f;
        }
        
        return $a;
    }
    
    public function comboMesFatura($selecionar=0, $nomeCombo='cmbMesFatura', $size='input-sm') {
        $rs = $this->executarQueryArray("select MES_FATURA from cm_fatura GROUP BY MES_FATURA ORDER BY MES_FATURA DESC");
        
        $select = '<select class="form-control '.$size.'" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option value="0">:: MÊS FATURA ::</option>';
        
        $fat = new mFatura();
        
        foreach($rs as $row) {
            
            $fat->setMesfatura($row['MES_FATURA']);
            
            $marcar = ($row['MES_FATURA'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['MES_FATURA'].'" '.$marcar.'>'.$fat->getMesFatura().'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function listarFaturaEquipe($idColaborador, $perfil, $mesFatura, $status) {
        
        
        $qy = "EXEC proc_cm_faturaListarFaturaEquipe ".$idColaborador.", ".$perfil.", ".$mesFatura.", ".$status;
        //echo $qy;
        
        $rs = $this->executarQueryArray($qy);        
        
        $faturas = array();

     
        if(count($rs) > 0) {
        
        
        foreach($rs as $row) {
            $fatura = new mFatura();
            $fatura->setIdFatura($row['ID_FATURA']);
            $fatura->selecionar();
            
            $faturas[] = $fatura; 
        }

        }
        return $faturas;
    } 
    
    
    public function listarFaturaEquipe2($idCola) {

        $qy = "
SELECT V.SETOR, V.ID_COLABORADOR, V.ID_SETOR, ISNULL(F.ID_FATURA,0) AS ID_FATURA FROM vw_colaboradorSetor V
LEFT JOIN CM_CARTAO C ON C.ID_COLABORADOR = V.ID_COLABORADOR
LEFT JOIN CM_FATURA F ON F.ID_CARTAO = C.ID_CARTAO
WHERE V.NIVEL = ".$idSetor."

ORDER BY V.SETOR ASC";
        
        $qy = "SELECT ID_FATURA FROM (

                SELECT MAX(MES_FATURA) AS MAXIMO FROM CM_FATURA

        ) S INNER JOIN CM_FATURA F ON F.MES_FATURA = S.MAXIMO
        INNER JOIN CM_CARTAO C ON C.ID_CARTAO = F.ID_CARTAO
        WHERE C.ID_COLABORADOR = ".$idCola."";
        
        $rs = $this->executarQueryArray($qy);        
        
        $faturas = array();

     
        if(count($rs) > 0) {
        
        
        foreach($rs as $row) {
            $fatura = new mFatura();
            $fatura->setIdFatura($row['ID_FATURA']);
            $fatura->selecionar();
            
            $faturas[] = $fatura; 
        }

        }
        return $faturas;
    } 
    
    public function totalFaturaEquipe($idSetor, $mesFatura=0) {
        $qy = "EXEC proc_cm_faturaTotalEquipe '".$idSetor."', '".$mesFatura."'";
        //echo $qy;
        return $this->executarQueryArray($qy);
    }
    public function selecionarFaturaAtual($idColaborador=0, $mesFatura=0) {

        $idColaborador = $idColaborador + 0;

        $qy = "proc_cm_faturaSelecionar '".$idColaborador."', '".$mesFatura."'";
        //echo $qy;
        $rs = $this->executarQueryArray($qy);
      
            $fatura = new mFatura();
            $fatura->setIdFatura($rs[1]['ID_FATURA']+0);
            $fatura->selecionar();
            
     
        return $fatura;
        
    }
    
    public function faturasAprovar($idSetor=0, $idSetorLogado=0, $mesFatura=0) {
        //echo "EXEC proc_cm_aprovacaoVerificarAprovacaoLote '".$idSetor."', '".$idSetorLogado."', '".$mesFatura."'";
        $rs = $this->executarQueryArray("EXEC proc_cm_aprovacaoVerificarAprovacaoLote '".$idSetor."', '".$idSetorLogado."', '".$mesFatura."'");
        
        return $rs[1]['NUM_FATURA'];
        
    }
    
    public function botoesFatura($idSetor=0) {
        $botoes[0] = '<a href="'.appConf::root.'fatura/abrirFatura/'.$this->idFatura.'" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a> ';
        $botoes[1] = '<a href="'.appConf::root.'aprovacao/abrirAprovacao/'.$this->idFatura.'" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-ok"></span>&nbsp;</a> ';
        $botoes[2] = '<a href="'.appConf::root.'fatura/imprimirFatura/'.$this->idFatura.'" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-print"></span>&nbsp;</a>';
        
        $botoesFinal = '';
        
        $status = $this->statusFatura->getIdStatusFatura();
        $perfil = appFunction::dadoSessao('perfil');
        
        $rs = $this->executarQueryArray("EXEC proc_cm_selecionarSetorAprovador ".$idSetor);
        
        $vago = $rs[1]['ID_SETOR'];
        $setorLogado = appFunction::dadoSessao('id_setor');
        
        
        
//        // se for APROVADO
//        if($status == 3) {
//             $botoes[1] = '';
//        } else {
//            
//            // se for AGUARDANDO APROV GD e o usuario logado for GR
//                if($status == 2 && $perfil == 3) {
//                    $botoes[1] = '';
//                } else {
//            
//                    // se for diferente de AGUARDANDO LANCAMENTO
//                    if ($status != 1) {
//                        $botoes[1] = '';
//                    } 
//            }
//            
//        }
        
        
        switch ($status) {
            
            // AGUARDANDO LANCAMENTOS
            case 1: {
               //echo '1';
                $botoes[1] = '';
                $botoes[2] = '';
                break;
            }
            
            // APROVADO
            case 3: {
                //echo '3';
                $botoes[1] = '';
                break;
            }
            
            // AGUARDANDO APROVACAO GD
            case 2: {
                //echo '2';
                if($perfil != 3) {
                    
                    if($setorLogado = $vago) {
                        $botoes[2] = '';
                    } else {
                        $botoes[1] = ''; 
                    }
                }  else {
                    $botoes[2] = '';
                }
                break;
            }
            
            // AGUARDANDO APROVACAO GR
            case 6: {
                if($perfil != 2) {
                   
                    if($setorLogado = $vago) {
                        $botoes[2] = '';
                    } else {
                        $botoes[1] = ''; 
                    }
                } else {
                    $botoes[2] = '';
                }
                break;
            }  
            
            // AGUARDANDO APROVACAO GN
            case 7: {
                //echo '7';
                if($perfil != 1) {
                   if($setorLogado = $vago) {
                        $botoes[2] = '';
                    } else {
                        $botoes[1] = ''; 
                    }
                } else {
                    $botoes[2] = '';
                }
                break;
            }     
            
            // REPROVADO
            case 4: {
                //echo '4';
                if($perfil != 1) {
                    $botoes[1] = '';
                }
                
                $botoes[2] = '';
                break;   
            }
        }
        
        
        
//        if(appFunction::dadoSessao('id_setor') == $vago) {
//           $botoes[1] = '<a href="'.appConf::root.'aprovacao/abrirAprovacao/'.$this->idFatura.'" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-ok"></span>&nbsp;</a> '; 
//        }

        foreach($botoes as $botao) {
            $botoesFinal .= $botao;
        }
        
        return $botoesFinal;
    }

    private function popularVariaveis($rs) {
        if(count($rs) > 0) {            
            $this->idCartao = $rs[1]['ID_CARTAO'];
            $this->idFatura = $rs[1]['ID_FATURA'];
            $this->mesFatura = $rs[1]['MES_FATURA'];
            $this->dataCadastroRecebimento = $rs[1]['DATA_CADASTRO_RECEBIMENTO'];
            $this->dataRecebimento = $rs[1]['DATA_RECEBIMENTO'];
            
            
            $rs = $this->executarQueryArray("EXEC proc_cm_faturaTotalFatura ".$this->idFatura);
            $this->totalFatura = $rs[1]['TOTAL'];
            
            $this->lancamento->setIdFatura($this->idFatura);
            
            $this->statusFatura->setIdFatura($this->idFatura);
            $this->statusFatura->selecionar();
        }
    }
    
}