<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

require_once('app/model/mDespesa.php');


class mLancamento extends appConexao {
    
    private $idLancamento;
    private $idFatura;
    private $dataLancamento;
    private $estabelecimento;
    private $tipoTransacao;
    private $autorizacao;
    private $valor;
    
    private $totalDespesa;

    public $despesa;
    
    
    public function __construct() {
        $this->idLancamento = 0;
        $this->idFatura = 0;
        $this->dataLancamento = '';
        $this->estabelecimento = '';
        $this->tipoTransacao = '';
        $this->autorizacao = '';
        $this->valor = 0;
        
        $this->despesa = new mDespesa();
    }
    
    public function setIdLancamento($valor) {
        $this->idLancamento = $valor;
    }
    
    public function setIdFatura($valor) {
        $this->idFatura = $valor;
    }
    
    public function setDataLancamento($valor) {
        $this->dataLancamento = $valor;
    }
    
    public function setEstabelecimento($valor) {
        $this->estabelecimento = $valor;
    }
    
    public function setTipoTransacao($valor) {
        $this->tipoTransacao = $valor;
    }
    
    public function setAutorizacao($valor) {
        $this->autorizacao = $valor;
    }
    
    public function setValor($valor) {
        $this->valor = $valor;
    }

    
    
    
    
    public function getIdLancamento() {
        return $this->idLancamento;
    }

    
    public function getIdFatura() {
        return $this->idFatura;
    }
    
    public function getDataLancamento() {
        return appFunction::formatarData($this->dataLancamento);
    }
    
    public function getEstabelecimento() {
        return $this->estabelecimento;
    }
    
    public function getTipoTransacao() {
        return $this->tipoTransacao;
    }
    
    public function getAutorizacao() {
        return $this->autorizacao;
    }
    
    public function getValor() {
        return appFunction::formatarMoedaBRL($this->valor,2);
    }
    
    public function getTotalDespesa($tipo=1) {
        $rs = $this->executarQueryArray("EXEC proc_cm_faturaTotalTipo ".$this->idFatura.", ".$tipo."");
        return $rs[1]['TOTAL']+0;
    }

    public function selecionar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_lancamentoSelecionar ".$this->idLancamento);
        if(count($rs) > 0) {
            $this->idLancamento = $rs[1]['ID_LANCAMENTO'];
            $this->idFatura = $rs[1]['ID_FATURA'];
            $this->dataLancamento = $rs[1]['DATA_LANCAMENTO'];
            $this->estabelecimento = $rs[1]['ESTABELECIMENTO'];
            $this->tipoTransacao = $rs[1]['TIPO_TRANSACAO'];
            $this->autorizacao = $rs[1]['AUTORIZACAO'];
            $this->valor = $rs[1]['VALOR'];
            
            
            
            $this->despesa->setIdLancamento($this->idLancamento);
            $this->despesa->selecionarPorLancamento();
        }
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("EXEC proc_cm_lancamentoListar ".$this->idFatura);
        $lancamentos = array();
        
        foreach($rs as $lanc) {
            $lancamento =  new mLancamento();
            $lancamento->setIdLancamento($lanc['ID_LANCAMENTO']);
            $lancamento->selecionar();
            $lancamentos[] = $lancamento;
        }
        return $lancamentos;
    }
    
    public function listarPorTipo($tipo=0) {
        
        $rs = $this->executarQueryArray("EXEC proc_cm_lancamentoListarTipo ".$tipo.", ".$this->idFatura);
        
        $despesas = array();
        
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $despesa = new mLancamento();
                $despesa->setIdLancamento($row['ID_LANCAMENTO']);
                $despesa->selecionar();

                $despesas[] = $despesa;
            }
        }
        
        return $despesas;
    }
    
    public function listarNaoJustificados() {
        $rs = $this->executarQueryArray("EXEC proc_cm_lancamentoListarNaoJust ".$this->idFatura);
        $lancamentos = array();
        
        if(count($rs) > 0) {
            foreach($rs as $lanc) {
                $lancamento =  new mLancamento();
                $lancamento->setIdLancamento($lanc['ID_LANCAMENTO']);
                $lancamento->selecionar();
                $lancamentos[] = $lancamento;
            }
        }
        return $lancamentos;
    }
}


?>