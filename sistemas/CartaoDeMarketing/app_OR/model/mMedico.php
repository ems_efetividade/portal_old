<?php

require_once('lib/appConexao.php');

class mMedico extends appConexao {
    
    private $idDespesa;
    private $CRM;
    private $nome;
    private $esp;
    private $tipo;
    
    public function __construct() {
        $this->idDespesa = 0;
        $this->CRM = '';
        $this->nome = 0;
        $this->tipo = '';
        $this->esp = '';
    } 
    
    public function setIdDespesa($valor) {
        $this->idDespesa = $valor;
    }
    
    public function setCRM($valor) {
        $this->CRM = $this->removerAspas($valor);
    }
    
    public function setNome($valor) {
        $this->nome = $this->removerAspas($valor);
    }
    
    public function setEsp($valor) {
        $this->esp = $valor;
    }
    
    public function setTipo($valor) {
        $this->tipo = $valor;
    }
      
    
    public function getIdDespesa() {
        return $this->idDespesa;
    }
    
    public function getCRM() {
        return $this->CRM;
    }
    
    public function getNome() {
        return $this->adicionarAspas($this->nome);
    } 
    
    public function getEsp() {
        return $this->esp;
    }
    
    public function getTipo() {
        return $this->tipo;
    }    
    
    public function getNumMedicos() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS TOTAL FROM CM_MEDICO WHERE ID_DESPESA = ".$this->idDespesa);
        return $rs[1]['TOTAL'];
    }
    
    public function salvar() {
        $this->executar("INSERT INTO CM_MEDICO (ID_DESPESA, CRM, NOME, TIPO) VALUES (".$this->idDespesa.", '".$this->CRM."', '".$this->nome."', '".$this->tipo."')");
    }
    
    public function excluir() {
        
    }
    
    public function excluirPorDespesa() {
        $this->executar("DELETE FROM CM_MEDICO WHERE ID_DESPESA = ".$this->idDespesa);
    }    
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_MEDICO WHERE ID_MEDICO = ".$this->idMedico);
        
        
        if(count($rs) > 0) {
            $this->idDespesa = $rs[1]['ID_DESPESA'];
            $this->CRM = $rs[1]['CRM'];
            $this->nome = $rs[1]['NOME'];
            $this->tipo = $rs[1]['TIPO'];
        }
    }
    
    public function listar() {
        $medicos = array();
        $rs = $this->executarQueryArray("SELECT CRM, NOME FROM CM_MEDICO WHERE ID_DESPESA = ".$this->idDespesa);
        
        if(count($rs) > 0) {
            foreach($rs as $row) {

                $medico = new mMedico();
                $medico->setCRM($row['CRM']);
                $medico->setNome($row['NOME']);
                //$medico->selecionar();

                $medicos[] = $medico;
            }

            
        }
        return $medicos;
    }
    
    public function pesquisarMedicoBase($crm) {
        $medicos = array();
        $rs = $this->executarQueryArray("EXEC proc_cm_medicoPesquisar '".$crm."'");
        if(count($rs) > 0) {
            foreach($rs as $row) {

                $medico = new mMedico();
                $medico->setCRM($row['CRM']);
                $medico->setNome($row['NOME']);
                $medico->setEsp($row['ESP']);

                $medicos[] = $medico;
            }
        }
        return $medicos;
    }
	
	private function removerAspas($texto) {
        return str_replace("'", "`", $texto);
    }
    
    private function adicionarAspas($texto) {
        return str_replace("`", "'", $texto);
    } 
    
}

?>