<?php

require_once('lib/appConexao.php');
require_once('plugins/wideImage/WideImage.php');

class mNotaFiscal extends appConexao {
    
    private $idDespesa;
    private $arquivoNF;
    private $arquivoMD5;
    private $arquivoTemp;
    private $tipo;
    
    public function __construct() {
        $this->idDespesa  = 0;
        $this->arquivoNF  = '';
        $this->arquivoMD5 = '';
        $this->arquivoTemp = '';
        $this->tipo = '';
    }
    
    public function setIdDespesa($valor) {
        $this->idDespesa = $valor;
    }
    
    public function setArquivoNF($valor) {
        $this->arquivoNF = str_replace("'", "´", $valor);
    }   
    
    public function setArquivoMD5($valor) {
        $this->arquivoMD5 = $valor;
    }  
    
    public function setArquivoTemp($valor) {
        $this->arquivoTemp = $valor;
    }
    
    public function setTipo($valor) {
        $this->tipo = $valor;
    }
    
    public function getIdDespesa() {
        return $this->idDespesa;
    }
    
    public function getArquivoNF() {
        return $this->arquivoNF;
    }   
    
    public function getArquivoMD5() {
        return $this->arquivoMD5;
    }    
    
    public function getTipo() {
        return $this->tipo;
    }
    
    
    public function salvar() {
        
        
        $ext = explode('.', $this->arquivoNF);
        $this->arquivoMD5 = md5(rand().$this->arquivoNF.date('Y-m-d H:i:s')).'.'.$ext[count($ext)-1];
        $mover = move_uploaded_file($this->arquivoTemp, appConf::nfFolderRoot.$this->arquivoMD5);
        
        if($mover) {
            $qy = "INSERT INTO CM_NOTA_FISCAL (ID_DESPESA, ARQUIVO_NF, ARQUIVO_NF_MD5, TIPO) VALUES (".$this->idDespesa.", '".$this->arquivoNF."', '".$this->arquivoMD5."', '".$this->tipo."')";
            $this->executar($qy);
            
            if(strpos($this->tipo, 'image')) {
                $imagem = WideImage::load(appConf::nfFolderRoot.$this->arquivoMD5);
                $imagem = $imagem->resize('40%');
                $imagem->saveToFile(appConf::nfFolderRoot.$this->arquivoMD5);
            }
        }
    }
    
    public function excluir() {
        $arquivo = appConf::nfFolderRoot.$this->arquivoMD5;
        if(file_exists($arquivo)) {
            unlink($arquivo);
        }
        $qy = "DELETE FROM CM_NOTA_FISCAL WHERE ID_DESPESA = ".$this->idDespesa." AND ARQUIVO_NF_MD5 = '".$this->arquivoMD5."'";
        $this->executar($qy);
        
    }
    
    public function excluirTudo() {
        $nfs = $this->listar();
        foreach($nfs as $nf) {
            $arquivo = appConf::nfFolderRoot.$nf->getArquivoMD5();
            if(file_exists($arquivo)) {
                unlink($arquivo);
            }
        }
        $qy = "DELETE FROM CM_NOTA_FISCAL WHERE ID_DESPESA = ".$this->idDespesa;
        $this->executar($qy);
        
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_NOTA_FISCAL WHERE ID_DESPESA = ".$this->idDespesa);
        $arr = array();
        if(count($rs) > 0) {
            
            
            foreach($rs as $row) {
                $nf = new mNotaFiscal();
                
                $nf->setIdDespesa($row['ID_DESPESA']);
                $nf->setArquivoNF($row['ARQUIVO_NF']);
                $nf->setArquivoMD5($row['ARQUIVO_NF_MD5']);
                
                $arr[] = $nf;
            }
        }
        
        return $arr;
    }
    
}   