<?php

require_once('lib/appConexao.php');

class mPerfil {
    
    private $idPerfil;
    private $perfil;
    private $nivel;
    private $admin;
    
    private $cn;
    
    public function __construct() {
        $this->idPerfil = '';
        $this->perfil = '';
        $this->nivel = '';
        $this->admin = '';
    }
    
    public function setIdPerfil($valor) {
        $this->idPerfil = $valor;
    }
    
    public function getIdPerfil() {
        return $this->idPerfil;
    }
    
    public function getPerfil() {
        return $this->perfil;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getAdmin() {
        return $this->admin;
    }
    
    public function selecionar() {
        $this->cn = new appConexao();
        $rs = $this->cn->executarQueryArray("SELECT * FROM PERFIL WHERE ID_PERFIL = ".$this->idPerfil);
        unset($this->cn);
        
        $this->popularVariaveis($rs);
    }
    
    private function popularVariaveis($rs) {
        $this->idPerfil = $rs[1]['ID_PERFIL'];
        $this->perfil = $rs[1]['PERFIL'];
        $this->nivel = $rs[1]['NIVEL'];
        $this->admin = $rs[1]['ADMIN'];
    }
    
}