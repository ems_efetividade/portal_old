<?php

require_once('lib/appConexao.php');

class mProduto extends appConexao {
    
    private $idProduto;
    private $produto;
    
    public function __construct() {
        $this->idProduto = 0;
        $this->produto = '';
    }
    
    public function setIdProduto($valor) {
        $this->idProduto = $valor;
    }    
    
    public function setProduto($valor) {
        $this->produto = $valor;
    }

    

    public function getIdProduto() {
        return $this->idProduto;
    }    

    
    public function getProduto() {
        return $this->produto;
    }

    
    public function salvar() {
        //$query = "INSERT INTO CM_PRODUTO_DESPESA (ID_PRODUTO, ID_DESPESA, PARTICIPACAO) VALUES (".$this->idProduto.", ".$this->idDespesa.", ".$this->participacao.")";
        //$this->executar($query);
        if($this->idProduto == 0) {
            $query = "INSERT INTO CM_PRODUTO (PRODUTO) VALUES ('".utf8_decode($this->produto)."')";
        } else {
            $query = "UPDATE CM_PRODUTO SET PRODUTO = '".utf8_decode($this->produto)."' WHERE ID_PRODUTO = ".$this->idProduto;
        }
        
        $this->executar($query);
    }
    
    public function excluir() {
        $this->executar("DELETE FROM CM_PRODUTO WHERE ID_PRODUTO = ".$this->idProduto." AND ID_PRODUTO NOT IN (SELECT ID_PRODUTO FROM CM_PRODUTO_DESPESA GROUP BY ID_PRODUTO)");
    }

    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PRODUTO WHERE ID_PRODUTO = ".$this->idProduto);
        
        if(count($rs) > 0) {
            $this->idProduto = $rs[1]['ID_PRODUTO'];
            $this->produto = $rs[1]['PRODUTO'];
        }
    }

    
    public function listar() {
        $produtos = array();
        $rs = $this->executarQueryArray("SELECT ID_PRODUTO FROM CM_PRODUTO ORDER BY PRODUTO ASC");
        
        if (count($rs) > 0) {
        foreach($rs as $row) {
            
            $produto = new mProduto;
            $produto->setIdProduto($row['ID_PRODUTO']);
            $produto->selecionar();
            
            $produtos[] = $produto;
        }
        
        return $produtos;
        
        }
        
    }
    
    public function comboProduto($sel=0, $nomeCombo='comboProduto') {
        $produtos = $this->executarQueryArray("SELECT ID_PRODUTO, PRODUTO FROM CM_PRODUTO ORDER BY PRODUTO ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($produtos as $produto) {
            $option .= '<option value="'.$produto['ID_PRODUTO'].'">'.$produto['PRODUTO'].'</option>';
        }
        
        return $select.$option.'</select>';
        
    }
    
}

?>