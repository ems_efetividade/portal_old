<?php

require_once('lib/appConexao.php');
require_once('app/model/mProduto.php');

class mProdutoDespesa extends appConexao {
    
    private $idProduto;
    private $idDespesa;
    private $participacao;
    
    public $produto;
    
    public function __construct() {
        $this->idProduto = 0;
        $this->idDespesa = 0;
        $this->participacao = 0;
        
        $this->produto = new mProduto();        
    }
    
    public function setIdProduto($valor) {
        $this->idProduto = $valor;
    }    
    
    public function setIdDespesa($valor) {
        $this->idDespesa = $valor;
    }
    
    public function setParticipacao($valor) {
        $this->participacao = $valor;
    }
    

    public function getIdProduto() {
        return $this->idProduto;
    }    
    
    public function getIdDespesa() {
        return $this->idDespesa;
    }
    
    //public function getProduto() {
   //     return $this->produto;
    //}
    
    public function getParticipacao() {
        return $this->participacao;
    }    
    
    public function salvar() {
        $query = "INSERT INTO CM_PRODUTO_DESPESA (ID_PRODUTO, ID_DESPESA, PARTICIPACAO) VALUES (".$this->idProduto.", ".$this->idDespesa.", ".$this->participacao.")";
        $this->executar($query);
    }
    
    public function excluir() {
        
    }
    
    public function excluirPorDespesa() {
        $this->executar("DELETE FROM CM_PRODUTO_DESPESA WHERE ID_DESPESA = ".$this->idDespesa);
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_PRODUTO_DESPESA WHERE ID_DESPESA = ".$this->idDespesa." AND ID_PRODUTO = ".$this->idProduto);
        
        
        if(count($rs) > 0) {
            $this->idProduto = $rs[1]['ID_PRODUTO'];
            $this->idDespesa = $rs[1]['ID_DESPESA'];
            $this->participacao = $rs[1]['PARTICIPACAO'];
            
            $this->produto = new mProduto();  
            $this->produto->setIdProduto($this->idProduto);
            $this->produto->selecionar();
        }
    }
    
    public function selecionarProduto() {
        $rs = $this->executarQueryArray("SELECT ID_PRODUTO, PRODUTO FROM CM_PRODUTO WHERE ID_PRODUTO = ".$this->idProduto);
        
        
        if(count($rs) > 0) {
            $this->idProduto = $rs[1]['ID_PRODUTO'];
            $this->idDespesa = 0;
            $this->produto = $rs[1]['PRODUTO'];
            $this->participacao = 0;
        }
    }
    
    public function listar() {
        $produtos = array();
        $rs = $this->executarQueryArray("SELECT ID_PRODUTO FROM CM_PRODUTO_DESPESA WHERE ID_DESPESA = ".$this->idDespesa);
        
        if (count($rs) > 0) {
        foreach($rs as $row) {
            
            $produto = new mProdutoDespesa();
            $produto->setIdProduto($row['ID_PRODUTO']);
            $produto->setIdDespesa($this->idDespesa);
            $produto->selecionar();
            
            $produtos[] = $produto;
        }
        
        
        
        }
        
        return $produtos;
        
    }

    public function comboProduto($sel=0, $nomeCombo='comboProduto') {
        $produtos = $this->executarQueryArray("SELECT ID_PRODUTO, PRODUTO FROM CM_PRODUTO ORDER BY PRODUTO ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($produtos as $produto) {
            $option .= '<option value="'.$produto['ID_PRODUTO'].'">'.$produto['PRODUTO'].'</option>';
        }
        
        return $select.$option.'</select>';
        
    }
    
}

?>