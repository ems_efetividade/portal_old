<?php

require_once('lib/appConexao.php');

require_once('app/model/mPerfil.php');

class mSetor extends mPerfil {
    private $idSetor;
    private $idPerfil;
    private $idLinha;
    private $nivel;
    private $setor;
    private $senha;
    private $nome;
    private $linha;
    
    private $cn;
    
    public function __construct() {
        $this->idSetor = 0;
        $this->idPerfil = '';
        $this->idLinha = '';
        $this->nivel = '';
        $this->setor = '';
        $this->senha = '';
        $this->nome = '';
    }
    
    
    public function setIdSetor($valor=0) {
        $this->idSetor = $valor;
    }
    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdPerfil() {
        return $this->idPerfil;
    }
    
    public function getIdLinha() {
        return $this->idLinha;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getSenha() {
        return $this->senha;
    }
    
    public function getNome() {
        return $this->nome;
    }
    
    public function getLinha() {
        return $this->linha;
    }
    
    public function selecionar() {
        $this->cn = new appConexao();
        $rs = $this->cn->executarQueryArray("SELECT S.*, L.NOME AS LINHA FROM SETOR S LEFT JOIN LINHA L ON L.ID_LINHA = S.ID_LINHA WHERE S.ID_SETOR = ".$this->idSetor);
        unset($this->cn);
        
        $this->popularVariaveis($rs);
    }
    
    
    public function listar() {
        $this->cn = new appConexao();
        $rs = $this->cn->executarQueryArray("SELECT ID_SETOR FROM SETOR ORDER BY SETOR ASC");
        unset($this->cn);
        
        
        $setores = array();
        
        foreach($rs as $row) {
            $setor = new mSetor();
            $setor->setIdSetor($row['ID_SETOR']);
            $setor->selecionar();
            
            $setores[] = $setor;
            
            
        }
        unset($setor);
        return $setores;
    }
    
    public function comboSetor($selecionar=0, $nomeCombo='cmbSetor') {
        $this->cn = new appConexao();
        $setores = $this->cn->executarQueryArray("SELECT ID_SETOR, SETOR FROM SETOR ORDER BY SETOR ASC");
        unset($this->cn);
        
        //echo print_r($setores);
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($setores as $setor) {
            $marcar = ($setor['ID_SETOR'] == $selecionar) ? 'selected' : '';
            $option .= '<option value="'.$setor['ID_SETOR'].'" '.$marcar.'>'.$setor['SETOR'].'</option>';
        }
        
        $select .= $option.'</select>';
        return $select;
    }
    
    private function popularVariaveis($rs) {
        $this->idSetor = $rs[1]['ID_SETOR'];
        $this->idPerfil = $rs[1]['ID_PERFIL'];
        $this->idLinha = $rs[1]['ID_LINHA'];
        $this->nivel = $rs[1]['NIVEL'];
        $this->setor = $rs[1]['SETOR'];
        $this->senha = $rs[1]['SENHA'];
        $this->nome = $rs[1]['NOME'];
        $this->linha = $rs[1]['LINHA'];
        
        parent::setIdPerfil($this->idPerfil+0);
        parent::selecionar();
    }
    
    
}