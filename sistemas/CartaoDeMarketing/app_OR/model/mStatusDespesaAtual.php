<?php

require_once('lib/appConexao.php');
require_once('app/model/mStatusDespesa.php');

class mStatusDespesaAtual extends appConexao {
    
    private $idDespesa;
    private $idStatusDespesa;
    private $observacao;
    private $dataCadastro;
    private $ativo;
    
    private $statusDespesa;
    
    public function __construct() {
        $this->idDespesa = 0;
        $this->idStatusDespesa = 1;
        $this->observacao = '';
        $this->dataCadastro = '';
        $this->ativo = '';
        
        $this->statusDespesa = new mStatusDespesa();
    }
    
    public function setIdDespesa($valor) {
        $this->idDespesa = $valor;
    }    
    
    public function setIdStatusDespesa($valor) {
        $this->idStatusDespesa = $valor;
    }

    public function setObservacao($valor) {
        $this->observacao = $valor;
    } 
    
    public function setDataCadastro($valor) {
        $this->dataCadastro = $valor;
    }
    
    public function setAtivo($valor) {
        $this->ativo = $valor;
    }
    

    
    public function getIdDespesa() {
        return $this->idDespesa;
    }    
    
    public function getIdStatusDespesa() {
        return $this->idStatusDespesa;
    }

    public function getObservacao() {
        return $this->observacao;
    } 
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }
    
    public function getAtivo() {
        return $this->ativo;
    }
    
    public function getStatus() {
        return $this->statusDespesa->getStatus();
    }
    
    
    public function salvar() {
        
        //if($this->idStatusAtual == 0) {
        //    $query = "INSERT INTO CM_STATUS_ATUAL "
        //            . "(ID_FATURA, ID_STATUS_FATURA, DESCRICAO, DATA_CADASTRO, ORDEM) "
        //            . "VALUES "
       //             . "(".$this->idFatura.", ".$this->idStatusFatura.", '".$this->descricao."', '".date('Y-m-d H:i:s')."', 1);";
       // }
        
        
        //$rs = $this->executarQueryArray("SELECT COUNT(*) AS EXISTE FROM CM_STATUS_DESPESA_ATUAL WHERE ID_STATUS")
        
        $query = "INSERT INTO CM_STATUS_DESPESA_ATUAL (ID_DESPESA, ID_STATUS_DESPESA, OBSERVACAO, DATA_CADASTRO, ATIVO) "
                . "VALUES (".$this->idDespesa.", ".$this->idStatusDespesa.", '".$this->observacao."', '".date('Y-m-d H:i:s')."', 1)";
        $this->executar("UPDATE CM_STATUS_DESPESA_ATUAL SET ATIVO = 0 WHERE ID_DESPESA = ".$this->idDespesa);
        
        return $this->executar($query);
        
    }
    
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT * FROM CM_STATUS_DESPESA_ATUAL WHERE ATIVO = 1 AND ID_DESPESA = ".$this->idDespesa));
    }
    
    private function popularVariaveis($rs) {
        
        if(count($rs) > 0) {           
            $this->idDespesa = $rs[1]['ID_DESPESA'];
            $this->idStatusDespesa = $rs[1]['ID_STATUS_DESPESA'];
            $this->observacao = $rs[1]['OBSERVACAO'];
            $this->dataCadastro = $rs[1]['DATA_CADASTRO'];
            $this->ativo = $rs[1]['ATIVO'];
            
            
            $this->statusDespesa->setIdStatusDespesa($this->idStatusDespesa);
            $this->statusDespesa->selecionar();
        }
        
    }
    
    
}