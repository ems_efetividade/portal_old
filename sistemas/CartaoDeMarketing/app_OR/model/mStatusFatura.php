<?php

require_once('lib/appConexao.php');

class mStatusFatura extends appConexao {
    
    private $idStatusFatura;
    private $descricao;
    
    private $labelStatus;

    
    public function __construct() {
        $this->idStatusFatura = 0;
        $this->descricao = '';
        
        $this->labelStatus[1][0] = 'label label-default label-as-badge';
        $this->labelStatus[1][1] = 'glyphicon glyphicon-hourglass';
        
        $this->labelStatus[2][0] = 'label label-warning';
        $this->labelStatus[2][1] = 'glyphicon glyphicon-exclamation-sign';
        
        $this->labelStatus[6][0] = 'label label-info';
        $this->labelStatus[6][1] = $this->labelStatus[2][1]; 
        
        $this->labelStatus[7][0] = 'label label-primary';
        $this->labelStatus[7][1] = $this->labelStatus[2][1];         
        
        $this->labelStatus[3][0] = 'label label-success label-as-badge';
        $this->labelStatus[3][1] = 'glyphicon glyphicon-ok';
        
        $this->labelStatus[4][0] = 'label label-danger';
        $this->labelStatus[4][1] = 'glyphicon glyphicon-remove';
    }
    
    public function setIdStatusFatura($valor) {
        $this->idStatusFatura = $valor;
    }
    
    public function setDescricao($valor) {
        $this->descricao = $valor;
    }
    
    public function getIdStatusFatura() {
        return $this->idStatusFatura;
    }
    
    public function getDescricao() {
        return $this->descricao;
    }
    
    public function getStatus() {
        return '<hs5><span class="'.$this->labelStatus[$this->idStatusFatura][0].'"><span class="'.$this->labelStatus[$this->idStatusFatura][1].'"></span> '.$this->descricao.'</span></hs5>';
    }
    
    public function salvar() {
        
    }
    
    public function excluir() {
        
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM CM_STATUS_FATURA WHERE ID_STATUS_FATURA = ".$this->idStatusFatura);
        
        
        if(count($rs) > 0) {
            $this->idStatusFatura = $rs[1]['ID_STATUS_FATURA'];
            $this->descricao = $rs[1]['DESCRICAO'];
        }
    }
    
    public function comboStatusFatura($selecionar=0, $nomeCombo='cmbStatusFatura') {
        $rs = $this->executarQueryArray("SELECT * FROM CM_STATUS_FATURA ORDER BY DESCRICAO ASC");
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option value="0">:: STATUS (TODOS) ::</option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_STATUS_FATURA'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_STATUS_FATURA'].'" '.$marcar.'>'.$row['DESCRICAO'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
        
        
    }
    

    
}

?>