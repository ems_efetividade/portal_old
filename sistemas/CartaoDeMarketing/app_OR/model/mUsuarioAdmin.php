<?php

require_once('lib/appConexao.php');

require_once('app/model/mSetor.php');
require_once('app/model/mColaborador.php');

class mUsuarioAdmin {
    private $idUsuario;
    private $idSetor;
    private $idColaborador;
    private $verDash;
    private $addUsuario;
    
    private $cn;
    
    public $setor;
    public $colaborador;

    public function __construct() { 
        $this->idUsuario = 0;
        $this->idSetor =  0;
        $this->idColaborador =  0;
        $this->verDash =  0;
        $this->addUsuario =  0;
        
        $this->cn = new appConexao();
    }

    
    public function setIdUsuario($valor) {
        $this->idUsuario = $valor;
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }

    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }

    public function setVerDash($valor) {
        $this->verDash = $valor;
    }

    public function setAddUsuario($valor) {
        $this->addUsuario = $valor;
    }

    public function getIdUsuario() {
        return $this->idUsuario;
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }

    public function getIdColaborador() {
        return $this->idColaborador;
    }

    public function getVerDash() {
        return $this->verDash;
    }

    public function getAddUsuario() {
        return $this->addUsuario;
    }

    
    public function selecionar() {
        $this->popularVariaveis($this->cn->executarQueryArray("SELECT * FROM CM_USUARIO_ADMIN WHERE ID_USUARIO = ".$this->idUsuario));
    }
    
    public function selecionarSetor() {
        $this->popularVariaveis($this->cn->executarQueryArray("SELECT * FROM CM_USUARIO_ADMIN WHERE ID_SETOR = ".$this->idSetor));
    }    
    
    public function excluir() {
        $this->cn->executar("DELETE FROM CM_USUARIO_ADMIN WHERE ID_USUARIO = ".$this->idUsuario);
    }
    
    public function listarColaboradores() {
        return $this->cn->executarQueryArray("select V.ID_SETOR, V.ID_COLABORADOR, V.SETOR, V.NOME, V.PERFIL 
	FROM vw_colaboradorSetor V INNER JOIN PERFIL P ON P.ID_PERFIL = V.ID_PERFIL WHERE P.NIVEL = 0");
    }
    
    public function salvar() {
        
        if($this->idUsuario == 0) {
            $query = "INSERT INTO [CM_USUARIO_ADMIN]
                            ([ID_SETOR]
                            ,[ID_COLABORADOR]
                            ,[VER_DASH]
                            ,[ADD_USUARIO])
                      VALUES
                            (".$this->idSetor."
                            ,".$this->idColaborador."
                            ,".$this->verDash."
                            ,".$this->addUsuario.")";
        } else {
            $query = "UPDATE [CM_USUARIO_ADMIN]
                        SET [ID_SETOR] = ".$this->idSetor."
                           ,[ID_COLABORADOR] = ".$this->idColaborador."
                           ,[VER_DASH] = ".$this->verDash."
                           ,[ADD_USUARIO] = ".$this->addUsuario."
                      WHERE [ID_USUARIO] = ".$this->idUsuario."";
        }
        
        $this->cn->executar($query);
        
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT ID_USUARIO FROM CM_USUARIO_ADMIN");
        
        $admins = array();
        
        if(count($rs) > 0) {
            foreach($rs as $row) {
                $admin = new mUsuarioAdmin();
                $admin->setIdUsuario($row['ID_USUARIO']);
                $admin->selecionar();

                $admins[] = $admin;

                unset($admin);
            }
        }
        
        return $admins;
    }
    
    private function popularVariaveis($rs){

        $this->idUsuario = $rs[1]['ID_USUARIO'];
        $this->idSetor =  $rs[1]['ID_SETOR'];
        $this->idColaborador =  $rs[1]['ID_COLABORADOR'];
        $this->verDash =  $rs[1]['VER_DASH'];
        $this->addUsuario =  $rs[1]['ADD_USUARIO'];

        
        $this->colaborador = new mColaborador();
        $this->colaborador->setIdColaborador($this->idColaborador+0);
        $this->colaborador->selecionar();
        
        $this->setor = new mSetor();
        $this->setor->setIdSetor($this->idSetor+0);
        $this->setor->selecionar();
    }

}