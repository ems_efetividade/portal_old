<?php 
	require_once('lib/appConf.php'); 
	require_once('lib/appFunction.php'); 
        appFunction::validarSessao();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 

<!--Plugin javascript do jquery e bootstrap-->
    <script src="/../../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
<script src="<?php echo appConf::root ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo appConf::root ?>public/js/jquery.mask.js" type="text/javascript"></script>
<script src="<?php echo appConf::root ?>public/js/index.js"></script>

<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::root ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>public/css/index.css" rel="stylesheet" type="text/css" />

<link href="<?php echo appConf::root ?>app/view/admin/admin.css" rel="stylesheet" type="text/css" />

</head>
    
    <script>
    
    $(document).ready(function(e){
        $('.panel-left').css('height', (screen.height-100));
    });
    
    
    </script>