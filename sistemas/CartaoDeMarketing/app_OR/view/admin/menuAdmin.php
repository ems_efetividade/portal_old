<?php
require_once('app/model/mUsuarioAdmin.php');
$userAdmin = new mUsuarioAdmin();
$userAdmin->setIdSetor(appFunction::dadoSessao('id_setor'));
$userAdmin->selecionarSetor();

?>

<br>

<div class="panael panael-default">
<div class="panel-abody">
    
                                      <h3 class="text-center"><small><b><span class="glyphicon glyphicon-credit-card"></span>  Cartão de Marketing</b></small></h3>
<p class="text-center"><small>Painel Administrativo</small></p>
</div>
</div>

<hr>

<small>
<ul class="nav nav-pills nav-stacked nav-pills-stacked-example"> 
<!--    <li role="presentation" class="<?php echo $menu5 ?>"><a href="<?php print appConf::root ?>dashboard"><span class="glyphicon glyphicon-upload"></span>  Dash</a></li> -->
    <li role="presentation" class="<?php echo $menu1 ?>"><a href="<?php print appConf::root ?>admin/listar/0/1"><span class="glyphicon glyphicon-file"></span>  Faturas</a></li> 
    <li role="presentation" class="<?php echo $menu2 ?>"><a href="<?php print appConf::root ?>admin/cartao/0/1"><span class="glyphicon glyphicon-credit-card"></span>  Cartões</a></li> 
<!--    <li role="presentation" class="<?php echo $menu3 ?>"><a href="<?php print appConf::root ?>admin/baixa"><span class="glyphicon glyphicon-ok"></span>  Recebimento Físico</a></li> -->
    
    <li role="presentation" class="<?php echo $menu6 ?>"><a href="<?php print appConf::root ?>admin/relatorio"><span class="glyphicon glyphicon-download"></span>  Exportar</a></li> 
    <?php if($userAdmin->getAddUsuario() == 1) { ?>
    <li role="presentation" class="<?php echo $menu8 ?>"><a href="<?php print appConf::root ?>admin/projeto"><span class="glyphicon glyphicon-asterisk"></span>  Projetos</a></li> 
    <li role="presentation" class="<?php echo $menu9 ?>"><a href="<?php print appConf::root ?>admin/produto"><span class="glyphicon glyphicon-asterisk"></span>  Produtos</a></li> 
    <li role="presentation" class="<?php echo $menu4 ?>"><a href="<?php print appConf::root ?>admin/atualizar"><span class="glyphicon glyphicon-refresh"></span>  Upload Faturas</a></li>
    <li role="presentation" class="<?php echo $menu7 ?>"><a href="<?php print appConf::root ?>admin/usuario"><span class="glyphicon glyphicon-user"></span>  Usuarios Admin</a></li>
    <?php } ?>
    
</ul>
</small>    