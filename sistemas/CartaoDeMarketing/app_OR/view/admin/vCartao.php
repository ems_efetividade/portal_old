<?php require_once('app/view/admin/includes.php'); ?>

<style>

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-credit-card"></span> Cadastro de Cartões </h3>
                    <h5><small><?php echo appFunction::formatarMoedaBRL($totalCartoes,0) ?>  cartão(ões) cadastrado(s).</small></h5>
                    
                    
                </div>
                
                <div class="col-lg-6 text-right">
                    
<!--                    <br>
                    <a href="<?php echo appConf::root ?>admin/exportar" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download"></span> Adicionar Cartão</a>-->
<br>
<form action="<?php echo appConf::root ?>admin/filtrarCartao" method="post">
                    <div class="input-group input-group-sm">
            
                        <input type="text" name="txtCriterio" class="form-control" value="<?php echo $criterios['criterio'] ?>" placeholder="Digite o número do cartao ou portador para pesquisar...">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-block" type="submit"><span class="glyphicon glyphicon-filter"></span> Filtrar Resultados</button>
                        </span>
                    </div>
                    </form>
                </div>
            </div>

            <hr>

            
            <div class="row">
                <div class="col-lg-10">
                    
                    <a href="#" data-toggle="modal" data-target="#modalEditarCartao" data-id="0" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Adicionar Cartão</a>
                </div>
                
                <div class="col-lg-2 text-right">
                    <a href="<?php echo appConf::root ?>admin/exportarCartao" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-download"></span> Exportar Listagem</a>
                </div>

            </div>
            
            <br>

            <?php if(count($cartoes) > 0) { ?>

            <small><small>
                <table class="table table-condendsed table-striped table-hover">
                    <tr>
                        <th>Cartao</th>
                        <th>Portador</th>
                        <th>Colaborador</th>
                        <th>Linha</th>
                        <th>Cargo</th>
                        <th>Status</th>
                        <th>&nbsp;</th>
                    </tr>
                    
                    <?php foreach($cartoes as $cartao) { ?>
                    <tr>
                        <td><a href="#" data-toggle="modal" data-target="#modalEditarCartao" data-id="<?php echo $cartao->getIdCartao() ?>"><?php echo appFunction::camuflarNroCartao($cartao->getNumCartao()) ?></a></td>
                        <td><?php echo $cartao->getPortador() ?></td>
                        <td><?php echo $cartao->setor->getSetor().' '.$cartao->colaborador->getNome() ?></td>
                        <td><?php echo $cartao->setor->getLinha();  ?></td>
                        <td><?php echo $cartao->setor->getPerfil();  ?></td>
                        <td><?php echo $cartao->getNomeStatus();  ?></td>
                        <td>
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirCartao" data-id="<?php echo $cartao->getIdCartao() ?>"><span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                    <?php } ?>
                </table>
                </small></small>

         <?php echo $paginacao ?>
            <?php } else { ?>
            
             <hr>
             <h4 class="text-center"><strong><span class="glyphicon glyphicon-exclamation-sign"></span> Ops! Nenhum registro encontrado!</strong></h4>
                <hr>
            <?php } ?>
        </div>
    </div>
</div>




<div class="modal fade" id="modalEditarCartao"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Cartão</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarCartao"><span class="glyphicon glyphicon-ok"></span> Salvar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalPesquisarColaborador"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-check"></span> Selecionar Colaborador</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSelecionar"><span class="glyphicon glyphicon-ok"></span> Selecionar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalExcluirCartao"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body max-height">
                <p>Deseja excluir este cartão?</p>
                    
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-danger btn-sm" id="btnExcluirCartao"><span class="glyphicon glyphicon-trash"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>



        <!--MODAL Aguarde-->
<div class="modal modal-center" style="z-index:10000000;" id="modalMsgBox" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
      </div>
      
      <div class="modal-body">
      
      	<div class="row">
        	<!--<div class="col-lg-3">
            	<p class="text-center text-danger" style="font-size:500%;"><span class="glyphicon glyphicon-remove"></span><p>
            </div>-->
            <div class="col-lg-12">
            	<h5><p class="text-center" id="msgboxTexto"></p></h5>
            </div>
        </div>
      
      	
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>

    </div>
  </div>
</div>


<script>
$(document).ready(function(e) {
   
    $('#modalEditarCartao').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        
        formulario = requisicaoAjax('<?php echo appConf::root ?>admin/formCartao/'+id);
        $('#modalEditarCartao .modal-body').html(formulario);
        
        $('#btnSalvarCartao').unbind().click(function(e) {
            //$('#formCartao').submit();
            
            $.ajax({
                type: "POST",
                url: $('#formCartao').attr('action'),
                data: $('#formCartao').serialize(),
                success: function(html) {
                    if(html != '') {
                        msgBox(html);
                    } else {
                        location.reload();
                    }
                }
            });
            
        });
        
        $('#cmbSetor').change(function(e) {
            combo = requisicaoAjax('<?php echo appConf::root ?>admin/comboColaboradorHistorico/'+$(this).val());
            $('#comboColaborador').html(combo);
        });
        
    });
    
    
   
    
    
    $('#modalPesquisarColaborador').on('show.bs.modal', function (event) {
        formulario = requisicaoAjax('<?php echo appConf::root ?>admin/listaColaborador/');
        $('#modalPesquisarColaborador .modal-body').html(formulario);
        
        
        $('#btnSelecionar').click(function(e) {
            idColab = $('input[name="optionsRadios"]:checked').val();
            idSetor = $('input[name="optionsRadios"]:checked').attr('id-setor');
            setor = $('input[name="optionsRadios"]:checked').attr('setor');
            
            $('input[name="txtIdColaborador"]').val(idColab);
            $('input[name="txtIdSetor"]').val(idSetor);
            
            $('#txtNomeSetor').val(setor);
            $('#modalPesquisarColaborador').modal('hide');
        });
        
    });  
    
    $('#modalExcluirCartao').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
 
        $('#btnExcluirCartao').unbind().click(function(e) {
            
            retorno = requisicaoAjax('<?php echo appConf::root ?>admin/excluirCartao/'+id);

            if(retorno != '') {
                alert(retorno);
            } else {
                location.href = '<?php echo appConf::root ?>admin/cartao';
            }
        });
        
    });      
   
});
</script>