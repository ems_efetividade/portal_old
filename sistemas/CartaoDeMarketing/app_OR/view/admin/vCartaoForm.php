<?php 
    $opt[$cartao->getStatus()] = 'selected'; 
    $faturas = $cartao->fatura->listarFaturas();    
 ?>
<form id="formCartao" action="<?php echo appConf::root ?>admin/salvarCartao" method="post">
<small>
    
    <input type="hidden" name="txtIdCartao" value="<?php print $cartao->getIdCartao(); ?>">   
<!--    <input type="hidden" name="txtIdColaborador" value="<?php print $cartao->getIdColaborador(); ?>">
    <input type="hidden" name="txtIdSetor" value="<?php print $cartao->getIdSetor(); ?>">-->
    
<div class="row">
    
    <div class="col-lg-4">
        <div class="form-group">
            <label for="exampleInputEmail1">Cartão</label>
            <input type="text" class="form-control input-sm justNumber" name="txtNumCartao" value="<?php echo $cartao->getNumCartao() ?>">
        </div>
    </div>
    
    <div class="col-lg-4">
        <div class="form-group">
            <label for="exampleInputEmail1">Limite</label>
            <input type="text" class="form-control input-sm money" name="txtLimite" value="<?php echo appFunction::formatarMoedaBRL($cartao->getLimite(),2) ?>">
        </div>
    </div> 
    
    <div class="col-lg-4">
        <div class="form-group">
            <label for="exampleInputEmail1">Validade</label>
            <input type="text" class="form-control input-sm data" name="txtValidade" value="<?php echo appFunction::formatarData($cartao->getValidade()) ?>">
        </div>
    </div>   
    
     
    
    <div class="col-lg-9">
        <div class="form-group">
            <label for="exampleInputEmail1">Portador</label>
            <input type="text" class="form-control input-sm" name="txtPortador" value="<?php echo $cartao->getPortador() ?>" >
        </div>
    </div>   
    
    <div class="col-lg-3">
        <div class="form-group">
            <label for="exampleInputEmail1">Status do Cartao</label>
            <select class="form-control input-sm" name="cmbStatus">
                <option value="1" <?php echo $opt[1] ?>>ATIVO</option>
                <option value="0" <?php echo $opt[0] ?>>INATIVO</option>
              </select>
        </div>
    </div>    
<!--    
    <div class="col-lg-12">
        <div class="form-group">
            <label for="exampleInputEmail1">Setor</label>
            <?php //echo $colaborador->comboColaborador(); ?>
        </div>
    </div>     -->
    
<!--    <div class="col-lg-12">
        <label for="exampleInputEmail1">Setor</label>
        <div class="input-group input-group-sm">
            
        <input type="text" id="txtNomeSetor" class="form-control" disabled="" value="<?php echo $cartao->setor->getSetor().' '.$cartao->colaborador->getNome() ?>" placeholder="Search for...">
          <span class="input-group-btn">
            <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalPesquisarColaborador">...</button>
          </span>
        </div> /input-group 
    </div>-->

    <div class="col-lg-3">
        <label for="exampleInputEmail1">Setor</label>
        <?php echo $comboSetor; ?>
    </div>

    <div class="col-lg-9">
        <label for="exampleInputEmail1">Colaborador</label>
        <div id="comboColaborador"><?php echo $comboColaborador; ?></div>
    </div>
    
</div>
    
    <?php if(count($faturas) > 0) { ?>
    <br>
    <h5><b>Faturas do Cartao</b></h5>
    <div style="max-height: 120px; overflow: auto">
        <small>
    <table class="table table-striped table-condensed">
        <tr>
            <th>Cód. Fatura</th>
            <th>Mês</th>
            <th>Valor Total</th>
            <th class="text-right">Status</th>
                
        </tr>
        <?php foreach($faturas as $fatura) { ?>
        <tr>
            <td><?php echo str_pad($fatura->getIdFatura(),5, 0, STR_PAD_LEFT) ?></td>
            <td><?php echo $fatura->getMesFatura() ?></td>
            <td><?php echo appFunction::formatarMoedaBRL($fatura->getTotalFatura()) ?></td>
            <td class="text-right"><?php echo $fatura->statusFatura->getStatus() ?></td>
        </tr>
        <?php } ?>
    </table>
    <small>
    </div>
    <?php } ?>
</small>
    
</form>