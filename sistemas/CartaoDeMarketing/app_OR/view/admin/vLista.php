<?php require_once('app/view/header.php'); ?>
<div class="container">
    
    <div class="row">
        <div class="col-lg-7">
            <h1 style="font-family:Roboto-Thin"><span class="glyphicon glyphicon-user"></span> Lançamentos</h1>
        </div>
    </div>
    
    <hr>
    
    <form action="<?php echo appConf::root ?>admin/filtrar" method="post">
    <div class="row">
        <div class="col-lg-2">
            <?php echo $fatura->comboMesFatura($criterios['mesFatura']) ?>
        </div>
        
        <div class="col-lg-3">
            <?php echo $fatura->statusFatura->statusAtual->comboStatusFatura($criterios['idStatus']) ?>
        </div>        
        
        <div class="col-lg-6">
            <input type="text" name="txtCriterio" class="form-control input-sm" value="<?php echo $criterios['criterio'] ?>" placeholder="">
        </div>
        
        <div class="col-lg-1">
            <button class="btn btn-default btn-block btn-sm" type="submit">Filtrar</button>
        </div>
        
    </div>
    </form>
    <br>
    
    <?php echo $paginacao ?>
    
    <small>
        <table class="table table-condendsed table-striped">
            <tr>
                <th>Setor</th>
                <th>Mês Fatura</th>
                <th>Total da Fatura</th>
                <th>Market</th>
                <th>Pessoal</th>
                <th>Outras</th>
                <th>Não Justificadas</th>
                <th>Última Modificação</th>
                <th>Status</th>
            </tr>
            <?php 
                foreach($listaFaturas as $f) {
                    
                    $cartao->setIdCartao($f->getIdCartao());
                    $cartao->selecionar();
            ?>
            <tr>
                <td><?php echo $cartao->colaborador->getSetor().' '.$cartao->colaborador->getColaborador() ?></td>
                <td><?php echo $f->getMesFatura(); ?></td>
                <td>R$ <?php print appFunction::formatarMoedaBRL($f->getTotalFatura(),2) ?></td>
                <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(1),2) ?></td>
                <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(2),2) ?></td>
                <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(3),2) ?></td>
                <td>R$ <?php print appFunction::formatarMoedaBRL(($f->getTotalFatura()-($f->lancamento->getTotalDespesa(2)+$f->lancamento->getTotalDespesa(1)+$f->lancamento->getTotalDespesa(3))),2) ?></td>
                <td><?php print appFunction::formatarData($f->statusFatura->getDataCadastro()) ?></td>
                <td><?php print $f->statusFatura->getStatus() ?></td>
            </tr>
                <?php } ?>
        </table>
    </small>
    
    <?php echo $paginacao ?>
    
    
</div>