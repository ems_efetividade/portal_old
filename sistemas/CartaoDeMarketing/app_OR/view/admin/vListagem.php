<?php // require_once('app/view/header.php'); ?>
<?php require_once('app/view/admin/includes.php'); ?>

<style>
    .table.label {display: block; padding:4px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <div class="row">
                <div class="col-lg-7">
                    <h3><span class="glyphicon glyphicon-file"></span> Listagem de Faturas</h3>
                    <h5><small><?php echo appFunction::formatarMoedaBRL($fatura->getNumTotalFaturas(),0)?> Fatura(s) cadastrada(s). </small></h5>
                    
                    
                </div>
                
                <div class="col-lg-5 text-right">
                    <br>
                    <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalConfirmarRecebimento"><span class="glyphicon glyphicon-ok"></span> Confirmar Recebimento</a>
<!--                    <a href="<?php echo appConf::root ?>admin/exportarLista" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-download"></span> Exportar Listagem</a>-->
                </div>
            </div>

            <hr>
            
            <form class="form-inline" action="<?php echo appConf::root ?>admin/filtrar" method="post">
                <div class="form-group">
                  <?php echo $fatura->comboMesFatura($criterios['mesFatura']) ?>
                </div>
                <div class="form-group">
                 <?php echo $fatura->statusFatura->statusAtual->comboStatusFatura($criterios['idStatus']) ?>
                </div>
                
                <div class="form-group">
                <select class="form-control input-sm" name="cmbRecebido">
                    <option value="" <?php echo (($criterios['recebido'] == '') ? 'selected':'') ?>>:: RECEBIDO ::</option>
                    <option value="NULL" <?php echo (($criterios['recebido'] == 'NULL') ? 'selected':'') ?>>NÃO RECEBIDO</option>
                    <option value="NOT NULL" <?php echo (($criterios['recebido'] == 'NOT NULL') ? 'selected':'') ?>>RECEBIDO</option>
                </select>
                </div>                
                <div class="form-group">
                 <?php echo $cmbPesquisa; ?>
                </div>     
                
                

                
                <div class="form-group">
                 <input type="text" name="txtCriterio" class="form-control input-sm" value="<?php echo $criterios['criterio'] ?>" placeholder="">
                </div>                
                
                <button class="btn btn-primary btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span> Filtrar Resultados</button>
                <a href="<?php print appConf::root ?>admin/listar/0/1" class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-filter"></span> Limpar Filtros</a>
                <a href="<?php print appConf::root ?>admin/exportarLista" class="btn btn-default btn-sm" type="submit"><span class="glyphicon glyphicon-download"></span>&nbsp;</a>
              </form>

         
            
            <br>

            <?php  if (count($listaFaturas) > 0) { ?>

            <small><ssmall>
                <table class="table table-hover table-striped">
                    <tr>
                        <th>Cód. Fatura</th>
                        <th>Setor</th>
                        <th>Mês Fatura</th>
                        <th>Total da Fatura</th>
<!--                        <th>Market</th>
                        <th>Pessoal</th>
                        <th>Outras</th>
                        <th>Não Justificadas</th>-->
                        <th>Última Modificação</th>
                        <th>Status</th>
                        <th class="text-center">Recebido?</th>
                    </tr>
                    <?php 
                        foreach($listaFaturas as $f) {

                            $cartao->setIdCartao($f->getIdCartao());
                            $cartao->selecionar();
                    ?>
                    <tr>
                        <td><a href="<?php echo appConf::root ?>admin/visualizarFatura/<?php echo $f->getIdFatura(); ?>/<?php echo $pagina; ?>" dataa-toggle="modal" daata-target="#modalVisualizarFatura" data-id="<?php echo $f->getIdFatura(); ?>"><?php echo str_pad($f->getIdFatura(),5, 0, STR_PAD_LEFT); ?></a></td>
                        <td><?php echo $cartao->setor->getSetor().' '.$cartao->colaborador->getNome() ?></td>
                        <td><?php echo $f->getMesFatura(); ?></td>
                        <td>R$ <?php print appFunction::formatarMoedaBRL($f->getTotalFatura(),2) ?></td>
<!--                        <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(1),2) ?></td>
                        <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(2),2) ?></td>
                        <td>R$ <?php print appFunction::formatarMoedaBRL($f->lancamento->getTotalDespesa(3),2) ?></td>
                        <td>R$ <?php print appFunction::formatarMoedaBRL(($f->getTotalFatura()-($f->lancamento->getTotalDespesa(2)+$f->lancamento->getTotalDespesa(1)+$f->lancamento->getTotalDespesa(3))),2) ?></td>-->
                        <td><?php print appFunction::formatarData($f->statusFatura->getDataCadastro()) ?></td>
                        <td><?php print $f->statusFatura->getStatus() ?></td>
                        <td class="text-center"><?php echo ($f->getCadastroDataRecebimento() == null) ? '<span class="glyphicon glyphicon-remove"></span>' : '<span class="glyphicon glyphicon-ok-circle"></span>' ; ?></td>
                    </tr>
                        <?php } ?>
                </table>
            </small></ssmall>

            <?php echo $paginacao ?>
                
            <?php } else { ?>
                <hr>
                <h4 class="text-center">Ops! Nenhum registro encontrado!</h4>
                <hr>
            <?php } ?>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalConfirmarRecebimento"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-ok-circle"></span> Confirmar Recebimento de Fatura</h5>
            </div>
            
            <div class="modal-body">
                <form id="formConfirmarRecebimento" action="<?php echo appConf::root ?>admin/confirmarRecebimento" method="post">
                <small><small>
                <div class="row">
                    <div class="col-lg-12">
                        <label for="exampleInputEmail1">Código da Fatura</label>
                        <div class="input-group input-group-sm">
                            <input type="text" name="txtIdFatura" class="form-control justNumber" placeholder="">
                            <span class="input-group-btn">
                                <button class="btn btn-default" id="btnVerificarCodFatura" type="button">Verificar</button>
                            </span>
                        </div>
                        <br>
                    </div>
                    
                    <div class="col-lg-12">
                        <div id="retornoCodFatura"></div>
                    </div>
                </div>
                </small></small>
            </form>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarConfirmacao"><span class="glyphicon glyphicon-ok"></span> Confirmar Recebimento</a>
            </div>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {

    $('#btnVerificarCodFatura').unbind().click(function(e) {
        id = $('input[name="txtIdFatura"]').val();
        retorno = requisicaoAjax('<?php echo appConf::root ?>admin/verificarCodFatura/'+id);
        $('#retornoCodFatura').html(retorno);
    });
    
    $('#btnSalvarConfirmacao').unbind().click(function(e) {
        $('#formConfirmarRecebimento').submit();
    });

});

</script>