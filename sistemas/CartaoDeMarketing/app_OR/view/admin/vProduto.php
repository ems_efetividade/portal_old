<?php require_once('app/view/admin/includes.php'); ?>

<style>

</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            <h3><span class="glyphicon glyphicon-credit-card"></span> Produtos </h3>
            <hr>
            
            <p>
                <button class="btn btn-success" data-toggle="modal" data-target="#modalEditarProduto" data-id="0">
                    <span class="glyphicon glyphicon-plus"></span>
                    Novo Produto
                </button>
            </p>
            
            <table class="table table-condensed table-striped">
                <tr>
                    <th>Produto</th>
                    <th></th>
                </tr>
                
                <?php foreach($produtos as $produto) { ?>
                <tr>
                    <td><small><?php echo $produto->getProduto() ?></small></td>
                    <td class="text-right">
                        <button class="btn btn-warning btn-xs btnEditar" data-toggle="modal" data-target="#modalEditarProduto" data-id="<?php echo $produto->getIdProduto() ?>"><small>Editar</small></button>
                        <button class="btn btn-danger btn-xs btnExcluir" data-toggle="modal" data-target="#modalExcluirProduto"  data-id="<?php echo $produto->getIdProduto() ?>"><small>Excluir</small></button>
                    </td>
                </tr>
                <?php } ?>    
            </table>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalEditarProduto"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Produto</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarProduto"><span class="glyphicon glyphicon-ok"></span> Salvar</a>
            </div>
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalExcluirProduto"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-credit-card"></span> Adicionar/Editar Produto</h5>
            </div>
            
            <div class="modal-body">
                <h4>Deseja excluir esse produto?</h4>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-danger btn-sm" id="btnExcluirProduto"><span class="glyphicon glyphicon-remove"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {
    $('#modalEditarProduto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        
        formulario = requisicaoAjax('<?php echo appConf::root ?>admin/produtoForm/'+id);
        $('#modalEditarProduto .modal-body').html(formulario);
        
        $('#btnSalvarProduto').unbind().click(function(e) {
            //$('#formCartao').submit();
            
            $.ajax({
                type: "POST",
                url: $('#formProduto').attr('action'),
                data: $('#formProduto').serialize(),
                success: function(html) {
                    if(html != '') {
                        msgBox(html);
                    } else {
                        location.reload();
                    }
                }
            });
            
            
        });
        
        
        
    });
    
    
    $('#modalExcluirProduto').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
                
        $('#btnExcluirProduto').unbind().click(function(e) {
            //$('#formCartao').submit();
            $.ajax({
                type: "POST",
                url: '<?php echo appConf::root ?>admin/produtoExcluir/'+id,
                success: function(html) {
                    
                    if($.trim(html) != '') {
                        msgBox(html);
                    } else {
                        location.reload();
                    }
                }
            });
            
            
        });
        
        
        
    });
   
});
</script>
