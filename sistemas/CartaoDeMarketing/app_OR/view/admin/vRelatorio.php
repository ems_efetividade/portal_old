<?php require_once('includes.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            
            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-download"></span> Exportar Base </h3>
                    
                    
                    
                </div>
                
                <div class="col-lg-6 text-right">
                   
                    
                    
                    
                </div>
            </div>
            
            <hr>
            
            <div class="row">
                
                <div class="col-lg-6 col-md-offset-3">
                    
                    <?php echo $fatura->comboMesFatura(); ?>
                    <br>
                    
                    <a href="#" option="1" class="exportarRelatorio btn btn-default btn-block"><span class="glyphicon glyphicon-file"></span> Base de conferência</a>
                    <a href="#" option="2" class="exportarRelatorio btn btn-default btn-block"><span class="glyphicon glyphicon-file"></span> Listagem de Lançamentos</a>
                    
                    
                    
                </div>
                
            </div>
            
            
        </div>
    </div>
</div>

<script>

$(document).ready(function(e) {
    
    $('.exportarRelatorio').click(function(e) {
        url = '<?php echo appConf::root ?>admin/exportarRelatorio/' + $(this).attr('option')+'/'+$('select[name="cmbMesFatura"]').val();
        location.href = url;
    });
        
    
    
});

</script>