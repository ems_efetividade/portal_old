<?php // require_once('app/view/header.php'); ?>
<?php require_once('app/view/admin/includes.php'); ?>

<style>
    .table.label {display: block; padding:4px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            <div class="row">
                <div class="col-lg-7">
                    <h3><span class="glyphicon glyphicon-user"></span> Usuários Administradores</h3>
                  
                </div>
                
                <div class="col-lg-5 text-right">
                    
                </div>
                 
            </div>

           
             <hr>
             <?php if($logado->getAddUsuario() == 1) { ?>
             <a href="#" data-toggle="modal" data-target="#modalAddColaborador" data-id="0" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Adicionar Usuário</a>
             <br>
             <br>
             
             <?php } ?>
             
             <small>
                <table class="table table-striped table-condensed">
                    <tr>
                        <th colspan="2">Usuario</th>
                        <th>&nbsp;</th>
                        <th>&nbsp;</th>
                    </tr>

                    <?php foreach($usuarios as $user) { ?>
                    <tr>
                        <td><?php echo $user->setor->getSetor() ?></td>
                        <td><?php echo $user->colaborador->getNome(); ?></td>
                        <td><?php echo $user->setor->getPerfil() ?></td>
                        <td>
                            <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalAddColaborador" data-id="<?php echo $user->getIdUsuario() ?>"><span class="glyphicon glyphicon-pencil"></span></button>
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirUsuario" data-id="<?php echo $user->getIdUsuario() ?>"><span class="glyphicon glyphicon-trash"></span></button>
                        </td>
                    </tr>
                    <?php } ?>

                </table>
             </small>

         
            
            <br>

            
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddColaborador"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-check"></span> Selecionar Colaborador</h5>
            </div>
            
            <div class="modal-body">
                    
               
                
        
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarUsuario"><span class="glyphicon glyphicon-ok"></span> Selecionar</a>
            </div>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalPesquisarColaborador"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-check"></span> Selecionar Colaborador</h5>
            </div>
            
            <div class="modal-body">
                asd
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSelecionar"><span class="glyphicon glyphicon-ok"></span> Selecionar</a>
            </div>
            
        </div>
    </div>
</div>



<div class="modal fade" id="modalExcluirUsuario"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body max-height">
                <p>Deseja excluir este usuario?</p>
                    
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-danger btn-sm" id="btnExcluirUsuario"><span class="glyphicon glyphicon-trash"></span> Excluir</a>
            </div>
            
        </div>
    </div>
</div>

<script>
$(document).ready(function(e) {

    $('#btnVerificarCodFatura').unbind().click(function(e) {
        id = $('input[name="txtIdFatura"]').val();
        retorno = requisicaoAjax('<?php echo appConf::root ?>admin/verificarCodFatura/'+id);
        $('#retornoCodFatura').html(retorno);
    });
    
    $('#btnSalvarConfirmacao').unbind().click(function(e) {
        $('#formConfirmarRecebimento').submit();
    });
    
    $('#modalAddColaborador').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php echo appConf::root ?>admin/usuarioForm/'+id);
        $('#modalAddColaborador .modal-body').html(formulario);
    });
    
    $('#btnSalvarUsuario').unbind().click(function(e) {
        $('#formSalvarUsuario').submit();
    });
    
    $('#modalPesquisarColaborador').on('show.bs.modal', function (event) {
        formulario = requisicaoAjax('<?php echo appConf::root ?>admin/listaColaborador/');
        $('#modalPesquisarColaborador .modal-body').html(formulario);
        
        
        $('#btnSelecionar').click(function(e) {
            idColab = $('input[name="optionsRadios"]:checked').val();
            idSetor = $('input[name="optionsRadios"]:checked').attr('id-setor');
            setor = $('input[name="optionsRadios"]:checked').attr('setor');
            
            $('input[name="txtIdColaborador"]').val(idColab);
            $('input[name="txtIdSetor"]').val(idSetor);
            
            $('#txtNomeSetor').val(setor);
            $('#modalPesquisarColaborador').modal('hide');
        });
        
    }); 
    
    $('#modalExcluirUsuario').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = button.data('id') // Extract info from data-* attributes
 
        $('#btnExcluirUsuario').unbind().click(function(e) {
            
            retorno = requisicaoAjax('<?php echo appConf::root ?>admin/excluirUsuario/'+id);

            if(retorno != '') {
                alert(retorno);
            } else {
                location.href = '<?php echo appConf::root ?>admin/usuario';
            }
        });
        
    });

});

</script>