 <div class="input-group">
     <input type="text" class="form-control" value="<?php echo $usuario->setor->getSetor().' '.$usuario->colaborador->getNome() ?>" disabled="" id="txtNomeSetor" placeholder="Search for...">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button" data-toggle="modal" data-target="#modalPesquisarColaborador">...</button>
        </span>
      </div>


    <hr>

    <h4>Permissões</h4>
    <form id="formSalvarUsuario" method="post" action="<?php echo appConf::root ?>admin/salvarUsuario">
        <input type="hidden" name="txtIdUsuario" value="<?php echo $usuario->getIdUsuario() ?>" />
        <input type="hidden" name="txtIdColaborador" value="<?php echo $usuario->getIdColaborador() ?>" />
        <input type="hidden" name="txtIdSetor" value="<?php echo $usuario->getIdSetor() ?>" />
    <div class="checkbox">
    <label>
        <input type="checkbox" name="chkAddUsuario" <?php echo ($usuario->getAddUsuario() == 0) ? '' : 'checked'; ?> value="">
      Adicionar Usuários
    </label>
  </div>
</form>
