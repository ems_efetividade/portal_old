<?php require_once('app/view/admin/includes.php'); ?>
<style>
    .dados-lancamento {margin-bottom: 20px;}
    .dados-lancamento p { margin:0px;  } 
    .dados-lancamento .panel-heading { padding: 5px 15px; }
    .scroll { max-height: 200px; overflow-x: auto }
    .striped, .striped-border { background-color: #f9f9f9; padding:5px; border-top: 1px solid #ddd; }
    .striped-border {background-color: #f2f2f2;}
    .pad {padding-left: 20px;padding-right: 20px;}
    .link { cursor: pointer }
    
    
    
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            <div class="row">
                <div class="col-lg-7">
                    <h3><span class="glyphicon glyphicon-user"></span> Visualizar Fatura</h3>
                    
                    
                </div>
                
                <div class="col-lg-5 text-right">
                    <br>
                   
            <?php if($fatura->statusFatura->getIdStatusFatura() == 3) { ?>
            <a href="<?php echo appConf::root ?>fatura/imprimirFatura/<?php echo $fatura->getIdFatura() ?>" class="btn btn-success"><span class="glyphicon glyphicon-print"></span>&nbsp; Imprimir Fatura</a>
            <?php } ?>
                </div>
            </div>
            
            
            
            <a href="<?php echo appConf::root ?>admin/listar/<?php echo $pagina ?>" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
            
            <hr>
            
            <small>
    <div class="row">
        
        <div class="col-lg-2">
            <img src="<?php echo appFunction::fotoColaborador($cartao->colaborador->getFoto()); ?>" class="img-responsive img-rounded" />
            <br>
        </div>
        
        <div class="col-lg-6">
            
            <h4><b><?php echo $cartao->setor->getSetor().' '.$cartao->getPortador(); ?></b></h4>
            <p><?php echo $cartao->setor->getPerfil().' '. $cartao->setor->getLinha() ?></p>
            <p>Fone Corp.: <?php echo $cartao->colaborador->getFoneCorp() ?></p>
            <p>Email: <a href="mailto:<?php echo $cartao->colaborador->getEmail() ?>" target="_top"><?php echo $cartao->colaborador->getEmail() ?></a></p>
            <p>Cartão: <?php echo appFunction::camuflarNroCartao($cartao->getNumCartao()); ?></p>
            <p>Mês Fatura: <?php echo $fatura->getMesFatura(); ?></p>
            <p><?php echo $fatura->statusFatura->getStatus() ?></p>
            
        </div>

        <div class="col-lg-4">
            <div class="panel panel-default">
        <div class="panel-body">
            <smasll>
                <table class="table table-condensed" style="margin-bottom:0px;">

    
                <tr>
                    <td>Marketing</td>
                    <td class="text-right"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(1),2) ?></td>
                </tr>
                
                <tr>
                    <td>Pessoal</td>
                    <td class="text-right"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(2),2) ?></td>
                </tr>   
                
                <tr>
                    <td>Outras</td>
                    <td class="text-right"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(3),2) ?></td>
                </tr>    
                <tr>
                    <td>Não Justif.</td>
                    <td class="text-right"><?php print appFunction::formatarMoedaBRL(($fatura->getTotalFatura()-($fatura->lancamento->getTotalDespesa(2)+$fatura->lancamento->getTotalDespesa(1)+$fatura->lancamento->getTotalDespesa(3))),2) ?></td>
                </tr>                    
         
                
                <tr>
                    <td><b>Total Fatura</b></td>
                    <td class="text-right"><b>R$ <?php print appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></b></td>
                </tr>
                
            </table>
            </small>
                </div>
    </div>
        </div>
    </div>
    
    
    <h5 class="well well-sm"><strong>Lançamentos do Cartão</strong></h5>
    
    <div class="dados-lancamento">
        <div class="pad">
        <div class="row striped-border"><small>
            <div class="col-lg-1"><p><strong>Data</strong></p></div>
            <div class="col-lg-4"><p><strong>Estabelecimento</strong></p></div>
            <div class="col-lg-3"><p><strong>Tipo</strong></p></div>
            <div class="col-lg-2"><p><strong>Valor</strong></p></div>
            <div class="col-lg-2 text-right"><p><strong>Status</strong></p></div>
            </small>
        </div>
        </div>

    <?php $lancamentos = $fatura->lancamento->listar(); 
        foreach($lancamentos as $i => $lanc) {  ?>
        
        <div class="pad">
            <small><small>
            <div class="link row <?php echo ((($i%2) == 0) ? 'striped-border' : 'striped')  ?>" data-toggle="collapse" href="#collapse<?php echo $lanc->getIdLancamento()  ?>">
                <div class="col-lg-1"><p><strong><?php echo $lanc->getDataLancamento() ?></strong></p></div>
                <div class="col-lg-4"><p><strong><?php echo $lanc->getEstabelecimento() ?></strong></p></div>
                <div class="col-lg-3"><p><strong><?php echo $lanc->despesa->tipoDespesa->getDescricao() ?></strong></p></div>
                <div class="col-lg-2"><p><strong><?php echo $lanc->getValor() ?></strong></p></div>
                <div class="col-lg-2 text-right"><p><strong><?php echo $lanc->despesa->status->getStatus() ?></strong></p></div>
                </small></small>
            </div>
                    
            <?php if($fatura->statusFatura->getIdStatusFatura() != 1) { ?>
                <div class=" collapse" id="collapse<?php echo $lanc->getIdLancamento()  ?>" style="padding-top:10px;">
                <?php if($lanc->despesa->getDespesaMkt() == 1) { ?>
 
                <div class="row" >
                    <div class="col-lg-4" style="padding-left:15px">

                        <small><small>
                        <table class="table table-striped table-condensed table-bordesred">
                            <tr>
                                <th colspan="2">Dados do Evento</th>
                            </tr>
                            <tr>
                                <td>Projeto: </td>
                                <td><?php echo $lanc->despesa->projeto->getDescricao() ?></td>
                            </tr>
                            <tr>
                                <td>Ação: </td>
                                <td><?php echo $lanc->despesa->acao->getDescricao() ?></td>
                            </tr>
                            <tr>
                                <td>Planjeado: </td>
                                <td><?php echo $lanc->despesa->getPlanejado() ?></td>
                            </tr>
                            <tr>
                                <td>Realizado: </td>
                                <td><?php echo $lanc->despesa->getRealizado() ?></td>
                            </tr>   

                            <tr>
                                <td>Local: </td>
                                <td><?php echo $lanc->despesa->getLocalEvento() ?></td>
                            </tr>  

                            <tr>
                                <td>Cidade: </td>
                                <td><?php echo $lanc->despesa->cidade->getCidade().'/'.$lanc->despesa->cidade->getUF() ?></td>
                            </tr>
                            <tr>
                                <td>Descrição: </td>
                                <td><?php echo $lanc->despesa->getDescricao() ?></td>
                            </tr>   
                            <tr>
                                <td>Notas Fiscais:</td>
                                <td>
                                    <?php 
                                        $NFs = $lanc->despesa->notaFiscal->listar();
                                        foreach($NFs as $NF) {        
                                    ?>
                                    <p><a href="<?php echo appConf::nfFolder . $NF->getArquivoMD5() ?> "><?php echo $NF->getArquivoNF(); ?></a></p>
                                        <?php } ?>
                                </td>
                            </tr>
                        </table>
                        </small></small>
                    </div>



                    <div class="col-lg-4">
                        <div  class="scroll">
                        <small><small>
                        <table class="table table-striped table-condensed table-bordsered">
                            <tr>
                                <th colspan="2">Médicos Participantes</th>
                            </tr>
                        <?php 
                            $medicos = $lanc->despesa->medico->listar();
                            foreach($medicos as $medico) {        
                        ?>
                        <tr>
                            <td><?php echo $medico->getCRM() ?></td>
                            <td><?php echo $medico->getNome() ?></td>
                        </tr>
                        <?php } ?>
                         </table>
                        </small></small>
                        </div>
                    </div>   


                    <div class="col-lg-4" style="padding-right:15px">
                        <small><small>
                        <table class="table table-striped table-condensed table-bordsered">
                            <tr>
                                <th colspan="2">Produtos Participantes</th>
                            </tr>
                        <?php 
                            $produtos = $lanc->despesa->produto->listar();
                            foreach($produtos as $produto) {        
                        ?>

                            <tr>
                                <td><?php echo $produto->produto->getProduto() ?></td>
                                <td><?php echo $produto->getParticipacao() ?>%</td>
                            </tr>


                        <?php } ?>
                        </table>
                        </small></small>
                    </div>
                    
                    <div class="col-lg-12" style="padding-right:15px">
                        <small><small>
                        <table class="table table-striped table-condensed table-bordsered">
                            <tr>
                                <th colspan="2">Comprovantes/Notas Fiscais</th>
                            </tr>
                        <?php 
                            $NFs = $lanc->despesa->notaFiscal->listar();
                            foreach($NFs as $NF) {        
                        ?>

                            <tr>
                                <td><a href="<?php echo appConf::root .'public/nf/' . $NF->getArquivoMD5() ?> "><?php echo $NF->getArquivoNF(); ?></a></td>
                                
                            </tr>


                        <?php } ?>
                        </table>
                        </small></small>
                        
                    </div>
                </div>
                
            <?php }  else { ?>
                    
                    <div class="row" >
                        <div class="col-lg-12" style="padding-left:0px">

                            <small><small>
                            <table class="tasble table-strsiped tabsle-condensed table-bordesred">

                                <tr>
                                    <td>Descrição: </td>
                                    <td><?php echo $lanc->despesa->getDescricao() ?></td>
                                </tr>

                            </table>
                            </small></small>
                        </div>

                    </div>
            <?php }  ?> 
            
            </div>
        <?php } ?> 

    </div>
    <?php } ?>
        


    </div>
    
    <hr>
    <h5 class="text-right"><strong>Total da Fatura:</strong> R$ <?php print appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></h5>
    <a href="<?php echo appConf::root ?>admin/listar/<?php echo $pagina ?>" class="btn btn-info btn-sm"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
    <br>
    <br>
    <br>
    
    
    
        </div>
    </div>
</div>