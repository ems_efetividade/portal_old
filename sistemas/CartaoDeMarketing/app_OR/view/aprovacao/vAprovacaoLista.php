<?php
    require_once('app/view/header.php');
?>
<script>
$(document).ready(function(e) {
    $('#cmbPerfil, #cmbMesFatura, #cmbStatusFatura').change(function(e) {
        modalAguarde('show');
        url = '<?php echo appConf::root ?>aprovacao/listarFaturaEquipe/'+$('#cmbPerfil').val()+'/'+$('#cmbMesFatura').val()+'/'+$('#cmbStatusFatura').val();
        location.href = url;

    });
    
    $('#btnMarcar').click(function(e){
        $('.chkDespesa').trigger('click');
    });  
    
    $('#btnSalvarTudo').click(function(e){
        alert('ss');
    });
    
  
});
</script>

<div class="container">
    
    <div class="row">
    <div class="col-lg-7">
        <h1 style="font-family:Roboto-Thin"><span class="glyphicon glyphicon-user"></span> Faturas da Equipe</h1>
    </div>
    
    <div class="col-lg-5">
        <div class="row">
            
<!--            <div class="col-lg-12">
                <p>
                    <div class="input-group input-group-sm">
                      <input type="text" id="txtCriterioBusca" class="form-control" value="" placeholder="Pesquisar pelo Nome...">
                      <span class="input-group-btn">
                        <button class="btn btn-default" type="button" id="btnPesquisar">Pesquisar</button>
                      </span>
                    </div> /input-group 
                </p>
              </div>-->
        </div>
    </div>
</div>
    <hr>
    
    <div class="row">
        
        <div class="col-lg-4">
            <button class="btn btn-default btn-sm" type="button" id="btnMarcar"><span class="glyphicon glyphicon-check"></span> Marcar/Desmarcar Todos</button>
            <button class="btn btn-success btn-sm" type="button" id="btnAprovarLote"><span class="glyphicon glyphicon-ok"></span> Aprovar</button>
            <button class="btn btn-danger  btn-sm" type="button" id="btnReprovarLote"><span class="glyphicon glyphicon-remove"></span> Reprovar</button>
        </div>
        
        <div class="col-lg-3">
            <div class="form-group form-group-sm">
                 <?php echo  $cartao->colaborador->comboPerfil($perfil); ?>
            </div>
        </div>
        
        <div class="col-lg-2">
            <div class="form-group form-group-sm">
                <?php echo  $fatura->comboMesFatura($mesFatura); ?>
            </div>
        </div>   
        
        <div class="col-lg-3">
            <div class="form-group form-group-sm">
                <?php echo  $fatura->statusFatura->statusAtual->comboStatusFatura($status); ?>
            </div>
        </div>        

    </div>    
    
    <div class="pdanel padnel-default">
        <form action="<?php print appConf::root ?>aprovacao/salvarTudo" method="post">        
        <div class="padnel-body">
            <small>
                <table class="table table-striped table-condensed">
                    <tr>
                        <th>Cartão</th>
                        <th>Mês Fatura</th>
                        <th>Total da Fatura</th>
                        <th>Market</th>
                        <th>Pessoal</th>
                        <th>Outras</th>
                        <th>Não Justificadas</th>
                        <th>Status</th>
                        <th>Ação</th>
                    </tr>
                    <?php foreach ($faturas as $fatura) {
                        $cartao->setIdCartao($fatura->getIdCartao());
                        $cartao->selecionar();
                        
                        $colaborador->setIdColaborador($cartao->getIdColaborador());
                        $colaborador->selecionarPorColaborador();
                    ?>
                    <tr>
                        
                            <?php
                                $check = '';
                                if($colaborador->getNivelPerfil() == 4 && appFunction::dadoSessao('perfil') == 2 && $fatura->statusFatura->statusAtual->getIdStatusFatura() == 6) {
                                    $check = '<input type="checkbox" class="chkDespesa">';
                                }
                                
                                if(($colaborador->getNivelPerfil() == 4 || $colaborador->getNivelPerfil() == 3) && appFunction::dadoSessao('perfil') == 1 && $fatura->statusFatura->statusAtual->getIdStatusFatura() == 7) {
                                    $check = '<input type="checkbox" class="chkDespesa">';
                                }
                               
                    
                            
                            ?>
                        
                        <td><?php print $check.' '.$cartao->setor->getSetor() .' '. $cartao->colaborador->getNome() ?></td>
                        <td><?php print $fatura->getMesFatura() ?></td>
                        <td><?php print appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></td>
                        <td><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(1),2) ?></td>
                        <td><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(2),2) ?></td>
                        <td><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(3),2) ?></td>
                        <td><?php print appFunction::formatarMoedaBRL(($fatura->getTotalFatura()-($fatura->lancamento->getTotalDespesa(2)+$fatura->lancamento->getTotalDespesa(1)+$fatura->lancamento->getTotalDespesa(3))),2) ?></td>
                        <td><?php print $fatura->statusFatura->getStatus() ?></td>
<!--                        <td>
                            <a href="<?php print appConf::root ?>fatura/abrirFatura/<?php print $fatura->getIdFatura() ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                            <?php if ($fatura->statusFatura->getIdStatusFatura() == 2 || $fatura->statusFatura->getIdStatusFatura() == 6 || $fatura->statusFatura->getIdStatusFatura() == 7) { ?>
                            <a href="<?php print appConf::root ?>aprovacao/abrirAprovacao/<?php print $fatura->getIdFatura() ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-ok"></span>&nbsp;</a>
                            <?php } ?>
                            <?php if ($fatura->statusFatura->getIdStatusFatura() == 3) { ?>
                            <a href="<?php print appConf::root ?>fatura/imprimirFatura/<?php print $fatura->getIdFatura() ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-print"></span>&nbsp;</a>
                            <?php } ?>
                        </td>-->
                        <td>
                            <?php print $fatura->botoesFatura() ?>
                        </td>
                    </tr>  
                    <?php } ?>
                </table> 
            </small>
        </div>
        </form>
    </div>  
    
</div>





<div class="modal fade" id="modalAprovarFaturaLote"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-question-sign"></span> Confirmação</h5>
            </div>
            
            <div class="modal-body text-center">
                <h4 class="text-primary"><strong>Deseja aprovar as faturas selecionadas e sua respectiva equipe (se houver)?</strong></h4>
<!--                <h5 class="text-danger">Essa operação não poderá ser desfeita!</h5>-->
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary btn-sm" id="btnSalvarTudo"><span class="glyphicon glyphicon-ok"></span> Salvar Aprovações</a>
            </div>
            
        </div>
    </div>
</div>