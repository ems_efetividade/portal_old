<script>
$('#cmbTipoDespesa').change(function(e) {
    $('input[name="txtIdTipoDespesa"]').val($(this).val());
    
    
    if($(this).val() == 4) {
        
       
        texto = 'HOSPEDAGEM: R$ 0,00 \n';
        texto = texto + 'LOCAÇÃO DE SALA: R$ 0,00 \n';
        texto = texto + 'ALIMENTAÇÃO: R$ 0,00 \n';
        texto = texto + 'EQUIPAMENTO DE PROJEÇÃO: R$ 0,00';
        
    
        $('textarea[name="txtDetalhe"]').val(texto);
    } else {
        $('textarea[name="txtDetalhe"]').val('');
    }
    
});


$('#btnAddNF').click(function(e) {

    nome = $.now();
    

    file = '<input type="file" id="arqNF'+nome+'" name="arqNF[]" style="display: none" accept=".pdf" />';
    
    $('#formOutros').append(file);
    $('#arqNF'+nome).click();
    $('#arqNF'+nome).change(function(e) {
        
        if(this.files[0].size <= 2000000) {
        
        
            arquivo = $(this).val().split(/(\\|\/)/g).pop();
            tr = '<tr><td>'+arquivo+'</td><td><button type="button" class="btn btn-xs btn-danger btnExcluirFile" file="arqNF'+nome+'"><span class="glyphicon glyphicon-remove"></span></button></td></tr>';
            $('#tbNF').append(tr);

            $('.btnExcluirFile').click(function(e) {
                fileExcluir = $(this).attr('file');

                $(this).closest('tr').remove();
                $('#'+fileExcluir).remove();
            });
        } else {
            msgBox('Tamanho de arquivo excedido. Máximo 2 MB!');
        }
    });
});

$('.btnExcluirNF').click(function(e) {
    nf = $(this).attr('nf');
    excluir = $('input[name="txtNFExcluir"]').val();
    excluir = excluir +  nf + ';';
    
    $('input[name="txtNFExcluir"]').val(excluir);
    
    $(this).closest('tr').remove();
    
});
</script>

<div class="row">
    <div class="col-lg-8">
        <h2 class="roboto" style="margin-top: 0px;"><?php print $lancamento->getEstabelecimento() ?></h2>
    </div>
    
    <div class="col-lg-4 text-right">
        <h4><?php print $lancamento->despesa->status->getStatus() ?></h4>
    </div>
</div>
<hr style="margin-top:0px">
<form id="formOutros" enctype="multipart/form-data"  action="<?php print appConf::root ?>despesa/salvar" method="post">
<input type="file" id="arqNF" name="arqNF" style="display: none" />
<input type="hidden" name="txtIdTipoDespesa" value="<?php print $lancamento->despesa->getIdTipoDespesa() ?>" />
<input type="hidden" name="txtDespesaMkt" value="0" />
<input type="hidden" name="txtIdDespesa" value="<?php print $lancamento->despesa->getIdDespesa() ?>" />
<input type="hidden" name="txtIdLancamento" value="<?php print $lancamento->getIdLancamento() ?>" />
<input type="hidden" name="txtNFExcluir" value="" />
    

<!-- Nav tabs -->
  <ul class="nav nav-tabs nav-justified" role="tablist">
    <li role="presentation" class="active">
		<a href="#home" aria-controls="home" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-pencil"></span> Dados do Lançamento</a>
	</li>
	
    <li role="presentation">
		<a href="#nf" aria-controls="nf" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-info-sign"></span> Notas Fiscais</a>
	</li>


  </ul>


<div class="tab-content">
    <div role="tabpanel" class="tab-pane active" id="home">
        <br>
        
        <div class="row">

            <div class="col-lg-12">
                <small>
                    <div class="row">

                          <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Data</label>
                                  <input type="text" class="form-control input-sm disabled" readonly="readonly" name="txtDataNF" value="<?php print $lancamento->getDataLancamento() ?>">
                              </div>
                          </div> 

  

                        <div class="col-lg-3">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Valor</label>
                                  <input type="text" class="form-control input-sm" readonly="readonly" value="<?php print $lancamento->getValor() ?>">
                              </div>
                          </div> 





                          <div class="col-lg-6">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Tipo de Despesa</label>
                                  <?php print $lancamento->despesa->tipoDespesa->comboTipoDespesa($lancamento->despesa->getIdTipoDespesa()) ?>
                              </div>
                          </div>     

                          <div class="col-lg-12">
                              <div class="form-group">
                                  <label for="exampleInputEmail1">Descricao</label>
                                  <textarea class="form-control input-sm" rows="6" name="txtDetalhe"><?php print $lancamento->despesa->getDescricao() ?></textarea>
                              </div>
                          </div> 


                        <div class="col-lg-12">

                        </div>
                    </div>
                </small>
            </div>

        </div>        
        
        
    </div>
    
    <div role="tabpanel" class="tab-pane" id="nf">
        <br>
        <p><button id="btnAddNF" type="button" class="btn btn-sm btn-success">Adicionar Nota Fiscal</button></p>
        <div style="height: 180px; overflow-x: auto;">
            <small>
                <table class="table table-condensed table-striped" id="tbNF">
                    <tr>
                        <th>Arquivo</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php $NFs = $lancamento->despesa->notaFiscal->listar();
                    foreach($NFs as $nf) { ?>
                    <tr>
                        <td><?php echo $nf->getArquivoNF(); ?></td>
                        <td><button type="button" class="btn btn-xs btn-danger btnExcluirNF" nf="<?php echo $nf->getArquivoMD5(); ?>"><span class="glyphicon glyphicon-remove"></span></button></td>
                    </tr>
                    
                    <?php } ?>
                    
                </table>
            </small>
        </div>
    </div>
	

</div>



</form>

