<link href="<?php echo appConf::root ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
    #tbLancamento {
        font-size: 8px;
    }
    
    #tbLancamento table td {border:0px; padding:2px;}
    #tbLancamento .background { background-color: #f9f9f9; }
    #tbLancamento td { padding:5px; border-top:1px solid #ddd; }
    #tbLancamento th { padding:5px; border-top:1px solid #ddd; font-weight: bold; background-color: #f9f9f9; }
    .divPanel {background-color:#f9f9f9; height: 80px; border:1px solid #ddd; border-radius: 10px; padding-left: 15px; padding-right: 15px; border-collapse: separate; border-spacing: 2px;}
    .bold { font-weight: bold; }

    #tbDados td { padding-bottom:3px;}
    #tbDados .font-dados { font-size: 13px }
    .label { border:0px; padding:15px !important; width: 100%; }
    #tbStatus td { padding-bottom:5px; }
    #tbStatus th { font-size:10px}
</style>

<table width="100%">
    <tr>
        <td>
            <h4 class="bold" style="margin:0px; margin-bottom:5px; padding:0px;">Cartão de Marketing - Fatura do Cartão - <?php print $fatura->getMesFatura(); ?></h4>
        </td>
        <td width="20%" class="text-right">
            Fatura: <span class="bold"><?php print str_pad($fatura->getIdFatura(),5, 0, STR_PAD_LEFT); ?></span>
        </td>
    </tr>
</table>


<hr>



<div class="pannel pannel-default">
    <div class="pannel-body">
        <table class="dados-col">
            <tr>
                <td width="100">
                     <img src="<?php echo appFunction::fotoColaborador($cartao->colaborador->getFoto()); ?>" height="100">
                    
                </td>
                <td valign="top" style="padding-left: 10px;" width="40%">
                    
                    <table id="tbDados">
                        <tr>
                            <td class="bold"><?php print $cartao->setor->getSetor() ?> <?php print $cartao->getPortador() ?></td>
                        </tr>
                        
                        <tr>
                            <td class="font-dados"><small><?php print $cartao->setor->getPerfil() ?> LINHA <?php print $cartao->setor->getLinha() ?></small></td>
                        </tr>
                        
                        <tr>
                            <td class="font-dados"><small><b>Telefone: </b><?php print $cartao->colaborador->getFoneCorp() ?></small></td>
                        </tr>
                        
                        <tr>
                            <td class="font-dados"><small><b>Email: </b><?php print $cartao->colaborador->getEmail() ?></small></td>
                        </tr>

                        <tr>
                            <td class="font-dados"><small><b>Cartao: </b><?php print appFunction::camuflarNroCartao($cartao->getNumCartao()) ?></small></td>
                        </tr>                        
                            
                    </table>

                </td>
                
                <td class="text-right" valign="top" width="60%" style="bodrder:1px solid #ccc;">
                    
                    <table id="tbResumo">
                        <tr >
                            <td width="120" class="text-center divPanel">
                                <h5><small>Market<small></h5>
                                <h5 class="bold">R$ <?php echo appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(1),2) ?></h5>
                            </td><td>&nbsp;</td>
                            
                            <td width="120" class="text-center divPanel">
                                <h5><small>Pessoal<small></h5>
                                <h5 class="bold">R$ <?php echo appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(2),2) ?></h5>
                            </td><td>&nbsp;</td>

                            <td width="120" class="text-center divPanel">
                                <h5><small>Outros<small></h5>
                                <h5 class="bold">R$ <?php echo appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(3),2) ?></h5>
                            </td><td>&nbsp;</td>

                            <td width="120" class="text-center divPanel">
                                <h5><small>Não Just.<small></h5>
                                <h5 class="bold">R$ <?php print appFunction::formatarMoedaBRL(($fatura->getTotalFatura()-($fatura->lancamento->getTotalDespesa(2)+$fatura->lancamento->getTotalDespesa(1)+$fatura->lancamento->getTotalDespesa(3))),2) ?></h5>
                            </td><td>&nbsp;</td>

                            <td width="120" class="text-center divPanel">
                                <h5><small>TOTAL<small></h5>
                                <h5 class="bold">R$ <?php echo appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></h5>
                            </td>                            
                        </tr>
                    </table>

                    
                </td>

            </tr>
        </table>
    </div>
</div>

<br>
<div class="well well-sm">
    <span class="bold">Despesas</span>
</div>

<?php $lancamentos = $fatura->lancamento->listar(); ?>
<small>
<table class="table table-bordered" id="tbLancamento">
    <tr>
        <th>Data da Nota</th>
        <th>Estabelecimento</th>
        <th>Valor</th>
        <th>Nº da Nota</th>
        <th>Distrito</th>
        <th>Tipo de Ação</th>
        <th>Projeto</th>
        <th>Planejado</th>
        <th>Realizado</th>
        <th>Local</th>
        <th>Cidade/UF</th>
        <th>Detalhe</th>
        <th>Produtos</th>
        <th>Médicos</th>
        <th>Tipo</th>

    </tr>
    <?php 
        $somaPlanejado = 0;
        $somaREalizado = 0;
        foreach($lancamentos as $i => $lancamento) { 
            $back =  ($i%2) ? 'background' : '';
            $produtos = $lancamento->despesa->produto->listar();
            $medicos = $lancamento->despesa->medico->listar();
            
            $listaProduto = '';
            $listaMedico = '';
            
            foreach($produtos as $produto) {
                $listaProduto .= '<tr><td>'.$produto->produto->getProduto().'</td><td>'.$produto->getParticipacao().'%</td></tr></p>';
            }
            
            foreach($medicos as $medico) {
                $listaMedico .= '<tr><td>'.$medico->getCRM().'</td><td>'.$medico->getNome().'</td></tr>';
            }            
            
            $somaPlanejado = $somaPlanejado + $lancamento->despesa->getPlanejado();
            $somaREalizado = $somaREalizado + $lancamento->despesa->getRealizado();
            
            ?>
    <tr >
        <td class="<?php echo $back  ?>"><?php echo $lancamento->getDataLancamento() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->getEstabelecimento() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->getValor() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->getNotaFiscal() ?></td>
        
        <td class="<?php echo $back  ?>"><table><tr><td><?php echo $lancamento->despesa->getSetorPag() ?></td><td><?php echo $lancamento->despesa->getColaboradorPag() ?></td></tr></table></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->acao->getDescricao() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->projeto->getDescricao() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->getPlanejado() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->getRealizado() ?></td>
        
        
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->getLocalEvento() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->cidade->getCidade().'/'.$lancamento->despesa->cidade->getUF() ?></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->getDescricao() ?></td>
        <td class="<?php echo $back  ?>" valign="top"><table><?php echo $listaProduto ?></table></td>
        <td class="<?php echo $back  ?>" valign="top"><table><?php echo $listaMedico ?></table></td>
        <td class="<?php echo $back  ?>"><?php echo $lancamento->despesa->tipoDespesa->getDescricao() ?></td>
    </tr>
    <?php } ?>
</table>
</small>

<div class="well well-sm">
    <span class="bold">Aprovações</span>
</div>

<table width="50%" id="tbStatus">
    <tr>
        <th>Data</th>
        <th>Status</th>
        <th>Aprovador</th>
    </tr>
    <?php $status = $fatura->statusFatura->listarTodosStatusFatura();
        foreach($status as $st) {?>
    <tr>
        
        <td class="">
            <p><small><small><?php print appFunction::formatarData($st->getDataCadastro()); ?></small></small></p>
        </td>
        
        <td>
            <p><small><small><?php print $st->statusAtual->getDescricao() ?></small></small></p>
        </td>
        
        <td>
            <small><small><?php print $st->getSetorAprovador().' '.$st->getColaboradorAprovador() ?></small></small>
        </td>
        
    </tr>
    <?php } ?>
</table>