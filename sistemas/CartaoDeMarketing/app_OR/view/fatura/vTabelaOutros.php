<div class="panel panel-default">
    <div class="panel-heading">
        <strong><span class="glyphicon glyphicon-ok"></span> Outroas</strong>
    </div>
</div>
<small>
    <table class="table table-condensed table-bordered table-hover table-striped">
        <tr class="active">
            <th></th>
            <th>Data</th>
            <th>Estabelecimento</th>
            <th class="text-center">Valor</th>
            <th>Tipo</th>
            <th>Descricao</th>
            <th class="text-center">Status</th>
            <th class="text-center"></th>
        </tr>

        <?php $x = 1;
        foreach($lanc as $u => $lanc1) {
            foreach($lanc1 as $i => $lancamento) {  ?>    
        <tr>
            <td class="text-center"><?php print $x; ?></td>
            <td><?php print $lancamento->despesa->getDataNotaFiscal(); ?></td>
            <td><?php print $lancamento->getEstabelecimento(); ?></td>
            <td class="text-center"><?php print $lancamento->getValor(); ?></td>
            <td><?php print $lancamento->despesa->tipoDespesa->getDescricao(); ?></td>
            <td><?php print $lancamento->despesa->getDescricao(); ?></td>
            <td class="text-center"><?php print $lancamento->despesa->status->getStatus(); ?></td>
            <td class="text-center">
                <a href="#" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                <?php if ($lancamento->despesa->status->getIdStatusDespesa() == 3) { ?>
                
                <a href="#" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#modalOutros" data-id-lancamento="<?php print $lancamento->getIdLancamento(); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a>
                <?php } ?>
                <?php if ($fatura->statusFatura->getIdStatusFatura() != 2 && $fatura->statusFatura->getIdStatusFatura() == 1) { ?>
                <a href="#" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#modalExcluirDespesa" data-id-despesa="<?php print $lancamento->despesa->getIdDespesa(); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;</a>
                <?php } ?>
            </td>
            
           
        </tr>
        <?php $x++; }  }  ?>
    </table>
</small>