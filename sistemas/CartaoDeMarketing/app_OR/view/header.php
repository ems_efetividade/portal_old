<?php 
	require_once('lib/appConf.php'); 
	require_once('lib/appFunction.php'); 
        appFunction::validarSessao();
       
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 


<title>Portal EMS Prescrição</title>

<!--Plugin javascript do jquery e bootstrap-->
    <script src="/../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
<!--<script src="<?php echo appConf::root ?>plugins/jquery-ui/jquery-ui.js"></script>-->
<script src="<?php echo appConf::root ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo appConf::root ?>public/js/jquery.mask.js" type="text/javascript"></script>
<script src="<?php echo appConf::root ?>public/js/index.js"></script>


<link rel="shortcut icon" href="<?php echo appConf::root ?>public/img/liferay.ico" type="image/x-icon" />


<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::root ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::root ?>public/css/index.css" rel="stylesheet" type="text/css" />

</head>
    
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
        <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-credit-card"></span>&nbsp;Cartão de Marketing</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
          <?php  if (appFunction::dadoSessao('perfil') == 0) {  ?>
        <li class="divider-vertical"></li>
        <li><a href="<?php print appConf::root ?>admin">listar</a></li>
        <?php  } ?>
        <li class="divider-vertical"></li>
        <li><a href="<?php print appConf::root ?>">Minhas Faturas</a></li>
        <li class="divider-vertical"></li>
        <?php  if (appFunction::dadoSessao('perfil') != 4) {  ?>
        <li><a href="<?php print appConf::root ?>aprovacao/">Faturas da Equipe</a></li>
        <?php  } ?>
<!--        <li class="divider-vertical"></li>
        <li><a href="<?php print appConf::root ?>aprovacao/">Aprovações</a></li>-->
        
      </ul>
      
<!--      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
          </ul>
        </li>
      </ul>-->
    </div> 
  </div><!-- /.container-fluid -->
</nav>
    
    
    
    
        <!--MODAL Aguarde-->
<div class="modal modal-center" style="z-index:10000000;" id="modalMsgBox" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
      </div>
      
      <div class="modal-body">
      
      	<div class="row">
        	<!--<div class="col-lg-3">
            	<p class="text-center text-danger" style="font-size:500%;"><span class="glyphicon glyphicon-remove"></span><p>
            </div>-->
            <div class="col-lg-12">
            	<h5><p class="text-center" id="msgboxTexto"></p></h5>
            </div>
        </div>
      
      	
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>

    </div>
  </div>
</div>

    
    
    <!--MODAL Aguarde-->
<div class="modal modal-center fade" style="z-index:100000;" id="modalAguarde" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
      
      <div class="modal-body">
      	<p class="text-center"><strong>Processando...</strong></p>
        <h5 class="text-center"><small>(Esta operação pode demorar alguns minutos.)</small></h5>
        
        <div class="progress">
          <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span class="sr-only"></span>
          </div>
        </div>
        
      </div>

    </div>
  </div>
</div>
