<?php
    require_once('app/view/header.php');
?>

<div class="container">
    
    <div class="row">
    <div class="col-lg-7">
        <h1 style="font-family:Roboto-Thin"><span class="glyphicon glyphicon-user"></span> Minhas Faturas</h1>
    </div>
    
    <div class="col-lg-5">
        <div class="row">
            
<!--            <div class="col-lg-12">
                <p>
                <div class="input-group input-group-sm">
                  <input type="text" id="txtCriterioBusca" class="form-control" value="" placeholder="Pesquisar pelo Nome...">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="button" id="btnPesquisar">Pesquisar</button>
                  </span>
                </div> /input-group 
            </p>
              </div>-->
            
            
            
         
        </div>
    </div>
    
    
    
</div>
    <hr>
    
    
    
    <div class="pdanel padnel-default">
                
        <div class="padnel-body">
            <small>
                <table class="table table-striped table-condensed">
                    <tr>
                        <th>Cartão</th>
                        <th class="text-center">Mês Fatura</th>
                        <th class="text-center">Total da Fatura</th>
                        <th class="text-center">Market</th>
                        <th class="text-center">Pessoal</th>
                        <th class="text-center">Outras</th>
                        <th class="text-center">Não Justificadas</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Receb. Físico?</th>
                        <th>&nbsp;</th>
                    </tr>
                    <?php foreach($cartoes as $cart) { $faturas = $cart->fatura->listarFaturas(); ?>
                    
                        <?php foreach ($faturas as $fatura) { ?>
                        <tr>
                            <td><?php print appFunction::camuflarNroCartao($cart->getNumCartao()) ?></td>
                            <td class="text-center"><?php print $fatura->getMesFatura() ?></td>
                            <td class="text-center"><?php print appFunction::formatarMoedaBRL($fatura->getTotalFatura(),2) ?></td>
                            <td class="text-center"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(1),2) ?></td>
                            <td class="text-center"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(2),2) ?></td>
                            <td class="text-center"><?php print appFunction::formatarMoedaBRL($fatura->lancamento->getTotalDespesa(3),2) ?></td>
                            <td class="text-center"><?php print appFunction::formatarMoedaBRL(($fatura->getTotalFatura()-($fatura->lancamento->getTotalDespesa(2)+$fatura->lancamento->getTotalDespesa(1)+$fatura->lancamento->getTotalDespesa(4)+$fatura->lancamento->getTotalDespesa(3))),2) ?></td>
                            <td class="text-center"><?php print $fatura->statusFatura->getStatus() ?></td>
                            <td class="text-center"><?php print ($fatura->getDataRecebimento() == '') ? '<span class="glyphicon glyphicon-remove"></span>' : '<span class="text-success glyphicon glyphicon-ok"></span>' ; ?></td>
    <!--                        <td>
                                <a href="<?php print AppConf::root ?>fatura/abrirFatura/<?php print $fatura->getIdFatura() ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-folder-open"></span>&nbsp;</a>
                                <a href="<?php print AppConf::root ?>fatura/imprimirFatura/<?php print $fatura->getIdFatura() ?>" class="btn btn-default btn-xs" value="<?php print $fatura->getIdFatura() ?>"><span class="glyphicon glyphicon-print"></span>&nbsp;</a>
                            </td>-->
                            <td class="text-right   ">
                                 <?php print $fatura->botoesFatura() ?>
                            </td>
                        </tr>  
                        <?php } ?>
                    
                    <?php } ?>
                </table> 
            </small>
        </div>
    </div>  
    
</div>