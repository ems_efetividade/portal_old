<?php
require_once('../../plugins/mpdf57/mpdf.php');
require_once('appConexao.php');


class appFunction {
    /**
        * Método que retorna a informação da sessao
         */
    public function dadoSessao($chave) {
        @session_start();
        return $_SESSION[$chave];
    }    
    
    public function validarSessao() {
        @session_start();
        if(!isset($_SESSION['id_setor'])) {
            header('Location: '.'https://'.SERVER_URL.'/login/');
        }
         
    }
    
    public function retornarMes($mes) {
    
        if(strlen($mes) == 5) {
            $retorno['mes'] = substr($mes, 0, 1);
            $retorno['ano'] = substr($mes, 1, 4);
        } else {
            $retorno['mes'] = substr($mes, 0, 2);
            $retorno['ano'] = substr($mes, 2, 4);
        }
        
        return $retorno;
    }
    
    
    
    public function formatarData($data) {
        if($data != "") {

            $dataHora = explode(" ", $data);

            if(count($dataHora) > 0) {
                    $campos = explode("-", $dataHora[0]);
                    $hora = explode(".", $dataHora[1]);
            } else {

                    $campos = explode("-", $data);
                    $hora = '';
            }
            


            if(count($campos) == 1) {
                    $campos = explode("/", $data);
                    return trim($campos[2])."-".trim($campos[1])."-".trim($campos[0]);
            } else {
                    return trim($campos[2])."/".trim($campos[1])."/".trim($campos[0]).' '.$hora[0];
            }

        }
    }
    
    public function formatarMoedaBRL($valor=0, $decimal=2) {
        
        $valor  = ($valor != null) ? $valor : 0;
        
        
            return number_format($valor, $decimal, ",", ".");
        
    }
    
    public function formatarMoedaSQL($valor) {
        return str_replace(",", ".", str_replace(".", "", $valor));
    }
    
    public function formatarDataFatura() {
        
    }
    
    public function formatarTamanho($tamanho) {
		 $kb = 1024;
		   $mb = 1048576;
		   $gb = 1073741824;
		   $tb = 1099511627776;
	 
		   if($tamanho<$kb){
		 
			 return($tamanho." bytes");
	   
		   }else if($tamanho>=$kb&&$tamanho<$mb){
	  
			 $kilo = number_format($tamanho/$kb,2);
		 
			 return($kilo." KB");
	   
		   }else if($tamanho>=$mb&&$tamanho<$gb){
		  
			$mega = number_format($tamanho/$mb,2);
	   
			 return($mega." MB");
	   
		   }else if($tamanho>=$gb&&$tamanho<$tb){
		   
			$giga = number_format($tamanho/$gb,2);
		  
			return($giga." GB");
		  }
	}    
    
        
        
        
    public function fotoColaborador($foto='') {
        $caminho = EMS_URL.'/public/profile/';
        $arqFoto = EMS_URL.'/public/img/exemplo_foteoo.jpg';
        
        if($foto != '') {
            if (file_exists(EMS_FOTO_PROFILE.$foto)) {
                $arqFoto = $caminho.$foto;
            } 
        }

        return $arqFoto;
       
	}
        
        
    public function camuflarNroCartao($nroCartao) {
        $ultimosDigitos = substr($nroCartao, strlen($nroCartao)-4, 4);
        return 'XXXX.XXXX.XXXX.'.$ultimosDigitos;
    }
    
    public function arquivoNF($arquivo) {
        $fisico    = AppConf::nfFolderRoot.$arquivo;
        $arquivoNF = AppConf::root.AppConf::nfFolder.$arquivo;
        
        if(!file_exists($fisico) || $arquivo == '') {
            $arquivoNF = AppConf::root.'public/images/semCupom.jpg';
        }
        
        return $arquivoNF;
    }
    
    public function gerarPDF($html='', $css='', $nomeArquivo='', $orientacao='A4') {
		ob_start();
		$html = urldecode($html);
		$nome_arquivo = $nomeArquivo;
		$arquivo_css = $css;
		
		//$stylesheet = file_get_contents('css/css_imprimir_print.css');
		$pdf = new mPDF('', $orientacao, 0, 'Arial', 7, 7, 7, 15, 7, 10, 'P');
                //$pdf = new mPDF('','A4',10,'DejaVuSansCondensed');
		$pdf->allow_charset_conversion=true;
		$pdf->showImageErrors = true;
		$pdf->charset_in='iso-8859-1';
		$pdf->SetDisplayMode('fullpage');
		$pdf->SetFooter('{DATE d/m/y H:i:s}||{PAGENO}');
		//$pdf->WriteHTML($stylesheet,1);
		if($arquivo_css != "") {
			$stylesheet = file_get_contents($arquivo_css);
			$pdf->WriteHTML($stylesheet,1); // The parameter 1 tells that this is css/style only and no body/html/text
		}
		$pdf->WriteHTML($html);
                
//                
                
                
		$pdf->Output($nome_arquivo.'.pdf', 'D');
		//exit();
    }
    
    public function enviarEmail($para, $assunto, $texto, $fromName='Portal EMS Prescrição') {
        //Nova instância do PHPMailer
        $mail = new PHPMailer;

        //Informa que será utilizado o SMTP para envio do e-mail
        $mail->IsSMTP();

        //Informa que a conexão com o SMTP será autênticado
        //$mail->SMTPAuth   = true;
        //$mail->SMTPDebug  = 2;
        $mail->CharSet = 'utf-8';
        //$mail->SMTPSecure = "tsl"; //Configura a segurança para SSL
        //$mail->Port       = 25;  //Informa a porta de conexão do GAMIL

        //Informa o HOST do GMAIL
        $mail->Host       = "127.0.0.1";      // sets GMAIL as the SMTP server

        //Usuário para autênticação do SMTP
        $mail->Username =   "george@localhost.com";

        //Senha para autênticação do SMTP
        $mail->Password =   "george";

        //Titulo do e-mail que será enviado
        $mail->Subject  =   $assunto;

        //Preenchimento do campo FROM do e-mail
        $mail->From = $para;
        $mail->FromName = $fromName;

        //E-mail para a qual o e-mail será enviado
        $mail->AddAddress($para);

        //Conteúdo do e-mail
        $mail->Body = $texto;
        $mail->AltBody = $mail->Body;

        //Dispara o e-mail
        $mail->Send();
    }
    
    
}