<?php

require_once("class.phpmailer.php");
require_once("class.smtp.php");

//Nova instância do PHPMailer
$mail = new PHPMailer;

//Informa que será utilizado o SMTP para envio do e-mail
$mail->IsSMTP();

//Informa que a conexão com o SMTP será autênticado
//$mail->SMTPAuth   = true;
$mail->SMTPDebug  = 2;

$mail->CharSet = 'utf-8';

//Configura a segurança para SSL
//$mail->SMTPSecure = "tsl";

//Informa a porta de conexão do GAMIL
//$mail->Port       = 25;

//Informa o HOST do GMAIL
$mail->Host       = "127.0.0.1";      // sets GMAIL as the SMTP server

//Usuário para autênticação do SMTP
$mail->Username =   "george@localhost.com";

//Senha para autênticação do SMTP
$mail->Password =   "george";

//Titulo do e-mail que será enviado
$mail->Subject  =   "Formulário de contato";

//Preenchimento do campo FROM do e-mail
$mail->From = "george@localhost.com";
$mail->FromName = "Portal EMS Prescrição";

//E-mail para a qual o e-mail será enviado
$mail->AddAddress("george@localhost.com");

//Conteúdo do e-mail
$mail->Body = "Novo contato feito através do site.";
$mail->AltBody = $mail->Body;

//Dispara o e-mail
$mail->Send();

?>