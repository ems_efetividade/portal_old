<?php
require_once('lib/appController.php');
require_once('/../model/mControleHeaderPermissoes.php');
require_once('/../view/formsControleHeaderPermissoes.php');

class cControleHeaderPermissoes extends appController
{

    private $modelControleHeaderPermissoes = null;

    public function __construct()
    {
        $this->modelControleHeaderPermissoes = new mControleHeaderPermissoes();
    }

    public function main()
    {
        $this->render('vControleHeaderPermissoes');
    }

    public function controlSwitch($acao)
    {
        $pesquisa = $_POST['isPesquisa'];
        if ($pesquisa == '1')
            $acao = 'pesquisar';
        switch ($acao) {
            case 'cadastrar':
                $forms = new formsControleHeaderPermissoes();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsControleHeaderPermissoes();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsControleHeaderPermissoes();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function listEmpresas()
    {
        return $this->modelControleHeaderPermissoes->listEmpresas();
    }

    public function listBu()
    {
        $retorno = $this->modelControleHeaderPermissoes->listBu();
        echo json_encode($retorno);
    }

    public function arrayListBu()
    {
        return $this->modelControleHeaderPermissoes->listBu();

    }

    public function listMenu()
    {
        return $this->modelControleHeaderPermissoes->listMenu();
    }

    public function listPerfil()
    {
        return $this->modelControleHeaderPermissoes->listPerfil();
    }

    public function listLinha()
    {
        $retorno = $this->modelControleHeaderPermissoes->listLinha();
        echo json_encode($retorno);
    }

    public function arrayListLinha()
    {
        return $this->modelControleHeaderPermissoes->listLinha();

    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $ArrayidLinha = $_POST['idlinha'];
        $ArrayidMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $arrayidPerfil = $_POST['idperfil'];

        foreach ($ArrayidLinha as $linha)
            foreach ($ArrayidMenu as $menu)
                foreach ($arrayidPerfil as $perfil) {
                    $_POST['idlinha'] = $linha;
                    $_POST['idmenu'] = $menu;
                    $_POST['idperfil'] = $perfil;
                    $this->modelControleHeaderPermissoes->save();
                }

    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];

        $this->modelControleHeaderPermissoes->delete();
    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];

        return $this->modelControleHeaderPermissoes->listObj();
    }

    public function loadObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];

        return $this->modelControleHeaderPermissoes->loadObj();
    }

    public function updateObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];

        $this->modelControleHeaderPermissoes->updateObj();
    }

}
