<?php
require_once('lib/appConexao.php');

class mControleHeaderPermissoes extends appConexao implements gridInterface
{

    private $id;
    private $idEmpresa;
    private $idBu;
    private $idLinha;
    private $idMenu;
    private $dataIn;
    private $ativo;
    private $idPerfil;
    private $idUsuario;

    public function __construct()
    {
        $this->id;
        $this->idEmpresa;
        $this->idBu;
        $this->idLinha;
        $this->idMenu;
        $this->dataIn;
        $this->ativo;
        $this->idPerfil;
        $this->idUsuario;
    }

    public function listEmpresas()
    {
        $sql = "SELECT ID_EMPRESA, NOME FROM EMPRESA";
        return $this->executarQueryArray($sql);
    }

    public function listBu()
    {
        $id_emrpesa = $_POST['empresaId'];
        $sql = "SELECT ID_UN_NEGOCIO,NOME FROM UN_NEGOCIO WHERE ID_EMPRESA = $id_emrpesa";
        return $this->executarQueryArray($sql);
    }

    public function listMenu()
    {
        $sql = "SELECT ID,CAMPO FROM CONTROLE_HEADER";
        return $this->executarQueryArray($sql);
    }

    public function listPerfil()
    {
        $sql = "SELECT ID_PERFIL,PERFIL FROM PERFIL";
        return $this->executarQueryArray($sql);
    }

    public function listLinha()
    {
        $id_bu = $_POST['buId'];
        $sql = "SELECT ID_LINHA,NOME FROM LINHA WHERE ID_UN_NEGOCIO = $id_bu";
        return $this->executarQueryArray($sql);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getNomeEmpresa()
    {
        $sql = "SELECT NOME FROM EMPRESA WHERE ID_EMPRESA = $this->idEmpresa";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getIdEmpresa()
    {
        return $this->idEmpresa;
    }

    public function getIdBu()
    {
        return $this->idBu;
    }

    public function getNomeBu()
    {
        $sql = "SELECT NOME FROM UN_NEGOCIO WHERE ID_UN_NEGOCIO = $this->idBu";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getNomeLinha()
    {
        $sql = "SELECT NOME FROM LINHA WHERE ID_LINHA = $this->idLinha";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getIdLinha()
    {
        return $this->idLinha;
    }

    public function getIdMenu()
    {
        return $this->idMenu;
    }

    public function getNomeMenu()
    {
        $sql = "SELECT CAMPO FROM CONTROLE_HEADER WHERE ID = $this->idMenu";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getDataIn()
    {
        return $this->dataIn;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function getIdPerfil()
    {
        return $this->idPerfil;
    }

    public function getPerfil()
    {
        $sql = "SELECT PERFIL FROM PERFIL WHERE ID_PERFIL = $this->idPerfil";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1][0];
    }

    public function getAtivoTratado()
    {
        if ($this->ativo == 1) {
            return "Sim";
        }
        return 'não';
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function setIdEmpresa($IdEmpresa)
    {
        $this->idEmpresa = $IdEmpresa;
    }

    public function setIdBu($IdBu)
    {
        $this->idBu = $IdBu;
    }

    public function setIdLinha($IdLinha)
    {
        $this->idLinha = $IdLinha;
    }

    public function setIdMenu($IdMenu)
    {
        $this->idMenu = $IdMenu;
    }

    public function setDataIn($DataIn)
    {
        $this->dataIn = $DataIn;
    }

    public function setAtivo($Ativo)
    {
        $this->ativo = $Ativo;
    }

    public function setIdPerfil($IdPerfil)
    {
        $this->idPerfil = $IdPerfil;
    }

    public function setIdUsuario($IdUsuario)
    {
        $this->idUsuario = $IdUsuario;
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];

        $sql = "select count(id) from CONTROLE_HEADER_PERMISSOES";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($idEmpresa, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_EMPRESA LIKE '%$idEmpresa%' ";
            $verif = true;
        }
        if (strcmp($idBu, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_BU LIKE '%$idBu%' ";
            $verif = true;
        }
        if (strcmp($idLinha, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_LINHA LIKE '%$idLinha%' ";
            $verif = true;
        }
        if (strcmp($idMenu, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_MENU LIKE '%$idMenu%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if (strcmp($idPerfil, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_PERFIL LIKE '%$idPerfil%' ";
            $verif = true;
        }
        if (strcmp($idUsuario, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_USUARIO LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];
        $sql = "INSERT INTO CONTROLE_HEADER_PERMISSOES ([ID_EMPRESA],[ID_BU],[ID_LINHA],[ID_MENU],[ATIVO],[ID_PERFIL],[ID_USUARIO])";
        $sql .= " VALUES ('$idEmpresa','$idBu','$idLinha','$idMenu','$ativo','$idPerfil','$idUsuario')";
        $this->executar($sql);
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == '+') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == '-') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (" . $sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= " . $atual;
        $paginacao .= " AND row <= " . $max . " ";
        return $paginacao;
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];

        $sql = "DELETE FROM CONTROLE_HEADER_PERMISSOES WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = '$id' ";
            $verif = true;
        }
        if (strcmp($idEmpresa, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_EMPRESA = '$idEmpresa' ";
            $verif = true;
        }
        if (strcmp($idBu, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_BU = '$idBu' ";
            $verif = true;
        }
        if (strcmp($idLinha, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_LINHA = '$idLinha' ";
            $verif = true;
        }
        if (strcmp($idMenu, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_MENU = '$idMenu' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ATIVO = '$ativo' ";
            $verif = true;
        }
        if (strcmp($idPerfil, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_PERFIL = '$idPerfil' ";
            $verif = true;
        }
        if (strcmp($idUsuario, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID_USUARIO = '$idUsuario' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "ID_EMPRESA ";
        $sql .= ",";
        $sql .= "ID_BU ";
        $sql .= ",";
        $sql .= "ID_LINHA ";
        $sql .= ",";
        $sql .= "ID_MENU ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "ID_PERFIL ";
        $sql .= ",";
        $sql .= "ID_USUARIO ";
        $sql .= " FROM CONTROLE_HEADER_PERMISSOES ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($idEmpresa, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_EMPRESA LIKE '%$idEmpresa%' ";
            $verif = true;
        }
        if (strcmp($idBu, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_BU LIKE '%$idBu%' ";
            $verif = true;
        }
        if (strcmp($idLinha, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_LINHA LIKE '%$idLinha%' ";
            $verif = true;
        }
        if (strcmp($idMenu, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_MENU LIKE '%$idMenu%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if (strcmp($idPerfil, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_PERFIL LIKE '%$idPerfil%' ";
            $verif = true;
        }
        if (strcmp($idUsuario, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID_USUARIO LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];
        $sql = "UPDATE  CONTROLE_HEADER_PERMISSOES";
        $sql .= " SET ";
        $sql .= "ID_EMPRESA = '$idEmpresa' ";
        $sql .= " , ";
        $sql .= "ID_BU = '$idBu' ";
        $sql .= " , ";
        $sql .= "ID_LINHA = '$idLinha' ";
        $sql .= " , ";
        $sql .= "ID_MENU = '$idMenu' ";
        $sql .= " , ";
        $sql .= "ATIVO = '$ativo' ";
        $sql .= " , ";
        $sql .= "ID_PERFIL = '$idPerfil' ";
        $sql .= " , ";
        $sql .= "ID_USUARIO = '$idUsuario' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $idEmpresa = $_POST['idempresa'];
        $idBu = $_POST['idbu'];
        $idLinha = $_POST['idlinha'];
        $idMenu = $_POST['idmenu'];
        $dataIn = $_POST['datain'];
        $ativo = $_POST['ativo'];
        $idPerfil = $_POST['idperfil'];
        $idUsuario = $_POST['idusuario'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "ID_EMPRESA ";
        $sql .= ",";
        $sql .= "ID_BU ";
        $sql .= ",";
        $sql .= "ID_LINHA ";
        $sql .= ",";
        $sql .= "ID_MENU ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "ID_PERFIL ";
        $sql .= ",";
        $sql .= "ID_USUARIO ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID_EMPRESA ) as row";
        $sql .= " FROM CONTROLE_HEADER_PERMISSOES ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($idEmpresa, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_EMPRESA LIKE '%$idEmpresa%' ";
            $verif = true;
        }
        if (strcmp($idBu, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_BU LIKE '%$idBu%' ";
            $verif = true;
        }
        if (strcmp($idLinha, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_LINHA LIKE '%$idLinha%' ";
            $verif = true;
        }
        if (strcmp($idMenu, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_MENU LIKE '%$idMenu%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if (strcmp($idPerfil, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_PERFIL LIKE '%$idPerfil%' ";
            $verif = true;
        }
        if (strcmp($idUsuario, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID_USUARIO LIKE '%$idUsuario%' ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE " . $where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function montaObj($param)
    {

        $o = new mControleHeaderPermissoes();
        $o->setId($param[0]);
        $o->setIdEmpresa($param[1]);
        $o->setIdBu($param[2]);
        $o->setIdLinha($param[3]);
        $o->setIdMenu($param[4]);
        $o->setDataIn($param[5]);
        $o->setAtivo($param[6]);
        $o->setIdPerfil($param[7]);
        $o->setIdUsuario($param[8]);

        return $o;

    }
}