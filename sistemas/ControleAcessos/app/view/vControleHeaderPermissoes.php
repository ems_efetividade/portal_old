<?php
require_once('header.php');

class vControleHeaderPermissoes extends gridView{

    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        $cols = array('ID','Empresa','BU','Linha','Perfil','Menu','Ativo');
        $array_metodo = array('Id','NomeEmpresa','NomeBu','NomeLinha','Perfil','NomeMenu','AtivoTratado');
        $colsPesquisaNome = array('Função Desativada');
        $colsPesquisa = array('id','idEmpresa','idBu','idLinha','idMenu','dataIn','ativo');
        gridView::setGrid_titulo('Controle de Acessos');
        gridView::setGrid_sub_titulo('Controle geral de acessos à ferramentas');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('ControleHeaderPermissoes');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }
}