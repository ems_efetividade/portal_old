<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

$name='Arial-BoldItalicMT';
$type='TTF';
$desc=array (
  'Ascent' => 905,
  'Descent' => -212,
  'CapHeight' => 715,
  'Flags' => 262212,
  'FontBBox' => '[-560 -376 1390 1018]',
  'ItalicAngle' => -12,
  'StemV' => 165,
  'MissingWidth' => 750,
);
$up=-106;
$ut=105;
$ttffile='C:/wamp/www/programacao3/MPDF56/ttfonts/arialbi.ttf';
$TTCfontID='0';
$originalsize=563800;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='arialBI';
$panose=' 8 5 2 b 7 4 2 2 2 9 2 4';
$haskerninfo=false;
$unAGlyphs=false;
?>