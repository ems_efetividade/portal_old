<?php
require_once('lib/appController.php');
require_once('/../model/mControleHeaderNivel.php');
require_once('/../view/formsControleHeaderNivel.php');

class cControleHeaderNivel extends appController
{

    private $modelControleHeaderNivel = null;

    public function __construct()
    {
        $this->modelControleHeaderNivel = new mControleHeaderNivel();
    }

    public function main()
    {
        $this->render('vControleHeaderNivel');
    }

    public function controlSwitch($acao)
    {
        $pesquisa = $_POST['isPesquisa'];
        if ($pesquisa == '1')
            $acao = 'pesquisar';
        switch ($acao) {
            case 'cadastrar':
                $forms = new formsControleHeaderNivel();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsControleHeaderNivel();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsControleHeaderNivel();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function listFerramenta()
    {
        return $this->modelControleHeaderNivel->listFerramenta();
    }

    public function listPerfil()
    {
        return $this->modelControleHeaderNivel->listPerfil();
    }

    public function listNivel()
    {
        return $this->modelControleHeaderNivel->listNivel();
    }

    public function nivelAcesso(){
        return $this->modelControleHeaderNivel->nivelAcesso();
    }

    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeaderNivel->save();
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeaderNivel->delete();
    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelControleHeaderNivel->listObj();
    }

    public function loadObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelControleHeaderNivel->loadObj();
    }

    public function updateObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeaderNivel->updateObj();
    }
}
