<?php
require_once('lib/appConexao.php');

class mControleHeaderNivel extends appConexao  {

    private $id;
    private $fkFerramenta;
    private $fkNivel;
    private $fkPerfil;
    private $ativo;
    private $dataIn;

    public function __construct(){
        $this->id;
        $this->fkFerramenta;
        $this->fkNivel;
        $this->fkPerfil;
        $this->ativo;
        $this->dataIn;
    }



    public function listFerramenta(){
        $sql = "SELECT ID,CAMPO FROM CONTROLE_HEADER WHERE FK_FERRAMENTA >0 OR FK_FERRAMENTA = -1";
        $retorno = $this->executarQueryArray($sql);
        return $retorno;
    }

    public function listPerfil(){
        $sql = "SELECT ID_PERFIL,PERFIL FROM PERFIL";
        $retorno = $this->executarQueryArray($sql);
        return $retorno;
    }

    public function listNivel(){
        $sql = "SELECT ID,NOME FROM CONTROLE_HEADER_NIVEL_PERFIL";
        $retorno = $this->executarQueryArray($sql);
        return $retorno;
    }


    public function getId(){
        return $this->id;
    }

    public function getFkFerramenta(){
        return $this->fkFerramenta;
    }

    public function nivelAcesso(){
        $sql = "SELECT PERMISSAO FROM CONTROLE_HEADER_NIVEL_PERFIL WHERE ID = $this->fkNivel";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['PERMISSAO'];
    }

    public function getNomeFerramenta(){
        $sql = "SELECT CAMPO FROM CONTROLE_HEADER WHERE ID = $this->fkFerramenta";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['CAMPO'];
    }

    public function getFkNivel(){
        return $this->fkNivel;
    }

    public function getNomeNivel(){
        $sql = "SELECT NOME FROM CONTROLE_HEADER_NIVEL_PERFIL WHERE ID = $this->fkNivel";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['NOME'];
    }

    public function getFkPerfil(){
        return $this->fkPerfil;
    }

    public function getNomePerfil(){
        $sql = "SELECT PERFIL FROM PERFIL WHERE ID_PERFIL = $this->fkPerfil";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['PERFIL'];
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getAtivoTratado(){
        if($this->ativo==1){
            return 'Sim';
        }
        return 'Não';
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setFkFerramenta($FkFerramenta){
        $this->fkFerramenta = $FkFerramenta;
    }

    public function setFkNivel($FkNivel){
        $this->fkNivel = $FkNivel;
    }

    public function setFkPerfil($FkPerfil){
        $this->fkPerfil = $FkPerfil;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from CONTROLE_HEADER_NIVEL";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($fkNivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_NIVEL LIKE '%$fkNivel%' ";
            $verif = true;
        }
        if(strcmp($fkPerfil, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_PERFIL LIKE '%$fkPerfil%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "INSERT INTO CONTROLE_HEADER_NIVEL ([FK_FERRAMENTA],[FK_NIVEL],[FK_PERFIL],[ATIVO],[DATA_IN])";
        $sql .=" VALUES ('$fkFerramenta','$fkNivel','$fkPerfil','$ativo','$dataIn')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM CONTROLE_HEADER_NIVEL WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_FERRAMENTA = '$fkFerramenta' ";
            $verif = true;
        }
        if(strcmp($fkNivel, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_NIVEL = '$fkNivel' ";
            $verif = true;
        }
        if(strcmp($fkPerfil, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_PERFIL = '$fkPerfil' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "FK_NIVEL ";
        $sql .= ",";
        $sql .= "FK_PERFIL ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM CONTROLE_HEADER_NIVEL ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($fkNivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_NIVEL LIKE '%$fkNivel%' ";
            $verif = true;
        }
        if(strcmp($fkPerfil, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_PERFIL LIKE '%$fkPerfil%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  CONTROLE_HEADER_NIVEL";
        $sql .=" SET ";
        $sql .="FK_FERRAMENTA = '$fkFerramenta' ";
        $sql .=" , ";
        $sql .="FK_NIVEL = '$fkNivel' ";
        $sql .=" , ";
        $sql .="FK_PERFIL = '$fkPerfil' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkFerramenta = $_POST['fkferramenta'];
        $fkNivel = $_POST['fknivel'];
        $fkPerfil = $_POST['fkperfil'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "FK_NIVEL ";
        $sql .= ",";
        $sql .= "FK_PERFIL ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM CONTROLE_HEADER_NIVEL ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($fkNivel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_NIVEL LIKE '%$fkNivel%' ";
            $verif = true;
        }
        if(strcmp($fkPerfil, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_PERFIL LIKE '%$fkPerfil%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mControleHeaderNivel();
        $o->setId($param[0]);
        $o->setFkFerramenta($param[1]);
        $o->setFkNivel($param[2]);
        $o->setFkPerfil($param[3]);
        $o->setAtivo($param[4]);
        $o->setDataIn($param[5]);

        return $o;

    }
}