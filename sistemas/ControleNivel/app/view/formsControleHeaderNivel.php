<?php

class formsControleHeaderNivel
{

    public function formEditar($objeto)
    {
        require_once('/../controller/cControleHeaderNivel.php');
        $control = new cControleHeaderNivel();
        $ferramentas = $control->listFerramenta();
        $perfil = $control->listPerfil();
        $nivels = $control->listNivel();
        ?>
        <form id="formEditar" action="<?php print appConf::caminho ?>ControleHeaderNivel/controlSwitch/atualizar">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkFerramenta">Ferramenta</label>
                        <select class="form-control " name="fkferramenta" id="fkFerramenta">
                            <?php
                            foreach ($ferramentas as $ferramenta) {
                                ?>
                                <option
                                       <?php
                                       if($objeto->getFkFerramenta()==$ferramenta['ID']){
                                           echo "Selected";
                                       }
                                       ?>

                                        value="<?php echo $ferramenta['ID'] ?>">
                                    <?php echo $ferramenta['CAMPO'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkPerfil">Perfil</label>
                        <select class="form-control " name="fkperfil" id="fkPerfil">
                            <?php
                            foreach ($perfil as $item) {
                                ?>
                                <option value="<?php echo $item['ID_PERFIL'] ?>"
                                    <?php
                                    if($objeto->getFkPerfil()==$item['ID_PERFIL'] ){
                                        echo "Selected";
                                    }
                                    ?>

                                >
                                    <?php echo $item['PERFIL'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkNivel">Nível</label>
                        <select class="form-control " name="fknivel" id="fkNivel">
                            <?php
                            foreach ($nivels as $nivel) {
                                ?>
                                <option value="<?php echo $nivel['ID'] ?>"
                                    <?php
                                    if($objeto->getFkNivel()==$nivel['ID']){
                                        echo "Selected";
                                    }
                                    ?>
                                >
                                    <?php echo $nivel['NOME'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Ativo</label>
                        <select class="form-control " name="ativo" id="ativo">
                            <option value="1">Ativo</option>
                            <option value="0"
                                <?php
                                if($objeto->getAtivo()==0){
                                    echo 'selected';
                                }
                                ?>
                            >Inativo</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formVisualizar($objeto)
    {

        ?>
        <form id="formVisualizar">
            <div class="row">
                <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkFerramenta">Ferramenta</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeFerramenta() ?>" name="fkferramenta" id="fkFerramenta">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkNivel">Nivel</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeNivel() ?>" name="fknivel" id="fkNivel">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkPerfil">Pefil</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomePerfil() ?>" name="fkperfil" id="fkPerfil">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Ativo</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getAtivoTratado() ?>" name="ativo" id="ativo">
                    </div>
                </div>
                <div class="col-sm-6 text-left" style="display: none">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">DATAIN</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getDataIn() ?>" name="datain" id="dataIn">
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formCadastrar()
    {
        require_once('/../controller/cControleHeaderNivel.php');
        $control = new cControleHeaderNivel();
        $ferramentas = $control->listFerramenta();
        $perfil = $control->listPerfil();
        $nivels = $control->listNivel(); ?>
        <form id="formSalvar" action="<?php print appConf::caminho ?>ControleHeaderNivel/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkFerramenta">Ferramenta</label>
                        <select class="form-control " name="fkferramenta" id="fkFerramenta">
                            <?php
                            foreach ($ferramentas as $ferramenta) {
                                ?>
                                <option value="<?php echo $ferramenta['ID'] ?>">
                                    <?php echo $ferramenta['CAMPO'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkPerfil">Perfil</label>
                        <select class="form-control " name="fkperfil" id="fkPerfil">
                            <?php
                            foreach ($perfil as $item) {
                                ?>
                                <option value="<?php echo $item['ID_PERFIL'] ?>">
                                    <?php echo $item['PERFIL'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkNivel">Nível</label>
                        <select class="form-control " name="fknivel" id="fkNivel">
                            <?php
                            foreach ($nivels as $nivel) {
                                ?>
                                <option value="<?php echo $nivel['ID'] ?>">
                                    <?php echo $nivel['NOME'] ?>
                                </option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Ativo</label>
                        <select class="form-control " name="ativo" id="ativo">
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left" style="display: none">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">DATAIN</label>
                        <input required type="text" class="form-control " name="datain" id="dataIn">
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}
