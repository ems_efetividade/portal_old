<?php
require_once('header.php');

class vControleHeaderNivel extends gridView{

    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        $cols = array('ID','Ferramenta','Perfil','Nível','Ativo');
        $colsPesquisaNome = array('Função desabilitada');
        $colsPesquisa = array('');
        $array_metodo = array('Id','NomeFerramenta','NomePerfil','NomeNivel','AtivoTratado');
        gridView::setGrid_titulo('Controle de Nível');
        gridView::setGrid_sub_titulo('Gerenciamento de perfil de acessos de ferramentas');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('ControleHeaderNivel');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }

}