<?php

class cMain extends Controller {
    
    private $mTabela;
    
    public function __construct() {
        parent::__construct();
        $this->mTabela = new mTabela();
    }
    
    public function main() {

        $dados['linha'] = $this->mTabela->listarTabela('LINHA', 'NOME');
        
        $this->set('dados', $dados);
        $this->render('main/vMain');
    }
    
    
}