<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * @author 053189 - Jefferson Oliveira em 28/08/2017
*/

class mTabela extends Model {
    
    
    public function listarTabela($tabela='', $orderBy='') {
        $query = "SELECT * FROM [".$tabela."]";
        $query .= ($orderBy != '') ? " ORDER BY ".$orderBy." " : '';
        return $this->cn->executarQueryArray($query);
    }
    
}