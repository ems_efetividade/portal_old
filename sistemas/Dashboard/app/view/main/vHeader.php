<!doctype html>
<html lang='en-EN'>

<head>
    <title><?php echo APP_NAME; ?></title>

    <meta charset='utf-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1'>
    <script src="/../../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
    <link href="<?php echo APP_URL ?>framework/libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="<?php echo APP_URL ?>framework/libraries/bootstrap/js/bootstrap.min.js"></script>

<!--    <link href="<?php echo APP_CSS ?>fonts.css" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400" rel="stylesheet">
    <link href="<?php echo APP_CSS ?>default.css" rel="stylesheet" type="text/css"/>

</head>

<body>
    
<div class="modal" id="modalAguarde" tabindex="-1" style="z-index: 100000"  data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      
      <div class="modal-body">
          <div class="progress" style="height: 20px">
            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
              Processando...
            </div>
          </div>
      </div>
      
    </div>
  </div>
</div>
    
    
    
<div class="modal fade" id="modalErro" tabindex="-1" style="z-index: 100000"  data-backdrop="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-info-sign"></span>  Atenção</h5>
      </div>
      <div class="modal-body">
          
          <h3 class="text-center text-muted"><span id="error-data"></span></h3>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>
    
<script>
    
    var EMS_URL = '<?php echo EMS_URL ?>';
    var APP_URL = '<?php echo APP_URL ?>';
    
    $('[data-toggle="popover"]').popover();
    
    function resizePanelLeft() {
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        $('.panel-left').css('height', (screenSize));
    }   
    
    
    
    function aguarde(acao) {
        $('#modalAguarde').modal(acao);
    }
</script>
