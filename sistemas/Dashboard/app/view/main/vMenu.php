<style>
    .main-menu {
        margin: 0 -15px;
    }
    
    .main-menu li {
        --text-align: center;
    }
   
    .main-menu a {
        color: #4f5b5e;
        text-decoration: none;
    }
    .main-menu a:hover {
        background: #bbb;
        color: #000;
    }
    
    .main-menu a {
        position: relative;
        display: block;
        padding: 10px 15px
    }
    
    .main-menu .glyphicon {
        padding-right: 13px;
    }
</style>
<br>
<img class="center-block" src="<?php echo EMS_URL ?>/public/img/logo_ems2.png" alt=""/>
<h4 class="text-center">
    <small><?php echo APP_NAME ?></small>
</h4>
<hr>


