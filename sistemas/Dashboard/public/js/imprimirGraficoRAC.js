$('.btnImprimir').unbind().click(function (e) {
    e.preventDefault();
    aval = $(this).data('id');
    $.ajax({
        type: "POST",
        url: APP_URL+'avaliacao/getSVG/' + aval,
        dataType: 'json',
        success: function (data) {
            a = gerarGrafico(data);
            b = exportarGraficoLinha(a, 900, 900);
            location.href = APP_URL+'avaliacao/imprimir/' + aval + '/' + b;
        },
        error: function (data) {

        }
    });
});

$('#btnFormRAC').unbind().click(function (e) {
    e.preventDefault();
    aval = $(this).data('setor');
    $.ajax({
        type: "POST",
        url: APP_URL+'avaliacao/getSVGSetor/' + aval,
        dataType: 'json',
        success: function (data) {
            a = gerarGrafico(data);
            b = exportarGraficoLinha(a, 900, 900);
            
            $('#modalAguarde').modal('show');
            location.href = APP_URL+'avaliacao/imprimirRAC/' + aval + '/' + b;
            setTimeout(alertFunc, 5000);
        },
        error: function (data) {

        }
    });
});

function gerarGrafico(dados) {
    //console.log(dados);


    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            defaultSeriesType: 'spline'
        },
        title: {
            text: 'Evolução dos Pilares'
        },
        xAxis: {
            labels: {
            },
            categories: $.parseJSON(dados.cabecalho)
        },
        yAxis: [{
                title: {
                    text: 'Produtividade / Cob. Objetivo'
                },
                labels: {
                    format: '{value}',
                },
            }, {
                title: {
                    text: 'Presc. Share / Market Share'
                },
                labels: {
                    format: '{value}',
                },
                opposite: true
            }],
        plotOptions: {
            series: {
                animation: true
            },
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: $.parseJSON(dados.dados)
    });

    return chart;
}


function exportarGraficoLinha(chart, w, h) {
    var rdata;
    var exportUrl = EMS_URL+'/plugins/exporting-server/index.php';



    var nome_arquivo = '';
    var rdata = '';

    var d = new Date();
    var n = d.getTime();
    nome_arquivo = n.toString();

    var obj = {}, chart;
    obj.svg = chart.getSVG();
    obj.type = 'image/png';
    obj.width = w;
    //obj.scale = 2;
    obj.async = true;
    obj.filename = nome_arquivo;
    obj.sourceWidth = w;
    obj.sourceHeight = h;

    $.ajax({
        type: "POST",
        url: exportUrl,
        data: obj,
        cache: false,
        async: false,
        crossDomain: true,
        success: function (data) {
			console.log(data);
			console.log(data.status);
			console.log(data.statusText);
			//console.log(data);
            //alert(data);
            rdata = data;
            //return rdata;
            //return nome_arquivo;
        },
        error: function (data) {
			console.log(data);
			console.log(data.status);
			console.log(data.statusText);
           //alert(data);
            //alert("----"+data.status);
            //alert(data.statusText);
        }
    });

    var obj = {}
    d = rdata.split('.');
    return d[0];
}
    