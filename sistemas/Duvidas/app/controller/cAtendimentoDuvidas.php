<?php
require_once('lib/appController.php');
require_once('app/model/mAtendimentoDuvidas.php');
require_once('/../../../../app/model/mControleHeader.php');
require_once('app/view/formsAtendimentoDuvidas.php');

class cAtendimentoDuvidas extends appController {

    private $modelAtendimentoDuvidas = null;

    public function __construct(){
        $this->modelAtendimentoDuvidas = new mAtendimentoDuvidas();
    }

    public function main(){
        $this->render('vAtendimentoDuvidas');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsAtendimentoDuvidas();
                $m_sistema = new mControleHeader();
                $_POST['sistemas'] = $m_sistema->listSistemasDisponiveis();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsAtendimentoDuvidas();
                $m_sistema = new mControleHeader();
                $objeto = $this->loadObj();
                $_POST['sistemas'] = $m_sistema->listSistemasDisponiveis();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsAtendimentoDuvidas();
                $m_sistema = new mControleHeader();
                $objeto = $this->loadObj();
                $_POST['sistemas'] = $m_sistema->listSistemasDisponiveis();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $this->modelAtendimentoDuvidas->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $this->modelAtendimentoDuvidas->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        return $this->modelAtendimentoDuvidas->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        return $this->modelAtendimentoDuvidas->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $this->modelAtendimentoDuvidas->updateObj();
    }
}
