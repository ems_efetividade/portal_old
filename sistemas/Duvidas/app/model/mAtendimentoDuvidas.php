<?php
require_once('lib/appConexao.php');

class mAtendimentoDuvidas extends appConexao implements gridInterface {

    private $id;
    private $tags;
    private $titulo;
    private $resposta;
    private $dataCadastro;
    private $responsavel;
    private $idSistemaAcoplado;

    public function __construct(){
        $this->id;
        $this->tags;
        $this->titulo;
        $this->resposta;
        $this->dataCadastro;
        $this->responsavel;
        $this->idSistemaAcoplado;
    }

    public function getId(){
        return $this->id;
    }

    public function getTags(){
        return $this->tags;
    }

    public function getTitulo(){
        return $this->titulo;
    }

    public function getResposta(){
        return $this->resposta;
    }

    public function getDataCadastro(){
        return $this->dataCadastro;
    }

    public function getResponsavel(){
        return $this->responsavel;
    }

    public function getIdSistemaAcoplado(){
        return $this->idSistemaAcoplado;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setTags($Tags){
        $this->tags = $Tags;
    }

    public function setTitulo($Titulo){
        $this->titulo = $Titulo;
    }

    public function setResposta($Resposta){
        $this->resposta = $Resposta;
    }

    public function setDataCadastro($DataCadastro){
        $this->dataCadastro = $DataCadastro;
    }

    public function setResponsavel($Responsavel){
        $this->responsavel = $Responsavel;
    }

    public function setIdSistemaAcoplado($IdSistemaAcoplado){
        $this->idSistemaAcoplado = $IdSistemaAcoplado;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $sql = "select count(id) from ATENDIMENTO_DUVIDAS";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tags, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TAGS LIKE '%$tags%' ";
            $verif = true;
        }
        if(strcmp($titulo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TITULO LIKE '%$titulo%' ";
            $verif = true;
        }
        if(strcmp($resposta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESPOSTA LIKE '%$resposta%' ";
            $verif = true;
        }
        if(strcmp($dataCadastro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if(strcmp($responsavel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESPONSAVEL LIKE '%$responsavel%' ";
            $verif = true;
        }
        if(strcmp($idSistemaAcoplado, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID_SISTEMA_ACOPLADO LIKE '%$idSistemaAcoplado%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $tags = utf8_decode($_POST['tags']);
        $titulo = utf8_decode($_POST['titulo']);
        $resposta = utf8_decode($_POST['resposta']);
        $dataCadastro = utf8_decode($_POST['datacadastro']);
        $responsavel = utf8_decode($_POST['responsavel']);
        $idSistemaAcoplado = utf8_decode($_POST['idsistemaacoplado']);
        $sql = "INSERT INTO ATENDIMENTO_DUVIDAS ([TAGS],[TITULO],[RESPOSTA],[DATA_CADASTRO],[RESPONSAVEL],[ID_SISTEMA_ACOPLADO])";
        $sql .=" VALUES ('$tags','$titulo','$resposta','$dataCadastro','$responsavel','$idSistemaAcoplado')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $sql = "DELETE FROM ATENDIMENTO_DUVIDAS WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($tags, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TAGS = '$tags' ";
            $verif = true;
        }
        if(strcmp($titulo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TITULO = '$titulo' ";
            $verif = true;
        }
        if(strcmp($resposta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="RESPOSTA = '$resposta' ";
            $verif = true;
        }
        if(strcmp($dataCadastro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if(strcmp($responsavel, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="RESPONSAVEL = '$responsavel' ";
            $verif = true;
        }
        if(strcmp($idSistemaAcoplado, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID_SISTEMA_ACOPLADO = '$idSistemaAcoplado' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "TAGS ";
        $sql .= ",";
        $sql .= "TITULO ";
        $sql .= ",";
        $sql .= "RESPOSTA ";
        $sql .= ",";
        $sql .= "DATA_CADASTRO ";
        $sql .= ",";
        $sql .= "RESPONSAVEL ";
        $sql .= ",";
        $sql .= "ID_SISTEMA_ACOPLADO ";
        $sql .= " FROM ATENDIMENTO_DUVIDAS ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tags, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TAGS LIKE '%$tags%' ";
            $verif = true;
        }
        if(strcmp($titulo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TITULO LIKE '%$titulo%' ";
            $verif = true;
        }
        if(strcmp($resposta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESPOSTA LIKE '%$resposta%' ";
            $verif = true;
        }
        if(strcmp($dataCadastro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if(strcmp($responsavel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESPONSAVEL LIKE '%$responsavel%' ";
            $verif = true;
        }
        if(strcmp($idSistemaAcoplado, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID_SISTEMA_ACOPLADO LIKE '%$idSistemaAcoplado%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $tags = utf8_decode($_POST['tags']);
        $titulo = utf8_decode($_POST['titulo']);
        $resposta = utf8_decode($_POST['resposta']);
        $dataCadastro = utf8_decode($_POST['datacadastro']);
        $responsavel = utf8_decode($_POST['responsavel']);
        $idSistemaAcoplado = utf8_decode($_POST['idsistemaacoplado']);
        $sql = "UPDATE  ATENDIMENTO_DUVIDAS";
        $sql .=" SET ";
        $sql .="TAGS = '$tags' ";
        $sql .=" , ";
        $sql .="TITULO = '$titulo' ";
        $sql .=" , ";
        $sql .="RESPOSTA = '$resposta' ";
        $sql .=" , ";
        $sql .="DATA_CADASTRO = '$dataCadastro' ";
        $sql .=" , ";
        $sql .="RESPONSAVEL = '$responsavel' ";
        $sql .=" , ";
        $sql .="ID_SISTEMA_ACOPLADO = '$idSistemaAcoplado' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $tags = $_POST['tags'];
        $titulo = $_POST['titulo'];
        $resposta = $_POST['resposta'];
        $dataCadastro = $_POST['datacadastro'];
        $responsavel = $_POST['responsavel'];
        $idSistemaAcoplado = $_POST['idsistemaacoplado'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "TAGS ";
        $sql .= ",";
        $sql .= "TITULO ";
        $sql .= ",";
        $sql .= "RESPOSTA ";
        $sql .= ",";
        $sql .= "DATA_CADASTRO ";
        $sql .= ",";
        $sql .= "RESPONSAVEL ";
        $sql .= ",";
        $sql .= "ID_SISTEMA_ACOPLADO ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM ATENDIMENTO_DUVIDAS ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($tags, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TAGS LIKE '%$tags%' ";
            $verif = true;
        }
        if(strcmp($titulo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TITULO LIKE '%$titulo%' ";
            $verif = true;
        }
        if(strcmp($resposta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESPOSTA LIKE '%$resposta%' ";
            $verif = true;
        }
        if(strcmp($dataCadastro, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_CADASTRO IN ('$dataCadastro') ";
            $verif = true;
        }
        if(strcmp($responsavel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESPONSAVEL LIKE '%$responsavel%' ";
            $verif = true;
        }
        if(strcmp($idSistemaAcoplado, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID_SISTEMA_ACOPLADO LIKE '%$idSistemaAcoplado%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mAtendimentoDuvidas();
        $o->setId($param[0]);
        $o->setTags($param[1]);
        $o->setTitulo($param[2]);
        $o->setResposta($param[3]);
        $o->setDataCadastro($param[4]);
        $o->setResponsavel($param[5]);
        $o->setIdSistemaAcoplado($param[6]);

        return $o;

    }
}