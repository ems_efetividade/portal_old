<?php

class formsAtendimentoDuvidas
{

    public function formEditar($objeto)
    {
        $sistema_acoplado = $objeto->getIdSistemaAcoplado();
        ?>
    <form id="formEditar" action="<?php print appConf::caminho ?>AtendimentoDuvidas/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="col-sm-12    text-left">
                <div class="form-group form-group-sm">
                    <label for="tags">Palvras Chave</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getTags() ?>" name="tags"
                           id="tags">
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="titulo">Título</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getTitulo() ?>" name="titulo"
                           id="titulo">
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="resposta">Resposta</label>
                    <textarea rows="6" type="text" class="form-control "
                              name="resposta" id="resposta"><?php echo $objeto->getResposta() ?></textarea>
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="resposta">Acoplado</label>
                    <select name="idsistemaacoplado" class="form-control">
                        <option value="0">Não acoplado</option>
                        <?php
                        foreach ($_POST['sistemas'] as $sistema) {
                            $selected = "";
                            if ($sistema_acoplado==$sistema->getId()){
                                $selected = "selected";
                            }
                            ?>
                            <option <?php echo $selected ?>
                                    value="<?php echo $sistema->getId() ?>">
                                <?php echo $sistema->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($objeto)
    {
        $sistema_acoplado = $objeto->getIdSistemaAcoplado();
        ?>
        <div class="panel panel-default text-left">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion"
                       href="#collapse1"><?php echo $objeto->getTitulo() ?></a>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse in">
                <div class="panel-body"><?php echo $objeto->getResposta() ?></div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="resposta">Acoplado</label>
                    <select disabled name="idsistemaacoplado" class="form-control">
                        <option value="0">Não acoplado</option>
                        <?php
                        foreach ($_POST['sistemas'] as $sistema) {
                            $selected = "";
                            if ($sistema_acoplado==$sistema->getId()){
                                $selected = "selected";
                            }
                            ?>
                            <option <?php echo $selected ?>
                                    value="<?php echo $sistema->getId() ?>">
                                <?php echo $sistema->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
         </div>
        <?php
    }

    public function formCadastrar()
    {
        //var_dump($_POST['sistemas']);
        ?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>AtendimentoDuvidas/controlSwitch/salvar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="tags">Palavras Chave </label>
                    <small> Separar palavras chave por #, máximo 5 palavras</small>
                    <input required type="text" class="form-control " name="tags" id="tags">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="responsavel">Cadastrado Por</label>
                    <input required type="text" class="form-control " readonly value="<?php echo $_SESSION['nome'] ?>">
                    <input required type="hidden" class="form-control "
                           value="<?php echo $_SESSION['id_colaborador'] ?>" name="responsavel" id="responsavel">
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="titulo">Título</label>
                    <input required type="text" class="form-control " name="titulo" id="titulo">
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="resposta">Resposta</label>
                    <textarea rows="6" required type="text" class="form-control " name="resposta"
                              id="resposta"></textarea>
                </div>
            </div>
            <div class="col-sm-12 text-left">
                <div class="form-group form-group-sm">
                    <label for="resposta">Acoplado</label>
                    <select name="idsistemaacoplado" class="form-control">
                        <option value="0">Não acoplado</option>
                        <?php
                        foreach ($_POST['sistemas'] as $sistema) {
                            ?>
                            <option value="<?php echo $sistema->getId() ?>">
                                <?php echo $sistema->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        </form><?php
    }
}
