<?php
require_once('header.php');

class vAtendimentoDuvidas extends gridView{


    public function __construct(){
        $this->viewGrid();
    }


    public function viewGrid() {

        $cols = array('ID','Título','Resposta','Cadastrado Por');

        $array_metodo = array('Id','Titulo','Resposta','Responsavel');
        gridView::setGrid_titulo('Cadastrar Dúvidas Frequentes');
        gridView::setGrid_sub_titulo('Cadastre respostas objetivas');
        gridView::setGrid_cols($cols);
        //gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        //gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('AtendimentoDuvidas');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();

    }

}