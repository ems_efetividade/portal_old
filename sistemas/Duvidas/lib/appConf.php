<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('..\\..\\lib\\appGlobalVar.php');

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);
define('SERVER_URL', $_SERVER['HTTP_HOST']);
define('APP_FOLDER', '/sistemas/Duvidas/');
define('urlRoot', SERVER_URL.APP_FOLDER);
//define('root', 'C:\\xampp\\htdocs\\projetos\\termoCompensacao\\');
define('imgFolder', urlRoot.'public/images/');


class appConf {
    
    const nomeProjeto = 'Dúvidas Frequentes';
    const urlInicio = 3;
    const caminhoGrid = "https://".SERVER_URL."app/view/abs/";
    const caminho = "https://".SERVER_URL.APP_FOLDER;
    const caminhoFisico = ROOT;
    const imageFolder = imgFolder;
    const caminhoPdf = "https://".SERVER_URL.'/sistemas/Gerador/public/files/';
    const nfFolder = fileFolder;
    const nfFolderRoot = fileFolderRoot;
    const pastaTermo = 'public\\files\\';
    const pastaPDF = ROOT.'public\\pdf\\';
    const redirecionar = 'https://'.SERVER_URL;

}

?>