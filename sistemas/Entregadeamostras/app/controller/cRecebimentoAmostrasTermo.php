<?php
require_once('lib/appController.php');
require_once('app/model/mRecebimentoAmostrasTermo.php');
require_once('app/view/formsRecebimentoAmostrasTermo.php');

/**
 * Class cRecebimentoAmostrasTermo
 */
class cRecebimentoAmostrasTermo extends appController {

    private $modelRecebimentoAmostrasTermo = null;

    public function __construct(){
        $this->modelRecebimentoAmostrasTermo = new mRecebimentoAmostrasTermo();
    }

    public function main(){
        $this->render('vRecebimentoAmostrasTermo');
    }

    public function controlSwitch($acao){
        switch ($acao){
            case 'cadastrar':
                $forms = new formsRecebimentoAmostrasTermo();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsRecebimentoAmostrasTermo();
                $objeto = $this->loadObj();
                $arrayVisualizar = $this->arrayVisualizar($objeto);
                $forms->formVisualizar($arrayVisualizar);
                break;
            case 'editar':
                $forms = new formsRecebimentoAmostrasTermo();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function listarPesquisa(){
        $array_termos = $this->modelRecebimentoAmostrasTermo->listViewAmostras();
        foreach ($array_termos as $termo){
            $_POST['chavearquivo'] = $termo;
            $lista[] = $this->loadObj();
        }
        return $lista;
    }

    /**
     * Usado para verificar quem visualiza os termos enviados por outros usuários
     */
    public function controleVisualizacao(){

    }

    public function listaGrid(){
        //var_dump($_POST);
        $this->controleVisualizacao();
        $pesquisa = $_POST['isPesquisa'];
       // if($pesquisa=='1')
         //   $lista = $this->listarPesquisa();
       // else
            $lista = $this->listObj();
        $arrayGrid="";
        if(is_array($lista))
            foreach ($lista as $objeto){
                $arrayGrid[] = $this->arrayVisualizar($objeto);
            }
        return $arrayGrid;
    }

    public function arrayVisualizar($objeto){
        $nome_medico = $this->modelRecebimentoAmostrasTermo->getMedicosByCRM($objeto->getCrm());
        $produtos = $this->modelRecebimentoAmostrasTermo->arrayMedicamentosByTermo($objeto->getChaveArquivo());
        $nomeColaborador = $this->modelRecebimentoAmostrasTermo->getNomeColaboradorBySetor($objeto->getSetor());
        $itens = $this->modelRecebimentoAmostrasTermo->getMedicamentosByTermo(($objeto->getChaveArquivo()));
        $arrayVisualizar =  array(
            'id'=>$objeto->getId(),
            'crm'=>$objeto->getCrm(),
            'chave_arquivo'=>$objeto->getChaveArquivo(),
            'data_assinatura'=>$objeto->getDataAssinatura(),
            'data_envio'=>$objeto->getDataEnvio(),
            'medico'=>$nome_medico,
            'setor'=>$objeto->getSetor(),
            'nome_colaborador'=>$nomeColaborador,
            'produtos'=>$produtos,
            'itens'=>$itens);
        return $arrayVisualizar;
    }

    public function listarEstados() {
        return $this->modelRecebimentoAmostrasTermo->listarEstados();
    }

    public function listarProdutos() {
        return $this->modelRecebimentoAmostrasTermo->listarProdutos();
    }

    public function getMedicoByCRM($crm='0'){
        $retorno = $this->modelRecebimentoAmostrasTermo->getMedicosByCRM($crm);
        echo $retorno;
    }

    public function guardarArquivo() {
        $arquivo = $_FILES['termo'];
        $todayDB = date("Y_m_d_H_i_s");
        $caminho = appConf::pastaPDF;
        $chavearquivo = $_POST['setor'].$todayDB.$arquivo['name'];
        if(move_uploaded_file($arquivo['tmp_name'], $caminho.$chavearquivo)) {
            return $chavearquivo;
        }
        else{
            return 0;
        }
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $quantidade = $_POST['quantidade'];
        $codigo = $_POST['codigo'];
        $_POST['crm'] = $_POST['uf'].$crm;
        $erro = 0;

        if(strcmp($crm,"0000000")==0){
            $erro = "Preencha o CRM do médico";
        }else if(strcmp($dataAssinatura,"")==0){
            $erro = "Informe a data de assinatura do termo";
        }else if(strcmp($_POST['uf'],"")==0){
            $erro = "Informe o UF do CRM";
        }else if(strcmp($quantidade[0],"")==0){
            $erro = "Informe pelo menos um medicamento";
        }
        if($erro!="0"){
            echo $erro;
            return;
        }
        $chaveArquivo  = $this->guardarArquivo();
        $_POST['chavearquivo'] = $chaveArquivo;
        if(strcmp($chaveArquivo,"0")==0){
            $erro = "Erro ao salvar o arquivo";
        }
        if($erro!="0"){
            echo $erro;
            return;
        }
        $this->modelRecebimentoAmostrasTermo->saveItens();
        $this->modelRecebimentoAmostrasTermo->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];

        $this->modelRecebimentoAmostrasTermo->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);
        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];

        return $this->modelRecebimentoAmostrasTermo->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];
        if($id!='') {
            unset($_POST);
            $_POST['id'] = trim($id);
        }
        return $this->modelRecebimentoAmostrasTermo->loadObj();

    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $newcrm = $_POST['newcrm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $newchaveArquivo = $_POST['newchavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $newdataAssinatura = $_POST['newdataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $newdataEnvio = $_POST['newdataenvio'];
        $setor = $_POST['setor'];
        $newsetor = $_POST['newsetor'];
        $id = $_POST['id'];
        $newid = $_POST['newid'];

        $objeto =  $this->modelRecebimentoAmostrasTermo->loadObj();
        $this->modelRecebimentoAmostrasTermo->updateObj($objeto);
    }
}
