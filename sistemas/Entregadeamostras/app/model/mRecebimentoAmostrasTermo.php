<?php
require_once('lib/appConexao.php');

class mRecebimentoAmostrasTermo extends appConexao implements gridInterface {

    private $crm;
    private $chaveArquivo;
    private $dataAssinatura;
    private $dataEnvio;
    private $setor;
    private $id;

    public function __construct(){
        $this->crm;
        $this->chaveArquivo;
        $this->dataAssinatura;
        $this->dataEnvio;
        $this->setor;
        $this->id;
    }

    public function listViewAmostras(){
        $_POST = appSanitize::filter($_POST);
        $p_produto = $_POST['p_produto'];
        $p_nome_medico = $_POST['p_nome_medico'];
        $p_crm = $_POST['p_crm'];
        $p_data_assinatura = $_POST['p_data_assinatura'];
        $p_data_envio = $_POST['p_data_envio'];
        $p_representante = $_POST['p_representante'];
        $p_setor = $_POST['p_setor'];

        $verif = false;
        if(strcmp($p_setor, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="SETOR LIKE '%$p_setor%' ";
            $verif = true;
        }
        if(strcmp($p_representante, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="REPRESENTANTE LIKE '%$p_representante%' ";
            $verif = true;
        }
        if(strcmp($p_data_envio, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="CONVERT(VARCHAR,DATA_ENVIO,103) = '$p_data_envio' ";
            $verif = true;
        }
        if(strcmp($p_data_assinatura, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="CONVERT(VARCHAR,DATA_ASSINATURA,103) = '$p_data_assinatura' ";
            $verif = true;
        }
        if(strcmp($p_crm, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="CRM LIKE '%$p_crm%' ";
            $verif = true;
        }
        if(strcmp($p_produto, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="PRODUTO LIKE '%$p_produto%' ";
            $verif = true;
        }
        if(strcmp($p_nome_medico, "") != 0){
            if($verif){
                $sql .=" OR ";
            }
            $sql .="NOME_MEDICO LIKE '%$p_nome_medico%' ";
            $verif = true;
        }

        $rs = "select distinct CHAVE_ARQUIVO from vw_RECEBIMENTO_AMOSTRAS";
        if($verif){
            $rs = $rs. ' WHERE '.$sql;
        }
        $rs = $this->executarQueryArray($rs);
        $view = array();
        if (count($rs) > 0) {
            foreach ($rs as $row) {
                    $view[] = $row['CHAVE_ARQUIVO'];
            }
        }
        return $view;
    }

    public function getMedicamentosByTermo($termo=''){
        if($termo=='')
            $termo = $this->getChaveArquivo();
        $sql = "SELECT SUM(QUANTIDADE) FROM [RECEBIMENTO_AMOSTRAS_ITEM] WHERE FK_CHAVE_ARQUIVO = '".$termo."'";
        $retorno =  $this->executarQueryArray($sql);
        return $retorno[1][0];

    }

    public function getNomeColaboradorBySetor($setor=''){
        if($setor=='')
            $setor = $this->getSetor();
        $sql = "SELECT NOME from vw_colaboradorSetor where SETOR = '".$setor."'";
        $retorno =  $this->executarQueryArray($sql);
        return $retorno[1]['NOME'];
    }

    public function arrayMedicamentosByTermo($termo='0'){
        if($termo=='0')
            $termo = $this->getChaveArquivo();
        $sql = "SELECT QUANTIDADE, COD_ITEM FROM [RECEBIMENTO_AMOSTRAS_ITEM] WHERE FK_CHAVE_ARQUIVO = '".$termo."'";
        $retorno =  $this->executarQueryArray($sql);
        $arrayLista='';
        foreach ($retorno as $value){
            $arrayLista[]= array ('quantidade'=>$value['QUANTIDADE'],'codigo'=>$value['COD_ITEM']);
        }
        foreach ($arrayLista as $key =>$value) {
            $produto = $this->getProdutosByCodigo($value['codigo']);
            $arrayRetorno[] = array('codigo' => $value['codigo'], 'produto' =>$produto[0][$value['codigo']],'quantidade'=>$value['quantidade']);
        }
        return $arrayRetorno;
    }

    public function listarEstados() {
        $rs = $this->executarQueryArray("select sigla from ESTADO");
        $estados = array();
        if (count($rs) > 0) {
            foreach ($rs as $row) {
                $estados[] = $row['sigla'];
            }
        }
        return $estados;
    }

    public function listarProdutos() {
        $rs = $this->executarQueryArray("select * from RECEBIMENTO_AMOSTRAS_PRODUTO");
        $cadastros = array();
        if (count($rs) > 0) {
            foreach ($rs as $row) {
                    $cadastros[$row['CODIGO']] = $row['NOME'];
            }
        }
        return $cadastros;
    }

    public function getMedicosByCRM($crm="0"){
        if($crm=="0")
            $crm = $this->getCrm();
        $rs = $this->executarQueryArray("exec proc_crm_medicos '" . $crm . "'");
        $medico = 0;
        if (count($rs) > 0) {
            foreach ($rs as $row) {
                $medico = $row['nome'];
            }
        }
        return $medico;
    }

    public function getProdutosByCodigo($codigo = "0") {

        if($codigo=='0')
            $rs = $this->executarQueryArray("select * from RECEBIMENTO_AMOSTRAS_PRODUTO");
        else
            $rs = $this->executarQueryArray("select * from RECEBIMENTO_AMOSTRAS_PRODUTO WHERE CODIGO = '".$codigo."'");
        $cadastros = array();
        if (count($rs) > 0) {
            foreach ($rs as $row) {
                $cadastros[] = array ($row['CODIGO']=>$row['PRODUTO']);
            }
        }
        return $cadastros;
    }

    public function saveItens()
    {
        $chaveArquivo = $_POST['chavearquivo'];
        $quantidadeArray = $_POST['quantidade'];
        $codItemArray = $_POST['codigo'];
        $i = 0;
        foreach ($codItemArray as $codItem){
            $sql = "INSERT INTO RECEBIMENTO_AMOSTRAS_ITEM ([FK_CHAVE_ARQUIVO],[QUANTIDADE],[COD_ITEM])";
            $sql .= " VALUES ('$chaveArquivo','$quantidadeArray[$i]','$codItem')";
            $this->executar($sql);
            $i++;
        }
    }

    public function getCrm(){
        return $this->crm;
    }

    public function getChaveArquivo(){
        return $this->chaveArquivo;
    }

    public function getDataAssinatura(){
        return $this->dataAssinatura;
    }

    public function getDataEnvio(){
        return $this->dataEnvio;
    }

    public function getSetor(){
        return $this->setor;
    }

    public function getId(){
        return $this->id;
    }

    public function setCrm($Crm){
        $this->crm = $Crm;
    }

    public function setChaveArquivo($ChaveArquivo){
        $this->chaveArquivo = $ChaveArquivo;
    }

    public function setDataAssinatura($DataAssinatura){
        $this->dataAssinatura = $DataAssinatura;
    }

    public function setDataEnvio($DataEnvio){
        $this->dataEnvio = $DataEnvio;
    }

    public function setSetor($Setor){
        $this->setor = $Setor;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];

        $sql = "INSERT INTO RECEBIMENTO_AMOSTRAS_TERMO ([CRM],[CHAVE_ARQUIVO],[DATA_ASSINATURA],[DATA_ENVIO],[SETOR])";
        $sql .=" VALUES ('$crm','$chaveArquivo','$dataAssinatura','$dataEnvio','$setor')";
        $this->executar($sql);
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);
        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];
        $sql = "DELETE FROM RECEBIMENTO_AMOSTRAS_TERMO WHERE ";
        $verif = false;
        if(strcmp($crm, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="CRM = '$crm' ";
            $verif = true;
        }
        if(strcmp($chaveArquivo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="CHAVE_ARQUIVO = '$chaveArquivo' ";
            $verif = true;
        }
        if(strcmp($dataAssinatura, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DATA_ASSINATURA = '$dataAssinatura' ";
            $verif = true;
        }
        if(strcmp($dataEnvio, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DATA_ENVIO = '$dataEnvio' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="SETOR = '$setor' ";
            $verif = true;
        }
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }

    public function loadObj(){
        $_POST = appSanitize::filter($_POST);
        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];
        $verif = false;
        $sql = "SELECT ";
        $sql .= "CRM ";
        $sql .= ",";
        $sql .= "CHAVE_ARQUIVO ";
        $sql .= ",";
        $sql .= "DATA_ASSINATURA ";
        $sql .= ",";
        $sql .= "DATA_ENVIO ";
        $sql .= ",";
        $sql .= "SETOR ";
        $sql .= ",";
        $sql .= "ID ";
        $sql .= " FROM RECEBIMENTO_AMOSTRAS_TERMO ";

        if(strcmp($crm, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CRM LIKE '%$crm%' ";
            $verif = true;
        }
        if(strcmp($chaveArquivo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CHAVE_ARQUIVO LIKE '%$chaveArquivo%' ";
            $verif = true;
        }
        if(strcmp($dataAssinatura, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_ASSINATURA IN ('$dataAssinatura') ";
            $verif = true;
        }
        if(strcmp($dataEnvio, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_ENVIO LIKE '%$dataEnvio%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function updateObj(){
        $crm = $_POST['newcrm'];
        $chaveArquivo = $_POST['newchavearquivo'];
        $dataAssinatura = $_POST['newdataassinatura'];
        $dataEnvio = $_POST['newdataenvio'];
        $setor = $_POST['newsetor'];
        $id = $_POST['newid'];
        $sql = "UPDATE RECEBIMENTO_AMOSTRAS_TERMO";
        $sql .=" SET CRM = '$crm' ,";
        $sql .=" CHAVE_ARQUIVO = '$chaveArquivo' ,";
        $sql .=" DATA_ASSINATURA = '$dataAssinatura' ,";
        $sql .=" DATA_ENVIO = '$dataEnvio' ,";
        $sql .=" SETOR = '$setor' ,";
        $sql .=" ID = '$id' ";
        $sql .=" WHERE CRM = '".$objeto->getCrm()."' AND ";
        $sql .="  CHAVE_ARQUIVO = '".$objeto->getChaveArquivo()."' AND ";
        $sql .="  DATA_ASSINATURA = '".$objeto->getDataAssinatura()."' AND ";
        $sql .="  DATA_ENVIO = '".$objeto->getDataEnvio()."' AND ";
        $sql .="  SETOR = '".$objeto->getSetor()."' AND ";
        $sql .="  ID = '".$objeto->getId()."'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];
        $verif = false;
        $sql = "SELECT ";
        $sql .= "CRM ";
        $sql .= ",";
        $sql .= "CHAVE_ARQUIVO ";
        $sql .= ",";
        $sql .= "DATA_ASSINATURA ";
        $sql .= ",";
        $sql .= "DATA_ENVIO ";
        $sql .= ",";
        $sql .= "SETOR ";
        $sql .= ",";
        $sql .= "ID ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM RECEBIMENTO_AMOSTRAS_TERMO ";

        if(strcmp($crm, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CRM LIKE '%$crm%' ";
            $verif = true;
        }
        if(strcmp($chaveArquivo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CHAVE_ARQUIVO LIKE '%$chaveArquivo%' ";
            $verif = true;
        }
        if(strcmp($dataAssinatura, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_ASSINATURA LIKE '%$dataAssinatura%' ";
            $verif = true;
        }
        if(strcmp($dataEnvio, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_ENVIO LIKE '%$dataEnvio%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;

        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();
        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);
        $crm = $_POST['crm'];
        $chaveArquivo = $_POST['chavearquivo'];
        $dataAssinatura = $_POST['dataassinatura'];
        $dataEnvio = $_POST['dataenvio'];
        $setor = $_POST['setor'];
        $id = $_POST['id'];
        $sql = "select count(id) from RECEBIMENTO_AMOSTRAS_TERMO";
        if(strcmp($crm, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CRM LIKE '%$crm%' ";
            $verif = true;
        }
        if(strcmp($chaveArquivo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CHAVE_ARQUIVO LIKE '%$chaveArquivo%' ";
            $verif = true;
        }
        if(strcmp($dataAssinatura, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_ASSINATURA LIKE '%$dataAssinatura%' ";
            $verif = true;
        }
        if(strcmp($dataEnvio, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DATA_ENVIO LIKE '%$dataEnvio%' ";
            $verif = true;
        }
        if(strcmp($setor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="SETOR LIKE '%$setor%' ";
            $verif = true;
        }
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function montaObj($param){

        $o = new mRecebimentoAmostrasTermo();
        $o->setCrm($param[0]);
        $o->setChaveArquivo($param[1]);
        $o->setDataAssinatura($param[2]);
        $o->setDataEnvio($param[3]);
        $o->setSetor($param[4]);
        $o->setId($param[5]);
        return $o;

    }

    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }
}