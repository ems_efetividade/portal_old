<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('lib/appConf.php');
	require_once('lib/appFunction.php');
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 



<title>Portal EMS Prescrição</title>

<!--Plugin javascript do jquery e bootstrap-->
    <script src="/../../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
<link rel="shortcut icon" href="<?php echo appConf::caminho ?>public/img/liferay.ico" type="image/x-icon" />
<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css" />
<style>
/*VACINA PRO IPAD EM LANDSCAPE*/
@media all and (min-width: 768px) and (max-width: 1024px) {
    .navbar-header {
        float: none;
    }
    .navbar-left,.navbar-right {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
		top: 0;
		border-width: 0 0 1px;
	}
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
		margin-top: 7.5px;
	}
	.navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
  		display:block !important;
	}
}





</style>
<script>
    $(document).ready(function(e){
        //$('.panel-left').css('height', (screen.height-100));
        //resizePanelLeft();
    });
    
    function resizePanelLeft() {
        
        var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
        
        $('.panel-left').css('height', (screenSize));
    }
</script>
</head>
