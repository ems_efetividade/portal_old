<?php require_once('includes.php'); ?>

<style>
    .panel-left { background: #ddd; }
    .progress { margin-bottom: 0px; height: 15px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
            
            <div class="row">
                <div class="col-lg-12">
                    <h2><b><span class="glyphicon glyphicon-pencil"></span> Assinaturas</b> <small> - Documentos Assinados</small></h2>
                </div>
                
            </div>
            

            <hr>
          
            <form id="formPesquisa" action="<?php echo appConf::caminho ?>admin/pesquisarResposta" method="post">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group">
                        <select class="form-control" name="cmbCampo">
                            <option value="">Pesquisar Por:</option>
                            <option value="R.SETOR">SETOR</option>
                            <option value="C.NOME">COLABORADOR</option>
                            <option value="D.TITULO">DOCUMENTO</option>
                        </select>
                    </div>
                </div>
                <div class="col-lg-8">
                    <div class="form-group">
                        <input type="email" name="txtCriterio" class="form-control" id="exampleInputEmail1" placeholder="Critério...">
                    </div>
                </div>
                <div class="col-lg-2">
                    <div class="form-group">
                        <button type="button" id="btnPesquisar" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
                    </div>
                </div>
            </div>
            </form>
            
            <hr>
            
            <div id="assinatura">
            <table class="table table-striped">
                <tr>
                <th><small>Colaborador</small></th>
                <th><small>Documento</small></th>
                <th><small>Data Assinatura</small></th>
                <th></th>
                </tr>
                <?php foreach($respostas as $resp) { ?>
                <tr>
                    <td><small><?php echo $resp['SETOR'].' '. $resp['NOME'] ?></small></td>
                    <td><small><?php echo $resp['DOCUMENTO']; ?></small></td>
                    <td><small><?php echo appFunction::formatarData($resp['DATA_CADASTRO']) ?></small></td>
                    <td>
                        <a class="btn btn-xs btn-primary" href="<?php echo appConf::caminho ?>home/gerarArquivoPDF/<?php echo $resp['ID_DOCUMENTO'] ?>" ><span class="glyphicon glyphicon-save"></span>&nbsp;</a>
                        <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#modalExcluir" data-id="<?php echo $resp['ID_DOCUMENTO'] ?>"><span class="glyphicon glyphicon-remove"></span>&nbsp;</a>
                    </td>
                </tr>
                <?php } ?>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção</h4>
      </div>
      <div class="modal-body">
        <h4 class="text-center">Deseja excluir essa assinatura?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-danger" id="btnExcluirDocumento">Excluir</button>
      </div>
    </div>
  </div>
</div>



<script>
    
resizePanelLeft();

$('#btnPesquisar').unbind().click(function(e) {
    $.ajax({
        type: "POST",
        url: $('#formPesquisa').attr('action'),
        data: $('#formPesquisa').serialize(),
        beforeSend: function() {
            $('#assinatura').html('Aguarde...');
        },
        success: function(retorno) {
           // alert(retorno);
           $('#assinatura').html(retorno);
           resizePanelLeft();
        }
    });
});    
    
$('#modalExcluir').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes


    $('#btnExcluirDocumento').unbind().click(function(e) {

        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>admin/excluirResposta/'+id,
            success: function(retorno) {
                if($.trim(retorno) != "") {
                    $('#modalExcluir').modal('hide');
                    $('#modalErro .modal-body h4').html(retorno);
                    $('#modalErro').modal('show');
                } else {
                    window.location = '<?php echo appConf::caminho ?>admin/assinaturas';
                }
            }
        });
        
    });

});
</script>