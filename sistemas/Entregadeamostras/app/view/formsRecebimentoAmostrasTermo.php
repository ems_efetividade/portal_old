<?php
class formsRecebimentoAmostrasTermo{

    public function formEditar($objeto){?>
    <form id="formEditar" action="<?php print appConf::caminho ?>RecebimentoAmostrasTermo/controlSwitch/atualizar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="id">ID</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getId()?>" name="id" id="id" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getId()?>" name="newid" id="newid" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="crm">CRM</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getCrm()?>" name="crm" id="crm" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getCrm()?>" name="newcrm" id="newcrm" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nomeMedico">NOMEMEDICO</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getNomeMedico()?>" name="nomemedico" id="nomeMedico" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getNomeMedico()?>" name="newnomemedico" id="newnomeMedico" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="representante">REPRESENTANTE</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getRepresentante()?>" name="representante" id="representante" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getRepresentante()?>" name="newrepresentante" id="newrepresentante" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="chaveAssinatura">CHAVEASSINATURA</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getChaveAssinatura()?>" name="chaveassinatura" id="chaveAssinatura" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getChaveAssinatura()?>" name="newchaveassinatura" id="newchaveAssinatura" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="assinado">ASSINADO</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getAssinado()?>" name="assinado" id="assinado" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getAssinado()?>" name="newassinado" id="newassinado" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataAssinatura">DATAASSINATURA</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getDataAssinatura()?>" name="dataassinatura" id="dataAssinatura" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getDataAssinatura()?>" name="newdataassinatura" id="newdataAssinatura" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="codMedicamento">CODMEDICAMENTO</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getCodMedicamento()?>" name="codmedicamento" id="codMedicamento" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getCodMedicamento()?>" name="newcodmedicamento" id="newcodMedicamento" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="termo">TERMO</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getTermo()?>" name="termo" id="termo" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getTermo()?>" name="newtermo" id="newtermo" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="setor">SETOR</label>
                    <input type="hidden"  class="form-control "   value="<?php echo $objeto->getSetor()?>" name="setor" id="setor" >
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getSetor()?>" name="newsetor" id="newsetor" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($arrayDados){
        ?>
        <form id="formVisualizar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label>Representante</label>
                    <label class="form-control" ><?php echo $arrayDados['nome_colaborador']?></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label>Setor</label>
                    <label class="form-control" ><?php echo $arrayDados['setor']?></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label>Data Assinatura</label>
                    <label class="form-control" ><?php echo date('d/m/Y', strtotime($arrayDados['data_assinatura']))?></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label>Data Envio</label>
                    <label class="form-control" ><?php echo date('d/m/Y', strtotime($arrayDados['data_envio']))?></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label>CRM</label>
                    <label class="form-control" ><?php echo $arrayDados['crm']?></label>
                </div>
            </div>
            <div class="col-sm-8 text-left">
                <div class="form-group form-group-sm">
                    <label>Médico</label>
                    <label class="form-control" ><?php echo $arrayDados['medico']?></label>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-sm">
                    <label style="color: white;">.</label><br>
                    <a style="text-decoration: none" class="btn btn-primary form-control" download="Termo.pdf" href="<?php echo appConf::caminhoPdf.$arrayDados['chave_arquivo']?>"><label class="glyphicon glyphicon-save-file"></label><label>&nbsp; Download</label></a>
                </div>
            </div>
            <div class="col-sm-12 text-justify">
                <div class="form-group form-group-sm">
                    <div style="overflow: auto; max-height: 160px;">
                        <table class="table table-condensed table-striped" id="produtos">
                            <thead class="thead-dark">
                            <tr>
                                <th scope="col">Cod.</th>
                                <th scope="col">Quantidade</th>
                                <th scope="col">Produto</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($arrayDados['produtos'] as $produto){
                                echo "<tr>";
                                echo '<td>'.$produto["codigo"].'</td>';
                                echo '<td>'.$produto["quantidade"].'</td>';
                                echo '<td>'.$produto["produto"].'</td>';
                                echo "</tr>";
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar(){
        require_once('/../controller/cRecebimentoAmostrasTermo.php');
        $control = new cRecebimentoAmostrasTermo();
        $today = date("d/m/Y");
        $todayDB = date("Y-m-d");
        ?>
        <script>
            function alertErro(mensagem) {
                $("#erro").html(mensagem);
                $("#erro").fadeIn();
                setTimeout(function() {
                    $("#erro").fadeOut();
                }, 4000);
            }
            $(document).ready(function(e) {
                $('#btnAddSetor').click(function(e) {
                    id = $('#cmbEquipe').val();
                    texto = $('#cmbEquipe option:selected').text();
                    tr = '<tr><td><input type="hidden" name="txtSetor[]" value ="' + id + '" />' + texto + '</td><td><button type="button" class="btn btn-xs btn-danger pull-right">Excluir</button></td></tr>';
                    $('#tbPermitido').append(tr);
                });
                $('.maskCRM').mask('0000000', {
                    reverse: true
                });
                $("#btnCrm").click(function() {
                    uf = $("#uf").val();
                    crm = $("#crmSave").val();
                    if (uf == "") {
                        msgBox("Selecione o UF.");
                        return;
                    }
                    if (crm == "") {
                        msgBox("Digite o CRM do Médico.");
                        return;
                    }
                    if (crm.length < 7) {
                        msgBox('CRM deve conter 7 Digitos <br><br><sub>Utilize "0" para completar campoas vazios</sub>');
                        return;
                    }
                    retorno = requisicaoAjax('<?php print appConf::caminho ?>RecebimentoAmostrasTermo/getMedicoByCrm/' + uf + crm);
                    if (retorno == '0') {
                        msgBox("Médico não encontrado");
                        return;
                    }
                    document.getElementById("nomeMedico").innerHTML = retorno;
                });
            });
        </script>
        <form id="formSalvar" action="<?php print appConf::caminho ?>RecebimentoAmostrasTermo/controlSwitch/salvar">
            <div class="row  text-left">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="representante">Responsável</label>
                        <label required  type="text" class="form-control" name="representante" id="representante" ><?php echo $_SESSION['nome']?></label>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="setor">Setor</label>
                        <label required  type="text" class="form-control " ><?php echo $_SESSION['setor']?></label>
                        <input type="hidden" value="<?php echo $_SESSION['setor']?>" name="setor" id="setor">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataAssinatura">Data Envio</label>
                        <label required type="date"  readonly class="form-control">
                            <?php echo $today;?></label>
                        <input type="hidden" value="<?php echo $todayDB?>" name="dataenvio" id="dataEnvio">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataAssinatura">Data Assinatura</label>
                        <input required type="date" class="form-control "
                               name="dataassinatura" id="dataAssinatura" >
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="form-group form-group-sm">
                        <label for="exampleInputEmail1">UF</label>
                        <select id="uf" name="uf" class="form-control">
                            <option value=""></option>
                            <?php
                            foreach ($control->listarEstados() as $key => $sigla)
                                echo '<option value="'.$sigla.'">'.$sigla.'</option>'
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="crm">CRM</label>
                        <input type="text" id="crmSave" name="crm" pattern="[0-9]{7}" title="CRM com 7 digitos" value="0000000" class="form-control maskCRM">
                    </div>
                </div>
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="crm" style="color:white;">.</label>
                        <label id="btnCrm" class="form-control btn btn-default"><span class="glyphicon glyphicon-search m-0 p-0" ></span> Pesquisar</label>
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <label for="nomeMedico">Médico</label>
                        <label required type="text"  class="form-control " name="nomemedico" id="nomeMedico" ></label>
                    </div>
                </div>
                <div class="col-sm-10 text-left">
                    <div class="form-group form-group-sm">
                        <label for="termo" >Arquivo</label>
                        <label  for="termo"  class="form-control"  id="nomeTermo" ></label>
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="termo" style="color: white;">.</label><br>
                        <label for="termo" class="form-control btn-default text-center">
                            <label for="termo" id="btnTermo" style="font-size: 18px" class="glyphicon glyphicon-cloud-upload"> </label>
                        </label>
                        <input required style="display: none;" type="file" name="termo" id="termo" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="codMedicamento">Código</label>
                        <label required  type="text" class="form-control " name="codmedicamento" id="codMedicamento" ></label>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="nomeMedicamento">Medicamento</label>
                        <select onchange="setarCodigo()" id="nomeMedicamento" name="nomeMedicamento" class="form-control">
                            <option value="0">Selecione...</option>
                            <?php
                            foreach ($control->listarProdutos() as $key => $produtos)
                                echo '<option id="'.$key.'" value="'.$key.'">'.$produtos.'</option>'
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="quantidade">Quantidade</label>
                        <input type="number" id="quantidade" name="quantidade" class="form-control" required/>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="quantidade" style="color: white">Controle Especial</label>
                        <label for="quantidade">Controle Especial</label>
                        <input type="checkbox" id="quantidade" name="quantidade" class="" required/>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="btnaAicionar" style="color:white;">.</label>
                        <span id="btnaAicionar" onclick="addItem()" class="btn btn-default form-control btn-block btn-lg">
                            <span class="glyphicon glyphicon-plus m-0 p-0" ></span> Adicionar
                        </span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group form-group-sm">
                        <div style="overflow: auto; height: 120px;">
                            <table class="table table-condensed table-striped" id="produtos">
                                <thead class="thead-dark">
                                <tr>
                                    <th scope="col">Cod.</th>
                                    <th scope="col">Quantidade</th>
                                    <th scope="col">Produto</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <script>
                            function addItem() {
                                var nome = $('#nomeMedicamento').val();
                                var codigo = $('#codMedicamento').val();
                                var quantidade = $('#quantidade').val();

                                if (codigo == 0){
                                    msgBox("Selecione o Produto.");
                                    return;
                                }
                                if (quantidade < 1 ) {
                                    msgBox("Informe a quantidade Fornecida");
                                    return;
                                }

                                var table = document.getElementById("produtos");
                                var row = table.insertRow(1);
                                var cell1 = row.insertCell(0);
                                var cell2 = row.insertCell(1);
                                var cell3 = row.insertCell(2);
                                var cell4 = row.insertCell(3);

                                cell1.innerHTML = codigo;
                                cell2.innerHTML = quantidade;
                                cell3.innerHTML = $('#'+nome).text();
                                cell4.innerHTML = '<input type="hidden" name="codigo[]" value="'+codigo+'"> <input type="hidden" name="quantidade[]" value="'+quantidade+'"><i id="btnCrm" onclick="removeItem('+codigo+')" class="btn btn-danger form-control btn-block"><i class="glyphicon glyphicon-trash"> Remover </i></i>';
                                row.id='tr'+codigo;
                                $('#nomeMedicamento').val("");
                                $('#quantidade').val("");
                                $('#codMedicamento').val("");
                                $('#codMedicamento').text("");
                            }
                            function removeItem(codigo) {
                                var tr = document.getElementById("tr"+codigo);
                                console.log(tr);
                                tr.remove();
                            }
                            function setarCodigo() {
                                codigo = document.getElementById('nomeMedicamento').value;
                                document.getElementById('codMedicamento').innerText = codigo;
                                document.getElementById('codMedicamento').value = codigo;
                            }
                            $("#termo").change(function () {
                                input = document.getElementById("termo");
                                nome = "Não há arquivo selecionado. Selecionar arquivo...";
                                console.log(input.files.length);
                                if(input.files.length > 0)
                                    nome = input.files[0].name;
                                document.getElementById("nomeTermo").innerText = nome;
                            });

                        </script>
                    </div>
                </div>
        </form><?php
    }
}
