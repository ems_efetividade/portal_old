/*
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

$('#modalMarketing').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var idLancamento = (button.data('id-lancamento') === undefined) ? 0 : parseInt(button.data('id-lancamento')); // Extract info from data-* attributes
    var idDespesa = (button.data('id-despesa') === undefined) ? 0 : parseInt(button.data('id-despesa')); // Extract info from data-* attributes
    formulario = requisicaoAjax('<?php print appConf::root ?>despesa/formularioMarketing/'+idLancamento+'/'+idDespesa);
    $('#modalMarketing .modal-body').html(formulario); 
});