<?php
require_once('header.php');
//require_once('lib/appConexao.php');
class vRecebimentoAmostrasTermo extends gridView{


    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        //ini_set('display_errors',1);
        //ini_set('display_startup_erros',1);
        //error_reporting(E_ALL);

        $colsPesquisaNome = array('CRM','Médico','Data Envio (DD/MM/AAAA)','Data Assinatura (DD/MM/AAAA)','Medicamentos', 'Setor', 'Representante');
        $colsPesquisa = array('P_PRODUTO','P_NOME_MEDICO','P_CRM','P_DATA_ASSINATURA','P_DATA_ENVIO','P_REPRESENTANTE','P_SETOR');
        $cols = array('CRM','Médico','Data Envio','Data Assinatura','Itens');
        $array_metodo = array('crm','medico','data_envio','data_assinatura','itens');
        gridView::setGrid_titulo('Termos Enviados');
        gridView::setGrid_sub_titulo('Relatório de termos de amostras dispensadas');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('RecebimentoAmostrasTermo');
        gridView::setGrid_metodo('listaGrid');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();

    }

}