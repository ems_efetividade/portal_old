<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once("/../../../app/view/abs/gridView.php");
require_once("/../../../app/view/interface/gridInterface.php");
class appView {
	
	public $appDados = array();
	
	public function set($nome, $valor) {
		$this->appDados[$nome] = $valor;	
	}
	
	public function bind($nome, $valor) {
        $this->appDados[$nome] = $valor; 
    }
	
	public function get($nome='') {
        if ($nome == '') {
            return $this->appDados;
        }
        else {
            if (isset($this->appDados[$nome]) && ($this->appDados[$nome] != '')) {
                return $this->appDados[$nome];
            }
            else {
                return '';
            }
        }
    }
	
	public function render($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		if (file_exists("app/view/{$arquivo}.php")) {
            include "app/view/{$arquivo}.php";
            new $arquivo();
        }
		
	}
	
	public function renderToString($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		
		if (file_exists("app/view/{$arquivo}.php")) {
			ob_start();
			include("app/view/{$arquivo}.php");
			return ob_get_clean();
        }
		
	}
}

?>