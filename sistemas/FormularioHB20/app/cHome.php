<?php 

require_once('lib/appController.php');

require_once('app/model/mProduto.php');
require_once('app/model/mPDV.php');
require_once('app/model/mPedidoProduto.php');
require_once('app/model/mPedido.php');

class cHome extends appController {
	
	private $produto = null;
	private $pdv = null;
	private $pedido = null;
	
	private $setor;
	private $idSetor;
	private $idColaborador;
	
	private $limiteDesconto = 0; //200%
	
	private $maxPorPagina = 20;
	
	private $arrListaProduto;
	
	function cHome() {
		$this->produto = new mProduto();
		$this->pdv = new mPDV();	
		$this->pedido = new mPedido();
		
		$this->setor         = appFunction::dadoSessao('setor');
		$this->idSetor       = appFunction::dadoSessao('id_setor');
		$this->idColaborador = appFunction::dadoSessao('id_colaborador');
	}
	
	function main($pagina='', $filtro='', $x=1) {
		
		
		
		$pagina = ($pagina == '') ? 1 : $pagina;
		//echo $x;
		
		$resumo = $this->ResumoDadosPedido();
		
		if(appFunction::dadoSessao('nivel_admin') == 0) {
			//$this->set('tabelaPedido', $this->listarPedidos($filtro));
		} else {
			$numReg = $this->pedido->getTotalRegistros($filtro);
        	$this->set('paginacao', $this->paginacao($numReg, $pagina, $filtro));
			
		}
		
		

		$this->set('totalPedidos', $resumo['TOTAL']);
		$this->set('totalPedidosExportados', $resumo['RECEBIDO']);
		$this->set('totalPedidosPendentes', $resumo['PENDENTE']);
		$this->set('mediaPedidosDia', appFunction::formatarMoeda($resumo['MEDIA_DIA'],1));
		$this->set('tabelaPedido', $this->listarPedidos($filtro, $pagina));
		
		
		$this->render('vHome');
	}
	
	function limparListaProduto() {
		@session_start();
		$_SESSION['arrListaProd'] = array();
		//echo $this->criarTabelaProdutos();
	}
	
	public function paginacao($total, $pagina=0, $filtro='') {
        
        $pagina = ($pagina == 0) ? 1 : $pagina;
		
		

        $numPagina = ceil($total / $this->maxPorPagina);
        $inicio = ($this->maxPorPagina * $pagina) - $this->maxPorPagina;
        
        $linkPrimeiro = appConf::caminho.'home/main/1/'.$filtro;
        $linkUltimo = appConf::caminho.'home/main/'.$numPagina.'/'.$filtro;
        
        if($pagina != 1) {
            $linkAnterior = appConf::caminho.'home/main/'.($pagina-1).'/'.$filtro;
        }
        
        if($pagina < $numPagina) {
            $linkProximo = appConf::caminho.'home/main/'.($pagina+1).'/'.$filtro;
            
        }
        
        
        
        $paginacao[0] =  '<nav>
                <ul class="pager">
                    <li><a href="'.$linkPrimeiro.'">Primeiro</a></li>
                  <li><a href="'.$linkAnterior.'">Anterior</a></li>
                  <li><small>Pagina <strong>'.$pagina.'</strong> de <strong>'.$numPagina.'</strong></small></li>
                  <li><a href="'. $linkProximo.'">Próximo</a></li>
                      <li><a href="'.$linkUltimo.'">Ultimo</a></li>
                </ul>
              </nav>';
        
        $paginacao[1] = '<div class="btn-group" role="group" aria-label="First group">
                            <a href="'.$linkPrimeiro.'" type="button" class="btn btn-primary btn-sm">|<</a>
                            <a href="'.$linkAnterior.'" type="button" class="btn btn-default btn-sm"><</a>
                            <a href="'. $linkProximo.'" type="button" class="btn btn-default btn-sm">></a>
                            <a href="'.$linkUltimo.'" type="button" class="btn btn-default btn-sm">>|</a>
                          </div>';
        
        return $paginacao;
    }
	
	
	function novoPedido() {
		
		//$listaDeProduto = new mPedidoProduto();
		
		//$this->setArray($listaDeProduto);
		$this->set('comboCondicao', appFunction::comboCondicaoPagto());
		$this->set('comboDistribuidor', appFunction::comboDistribuidor());
		$this->set('comboPDV', $this->pdv->comboPDVSetor($this->setor));
		$this->set('comboProduto', $this->produto->comboProduto());
                $this->set('listaProduto', $this->produto->listar());
		
		$this->render('vFormularioPedido');
		
		@session_start();
		$_SESSION['arrListaProd'] = array();
		$this->arrListaProduto = array();
		
		
	}
	
        public function gerarListaPedido($icms=19) {
            $listaProduto = $this->produto->listar();
            $html = '
                <table id="tableProdutos" class="table table-bordaered table-striped table-condensed">
                <tr class="active">
                    <th class="text-center">Ind.</th>
                    
                    
                    
                    <th class="text-center">Produto</th>
                    <th class="text-center">&nbsp;</th>
                    <th class="text-center">Código</th>
                    <th class="text-left">Apresentação</th>
                    <th class="text-center">Qtd</th>
                    <th class="text-center">Desc. Conc.</th>
                    <th class="text-center">Desc. Máx</th>
                    <th class="text-center">R$ Fábrica</th>
                    <th class="text-center">R$ Consumidor</th>
                    <th class="text-center">Valor Total</th>
                </tr>';

            foreach($listaProduto as $i => $produto) {
                
                $hb20 = '&nbsp;';
                if($produto->getHB20() == 1) {
                    $hb20 = '<span class="label label-info">HB20</span>';
                }
                
             $html .=   '<tr  ind="'.($i+1).'" preco-fabb="'.$produto->getPrecoFabrica($icms).'">
                <td class="text-center"><input type="hidden" name="txtCodProduto[]" value="'.$produto->getCodigo().'" />'.($i+1).'</td>
                <td class="text-center">'.$produto->getProduto().'</td>
                    <td class="text-center">'.$hb20.'</td>
                <td class="text-center">'.$produto->getCodigo().'</td>
                <td class="text-left">'.$produto->getApresentacao().'</td>
                <td class="text-center"><input name="txtQtd[]" style="width: 50px;" class="form-control input-sm qtd number" type="text" desc="" preco-fab="'.$produto->getPrecoFabrica($icms).'"></td>
                <td class="text-center"><input name="txtDesconto[]" style="width: 50px;" class="form-control input-sm desc number" type="text" qtd="" value="" value-max="'.$produto->getDesconto().'" preco-fab="'.$produto->getPrecoFabrica($icms).'"></td>
                <td class="text-center">'.$produto->getDesconto().'</td>
                <td class="text-center">'.appFunction::formatarMoeda($produto->getPrecoFabrica($icms),2).'</td>
                <td class="text-center">'.appFunction::formatarMoeda($produto->getPrecoConsumidor($icms),2).'</td>
                <td class="text-center"><strong><span class="total-produto" id="total'.($i+1).'">0,00</span></strong></td>
            </tr>';

                 
             }
             $html .= '</table>';
             
             echo $html;
        }
        
        function formatarMoeda($valor) {
            echo appFunction::formatarMoeda($valor,2);
        }
        
	function visualizarPedido($numPedido) {
		$this->pedido->setIdPedido($numPedido);	
		$this->pedido->selecionar();
		$produtos = $this->pedido->produto->listar();
		
		@session_start();
		$_SESSION['arrListaProd'] = array();
		
		
		foreach($produtos as $produto) {
			array_push($_SESSION['arrListaProd'], $produto);
		}
		
		$tabela = $this->criarTabelaProdutos(1, $this->pedido->getICMS());
		
		$this->set('idPedido', $this->pedido->getIdPedido());
		$this->set('dataPedido', $this->pedido->getDataPedido());
		$this->set('nomePDV', $this->pedido->pdv->getCNPJ().' - '.$this->pedido->pdv->getFantasia());
		$this->set('nomeDistribuidor', $this->pedido->getDistribuidor());
		$this->set('pagamento', $this->pedido->getPagamento());
		$this->set('icms', $this->pedido->getICMS());
		$this->set('status', $this->pedido->getStatus());
		$this->set('tabelaProduto', $tabela['tabela']);
		$this->set('totalTabela', $tabela['totalGeral']);
		
		$this->render('vVisualizarPedido');
	}
	
	function ResumoDadosPedido() {
		return $this->pedido->resumoPedidos();
	}
	
	function listarPedidos($filtro='', $pagina=0) {
		
		
		if(appFunction::dadoSessao('nivel_admin') == 0) {
			$this->pedido->setIdSetor($this->idSetor);
			$pedidos = $this->pedido->listar();
		} else {
			
			$pagina = ($pagina == 0) ? 1 : $pagina;
			$inicio = ($this->maxPorPagina * $pagina) - $this->maxPorPagina + 1;
        	$max = $this->maxPorPagina * $pagina;
			
			$pedidos = $this->pedido->listarTudo($filtro, $inicio, $max);
			
			//echo '-'.$filtro;
			
			//echo '$this->pedido->listarTudo('.$filtro.', '.$inicio.', '.$max.')';
		}
		
		$this->set('totalPedido', count($pedidos));
		
		foreach($pedidos as $pedido) {
			
			
			$botao = $this->barraBotoes($pedido->getIdPedido());
			
			
			
			$tr .= '<tr>
						<td>'.str_pad($pedido->getIdPedido(), 5, "0", 0).'</td>
						<td>'.appFunction::formatarData($pedido->getDataPedido()).'</td>
						<td>'.$pedido->colaborador->getSetor().' '.$pedido->colaborador->getNome().'<p><small>'.$pedido->colaborador->getEmail().'</small></p></td>
						<td>'.$pedido->pdv->getCNPJ().' '.$pedido->pdv->getFantasia().'</td>
						<td>'.appFunction::formatarMoeda($pedido->getTotalPedido(),2).'</td>
						<td><h5 id="status'.$pedido->getIdPedido().'">'.$pedido->getStatus().'</h5></td>
						<td>
							'.$botao.'
							
						</td>
					</tr>';
		}
		$html = '<small>
					<table class="table table-striped table-condensed">
						<tr>
							<th>Cód. Pedido</th>
							<th>Data do Pedido</th>
							<th>Solicitante</th>
							<th>Nome do PDV</th>
							<th>Valor do Pedido</th>
							<th>Status</th>
							<th>Ações</th>
						</tr>
						'.$tr.'
					</table>
				</small>';
			
		return $html;
	}
	
	function barraBotoes($idPedido) {
		$botoes[0] = '<a class="btn btn-default btn-xs" href="'.appConf::caminho.'home/visualizarPedido/'.$idPedido.'" role="button">Visualizar</a> ';
		$botoes[1] = '<a class="btn btn-default btn-xs" href="#" data-cod="'.$idPedido.'" data-toggle="modal" data-target="#modalExcluirPedido" role="button">Excluir</a> ';
		$botoes[2] = '<a class="btn btn-info btn-xs exportar" href="#" data-cod="'.$idPedido.'"  role="button">Exportar</a>';
		
		//$botao = $botoes[0].$botoes[1].$botoes[2];
		
		if(appFunction::dadoSessao('nivel_admin') == 0) {
			
			$botoes[2] = '';
			
			$this->pedido->setIdPedido($idPedido);
			$this->pedido->selecionar();
			
			if($this->pedido->getIdStatus() == 1) {
				$botoes[1] = "";	
			}
			
		} else {
			$botoes[1] = '';	
		}
		
		
		$botao = '';
		for($i=0;$i<count($botoes);$i++) {
			$botao .= $botoes[$i];
		}
		
		return $botao;
		
	}
	
	function getStatus($codigo) {
		$this->pedido->setIdPedido($codigo);
		
		$this->pedido->setIdStatus(1);
		$this->pedido->atualizarStatus();
		
		
		$this->pedido->selecionar();
		
		echo  $this->pedido->getStatus();
	}
	
	function comboApresentacao() {
		$produto = $_POST['produto'];	
		
		echo $this->produto->comboApresentacao($produto);
	}
	
	function selecionarProduto() {
		$cod = $_POST['codigo'];
		$cnpj = $_POST['cnpj'];
		$icms = $_POST['icms'];
		
		$tituloMedia = 'Média PDV';
		
		$this->produto->setCodigo($cod);
		$this->produto->selecionar();
		
		$maxDesc = $this->produto->getDesconto();
		
		$media = $this->pdv->getMediaPDVProduto($this->setor, $cnpj, $cod);
		

		if($media == null) {
			$media = $this->produto->getMaxProdutoBR();
			$tituloMedia = 'Máx. BR';
			
			if($this->produto->getHB20() == 1) {
				$maxDesc = 5;
			} else {
				$maxDesc = $this->produto->getDesconto();
			}
		}
		
		$max = floor($media * $this->limiteDesconto + $media);
		
		$precoCons = ($this->produto->getPrecoConsumidor($icms) == null) ? null : appFunction::formatarMoeda($this->produto->getPrecoConsumidor($icms),2);
		
		$dados = array('descontoMax' => $maxDesc,
					   'precoFab' => appFunction::formatarMoeda($this->produto->getPrecoFabrica($icms),2),
					   'precoCons' => $precoCons,
					   'media' => $media,
					   'maxProd' => $max,
					   'tituloMedia' => $tituloMedia,
					   'hb20' => $this->produto->getHB20());
		
		echo json_encode($dados);
		
	}
	
	function calcularValorTotalProdutos() {
		//$valor = str_replace(",", ".", $_POST['valor']);
		//$desconto = $_POST['desconto'] / 100;
		//$qtd = $_POST['qtd'];
		//$descMax = $_POST[''];
			
		//$preco = ($valor - ($valor * $desconto)) * $qtd;
		
		//echo appFunction::formatarMoeda($preco,2);
		
		$erro = '';
		$erroNum = '';
		$avaliarQtd = 0;
		
		$qtd = $_POST['qtd'];
		$codigo = $_POST['codigo'];
		$desconto = $_POST['desconto'];
		$icms = $_POST['icms'];
		$PDV = $_POST['cnpj'];
		
		$this->produto->setCodigo($codigo);
		$this->produto->selecionar();
		$valor = $this->produto->getPrecoFabrica($icms);
		
		if($this->produto->getHB20() == 0) {
		
			if($desconto > $this->produto->getDesconto()) {
				$erroNum = 0;
				$erro = 'Desconto acima do permitido!';	
			}
		
		} else {
			$media = $this->pdv->getMediaPDVProduto($this->setor, $PDV, $codigo);
			
			if($media == null) {
				$media = $this->produto->getMaxProdutoBR();
				$maxDesc = 5;
			} else {
				$maxDesc = $this->calcularDesconto($media, $qtd, $codigo, 1);
			}
			
			$maxProd = floor($media * $this->limiteDesconto + $media);
	
			if($desconto > $maxDesc) {
				$erroNum = 0;
				$erro = 'Desconto acima do permitido!';
			} else {
			
				if($maxProd > 0) {
					if($qtd > $maxProd) {
						//$erroNum = 1;
						//$erro = 'Quantidade de produtos acima do permitido!';	
						$avaliarQtd = 1;
					}
				}
					
			}
		}
		
		$preco = appFunction::formatarMoeda(($valor - ($valor * ($desconto / 100))) * $qtd,2);
		
		$arr = array('erroNum' => $erroNum, 'erro' => $erro, 'desconto' => $preco, 'avaliarQtd' => $avaliarQtd);
		echo json_encode($arr);
		
	}
	
	function adicionarProduto() {
		$erro = "";
		
		$codigo = $_POST['cmbApresentacao'];	
		$demanda = $_POST['txtMediaDemanda'];	
		$qtd = $_POST['txtQtd'];	
		$descConc = $_POST['txtDescConc'];	
		$descMax = $_POST['txtDescMax'];
		$precoFab = $_POST['txtPrecoFab'];
		$precoCon = $_POST['txtPrecoCons'];
		$avaliarQtd = $_POST['txtAvaliarQtd'];
		$icms = $_POST['txtICMS'];
		
		if($qtd == 0  || $codigo == 0) {
			echo 'Os campos PRODUTO, APRESENTACAO, QTD são obrigatorios!';
			return false;	
		}
		
		if($descConc == "") {
			$descConc = 0;	
		}
		
		if($erro == "") {
			
			$precoCon = appFunction::formatarMoedaSQL($precoCon);
			
			$listaDeProduto = new mPedidoProduto();
			$listaDeProduto->setCodigoProduto($codigo);
			$listaDeProduto->setQuantidade($qtd);
			$listaDeProduto->setDescConcedido($descConc);
			$listaDeProduto->setMediaDemanda($demanda);
			$listaDeProduto->setPrecoFabrica(appFunction::formatarMoedaSQL($precoFab));
			$listaDeProduto->setPrecoConsumidor($precoCon);
			$listaDeProduto->setDescMaximo($descMax);
			$listaDeProduto->setAvaliarQtd($avaliarQtd);
			
			@session_start();
			array_push($_SESSION['arrListaProd'], $listaDeProduto);
			
			echo $this->criarTabelaProdutos(0,$icms);
		
		}
		
		
			
	}
	
	function removerProduto($codigo) {
		
		@session_start();
		$arr = $_SESSION['arrListaProd'];
		
		unset($arr[$codigo]);

		$_SESSION['arrListaProd'] = array_values($arr);
		
		echo $this->criarTabelaProdutos();
		//echo $codigo;
	}
	
	public function salvarPedido2() {
		
		$dataPedido = $_POST['txtDataPedido'];
		$cnpj = $_POST['cmbPDV'];
		$distribuidor = $_POST['cmbDistribuidor'];
		$condicao = $_POST['cmbCondicaoPagto'];
		$icms = $_POST['cmbICMS'];
		
		@session_start();
		
		
		$this->pedido->setIdSetor($this->idSetor);
		$this->pedido->setIdColaborador($this->idColaborador);
		$this->pedido->setDataPedido(appFunction::formatarData($dataPedido));
		$this->pedido->setCNPJ($cnpj);
		$this->pedido->setDistribuidor($distribuidor);
		$this->pedido->setPagamento($condicao);
		$this->pedido->setICMS($icms);
		$this->pedido->setProduto($_SESSION['arrListaProd']);
		
		$this->pedido->salvar();
		
		//$this->location(appConf::caminho);
	}
        
        public function salvarPedido() {
            $dataPedido = $_POST['txtDataPedido'];
            $cnpj = $_POST['cmbPDV'];
            $distribuidor = $_POST['cmbDistribuidor'];
            $condicao = $_POST['cmbCondicaoPagto'];
            
            $codProduto = $_POST['txtCodProduto'];
            $quantidade = $_POST['txtQtd'];
            $desconto = $_POST['txtDesconto'];
            
            $icms = $_POST['cmbICMS'];
            
            
            /*
            
            $dados = array('descontoMax' => $maxDesc,
                        'precoFab' => appFunction::formatarMoeda($this->produto->getPrecoFabrica($icms),2),
                        'precoCons' => $precoCons,
                        'media' => $media,
                        'maxProd' => $max,
                        'tituloMedia' => $tituloMedia,
                        'hb20' => $this->produto->getHB20());
            
            
            */
            
            $txt = '';
            $produtosPedido = array();
            
            foreach($codProduto as $i => $produto) {
                $prod = new mProduto();
                $prod->setCodigo($produto);
                $prod->selecionar();
                
                
                if($quantidade[$i] > 0 || $quantidade[$i] != '') {
                    $listaDeProduto = new mPedidoProduto();
                    $listaDeProduto->setCodigoProduto($prod->getCodigo());
                    $listaDeProduto->setQuantidade($quantidade[$i]);
                    $listaDeProduto->setDescConcedido($desconto[$i]);
                    $listaDeProduto->setMediaDemanda(0);
                    $listaDeProduto->setPrecoFabrica($prod->getPrecoFabrica($icms));
                    $listaDeProduto->setPrecoConsumidor($prod->getPrecoConsumidor($icms));
                    $listaDeProduto->setDescMaximo($prod->getDesconto());
                    $listaDeProduto->setAvaliarQtd(0);
                    
                     $produtosPedido[] =$listaDeProduto;
                    
                    $txt .= 'Produto: '.$produto.' - Qtd: '.$quantidade[$i].' - Desconto: '.$desconto[$i].'<br>';
                }
                
                
                
                
            }
            
		$this->pedido->setIdSetor($this->idSetor);
		$this->pedido->setIdColaborador($this->idColaborador);
		$this->pedido->setDataPedido(appFunction::formatarData($dataPedido));
		$this->pedido->setCNPJ($cnpj);
		$this->pedido->setDistribuidor($distribuidor);
		$this->pedido->setPagamento($condicao);
		$this->pedido->setICMS($icms);
		$this->pedido->setProduto($produtosPedido);
		
		$this->pedido->salvar();
            
            
            //print_r($produtosPedido);
            
            
        }
	
	public function excluirPedido($pedido) {
		$this->pedido->setIdPedido($pedido);
		$this->pedido->excluir();	
	}
	
	function mediaDemandaPDV() {
		$media = $this->pdv->getMediaPDVProduto($this->setor, $_POST['pdv'], $_POST['produto']);
		
		echo ($media == '') ? 0 : $media;
		//echo '$this->pdv->getMediaPDVProduto('.$this->setor.', '.$pdv.', '.$produto.');';
		
	}
	
	function listarTabelaProduto() {
			
	}
	
	private function criarTabelaProdutos($echo=0, $icms='') {
	
		$totalGeral = 0;
		@session_start();
		
		foreach($_SESSION['arrListaProd'] as $i => $listaProd) {
		
		$this->produto->setCodigo($listaProd->getCodigoProduto());
		$this->produto->selecionar();
		
		
		$btnExcuir[0] = '<td><a class="btn btn-danger btn-xs" href="#" role="button" data-cod="'.$i.'" data-toggle="modal" data-target="#modalExcluir"><span class="glyphicon glyphicon-remove"></span></a></td>';
		$btnExcuir[1] = '<td>&nbsp;</td>';
		
		if($echo == 1) {
			$btnExcuir[0] = '';
			$btnExcuir[1] = '';
		}
		
		
		$total = ($this->produto->getPrecoFabrica($icms) - ($this->produto->getPrecoFabrica($icms) * ( $listaProd->getDescConcedido() / 100))) * $listaProd->getQuantidade();
		
		$totalGeral = $totalGeral + $total;
		
		$clsHB20 = ($this->produto->getHB20() == 1) ? 'success' : '';
		
		
			$html .= '<tr class="'.$clsHB20.'">
							<td class="text-center">'.($i + 1).'</td>
                        	<td class="text-center">'.$listaProd->getCodigoProduto().'</td>
                        	<td class="text-left">'.$this->produto->getApresentacao().'</td>
							<td class="text-center">'.$listaProd->getMediaDemanda().'</td>
                            <td class="text-center">'.$listaProd->getQuantidade().'</td>
                            <td class="text-center">'.$listaProd->getDescConcedido().'%</td>
                            <td class="text-center">'.$this->produto->getDesconto().'%</td>
                            <td class="text-center">'.appFunction::formatarMoeda($this->produto->getPrecoFabrica($icms),2).'</td>
                            <td class="text-center">'.appFunction::formatarMoeda($this->produto->getPrecoConsumidor($icms),2).'</td>
                            <td class="text-center">'.appFunction::formatarMoeda($total,2).'</td>
                            '.$btnExcuir[0].'
                            
                        </tr>';
		
		}
		
		$tabela =  '<table id="tableProdutos" class="table table-bordaered table-strriped table-cosndensed">
                    	<tr class="active">
							<th class="text-center">Ind.</th>
                        	<th class="text-center">Código</th>
                        	<th class="text-left">Apresentação</th>
                            <th class="text-center">Média/Máx</th>
                            <th class="text-center">Qtd</th>
                            <th class="text-center">Desc. Conc.</th>
                            <th class="text-center">Desc. Máx.</th>
                            <th class="text-center">R$ Fábrica</th>
                            <th class="text-center">R$ Consumidor</th>
                            <th class="text-center">Valor Total</th>
                            '.$btnExcuir[1].'
                        </tr>
						'.$html.'
				</table>';
		
		
		$arr = array('tabela' => $tabela, 'totalGeral' => appFunction::formatarMoeda($totalGeral,2));
		
		if($echo == 0) {
			return json_encode($arr);
		} else {
			return $arr;
		}
			
	}
	
	function calcularDesconto($mediaDemanda=0, $qtd=0, $codProd, $echo=0) {
		
		$maxProd = 0;
		$descontoMax = 0;
		
		$this->produto->setCodigo($codProd);
		$this->produto->selecionar();
		
		if($this->produto->getHB20() == 1) {
			
			if($mediaDemanda == 0) {
				$mediaDemanda = $this->produto->getMaxProdutoBR();
				$maxProd = $mediaDemanda * $this->limiteDesconto + $mediaDemanda;
				$descontoMax = 5;
			} else {

				$fator = $qtd * (1 / $mediaDemanda) -1;
				
				switch($fator) {
				
					case ($fator < 0): {
						$descontoMax = 5;
						break;	
					}
								
					case ($fator > 0 && $fator <= 0.30) : {
						$descontoMax = 5;	
						break;
					}
					
					case ($fator >= 0.31 && $fator <= 0.50): {
						$descontoMax = 10;	
						break;
					}
					
					case ($fator >= 0.51 && $fator <= 0.99): {
						$descontoMax = 15;
						break;	
					}
					
					case ($fator >= 1): {
						$descontoMax = 20;
						break;	
					}
					
				}
			
			}

		} else {
			
			$descontoMax = $this->produto->getDesconto();
			
		}
		
		$arr = array('desconto' => $descontoMax,
					 'limite' => $maxProd);
		
		if($echo == 0) {
		
		echo json_encode($arr);		 
		
		} else {
			return $descontoMax;
		}
		
	}
	
	
	public function exportarPedido() {

		$idPedido = $_POST['idPedido'];
		
		
		$this->pedido->setIdPedido($idPedido);
		$this->pedido->selecionar();
		
		
		
		$dados = $this->pedido->exportarPedido();
		
		$colunas = array_keys($dados[1]);
		
		/*for($i=0;$i<count($colunas);$i++) {
			
			$colunas[$i] = '"'.$colunas[$i].'"';
			
		}
		
		
		
		for($x=1;$x<=count($dados);$x++) {
			for($i=0;$i<count($colunas);$i++) {
				
				$dados[$x][$colunas[$i]] = '"'.$dados[$x][$colunas[$i]].'"';
				
			}
		}*/
		
		$nomeArquivo = strtolower('pedido_'.str_pad($idPedido, 5, '0', 0)).'.csv';

	   	$df = fopen(appConf::caminhoArquivo.'temp\\'.$nomeArquivo, 'w');
		
	   	fputcsv($df, $colunas, ';');
	   	foreach ($dados as $row) {
		  fputcsv($df, $row, ';');
	   	}
	   	fclose($df);
		
	   	header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="'.$nomeArquivo.'"');
		header('Content-Type: application/octet-stream');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize(appConf::caminhoArquivo.'temp\\'.$nomeArquivo));
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Expires: 0');
		
		// Envia o arquivo para o cliente
		readfile(appConf::caminhoArquivo.'temp\\'.$nomeArquivo);
		
		unlink(appConf::caminhoArquivo.'temp\\'.$nomeArquivo);
		
		
		
		//sleep(10);
		
		//$this->location(appConf::caminho);
		
		
	}
	
}

?>