<?php

require_once('lib/appConexao.php');
require_once('lib/appFunction.php');

class mColaborador extends appConexao {
	
	private $id;
	private $idColaborador;
	private $matricula;
	private $departamento;
	private $funcao;
	private $nome;
	private $endereco;
	private $numero;
	private $complemento;
	private $bairro;
	private $uf;
	private $cidade;
	private $cep;
	private $dddFoneRes;
	private $foneRes;
	private $dddCelCorp;
	private $celCorp;
	private $dddCelPess;
	private $celPess;
	private $email;
	private $aeroporto;
	private $dataNasc;
	private $cpf;
	private $rg;
	private $estadoCivil;
	private $camiseta;
	private $sapato;
	private $fumante;
	private $carro;
	private $placa;
	private $modeloIpad;
	private $conexao;
	private $registro;
	private $ativo;
	private $numAtivo;
	
	private $setor;
	private $nomeSetor;
	private $dataInicio;
	private $funcaoSetor;
	private $linha;
	
	private $enderecoA;
	private $numeroA;
	private $complementoA;
	private $bairroA;
	private $ufA;
	private $cidadeA;
	private $cepA;
	
	private $nomeDepartamento;
	
	private $fotoColaborador;
	
	function mColaborador() {
		$this->id = 0;
		$this->idColaborador = '';
		$this->matricula = 0;
		$this->departamento = '';
		$this->funcao = '';
		$this->nome = '';
		$this->endereco = '';
		$this->numero = '';
		$this->complemento = '';
		$this->bairro = '';
		$this->uf = '';
		$this->cidade = '';
		$this->cep = '';
		$this->dddFoneRes = '';
		$this->foneRes = '';
		$this->dddCelCorp = '';
		$this->celCorp = '';
		$this->dddCelPess = '';
		$this->celPess = '';
		$this->email = '';
		$this->aeroporto = '';
		$this->dataNasc = '';
		$this->cpf = '';
		$this->rg = '';
		$this->estadoCivil = '';
		$this->camiseta = '';
		$this->sapato = '';
		$this->fumante = '';
		$this->carro = '';
		$this->placa = '';
		
		$this->modeloIpad = '';
		$this->conexao = '';
		$this->registro = '';
		$this->ativo = '';
		$this->numAtivo = '';
	}
	
	public function setId($valor) {
		$this->id = $valor;	
	}
	
	public function setIdColaborador($valor) {
		$this->idColaborador = $valor;
	}
	
	public function setMatricula($valor) {
		$this->matricula = $valor;
	}
	
	public function setDepartamento($valor) {
		$this->departamento = $valor;
	}
	
	public function setFuncao($valor) {
		$this->funcao = $valor;
	}
	
	public function setNome($valor) {
		$this->nome = $valor;
	}
	
	public function setEndereco($valor) {
		$this->endereco = $valor;
	}
	
	public function setNumero($valor) {
		$this->numero = $valor;	
	}
	
	public function setComplemento($valor) {
		$this->complemento = $valor;
	}
	
	public function setBairro($valor) {
		$this->bairro = $valor;
	}
	
	public function setUf($valor) {
		$this->uf = $valor;	
	}
	
	public function setCidade($valor) {
		$this->cidade = $valor;	
	}
	
	public function setCep($valor) {
		$this->cep = $valor;	
	}
	
	public function setDDDFoneRes($valor) {
		$this->dddFoneRes = $valor;	
	}
	
	public function setFoneRes($valor) {
		$this->foneRes = $valor;	
	}
	
	public function setDDDCelCorp($valor) {
		$this->dddCelCorp = $valor;	
	}
	
	public function setCelCorp($valor) {
		$this->celCorp = $valor;	
	}
	
	public function setDDDCelPess($valor) {
		$this->dddCelPess = $valor;	
	}
	
	public function setCelPess($valor) {
		$this->celPess = $valor;	
	}

	public function setEmail($valor) {
		$this->email = $valor;
	}
	
	public function setAeroporto($valor) {
		$this->aeroporto = $valor;	
	}
	
	public function setDataNasc($valor) {
		$this->dataNasc = $valor;
	}	
	
	public function setCPF($valor) {
		$this->cpf = $valor;
	}
	
	public function setRG($valor) {
		$this->rg = $valor;	
	}
	
	public function setEstadoCivil($valor) {
		$this->estadoCivil = $valor;
	}
	
	public function setCamiseta($valor) {
		$this->camiseta = $valor;
	}
	
	public function setSapato($valor) {
		$this->sapato = $valor;
	}
	
	public function setFumante($valor) {
		$this->fumante = $valor;
	}
	
	public function setCarro($valor) {
		$this->carro = $valor;
	}
	
	public function setPlaca($valor) {
		$this->placa = $valor;
	}
	
	public function setModeloIpad($valor) {
		$this->modeloIpad = $valor;
	}
	
	public function setConexao($valor) {
		$this->conexao = $valor;
	}
		
	public function setRegistro($valor) {
		$this->registro = $valor;
	}
	
	public function setAtivo($valor) {
		$this->ativo = $valor;
	}
	
	public function setNumAtivo($valor) {
		$this->numAtivo = $valor;
	}
	
	public function setFotoColaborador($valor) {
		$this->fotoColaborador = $valor;
	}	
	
	
	
	
	public function getId() {
		return $this->id;	
	}
	
	public function getIdColaborador() {
		return $this->idColaborador;
	}
	
	public function getMatricula() {
		return $this->matricula;
	}
	
	public function getDepartamento() {
		return $this->departamento;
	}
	
	public function getNomeDepartamento() {
		return $this->NomeDepartamento;
	}
	
	public function getFuncao() {
		return $this->funcao;
	}
	
	public function getNome() {
		return $this->nome;
	}
	
	public function getEndereco() {
		return $this->endereco;
	}
	
	public function getNumero() {
		return $this->numero;	
	}
	
	public function getComplemento() {
		return $this->complemento;
	}
	
	public function getBairro() {
		return $this->bairro;
	}
	
	public function getUf() {
		return $this->uf;	
	}
	
	public function getCidade() {
		return $this->cidade;	
	}
	
	public function getCep() {
		return $this->cep;	
	}
	
	public function getDDDFoneRes() {
		return $this->dddFoneRes;	
	}
	
	public function getFoneRes() {
		return $this->foneRes;	
	}
	
	public function getDDDCelCorp() {
		return $this->dddCelCorp;	
	}
	
	public function getCelCorp() {
		return $this->celCorp;	
	}
	
	public function getDDDCelPess() {
		return $this->dddCelPess;	
	}
	
	public function getCelPess() {
		return $this->celPess;	
	}

	public function getEmail() {
		return $this->email;
	}
	
	public function getAeroporto() {
		return $this->aeroporto;	
	}
	
	public function getDataNasc() {
		return $this->dataNasc;
	}	
	
	public function getCPF() {
		return $this->cpf;
	}
	
	public function getRG() {
		return $this->rg;	
	}
	
	public function getEstadoCivil() {
		return $this->estadoCivil;
	}
	
	public function getCamiseta() {
		return $this->camiseta;
	}
	
	public function getSapato() {
		return $this->sapato;
	}
	
	public function getFumante() {
		return $this->fumante;
	}
	
	public function getCarro() {
		return $this->carro;
	}
	
	public function getPlaca() {
		return $this->placa;
	}
	
	public function getModeloIpad() {
		return $this->modeloIpad;
	}
	
	public function getConexao() {
		return $this->conexao;
	}
		
	public function getRegistro() {
		return $this->registro;
	}
	
	public function getAtivo() {
		return $this->ativo;
	}
	
	public function getNumAtivo() {
		return $this->numAtivo;
	}
	
	
	
	public function getSetor() {
		return $this->setor;
	}	
	
	public function getNomeSetor() {
		return $this->nomeSetor;	
	}
	
	public function getLinha() {
		return $this->linha;	
	}
	
	public function getFuncaoSetor() {
		return $this->funcaoSetor;
	}	
	
	public function getDataInicio() {
		return appFunction::formatarData($this->dataInicio);
	}	
	
	
	
	
	public function getEnderecoA() {
		return $this->enderecoA;
	}
	
	public function getNumeroA() {
		return $this->numeroA;	
	}
	
	public function getComplementoA() {
		return $this->complementoA;
	}
	
	public function getBairroA() {
		return $this->bairroA;
	}
	
	public function getUfA() {
		return $this->ufA;	
	}
	
	public function getCidadeA() {
		return $this->cidadeA;	
	}
	
	public function getCepA() {
		return $this->cepA;	
	}
	
	public function getFotoColaborador() {
		return $this->fotoColaborador;
	}
	
	private function validarCampos() {
		if($this->idColaborador == '' || $this->nome == "" || $this->funcao == "" || $this->departamento == '' || $this->email == '') {
			return false;
		} else {
			return true;
		}	
	}
	
	public function salvar() {
				
		if($this->validarCampos() == true) {
		
			$query = "EXEC proc_salvarColaborador
						  '".$this->id."', 
						  '".$this->idColaborador."',
						  '".$this->matricula."',
						  '".$this->departamento."',
						  '".utf8_decode($this->funcao)."',
						  '".utf8_decode($this->nome)."',
						  '".utf8_decode($this->endereco)."',
						  '".$this->numero."',
						  '".utf8_decode($this->complemento)."',
						  '".utf8_decode($this->bairro)."',
						  '".$this->uf."',
						  '".$this->cidade."',
						  '".$this->cep."',
						  '".$this->dddFoneRes."',
						  '".$this->foneRes."',
						  '".$this->dddCelCorp."',
						  '".$this->celCorp."',
						  '".$this->dddCelPess."',
						  '".$this->celPess."',
						  '".strtolower($this->email)."',
						  '".$this->aeroporto."',
						  '".$this->dataNasc."',
						  '".$this->cpf."',
						  '".$this->rg."',
						  '".$this->estadoCivil."',
						  '".$this->camiseta."',
						  '".$this->sapato."',
						  '".$this->fumante."',
						  '".$this->carro."',
						  '".$this->placa."',
						  '".$this->modeloIpad."',
						  '".$this->conexao."',
						  '".$this->registro."',
						  '".$this->ativo."',
						  '".$this->numAtivo."'";
			
	
			$rs = $this->executarQueryArray($query);
			return $rs[1]['MSG'];
		
		} else {
			return 'Os campos "ID", "Nome", "Departamento", "Função" e "Email" são obrigatórios!';	
		}
	}
	
	public function excluir() {
		$this->executarQueryArray("EXEC proc_excluirColaborador '".$this->id."'");
	}
	
	public function exportar($tipo=0) {
		return $this->executarQueryArray("EXEC proc_exportarColaborador '".appFunction::dadoSessao('id_setor')."', '', '', '".$tipo."'");
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT * FROM vw_colaborador WHERE ID_COLABORADOR = ".$this->id);
		
		
		
		if(count($rs) > 0) {
		
		//$rsPerfil = $this->executarQueryArray("SELECT PERFIL FROM PERFIL WHERE ID_PERFIL = ".$rs[1]['ID_PERFIL']);
		$rsAmostra = $this->executarQueryArray("SELECT * FROM ENDERECO_AMOSTRA WHERE ID_COLABORADOR = ".$this->id);
		$rsFoto = $this->executarQueryArray("SELECT FOTO FROM FOTO_PERFIL WHERE ID_COLABORADOR = ".$this->id);
		$rsSetor = $this->executarQueryArray("SELECT
													SETOR,
													NOME_SETOR,
													LINHA,
													DT_INICIO,
													PERFIL
												FROM 
													vw_colaboradorSetor
												WHERE ID_COLABORADOR = ".$this->id);
		
		
		$this->idColaborador = $rs[1]['ID'];
		$this->matricula = $rs[1]['MATRICULA'];
		$this->departamento = $rs[1]['ID_PERFIL'];
		$this->funcao = $rs[1]['FUNCAO'];
		$this->nome = $rs[1]['NOME'];
		$this->endereco = $rs[1]['ENDERECO'];
		$this->numero  = $rs[1]['NRO'];
		$this->complemento = $rs[1]['COMPLEMENTO'];
		$this->cep = $rs[1]['CEP'];
		$this->uf = $rs[1]['UF'];
		$this->cidade = $rs[1]['CIDADE'];
		$this->bairro = $rs[1]['BAIRRO'];
		$this->email = $rs[1]['EMAIL'];
		$this->aeroporto = $rs[1]['AEROPORTO'];
		$this->dataNasc = $rs[1]['NASCIMENTO'];
		$this->cpf = $rs[1]['CPF'];
		$this->rg = $rs[1]['RG'];
		$this->estadoCivil = $rs[1]['ESTADO_CIVIL'];
		$this->camiseta = $rs[1]['CAMISETA'];
		$this->sapato = $rs[1]['SAPATO'];
		$this->fumante = $rs[1]['FUMANTE'];
		$this->carro = $rs[1]['CARRO'];
		$this->placa = $rs[1]['PLACA'];
		$this->modeloIpad = $rs[1]['MODELO'];
		$this->conexao = $rs[1]['CONEXAO'];
		$this->registro = $rs[1]['REGISTRO'];
		$this->ativo = $rs[1]['ATIVO'];
		$this->numAtivo = $rs[1]['ATIVO'];
		
		//dados dos telefones
		$this->dddCelCorp = $rs[1]['DDD_CORPORATIVO'];
		$this->dddCelPess = $rs[1]['DDD_PARTICULAR'];
		$this->dddFoneRes = $rs[1]['DDD_RESIDENCIAL'];
		$this->celCorp = $rs[1]['F_CORPORATIVO'];
		$this->celPess = $rs[1]['F_PARTICULAR'];
		$this->foneRes = $rs[1]['F_RESIDENCIAL'];
		
		//dados do setor
		$this->setor = $rsSetor[1]['SETOR'];
		$this->dataInicio = $rsSetor[1]['DT_INICIO'];
		$this->funcaoSetor = $rsSetor[1]['PERFIL'];
		$this->nomeSetor = $rsSetor[1]['NOME_SETOR'];
		$this->linha = $rsSetor[1]['LINHA'];
		
		//dados endereco amostra
		$this->enderecoA = $rsAmostra[1]['ENDERECO'];
		$this->complementoA = $rsAmostra[1]['COMPLEMENTO'];
		$this->bairroA = $rsAmostra[1]['BAIRRO'];
		$this->cidadeA = $rsAmostra[1]['CIDADE'];
		$this->numeroA  = $rsAmostra[1]['NRO'];
		$this->cepA  = $rsAmostra[1]['CEP'];
		
		//dados foto
		$this->fotoColaborador = $rsFoto[1]['FOTO'];
		
		//dado perfil
		$this->nomeDepartamento = $rsPerfil[1]['PERFIL'];
		
		}
	}
	
	
	
}