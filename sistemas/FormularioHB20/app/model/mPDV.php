<?php 

require_once('lib/appConexao.php');

class mPDV extends 	appConexao {
	
	private $cnpj;
	private $fantasia;
	
	
	function mPDV () {
		$this->cnpj = '';
		$this->fantasia = '';	
	}
	
	public function setCNPJ($valor) {
		$this->cnpj = str_pad($valor, 14, "0", STR_PAD_LEFT);
	}
	
	public function getCNPJ() {
		return $this->cnpj;
	}
	
	public function getFantasia() {
		return $this->fantasia;	
	}
	
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT CNPJ_PDV, PDV FROM PDV_CNPJ WHERE CNPJ_PDV = '". $this->cnpj ."'");	
		
		//$this->cnpj = str_pad($rs[1]['CNPJ_PDV'], 14, "0", STR_PAD_LEFT);
		$this->fantasia = $rs[1]['PDV'];
	}
	
	public function comboPDVSetor($setor, $selecionar='', $nomeCombo='cmbPDV') {
		$option = '<option></option>';
		
		$rs = $this->executarQueryArray("SELECT CNPJ_PDV, PDV FROM PDV_CNPJ WHERE SETOR = '".$setor."' GROUP BY CNPJ_PDV, PDV ORDER BY PDV ASC");	
		
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.str_pad($rs[$i]['CNPJ_PDV'], 14, "0", STR_PAD_LEFT).'">'.$rs[$i]['CNPJ_PDV']. ' - '. $rs[$i]['PDV'].'</option>';	
		}
		
		return '<select id="'.$nomeCombo.'" name="'.$nomeCombo.'" class="form-control input-sm">'.$option.'</select>';
	}	
	
	public function getMediaPDVProduto($setor, $cnpj, $codProd) {
		
		$rs = $this->executarQueryArray("SELECT MEDIA FROM PDV_CNPJ WHERE SETOR = '".$setor."' AND CNPJ_PDV = '".$cnpj."' AND SAP = '".$codProd."'");
		return $rs[1]['MEDIA'];
		//return "SELECT MEDIA FROM PDV_CNPJ WHERE SETOR = '".$setor."' AND CNPJ_PDV = '".$cnpj."' AND SAP = '".$codProd."'";
		
	}
}