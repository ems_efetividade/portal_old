<?php 


require_once('lib/appConexao.php');

require_once('app/model/mPedidoProduto.php');
require_once('app/model/mPDV.php');
require_once('app/model/mColaborador.php');


class mPedido extends appConexao {
	
	private $idPedido;
	private $dataPedido;
	private $cnpj;
	private $idDistribuidor;
	private $distribuidor;
	private $pagamento;
	private $icms;
	private $totalPedido;
	private $idSetor;
	private $idColaborador;
	private $status;
	private $idStatus;

	
	public $produto;
	public $pdv;
	public $colaborador;
	
	
	public function mPedido() {
		$this->idPedido = 0;
		$this->dataPedido = '';
		$this->cnpj = '';
		$this->idDistribuidor = 0;
		$this->pagamento = '';
		$this->icms = 0;	
		$this->status = 0;
		$this->idColaborador = 0;
		
		$this->pdv = new mPDV();
		$this->produto = new mPedidoProduto();
		$this->colaborador = new mColaborador();
		
	}
	

	
	
	public function setIdPedido($valor) {
		$this->idPedido	= $valor;
	}
	
	public function setIdSetor($valor) {
		$this->idSetor	= $valor;
	}
	
	public function setIdColaborador($valor) {
		$this->idColaborador = $valor;
	}
	
	public function setDataPedido($valor) {
		$this->dataPedido	= $valor;
	}
	
	public function setCNPJ($valor) {
		$this->cnpj	= str_pad($valor, 14, "0", STR_PAD_LEFT);
	}
	
	public function setIdDistribuidor($valor) {
		$this->idDistribuidor	= $valor;
	}
	
	public function setDistribuidor($valor) {
		$this->distribuidor	= $valor;
	}
	
	public function setPagamento($valor) {
		$this->pagamento	= $valor;
	}
	
	public function setICMS($valor) {
		$this->icms	= $valor;
	}
	
	public function setProduto($arrProduto) {
		$this->produto = $arrProduto;
	}
	
	public function setIdStatus($valor) {
		$this->idStatus = $valor;
	}
	
	
	public function getIdPedido() {
		return $this->idPedido;
	}
	
	public function getDataPedido() {
		return $this->dataPedido;
	}
	
	public function getCNPJ() {
		return $this->cnpj;
	}
	
	public function getIdDistribuidor() {
		return $this->idDistribuidor;
	}
	
	public function getDistribuidor() {
		return $this->distribuidor;
	}
	
	public function getPagamento() {
		return $this->pagamento;
	}
	
	public function getICMS() {
		return $this->icms;
	}
	
	public function getTotalPedido() {
		return $this->totalPedido;	
	}
	
	public function getIdStatus() {
		return $this->idStatus;
	}
	public function getStatus() {
		$status = '';
		
		switch($this->status) {
		
			case 0: {
				$status = '<span class="label label-danger">Pendente</span>';
				break;
			}
			
			case 1: {
				$status = '<span class="label label-success">Recebido</span>';
				break;
			}			
			
		}
		
		return $status;
		
	}

	
	public function getProduto() {
		//return $this->produto;	
	}
	
	
	private function validar() {
				
		if($this->cnpj == "00000000000000") {
			return 'Selecione o PDV.';
			return false;
		}
		
		if($this->distribuidor == "") {
			return 'Selecione o distribuidor.';
			return false;	
		}
		
		if($this->pagamento == "") {
			return 'Selecione a condição do Pagamento.';
			return false;	
		}
		
		if($this->icms == "") {
			return 'Selecione o ICMS.';
			return false;	
		}
		
		if(count($this->produto) == 0) {
			return 'Adicione ao menos um produto na lista.';
			return false;	
		}
		
	}
	
	public function salvar() {
		$queryPedido = "INSERT INTO [PDV_PEDIDO]
					   ([ID_DISTRIBUIDOR]
					   ,[DATA_PEDIDO]
					   ,[CNPJ]
					   ,[PAGAMENTO]
					   ,[ICMS]
					   ,[ID_SETOR]
					   ,[STATUS]
					   ,[ID_COLABORADOR]
					   ,[DATA_CADASTRO]
					   ,[DISTRIBUIDOR])
				 VALUES
					   (".$this->idDistribuidor."
					   ,'".$this->dataPedido."'
					   ,".$this->cnpj."
					   ,".$this->pagamento."
					   ,".$this->icms."
					   ,".$this->idSetor."
					   ,0
					   ,".$this->idColaborador."
					   ,'".date('Y-m-d H:i:s')."'
					   ,'".$this->distribuidor."'); SELECT @@IDENTITY AS ID_PEDIDO";

                //echo $queryPedido;
                
		$validar = $this->validar();

		if($validar != "") {
			echo $validar;
			return false;	
		}

                $rs = $this->executarQueryArray($queryPedido);
		if($rs) {
		
			//$rs = $this->executarQueryArray("SELECT @@IDENTITY AS ID_PEDIDO");

			foreach($this->produto as $produto) {
				
				$qy .= 'INSERT INTO [PDV_PEDIDO_PRODUTO]
                                                    ([ID_PEDIDO]
                                                    ,[CODIGO]
                                                    ,[MEDIA]
                                                    ,[QTD]
                                                    ,[DESCONTO]
                                                    ,[DESCONTO_MAX]
                                                    ,[PRECO_FABRICA]
                                                    ,[PRECO_CONSUMIDOR]
                                                    ,[AVALIAR_QTD])
                                          VALUES
                                                    ('.$rs[1]['ID_PEDIDO'].'
                                                    ,'.$produto->getCodigoProduto().'
                                                    ,'.$produto->getMediaDemanda().'
                                                    ,'.$produto->getQuantidade().'
                                                    ,'.$produto->getDescConcedido().'
                                                    ,'.$produto->getDescMaximo().'
                                                    ,'.$produto->getPrecoFabrica().'
                                                    ,'.$produto->getPrecoConsumidor().'
                                                    ,'.$produto->getAvaliarQtd().')';
				
			}
			
			if(!$this->executar($qy)) {
                            $this->executar("DELETE FROM PDV_PEDIDO WHERE ID_PEDIDO = ".$rs[1]['ID_PEDIDO']);
                            echo 'houve um erro' .odbc_errormsg($this->cn).$qy;	
			}
		} else {
			echo 'houve um erro' .odbc_errormsg($this->cn);	
		}

	}
	
	public function atualizarStatus() {
		$this->executar("UPDATE PDV_PEDIDO SET STATUS = ".$this->idStatus." WHERE ID_PEDIDO = ".$this->idPedido."");	
	}
	
	public function selecionar() {
		$query = 'WITH SOMA_PRODUTO AS (
					SELECT
						ID_PEDIDO,
						QTD,
						PRECO_FABRICA,
						DESCONTO,
						(QTD * PRECO_FABRICA) AS TOTAL,
						(QTD * PRECO_FABRICA) - (QTD * PRECO_FABRICA) * (DESCONTO / CONVERT(FLOAT ,100)) AS TOTAL_DESCONTO
					FROM PDV_PEDIDO_PRODUTO WHERE ID_PEDIDO = '.$this->idPedido.'
				)
				
				
				SELECT
					P.ID_PEDIDO,
					P.ID_DISTRIBUIDOR,
					P.DATA_PEDIDO,
					P.CNPJ,
					P.PAGAMENTO,
					P.ICMS,
					P.STATUS,
					P.ID_COLABORADOR,
					P.DISTRIBUIDOR,
					SUM(I.TOTAL_DESCONTO) AS TOTAL
				FROM
					PDV_PEDIDO P
				INNER JOIN 
					SOMA_PRODUTO I ON I.ID_PEDIDO = P.ID_PEDIDO
				
				GROUP BY
					P.ID_PEDIDO,
					P.ID_DISTRIBUIDOR,
					P.DATA_PEDIDO,
					P.CNPJ,
					P.PAGAMENTO,
					P.ICMS,
					P.STATUS,
					P.ID_COLABORADOR,
					P.DISTRIBUIDOR';	
		
		$rs = $this->executarQueryArray($query);
		
		
		$this->idPedido = $rs[1]['ID_PEDIDO'];
		$this->dataPedido = $rs[1]['DATA_PEDIDO'];
		$this->cnpj = $rs[1]['CNPJ'];
		$this->idDistribuidor = $rs[1]['ID_DISTRIBUIDOR'];
		$this->pagamento = $rs[1]['PAGAMENTO'];
		$this->icms = $rs[1]['ICMS'];
		$this->totalPedido = $rs[1]['TOTAL'];
		$this->idSetor = $rs[1]['ID_SETOR'];
		$this->status = $rs[1]['STATUS'];
		$this->idStatus = $rs[1]['STATUS'];
		$this->distribuidor = $rs[1]['DISTRIBUIDOR'];
		
		
		$this->pdv->setCNPJ($this->cnpj);
		$this->pdv->selecionar();
		
		$this->produto->setIdPedido($this->idPedido);
		
		
		$this->colaborador->setId($rs[1]['ID_COLABORADOR']);
		$this->colaborador->selecionar();

	}
	
	public function excluir() {
		$this->executar("DELETE FROM PDV_PEDIDO WHERE ID_PEDIDO = ".$this->idPedido);	
		$this->executar("DELETE FROM PDV_PEDIDO_PRODUTO WHERE ID_PEDIDO = ".$this->idPedido);	
	}
	
	public function listar() {
		
		$arr = array();
		$rs = $this->executarQueryArray("SELECT ID_PEDIDO FROM PDV_PEDIDO WHERE ID_SETOR = ".$this->idSetor."");
		
		for($i=1;$i<=count($rs);$i++) {
			
			$ped = new mPedido();
			$ped->setIdPedido($rs[$i]['ID_PEDIDO']);
			$ped->selecionar();
			
			$arr[] = $ped;
		}
		
		return $arr;
	}
	
	public function getTotalRegistros($filtro='') {
		
		if($filtro == '') {
			$query = "SELECT count(*) as TOTAL FROM PDV_PEDIDO";	
		} else {
			$query = "SELECT count(*) as TOTAL FROM PDV_PEDIDO WHERE STATUS = ".$filtro."";
		}
		
        $rs = $this->executarQueryArray($query);
        return $rs[1]['TOTAL'];
    }
	
	public function listarTudo($filtro='', $min=1, $max=500) {
		
		$maxPagina = 10;
		
		$arr = array();
		
		
		
		if($filtro == "") {
			//$query = "SELECT ID_PEDIDO FROM PDV_PEDIDO ORDER BY DATA_PEDIDO DESC, ID_PEDIDO DESC";
			$query = "SELECT * FROM 
						(SELECT ROW_NUMBER() OVER(ORDER BY DATA_CADASTRO DESC) AS INDICE, ID_PEDIDO FROM PDV_PEDIDO) X
					WHERE INDICE BETWEEN ".$min." AND ".$max."";
			
			
			
		} else {
			//$query = "SELECT ID_PEDIDO FROM PDV_PEDIDO WHErE STATUS = ".$filtro." ORDER BY DATA_PEDIDO DESC, ID_PEDIDO DESC";	
			$query = "SELECT * FROM 
						(SELECT ROW_NUMBER() OVER(ORDER BY DATA_CADASTRO DESC) AS INDICE, ID_PEDIDO FROM PDV_PEDIDO WHERE STATUS = ".$filtro.") X
					WHERE INDICE BETWEEN ".$min." AND ".$max."";
		}
		
		$rs = $this->executarQueryArray($query);
		
		for($i=1;$i<=count($rs);$i++) {
			
			$ped = new mPedido();
			$ped->setIdPedido($rs[$i]['ID_PEDIDO']);
			$ped->selecionar();
			
			$arr[] = $ped;
		}
		
		//echo $query;
		return $arr;
	}
	

	public function exportarPedido() {
		$qy = "SELECT 
					SETOR.SETOR + ' ' +COLABORADOR.NOME AS 'NOME',
					COLABORADOR.EMAIL, 
					PDV_PEDIDO.CNPJ, 
					PDV_PEDIDO.PAGAMENTO, 
					PDV_PEDIDO_PRODUTO.CODIGO, 
					PDV_PEDIDO_PRODUTO.QTD, 
					PDV_PEDIDO_PRODUTO.DESCONTO, 
					PDV_PEDIDO.DISTRIBUIDOR,
					CASE WHEN PDV_PEDIDO_PRODUTO.AVALIAR_QTD = 0 THEN '' ELSE '*' END AS AVALIAR
				FROM         
					PDV_PEDIDO 
				INNER JOIN 
					PDV_PEDIDO_PRODUTO ON PDV_PEDIDO.ID_PEDIDO = PDV_PEDIDO_PRODUTO.ID_PEDIDO 
				INNER JOIN 
					COLABORADOR ON PDV_PEDIDO.ID_COLABORADOR = COLABORADOR.ID_COLABORADOR 
				INNER JOIN 
					SETOR ON PDV_PEDIDO.ID_SETOR = SETOR.ID_SETOR 
				WHERE
					PDV_PEDIDO.ID_PEDIDO = ".$this->idPedido."
				GROUP BY 
					SETOR.SETOR, 
					COLABORADOR.NOME, 
					PDV_PEDIDO.CNPJ, 
					PDV_PEDIDO.PAGAMENTO, 
					PDV_PEDIDO_PRODUTO.CODIGO, 
					PDV_PEDIDO_PRODUTO.QTD, 
					PDV_PEDIDO_PRODUTO.DESCONTO, 
					PDV_PEDIDO.DISTRIBUIDOR, 
					COLABORADOR.EMAIL,
					PDV_PEDIDO_PRODUTO.AVALIAR_QTD";	
		
		return $this->executarQueryArray($qy);
	}
	
	public function resumoPedidos() {
		$arr = array();
		
		$rsTotal = $this->executarQueryArray("SELECT count(*) AS TOTAL FROM PDV_PEDIDO");
		$rsRecebido = $this->executarQueryArray("SELECT count(*) AS RECEBIDO FROM PDV_PEDIDO WHERE STATUS = 1");
		$rsPendente = $this->executarQueryArray("SELECT count(*) AS PENDENTE FROM PDV_PEDIDO WHERE STATUS = 0");
		$rsDias = $this->executarQueryArray("SELECT COUNT(D.DIA) AS DIAS FROM (

												SELECT
													DATA_PEDIDO as DIA
												FROM 
													PDV_PEDIDO
												GROUP BY
													DATA_PEDIDO
												
											) D");
													
		$arr['TOTAL'] = $rsTotal[1]['TOTAL'];
		$arr['RECEBIDO'] = $rsRecebido[1]['RECEBIDO'];
		$arr['PENDENTE'] = $rsPendente[1]['PENDENTE'];
		$arr['MEDIA_DIA'] = $rsTotal[1]['TOTAL'] / $rsDias[1]['DIAS'];
		
		return $arr;
		
	}
	
}

?>