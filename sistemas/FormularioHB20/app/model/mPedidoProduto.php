<?php 


require_once('lib/appConexao.php');

class mPedidoProduto extends appConexao {
	
	private $idPedido;
	private $codigoProduto;
	private $quantidade;
	private $mediaDemanda;
	private $descConcedido;
	private $descMaximo;
	private $precoFabrica;
	private $precoConsumidor;
	private $avaliarQtd;
	
	
	public function mPedidoProduto() {
		$this->idPedido         = 0;
		$this->codigoProduto    = 0;
		$this->descConcedido    = 0;
		$this->descMaximo       = 0;
		$this->mediaDemanda     = 0;
		$this->precoConsumidor  = 0;
		$this->precoFabrica     = 0;
		$this->quantidade       = 0;
		$this->avaliarQtd       = 0;
	}
	
	public function setIdPedido($valor) {
		$this->idPedido = $valor;	
	}
	
	public function setCodigoProduto($valor) {
		$this->codigoProduto = $valor;	
	}
	
	public function setQuantidade($valor) {
		$this->quantidade = $valor;	
	}
	
	public function setMediaDemanda($valor) {
		$this->mediaDemanda = $valor;	
	}
	
	public function setDescConcedido($valor=0) {
		$this->descConcedido = $valor+0;	
	}
	
	public function setDescMaximo($valor) {
		$this->descMaximo = $valor;	
	}	
	
	public function setPrecoFabrica($valor) {
		$this->precoFabrica = $valor;	
	}	
	
	public function setPrecoConsumidor($valor) {
		$this->precoConsumidor = ($valor == "") ? 'NULL' : $valor;	
	}	
	
	public function setAvaliarQtd($valor) {
		$this->avaliarQtd = $valor;	
	}	
	
	
	
	public function getIdPedido() {
		return $this->idPedido;	
	}
	
	public function getCodigoProduto() {
		return $this->codigoProduto;	
	}
	
	public function getQuantidade() {
		return $this->quantidade;	
	}
	
	public function getMediaDemanda() {
		return $this->mediaDemanda;	
	}
	
	public function getDescConcedido() {
		return $this->descConcedido;	
	}
	
	public function getDescMaximo() {
		return $this->descMaximo;	
	}	
	
	public function getPrecoFabrica() {
		return $this->precoFabrica;	
	}	
	
	public function getPrecoConsumidor() {
		return $this->precoConsumidor;	
	}	
	
	public function getAvaliarQtd() {
		return $this->avaliarQtd;	
	}	
	
	public function selecionar() {
		//$rs = $this->executarQueryArray("SELECT * FROM PDV_PEDIDO_PRODUTO WHERE ID_PEDIDO");	
	}
	
	public function listar() {
		$rs = $this->executarQueryArray("SELECT * FROM PDV_PEDIDO_PRODUTO WHERE ID_PEDIDO = ".$this->idPedido);
		
		$arr = array();
		
		for($i=1;$i<=count($rs);$i++) {
			
			$prod = new mPedidoProduto();
			
			$prod->setCodigoProduto($rs[$i]['CODIGO']);
			$prod->setDescConcedido($rs[$i]['DESCONTO']);
			$prod->setDescMaximo($rs[$i]['DESCONTO_MAX']);
			$prod->setIdPedido($rs[$i]['ID_PEDIDO']);
			$prod->setMediaDemanda($rs[$i]['MEDIA']);
			$prod->setPrecoConsumidor($rs[$i]['PRECO_CONSUMIDOR']);
			$prod->setPrecoFabrica($rs[$i]['PRECO_FABRICA']);
			$prod->setQuantidade($rs[$i]['QTD']);
			$prod->setAvaliarQtd($rs[$i]['AVALIAR_QTD']);
			
			$arr[] = $prod;
			
		}
		
		return $arr;
		
	}
	
	
}

?>