<?php 

require_once('lib/appConexao.php');

class mProduto extends 	appConexao {
	
	private $codigo;
	private $produto;
	private $apresentacao;
	private $desconto;
	private $precoFabrica;
	private $precoConsumidor;
	private $hb20;
	
	private $maxBrasil;
	
	public function mProduto() {
		$this->codigo = 0;
		$this->produto = '';
		$this->apresentacao = '';
		$this->desconto = 0;
		$this->precoFabrica = '';
		$this->precoConsumidor = '';
		$this->hb20 = 0;
	}
	
	public function setCodigo($codigo) {
		$this->codigo = $codigo;	
	}
	
	public function getCodigo() {
		return $this->codigo;	
	}
	
	public function getProduto() {
		return $this->produto;	
	}
	
	public function getApresentacao() {
		return $this->apresentacao;	
	}
	
	public function getDesconto() {
		return $this->desconto;	
	}
	
	public function getPrecoFabrica($icms=20) {
            //print_r($this->precoFabrica);
		return $this->precoFabrica['icms'.$icms];
	}
	
	public function getPrecoConsumidor($icms=20) {
		return $this->precoConsumidor['icms'.$icms];
	}
	
	public function getHB20() {
		return $this->hb20;	
	}
	
	public function getMaxProdutoBR() {
		return $this->maxBrasil;	
	}
	
	
	public function selecionar() {
            
		$rs = $this->executarQueryArray("SELECT * FROM PDV_PRODUTO WHERE CODIGO = ".$this->codigo."");	
		//print_r($rs);
                
		$this->codigo = $rs[1]['CODIGO'];
		$this->produto = $rs[1]['PRODUTO'];
		$this->apresentacao = $rs[1]['APRESENTACAO'];
		$this->desconto = $rs[1]['DESCONTO'];
		$this->hb20 = $rs[1]['HB20'];
		
		$this->precoFabrica['icms12']   = $rs[1]['PF_12'];
		$this->precoFabrica['icms17']   = $rs[1]['PF_17'];
                $this->precoFabrica['icms17.5'] = $rs[1]['PF_175'];
		$this->precoFabrica['icms18']   = $rs[1]['PF_18'];
		$this->precoFabrica['icms20']   = $rs[1]['PF_20'];
		
		$this->precoConsumidor['icms12']    = $rs[1]['PC_12'];
		$this->precoConsumidor['icms17']    = $rs[1]['PC_17'];
                $this->precoConsumidor['icms17.5']  = $rs[1]['PC_175'];
		$this->precoConsumidor['icms18']    = $rs[1]['PC_18'];
		$this->precoConsumidor['icms20']    = $rs[1]['PC_20'];
		
		$this->maxBrasil = $this->mediaDemandaBR();
				
		
	}
        
        public function listar() {
            $produtos = array();
            $rs = $this->executarQueryArray("SELECT CODIGO FROM PDV_PRODUTO WHERE ATIVO = 1 ORDER BY  APRESENTACAO ASC");	
            
            foreach($rs as $row) {
                $prod = new mProduto();
                $prod->setCodigo($row['CODIGO']);
                $prod->selecionar();
                
                $produtos[] = $prod;
            }
            
            return $produtos;
            
        }
	
	private function mediaDemandaBR() {
		$rs = $this->executarQueryArray("select isnull(MAX(media),0) as MAXIMO from PDV_CNPJ where SAP = '".$this->codigo."'");
		return $rs[1]['MAXIMO'];
	}
	
	public function comboProduto($selecionar='', $nomeCombo='cmbProduto') {
		$option = '<option></option>';
		
		$rs = $this->executarQueryArray("SELECT CASE WHEN HB20 = 1 THEN PRODUTO + ' *' ELSE PRODUTO END AS LABEL, PRODUTO FROM PDV_PRODUTO GROUP BY PRODUTO, HB20 ORDER BY PRODUTO, HB20 ASC");	
		
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.$rs[$i]['PRODUTO'].'">'.$rs[$i]['LABEL'].'</option>';	
		}
		
		return '<select id="'.$nomeCombo.'" name="'.$nomeCombo.'" class="form-control input-sm">'.$option.'</select>';
	}
	
	public function comboApresentacao($nomeProduto, $selecionar='', $nomeCombo='cmbApresentacao') {
		$option = '<option></option>';
		
		$rs = $this->executarQueryArray("SELECT CODIGO, APRESENTACAO FROM PDV_PRODUTO WHERE PRODUTO = '".$nomeProduto."' AND ATIVO = 1  ORDER BY APRESENTACAO ASC");	
		
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.$rs[$i]['CODIGO'].'">('.$rs[$i]['CODIGO']. ') '. $rs[$i]['APRESENTACAO'].'</option>';	
		}
		
		return '<select id="'.$nomeCombo.'" name="'.$nomeCombo.'" class="form-control input-sm">'.$option.'</select>';
	}
	
	
	
}

?>