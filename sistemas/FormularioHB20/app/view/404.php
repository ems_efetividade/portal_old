<?php include('header.php') ?>

<div class="container">
	<div class="row" style="min-height:300px;">
    	<div class="col-lg-12">
        	<h1 class="text-danger text-center">Ops!!! Página não encontrada</h1>
            <h4 class="text-info text-center">A página solicitada não pôde ser encontrada em nossos servidores.</h4>
        </div>
    </div>
</div>

