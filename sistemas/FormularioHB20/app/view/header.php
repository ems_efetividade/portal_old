<?php 
    require_once('lib/appConf.php'); 
    require_once('lib/appFunction.php');
    appFunction::validarSessao();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 



<title>Portal EMS Prescrição</title>

<!--Plugin javascript do jquery e bootstrap-->
    <script src="<?php echo appConf::caminho ?>plugins/jquery/jquery-1.12.1.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo appConf::caminho ?>plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo appConf::caminho ?>app/view/js/index.js"></script>


<link rel="shortcut icon" href="<?php echo appConf::caminho ?>public/img/liferay.ico" type="image/x-icon" />


<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo appConf::caminho ?>public/css/index.css" rel="stylesheet" type="text/css" />
<style>
/*VACINA PRO IPAD EM LANDSCAPE*/
@media all and (min-width: 768px) and (max-width: 1024px) {
    .navbar-header {
        float: none;
    }
    .navbar-left,.navbar-right {
        float: none !important;
    }
    .navbar-toggle {
        display: block;
    }
    .navbar-collapse {
        border-top: 1px solid transparent;
        box-shadow: inset 0 1px 0 rgba(255,255,255,0.1);
    }
    .navbar-fixed-top {
		top: 0;
		border-width: 0 0 1px;
	}
    .navbar-collapse.collapse {
        display: none!important;
    }
    .navbar-nav {
        float: none!important;
		margin-top: 7.5px;
	}
	.navbar-nav>li {
        float: none;
    }
    .navbar-nav>li>a {
        padding-top: 10px;
        padding-bottom: 10px;
    }
    .collapse.in{
  		display:block !important;
	}
}





</style>
</head>

<body>

<!--MODAL Aguarde-->
<div class="modal fade" style="z-index:10000000;" id="modalMsgBox" tabindex="-1" role="dialog" data-backdrop="false" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
      </div>
      
      <div class="modal-body">
      
      	<div class="row">
        	<!--<div class="col-lg-3">
            	<p class="text-center text-danger" style="font-size:500%;"><span class="glyphicon glyphicon-remove"></span><p>
            </div>-->
            <div class="col-lg-12">
            	<h5><p class="text-center" id="msgboxTexto"></p></h5>
            </div>
        </div>
      
      	
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>

    </div>
  </div>
</div>

<!--MODAL Aguarde-->
<div class="modal fade" style="z-index:100000;" id="modalAguarde" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
      
      <div class="modal-body">
      	<p class="text-center"><strong>Processando...</strong></p>
        <h5 class="text-center"><small>(Esta operação pode demorar alguns minutos.)</small></h5>
        
        <div class="progress">
          <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span class="sr-only"></span>
          </div>
        </div>
        
      </div>

    </div>
  </div>
</div>



<!--MODAL LOGOUT-->
<div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Sair do sistema</h5>
      </div>
      
      <div class="modal-body">
      	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img class="img-responsive" src="<?php echo appConf::caminho ?>public/img/question.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja sair do sistema?</h4>
            </div>
        </div>
      </div>
          

      <div class="modal-footer">
      	<form action="<?php echo appConf::caminho ?>login/logout" method="POST">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">
        	<span class="glyphicon glyphicon-remove"></span> Fechar
        </button>
        
        <button type="submit" class="btn btn-info btn-md">
        	<span class="glyphicon glyphicon-off"></span> Sair do sistema
        </button>
        </form>
      </div>
    </div>
  </div>
</div>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        Formulario de Pedido Fatura PDV/HB20 - <?php print appFunction::dadoSessao('setor').' '.appFunction::dadoSessao('nome') ?>
      </a>
    </div>
  </div>
</nav>