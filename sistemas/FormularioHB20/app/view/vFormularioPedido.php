<?php require_once('app/view/header.php'); ?>
<script src="<?php print appConf::caminho ?>plugins/mask/jquery.mask.js" type="text/javascript"></script>
<script>

var lastPDV;
var lastICMS;

$(document).ready(function(e) {
    
    //gerarTabelaProduto(0);
    
    $('#btnLimparLista').click(function(e) {
        resetarArrayProduto();
    });

    $('#cmbPDV, #cmbICMS').change(function(e) {

        if($('#cmbPDV').val() != "" && $('#cmbICMS').val() != "") {
            gerarTabelaProduto($('#cmbICMS').val());
        }
        
    });
    
    
	
	$('#btnProduto').click(function(e) {
        
		//alert($('#cmbPDV').val());
		
		if($('#cmbPDV').val() == "" || $('#cmbICMS').val() == "") {
			msgBox('Os campos Nome do PDV e ICMS não podem estar vazios!');	
		} else {
			//$('#modalProdutos').modal('show');	
                        gerarTabelaProduto($('#cmbICMS').val());
		}
		
    });
	/*
	$('#cmbPDV').click(function(e) {
            lastPDV = $(this).val();
		
		if($('#tableProdutos').html() != "") {
                    $('#modalLimpar').modal('show');
                    $('#btnAlterar').click(function(e) {
                        resetarArrayProduto();
                        $('#modalLimpar').modal('hide');
                    });	
                }
    });
	
	
	$('#cmbICMS').click(function(e) {
         lastICMS = $(this).val();
		 
		 $('input[name="txtICMS"]').val($(this).val());
		
		if($('#tableProdutos').html() != "") {
			$('#modalLimpar').modal('show');
			
			$('#btnAlterar').click(function(e) {
                resetarArrayProduto();
				$('#modalLimpar').modal('hide');
            });
			
		}
    });
    
*/
	
	$('#cmbProduto').change(function(e) {
		
		
		$.ajax({
			type: "POST",
		  	url: '<?php print appConf::caminho ?>home/comboApresentacao',
		  	data: {
				produto: $(this).val()	
			},
			beforeSend: function(e){
				$('#comboApresentacao').html('<select class="form-control input-sm"><option>Carregando...</option></select>');
			},
		  	success: function(u) {
				$('#comboApresentacao').html(u);
				
					$('#cmbApresentacao').change(function(e) {
						popularDadosProduto($(this).val());
					});
				
					limparCampos();
				
		   	}
		});
	   
	   
    });
	
	$('input[name="txtDescConc"]').keyup(function(e) {
       // alert($(this).val());
		//calcularValorTotal($('input[name="txtQtd"]').val(), $('input[name="txtPrecoFab"]').val(), $('input[name="txtDescConc"]').val(), $('input[name="txtDescMax"]').val());
		calcularValorTotal($('#cmbApresentacao').val(), $('input[name="txtQtd"]').val(), $('input[name="txtDescConc"]').val());
    });
	
	$('input[name="txtQtd"]').keyup(function(e) {
				
		maxProd = parseInt($('input[name="txtQtdMax"]').val());
		qtdProd = parseInt($(this).val());
		
		if (isNaN(qtdProd)) {
			qtdProd = 0;
		}
		
		//$('input[name="txtDescConc"]').val(qtdProd);
		
		if(qtdProd != "NaN") {
		
		$.ajax({
			type: "POST",
			url: '<?php print appConf::caminho ?>home/mediaDemandaPDV',
			data: {
				pdv: $('#cmbPDV').val(),
				produto:  $('#cmbApresentacao').val()
			},
			success: function(rt) {
				mediaPDV = rt;
				
				
				
				$.ajax({
					type: "POST",
					url: '<?php print appConf::caminho ?>home/calcularDesconto/' + mediaPDV + '/'+ qtdProd +'/' + $('#cmbApresentacao').val(),
					success: function(u) {
						//alert(u);
						dados = $.parseJSON(u);
						
							
						$('input[name="txtDescMax"]').val(dados.desconto);
						$('input[name="txtDescConc"]').val(dados.desconto);
									
						calcularValorTotal($('#cmbApresentacao').val(), $('input[name="txtQtd"]').val(), $('input[name="txtDescConc"]').val());
					
						
					}
				});
				
				
			}
		});
		
		
		
		}
         
    });
	
	
	$('#btnAdicionarProduto').click(function(e) {
        
          
        
        /*
            $.ajax({
			type: "POST",
		  	url: $('#formAdicionarProduto').attr('action'),
		  	data: $('#formAdicionarProduto').serialize(),
		  	success: function(u) {
				
				
				try {
				   $.parseJSON(u)
				   dados = $.parseJSON(u);
					$('#modalProdutos').modal('hide');
					$('#tableProdutos').html(dados.tabela);
					$('#strTotalGeral').html(dados.totalGeral)
				} catch(e) {
					//alert(u);    
					msgBox(u);
				}
				
				
				//dados = $.parseJSON(u);
				
				
		   	}
		});
        
        */
    });
	
	$('#btnSalvarPedido').click(function(e) {
        ///$('#formPedido').submit();
		
		 $.ajax({
			type: "POST",
		  	url: $('#formPedido').attr('action'),
		  	data: $('#formPedido').serialize(),
		  	success: function(u) {
				
				
				if(u == "") {
					window.location = '<?php print appConf::caminho ?>';
				} else {
					//alert(u);	
					msgBox(u);
				}
				//dados = $.parseJSON(u);
				
				
		   	}
		});
		
    });
	
	$('#modalProdutos').on('show.bs.modal', function (event) {
		limparCampos();
		$('#cmbApresentacao').val(0);
		$('#cmbProduto').val(0);
	});
	
	$('#modalExcluir').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var codigo = button.data('cod') // Extract info from data-* attributes
	  	
		$('#btnRemoverProduto').one('click', function() {
			
            excluirProdutoLista(codigo);
			$('#modalExcluir').modal('hide');
        });
		
		//e.stopPropagation();
		
	});
        
        $('.number').mask('000');
	

});

function gerarTabelaProduto(icms) {
    $.ajax({
        type: "POST",
        url: '<?php print appConf::caminho ?>home/gerarListaPedido/'+icms,
        beforeSend: function (xhr) {
          $('#tableProdutos').html('Listando produtos...');              
        },
        success: function(u) {
            $('#tableProdutos').hide().fadeIn('fast').html(u);

            $('.qtd').keyup(function(e) {
                ind = $(this).closest('tr').attr('ind');
                precoFab = $(this).closest('tr').attr('preco-fabb');
                desc = $(this).closest('tr').find('.desc').val();
                valorTotal(precoFab, $(this).val(), desc, ind);                                    
            });

            $('.desc').keyup(function(e) {
                
                max = parseInt($(this).attr('value-max'));
                if(parseInt($(this).val()) > max) {
                    $(this).val(max);
                }
                
                ind = $(this).closest('tr').attr('ind');
                qtd = $(this).closest('tr').find('.qtd').val();
                precoFab = $(this).closest('tr').attr('preco-fabb');
                valorTotal(precoFab, qtd , $(this).val(), ind);                                    
            });                                

               
            $('#strTotalGeral').html('0,00');
        }
    });
}

function resetarArrayProduto() {
	$.ajax({
		type: "POST",
		url: '<?php print appConf::caminho ?>home/limparListaProduto',
		success: function(u) {
			$('#tableProdutos').html('');
			$('#strTotalGeral').html(0);

		}
	});
}
function limparCampos() {
	$('input[name="txtQtd"]').val('');
	$('input[name="txtDescMax"]').val('');
	$('input[name="txtPrecoFab"]').val('');
	$('input[name="txtPrecoCons"]').val('');
	$('input[name="txtMediaDemanda"]').val('');
	$('input[name="txtQtdMax"]').val('');
	$('input[name="txtDescConc"]').val('');
	$('input[name="txtPrecoTotal"]').val('');	
}

function popularDadosProduto(codigo) {

	$.ajax({
		type: "POST",
		url: '<?php print appConf::caminho ?>home/selecionarProduto',
		data: {
			codigo: codigo,
			cnpj: $('#cmbPDV').val(),
			icms: $('#cmbICMS').val()
		},
		
		
		beforeSend: function(e) {
			loading = 'Carregando...';
			
			$('input[name="txtQtd"]').val('');
			$('input[name="txtDescMax"]').val(loading);
			$('input[name="txtPrecoFab"]').val(loading);
			$('input[name="txtPrecoCons"]').val(loading);
			$('input[name="txtMediaDemanda"]').val(loading);
			$('input[name="txtQtdMax"]').val(loading);
			$('input[name="txtDescConc"]').val('');
			$('input[name="txtPrecoTotal"]').val('');
		},
		success: function(u) {
			
			//alert(u);

			produto = $.parseJSON(u);
			
			$('input[name="txtDescMax"]').val(produto.descontoMax);
			$('input[name="txtPrecoFab"]').val(produto.precoFab);
			$('input[name="txtPrecoCons"]').val(produto.precoCons);
			$('input[name="txtMediaDemanda"]').val(produto.media);
			$('input[name="txtQtdMax"]').val(produto.maxProd);
			$('#tituloMediaPDV').html(produto.tituloMedia);
			$('input[name="txtQtd"]').val('');
			
			//if(produto.hb20 == 1) {
			//	$('input[name="txtDescConc"]').val('');
			//} else {
				$('input[name="txtDescConc"]').val(produto.descontoMax);
			//}
					
		}
	});
}

function calcularValorTotal(codigo, qtd, desconto) {

	

	$.ajax({
		type: "POST",
		url: '<?php print appConf::caminho ?>home/calcularValorTotalProdutos',
		data: {
			qtd: qtd,
			codigo: codigo,
			desconto:desconto,
			icms: $('#cmbICMS').val(),
			cnpj: $('#cmbPDV').val()
		},
		success: function(u) {
			total = $.parseJSON(u);
			
			if(total.erro == "") {
				$('#vlrTotalProduto').val(total.desconto);
				$('input[name="txtAvaliarQtd"]').val(total.avaliarQtd);
				//$('#vlrTotalProduto').html(total.desconto);
			} else {
				
				if(total.erroNum == 0) {
					$('input[name="txtDescConc"]').val($('input[name="txtDescMax"]').val());
				}
				
				if(total.erroNum == 1) {
					$('input[name="txtQtd"]').val($('input[name="txtQtdMax"]').val());
				}
				
				msgBox(total.erro);
				//alert(total.erro);	
				
			}
			
			
		}
	});	
	

	/*
	qtd = parseInt(qtd);
	vlr = parseFloat(vlr);
	desc = parseFloat(desc);
	
	preco = Number(parseFloat((qtd * vlr) - (vlr * (desc / 100)))).toFixed(2);
	
	alert(preco);*/
}


function valorTotal(valor, qtd, desconto, ind) {
    qtd = parseInt(qtd);
    valor = parseFloat(valor);
    desconto = parseInt(desconto);
    totalGeral = 0;
    
    if(qtd > 0) {
    
        if(desconto > 0) {
            total = (qtd * valor) - ((qtd * valor) * (desconto / 100));
        } else {
            total = (qtd * valor);
        }
        
    } else {
        total = 0;    
    }


        //alert('(' + qtd + ' * ' + valor + ') * (100 / ' + desconto + ')');
        $.post( '<?php print appConf::caminho ?>home/formatarMoeda/'+total, function( data ) {
            $('#total'+ind).hide().fadeIn('fast').html(data);

            $(".total-produto").each(function() {
                v = $(this).html();
                v = v.replace('.', '');
                v = v.replace(',', '.');

                totalGeral = totalGeral + parseFloat(v);
                //alert(totalGeral);
            });

            $.post( '<?php print appConf::caminho ?>home/formatarMoeda/'+totalGeral, function(d) {
                $('#strTotalGeral').html(d);
            });

        });
    

}

function formatarMoeda(valor) {
valor = 0;
    $.post( '<?php print appConf::caminho ?>home/formatarMoeda/'+valor, function( data ) {
      valor = data;
    });
   return valor;


    
}
function excluirProdutoLista(produto) {


	$.ajax({
		type: "POST",
		url: '<?php print appConf::caminho ?>home/removerProduto/'+produto,
		success: function(u) {
			//alert(u);
			dados = $.parseJSON(u);
			$('#tableProdutos').html(dados.tabela);
			$('#strTotalGeral').html(dados.totalGeral);
		}
	});
}

</script>
<style>
.input-sm { padding:5px; }
</style>
<div class="container">

	<form id="formPedido" action="<?php print appConf::caminho ?>home/salvarPedido" method="post">
	<div class="row">
    
    	<div class="col-lg-12">
        	<div class="panel panel-primary">
            	<div class="panel-heading">
                	<div class="row">
                    	<div class="col-lg-6 col-md-8 col-xs-8">
                    		<h3 style="margin:0px;"><sthrong><span class="glyphicon glyphicon-list-alt"></span> Formulário de Pedidos - Fatura PDV/HB20</strong></h3>    
                        </div>
                        
                        <div class="col-lg-6 col-md-4 col-xs-4">
                        	<div class="pull-right"><a class="btn btn-default btn-sm" href="<?php print appConf::caminho ?>" role="button"><span class="glyphicon glyphicon-chevron-left"></span> Histórico de Pedidos</a></div>
                        </div>
                    </div>
                	
                    
                </div>
            </div>
        	
        </div>
       
    </div>


   
    <div class="panel panel-default">
            	
                <div class="panel-body">
    <div class="row">
    
    	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Data do Pedido</small></label>
                <input type="text" class="form-control input-sm" placeholder="12/12/2015" name="txtDataPedido" value="<?php print date('d/m/Y') ?>" readonly="readonly">
            </div>
        </div>
          
        
       <div class="col-lg-5 col-md-5 col-xs-9">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Nome do PDV</small></label>
                <?php print $comboPDV; ?>
            </div>
        </div>
        
    	<div class="col-lg-3 col-md-3 col-xs-4">
            <div class="form-group">
                <label for="exampleInputEmail1 input-sm"><small>Distribuidor</small></label>
 				<?php print $comboDistribuidor; ?>
                
            </div>
        </div>        
        
        
    	<div class="col-lg-1 col-md-1 col-xs-4">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Pagto</small></label>
                <?php print $comboCondicao; ?>
                
            </div>
        </div>
        
		<div class="col-lg-1 col-md-1 col-xs-4">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>ICMS</small></label>
                <select class="form-control input-sm" name="cmbICMS" id="cmbICMS">
                  <option></option>
                  <option value="12">12%</option>
                  <option value="17">17%</option>
                  <option value="17.5">17,5%</option>
                  <option value="18">18%</option>
                  <option value="20">20%</option>
                </select>
                
            </div>
        </div>
        
        
        
     
    </div>
                    
                    
   
    </div>
    
    
    </div>
            


            
            
     <div class="panel panel-default">
            	
    <div class="panel-body">
    <div class="row">
    	
        <div class="col-lg-6 col-md-6 col-xs-6 text-left">
        	<!--<p><a class="btn btn-success btn-sm" href="#" role="button" daata-toggle="modal" daata-target="#modalProdutos" id="btnProduto"><span class="glyphicon glyphicon-plus"></span> Adicionar Produto</a></p>-->
        </div>
        
        <div class="col-lg-6 col-md-6 col-xs-6 text-right">
        	<!--<p><a class="btn btn-danger btn-sm" href="#" role="button" id="btnLimparLista"><span class="glyphicon glyphicon-trash"></span> Limpar Lista</a></p>-->
                <p><a class="btn btn-primary btn-sm" href="#" role="button" data-toggle="modal" data-target="#modalConfirmar"><span class="glyphicon glyphicon-ok"></span> Salvar Pedido</a></p>
        </div>
	
            
    </div>
    
   
    
    <small>
        
       
     <div id="tableProdutos"></div>
    
    
     
    
    </small>
    </div>
    
    </div>
    
             <div class="panel panel-default">
            	
                <div class="panel-body">
                	<h4 class="text-right"><strong>Total Geral: </strong><span id="strTotalGeral">0</span></h4>
                </div>
         </div>
         
         <a class="btn btn-primary btn-lg pull-right" href="#" role="button" data-toggle="modal" data-target="#modalConfirmar"><span class="glyphicon glyphicon-ok"></span> Salvar Pedido</a>
      <br />
  <br />
  </form>
  <br />


</div>





<!--MODAL PRODUTOS-->
<div class="modal fade" style="z-index:100000;" id="modalProdutos" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-check"></span> Selecionar Produto</h5>
      </div>
      
      <div class="modal-body">
      <form id="formAdicionarProduto" action="<?php print appConf::caminho ?>home/adicionarProduto">
      	<input type="hidden" name="txtListaDeProduto" value="<?php //print $listaDeProduto; ?>" />
        <input type="hidden" name="txtAvaliarQtd" value="0" />
        <input type="hidden" name="txtICMS" value="0" />
      	<div class="row">
        	<div class="col-lg-12 col-md-4 col-xs-4">
        
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Produto (* = HB20)</small></label>
				<?php print $comboProduto; ?>
            </div>
        </div>
        
		<div class="col-lg-12 col-md-8 col-xs-8">
        
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Apresentação</small></label>
                <span id="comboApresentacao">
               	<select class="form-control input-sm">
                  <option></option>
                </select></span>
            </div>
        </div>
        
        </div>
        
        <hr>
        
        <div class="row">
        
    	<div class="col-lg-2 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small id="tituloMediaPDV">Média PDV</small></label>
                <input type="text" class="form-control input-sm" name="txtMediaDemanda" readonly="readonly">
            </div>
        </div>   
        
    	<div class="col-lg-2 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Qtd. Máx</small></label>
                <input type="text" class="form-control input-sm" name="txtQtdMax" readonly="readonly">
            </div>
        </div>       
        
        <div class="col-lg-2 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Desc. Máx %</small></label>
                <input type="text" class="form-control input-sm" name="txtDescMax" readonly="readonly">
            </div>
        </div> 
        
    	<div class="col-lg-3 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>R$ Fábrica</small></label>
                <input type="text" class="form-control text-right input-sm" name="txtPrecoFab" readonly="readonly">
            </div>
        </div>    
        
    	<div class="col-lg-3 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>R$ Consumidor</small></label>
                <input type="text" class="form-control text-right input-sm" name="txtPrecoCons" readonly="readonly">
            </div>
        </div>          
        
    	<div class="col-lg-2 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Qtd</small></label>
                <input type="text" class="form-control input-sm" name="txtQtd">
            </div>
        </div>         
        
    	<div class="col-lg-2 col-md-3 col-xs-3">
            <div class="form-group">
                <label for="exampleInputEmail1"><small>Desc. Con %</small></label>
                <input type="text" class="form-control input-sm" name="txtDescConc">
            </div>
        </div> 
        

        

        
    	<div class="col-lg-8 col-md-3 col-xs-3">
            <div class="form-group has-warning">
                <label class="control-label text-right" for="inputSuccess1"><small>Valor Total</small></label>
                <input type="text" style="background-color:#fcf8e3;color:#8a6d3b;font-weight:bold" class="input-sm form-control text-right" id="vlrTotalProduto" name="txtPrecoTotal" disabled>
            </div>
        </div>  
        
        
        
        

            
        
        </div>
        
    
      
       </form>
      	
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary btn-sm" id="btnAdicionarProduto"><span class="glyphicon glyphicon-ok"></span> Adicionar</button>
      </div>

    </div>
  </div>
</div>



<!--MODAL CONFIMARA MUDANCA-->
<div class="modal fade"  id="modalLimpar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Atenção</h5>
      </div>
      
      <div class="modal-body">
      	<h4>Ao alterar o PDV ou o ICMS, os dados serão perdidos. Continuar?</h4>      	
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-sm" id="btnAlterar"><span class="glyphicon glyphicon-remove"></span> Alterar</button>
      </div>

    </div>
  </div>
</div>



<!--MODAL Confirmar-->
<div class="modal fade"  id="modalConfirmar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Salvar pedido</h5>
      </div>
      
      <div class="modal-body text-center">
      	<h4>O pedido não poderá ser editado futuramente.</h4><h4> Deseja realmente salvar as informações deste pedido?</h4>      	
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-info btn-sm" id="btnSalvarPedido"><span class="glyphicon glyphicon-ok"></span> Salvar Pedido</button>
      </div>

    </div>
  </div>
</div>


<!--MODAL PRODUTOS-->
<div class="modal fade"  id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Selecionar Produto</h5>
      </div>
      
      <div class="modal-body text-center text-danger">
      	<h4>Deseja remover esse produto da lista?</h4>      	
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-sm" id="btnRemoverProduto"><span class="glyphicon glyphicon-remove"></span> Remover</button>
      </div>

    </div>
  </div>
</div>