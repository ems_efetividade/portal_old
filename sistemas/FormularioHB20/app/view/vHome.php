<?php require_once('app/view/header.php'); ?>

<?php

$mes = date('m');
$ano = date('Y');
$dia = date('d');

$ultimo_dia = date("t", mktime(0,0,0,$mes,'01',$ano));
$semana = date("w", strtotime($ano.'-'.$mes.'-'.$ultimo_dia));

$penultimo = $ultimo_dia - 1;

if($semana == 0) {
	$penultimo = $penultimo - 2;
}

if($semana == 6) {
	$penultimo = $penultimo - 1;
}

if($semana == 1) {
	$penultimo = $penultimo - 3;
}


if($dia >= $penultimo && $dia <= $ultimo_dia) {
	$botao =  '';
} else {
	$botao = '<div class="row">
                        <div class="col-lg-12">
                                <div class="pull-left">
                                         <a class="btn btn-success" href="'.appConf::caminho.'home/novoPedido" role="button"><span class="glyphicon glyphicon-plus"></span> Novo Pedido</a>
                                </div>
                        </div>
                </div>';
}

$semana = date('w');

if(($semana == 0 || $semana == 6) || (date('H') <= 8 || date('H') >= 19)) {
    $botao = '';
}
/*
if(date('H') <= 8 || date('H') >= 19) {
    $botao = '';
}
 * 

 */

 ?>

<script>

$(document).ready(function(e) {
    
	$('#modalExcluirPedido').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget) // Button that triggered the modal
		var codigo = button.data('cod') // Extract info from data-* attributes
		
                //alert(codigo);
                
		$('#btnExcluirPedido').click(function(e) {
            
			$.ajax({
				type: "POST",
				url: '<?php print appConf::caminho ?>home/excluirPedido/' + codigo,
				success: function(u) {
					window.location = '<?php print appConf::caminho ?>';
				}
			});
			
        });
		
	});
	
	$('.exportar').click(function(e) {
        
		idPedido = $(this).attr('data-cod');
		
		$('#idPedido').val(idPedido);
		$('#formExportar').submit();
		
		
		setStatus(idPedido);

    });
	
	$('#cmbFiltroStatus').change(function(e) {
        window.location = '<?php print appConf::caminho ?>home/main/1/'+$(this).val();
    });
	
});

function setStatus(a) {
	status = requisicaoAjax('<?php print appConf::caminho ?>home/getStatus/'+a);
	$('#status'+idPedido).html(status);
}

</script>

<form id="formExportar" action="<?php print appConf::caminho ?>home/exportarPedido" method="post">
	<input type="hidden" name="idPedido" id="idPedido" value="0"  />
</form>

<div class="container">

	<?php if(appFunction::dadoSessao('nivel_admin') == 0) { 
		print $botao;
	} else { ?>
    
    <div class="row">
    
    	<div class="col-lg-3">
        	<div class="panel panel-default">
            	<div class="panel-body text-center text-primary">
                	<h1 style="margin:0px;"><strong><?php print $totalPedidos; ?></strong></h1>
                    <h5>pedidos cadastrados.</h5>
                </div>
            </div>
        </div>
    
    	<div class="col-lg-3">
        	<div class="panel panel-default">
            	<div class="panel-body text-center text-success">
                	<h1 style="margin:0px;"><strong><?php print $totalPedidosExportados; ?></strong></h1>
                    <h5>pedidos recebidos/exportados!</h5>
                </div>
            </div>
        </div>
        
    	<div class="col-lg-3">
        	<div class="panel panel-default">
            	<div class="panel-body text-center text-danger">
                	<h1 style="margin:0px;"><strong><?php print $totalPedidosPendentes; ?></strong></h1>
                    <h5>pedidos pendentes!</h5>
                </div>
            </div>
        </div>
        
    	<div class="col-lg-3">
        	<div class="panel panel-default">
            	<div class="panel-body text-center text-warning">
                	<h1 style="margin:0px;"><strong><?php print $mediaPedidosDia; ?></strong></h1>
                    <h5>pedidos recebidos por dia (média)</h5>
                </div>
            </div>
        </div>
        
        
        
    	
    
    </div>
    
    <?php }  ?>
    <br />


	<div class="row">
    	<div class="col-lg-12">
        	
            <div class="panel panel-default">
            	<div class="panel-heading">
                	<div class="row">
                    	<div class="col-lg-9">
                        	Últimos Pedidos (<?php //print $totalPedido; ?>)
                        </div>
                        <div class="col-lg-3">
                 <?php if(appFunction::dadoSessao('nivel_admin') > 0) { ?>       	         
                <select class="form-control input-sm" id="cmbFiltroStatus">
                  <option value="">::Filtrar por Status::</option>
                  <option value="">TODOS</option>
                  <option value="0">PENDENTE</option>
                  <option value="1">RECEBIDO</option>
                </select> 
                <?php } ?>
                
            
                        </div>
                    </div>
                    </div>
                <div class="panel-body">
                
                	<?php print $tabelaPedido; ?>
                    <?php print $paginacao[0]; ?>
                
                </div>
            </div>
            
        </div>
    </div>
</div>






<!--MODAL Confirmar-->
<div class="modal fade"  id="modalExcluirPedido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Excluir</h5>
      </div>
      
      <div class="modal-body text-center">
      	<h4>Deseja excluir esse pedido?</h4>      	
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-sm" id="btnExcluirPedido"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>

    </div>
  </div>
</div>


