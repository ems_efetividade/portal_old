<?php require_once('app/view/header.php'); ?>

<style>
.input-sm { padding:5px; }

</style>
<div class="container">

	<form id="formPedido" action="<?php print appConf::caminho ?>home/salvarPedido" method="post">
	<div class="row">
    
    	<div class="col-lg-12">
        	<div class="panel panel-primary">
            	<div class="panel-heading">
                	<div class="row">
                    	<div class="col-lg-6 col-md-8 col-xs-8">
                    		<h3 style="margin:0px;"><sthrong><span class="glyphicon glyphicon-list-alt"></span> Visualizar Pedido (Nº Pedido: <?php print str_pad($idPedido, 5, 0, 0) ?>)</strong></h3>    
                        </div>
                        
                        <div class="col-lg-6 col-md-4 col-xs-4">
                        	<div class="pull-right"><a class="btn btn-default btn-sm" href="<?php print appConf::caminho ?>" role="button"><span class="glyphicon glyphicon-chevron-left"></span> Histórico de Pedidos</a></div>
                        </div>
                    </div>
                	
                    
                </div>
            </div>
        	
        </div>
       
    </div>


   
    <div class="panel panel-default">
            	
                <div class="panel-body">
    <div class="row">
    
    	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
           <h5><strong>Data do Pedido</strong></h5>
           <h5><?php print appFunction::formatarData($dataPedido); ?></h5>
        </div>
          
        
       <div class="col-lg-4 col-md-4 col-xs-12">
           <h5><strong>Nome do PDV</strong></h5>
           <h5><?php print $nomePDV; ?></h5>
        </div>
        
    	<div class="col-lg-3 col-md-2 col-xs-12">
           <h5><strong>Distribuidor</strong></h5>
           <h5><?php print $nomeDistribuidor; ?></h5>
        </div>        
        
        
    	<div class="col-lg-1 col-md-1 col-xs-6">
           <h5><strong>Pagamento</strong></h5>
           <h5><?php print $pagamento; ?></h5>
        </div>
        
		<div class="col-lg-1 col-md-1 col-xs-6">
           <h5><strong>ICMS</strong></h5>
           <h5><?php print $icms; ?>%</h5>
        </div>
        
		<div class="col-lg-1 col-md-1 col-xs-12">
           <h5><strong>Status</strong></h5>
           <h5><?php print $status; ?></h5>
        </div>
        
     
    </div>
   
    </div>
    
    
    </div>
    
     <div class="panel panel-default">
            	
    <div class="panel-body">
   
   
    
    <small>
        
       
     <div id="tableProdutos">
     	<?php print $tabelaProduto ?>
     </div></small>
    </div>
    
    </div>
    
             <div class="panel panel-default">
            	
                <div class="panel-body">
                	<h4 class="text-right"><strong>Total Geral: </strong><span id="strTotalGeral"><?php print $totalTabela ?></span></h4>
                </div>
         </div>
         
         
    
  </form>
  <br />

</div>




