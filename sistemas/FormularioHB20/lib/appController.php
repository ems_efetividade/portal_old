<?php

require_once('lib/appView.php');
require_once('lib/appFunction.php');

class appController extends appView {
	
	public $visao = null;
	public $funcao = null;
	
	function appController() {
		$this->visao = new appView();
		$this->funcao = new appFunction();
	}

	public function main() {
		die('main do appController');
	}
	
	public function location($url) {
		header('Location: '.$url);
	}

}

?>