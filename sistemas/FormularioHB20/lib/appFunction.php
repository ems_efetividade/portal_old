<?php

require_once('lib/appConexao.php');

class appFunction {

	private $cn = null;

	function appFunction() {
		$this->cn = new appConexao();
	}
	/**
        * Método que retorna a informação da sessao
         */
	public function dadoSessao($chave) {
		@session_start();
		return $_SESSION[$chave];
	}
        
        public function validarSessao() {
            @session_start();
            if(!isset($_SESSION['id_setor'])) {
                header('Location: '.'https://'.SERVER_URL.'/login/');
            }
        }  
	
	public function formatarMoeda($numero, $decimal=1, $simbolo='') {
		
		if($numero == '' || is_null($numero)) {
			return '  &nbsp;';
		} else {
			return number_format($numero, $decimal, ",", ".").$simbolo;
		}
	}
	
	public function formatarMoedaSQL($valor) {
		return str_replace(",", ".", str_replace(".", "", $valor));
	}
	
	public function comboDistribuidor($selecionar='', $nomeCombo='cmbDistribuidor') {
		$this->cn = new appConexao();
		$option = '<option></option>';
		
		$rs = $this->cn->executarQueryArray("SELECT DISTRIBUIDOR FROM PDV_DISTRIBUIDOR GROUP BY DISTRIBUIDOR ORDER BY DISTRIBUIDOR ASC");	
		
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.$rs[$i]['DISTRIBUIDOR'].'">'.$rs[$i]['DISTRIBUIDOR'].'</option>';	
		}
		
		return '<select id="'.$nomeCombo.'" name="'.$nomeCombo.'" class="form-control input-sm">'.$option.'</select>';
	}	
	
	
	public function comboCondicaoPagto($selecionar='', $nomeCombo='cmbCondicaoPagto') {
		$this->cn = new appConexao();
		$option = '<option></option>';
		
		$rs = $this->cn->executarQueryArray("SELECT * FROM PDV_CONDICAO_PAGTO ORDER BY CONDICAO ASC");	
		
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.$rs[$i]['CONDICAO'].'">'.$rs[$i]['CONDICAO'].'</option>';	
		}
		
		return '<select id="'.$nomeCombo.'" name="'.$nomeCombo.'" class="form-control input-sm">'.$option.'</select>';
	}
	
	
	
	public function formatarData($data) {
		if($data != "") {
			
			$dataHora = explode(" ", $data);
			
			if(count($dataHora) > 0) {
				$campos = explode("-", $dataHora[0]);
				$hora = explode(".", $dataHora[1]);
			} else {
				$campos = explode("-", $data);
				$hora = '';
			}

			if(count($campos) == 1) {
				$campos = explode("/", $data);
				return $campos[2]."-".$campos[1]."-".$campos[0];
			} else {
				return $campos[2]."/".$campos[1]."/".$campos[0].' '.$hora[0];
			}
			
		}
	}
	
}

?>