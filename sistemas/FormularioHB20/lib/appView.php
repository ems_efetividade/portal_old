<?php

class appView {
	
	public $dados = array();
	private $saveArray;
	
	public function set($nome, $valor) {
		$this->dados[$nome] = $valor;	
	}
	
	public function setArray($arr) {
		$this->saveArray = $arr;
	}
	
	public function getArray() {
		return $this->saveArray;
	}
	
	public function bind($nome, $valor) {
        $this->dados[$nome] = $valor; 
    }
	
	public function get($nome='') {

        if ($nome == '') {
            return $this->dados;
        }
        else {
            if (isset($this->dados[$nome]) && ($this->dados[$nome] != '')) {
                return $this->dados[$nome];
            }
            else {
                return '';
            }
        }
    }
	
	public function render($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		
		if (file_exists("app/view/{$arquivo}.php")) {
            include "app/view/{$arquivo}.php";
			
        }
		
	}
	
	public function renderToString($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		
		if (file_exists("app/view/{$arquivo}.php")) {
			ob_start();
			include("app/view/{$arquivo}.php");
			return ob_get_clean();
        }
		
	}
	
}

?>