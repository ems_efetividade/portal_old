<?php
require_once('lib/appController.php');
require_once('app/model/mControleHeader.php');
require_once('app/view/formsControleHeader.php');

class cControleHeader extends appController
{

    private $modelControleHeader = null;

    public function __construct()
    {
        $this->modelControleHeader = new mControleHeader();
    }

    public function main()
    {
        $this->render('vControleHeader');
    }

    public function controlSwitch($acao)
    {
        $pesquisa = $_POST['isPesquisa'];
        if ($pesquisa == '1')
            $acao = 'pesquisar';
        switch ($acao) {
            case 'cadastrar':
                $forms = new formsControleHeader();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsControleHeader();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'loadFerramenta':
                require_once('cferramenta2.php');
                $control_ferramenta = new cferramenta2();
                $objeto = $control_ferramenta->loadObj();
                $arrayJson['caminho'] = $objeto->getCaminho();
                $arrayJson['ferramenta'] = $objeto->getFerramenta();
                echo json_encode($arrayJson);
                break;
            case 'editar':
                $forms = new formsControleHeader();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            case 'CadastrarFerramenta':
                $forms = new formsControleHeader();
                $forms->formFerramenta();
                break;
            case 'salvarFerramenta':
                require_once('cferramenta2.php');
                $control_ferramenta = new cferramenta2();
                $control_ferramenta->save();
                echo 1;
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save()
    {
        $_POST = appSanitize::filter($_POST);

        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeader->save();
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeader->delete();
    }

    public function listAllObj()
    {
        $_POST = appSanitize::filter($_POST);
        $_POST['max'] = $this->modelControleHeader->countRows();
        return $this->modelControleHeader->listObj();
    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        return $this->modelControleHeader->listObj();
    }

    public function loadObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        return $this->modelControleHeader->loadObj();
    }

    public function updateObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $this->modelControleHeader->updateObj();
    }
}
