<?php
require_once('lib/appController.php');
require_once('app/model/mferramenta2.php');
require_once('app/view/formsferramenta2.php');

class cferramenta2 extends appController {

    private $modelferramenta2 = null;

    public function __construct(){
        $this->modelferramenta2 = new mferramenta2();
    }

    public function main(){
        $this->render('vferramenta2');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsferramenta2();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsferramenta2();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsferramenta2();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];
        if($_POST['id']>0) {
            $_POST['idferramenta'] = $_POST['id'];
            $this->modelferramenta2->updateObj();
            return;
        }
        $this->modelferramenta2->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $this->modelferramenta2->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        return $this->modelferramenta2->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        return $this->modelferramenta2->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $this->modelferramenta2->updateObj();
    }
}
