<?php
require_once('lib/appConexao.php');

class mControleHeader extends appConexao implements gridInterface {

    private $id;
    private $campo;
    private $url;
    private $icone;
    private $nivel;
    private $ordem;
    private $super;
    private $fkFerramenta;
    private $ativo;
    private $imagem;
    private $dataIn;

    public function __construct(){
        $this->id;
        $this->campo;
        $this->url;
        $this->icone;
        $this->nivel;
        $this->ordem;
        $this->super;
        $this->fkFerramenta;
        $this->ativo;
        $this->imagem;
        $this->dataIn;
    }

    public function getId(){
        return $this->id;
    }

    public function getCampo(){
        return $this->campo;
    }

    public function getUrl(){
        return $this->url;
    }

    public function getIcone(){
        return $this->icone;
    }

    public function getNivel(){
        return $this->nivel;
    }

    public function getOrdem(){
        return $this->ordem;
    }

    public function getSuper(){
        return $this->super;
    }

    public function getFkFerramenta(){
        return $this->fkFerramenta;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getAtivoTratado(){
         if($this->getAtivo()==1){
             return 'Sim';
         }else
             return 'Não';
    }

    public function getImagem(){
        return $this->imagem;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setCampo($Campo){
        $this->campo = $Campo;
    }

    public function setUrl($Url){
        $this->url = $Url;
    }

    public function setIcone($Icone){
        $this->icone = $Icone;
    }

    public function setNivel($Nivel){
        $this->nivel = $Nivel;
    }

    public function setOrdem($Ordem){
        $this->ordem = $Ordem;
    }

    public function setSuper($Super){
        $this->super = $Super;
    }

    public function setFkFerramenta($FkFerramenta){
        $this->fkFerramenta = $FkFerramenta;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setImagem($Imagem){
        $this->imagem = $Imagem;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from CONTROLE_HEADER";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($campo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if(strcmp($url, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="URL LIKE '%$url%' ";
            $verif = true;
        }
        if(strcmp($icone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($ordem, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if(strcmp($super, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="SUPER LIKE '%$super%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($imagem, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="IMAGEM LIKE '%$imagem%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $sql = "INSERT INTO CONTROLE_HEADER ([CAMPO],[URL],[ICONE],[NIVEL],[ORDEM],[SUPER],[FK_FERRAMENTA],[ATIVO],[IMAGEM],[DATA_IN])";
        $sql .=" VALUES ('$campo','$url','$icone','$nivel','$ordem','$super','$fkFerramenta','$ativo','$imagem','$dataIn')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM CONTROLE_HEADER WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($campo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="CAMPO = '$campo' ";
            $verif = true;
        }
        if(strcmp($url, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="URL = '$url' ";
            $verif = true;
        }
        if(strcmp($icone, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ICONE = '$icone' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="NIVEL = '$nivel' ";
            $verif = true;
        }
        if(strcmp($ordem, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ORDEM = '$ordem' ";
            $verif = true;
        }
        if(strcmp($super, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="SUPER = '$super' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_FERRAMENTA = '$fkFerramenta' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($imagem, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="IMAGEM = '$imagem' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CAMPO ";
        $sql .= ",";
        $sql .= "URL ";
        $sql .= ",";
        $sql .= "ICONE ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "ORDEM ";
        $sql .= ",";
        $sql .= "SUPER ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "IMAGEM ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM CONTROLE_HEADER ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($campo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if(strcmp($url, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="URL LIKE '%$url%' ";
            $verif = true;
        }
        if(strcmp($icone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($ordem, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if(strcmp($super, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="SUPER LIKE '%$super%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($imagem, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="IMAGEM LIKE '%$imagem%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  CONTROLE_HEADER";
        $sql .=" SET ";
        $sql .="CAMPO = '$campo' ";
        $sql .=" , ";
        $sql .="URL = '$url' ";
        $sql .=" , ";
        $sql .="ICONE = '$icone' ";
        $sql .=" , ";
        $sql .="NIVEL = '$nivel' ";
        $sql .=" , ";
        $sql .="ORDEM = '$ordem' ";
        $sql .=" , ";
        $sql .="SUPER = '$super' ";
        $sql .=" , ";
        $sql .="FK_FERRAMENTA = '$fkFerramenta' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="IMAGEM = '$imagem' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $campo = $_POST['campo'];
        $url = $_POST['url'];
        $icone = $_POST['icone'];
        $nivel = $_POST['nivel'];
        $ordem = $_POST['ordem'];
        $super = $_POST['super'];
        $fkFerramenta = $_POST['fkferramenta'];
        $ativo = $_POST['ativo'];
        $imagem = $_POST['imagem'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CAMPO ";
        $sql .= ",";
        $sql .= "URL ";
        $sql .= ",";
        $sql .= "ICONE ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "ORDEM ";
        $sql .= ",";
        $sql .= "SUPER ";
        $sql .= ",";
        $sql .= "FK_FERRAMENTA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "IMAGEM ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM CONTROLE_HEADER ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($campo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CAMPO LIKE '%$campo%' ";
            $verif = true;
        }
        if(strcmp($url, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="URL LIKE '%$url%' ";
            $verif = true;
        }
        if(strcmp($icone, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ICONE LIKE '%$icone%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($ordem, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ORDEM LIKE '%$ordem%' ";
            $verif = true;
        }
        if(strcmp($super, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="SUPER LIKE '%$super%' ";
            $verif = true;
        }
        if(strcmp($fkFerramenta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_FERRAMENTA LIKE '%$fkFerramenta%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($imagem, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="IMAGEM LIKE '%$imagem%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mControleHeader();
        $o->setId($param[0]);
        $o->setCampo($param[1]);
        $o->setUrl($param[2]);
        $o->setIcone($param[3]);
        $o->setNivel($param[4]);
        $o->setOrdem($param[5]);
        $o->setSuper($param[6]);
        $o->setFkFerramenta($param[7]);
        $o->setAtivo($param[8]);
        $o->setImagem($param[9]);
        $o->setDataIn($param[10]);

        return $o;

    }
}