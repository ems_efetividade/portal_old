<?php
require_once('lib/appConexao.php');

class mferramenta2 extends appConexao implements gridInterface {

    private $idFerramenta;
    private $nivel;
    private $tipoAcesso;
    private $ferramenta;
    private $caminho;
    private $descricao;
    private $target;
    private $empresa;
    private $ativo;

    public function __construct(){
        $this->idFerramenta;
        $this->nivel;
        $this->tipoAcesso;
        $this->ferramenta;
        $this->caminho;
        $this->descricao;
        $this->target;
        $this->empresa;
        $this->ativo;
    }

    public function getIdFerramenta(){
        return $this->idFerramenta;
    }

    public function getNivel(){
        return $this->nivel;
    }

    public function getTipoAcesso(){
        return $this->tipoAcesso;
    }

    public function getFerramenta(){
        return $this->ferramenta;
    }

    public function getCaminho(){
        return $this->caminho;
    }

    public function getDescricao(){
        return $this->descricao;
    }

    public function getTarget(){
        return $this->target;
    }

    public function getEmpresa(){
        return $this->empresa;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function setIdFerramenta($IdFerramenta){
        $this->idFerramenta = $IdFerramenta;
    }

    public function setNivel($Nivel){
        $this->nivel = $Nivel;
    }

    public function setTipoAcesso($TipoAcesso){
        $this->tipoAcesso = $TipoAcesso;
    }

    public function setFerramenta($Ferramenta){
        $this->ferramenta = $Ferramenta;
    }

    public function setCaminho($Caminho){
        $this->caminho = $Caminho;
    }

    public function setDescricao($Descricao){
        $this->descricao = $Descricao;
    }

    public function setTarget($Target){
        $this->target = $Target;
    }

    public function setEmpresa($Empresa){
        $this->empresa = $Empresa;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $sql = "select count(id) from FERRAMENTA2";
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID_FERRAMENTA LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($tipoAcesso, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TIPO_ACESSO LIKE '%$tipoAcesso%' ";
            $verif = true;
        }
        if(strcmp($ferramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FERRAMENTA LIKE '%$ferramenta%' ";
            $verif = true;
        }
        if(strcmp($caminho, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CAMINHO LIKE '%$caminho%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($target, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TARGET LIKE '%$target%' ";
            $verif = true;
        }
        if(strcmp($empresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="EMPRESA LIKE '%$empresa%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $sql = "INSERT INTO FERRAMENTA2 ([NIVEL],[TIPO_ACESSO],[FERRAMENTA],[CAMINHO],[DESCRICAO],[TARGET],[EMPRESA],[ATIVO])";
        $sql .=" VALUES ('$nivel','$tipoAcesso','$ferramenta','$caminho','$descricao','$target','$empresa','$ativo')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $sql = "DELETE FROM FERRAMENTA2 WHERE ";

        $verif = false;
        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID_FERRAMENTA = '$idFerramenta' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="NIVEL = '$nivel' ";
            $verif = true;
        }
        if(strcmp($tipoAcesso, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TIPO_ACESSO = '$tipoAcesso' ";
            $verif = true;
        }
        if(strcmp($ferramenta, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FERRAMENTA = '$ferramenta' ";
            $verif = true;
        }
        if(strcmp($caminho, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="CAMINHO = '$caminho' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DESCRICAO = '$descricao' ";
            $verif = true;
        }
        if(strcmp($target, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TARGET = '$target' ";
            $verif = true;
        }
        if(strcmp($empresa, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="EMPRESA = '$empresa' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID_FERRAMENTA ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "TIPO_ACESSO ";
        $sql .= ",";
        $sql .= "FERRAMENTA ";
        $sql .= ",";
        $sql .= "CAMINHO ";
        $sql .= ",";
        $sql .= "DESCRICAO ";
        $sql .= ",";
        $sql .= "TARGET ";
        $sql .= ",";
        $sql .= "EMPRESA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= " FROM FERRAMENTA2 ";

        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID_FERRAMENTA LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($tipoAcesso, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TIPO_ACESSO LIKE '%$tipoAcesso%' ";
            $verif = true;
        }
        if(strcmp($ferramenta, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FERRAMENTA LIKE '%$ferramenta%' ";
            $verif = true;
        }
        if(strcmp($caminho, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="CAMINHO LIKE '%$caminho%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($target, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TARGET LIKE '%$target%' ";
            $verif = true;
        }
        if(strcmp($empresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="EMPRESA LIKE '%$empresa%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];
        $sql = "UPDATE  FERRAMENTA2";
        $sql .=" SET ";
        $sql .="NIVEL = '$nivel' ";
        $sql .=" , ";
        $sql .="TIPO_ACESSO = '$tipoAcesso' ";
        $sql .=" , ";
        $sql .="FERRAMENTA = '$ferramenta' ";
        $sql .=" , ";
        $sql .="CAMINHO = '$caminho' ";
        $sql .=" , ";
        $sql .="DESCRICAO = '$descricao' ";
        $sql .=" , ";
        $sql .="TARGET = '$target' ";
        $sql .=" , ";
        $sql .="EMPRESA = '$empresa' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" WHERE ID_FERRAMENTA = '$idFerramenta'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $idFerramenta = $_POST['idferramenta'];
        $nivel = $_POST['nivel'];
        $tipoAcesso = $_POST['tipoacesso'];
        $ferramenta = $_POST['ferramenta'];
        $caminho = $_POST['caminho'];
        $descricao = $_POST['descricao'];
        $target = $_POST['target'];
        $empresa = $_POST['empresa'];
        $ativo = $_POST['ativo'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID_FERRAMENTA ";
        $sql .= ",";
        $sql .= "NIVEL ";
        $sql .= ",";
        $sql .= "TIPO_ACESSO ";
        $sql .= ",";
        $sql .= "FERRAMENTA ";
        $sql .= ",";
        $sql .= "CAMINHO ";
        $sql .= ",";
        $sql .= "DESCRICAO ";
        $sql .= ",";
        $sql .= "TARGET ";
        $sql .= ",";
        $sql .= "EMPRESA ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID_FERRAMENTA ) as row";
        $sql .= " FROM FERRAMENTA2 ";

        if(strcmp($idFerramenta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID_FERRAMENTA LIKE '%$idFerramenta%' ";
            $verif = true;
        }
        if(strcmp($nivel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="NIVEL LIKE '%$nivel%' ";
            $verif = true;
        }
        if(strcmp($tipoAcesso, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TIPO_ACESSO LIKE '%$tipoAcesso%' ";
            $verif = true;
        }
        if(strcmp($ferramenta, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FERRAMENTA LIKE '%$ferramenta%' ";
            $verif = true;
        }
        if(strcmp($caminho, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="CAMINHO LIKE '%$caminho%' ";
            $verif = true;
        }
        if(strcmp($descricao, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DESCRICAO LIKE '%$descricao%' ";
            $verif = true;
        }
        if(strcmp($target, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TARGET LIKE '%$target%' ";
            $verif = true;
        }
        if(strcmp($empresa, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="EMPRESA LIKE '%$empresa%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mferramenta2();
        $o->setIdFerramenta($param[0]);
        $o->setNivel($param[1]);
        $o->setTipoAcesso($param[2]);
        $o->setFerramenta($param[3]);
        $o->setCaminho($param[4]);
        $o->setDescricao($param[5]);
        $o->setTarget($param[6]);
        $o->setEmpresa($param[7]);
        $o->setAtivo($param[8]);

        return $o;

    }
}