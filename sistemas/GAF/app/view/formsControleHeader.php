<?php

class formsControleHeader
{

    public function formEditar($objeto)
    {
        require_once('/../controller/cControleHeader.php');
        require_once('/../controller/cferramenta2.php');
        $control = new cControleHeader();
        $controlFerramentas = new cferramenta2();
        $_POST['id'] = '';
        $menus = $control->listAllObj();
        $ferramentas = $controlFerramentas->listObj();
        ?>
    <form id="formEditar" action="<?php print appConf::caminho ?>ControleHeader/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="campo">Nome Menu</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getCampo() ?>" name="campo"
                           id="campo">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="url">URL</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getUrl() ?>" name="url"
                           id="url">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="icone">Icone</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getIcone() ?>" name="icone"
                           id="icone">
                </div>
            </div>
            <div class="col-sm-6 text-left" style="display: none;">
                <div class="form-group form-group-sm">
                    <label for="nivel">NIVEL</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getNivel() ?>" name="nivel"
                           id="nivel">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ordem">Ordem</label>
                    <input type="number" class="form-control " value="<?php echo $objeto->getOrdem() ?>" name="ordem"
                           id="ordem">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="super">Menu Super</label>
                    <select name="super" id="super" class="form-control ">
                        <?php if ($objeto->getSuper() == 0) {
                            echo '<option value="0" selected>Nenhum</option>';
                        } else {
                            echo '<option value="0" >Nenhum</option>';
                        }
                        ?>
                        <?php
                        foreach ($menus as $menu) {
                            ?>
                            <option value="<?php echo $menu->getId(); ?>"
                                <?php
                                if ($objeto->getSuper() == $menu->getId()) {
                                    echo 'selected';
                                }
                                ?>
                            >
                                <?php echo $menu->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fkFerramenta">Ferramenta</label>
                    <select name="fkferramenta" id="fkferramenta" class="form-control ">
                        <?php
                        if ($objeto->getFkFerramenta() == '-1') {
                            echo '<option value="-1" selected>Barra Inicio</option>';
                        } else {
                            echo '<option value="-1" >Barra Inicio</option>';
                        }
                        if ($objeto->getFkFerramenta() == '0') {
                            echo '<option value="0" selected>Divisor</option>';
                        } else {
                            echo '<option value="0">Divisor</option>';
                        }
                        if ($objeto->getFkFerramenta() == '-2') {
                            echo '<option value="-2" selected>Externo</option>';
                        } else {
                            echo '<option value="-2">Externo</option>';
                        }

                        foreach ($ferramentas as $ferramenta) {
                            ?>
                            <option <?php
                            if ($objeto->getFkFerramenta() == $ferramenta->getIdFerramenta()) {
                                echo 'selected';
                            }
                            ?> value="<?php echo $ferramenta->getIdFerramenta() ?>">
                                <?php echo $ferramenta->getFerramenta() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">Ativo</label>
                    <select id="ativo" name="ativo" class="form-control">
                        <?php
                        if ($objeto->getAtivo() == 0) {
                            echo '<option selected value="0">Inativo</option>';
                        } else {
                            echo '<option value="0">Inativo</option>';
                        }
                        if ($objeto->getAtivo() == 1) {
                            echo '<option selected value="1">Ativo</option>';
                        } else {
                            echo '<option value="1">Ativo</option>';
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="imagem">Imagem</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getImagem() ?>" name="imagem"
                           id="imagem">
                </div>
            </div>
            <div class="col-sm-6 text-left" style="display: none;">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DataIn</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getDataIn() ?>" name="datain"
                           id="dataIn">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formFerramenta(){
        require_once('/../controller/cferramenta2.php');
        $controlFerramentas = new cferramenta2();
        $controlFerramentas->controlSwitch('cadastrar');
    }

    public function formVisualizar($objeto)
    {
        require_once('/../controller/cControleHeader.php');
        require_once('/../controller/cferramenta2.php');
        $control = new cControleHeader();
        $controlFerramentas = new cferramenta2();
        $_POST['id'] = '';
        $menus = $control->listAllObj();
        $ferramentas = $controlFerramentas->listObj();
        ?>
    <form id="formEditar" action="<?php print appConf::caminho ?>ControleHeader/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="campo">Nome Menu</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getCampo() ?>"
                           name="campo"
                           id="campo">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="url">URL</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getUrl() ?>" name="url"
                           id="url">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="icone">Icone</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getIcone() ?>"
                           name="icone"
                           id="icone">
                </div>
            </div>
            <div class="col-sm-6 text-left" style="display: none;">
                <div class="form-group form-group-sm">
                    <label for="nivel">NIVEL</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getNivel() ?>"
                           name="nivel"
                           id="nivel">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ordem">Ordem</label>
                    <input readonly type="number" class="form-control " value="<?php echo $objeto->getOrdem() ?>"
                           name="ordem"
                           id="ordem">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="super">Menu Super</label>
                    <select disabled name="super" id="super" class="form-control ">
                        <?php if ($objeto->getSuper() == 0) {
                            echo '<option value="0" selected>Nenhum</option>';
                        } else {
                            echo '<option value="0" >Nenhum</option>';
                        }
                        ?>
                        <?php
                        foreach ($menus as $menu) {
                            ?>
                            <option value="<?php echo $menu->getId(); ?>"
                                <?php
                                if ($objeto->getSuper() == $menu->getId()) {
                                    echo 'selected';
                                }
                                ?>
                            >
                                <?php echo $menu->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fkFerramenta">Ferramenta</label>
                    <select disabled name="fkferramenta" id="fkferramenta" class="form-control ">
                        <?php
                        if ($objeto->getFkFerramenta() == '-1') {
                            echo '<option value="-1" selected>Barra Inicio</option>';
                        } else {
                            echo '<option value="-1" >Barra Inicio</option>';
                        }
                        if ($objeto->getFkFerramenta() == '0') {
                            echo '<option value="0" selected>Divisor</option>';
                        } else {
                            echo '<option value="0">Divisor</option>';
                        }
                        if ($objeto->getFkFerramenta() == '-2') {
                            echo '<option value="-2" selected>Externo</option>';
                        } else {
                            echo '<option value="-2">Externo</option>';
                        }

                        foreach ($ferramentas as $ferramenta) {
                            ?>
                            <option <?php
                            if ($objeto->getFkFerramenta() == $ferramenta->getIdFerramenta()) {
                                echo 'selected';
                            }
                            ?> value="<?php echo $ferramenta->getIdFerramenta() ?>">
                                <?php echo $ferramenta->getFerramenta() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">Ativo</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getAtivoTratado() ?>"
                           name="ativo"
                           id="ativo">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="imagem">Imagem</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getImagem() ?>"
                           name="imagem"
                           id="imagem">
                </div>
            </div>
            <div class="col-sm-6 text-left" style="display: none;">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DataIn</label>
                    <input readonly type="text" class="form-control " value="<?php echo $objeto->getDataIn() ?>"
                           name="datain"
                           id="dataIn">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar()
    {
        require_once('/../controller/cControleHeader.php');
        require_once('/../controller/cferramenta2.php');
        $control = new cControleHeader();
        $controlFerramentas = new cferramenta2();
        $menus = $control->listAllObj();
        $ferramentas = $controlFerramentas->listObj();
        ?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>ControleHeader/controlSwitch/salvar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="campo">Nome Menu</label>
                    <input required type="text" class="form-control " name="campo" id="campo">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="url">URL</label>
                    <input required type="text" class="form-control " name="url" id="url">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="icone">Icone</label>
                    <input required type="text" class="form-control " name="icone" id="icone">
                </div>
            </div>
            <!--div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nivel">Nível</label>
                    <input required type="text" class="form-control " name="nivel" id="nivel" >
                </div>
            </div-->
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ordem">Ordem Exibição</label>
                    <input required type="number" class="form-control " name="ordem" id="ordem">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="super">Menu Super</label>
                    <select name="super" id="super" class="form-control ">
                        <option value="0">Nenhum</option>
                        <?php
                        foreach ($menus as $menu) {
                            ?>
                            <option value="<?php echo $menu->getId() ?>">
                                <?php echo $menu->getCampo() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fkFerramenta">Ferramenta</label>
                    <select name="fkferramenta" id="fkferramenta" class="form-control ">
                        <option value="-1">Barra Inicio</option>
                        <option value="0">Divisor</option>
                        <option value="-2">Externo</option>
                        <?php
                        foreach ($ferramentas as $ferramenta) {
                            ?>
                            <option value="<?php echo $ferramenta->getIdFerramenta() ?>">
                                <?php echo $ferramenta->getFerramenta() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">Ativo</label>
                    <select id="ativo" name="ativo" class="form-control">
                        <option value="0">Inativo</option>
                        <option value="1">Ativo</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="imagem">Imagem</label>
                    <input required type="text" class="form-control " name="imagem" id="imagem">
                </div>
            </div>
            <!--div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataIn">Data</label>
                    <input required type="text" class="form-control " name="datain" id="dataIn" >
                </div>
            </div-->
        </div>
        </form><?php
    }
}
