<?php

class formsferramenta2
{

    public function formEditar($objeto)
    {
        ?>
    <form id="formEditar" action="<?php print appConf::caminho ?>Ferramenta2/controlSwitch/atualizar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="idFerramenta">IDFERRAMENTA</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getIdFerramenta() ?>"
                           name="idferramenta" id="idFerramenta">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nivel">NIVEL</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getNivel() ?>" name="nivel"
                           id="nivel">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="tipoAcesso">TIPOACESSO</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getTipoAcesso() ?>"
                           name="tipoacesso" id="tipoAcesso">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ferramenta">FERRAMENTA</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getFerramenta() ?>"
                           name="ferramenta" id="ferramenta">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="caminho">CAMINHO</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getCaminho() ?>" name="caminho"
                           id="caminho">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="descricao">DESCRICAO</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getDescricao() ?>"
                           name="descricao" id="descricao">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="target">TARGET</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getTarget() ?>" name="target"
                           id="target">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="empresa">EMPRESA</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getEmpresa() ?>" name="empresa"
                           id="empresa">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">ATIVO</label>
                    <input type="text" class="form-control " value="<?php echo $objeto->getAtivo() ?>" name="ativo"
                           id="ativo">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($objeto)
    {
        ?>
        <form id="formVisualizar">
        <div class="row">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="idFerramenta">IDFERRAMENTA</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getIdFerramenta() ?>" name="idferramenta" id="idFerramenta">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nivel">NIVEL</label>
                    <input type="text" disabled class="form-control " readonly value="<?php echo $objeto->getNivel() ?>"
                           name="nivel" id="nivel">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="tipoAcesso">TIPOACESSO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getTipoAcesso() ?>" name="tipoacesso" id="tipoAcesso">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ferramenta">FERRAMENTA</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getFerramenta() ?>" name="ferramenta" id="ferramenta">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="caminho">CAMINHO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getCaminho() ?>" name="caminho" id="caminho">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="descricao">DESCRICAO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getDescricao() ?>" name="descricao" id="descricao">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="target">TARGET</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getTarget() ?>" name="target" id="target">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="empresa">EMPRESA</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getEmpresa() ?>" name="empresa" id="empresa">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">ATIVO</label>
                    <input type="text" disabled class="form-control " readonly value="<?php echo $objeto->getAtivo() ?>"
                           name="ativo" id="ativo">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar()
    {
        require_once('/../controller/cferramenta2.php');
        $control = new cferramenta2();
        $ferramentas = $control->listObj();

        ?>
    <form id="formSalvarFerramenta"
          action="<?php print appConf::caminho ?>ControleHeader/controlSwitch/salvarFerramenta">
        <div class="row">
            <div class="col-sm-12 text-left">
                <ul class="nav nav-tabs">
                    <li id="tabCadastrar" class="active" style="cursor: pointer"><a>Cadastrar</a></li>
                    <li id="tabEditar" style="cursor: pointer"><a>Editar</a></li>
                </ul>
                <br>
                <script>
                    $('#tabEditar').click(function () {
                        $('#opcaoEditar').show();
                        $('#tabEditar').addClass('active');
                        $('#tabCadastrar').removeClass('active');
                    });
                    $('#tabCadastrar').click(function () {
                        $('#opcaoEditar').hide();
                        $('#tabCadastrar').addClass('active');
                        $('#tabEditar').removeClass('active');
                        $('#idferramentaHidden').val('0');
                        $('#nomeFerramenta').val('');
                        $('#caminho').val('');
                        $('#ferramentaEditar').val(0);

                    });
                    $('#ferramentaEditar').change(function () {
                        id_ferramenta = $('#ferramentaEditar').val();
                        $('#idferramentaHidden').val(id_ferramenta);
                        formData = new FormData();
                        formData.append('idferramenta', id_ferramenta);
                        $.ajax({
                            type: "POST",
                            url: "<?php print appConf::caminho ?>ControleHeader/controlSwitch/loadFerramenta",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                obj = $.parseJSON(retorno);
                                document.getElementById('nomeFerramenta').value = obj.ferramenta;
                                document.getElementById('caminho').value = obj.caminho;
                            }
                        });
                    });

                </script>
            </div>
            <input type="hidden" value="0" id="idferramentaHidden" name="idferramenta">
            <div class="col-sm-12 text-left" id="opcaoEditar" style="display: none">
                <div class="form-group form-group-sm">
                    <label for="ferramenta">Ferramentas</label>
                    <select class="form-control" id="ferramentaEditar">
                        <option value="0">Selecione...</option>
                        <?php
                        foreach ($ferramentas as $ferramenta) {
                            ?>
                            <option value="<?php echo $ferramenta->getIdFerramenta() ?>">
                                <?php echo $ferramenta->getFerramenta() ?>
                            </option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ferramenta">Nome Ferramenta</label>
                    <input required type="text" class="form-control " name="ferramenta" id="nomeFerramenta">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="caminho">Caminho Fisico</label>
                    <small>/sistema/...</small>
                    <input required type="text" class="form-control " name="caminho" id="caminho">
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <div class="form-group form-group-sm">
                    <span onclick="enviar()" class="btn btn-success">Salvar</span>
                    <span onclick="cancelar()" class="btn btn-danger">Cancelar</span>
                </div>
            </div>
            <script>
                function enviar() {
                    id = document.getElementById('idferramentaHidden').value;
                    nome = document.getElementById('nomeFerramenta').value;
                    caminho = document.getElementById('caminho').value;
                    form = document.getElementById('formSalvarFerramenta').action;
                    if (nome.length < 1) {
                        msgBox('Informe o nome da ferramenta');
                    }
                    else if (caminho.length < 1) {
                        msgBox('Informe o caminho da Ferramenta');
                    } else {
                        formData = new FormData();
                        formData.append('ferramenta', nome);
                        formData.append('caminho', caminho);
                        formData.append('id', id);
                        $.ajax({
                            type: "POST",
                            url: form,
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                if (retorno == 1) {
                                    location.reload();
                                }
                            }
                        });
                    }
                }

                function cancelar() {
                    location.reload();
                }
            </script>
        </div>
        </form><?php
    }
}
