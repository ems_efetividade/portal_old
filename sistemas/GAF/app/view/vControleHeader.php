<?php
require_once('header.php');

class vControleHeader extends gridView{

    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        $cols = array('ID','Nome Menu','URL','Ativo');
        $colsPesquisaNome = array('Nome Menu','URL');
        $colsPesquisa = array('campo','url');
        $array_metodo = array('Id','Campo','Url','AtivoTratado');
        gridView::setGrid_sub_titulo('Gerenciador integrado de ferramentas e menus disponíveis');
        gridView::setGrid_titulo('Gerenciador de Ferramentas');
        gridView::setGrid_cols($cols);
        gridView::setGrid_botao_topo(array(" Ferramentas","CadastrarFerramenta","btn_ferramenta"," btn-success","cog",0,1));
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('ControleHeader');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }

}