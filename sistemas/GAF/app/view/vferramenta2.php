<?php
require_once('header.php');

class vferramenta2 extends gridView{


    public function __construct(){
        $this->viewGrid();
    }


    public function viewGrid() {

        $cols = array('IDFERRAMENTA','NIVEL','TIPOACESSO','FERRAMENTA','CAMINHO','DESCRICAO','TARGET','EMPRESA','ATIVO');
        $colsPesquisaNome = array('IDFERRAMENTA','NIVEL','TIPOACESSO','FERRAMENTA','CAMINHO','DESCRICAO','TARGET','EMPRESA','ATIVO');
        $colsPesquisa = array('idFerramenta','nivel','tipoAcesso','ferramenta','caminho','descricao','target','empresa','ativo');
        $array_metodo = array('IdFerramenta','Nivel','TipoAcesso','Ferramenta','Caminho','Descricao','Target','Empresa','Ativo');
        gridView::setGrid_titulo('ferramenta2');
        gridView::setGrid_sub_titulo('ferramenta2');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('ferramenta2');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();

    }

}