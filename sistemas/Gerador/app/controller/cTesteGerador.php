<?php
require_once('lib/appController.php');
require_once('app/model/mtestegerador.php');
require_once('app/view/formstestegerador.php');

class ctestegerador extends appController {

    private $modeltestegerador = null;

    public function __construct(){
        $this->modeltestegerador = new mtestegerador();
    }

    public function main(){
        $this->render('vtestegerador');
    }

    public function controlSwitch($acao){
        switch ($acao){
            case 'cadastrar':
                $forms = new formstestegerador();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formstestegerador();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formstestegerador();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $this->modeltestegerador->save();

    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $this->modeltestegerador->delete();

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        return $this->modeltestegerador->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        return $this->modeltestegerador->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $this->modeltestegerador->updateObj();
    }
}
