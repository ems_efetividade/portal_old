<?php
require_once('lib/appConexao.php');

class mtestegerador extends appConexao implements gridInterface {

    private $id;
    private $nome;
    private $endereco;
    private $complemento;
    private $telefone;

    public function __construct(){
        $this->id;
        $this->nome;
        $this->endereco;
        $this->complemento;
        $this->telefone;
    }

    public function getId(){
        return $this->id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getEndereco(){
        return $this->endereco;
    }

    public function getComplemento(){
        return $this->complemento;
    }

    public function getTelefone(){
        return $this->telefone;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setNome($Nome){
        $this->nome = $Nome;
    }

    public function setEndereco($Endereco){
        $this->endereco = $Endereco;
    }

    public function setComplemento($Complemento){
        $this->complemento = $Complemento;
    }

    public function setTelefone($Telefone){
        $this->telefone = $Telefone;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $sql = "select count(id) from TESTEGERADOR";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="nome IN ('$nome') ";
            $verif = true;
        }
        if(strcmp($endereco, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="endereco IN ('$endereco') ";
            $verif = true;
        }
        if(strcmp($complemento, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="complemento IN ('$complemento') ";
            $verif = true;
        }
        if(strcmp($telefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="telefone LIKE '%$telefone%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $sql = "INSERT INTO TESTEGERADOR ([nome],[endereco],[complemento],[telefone])";
        $sql .=" VALUES ('$nome','$endereco','$complemento','$telefone')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $sql = "DELETE FROM TESTEGERADOR WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="id = '$id' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="nome = '$nome' ";
            $verif = true;
        }
        if(strcmp($endereco, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="endereco = '$endereco' ";
            $verif = true;
        }
        if(strcmp($complemento, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="complemento = '$complemento' ";
            $verif = true;
        }
        if(strcmp($telefone, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="telefone = '$telefone' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "id ";
        $sql .= ",";
        $sql .= "nome ";
        $sql .= ",";
        $sql .= "endereco ";
        $sql .= ",";
        $sql .= "complemento ";
        $sql .= ",";
        $sql .= "telefone ";
        $sql .= " FROM TESTEGERADOR ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="nome IN ('$nome') ";
            $verif = true;
        }
        if(strcmp($endereco, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="endereco LIKE '%$endereco%' ";
            $verif = true;
        }
        if(strcmp($complemento, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="complemento LIKE '%$complemento%' ";
            $verif = true;
        }
        if(strcmp($telefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="telefone LIKE '%$telefone%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];
        $sql = "UPDATE  TESTEGERADOR";
        $sql .=" SET ";
        $sql .="nome = '$nome' ";
        $sql .=" , ";
        $sql .="endereco = '$endereco' ";
        $sql .=" , ";
        $sql .="complemento = '$complemento' ";
        $sql .=" , ";
        $sql .="telefone = '$telefone' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $endereco = $_POST['endereco'];
        $complemento = $_POST['complemento'];
        $telefone = $_POST['telefone'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "id ";
        $sql .= ",";
        $sql .= "nome ";
        $sql .= ",";
        $sql .= "endereco ";
        $sql .= ",";
        $sql .= "complemento ";
        $sql .= ",";
        $sql .= "telefone ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM TESTEGERADOR ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="id LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="nome LIKE '%$nome%' ";
            $verif = true;
        }
        if(strcmp($endereco, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="endereco LIKE '%$endereco%' ";
            $verif = true;
        }
        if(strcmp($complemento, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="complemento LIKE '%$complemento%' ";
            $verif = true;
        }
        if(strcmp($telefone, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="telefone LIKE '%$telefone%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mtestegerador();
        $o->setId($param[0]);
        $o->setNome($param[1]);
        $o->setEndereco($param[2]);
        $o->setComplemento($param[3]);
        $o->setTelefone($param[4]);

        return $o;

    }
}