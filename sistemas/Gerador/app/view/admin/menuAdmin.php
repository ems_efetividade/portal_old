
<br>
<img class="center-block" src="../../../../../public/img/logo_ems2.png" alt=""/>
<h4 class="text-center">
    <small>Aceite de Documentos</small>
</h4>
<hr>

<ul class="nav nav-pills nav-stacked">
  <li role="presentation" class="<?php echo $menu1 ?>">
      <a href="<?php echo appConf::caminho ?>admin"><span class="glyphicon glyphicon-file"></span> Documentos</a>
  </li>
  <li role="presentation" class="<?php echo $menu2 ?>">
      <a href="<?php echo appConf::caminho ?>admin/assinaturas"><span class="glyphicon glyphicon-pencil"></span> Assinaturas</a>
  </li>
</ul>
