<?php require_once('includes.php'); ?>

<style>
    
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            
           
            
            <form id="formDocumento" enctype="multipart/form-data" action="<?php echo appConf::caminho ?>admin/salvarDocumento" method="post" >
            
            <div class="row">
                <div class="col-lg-9">
                     <h2><b><span class="glyphicon glyphicon-ok-sign"></span> Aceite de Documentos</b> <small> - Editar/Salvar Documento </small></h2>
                </div>
                <div class="col-lg-3 text-right">
                    <br>
                    <p>
                        <a href="<?php echo appConf::caminho ?>admin" class="btn btn-sm btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>  &nbsp;
                        <button type="button" id="btnSalvarDocumento" class="btnSalvarDocumento pull-right btn-sm btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar Documento</button>
                    </p>
                </div>
            </div>
           <hr>
            
            
                
                <input type="hidden" name="txtIdDocumento" value="<?php echo $documento->getIdDocumento() ?>" />
                <input type="file" id="filDocumento" name="filDocumento" style="display: none" />
                
                <div class="row">
                
                    <div class="col-lg-12">                
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nome do Documento</label>
                            <input type="text" name="txtNomeDocumento" class="form-control" value="<?php echo $documento->getTitulo() ?>" placeholder="">
                        </div>
                    </div>
                    <div class="col-lg-12">                
                        <div class="form-group">
                            <label for="exampleInputEmail1">Arquivo</label>
                            <div class="input-group">
                                <input type="text" readonly id="txtNomeArquivo" name="txtNomeArquivo" value="<?php echo $documento->getNomeArquivo() ?>" class="form-control" placeholder="">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="btnSelecionarArquivo" type="button">Selecionar Arquivo</button>
                                </span>
                              </div><!-- /input-group -->
                        </div>
                    </div>
                    
                    <div class="col-lg-2">
                        <?php $dtInicio = explode(" ", $documento->getDataInicio()); ?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Data Início</label>
                            <input type="text" name="txtDataInicio" class="data form-control" value="<?php echo appFunction::formatarData($dtInicio[0]) ?>" placeholder="___/___/___">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hora Início</label>
                            <select name="cmbHoraInicio" class="form-control">
                                <?php for($i=0;$i<=47;$i++) { $hora = date('H:i:s', ((1488769200) + (1800*$i))) ?>
                                <option value="<?php echo $hora ?>" <?php echo (($hora.'.000' == $dtInicio[1]) ? 'selected="selected"' : '') ?> ><?php echo $hora ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <?php $dtExpirar = explode(" ", $documento->getDataExpirar()); ?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Data Expirar</label>
                            <input type="text" name="txtDataExpirar" class="data form-control" value="<?php echo appFunction::formatarData($dtExpirar[0]) ?>" placeholder="___/___/___">
                        </div>
                    </div>
                    <div class="col-lg-2">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Hora Expirar</label>
                            <select name="cmbHoraExpirar" class="form-control">
                                <?php for($i=0;$i<=47;$i++) { $hora = date('H:i:s', ((1488769200) + (1800*$i))) ?>
                                <option  value="<?php echo $hora ?>" <?php echo (($hora.'.000' == $dtExpirar[1]) ? 'selected="selected"' : '') ?>><?php echo $hora ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-2">        
                        <?php //if($documento->getIdDocumento() != 0) { ?>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Ativo?</label>
                            <!-- <input type="text" name="txtAtivo" class="form-control" value="<?php echo $documento->getAtivo() ?>" placeholder=""> -->
                            
                            
                            <select name="txtAtivo" class="form-control">
                                <option value=""></option>
                                <option value="1" <?php echo (($documento->getAtivo() == '1') ? 'selected' : "") ?>>SIM</option>
                                <option value="0" <?php echo (($documento->getAtivo() == '0') ? 'selected' : "") ?>>NAO</option>
                            </select>
                            
                        </div>
                        <?php //} ?>
                    </div>
                    
                </div>
                <br>
                
           
                <div class="wsell wsell-sm">
                    <p><b>Documento visível para:</b></p>
                </div>
                
                <div class="row">
                    
                    <div class="col-lg-8">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div class="row">
                                    <?php $permissoes = $documento->permissao->selecionar();  ?>
                                    <?php foreach($perfis as $perfil) { ?>
                                    <div class="col-lg-6">
                                        <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="chkPerfil[]" value="<?php echo $perfil->getIdPerfil() ?>"  <?php echo ((in_array($perfil->getIdPerfil(), $permissoes['PERFIL'])) ? 'checked="checked"' :  ''); ?> > <?php echo $perfil->getPerfil() ?>
                                        </label>
                                      </div>
                                    </div>
                                    <?php } ?>


                                </div>
                                </div>
                        </div>
                    </div>
                    
                    <div class="col-lg-4">
                        
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <?php $permissoes = $documento->permissao->selecionar();  ?>
                            <?php foreach($linhas as $linha) { ?>
                            <div class="col-lg-12">
                                <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="chkLinha[]" <?php echo ((in_array($linha->getIdLinha(), $permissoes['LINHA'])) ? 'checked="checked"' :  ''); ?> value="<?php echo $linha->getIdLinha() ?>"> <?php echo $linha->getNome() ?>
                                </label>
                              </div>
                            </div>
                            <?php } ?>
                            
                            
                        </div>
                        </div>
                </div>
                    </div>
                </div>
                
                
                
                
                
                <br>
                <button type="button" id="btnSalvarDocumento" class="btnSalvarDocumento pull-right btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar Documento</button>
                <a href="<?php echo appConf::caminho ?>admin" class="btn btn-default"><span class="glyphicon glyphicon-arrow-left"></span> Voltar</a>
            </form>
            <br><br>
            
        </div>
    </div>
</div>

<div class="modal fade" id="modalErro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo appConf::caminho ?>public/js/jquery.mask.js" type="text/javascript"></script>
<script>resizePanelLeft();</script>
<script>
    
    $('.data').mask('00/00/0000');
    
    
    $('#btnSelecionarArquivo').unbind().click(function(e) {
        $('#filDocumento').click();
        
        $('#filDocumento').change(function(e) {
            
            fileName = $(this).val().replace(/.*(\/|\\)/, '');
            $('#txtNomeArquivo').val(fileName);
            
        });
    });
    
    
    
    
    $('.btnSalvarDocumento').unbind().click(function(e) {
        $.ajax({
            type: "POST",
            url: $('#formDocumento').attr('action'),
            data: new FormData($('#formDocumento')[0]),
            contentType: false,
            processData: false,
            success: function(retorno) {
                if($.trim(retorno) != "") {
                    $('#modalErro .modal-body').html(retorno);
                    $('#modalErro').modal('show');
                } else {
                    window.location = '<?php echo appConf::caminho ?>admin';
                }
            }
        });
    });
</script>