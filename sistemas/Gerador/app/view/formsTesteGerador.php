<?php
class formstestegerador{

    public function formEditar($objeto){?>
    <form id="formEditar" action="<?php print appConf::caminho ?>Testegerador/controlSwitch/atualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">NOME</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="endereco">ENDERECO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getEndereco()?>" name="endereco" id="endereco" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="complemento">COMPLEMENTO</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getComplemento()?>" name="complemento" id="complemento" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="telefone">TELEFONE</label>
                    <input type="text"  class="form-control "   value="<?php echo $objeto->getTelefone()?>" name="telefone" id="telefone" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formVisualizar($objeto){?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId()?>">                  <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="nome">NOME</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getNome()?>" name="nome" id="nome" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="endereco">ENDERECO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getEndereco()?>" name="endereco" id="endereco" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="complemento">COMPLEMENTO</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getComplemento()?>" name="complemento" id="complemento" >
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="telefone">TELEFONE</label>
                    <input type="text" disabled class="form-control "  readonly value="<?php echo $objeto->getTelefone()?>" name="telefone" id="telefone" >
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar(){?>
    <form id="formSalvar" action="<?php print appConf::caminho ?>Testegerador/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="nome">NOME</label>
                        <input required type="text" class="form-control " name="nome" id="nome" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="endereco">ENDERECO</label>
                        <input required type="text" class="form-control " name="endereco" id="endereco" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">COMPLEMENTO</label>
                        <input required type="text" class="form-control " name="complemento" id="complemento" >
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="telefone">TELEFONE</label>
                        <input required type="text" class="form-control " name="telefone" id="telefone" >
                    </div>
                </div>
            </div>
        </form><?php
    }
}
