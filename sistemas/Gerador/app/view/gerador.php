<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */
require_once('header.php');
//require_once('app/components/gerador.html');

class gerador extends gridView {

    public function __construct(){
        $this->viewGrid();
    }

    public function viewGrid() {
        gridView::setGrid_titulo("Gerador De Sistemas");
        gridView::setGrid_sub_titulo("Informe os campos desejados e clique eu salvar");
        gridView::setGrid_cadastrar(0);
        gridView::renderGrid();
        ?>
        <!--script>
            let arrayCampos=[];
            function adicionarLinha(){
                today=new Date();
                h=today.getHours();
                m=today.getMinutes();
                s=today.getSeconds();
                c=today.getMilliseconds();
                data = h+"_"+"_"+m+"_"+s+"_"+c;
                linha = '<div class="row" id="'+data+'">\n' +
                    '                    <div class="col-sm-3 text-left">\n' +
                    '                        <div class="form-group form-group-sm">\n' +
                    '                            <label for="id">Nome Campo</label>\n' +
                    '                            <input type="text" id="nome'+data+'" class="form-control ">\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                    <div class="col-sm-3 text-left">\n' +
                    '                        <div class="form-group form-group-sm">\n' +
                    '                            <label for="id">Tipo</label>\n' +
                    '                            <select id="tipo'+data+'" class="form-control">\n' +
                    '                                <option></option>\n' +
                    '                                <optgroup label="Numéricos exatos">\n' +
                    '                                    <option value="bigint">bigint</option>\n' +
                    '                                    <option value="bit">bit</option>\n' +
                    '                                    <option value="decimal">decimal</option>\n' +
                    '                                    <option value="int">int</option>\n' +
                    '                                    <option value="numeric">numeric</option>\n' +
                    '                                    <option value="smallint">smallint</option>\n' +
                    '                                    <option value="tinyint">tinyint</option>\n' +
                    '                                </optgroup>\n' +
                    '                                <optgroup label="Numéricos aproximados">\n' +
                    '                                    <option value="float">float</option>\n' +
                    '                                    <option value="real">real</option>\n' +
                    '                                </optgroup>\n' +
                    '                                <optgroup label="Data e hora">\n' +
                    '                                    <option value="date">date</option>\n' +
                    '                                    <option value="datetime2">datetime2</option>\n' +
                    '                                    <option value="time">time</option>\n' +
                    '                                    <option value="smalldatetime">smalldatetime</option>\n' +
                    '                                    <option value="datetimeoffset">datetimeoffset</option>\n' +
                    '                                    <option value="datetime">datetime</option>\n' +
                    '                                </optgroup>\n' +
                    '                                <optgroup label="Cadeias de caracteres">\n' +
                    '                                    <option value="char">char</option>\n' +
                    '                                    <option value="varchar">varchar</option>\n' +
                    '                                    <option value="text">text</option>\n' +
                    '                                </optgroup>\n' +
                    '                                <optgroup label="Cadeias de caracteres Unicode">\n' +
                    '                                    <option value="nchar">nchar</option>\n' +
                    '                                    <option value="nvarchar">nvarchar</option>\n' +
                    '                                    <option value="ntext">ntext</option>\n' +
                    '                                </optgroup>\n' +
                    '                                <optgroup label="Cadeias de caracteres binárias">\n' +
                    '                                    <option value="binary">binary</option>\n' +
                    '                                    <option value="varbinary">varbinary</option>\n' +
                    '                                    <option value="image">image</option>\n' +
                    '                                </optgroup>\n' +
                    '                            </select>\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                    <div class="col-sm-2 text-left">\n' +
                    '                        <div class="form-group form-group-sm">\n' +
                    '                            <input type="checkbox" value="0"> <label for="id"> Tamanho  </label>\n' +
                    '                            <input type="text" id="tamanho'+data+'" class="form-control ">\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                    <div class="col-sm-4 text-left">\n' +
                    '                        <div class="form-group form-group-sm">\n' +
                    '                            <label for="id">Mais Opções</label><br>\n' +
                    '                            <input type="checkbox"  id="nulo'+data+'" >Permitir Nulo\n' +
                    '                            <input type="radio"  id="increment'+data+'" name="increment" >Identity\n' +
                    '                            <input type="radio"  id="primaria'+data+'" name="primaria">Chave Primaria\n' +
                    '                        </div>\n' +
                    '                    </div>\n' +
                    '                </div>';
                document.getElementById('conteudo').innerHTML+=linha;
                arrayCampos.push(data);
            }
            setTimeout(function(){
                gerarTabela();
            }, 3000);
            function gerarTabela() {
                nome = document.getElementById("NomeTabela").value;
                nome = "CREATE TABLE "+nome+" (\n";
                for(i=0;i<arrayCampos.length;i++){
                    tamanho = $("#tamanho"+arrayCampos[i]).val();
                    nulo = "NOT NULL";
                    ident = "";
                    prim = "";
                    if ($("#nulo"+arrayCampos[i])[0].checked) {
                        nulo = "NULL";
                    }
                    if ($("#increment"+arrayCampos[i])[0].checked) {
                        ident = "IDENTITY";
                    }
                    if ($("#primaria"+arrayCampos[i])[0].checked) {
                        prim = "Primary key";
                    }
                    if(tamanho !='') {
                        tamanho = "(" + tamanho + ")";
                    }
                    nome += ""+$("#nome"+arrayCampos[i]).val()+" "+$("#tipo"+arrayCampos[i]).val()+
                        " "+tamanho+" "+ident+" "+prim+" "+nulo+",\n";
                }
                setTimeout(function(){
                    gerarTabela();
                }, 3000);

                nome += " )";
                document.getElementById("tabela").innerText =  nome.toUpperCase();
            }
        </script>
        <div class="container jumbotron" >
            <div class="row">
                <div class="col-sm-3 text-left">
                    <div class="row">
                        <div class="col-sm-12 text-left">
                            <small id="tabela"></small>
                        </div>
                    </div>
                </div>
                <div class="col-sm-9 text-left">
                    <div class="row">
                        <div class="col-sm-10 text-left">
                            <div class="form-group form-group-sm">
                                <label for="id">Nome Tabela</label>
                                <input type="text" id="NomeTabela" class="form-control ">
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="id">.</label>
                                <button type="button" class="form-control btn btn-info" onclick="adicionarLinha()">Adicionar</button>
                            </div>
                        </div>
                    </div>
                    <form id="">
                        <div id="conteudo">
                        </div>
                    </form>
                </div>
            </div>

        </div-->

        <div class="container" >
            <div class="row" >
                <iframe style="overflow:hidden;" src="/../app/components/gerador.html"></iframe>
            </div>
        </div>
        <?php
    }
}