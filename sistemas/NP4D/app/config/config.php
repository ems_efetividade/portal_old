<?php


/* CONFIGURAÇÕES DO PROJETO */
require_once('..\\..\\lib\\appGlobalVar.php');

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);
define('SERVER_URL', $_SERVER['HTTP_HOST']);
define("APP_FOLDER", '/sistemas/NP4D/');
define("APP_NAME",   "Segmentação de Médicos");

define('APP_URL', APP_HOST.APP_FOLDER);
define('APP_CSS', APP_URL.'public/css/');
define('APP_IMG', APP_URL.'public/img/');
define('APP_JS',  APP_URL.'public/js/');

define('APP_EXPORT', ROOT.'public/upload/');

require_once HELPER_PATH . 'Functions.php';

?>