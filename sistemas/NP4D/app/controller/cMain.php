<?php

require_once('app/model/mP4D.php');
class cMain extends Controller {
    
    private $mTabela;
    private $setor;
    private $maxReg;

    private $P4D;
    
    public function __construct() {
        parent::__construct();

        $this->mTabela = new mTabela();
        $this->P4D =  new mPerfil4D();
        
        $this->setor = fnDadoSessao('setor');
        $this->maxReg = $this->mTabela->getConf('MAX_REGISTRO');
    }
    
    public function main() {

        /* P4D ANTIGO
        $this->set('audit', $this->mTabela->getAuditoria());
        $this->set('filtros', $this->mTabela->getMenu());
        $this->set('categorias', $this->mTabela->getCategoria());
        $this->set('maxReg', $this->maxReg);
        */



        $this->set('filtros', $this->P4D->getMenu());
        $this->render('main/vMain');

    }
    
    public function menuLinha($linha) {

        $dados = $this->PD4->getMenu($linha);


        print_r($dados);
        
        $produtoBU = '';
        foreach($produtos as $produto) {
            //$produtoBU = (($produto['PROD_BU'] == 1) ? $produto['PRODUTO'] : '');

            if($produtoBU == '' && $produto['PROD_BU'] == 1) {
                $produtoBU = (($produto['PROD_BU'] == 1) ? $produto['PRODUTO'] : '');
            }

            $opt .= '<option value="'.$produto['PRODUTO'].'" "'.(($produto['PROD_BU'] == 1) ? "selected" : '').'">'.$produto['PRODUTO'].'</option>';
        }
        $combo = '<select class="form-control input-sm" name="opt-produto[]">'.$opt.'</select>';

        echo json_encode($combo);
    }

    public function filtrar() {
        
        $keys = array_keys( $_POST);

        foreach($_POST as $key => $post) {
            $campo = strtoupper(str_replace('opt-', '', $key));
            $valores = "'" . implode("','", $post) . "'";

           $array[] = array(
                'campo' => $campo,
                'valores' => $valores
           );
        }

        $retorno = $this->P4D->TotalCategoria($array);

        foreach($retorno as $registro) {
            $categoria[] = array(
                'perfil' => $registro['PERFIL'],
                'qtd' => $registro['QTD'],
                'share' => $registro['SHARE']
            );
        }

        $produtos = $this->P4D->getProdutos($_POST['opt-mercado'][0]);
        
        $produtoBU = '';
        foreach($produtos as $produto) {
            //$produtoBU = (($produto['PROD_BU'] == 1) ? $produto['PRODUTO'] : '');

            if($produtoBU == '' && $produto['PROD_BU'] == 1) {
                $produtoBU = (($produto['PROD_BU'] == 1) ? $produto['PRODUTO'] : '');
            }

            $opt .= '<option value="'.$produto['PRODUTO'].'" "'.(($produto['PROD_BU'] == 1) ? "selected" : '').'">'.$produto['PRODUTO'].'</option>';
        }
        $combo = '<select class="form-control input-sm" name="cmb-produto">'.$opt.'</select>';
         
        $dadosGrafico = $this->P4D->GraficoBolha($array, $produtoBU);
        $cores = array(
            'AB' => array('cor' => 'rgba(60, 118, 61, 0.5)', 'symbol' => 'circle', 'texto' => 'rgba(60, 118, 61, 1)'),
            'AA' => array('cor' => 'rgba(49, 112, 143, 0.5)', 'symbol' => 'diamond', 'texto' => 'rgba(49, 112, 143, 1)'),
            'BA' => array('cor' => 'rgba(138, 109, 59, 0.5)', 'symbol' => 'rect', 'texto' => 'rgba(138, 109, 59, 1)'),
            'BB' => array('cor' => 'rgba(169, 68, 66, 0.5)', 'symbol' => 'triangle', 'texto' => 'rgba(169, 68, 66, 1)'),
        ); 

        
        foreach($dadosGrafico['rs'] as $dado) {
            
            $data[] = array(
                
                'name' => $dado['PERFIL'],
                'value' => array($dado['ADOCAO'], $dado['POTENCIAL']),
                'symbol' =>  $cores[$dado['PERFIL']]['symbol'],
                'itemStyle' => array(
                                    'color' => $cores[$dado['PERFIL']]['cor'],
                                ),
                'tooltip' => array(
                    'backgroundColor' => "#FFF",
                    'borderColor' => $cores[$dado['PERFIL']]['texto'],
                    'borderWidth' => 1,
                    'textStyle' => array(
                        'color' => $cores[$dado['PERFIL']]['texto']
                    )
                ),


                'P4D' => array(
                    'medico'    => $dado['CRM']. ' ' .  $dado['NOME'],
                    'mercado'   => $_POST['opt-mercado'][0],
                    'produto'   => $_POST['opt-produto'][0],
                    'adocao'    => fnFormatarMoedaBRL($dado['ADOCAO']),
                    'potencial' => fnFormatarMoedaBRL($dado['POTENCIAL']),
                    'cor'       => $cores[$dado['PERFIL']]['cor']
                )


            ) ;


        }

        $mediaa = $this->P4D->Media($_POST['opt-mercado'][0], $_POST['opt-produto'][0]);


        $grafico = array(
            'dados' => array(
                'name' =>  'AA',
                'type' => 'scatter',
                'symbolSize' => 15,
                'data' => $data,     
                'markLine' => array(
                    'lineStyle' => array(
                        'normal' => array(
                            'type' => 'solid',
                            'color' => '#888',
                            'width' => 2
                        )
                    ),
                    'data' => array(
                        array('name' => 'Adoção', 'xAxis' => $mediaa['rs'][1]['RANGE_ADOCAO']),
                        array('name' => 'Potencial', 'yAxis' => $mediaa['rs'][1]['RANGE_POTENCIAL'])
                    ) 
                ),
                
            ),
            'eixos' => array(
                'y' => ($mediaa['rs'][1]['RANGE_ADOCAO']),
                'x' => ($mediaa['rs'][1]['RANGE_POTENCIAL'])
            )
        );


        /*
        $grafico = array(
            'dados' => array(
                'symbolSize'  => 15,
                'data' => $data,
                'type' => 'scatter',               
            ),
            'eixos' => array(
                'y' => ($mediaa['rs'][1]['RANGE_ADOCAO']),
                'x' => ($mediaa['rs'][1]['RANGE_POTENCIAL'])
            )
        );
*/


        



        $medicos = $this->P4D->ListagemMedicos($array);
        foreach($medicos as $medico) {
            $tb .= '<tr>
                        <td>'.$medico['CRM'].'</td>
                        <td>'.$medico['NOME'].'</td>
                        <td>'.$medico['ESPEC1'].'</td>
                        <td>'.$medico['CIDADE'].'</td>
                        <td>'.$medico['ADOCAO'].'</td>
                        <td>'.$medico['POTENCIAL'].'</td>
                        <td>'.$medico['PERFIL'].'</td>
                    </tr>';
        }

        $tb = '<table class="table table-sm">
                <tr>
                    <th>CRM</th>
                    <th>NOME</th>
                    <th>ESPECILIDADE</th>
                    <th>CIDADE</th>
                    <th>ADOCAO</th>
                    <th>POTENCIAL</th>
                    <th>PERFIL</th>
                </tr>
                '.$tb.'
                </table>';


        $final = array(
            'categorias' => $categoria,
            'grafico'    => $grafico,
            'produtos'   => $combo, 
            'medicos'    => $tb,
            'media'      => $media,

            'query'  => $dadosGrafico['query']
        );

        echo json_encode($final, JSON_NUMERIC_CHECK);
    }


    
    private function gerarFiltros($linha='') {

        $dados = array(
            /*
            array(
                'modulo' => 'Linha',
                'multi' => 0,
                'dados' => $this->P4D->getLinhas()
            ),
            */
            array(
                'modulo' => 'Setor',
                'campo' => 'SETOR',
                'multi' => 1,
                'dados' => $this->P4D->getSetores($linha)
            ),
            array(
                'modulo' => 'Medico',
                'campo' => 'VISITADO',
                'multi' => 1,
                'dados' => array(
                    array('LABEL' => 'VISITADO', 'OPCAO' => 1),
                    array('LABEL' => 'NAO VISITADO', 'OPCAO' => 0),
                )
            ),
            array(
                'modulo' => 'Especialidades',
                'campo' => 'CDGESPECIALIDADE',
                'multi' => 1,
                'dados' => $this->P4D->getEspecs($linha)
            ),
            array(
                'modulo' => 'Mercado',
                'campo' => 'MERCADO',
                'multi' => 0,
                'dados' => $this->P4D->getMercados($linha)
            ),
            
            
            
        );

        return $dados;

    }
    
    
    
    
}