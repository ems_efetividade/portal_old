<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * @author 053189 - Jefferson Oliveira em 28/08/2017
*/

class mTabela extends Model {
    
    public function __construct() {
        parent::__construct();
    }
    
    public function getConf($key='') {
        $query = "SELECT ".$key." FROM P4D_CONFIG";
        $rs = $this->cn->executarQueryArray($query);
        return $rs[1][$key];
    }
    
    public function listarTabela($tabela='', $orderBy='') {
        $query = "SELECT * FROM [".$tabela."]";
        $query .= ($orderBy != '') ? " ORDER BY ".$orderBy." " : '';
        return $this->cn->executarQueryArray($query);
    }
    
    public function getMenu($linha='') {
        $setor = fnDadoSessao('setor');
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;

        $arr['linha']  = (fnDadoSessao('perfil') == 0) ? $this->cn->executarQueryArray("SELECT L.NOME AS LINHA FROM LINHA L INNER JOIN P4D_MERCADO_LINHA M ON M.ID_LINHA = L.ID_LINHA GROUP BY L.NOME ORDER BY L.NOME ASC") : '';
        $arr['esp']  = $this->cn->executarQueryArray("EXEC proc_P4D_listarEspecialidade '".$linha."'");
        $arr['merc'] = $this->cn->executarQueryArray("EXEC proc_P4D_listarMercado '".$linha."'");       
        $arr['setor'] = $this->setores($setor, $linha);  
        
        $arr['erro'] = "EXEC proc_P4D_listarEspecialidade '".$linha."'";
       
        return $arr;
    }
    
    private function prepararVariaveis($vars=[]) {
        $variaveis = array();
        if(isset($vars)) {
            foreach($vars as $var) {
                $variaveis[] = (isset($var)) ? implode("'',''", $var) : '';
            }
        }
        return $variaveis;
    }
    
    private function getLinhaSetor() {
        $rs = $this->cn->executarQueryArray("SELECT L.NOME AS LINHA FROM LINHA L INNER JOIN SETOR S ON S.ID_LINHA = L.ID_LINHA WHERE S.SETOR = '".fnDadoSessao('setor')."'");
        return $rs[1]['LINHA'];
    }
    
    public function getDados($criterios, $linha='') {
        $vars = $this->prepararVariaveis($criterios);
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        
        $query = "EXEC proc_P4D_query2 '".$linha."',  '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 0";
        $rs = $this->cn->executarQueryArray($query);
        
        $query2 = "EXEC proc_P4D_query2 '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 1";
        $rs2 = $this->cn->executarQueryArray($query2);
        
        $query3 = "EXEC proc_P4D_medicos '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."'''";
        $rs3 = $this->cn->executarQueryArray($query3);
        
        $x['share'] = $rs2;
        $x['dados'] = $rs;
        $x['medicos'] = $rs3;
        $x['query'] = $query3;
        return $x;
    }
    
    public function getDadosGrafico($criterios, $linha='') {
        $vars = $this->prepararVariaveis($criterios);
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $query = "EXEC proc_P4D_query2 '".$linha."',  '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 0";
        $rs = $this->cn->executarQueryArray($query);
        $a = array('query' => $query, 'data' => $rs);
        return $a;
    }
    
    public function getDadosMedPerfil($criterios, $linha='') {
        $vars = $this->prepararVariaveis($criterios);
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $query = "EXEC proc_P4D_query2 '".$linha."',  '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 4";
        $rs = $this->cn->executarQueryArray($query);
        return $this->arrumarColunas($rs);
    }    
    
    public function getDadosResumo($criterios, $linha='') {
        $vars = $this->prepararVariaveis($criterios);
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $query = "EXEC proc_P4D_query2 '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 1";
        return $this->cn->executarQueryArray($query);
    }    
    
    public function getDadosMed($criterios, $linha='', $limite=0) {
        $vars = $this->prepararVariaveis($criterios);
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $query = "EXEC proc_P4D_medicos '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', '".$limite."'";
        $rs =  $this->cn->executarQueryArray($query);
        return $this->arrumarColunas($rs);
    }    
    
    public function getDadosMedicos($criterios,$linha='') {
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $vars = $this->prepararVariaveis($criterios);
        $query = "EXEC proc_P4D_query2 '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."''', 3";
        $rs = $this->cn->executarQueryArray($query);
        return $this->arrumarColunas($rs);
    }
    
    public function getDadosMedicosMercado($criterios,$linha='') {
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $vars = $this->prepararVariaveis($criterios);
        $query = "EXEC proc_P4D_medicos '".$linha."', '''".$vars[0]."''', '''".$vars[1]."''', '''".$vars[2]."'''";
        $rs = $this->cn->executarQueryArray($query);
        return $rs;
    }
    
    public function getCategoria($order=0) {
        $order = ($order == 0) ? '' : ' ORDER BY DESCRICAO ASC';
        return $this->cn->executarQueryArray("SELECT * FROM P4D_CATEGORIA ".$order);
    }
    
    public function setores($setor, $linha) {
        $query = "exec proc_P4D_listarSetor '".$setor."', '".$linha."'";
        $rs = $this->cn->executarQueryArray($query);
        return $this->montarArraySetor($rs, '');
    }
    
    public function montarArraySetor($rs, $str='') {
        $str = ($str == '') ? '<ul class="list-unstyled" style="">' : '<ul style="margin-left:10px;" class="list-unstyled collapse" id="col'.$rs[1]['NIVEL'].'">';
        foreach($rs as $item) {            
            $query = "SELECT * FROM SETOR WHERE NIVEL = ".$item['ID_SETOR']." ORDER BY SETOR ASC";
            $rsA = $this->cn->executarQueryArray($query);
            $str .= '<li><div>'
                    
                    .'<div class="checkbox"><label>'
                        .'<input type="checkbox" data-child="'.$item['ID_SETOR'].'" class="filtro" name="chkSetor[]" value="'.$item['SETOR'].'">'
                            . '<span data-toggle="collapse" href="#col'.$item['ID_SETOR'].'" aria-expanded="false" aria-controls="col'.$item['ID_SETOR'].'">'
                                .$item['SETOR'].'<span class="glyphicon"></span>'
                    . '</label></span>'
                    .'</div>';
            if(count($rsA) > 0) {
                $str .= $this->montarArraySetor($rsA, $str);
            } 
            $str .= '</div></li>';
        }
        $str .= '</ul>';
        return $str;
    }
    
    public function getMedicosPerfil($setor, $esps='', $mercados='', $perfil='', $linha='') {
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $iSetor = implode("'', ''", $setor);
        $iEsps = implode("'', ''", $esps);
        $iMerc = implode("'', ''", $mercados);
        $query = "EXEC proc_P4D_query2 '".$linha."',  '''".$iSetor."''', '''".$iMerc."''', '''".$iEsps."''', 2, '".$perfil."'";
        $rs = $this->cn->executarQueryArray($query);
        return $this->arrumarColunas($rs);
    }
    
    public function fichaMedico($crm='', $linha=''){
        $linha = ($linha == '') ? $this->getLinhaSetor() : $linha;
        $rsMedico = $this->cn->executarQueryArray("SELECT CRM, NOME, ESPECIALIDADE, CIDADE, UF FROM P4D_MEDICO_".$linha." WHERE CRM = '".$crm."' GROUP BY CRM, NOME, ESPECIALIDADE, CIDADE, UF");
        $rsSetor = $this->cn->executarQueryArray("SELECT SETOR, ENDERECO, NRO, COMPLEMENTO, BAIRRO, CEP FROM P4D_BASE_MEDICO WHERE CRM = '".$crm."' GROUP BY SETOR, ENDERECO, NRO, COMPLEMENTO, BAIRRO, CEP");
        $rsMercado = $this->cn->executarQueryArray("SELECT MERCADO, CAT_ATU AS CAT, MERC_ATU AS MERC, ISNULL(PROD_ATU,0) AS PROD, (ISNULL(PROD_ATU,0)/ CAST(MERC_ATU AS FLOAT)) * 100 AS SHARE FROM P4D_PX_".$linha." WHERE CRM = '".$crm."' ORDER BY (ISNULL(PROD_ATU,0)/ CAST(MERC_ATU AS FLOAT)) * 100 DESC");
        return array('medico' => $rsMedico, 'setores' => $rsSetor, 'mercados' => $rsMercado);
    }
    
    public function getPerfil($idClass) {
        $rs = $this->cn->executarQueryArray("SELECT * FROM P4D_CATEGORIA WHERE ID_CATEGORIA = ".$idClass);
        return $rs[1];
    }
    
    public function getAuditoria() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM P4D_AUDITORIA");
        return $rs;
    }
    
    public function getPeriodoPX($per) {
        $rs = $this->cn->executarQueryArray("SELECT * FROM P4D_SEMESTRE WHERE CODIGO = '".$per."'");
        return $rs[1];
    }
        
    private function arrumarColunas($rs) {
        $arr = array();
        foreach($rs as $row) {
            foreach($row as $key => $value){
                if(is_numeric($key)) {
                    unset($row[$key]);
                }
            }
            $arr[] = $row;
        }
        return $arr;
    }
}