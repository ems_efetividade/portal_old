          <h3><b>Dr. <?php echo $dados['medico'][1]['NOME'] ?></b></h3>

          <p><small><?php echo $dados['medico'][1]['CRM'] ?> - <?php echo $dados['medico'][1]['ESPECIALIDADE'] ?> - <?php echo $dados['medico'][1]['CIDADE'] ?>/<?php echo $dados['medico'][1]['UF'] ?></small></p>

          
          <hr>

          
          <div class="row">
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div>Potencial</div>
                        <h5 style="margin: 0px;"><b><?php echo $potencial ?></b></h5>
                    </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                <div class="panel panel-default">
                    <div class="panel-body text-center">
                        <div>Adoção</div>
                        <h5 style="margin: 0px;"><b><?php echo $adocao ?></b></h5>
                    </div>
                  </div>
              </div>
              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="panel panel-<?php echo $class['COR'] ?>">
                    <div class="panel-body bg-<?php echo $class['COR'] ?> text-center text-<?php echo $class['COR'] ?>">
                        <div><small>Perfil do Médico:</small></div>
                        
                        <h5 style="margin: 0px;"><b><span class="glyphicon <?php echo $class['ICONE'] ?>"></span> <?php echo $class['DESCRICAO'] ?></b></h5>
                    </div>
                  </div>
              </div>
          </div>
          
          
          
          <div class="well well-sm">
              <b>Setores Atuantes</b>
          </div>
          <small>
            <table class="table table-striped table-condensed" style="font-size: 11px">
                <tr>
                    <th>Setor</th>
                    <th>Endereço</th>
                </tr>
                <?php foreach($dados['setores'] as $setor) { ?>
                <tr>
                    <td><?php echo $setor['SETOR'] ?></td>
                    <td>
                        <?php echo $setor['ENDERECO'] ?>, <?php echo $setor['NRO'] ?> 
                        <?php echo $setor['COMPLEMENTO'] ?>,
                        <?php echo $setor['BAIRRO'] ?>,
                        <?php echo $setor['CEP'] ?>
                    </td>
                </tr>
                <?php } ?>
            </table>
          </small>
          
          <div class="well well-sm">
              <b>Mercados Prescritos </b> <small>(<?php echo $periodoPX['SEMESTRE'] ?>)</small>
          </div>
            <table class="table table-striped table-condensed" style="font-size: 11px">
                <tr>
                    <th>Mercado</th>
                    <th class="text-center">Categoria </th>
                    <th class="text-center">PX Mercado</th>
                    <th class="text-center">Share EMS</th>
                </tr>
                <?php foreach($dados['mercados'] as $mercado) { ?>
                <tr>
                    <td><?php echo $mercado['MERCADO'] ?></td>
                    <td class="text-center"><?php echo $mercado['CAT'] ?></td>
                    <td class="text-center"><?php echo $mercado['MERC'] ?></td>
                    <td class="text-center"><?php echo fnFormatarMoedaBRL(($mercado['SHARE']),1) ?>%</td>
                </tr>
                <?php } ?>
                
            </table>

          