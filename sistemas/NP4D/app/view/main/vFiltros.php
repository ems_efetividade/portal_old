

<form id="formFiltro" action="<?php echo APP_URL ?>main/filtrar" method="post">
    
<<<<<<< HEAD:sistemas/NP4D/app/view/main/vFiltros.php
        <!-- COMBO LINHA -->
        <?php  if($filtros['linha']) { ?>
        <select id="cmbLinha" name="cmbLinha" class="form-control input-sm">
            <option value="">:: SELECIONE A LINHA ::</option>
            <?php foreach($filtros['linha'] as $produto) { ?>
            <option value="<?php echo trim($produto['LINHA']) ?>">
                <?php echo $produto['LINHA'] ?>
            </option>
            <?php } ?>
        </select>
        <br>
        <?php } ?>
=======
    <div id="fil">
>>>>>>> 7a6b051cf5a7f23bc37318e10457a88fe7c7546f:sistemas/P4D_old/app/view/main/vFiltros.php

        <?php foreach($data['filtros'] as $filtro) { ?>

        <!-- LINHA -->
        <a class="btn btn-primary btn-sm btn-block" data-toggle="collapse" href="#clp-<?php echo $filtro['modulo'] ?>" aria-expanded="false" aria-controls="collapseExample"><?php echo $filtro['modulo'] ?></a>
        <div class="filter collapse inn" id="clp-<?php echo $filtro['modulo'] ?>" style="max-height:300px;overflow:auto">
            
                <div class="filter-text">

                <?php if($filtro['multi'] == 1) {  ?>

                    <?php foreach($filtro['dados'] as $dado) { ?>
                    
                    
                    <div class="checkbox">
                        <label>
                        <?php echo (isset($dado['ESPACO']) ? str_repeat('&nbsp;', ((($dado['ESPACO']+0)-1)*4)) : '') ?>
                        <input type="checkbox" class="" name="opt-<?php echo strtolower($filtro['campo']) ?>[]" value="<?php echo $dado['OPCAO'] ?>"><?php echo $dado['LABEL'] ?>
                        </label>
                    </div>

                    <div id="container-<?php echo $filtro['modulo'] ?>-<?php echo $dado['LABEL'] ?>"></div>
                    
                    <?php } ?>
                    <?php } else { ?>
                        <select id="cmb-produto" name="opt-<?php echo strtolower($filtro['campo']) ?>[]" class="form-control input-sm">
                        <?php foreach($filtro['dados'] as $dado) { ?>
                            <option value="<?php echo $dado['OPCAO'] ?>"><?php echo $dado['LABEL'] ?></option>
                            
                            
                        <?php } ?>
                        </select>
                        <br>
                        <div id="container-produto"></div>
                    <?php } ?>
                </div>
            
        </div>

        <?php } ?>

        <br>
        <button type="button" class="btn btn-success btn-block" id="btn-executar">Aplicar Filtros</button>

    </div>

           
       
</form>
<script src="<?php echo APP_URL ?>framework/libraries/echarts/echarts.min.js"></script>
<script>

$('#cmb-produto').change(function(e){

    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/listarProduto',
        data: {
            mercado: $(this).val()
        },
        dataType : 'json', 
        
        success: function(retorno) {
            $('#container-produto').html(retorno);

        },
        error: function(xhr, status, error) {
          console.log(xhr);
        },
        beforeSend: function (a) {
            
        
        }
    });

});

$('#btn-executar').click(function(e) {

    $.ajax({
        type: "POST",
        url: $('#formFiltro').attr('action'),
        data: $('#formFiltro').serialize(),
        dataType : 'json',  
        
        success: function(retorno) {
            $('#aguarde').modal('hide');
            console.log(retorno);



            /* popula as caixas */
            $('.count-up').each(function( key, item ) {
                $(this).data('end', 0);
                //alert($(this).data('end'));
            });

            $.each(retorno.categorias, function(i, item) {

                $('#div-'+ item.perfil +'-num').data('prefix', '[');
                $('#div-'+ item.perfil +'-num').data('suffix', ' Médico(s)]');
                $('#div-'+ item.perfil +'-num').data('end', item.qtd);

                $('#div-'+ item.perfil +'-sh').data('end' , item.share);
            });

            fCountUp();

            /* Atualizar Grafico */
            updateChart(retorno.grafico);

            /* tabela */
            $('#tbDados').html(retorno.medicos);

        },
        error: function(xhr, status, error) {
          
        },
        beforeSend: function (a) {
            $('#aguarde').modal('show');
        
        }
    });


});
function getCor(params) {
 return params.data.P4D.color;
}
function updateChart(data) {


    var option = {};
    var trafego = '';


    option = {
        title : {
            text: 'Gráfico de Segmentação',
            subtext: 'Divisão de médicos por perfil'
        },
    grid: {
        left: '3%',
        right: '7%',
        bottom: '3%',
        containLabel: true
    },
    tooltip : {
        // trigger: 'axis',
        showDelay : 0,

        formatter : function (params) {
            if (params.value.length > 1) {

                P4D = params.data.P4D;
                cor = P4D.cor;
                return '<small><b>' +  P4D.medico + '</b><br />' + 
                        '</small><small>' + 
                        'Produto: ' + P4D.produto +' <br />' + 
                        'Adoção: ' + P4D.adocao + '%<br />' + 
                        'Potencial: ' + P4D.potencial + '%</small>';
            }
            else {
                return params.seriesName + ' :<br/>'
                + params.name + ' : '
                + params.value + '';
            }
        },
        
        axisPointer:{
            show: true,
            type : 'cross',
            lineStyle: {
                type : 'dashed',
                width : 1
            }
        }
    },
    toolbox: {
        feature: {
            dataZoom: {
                title: {
                    zoom: "Selecionar",
                    back: "Voltar"
                }
            },
            saveAsImage: {
                title: "Exportar",
                name: "GraficoP4D"
            }
        }
    },

    legend: {
        show: 'true',
        data: ['AA','AB', 'BA', 'BB'],
        left: 'center'
    },
    
    xAxis : [
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: false
            },
            axisLine: {
                show:false
            },
        }
    ],
    yAxis : [
      
        {
            type : 'value',
            scale:true,
            axisLabel : {
                formatter: '{value}'
            },
            splitLine: {
                show: true
            },
            splitNumber: 5,
             axisLine: {
                show:false
            },
        }
    ],
    series: [data.dados]
};


   
    $('#container').hide();
    $('#container').html('');
    $('#container').html('<div id="graf" class="charts" style="width: 100%; height:450px; margin: 0 auto"></div>');
    $('#container').show();

    trafego = echarts.init(document.getElementById('graf'));
    trafego.setOption(option, true);

   
}    
    
    
</script>