<?php if($dados) { ?>

<table class="table table-striped table-condensed table-hover" style="font-size: 10px;">
      <tr>
          <th>#</th>
          <th>CRM</th>
          <th>NOME</th>
          <th>ESPECIALIDADE</th>
          <th>CIDADE</th>
          <th>ADOÇÃO</th>
          <th>POTENCIAL</th>
          <th>PERFIL</th>
      </tr>
      <?php foreach($dados as $i=> $setor) { ?>
      
      <tr class="">
          <td><?php echo ($i+1) ?></td>
      <?php echo '<td class=""><a href="#" class="ficha-medico" data-id="'.$setor['CRM'].'" data-y="'.$setor['POTENCIAL'].'" data-x="'.$setor[utf8_decode('ADOÇÃO')].'" data-perfil="'.$setor['ID_CATEGORIA'].'">'.implode('</td><td class="">', array_slice($setor, 0, count($setor)-1)).'</a></td>'; ?>
      </tr>
      
      <!--
      <tr class="">
          <td>
              <a href="#" class="ficha-medico" data-id="<?php echo $setor['CRM'] ?>" data-y="<?php echo fnFormatarMoedaBRL($setor['X'],2) ?>" data-x="<?php echo fnFormatarMoedaBRL($setor['Y'],2) ?>" data-perfil="<?php echo $setor['ID_CATEGORIA'] ?>">
              <?php echo $setor['CRM'] ?>
              </a>
          </td>
          <td><?php echo $setor['NOME']; ?></td>
          <td><?php echo $setor['ESPECIALIDADE'] ?></td>
          <td><?php echo $setor['CIDADE'].'/'.$setor['UF'] ?></td>
          <td><?php echo fnFormatarMoedaBRL($setor['X'],2) ?>%</td>
          <td><?php echo fnFormatarMoedaBRL($setor['Y'],2) ?>%</td>
          <td><?php echo $setor['PERFIL'] ?></td>
      </tr>
      !-->
      <?php } ?>
  </table>

<?php } ?>
