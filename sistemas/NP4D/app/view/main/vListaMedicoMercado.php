
<?php if($dados) {  ?>

<table class="tb table table-striped table-hover table-condensed" style="font-size: 10px;">
      <tr>
          <th>#</th>
         <?php echo '<th>'.implode('</th><th>', $colunas).'</th>'; ?>
      </tr>
      <?php foreach($dados as $i => $row) { ?>
      <tr>
          <td><?php echo ($i+1) ?></td>
          <?php echo '<td>'.implode('</td><td>', $row).'</td>'; ?>
      </tr>
      <?php unset($row); } ?>    
  </table>

<?php } ?>

