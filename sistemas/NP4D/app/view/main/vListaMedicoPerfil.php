<h3>
    Médicos por Perfil - <b><?php echo $perfil['DESCRICAO'] ?> <small>(<?php echo (count($dados)) ?> Médico(s) )</small> </b>
</h3>
<p>
<small>
    <div><b>Especialidade(s):</b> <?php echo $filtro['esp'] ?></div>
    <div><b>Mercado(s):</b> <?php echo $filtro['merc'] ?></div>
    <div><b>Território(s):</b> <?php echo $filtro['setor'] ?></div>
</small>    
</p>


<table class="table table-striped table-condensed table-hover" style="font-size: 10px;">
<tr>
          <th>#</th>
         <?php echo '<th>'.utf8_encode(implode('</th><th>', array_keys($dados[1]))).'</th>'; ?>
      </tr>
      <?php foreach($dados as $i => $row) { ?>
      <tr>
          <td><?php echo ($i+1) ?></td>
          <?php echo '<td>'.implode('</td><td>', $row).'</td>'; ?>
      </tr>
      <?php unset($row); } ?>   

</table>

<!--
<table class="table table-striped" style="font-size: 10px;">
      <tr>
          <th>Setor</th>
          <th>Médico</th>
          <th>Endereço</th>
          <th>Cidade</th>
          <th>Potencial</th>
          <th>Adoção</th>
          <th>Perfil</th>
      </tr>
      <?php foreach($dados as $setor) { ?>
      <tr>
          <td><?php echo $setor['SETOR'] ?></td>
          <td><?php echo $setor['CRM'].' '.$setor['NOME'] ?></td>
          <td>
              <?php echo $setor['ENDERECO'] ?>, <?php echo $setor['NRO'] ?> 
              <?php echo $setor['COMPLEMENTO'] ?>,
              <?php echo $setor['BAIRRO'] ?>,
              <?php echo $setor['CEP'] ?>
          </td>
          <td><?php echo $setor['CIDADE'].'/'.$setor['UF'] ?></td>
          <td><?php echo fnFormatarMoedaBRL($setor['Y'],2) ?>%</td>
          <td><?php echo fnFormatarMoedaBRL($setor['X'],2) ?>%</td>
          
          <td><?php echo $setor['PERFIL'] ?></td>
      </tr>
      <?php } ?>
  </table>

-->
