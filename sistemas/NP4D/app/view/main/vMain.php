<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>
<style>
    .vertical-alignment-helper {
        display:table;
        height: 100%;
        width: 100%;
        pointer-events:none; /* This makes sure that we can still click outside of the modal to close it */
    }
    .vertical-align-center {
        /* To center vertically */
        display: table-cell;
        vertical-align: middle;
        pointer-events:none;
    }
    .modal-content {
        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
        width:inherit;
        height:inherit;
        /* To center horizontally */
        margin: 0 auto;
        pointer-events: all;
    }

    .progress { margin-bottom: 0px; height: 15px;}
    .quad p {margin:0px}
    .quad .panel-body {padding: 9px;}
    .quad-value {font-size: 38px; font-weight: 900;}
    .quad-title {font-size: 24px}
    .panel-heading {padding-top:2px;padding-bottom:2px;}
    .quad {
        padding-left: 10px;
        padding-right: 10px;
    }
    .quad .col-lg-2 {
        padding-left: 5px;
        padding-right: 5px;
    }
    .quad-border {
        border-right: 2px solid #ddd;
    }
    
    
    .count {}
    .count .stats_count:before {
        content: "";
        position: absolute;
        left: 0;
        height: 65px;
        border-left: 2px solid #ADB2B5;
        margin-top: 10px;
    }
    
    .text-green {
        color: #1ABB9C;
    }
    .text-red {
            color: #E74C3C;
    }
    .quad-top {font-size: 12px;}
    .quad-bottom {font-size: 12px;}
    
    .filter {bordesr:1px solid #ddd; padding:5px;margin-bottom: 10px; margin-top: 0px;}
    .filter-text {font-size:10px;}
    hr {margin-top:3px;margin-bottom:10px;border-top: 2px solid #eee;}
    h3, h1 {margin:0px; margin-top: 3px;}
    #container {width: 100%;min-height: 350px;margin: 0 auto}
    .checkbox, .radio {margin:0px;}
    input[type=checkbox], input[type=radio] {
        margin:0px;
    }
    
    .box h1  {
        padding:0px;
        margin:0px;
    }
    
     li ul li {
        margin-right:4px;
    }
    
    .box h4  {
        padding:0px;
        margin:10px;
    }
    .box .panel-body {
        padding: 5px;
    }
    
    .btnMedicoPerfil .count-up {
        font-size: 11px;
    }
    
</style>
<script src="<?php echo APP_URL ?>framework/libraries/countUp/dist/countUp.min.js"></script>
<?php //var_dump($_SESSION) ?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('vMenu.php'); ?>
            <?php require_once('vFiltros.php'); ?>
        </div>
        <div class="col-lg-10" id="centro">
<<<<<<< HEAD:sistemas/NP4D/app/view/main/vMain.php
=======

>>>>>>> 7a6b051cf5a7f23bc37318e10457a88fe7c7546f:sistemas/P4D_old/app/view/main/vMain.php
            <div class="row">
                <div class="col-lg-10">
                    <h2>
                        <strong>
                            <span class="glyphicon glyphicon-user"></span> Segmentação de Médicos
                        </strong>
                    </h2>
                </div>
                <div class="col-lg-2">
                
                    <h2>
                        <div id="cmb-produto"></div>
                    </h2>        
                </div>
            </div>
            
            
            
            
            <div class="collapse " id="clpsFiltro" >
                <p>
                    <small>
                        <div><small><b>Especialidade(s):</b> <span id="filtro-esp"></span></small></div>
                        <div><small><b>Mercado(s):</b> <span id="filtro-merc"></span></small></div>
                        <div><small><b>Território(s):</b> <span id="filtro-setor"></span></small></div>                        
                    </small>
                </p>
            </div>
            <a data-toggle="collapse" href="#clpsFiltro" aria-expanded="false" aria-controls="clpsFiltro">
                <small>
                    <span class="glyphicon glyphicon-triangle-right"></span>
                    Ocultar/Mostrar Filtros
                </small></a>
            <hr>
            
            <div class="row box">
                <?php foreach($data['categoria'] as $categoria) { ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                    <div class="panel panel-<?php echo $categoria['COR'] ?>">
                        <div class="panel-body text-<?php echo $categoria['COR'] ?>">
                            <h4 class="text-center"><b><?php echo $categoria['PERFIL'] ?></b></h4>
                            <h1 class="text-center">
                                <b>
                                    <span class="glyphicon <?php echo $categoria['ICONE'] ?>"></span>
                                    <span class="count-up" data-start="0" data-end="0" data-num-decimal="1" data-duration="3" data-easing="true" data-group="true" data-separator="." data-decimal="," data-prefix="" data-suffix="%" id="div-<?php echo $categoria['PERFIL'] ?>-sh">0%</span>
                                </b>
                            </h1>

                            <a href="#" class="btnMedicoPerfil text-<?php echo $categoria['COR'] ?>" data-perfil="<?php //echo $categoria['ID_CATEGORIA'] ?>">
                                <h5 class="text-center count-up" data-start="" data-end="" data-num-decimal="0" data-duration="3" data-easing="true" data-group="true" data-separator="." data-decimal="" data-prefix="" data-suffix="" id="div-<?php echo $categoria['PERFIL'] ?>-num"></h5>
                            </a>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>

            <div class="panel panel-default">
<!--                <div class="panel-heading">
                    <h4>
                        <small><small><a data-toggle="collapse" href="#clpsGrafico"><span  class="glyphicon glyphicon-triangle-bottom"></span></a></small> <b>Segmentação de Médicos</b></small>
                    </h4>
                    
                
                </div>-->
                <div class="panel-body collapse in" id="clpsGrafico">
                    <div class="row">
                        <div class="col-lg-12">
                            <!--
                            <h4><b><span class="glyphicon glyphicon-stats"></span> Gráfico de Segmentação</b> <small>(Divisão de médicos por perfil)</small></h4>
                            <hr>
                            -->
                            <div id="container"></div>
                        </div>
                        
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
<!--                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>
                           <small><small><a data-toggle="collapse" href="#clpsM1"><span  class="glyphicon glyphicon-triangle-bottom"></span></a></small> <b>Médicos por Perfil</b></small>
                            </h4>
                            
                            
                            
                        </div>
                        <div class="col-lg-6">
                            <h5 class="text-right">
                                <a href="#" class="btn btn-xs btn-default btnExportar" data-tipo="1"><span class="glyphicon glyphicon-download-alt"></span></a>
                            </h5>

                        </div>
                    </div>
                        
                    </div>-->
                    <div class="panel-body">
                        <div class="row">
                        <div class="col-lg-10">
                            <h4><b><small><a data-toggle="collaspse" href="#clpsM1">
                                            <span  class="glyphicon glyphicon-menu-up"></span></a></small> Médicos </b> <small>(Listagem dos <?php //echo $maxReg ?> principais médicos que compõe o gráfico)</small></h4>
                        </div>
                        <div class="col-lg-2 text-right">
                            <a href="#" class="btn btn-xs btn-default btnExportar" data-tipo="1"><span class="glyphicon glyphicon-download-alt"></span> Exportar em .csv</a>
                        </div>
                    </div>
                        
                        
                            <hr>
                        <div class="collaspse clps" id="clpsM1" style="max-height: 500px; overflow: auto">

                        <div id="tbDados"></div>
                                
                       </div>
                    </div>
                         
            

                </div>
            
            
            <div class="panel panel-default">
<!--                <div class="panel-heading">
                    <div class="row">
                        <div class="col-lg-6">
                            <h4>
                           <small><small><a data-toggle="collapse" href="#clpsM2"><span  class="glyphicon glyphicon-triangle-bottom"></span></a></small> <b>Médicos por Mercado</b></small>
                            </h4>
                        </div>
                        <div class="col-lg-6">
                            <h5 class="text-right">
                            <a href="#" class="btn btn-xs btn-default btnExportar" data-tipo="2"><span class="glyphicon glyphicon-download-alt"></span></a>
                            </h5>

                        </div>
                    </div>
                        
                    </div>-->
                <div class="panel-body">
                    
                    <div class="row hidden-xs">
                        <div class="col-lg-10">
                            <h4>
                            <b>
                                <small>
                                    <a data-toggle="collapse" href="#clpsM2">
                                        <span  class="glyphicon glyphicon-menu-up"></span>
                                    </a>
                                </small> 
                                Médicos por Mercado
                            </b> 
                            <small>
                                (Listagem dos <?php echo $maxReg ?> principais médicos por mercado selecionado)
                            </small>
                            </h4>
                        </div>
                        <div class="col-lg-2 text-right">
                            <a href="#" class="btn btn-xs btn-default btnExportar" data-tipo="2"><span class="glyphicon glyphicon-download-alt"></span><span class="hidden-xs"> Exportar em .csv</small></a>
                        </div>
                    </div>
                    
                    <div class="row hidden-lg">
                        <div class="col-xs-10">
                            <h5>
                            <b>
                                <small>
                                    <a data-toggle="collapse" href="#clpsM2">
                                        <span  class="glyphicon glyphicon-menu-up"></span>
                                    </a>
                                </small> 
                                Médicos por Mercado
                            </b>
                            
                            
                            <small>
                                (<?php echo $maxReg ?> principais médicos por mercado selecionado)
                            </small>
                            </h5>
                        </div>
                        <div class="col-xs-2 text-right">
                            <a href="#" class="btn btn-xs btn-default btnExportar" data-tipo="2"><span class="glyphicon glyphicon-download-alt"></span><span class="hidden-xs"></small></a>
                        </div>
                    </div>
                    
                    
                            <hr>

                <div class="collapse clps" id="clpsM2" style="max-height: 500px; overflow-y: auto">
                    
                         
                    <div id="tbMercado"></div>
                    </div>
                </div>
                    
            </div>
            
            
            </div>
            
            
            
            
    
    </div>
</div>


<div class="modal" id="aguarde" tabindex="-1" role="dialog"  aria-labelledby="myModalLabel" data-backdrop="static">
    <div class="vertical-alignment-helper">
  <div class="modal-dialog modal-sm vertical-align-center" role="document">
    <div class="modal-content">
      
      <div class="modal-body text-center ">
          <img src="public/img/loading.gif" height="120" />
          <!--
          <p>Processando...</p>
          <small><div id="time">0:00:00</div></small>
          -->
      </div>
      
    </div>
  </div>
    </div>
    
</div>


<div class="modal fade" id="modalMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header btn-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">Ficha do Médico<div>
            </div>
            </div>
        </div>
        
        <div class="modal-body" style="max-height: 400px; overflow: auto">
          
      </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
      
    </div>
  </div>
    
</div>


<div class="modal fade" id="modalMedicoPerfil" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" data-backdrop="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header btn-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <div class="modal-title">Médicos por Perfil<div>
            </div>
            </div>
        </div>
        
        <div class="modal-body" style="max-height: 400px; overflow: auto">
          
      </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
      
    </div>
  </div>
    
</div>

<script src="<?php echo EMS_URL ?>/plugins/highchart/js/highcharts.js"></script>
<!--<script src="https://code.highcharts.com/highcharts.js"></script>-->

<script>
   //resizePanelLeft();

var Timer;
var myInterval = 1;
fCountUp();


if($("#cmbLinha").length) {
   if($("#cmbLinha").val() == "") {
       $('#fil').hide();
   }
}


//resize();

$('a[data-toggle="collapse"]').unbind().click(function(e){
    
    if($('span', this).length) {
        //html = $('span', this).attr('class');
        //alert(html.search('down'));
        //nhtml = (html.search('down')) ? html.replace('up', 'down') : html.replace('down', 'up');
        //$('span', this).attr('class', nhtml);
        $('span', this).toggleClass('glyphicon-menu-up').toggleClass('glyphicon-menu-down');
    }
});

$('.collapse').on('hidden.bs.collapse', function (e) {
    //var $clickedBtn = $(e.target).data('bs.collapse').$trigger;
    resize();
});

$('.collapse').on('shown.bs.collapse', function (e) {
    resize();
});

prepararFiltro();

$('#cmbLinha').unbind().change(function(e){

    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/menuLinha/'+$(this).val(),
        dataType : 'json',  
        success: function(retorno) {
            $('.checkAll').prop('checked', false);
            $('#chk-esp').hide().html(retorno.esp).fadeIn(350);
            $('#chk-merc').hide().html(retorno.merc).fadeIn(350);
            $('#chk-setor').hide().html(retorno.setor).fadeIn(350);
            prepararFiltro();
            $('#fil').show();
        },
        beforeSend: function (a) {
       
        }
    });
});



$('.btnMedicoPerfil').unbind().click(function(e){
   
    
    $.ajax({
        type: "POST",
        url: '<?php echo APP_URL ?>main/medicoPerfil/'+$(this).data('perfil'),
        data: $('#formFiltro').serialize(),  
        success: function(retorno) {
            $('#aguarde').modal('hide');
            $('#modalMedicoPerfil .modal-body').html('');
            $('#modalMedicoPerfil .modal-body').html(retorno);
            $('#modalMedicoPerfil').modal('show');
            timer(0);
           $('#aguarde').modal('hide');
        },
        beforeSend: function (a) {
           timer(1);
           $('#aguarde').modal('show');
            
            
        }
    });
});


   resize();
   
   
function prepararFiltro (){
    $(".checkAll").click(function () {
    var child = $(this).data('child');
    
    //alert(child);
    var nomeObj = $(this).data('name');
    $('input[name="'+nomeObj+'[]"]').not(this).prop('checked', this.checked);
    $('input[name="'+nomeObj+'[]"]').not(this).prop('checked', this.checked).attr('disabled', this.checked);
    

     clearTimeout(Timer);
    Timer = setTimeout(function(){
       filtrar();
    }, 1000);
});

$('.filtro').unbind().click(function(e){
    var child = $(this).data('child');
    $('#col'+child+' .filtro').not(this).prop('checked', this.checked).attr('disabled', this.checked);

   //alert(child);
    //filtrar();
    
    
    clearTimeout(Timer);
    Timer = setTimeout(function(){
        filtrar();
    }, 1000);
    
    //debounce(function(ev) {
    //    filtrar();
    //}, 5000, 0);
});
}
   
function fCountUp() {
    $('.count-up').each(function( key, item ) {
        var options = {
            useEasing:   (!$(this).data('easing')) ? true : $(this).data('easing'), 
            useGrouping: (!$(this).data('group')) ? true : $(this).data('group'), 
            separator:   $(this).data('separator'), 
            decimal:     $(this).data('decimal'), 
            suffix:      $(this).data('suffix'),
            prefix:      $(this).data('prefix')
        };

        countUp = new CountUp(
               $(this).attr('id'),
               $(this).data('start'),
               $(this).data('end'),
               $(this).data('num-decimal'),
               $(this).data('duration'), options
        );
       countUp.start();
    });
}   
function filtrar() {
    
    
    
    $.ajax({
        type: "POST",
        url: $('#formFiltro').attr('action'),
        data: $('#formFiltro').serialize(),
        dataType : 'json',  
        timeout: 0,
        success: function(retorno) {
            
            if(retorno.dados) {
            
            console.log(retorno.err);
            console.log(retorno.boxes);
            console.log(retorno.query);
            console.log(retorno.dados);
            atualizarGrafico(retorno);
            
            $('#tbMercado').hide().html(retorno.tbMercado).fadeIn(300);
            $('#tbDados').hide().html(retorno.table).fadeIn(300);
            
            
            //resize();
            $('#chkViewMercado').unbind().click(function(e){
                //alert($(this).prop('checked'));
                
                if($(this).prop('checked') == false) {
                    $('.merc').hide();
                }else {
                    $('.merc').show();
                }
            });
            
            
            $(".ficha-medico").unbind().click(function(e){
                e.preventDefault();
                modalMedico($(this).data('id'), $(this).data('y'), $(this).data('x'), $(this).data('perfil')); 
            });
            
            $('.btnExportar').unbind().click(function(e){
                e.preventDefault();
                
                
                
                var tipo = $(this).data('tipo');
                
                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>main/exportar/'+tipo,
                    data: $('#formFiltro').serialize(),  
                    timeout: 0,
                    success: function(retorno) {
                       window.location = '<?php echo APP_URL ?>main/download/'+retorno;
                       $('#aguarde').modal('hide');
                       timer(0);
                    },
                    error: function(xhr, status, error) {
                        $('#aguarde').modal('hide');
                        timer(0);
                        console.log(error);
                        alert('erro:' + error);
                    },
                    beforeSend: function (a) {
                        timer(1);
                        $('#aguarde').modal('show');
                    }
                });
            }); 
            
            resize();
            $('.clps').collapse('show');
            }
            $('#aguarde').modal('hide');
            timer(0);
            
            
        },
        error: function(xhr, status, error) {
            $('#aguarde').modal('hide');
            timer(0);
            alert(error);
        },
        beforeSend: function (a) {
            
            $('.clps').collapse('hide');
            timer(1);
           $('#aguarde').modal('show');
        }
    });
}   



function timer(acao) {

var counter = 0;
$('#time').html('00:00:00');

    if(acao == 1) {
        
        myInterval = setInterval(function () {
            ++counter;
            var result = secondsTimeSpanToHMS(counter);
            $('#time').html(result);
        }, 1000);
    } else {
        clearInterval(myInterval);
        
    }
}

function atualizarGrafico(dados) {

    try {

        var chart = new Highcharts.Chart({
            chart: {
                renderTo: 'container',
                events: {
                    redraw: function(event) {
                        //alert('opa');    
                        $('#aguarde').modal('hide');
                        timer(0);
                    }
                },  
                type: 'scatter',
                zoomType: 'xy',
                
            },

            colors: dados.cor,

            title: {
                text: null
            },
            subtitle: {
                text: null
            },
            xAxis: {
                title: {
                    enabled: true,
                    text: 'Adoção'
                },
                startOnTick: true,
                endOnTick: true,
                showLastLabel: true,
                dashStyle: 'Solid',
                min:-0,
                //max:chart.xAxis[0].max,
                //tickInterval: 1,
                plotLines: [{
                    color: 'gray',
                    width: 2,
                    value:  dados.mediaX,
                    label: {
                        align: 'bottom',
                        style: {
                            fontStyle: 'italic'
                        },
                        text: dados.mediaX
                    },
                    zIndex: 3
                }]
            },
            yAxis: {
                //reversedStacks: true ,
                title: {
                    text: 'Potencial'
                },
                dashStyle: 'Solid',
                plotLines: [{
                    color: 'gray',
                    
                    width: 2,
                    value:  dados.mediaY,
                    label: {
                        align: 'right',
                        style: {
                            fontStyle: 'bold'
                        },
                        text: dados.mediaY
                    },
                    zIndex: 3
                }]
            },

            plotOptions: {
                scatter: {
                    marker: {
                        radius: 7,
                        states: {
                            hover: {
                                enabled: true,
                                lineColor: 'rgb(100,100,100)'
                            }
                        }
                    },
                    states: {
                        hover: {
                            marker: {
                                enabled: true
                            }
                        }
                    },
                    tooltip: {
                        useHTML: true,
                        headerFormat: '',
                        pointFormat: '<b>{point.name}</b><br> <span >Potencial:</span> {point.y}%<br>Adoção: {point.x}%',
                        valueDecimals: 2
                    },

                },
                series: {
                    //colorByPoint: true,
                    turboThreshold:0,
                    point: {
                        events: {
                            click: function () {
                                modalMedico(this.id, (this.category + '%'), (this.y + '%'),  this.options.classificacao);                            
                            }
                        }
                    }
                }

            },
            series: dados.dados
        });


        /*
        chart.yAxis[0].update({
           //min:0,
           //max:chart.yAxis[0].max,
           //tickInterval: 10,
            plotLines: [{
                color: 'gray',
                dashStyle: 'solid',
                width: 2,
                value:  dados.mediaY,
                label: {
                    align: 'right',
                    style: {
                        fontStyle: 'bold'
                    },
                    text: dados.mediaY
                },
                zIndex: 3
            }]

        });
        
        
        chart.xAxis[0].update({
            min:-0,
            //max:chart.xAxis[0].max,
            //tickInterval: 1,
            plotLines: [{
                color: 'gray',
                dashStyle: 'solid',
                width: 2,
                value:  dados.mediaX,
                label: {
                    align: 'bottom',
                    style: {
                        fontStyle: 'italic'
                    },
                    text: dados.mediaX
                },
                zIndex: 3
            }]

        });
*/
        $('#filtro-esp').html(dados.filtro.esp);
        $('#filtro-setor').html(dados.filtro.setor);
        $('#filtro-merc').html(dados.filtro.merc);

        $('.count-up').each(function( key, item ) {
            $(this).data('end', 0);
        });

        $.each(dados.boxes, function(i, item) {

            perfil = item.ID_CATEGORIA.toString().toLowerCase();
            qtd = item.QTD;
            share = item.SHARE;

            label = (qtd == 1) ? 'Médico' : 'Médicos';


            $('#div-'+ perfil +'-num').data('prefix', '[');
            $('#div-'+ perfil +'-num').data('suffix', ' '+label+']');
            $('#div-'+ perfil +'-num').data('end', qtd);

            $('#div-'+ perfil +'-sh').data('end' , share);
        })
        fCountUp();
        timer(0);
        
    } catch(err) {
        alert("Houve um erro ao executar o processo: " +  err.message);
        $('#aguarde').modal('hide');
    }
}
   
function modalMedico(crm, y, x, classe) {

$.ajax({
    type: "POST",
    url: '<?php echo APP_URL ?>main/fichaMedico/'+crm, 
    data: {
        crm: crm,
        adocao: y,
        potencial: x,
        classificacao: classe,
        linha: $('#cmbLinha').val()
    },
    success: function(retorno) {
        timer(0);
       $('#aguarde').modal('hide');
        
        
        $('#modalMedico .modal-body').html('');
        $('#modalMedico .modal-body').html(retorno);
        $('#modalMedico').modal('show');

    },
    beforeSend: function (a) {
       //modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
       $('#aguarde').modal('show');
       timer(1);
       
    }
});




  
}



function debounce(func, wait, immediate) {
    var timeout;

    console.log(func);
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};

function secondsTimeSpanToHMS(s) {
    var h = Math.floor(s/3600); //Get whole hours
    s -= h*3600;
    var m = Math.floor(s/60); //Get remaining minutes
    s -= m*60;
    return h+":"+(m < 10 ? '0'+m : m)+":"+(s < 10 ? '0'+s : s); //zero padding on minutes and seconds
}

function resize() {
    var isBreakPoint = function (bp) {
        var bps = [320, 480, 768, 1024],
            w = $(window).width(),
            min, max
        for (var i = 0, l = bps.length; i < l; i++) {
          if (bps[i] === bp) {
            min = bps[i-1] || 0
            max = bps[i]
            break
          }
        }
        return w > min && w <= max
    }
        
        
        
        if(!isBreakPoint(320)) {
  var divHeight = $('#centro').height(); 
    $('.panel-left').css('min-height', divHeight+'px');   
}
}




</script>
