<?php

class Loader {

    // Load library classes

    public function library($lib){

        include LIB_PATH . $lib;

    }


    // loader helper functions. Naming conversion is xxx_helper.php;

    public function helper($helper){

        include HELPER_PATH . $helper;

    }

}

?>