<?php

require_once('lib/appConexao.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/mercadoModel.php');
require_once('app/model/produtoModel.php');
require_once('app/model/setorModel.php');
require_once('app/model/psShareModel.php');

class homeController extends appController
{

    private $conexao = null;
    private $funcao = null;
    private $mercado = null;
    private $produto = null;
    private $setor = null;
    private $psShare = array();
    private $config = '';

    private $arrProduto = null;
    private $periodo = null;

    private $tmpIdSetor = null;
    private $tmpCodLinha = null;


    private $maxProd = APP_MAX_PROD;

    private $valorProdEMSBrasil;

    private $maxProdPag = 9;

    public function homeController()
    {
        $this->funcao = new appFunction();
        $this->conexao = new appConexao();
        $this->mercado = new mercadoModel();
        $this->setor = new setorModel();
        $this->psShare = new psShareModel();
        $this->produto = new produtoModel();


        $this->tmpIdSetor = appFunction::dadoSessao('id_setor');
        $this->tmpCodLinha = appFunction::dadoSessao('id_linha');

        if (appFunction::dadoSessao('id_perfil') > 4) {
            $this->tmpIdSetor = $this->psShare->getIdSetorEstrutura(appFunction::dadoSessao('id_linha'));
        }


        $this->config = $this->psShare->getConfiguracao();
    }

    public function main()
    {
        $this->abrir();
    }

    public function trocaLinha($idLinha)
    {
        $_SESSION['id_linha'] = $idLinha;
        echo 1;
    }

    public function abrir()
    {
        $_POST = appSanitize::filter($_POST);
        $mercado = $_POST['mercado'];
        $produtos = $_POST['produtos'];
        $conc = $concs;


        $mercados = $this->mercado->listar($this->tmpCodLinha);

        $mercado = ($mercado == '') ? $mercados[1] : $this->mercado->selecionar($mercado);
        $mercadoProdutos = $this->psShare->getProdutosMercado($mercado, $this->tmpCodLinha);

        if (isset($produtos)) {
            foreach ($produtos as $k => $prod) {
                $dado = explode("|", $prod);
                $temp[($k + 1)] = array('ID_PRODUTO' => $dado[0], 'PRODUTO' => $dado[1]);
            }
            $produtos = $temp;
        } else {
            $produtos = $this->psShare->getPrincProdutos($mercado, $this->tmpCodLinha);
        }


        $tabelaPxShare = $this->gerarTabela($this->tmpIdSetor, $mercado, $produtos);
        $serieGraficoPizza2 = $this->graficoPizzaECharts($this->tmpIdSetor, $mercado, $produtos);


        $dadosView['mercado'] = $mercado;
        $dadosView['produtos'] = json_encode($produtos);
        $dadosView['produtoLab'] = $this->psShare->getProdutoLab($mercado);
        $dadosView['trimestre'] = $this->psShare->getTrimestre();

        $this->set('drill', $graficoDrill);
        $this->set('mercadoSelecionado', $mercado);
        $this->set('mercadoProdutos', $mercadoProdutos);
        $this->set('dadosView', $dadosView);
        $this->set('listaMercado', $mercados);
        $this->set('serieGrafPizzaE', $serieGraficoPizza2);
        $this->set('tabelaPxShare', $tabelaPxShare);
        $this->set('config', $this->config);
        $this->render('homeView.1');
    }


    public function dadosDrill()
    {
        $setor = $_POST['setor'];
        $mercado = $_POST['mercado'];
        $cor = $_POST['cor'];
        $nivel = $_POST['nivel'];
        $produto = $_POST['produto'];

        $idSetor = ($setor == '') ? $this->tmpIdSetor : $setor;

        $dados = $this->psShare->getDadosGraficoDrill($idSetor, $mercado, $produto);

        $eChart = '';

        foreach ($dados as $rep) {
            $nome = explode(' ', $rep['NOME']);
            $label = $nome[0] . ' ' . $nome[count($nome) - 1];

            $dado['setor'] = $rep['ID_SETOR'];
            $dado['sub'] = $rep['SUB'];
            $dado['nivel'] = $rep['NIVEL'];
            $dado['cor'] = $cor;
            $dado['indice'] = $rep['INDICE'];
            $nome = $rep['SETOR'] . ' ' . $label;


            /*e-charts*/
            $eChart['name'][] = $nome;
            $eChart['data'][] = array('type' => 'bar',
                'barGap' => '-100%',
                'barWidth' => '60%',
                'id' => $rep['ID_SETOR'],
                ///'color' => null,
                'label' => array('show' => 'true',
                    'position' => 'top',
                    'fontWeight' => 'bold',
                    'color' => '#000000',
                    'fontSize' => 10,
                    'formatter' => appFunction::formatarMoeda($rep['SHARE'], 1) . '%'
                ),
                'data' => array([str_replace(' ', PHP_EOL, $nome), $rep['SHARE']]));

            if ($rep['SUB'] != 0) {
                $nome = '<b>' . $nome . '</b>';
            }

            $dado['name'] = $nome;
            $dado['y'] = $rep['SHARE'];

            $data[] = $dado;

        }

        $final['brasil'] = 254;
        $final['color'] = $cor;
        $final['colorByPoint'] = ($idSetor == appFunction::dadoSessao('id_setor')) ? true : false;
        $final['data'] = $data;


        $outro['label'] = $eChart['name'];
        $outro['data'] = $eChart['data'];

        $final2['graf1'] = $final;
        $final2['graf2'] = $outro;

        echo json_encode($final2, JSON_NUMERIC_CHECK);
    }


    public function dadosDrillEchart()
    {

        $criterios = $_POST['dados'];
        $criterios = json_decode($criterios, true)[0];

        $criterios['id'] = ($criterios['id'] == '') ? $this->tmpIdSetor : $criterios['id'];

        $dados = $this->psShare->getDadosGraficoDrill($criterios['id'], $criterios['mercado'], $criterios['produto']);


        $eChart = '';

        foreach ($dados as $k => $rep) {

            if ($rep['ID_SETOR'] == $criterios['id'] && $k == 1) {
                $criterios['cor'] = null;
            }

            $nome = explode(' ', $rep['NOME']);
            $label = $nome[0] . ' ' . $nome[count($nome) - 1];

            $nome = $rep['SETOR'] . ' ' . $label;

            $eChart['name'][] = $nome;
            $eChart['data'][] = array('type' => 'bar',
                'barGap' => '-100%',
                'barWidth' => '50%',
                'id' => array($rep['ID_SETOR'], $rep['SUB']),
                'color' => $criterios['cor'],
                'label' => array('show' => 'true',
                    'position' => 'top',
                    'fontWeight' => 'bold',
                    'color' => '#777777',
                    'fontSize' => 10,
                    'formatter' => appFunction::formatarMoeda($rep['SHARE'], $this->config['DECIMAL']) . $this->config['SUFIXO']
                ),
                'data' => array([str_replace(' ', PHP_EOL, $nome), round($rep['SHARE'], $this->config['DECIMAL'])]));

            if ($criterios['id'] == $rep['ID_SETOR']) {
                $outro['voltar'] = $rep['ID_NIVEL'];
            }

        }

        $outro['color'] = $criterios['cor'];
        $outro['data'] = $eChart['data'];

        $final2 = $outro;

        echo json_encode($final2, JSON_NUMERIC_CHECK);

    }

    public function dadosGraficoDrill($idSetor, $mercado)
    {
        $dados = $this->psShare->getDadosGraficoDrillSetor($idSetor, $mercado);

        foreach ($dados as $dado) {
            $arr['ID_SETOR'] = $dado['ID_SETOR'];
            $arr['SETOR'] = $dado['SETOR'];
            $arr['NOME'] = $dado['NOME'];
            $arr['SHARE'] = $dado['SHARE'];

            if ($this->psShare->verificaEstrutura($dado['ID_SETOR']) > 0) {
                $arr['EQUIPE'] = $this->graficoDrill($dado['ID_SETOR'], $mercado);
            }

            $final[] = $arr;
        }
        return $final;


    }

    public function graficoPizza($idSetor, $mercado, $produto)
    {

        foreach ($produto as $prod) {
            $arrProduto[] = $prod['PRODUTO'];
        }

        $dados = $this->psShare->getDadosGraficoPizza($idSetor, $mercado, $arrProduto);

        foreach ($dados as $k => $dado) {
            $soma += $dado['SHARE'];

            $graf['name'] = $dado['PRODUTO'] . ' (' . appFunction::formatarMoeda($dado['SHARE'], $this->config['DECIMAL']) . '%)';
            $graf['y'] = $dado['SHARE'];
            $graf['sliced'] = false;
            $graf['selected'] = false;

            if ($k == 1) {
                $graf['sliced'] = 'true';
                $graf['selected'] = 'true';
            }

            $serie[] = $graf;
        }

        //if($soma < 100) {
        //	array_push($serie, array('name' => 'OUTROS'.$soma.' ('. appFunction::formatarMoeda((100 - $soma), $this->config['DECIMAL']).'%)', 'y' => (100 - $soma)));
        //}
        return json_encode($serie, JSON_NUMERIC_CHECK);
    }


    public function graficoPizzaECharts($idSetor, $mercado, $produto)
    {

        foreach ($produto as $prod) {
            if ($prod['ID_PRODUTO'] != '') {
                $arrProduto[] = $prod['ID_PRODUTO'];
            }
        }

        $dados = $this->psShare->getDadosGraficoPizza($idSetor, $mercado['ID_MERCADO'], $arrProduto, $this->tmpCodLinha);

        foreach ($dados as $k => $dado) {
            $soma += $dado['SHARE'];

            $graf['name'] = $dado['PRODUTO'] . ' (' . appFunction::formatarMoeda($dado['SHARE'], $this->config['DECIMAL']) . '%)';
            $graf['value'] = round($dado['SHARE'], $this->config['DECIMAL']);
            $graf['sliced'] = false;
            $graf['selected'] = false;

            if ($k == 1) {
                $graf['sliced'] = 'true';
                $graf['selected'] = 'true';
            }

            $serie[] = $graf;

            $legend[] = $graf['name'];
        }

        if (round($soma) < 100) {
            $graf['name'] = 'OUTROS (' . appFunction::formatarMoeda((100 - $soma), $this->config['DECIMAL']) . '%)';
            $graf['value'] = round((100 - $soma), $this->config['DECIMAL']);


            $serie[] = $graf;
            $legend[] = $graf['name'];
        }

        $final['dados'] = json_encode($serie, JSON_NUMERIC_CHECK);
        $final['legend'] = json_encode($legend, JSON_NUMERIC_CHECK);

        return $final;
    }

    public function gerarTabela($idSetor, $mercado, $produtos = [], $equipe = 0)
    {
        $dados['mercado'] = $mercado;
        $dados['princProd'] = $produtos;
        $dados['dados'] = $this->psShare->getTabelaDados($idSetor, $mercado['ID_MERCADO'], $produtos, $equipe);


        $this->set('tabelaDados', $dados);
        $this->set('config', $this->config);
        $this->set('conf', array('table' => 1));

        $view = $this->renderToString('vTabela');

        return $view;
    }

    public function tabelaEquipe()
    {

        $idSetor = $_POST['id_setor'];
        $mercado = $_POST['mercado'];
        $produtos = json_decode($_POST['produtos'], true);


        $dados['mercado'] = $this->mercado->selecionar($mercado);
        $dados['princProd'] = $produtos;
        $dados['dados'] = $this->psShare->getTabelaDados($idSetor, $mercado, $produtos, 1);

        $conf['table'] = 0;
        $conf['pad'] = '&nbsp;';

        $this->set('conf', $conf);
        $this->set('config', $this->config);
        $this->set('tabelaDados', $dados);

        $view = $this->renderToString('vTabela');

        echo $view;

    }

    public function listarMedicos()
    {
        $criterio['ID_SETOR'] = $_POST['setor'];
        $criterio['ID_MERCADO'] = $_POST['mercado'];
        $criterio['PERIODO'] = $_POST['periodo'];
        $criterio['ID_PRODUTO'] = $_POST['produto'];

        $merc['MERCADO'] = $this->mercado->selecionar($criterio['ID_MERCADO']);
        $merc['PRODUTO'] = $this->produto->selecionar($criterio['ID_PRODUTO']);

        $dados = $this->psShare->getMedicosPeriodo($criterio['ID_SETOR'], $criterio['ID_MERCADO'], $criterio['ID_PRODUTO'], $criterio['PERIODO']);
        $dados['resumo'] = $this->psShare->getResumoCategoria($criterio['ID_SETOR'], $criterio['ID_MERCADO'], $criterio['ID_PRODUTO'], $criterio['PERIODO']);


        $this->set('trim', $this->psShare->getTrimestre());
        $this->set('criterio', $criterio);
        $this->set('dados', $dados);
        $this->set('merc', $merc);
        $this->set('config', $this->config);
        $view = $this->renderToString('vTabelaMedico');

        echo $view;
    }
public function fichaMedicoo() {
    echo 'ijasijasd';
}
    public function fichaMedico()
    {
        $criterio['CRM']      = $_POST['crm'];
        $criterio['NOME']     = $_POST['nome'];
        $criterio['ESP']      = $_POST['esp'];
        $criterio['PERIODO']  = $_POST['periodo'];
        $criterio['ID_LINHA'] = appFunction::dadoSessao('id_linha');

        $dados = $this->psShare->getFichaMedico($criterio['CRM'], $criterio['ID_LINHA'], $criterio['PERIODO']);

        $this->set('criterio', $criterio);
        $this->set('dados', $dados);
        $this->set('config', $this->config);
        $view = $this->renderToString('vFichaMedico');

        echo $view;
    }

    public function fichaMedicoForever()
    {
        $criterio['CRM'] = $_POST['crm'];
        $criterio['NOME'] = $_POST['nome'];
        $criterio['ESP'] = $_POST['esp'];
        $criterio['PERIODO'] = $_POST['periodo'];
        $dados = $this->psShare->getFichaMedicoSigno($criterio['CRM'], $criterio['ID_LINHA'], $criterio['PERIODO']);
        $this->set('criterio', $criterio);
        $this->set('dados', $dados);
        $this->set('config', $this->config);
        $view = $this->renderToString('vFichaMedico');
        echo $view;
    }


    public function exportarListaMedico($idSetor, $mercado, $produto, $periodo)
    {

        $criterio['ID_SETOR'] = $idSetor;
        $criterio['PRODUTO'] = $produto;
        $criterio['MERCADO'] = $mercado;
        $criterio['PERIODO'] = $periodo;

        $dados = $this->psShare->getExportarMedicosPeriodo($criterio['ID_SETOR'], $criterio['MERCADO'], $criterio['PRODUTO'], $criterio['PERIODO']);

        $subs = array(
            array('DE' => 'TRM_ANT', 'PARA' => $dados['header']['TRM01']),
            array('DE' => 'TRM_ATU', 'PARA' => $dados['header']['TRM00']),
        );

        $this->exportarCSV($dados['dados'], 'Médicos Prescritores', '', $subs);
        //print_r($dados['dados']);
    }

    public function exportarFichaMedico($crm, $periodo)
    {
        $criterio['CRM'] = $crm;
        $criterio['PERIODO'] = $periodo;

        $dados = $this->psShare->getExportarFichaMedico($criterio['CRM'], $criterio['PERIODO']);


        $subs = array(
            array('DE' => 'CAT_TRM02', 'PARA' => 'CAT ' . $dados['header'][1]['TRM02']),
            array('DE' => 'CAT_TRM01', 'PARA' => 'CAT ' . $dados['header'][1]['TRM01']),
            array('DE' => 'CAT_TRM00', 'PARA' => 'CAT ' . $dados['header'][1]['TRM00']),

            array('DE' => 'SHARE_TRM02', 'PARA' => 'SHARE ' . $dados['header'][1]['TRM02']),
            array('DE' => 'SHARE_TRM01', 'PARA' => 'SHARE ' . $dados['header'][1]['TRM01']),
            array('DE' => 'SHARE_TRM00', 'PARA' => 'SHARE ' . $dados['header'][1]['TRM00']),
        );

        $this->exportarCSV($dados['dados'], 'Ficha do Médico', "", $subs);
    }


    public function pesquisar()
    {
        $dadosPesquisa['criterio'] = $_POST['txtCriterio'];
        $dadosPesquisa['campo'] = $_POST['cmbCampo'];
        $dadosPesquisa['pagina'] = (!isset($_POST['txtPagina'])) ? 1 : $_POST['txtPagina'];

        $mercados = $this->mercado->listar($this->tmpCodLinha);
        $mercado = ($mercado == '') ? $mercados[1]['MERCADO'] : $mercado;


        /*Calcular Pagina*/
        $de = ($dadosPesquisa['pagina'] == 0) ? 1 : (($dadosPesquisa['pagina'] * $this->config['MAX_POR_PAG']) - $this->config['MAX_POR_PAG']) + 1;
        $ate = ($de + $this->config['MAX_POR_PAG'] - 1);

        $campos = $this->psShare->getCamposPesquisaMedico();
        $dados = $this->psShare->pesquisarMedico($this->tmpIdSetor, $dadosPesquisa, $de, $ate);

        $paginacao = $this->gerarPaginacao($dados['count'], $this->config['MAX_POR_PAG'], $dadosPesquisa['pagina']);

        $this->set('dados', $dados);
        $this->set('campos', $campos);
        $this->set('paginacao', $paginacao);
        $this->set('dadosPesquisa', $dadosPesquisa);
        $this->set('listaMercado', $mercados);
        $this->set('config', $this->config);

        $this->render('pesquisarView');
    }

    public function search() {

        $mercados = $this->mercado->listar($this->tmpCodLinha);
        $this->set('listaMercado', $mercados);
        $this->set('totalmed', $this->psShare->contarMedicosSetor());

        $dados = $this->psShare->buscarMedicos($criterio, 1, $this->maxProdPag);
        
        $dadosRetorno = $this->gerarTabelaConsulta($dados, $criterio, $this->maxProdPag);

        $this->set('tb', $dadosRetorno);
        $this->render('pesquisarViewNew');

    }

    public function pesquisaCriterio() {
        $criterio = $_POST['criterio'];

        $arrCrit = str_replace(' ', '|', $criterio);

        $dados = $this->psShare->pesquisarCriterio($criterio);
        $html = '';
      
        $bold = '<b><u>$1</u></b>';
        if($dados) {
            foreach($dados as $dado) {
                $string['texto'] = $dado['NOME'] . ' - ' . $dado['ESP'] . ' - ' . $dado['CIDADE'] . ' - ' . $dado['UF'];
                $string['label'] = preg_replace("#(".$arrCrit.")#i", $bold,  $string['texto']);
                $arr[] = $string;
            }
            if(count($dados) >= 10) {
            $string['texto'] = $criterio;
            $string['label'] = 'Ver todos os resultados...';
            $arr[] = $string;
            }
        } 
        echo json_encode($arr);
    }

    public function buscarMedicos() {
        $criterio  = $_POST['criterio'];
        $pagina    = ($_POST['pagina'] == 0) ? 1 : $_POST['pagina'];

        $dados = $this->psShare->buscarMedicos($criterio, $pagina, $this->maxProdPag);

        $dadosRetorno = $this->gerarTabelaConsulta($dados, $criterio, $this->maxProdPag);

        echo  json_encode(
            array(
                'html' =>  $dadosRetorno['html'] . $dadosRetorno['paginacao'],
                'pagina' => $dados['paginas']['prox'],
                'query' => $dados['query'],
                'wow' => $dados['wow']
            )
        );
    }

    private function gerarTabelaConsulta($dados, $criterio='', $maxPag=10) {

        
        if($dados['dados']) {
            foreach($dados['dados'] as $dado) {

                $u = "'";
                $html = '<div style="mMargin-bottom:20px; maMrgin-top:20px">

                            <div style="font-size:16px;">
                                <a href="#" onClick="fichaMedico('.$u.$dado['ID_CRM'].$u.', '.$u.$dado['NOME'].$u.', '.$u.$espss.$u.', '.$u.'TRM01|TRM00'.$u.')">
                                    <b>'.$dado['NOME'] .' ('. $dado['CRM'].') </b>
                                </a>
                            </div>

                            <p style="font-size:14px; margin: 0 0 2px" class="text-success">
                                <small>
                                    '.$dado['ESP'].'
                                </small>
                            </p>

                            <div class="text-muted">
                                <small>
                                    '.$dado['CIDADE'].' / '.$dado['UF'].
                                '</small>
                            </div>

                        </div>';

                $dadosTabela[] = array($html);
                //$dadosTabela[] = array($dado['CRM']. ' - ' . $dado['NOME'], $dado['ESP'], $dado['CIDADE'].' / '.$dado['UF']);
            }
        }

        $tabela['classe'] = 'table ';
        if($criterio != "") {
            $tabela['header'] = array('<h4 class="text-muted">Resultado da pesquisa por <b>"'.$criterio.'"</b> (' . appFunction::formatarMoeda($dados['total'],0) . ' registro(s)):</h4>');
        }
        $tabela['data'] = $dadosTabela;
        $tabela['paginacao'] = $dados['paginas'];
        $tabela['max_por_pag'] = $maxPag;

        $this->set('dados', $tabela);
        $this->set('criterio', $criterio);
        $tabelaHTML = $this->renderToString('vTabelaPesquisa');

        return array(
            'html'      => $tabelaHTML,
        );

    }

    private function gerarPaginacao($totalReg, $maxPorPag = 20, $pagAtual = 1)
    {
        $paginas = ceil($totalReg / $maxPorPag);
        /*
                for($i=1;$i<=$paginas;$i++) {
                    $class = ($pagAtual == $i) ? 'btn-primary' : 'btn-default';
                    $links[] = '<a href="#" class="btn btn-xs '.$class.' link-pagina" data-pag="'.$i.'">'.$i.'</a> ';
                }
        */

        $prox = (($pagAtual + 1) > $paginas) ? $paginas : ($pagAtual + 1);
        $ant = (($pagAtual - 1) == 0) ? 1 : ($pagAtual - 1);

        $html = '<nav aria-label="...">
				<ul class="pager">
				<li><a href="#" class="link-pagina" data-pag="1"><small>Primeiro</small></a></li>
				<li><a href="#" class="link-pagina" data-pag="' . $ant . '"><small>Anterior</small></a></li>
				<li><small>Página ' . appFunction::formatarMoeda($pagAtual, 0) . ' de ' . appFunction::formatarMoeda($paginas, 0) . '</small></li>
				<li><a href="#" class="link-pagina" data-pag="' . $prox . '"><small>Próximo</small></a></li>
				<li><a href="#" class="link-pagina" data-pag="' . $paginas . '"><small>Último</small></a></li>
				</ul>
			</nav>';

        return array(
            'PAGINAS' => $paginas,
            'LINKS' => array($html)
        );
    }


    private function exportarCSV($dados, $nomeArquivo, $titulo = '', $subsCab = '')
    {

        $colunas = array_keys($dados[1]);

        foreach ($colunas as $key => $value) {
            if (is_int($value)) {
                unset($colunas[$key]);
            }
        }

        if (is_array($subsCab)) {
            foreach ($subsCab as $sub) {
                foreach ($colunas as $key => $value) {
                    if ($value == $sub['DE']) {
                        $colunas[$key] = $sub['PARA'];
                        break;
                    }
                }
            }
        }


        $nomeArquivo = $nomeArquivo . '_' . date('Ymd') . '.csv';

        $df = fopen(ROOT . 'temp\\' . $nomeArquivo, 'w');

        if ($titulo != '') {
            $arrTitulo[1] = utf8_decode($titulo);
            fputcsv($df, $arrTitulo, ';');
        }

        fputcsv($df, $colunas, ';');

        foreach ($dados as $row) {
            $a = [];

            for ($i = 0; $i <= count($row); $i++) {
                $a[$i] = $row[$i];
            }
            fputcsv($df, $a, ';');
        }

        fclose($df);
        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename="' . $nomeArquivo . '"');
        header('Content-Type: application/octet-stream');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize(ROOT . 'temp\\' . $nomeArquivo));
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Expires: 0');

        // Envia o arquivo para o cliente
        readfile(ROOT . 'temp\\' . $nomeArquivo);
        unlink(ROOT . 'temp\\' . $nomeArquivo);
    }


}


?>