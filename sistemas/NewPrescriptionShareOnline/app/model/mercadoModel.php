<?php

require_once('lib/appConexao.php');

class mercadoModel extends appConexao {

	private $idLinha;
	private $mercado;
	private $produto;
	private $concorrentes;
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;	
	}
	
	public function setMercado($valor) {
		$this->mercado = $valor;	
	}

	public function setConcorrentes($concs) {
		$this->concorrentes = $concs;
	}

	public function getConcorrentes() {
		return $this->concorrentes;
	}
	
	public function getIdLinha() {
		return $this->idLinha;	
	}
	
	public function getMercado() {
		return $this->mercado;	
	}
	
	public function selecionar($idMercado) {
		$query = "SELECT ID AS ID_MERCADO, MERCADO FROM PS_DIM_MERCADO WHERE ID = ".$idMercado;
		return $this->executarQueryArray($query)[1];
	}

	public function listar($idSetor=0) {
		//$query = 'SELECT MERCADO FROM PS_FATO WHERE ID_LINHA = '.$idSetor.' GROUP BY MERCADO ORDER BY MERCADO ASC';
		$query = "EXEC proc_psn_listarMercado ".$idSetor;
		return $this->executarQueryArray($query);
	}


}