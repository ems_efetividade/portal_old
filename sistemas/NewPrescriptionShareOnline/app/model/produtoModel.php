<?php

require_once('lib/appConexao.php');

class produtoModel extends appConexao {
    
    private $idProduto;
    private $mercado;
    private $produto;
    private $layoutArquivo;
    
    public function __construct() {
        parent::__construct();
        
        $this->idProduto = 0;
        $this->mercado = '';
        $this->produto = '';
        $this->layoutArquivo = array('MERCADO', 'PRODUTO_SGP');
    }
    
    public function setMercado($mercado) {
        $this->mercado = $mercado;
    }

    public function setProduto($produto) {
        $this->produto = $produto;
    }

    public function setIdProduto($id) {
        $this->idProduto = $id;
    }

    public function getMercado() {
        return $this->mercado;
    }

    public function getProduto() {
        return $this->produto;
    }
    
    public function getLayout() {
        return $this->layoutArquivo;
    }

    public function selecionar($idProduto) {
        $query = "SELECT ID AS ID_PRODUTO, PRODUTO FROM PS_DIM_PRODUTO WHERE ID = ".$idProduto;
		return $this->executarQueryArray($query)[1];
    }

    public function salvar() {
        
        $validar = $this->validar();
        
        if($validar == '') {
            
        
            $query = "INSERT INTO [PS_PRODUTO_SGP]
                            ([MERCADO]
                            ,[PRODUTO_SGP])
                      VALUES
                            ('".$this->mercado."'
                            ,'".$this->produto."')";

            $this->executar($query);
        } else {
            return $validar;
        }
    }
    
    public function excluir() {
        $this->executar("DELETE FROM PS_PRODUTO_SGP WHERE MERCADO = '".$this->mercado."' AND PRODUTO_SGP = '".$this->produto."'");
    }
    
    public function limparTabela() {
        $this->executar("DELETE FROM PS_PRODUTO_SGP");
    }
    
    private function validar() {
        $erro = '';
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS T FROM PS_PRODUTO_SGP WHERE MERCADO = '".$this->mercado."' AND PRODUTO_SGP = '".$this->produto."'");
        
        if($rs[1]['T'] > 0) {
            $erro =  'Já existe um mercado e um produto com esse nome!';
        }
        
        return $erro;
    }
    
    public function listar() {
        $arr = array();
        $rs = $this->executarQueryArray("SELECT * FROM PS_PRODUTO_SGP ORDER BY MERCADO ASC");
        if($rs) {
            foreach($rs as $row) {
                $a = new produtoModel();
                $a->popular($row);
                
                $arr[] = $a;
            }
        }
        return $arr;
    }
    
    protected function popular($row) {
        $this->mercado = $row['MERCADO'];
        $this->produto = $row['PRODUTO_SGP'];
    }
    
}