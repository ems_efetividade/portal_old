<?php

require_once('lib/appConexao.php');

class psShareModel extends appConexao {
	
	private $idSetor;
	private $idLinha;
	private $mercado;
	
	
	public function setIdSetor($valor) {
		$this->idSetor = $valor;
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;
	}
	
	public function setMercado($valor) {
		$this->mercado = $valor;
	}

	public function getConfiguracao() {
		$rs = $this->executarQueryArray("SELECT * FROM PS_CONF");

		foreach($rs as $conf) {
			$arr[$conf['PARAMETRO']] = $conf['VALOR'];
		}

		return $arr;
	}
	
	public function getTrimestre() {
		return $this->executarQueryArray("EXEC proc_psn_getTrimestre");	
	}
	
	public function getProdutosMercado($mercado, $idLinha) {
		$query = "EXEC proc_psn_listarProdutoMercado ".$mercado['ID_MERCADO'].", ".$idLinha."";
		
		return $this->executarQueryArray($query);	
	}

	public function getProdutoLab($mercado='') {
		$query = "EXEC proc_psn_selProdutoLab ".$mercado['ID_MERCADO']."";
		$rs = $this->executarQueryArray($query);
		return 	$rs[1]['PRODUTO'];
	}

	public function verificaEstrutura($idSetor=0) {
		$query = "SELECT COUNT(*) AS TOTAL FROM SETOR WHERE NIVEL = ".$idSetor."";
		$rs = $this->executarQueryArray($query);
		return $rs[1]['TOTAL'];	
	}

	public function getDadosGraficoPizza($idSetor, $mercado, $produto, $idLinha) {
		$produtos = implode(",", $produto);
		$query = "EXEC proc_psn_listarDadosGrafPizza ".$idSetor.", " .$mercado. ", '".$produtos."', ".$idLinha."";
		return $this->executarQueryArray($query);
	}

	public function getDadosGraficoDrill($idSetor, $mercado, $produto) {
		$query = "EXEC proc_psn_listarDadosGraficoDrill ".$idSetor.", ".$mercado.", ".$produto."";
		return $this->executarQueryArray($query);
	}

	public function getIdSetorEstrutura($idLinha) {
		$rs = $this->executarQueryArray("SELECT top 1 ID_SETOR FROM vw_colaboradorSetor WHERE ID_LINHA = " . $idLinha . " AND ID_PERFIL = 4");
		return $rs[1]['ID_SETOR'];
	}

	public function getTabelaDados($idSetor=0, $mercado='', $produtos, $listarEquipe=0) {

		/* Cria os produtos */
		foreach($produtos as $k => $produto) {
			if($produto['ID_PRODUTO'] != "") {
				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.SH_TRM01 ELSE 0 END) * 100 AS PROD".$k."_TRM01";
				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.SH_TRM00 ELSE 0 END) * 100 AS PROD".$k."_TRM00";
				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.EVO_SH_TRM00 ELSE NULL END) * 100 AS  PROD".$k."_PEN";

				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.PX_TRM01 ELSE 0 END) AS PROD".$k."_PX_TRM01";
				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.PX_TRM00 ELSE 0 END) AS PROD".$k."_PX_TRM00";
				$campos .=  ",MAX(CASE WHEN A.ID_PRODUTO = ".$produto['ID_PRODUTO']." THEN A.EVO_PX_TRM00 ELSE NULL END) AS  PROD".$k."_PX_PEN";
				$produtosw[] = $produto['ID_PRODUTO'];
			}
		}

		$produtosw = implode(",", $produtosw);

		/* mostra a estrutura do setor */
		$query = "EXEC proc_psn_tabela " . $idSetor . ", " . $mercado . ", '".$campos."', '".$produtosw."', ".$listarEquipe."";

		//echo $query;


		$dados['dados'] = $this->executarQueryArray($query);
		$dados['header'] = $this->getTrimestre();

		return $dados;
	}

	public function getPrincProdutos($mercado='', $idLinha) {
		$conf = $this->getConfiguracao();

		$query = "SELECT PRODUTO, PRODUTO_BU FROM PS_FATO_MERCADO WHERE MERCADO = '".$mercado."' AND PRODUTO_BU <= ".$conf['MAX_PROD']." GROUP BY PRODUTO, PRODUTO_BU ORDER BY PRODUTO_BU";
		$query = "EXEC proc_psn_listarPrincipaisProdutos ".$mercado['ID_MERCADO'].", ".$idLinha.",  ".$conf['MAX_PROD']." ";
		//echo $query;
		return $this->executarQueryArray($query);
	}

	public function getResumoCategoria($idSetor, $mercado, $produto, $periodo) {
		//print_r($periodo);
		$periodo = explode("|", $periodo);

		$trim = $this->getTrimestre();

		if($periodo[1] == 'TRM01') {
			$cab = array(
				'TRM01' => $trim[1]['TRM02'],
				'TRM00' => $trim[1]['TRM01']
			);
			
		} else {
			$cab = array(
				'TRM01' => $trim[1]['TRM01'],
				'TRM00' => $trim[1]['TRM00']
		);
		}

		$query = "EXEC proc_psn_listarResumoCategoria ".$idSetor.", '".$mercado."', '".$produto."', '".$periodo[0]."', '".$periodo[1]."' ";

		return $this->executarQueryArray($query);
	}

	public function getMedicosPeriodo($idSetor, $mercado, $produto, $periodo) {
		//print_r($periodo);
		$periodo = explode("|", $periodo);

		$trim = $this->getTrimestre();

		if($periodo[1] == 'TRM01') {
			$cab = array(
				'TRM01' => $trim[1]['TRM02'],
				'TRM00' => $trim[1]['TRM01']
			);
			
		} else {
			$cab = array(
				'TRM01' => $trim[1]['TRM01'],
				'TRM00' => $trim[1]['TRM00']
		   );
		}

		$query  = "EXEC proc_psn_listarMedicosPeriodo ".$idSetor.", '".$mercado."', '".$produto."', '".$periodo[0]."', '".$periodo[1]."' ";
		//echo $query;
		$rs  = $this->executarQueryArray($query);
		
		$dados['header'] = $cab;
		$dados['dados']  = $rs;

		return $dados;
	}

	public function getExportarMedicosPeriodo($idSetor, $mercado, $produto, $periodo) {
		//print_r($periodo);
		$periodo = explode("|", $periodo);

		$trim = $this->getTrimestre();

		if($periodo[1] == 'TRM01') {
			$cab = array(
				'TRM01' => $trim[1]['TRM02'],
				'TRM00' => $trim[1]['TRM01']
			);
			
		} else {
			$cab = array(
				'TRM01' => $trim[1]['TRM01'],
				'TRM00' => $trim[1]['TRM00']
		   );
		}

		$query  = "EXEC proc_psn_exportarListaMedico ".$idSetor.", '".$mercado."', '".$produto."', '".$periodo[0]."', '".$periodo[1]."' ";
//echo $query;
		$rs  = $this->executarQueryArray($query);
		
		$dados['header'] = $cab;
		$dados['dados']  = $rs;

		return $dados;
	}

    public function getFichaMedico($idCRM, $idLinha, $periodo) {

        $periodo = explode("|", $periodo);
        $trim = $this->getTrimestre();

        if($periodo[1] == 'TRM01') {
            $cab = array(
                'TRM01' => $trim[1]['TRM02'],
                'TRM00' => $trim[1]['TRM01']
            );

        } else {
            $cab = array(
                'TRM01' => $trim[1]['TRM01'],
                'TRM00' => $trim[1]['TRM00']
            );
        }

        $query2 = "EXEC proc_psn_listarEnderecoFichaMedico '".$idCRM."'";
        $query  = "EXEC proc_psn_listarFichaMedico '".$idCRM."', '".$idLinha."',  '".$periodo[0]."', '".$periodo[1]."' ";



        $rs  = $this->executarQueryArray($query);
        $rs2  = $this->executarQueryArray($query2);

        $dados['header'] = $cab;
        $dados['dados']  = $rs;
        $dados['enderecos']  = $rs2;

        return $dados;
    }


	public function getFichaMedicoSigno($idCRM, $idLinha, $periodo) {

		$periodo = explode("|", $periodo);
		$trim = $this->getTrimestre();

		if($periodo[1] == 'TRM01') {
			$cab = array(
				'TRM01' => $trim[1]['TRM02'],
				'TRM00' => $trim[1]['TRM01']
			);

		} else {
			$cab = array(
				'TRM01' => $trim[1]['TRM01'],
				'TRM00' => $trim[1]['TRM00']
		   );
		}

		$query2 = "EXEC proc_psn_listarEnderecoFichaMedico '".$idCRM."'";
		$query  = "EXEC proc_psn_listarFichaMedicoSigno '".$idCRM."', '".$idLinha."',  '".$periodo[0]."', '".$periodo[1]."' ";
	
		$rs  = $this->executarQueryArray($query);
		$rs2  = $this->executarQueryArray($query2);

		$dados['header'] = $cab;
		$dados['dados']  = $rs;
		$dados['enderecos']  = $rs2;

		return $dados;
	}

	public function getExportarFichaMedico($idCRM, $periodo) {

		$periodo = explode("|", $periodo);
		$trim = $this->getTrimestre();

		if($periodo[1] == 'TRM01') {
			$cab = array(
				'TRM01' => $trim[1]['TRM02'],
				'TRM00' => $trim[1]['TRM01']
			);

		} else {
			$cab = array(
				'TRM01' => $trim[1]['TRM01'],
				'TRM00' => $trim[1]['TRM00']
		   );
		}

		//$query2 = "EXEC proc_psn_listarEnderecoFichaMedico '".$idCRM."'";
		$query  = "EXEC proc_psn_exportarFichaMedico ".$idCRM." ";
	
		//echo $query;  
		$rs  = $this->executarQueryArray($query);

		$dados['header'] = $trim;
		$dados['dados']  = $rs;

		return $dados;
	}

	public function getCamposPesquisaMedico() {
		$query = 'EXEC proc_psn_pesquisarMedicoCampos';
		$rs  = $this->executarQueryArray($query);
		return $rs;
	}

	public function pesquisarMedico($idSetor, $criterio='', $de=0, $ate=20) {
		$query = "EXEC proc_psn_pesquisarMedico ".$de.", ".$ate.", ".$idSetor.", '".$criterio['criterio']."', '".$criterio['campo']."', 0";
		$query2 = "EXEC proc_psn_pesquisarMedico ".$de.", ".$ate.", ".$idSetor.", '".$criterio['criterio']."', '".$criterio['campo']."', 1";

		$a = explode(" ", $criterio['criterio']);
		

		$crit = '';
		if(count($a) > 1) {
			foreach($a as $k) {
				$crit .= "OR LIKE '%" . $k .  "%' ";
			}
		}


		$rsCont  = $this->executarQueryArray($query2);

		$rs  = $this->executarQueryArray($query);

		$dados['dados'] = $rs;
		$dados['count'] = $rsCont[1]['CONTAR'];

		return $dados;
	}

	public function pesquisarCriterio($criterio) {
		$setor = appFunction::dadoSessao('setor');

		if(appFunction::dadoSessao('perfil') == 0) {
			$rsSetor = $this->executarQueryArray("SELECT SETOR FROM vw_colaboradorSetor WHERE ID_LINHA = ".appFunction::dadoSessao('id_linha')." AND NIVEL_PERFIL = 1");
			if($rsSetor) {
				$setor = $rsSetor[1]['SETOR'];
			}
		}

		$query = "WITH SETO AS (
					SELECT ID_SETOR, NIVEL, SETOR FROM SETOR WHERE SETOR = '".$setor."'
					UNION ALL
					SELECT A.ID_SETOR, A.NIVEL, A.SETOR FROM SETOR A INNER JOIN SETO ON SETO.ID_SETOR = A.NIVEL
				)
		
				SELECT TOP 10 PS_FATO_PESQUISA_MEDICO.* FROM
					PS_FATO_PESQUISA_MEDICO 
					INNER JOIN PS_DIM_CRM_MEDICO B ON PS_FATO_PESQUISA_MEDICO.ID_CRM = B.ID_CRM
					INNER JOIN SETO C ON C.ID_SETOR = B.ID_SETOR
				WHERE 1 = 1 ";

		$dados = explode(' ', trim($criterio));

		$search = '';
		$asp = '"';
		
		if ($criterio != "") {
			//foreach($dados as $valor) {
			//	$search .= ' AND CONTAINS (SEARCH, '. "'" . $asp .  $valor . $asp . "'" .  ')';
			//}
			$search = "AND CONTAINS (SEARCH, '\"" . implode('" AND "', $dados)  . "\"')";
		}

		$query = $query . $search;
		return  $this->executarQueryArray($query);					
	}

	public function buscarMedicos($criterio, $pag=1, $maxPorPag=20) {
		$criterio = str_replace(" - ", " ", $criterio);
		$dados = explode(' ', trim($criterio));
		$setor = appFunction::dadoSessao('setor');


		if(appFunction::dadoSessao('perfil') == 0) {
			$rsSetor = $this->executarQueryArray("SELECT SETOR FROM vw_colaboradorSetor WHERE ID_LINHA = ".appFunction::dadoSessao('id_linha')." AND NIVEL_PERFIL = 1");
			if($rsSetor) {
				$setor = $rsSetor[1]['SETOR'];
			}
		}
		
		$search = '';
		$asp    = '"';

		if($criterio != "") {
			if (count($dados > 0)) {
				$search = "AND CONTAINS (SEARCH, '\"" . implode('" AND "', $dados)  . "\"')";
			}	
		}

		$queryContar = "
			;WITH SETO AS (
				SELECT ID_SETOR, NIVEL, SETOR FROM SETOR WHERE SETOR = '".$setor."'
				UNION ALL
				SELECT A.ID_SETOR, A.NIVEL, A.SETOR FROM SETOR A INNER JOIN SETO ON SETO.ID_SETOR = A.NIVEL
			)
			
			SELECT COUNT(*) AS TOTAL FROM PS_FATO_PESQUISA_MEDICO
			INNER JOIN PS_DIM_CRM_MEDICO B ON PS_FATO_PESQUISA_MEDICO.ID_CRM = B.ID_CRM
			INNER JOIN SETO C ON C.ID_SETOR = B.ID_SETOR
			
			WHERE 1=1 ".$search;


			$rsContar = $this->executarQueryArray($queryContar . $search);
			//if($rsContar[1]['TOTAL'] > 0) {
				//$paginas = ceil($rsContar[1]['TOTAL'] / $maxPorPag);
				$inicio = 1 + (($pag * $maxPorPag) - $maxPorPag);
				$fim = $inicio + $maxPorPag -1;
			//}

		

		
		$query = "

		;WITH SETO AS (
			SELECT ID_SETOR, NIVEL, SETOR FROM SETOR WHERE SETOR = '".$setor."'
			UNION ALL
			SELECT A.ID_SETOR, A.NIVEL, A.SETOR FROM SETOR A INNER JOIN SETO ON SETO.ID_SETOR = A.NIVEL
		)
		
		SELECT * FROM (
			SELECT
				ROW_NUMBER() OVER(ORDER BY NOME ASC) AS INDICE,
				PS_FATO_PESQUISA_MEDICO .* 
			FROM 
				PS_FATO_PESQUISA_MEDICO 
				INNER JOIN PS_DIM_CRM_MEDICO B ON PS_FATO_PESQUISA_MEDICO .ID_CRM = B.ID_CRM
				INNER JOIN SETO C ON C.ID_SETOR = B.ID_SETOR 
			WHERE 1=1 ".$search."

		) K  WHERE K.INDICE BETWEEN ".$inicio." AND ".$fim;
		
		return array(
				'dados' =>  $this->executarQueryArray($query),
				'total' => $rsContar[1]['TOTAL'],

				'query' => $query,

				'wow' => appFunction::dadoSessao('setor'),
				'paginas' => array(
					'atual' => $pag,
					'prox'  => $pag+1,
					'ant'   => $pag-1,
					'total_registros' => $rsContar[1]['TOTAL']
				)
			);
	}

	public function contarMedicosSetor() {
		$setor = appFunction::dadoSessao('setor');
		if(appFunction::dadoSessao('perfil') == 0) {
			$rsSetor = $this->executarQueryArray("SELECT SETOR FROM vw_colaboradorSetor WHERE ID_LINHA = ".appFunction::dadoSessao('id_linha')." AND NIVEL_PERFIL = 1");
			if($rsSetor) {
				$setor = $rsSetor[1]['SETOR'];
			}
		}

		$query = "
			;WITH SETO AS (
				SELECT ID_SETOR, NIVEL, SETOR FROM SETOR WHERE SETOR = '".$setor."'
				UNION ALL
				SELECT A.ID_SETOR, A.NIVEL, A.SETOR FROM SETOR A INNER JOIN SETO ON SETO.ID_SETOR = A.NIVEL
			)
						
			SELECT COUNT(*) AS TOTAL FROM (
				SELECT
					ROW_NUMBER() OVER(ORDER BY NOME ASC) AS INDICE,
					PS_FATO_PESQUISA_MEDICO.* 
				FROM 
					PS_FATO_PESQUISA_MEDICO 
					INNER JOIN PS_DIM_CRM_MEDICO B ON PS_FATO_PESQUISA_MEDICO .ID_CRM = B.ID_CRM
					INNER JOIN SETO C ON C.ID_SETOR = B.ID_SETOR 
				WHERE 1=1
			
			) K";

		$rs = $this->executarQueryArray($query);
		return $rs[1]['TOTAL'];
	}

	public function listarEspMedico($idCRM) {
		$dados =  $this->executarQueryArray("SELECT DISTINCT B.ESP FROM PS_DIM_CRM_MEDICO A INNER JOIN PS_DIM_ESPECIALIDADE B ON B.ID = A.ID_ESPECIALIDADE	WHERE A.ID_CRM = " . $idCRM);
		if($dados) {
			foreach($dados as $dado) {
				$aa[] = $dado['ESP'];
			}
		}
		return $aa;
	}

}

?>