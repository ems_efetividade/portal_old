<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Tabela PRODUTO_SGP</b></h3>
            <hr>
            
            <p>
                <a href="<?php echo APP_CAMINHO ?>admin/produtoForm" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> 
                    Adicionar
                </a>
                <a href="<?php echo APP_CAMINHO ?>admin/produtoImportarForm" class="btn btn-default pull-right">
                    <span class="glyphicon glyphicon-import"></span> 
                    Importar Arquivo .csv
                </a>
            </p>
            
            <table class="table table-striped table-condensed">
                <tr>
                    <th><small>Mercado</small></th>
                    <th><small>Produto</small></th>
                    <th><small>Ação</small></th>
                </tr>
                    
                <?php foreach($listaProduto as $produto) { ?>
                <tr>
                    <td><small><?php echo $produto->getMercado() ?></small></td>
                    <td><small><?php echo $produto->getProduto() ?></small></td>
                    <td>
                        <a href="<?php echo APP_CAMINHO ?>admin/produtoExcluir/<?php echo base64_encode($produto->getMercado()) ?>/<?php echo base64_encode($produto->getProduto()) ?>">Excluir</a>
                    </td>
                </tr>
                <?php } ?>
            </table>

        </div>
    </div>
</div>