<?php require_once('app/view/admin/adminHeader.php');  ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Importar ArquivAo</b></h3>
            <hr>
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <form method="Post" action="<?php echo APP_CAMINHO ?>admin/rankProdutoImportar" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="exampleInputFile">Arquivo</label>
                            <input name="arqImportar" type="file" id="exampleInputFile">
                            <p class="help-block">Atenção: a tabela será apagada e será importado o conteúdo desse arquivo.</p>
                        </div>
                        
                        <?php if(!empty($erro)) { ?>
                        <div class="alert alert-danger">
                            <p><b><?php echo $erro ?></b></p>
                        </div>
                        <?php } ?>
                        <button type="submit" class="btn btn-default">Salvar</button>
                    </form>
                    
                </div>
            </div>
            
            
            
            
        </div>
    </div>
</div>