<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Tabela PS_TRIMESTRE</b></h3>
            <hr>
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <form method="Post" action="<?php echo APP_CAMINHO ?>admin/trimestreSalvar">
                        
                        <input type="hidden" name="txtId" value="<?php echo base64_encode($dado->getId()); ?>" />

                        
                        <div class="form-group">
                          <label for="exampleInputEmail1">Código</label>
                          <input type="text" class="form-control" name="txtCodigo" id="exampleInputEmail1" placeholder="" value="<?php echo $dado->getCodigo() ?>">
                        </div>
                        
                        <div class="form-group">
                          <label for="exampleInputPassword1">Trimestre</label>
                          <input type="text" class="form-control" name="txtTrimestre" id="exampleInputPassword1" placeholder="" value="<?php echo $dado->getTrimestre() ?>">
                        </div>
                  
                        
                        <h4 class="text-danger"><?php echo $erro ?></h4>
                        
                        <button type="submit" class="btn btn-default">Salvar</button>
                        <a href="<?php echo APP_CAMINHO ?>admin/trimestre" class="btn btn-default">Voltar</a>
                    </form>
                    
                </div>
            </div>
            
            
            
            
        </div>
    </div>
</div>