<?php 
	require_once('lib/appConf.php'); 
	require_once('lib/appFunction.php'); 
        appFunction::validarSessao();
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" /> 

<title>Portal EMS Prescrição</title>

<!--Plugin javascript do jquery e bootstrap-->
    <script src="/../../../../plugins/jquery/jquery-1.12.1.js" type="text/javascript"></script>
<script src="<?php echo APP_CAMINHO ?>plugins/jquery-ui/jquery-ui.js"></script>
<script src="<?php echo APP_CAMINHO ?>plugins/bootstrap/js/bootstrap.min.js"></script>

<script src="../../../plugins/highchart/js/highcharts.js"></script>
<script src="../../../plugins/highchart/js/highcharts-more.js"></script>
<script src="../../../plugins/highchart/js/modules/data.js"></script>
<script src="../../../plugins/highchart/js/modules/drilldown.js"></script>

<script src="<?php echo EMS_URL ?>/plugins/scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js" type="text/javascript"></script>
<link   href="<?php echo EMS_URL ?>/plugins/scrollbar-plugin/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css"/>

<script src="<?php echo APP_CAMINHO ?>public/js/index.js"></script>
<script>
$(window).ready(function(e) {
    //$('#menuPrescription').addClass('nav-hide');
});
$(document).ready(function(e) {
	//$('#menuPrescription').addClass('nav-hide');
	
  
});
</script>
<style>

.nav-hide {
	display:none !important;	
}
.navbar .divider-vertical {
height: 50px;
margin: 0 9px;
border-right: 1px solid #ffffff;
border-left: 1px solid #f2f2f2;
}

.navbar-inverse .divsider-vertical {
border-right-color: #222222;
border-left-color: #111111;
}

@media (max-width: 767px) {
.navbar-collapse .nav > .divider-vertical {
    display: none;
  }
}

.modal-open {
	padding:0px !important;
}	
</style>
<link rel="shortcut icon" href="<?php echo APP_CAMINHO ?>public/img/liferay.ico" type="image/x-icon" />


<!--Folhas de estilo do bootstrap, das fontes e do index-->
<link href="<?php echo APP_CAMINHO ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo APP_CAMINHO ?>plugins/bootstrap/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css" />
<link href="<?php echo APP_CAMINHO ?>public/css/fonts.css" rel="stylesheet" type="text/css" />
<link href="<?php echo APP_CAMINHO ?>public/css/index.css" rel="stylesheet" type="text/css" />
</head>

<body>



<div id="menuPrescription" class="navbar-fixed-top nav-hide" style="background-color:#FFF;border-bottom:1px solid #ddd;margin-bottom:0px;">

    <div style="padding-right:15px;padding-left:15px;">
    
    
    <div class="ppanel panel-default">
	<div class="ppanel-body" style="padding:15px;padding-bottom:2px">
		<div class="table-responesive">
        <small>
            <table width="100%" class="taable taable-bordered taable-striped">
              <tr>
                <td rowspan="3" width="28%">
                   
                   <h4 class="text-primary"><b>
                   <img height="26" src="<?php print APP_CAMINHO ?>public/img/ems.png" />
                   Mercado de <?php print $nomeMercado ?></b></h4>
                   
                    </td>
                <td colspan="4" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Trimestre Anterior (%)
                     </strong></p></td>
                <td colspan="4" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Trimestre Atual (%)</strong>
                    </p></td>
                <td colspan="4" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Penetracao (P.P)</strong>
                    </p></td>
              </tr>
              <tr class="info">
                <?php print $linhaPrincProdutos; ?>
              </tr>
              <tr class="warning">
                <?php print $linhaBrasil; ?>
              </tr>
              <!--<tr class="">-->
                
              <!--</tr>-->
              
             
            </table>
        </small>
        </div>
	</div>
</div> -->

</div>

</div>

<!--Barra de Navegação-->

<!-- 
<nav class="navbar navbar-default navbar-fixehd-top nav-collapse" role="navigation" style="margin-bottom:3px;font-size:14px;" id="menuPresscription">
            <div class="navbar-header">
                <button type="button" id="button-collapsed" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
             <a class="navbar-brand" href="#"><img height="30" class="pull-left" src="<?php print APP_CAMINHO ?>public/img/write_document.png"  /> Prescription Share Online</a>
            </div>
            
            <div class="collapse navbar-collapse" id="navbar-collapse-1" >
            	<ul class="nav navbar-nav navbar-right">
                	<li>
                    
            <form class="navbar-form navbar-right" id="formSelMercado" method="post" action="<?php echo APP_CAMINHO ?>home/selecionar"><span>Mercado: </span>
               <select class="form-control" id="comboMercado" name="mercado">
                	<option>::SELECIONE O MERCADO::</option>	
                  	<?php print $comboMercado ?>
                </select>
                
            </form>
                    
                    
                    </li>
                    <li class="divider-vertical"></li>
                	<li><a href="#" id="btnPesquisarMedico"><span class="glyphicon glyphicon-search"></span> Pesquisar Médicos</a></li>
                	
                </ul>
				<div class="pull-right">
                
               
              
              </div>
            </div>
        </nav>



-->






<!--MODAL Aguarde-->
<div class="modal fade" style="z-index:100000;" id="modalMsgBox" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content ">
      
       <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-warning-sign"></span> Aviso</h5>
      </div>
      
      <div class="modal-body">
      
      	<div class="row">
        	<div class="col-lg-3">
            	<p class="text-center text-danger" style="font-size:500%;"><span class="glyphicon glyphicon-remove"></span><p>
            </div>
            <div class="col-lg-9">
            	<h5><p class="text-center" id="msgboxTexto"></p></h5>
            </div>
        </div>
      
      	
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>

    </div>
  </div>
</div>

<!--MODAL Aguarde-->
<div class="modal fade" style="z-index:100000;" id="modalAguarde" daata-backdrop="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content ">
      
      
      <div class="modal-body">
      	<p class="text-center"><strong>Processando...</strong></p>
        <!--<h5 class="text-center"><small>(Esta operação pode demorar alguns minutos.)</small></h5>-->
        
        <div class="progress">
          <div class="progress-bar progress-bar-primary progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
            <span class="sr-only"></span>
          </div>
        </div>
        
      </div>

    </div>
  </div>
</div>



<!--MODAL LOGOUT-->
<div class="modal fade" id="modalLogout" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Sair do sistema</h5>
      </div>
      
      <div class="modal-body">
      	<div class="row">
        	<div class="col-lg-4 text-center text-danger">
            	<img class="img-responsive" src="<?php echo APP_CAMINHO ?>public/img/question.png" />
            </div>
            <div class="col-lg-8" style="height:128px;">
            	<h4 class="text-center">Deseja sair do sistema?</h4>
            </div>
        </div>
      </div>
          

      <div class="modal-footer">
      	<form action="<?php echo APP_CAMINHO ?>login/logout" method="POST">
        <button type="button" class="btn btn-default btn-md" data-dismiss="modal">
        	<span class="glyphicon glyphicon-remove"></span> Fechar
        </button>
        
        <button type="submit" class="btn btn-info btn-md">
        	<span class="glyphicon glyphicon-off"></span> Sair do sistema
        </button>
        </form>
      </div>
    </div>
  </div>
</div>

