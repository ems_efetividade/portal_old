<?php require_once('app/view/headerView.php'); ?>



<div style="padding-left:15px;padding-right:15px">

    

    <div class="row">

        <?php require_once('menuMercado.php') ?>

        <div class="col-lg-10 col-md-12 panel-right">
        
                <br>
            <!-- nome do mercado -->
            <div class="row" style="margin-bottom:10px;">
                <div class="col-lg-12">
                    <h4 style="margin-top:0px;"><?php echo $mercadoSelecionado['MERCADO'] ?> <small>Prescription Share - <?php echo appFunction::dadoSessao('unidade_neg') ?> </small><span class="pull-right"><small><small>Fonte: Audit Pharma</small></small></span></h4>
                </div>
            </div>


            <div class="row">

                <!-- coluna PRINCIPAIS PRODUTOS -->
                <div class="col-lg-4 col-md-4" style="padding-right:2px">

                    <div class="panel panel-default" style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <small><span class="glyphicon glyphicon-star"></span> Principais Produtos</small>
                            </h5>
                        </div>
                        
                        <div class="panel-body" style="min-height:257px">
                            <form id="formMudarProduto" method="post" action="<?php echo APP_CAMINHO ?>home/alterarProduto">
                                <input type="hidden" name="mercado" value="<?php echo $mercadoSelecionado['ID_MERCADO'] ?>">

                            <?php  foreach($tabelaDados['princProd'] as $prod) { ?>
                                <select class="form-control input-sm" name="produtos[]">
                                <option value="">(NENHUM)</option>
                                <?php  foreach($mercadoProdutos as $mercProd) {?>
                                    <option <?php echo ($prod['ID_PRODUTO'] == $mercProd['ID_PRODUTO']) ? 'style="font-weight: bold;"' : 'style="color:#bbb"' ?>  value="<?php echo $mercProd['ID_PRODUTO'].'|'.$mercProd['PRODUTO']  ?>" <?php echo ($prod['ID_PRODUTO'] == $mercProd['ID_PRODUTO']) ? 'selected' : '' ?>><?php echo $mercProd['PRODUTO']  ?></b></option>
                                <?php } ?>     
                                </select>
                                <p></p>
                            <?php } ?>

                        <button type="button" class="btn-block btn btn-sm btn-warning" id="btnAlterarProduto"><span class="glyphicon glyphicon-refresh"></span> Recalcular</button>
                        </form>
                        </div>
                    
                    </div>

                    <!-- coluna GRAFICO PIZZA -->
                    <div class="panel panel-default"  style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <small class="text-primary">
                                    <span class="glyphicon glyphicon-stats"></span> Participação no Mercado - <?php print $trmAtu ?>
                                </small>
                            </h5>
                        </div>
                        <div class="panel-body" style="padding:0px">
                        <!-- <div id="container" style="min-width: 310px; height: 209px; max-width: 600px; margin: 0 auto"></div> -->
                        <div id="graf-pizza" style="min-width: 310px; height: 209px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    
                </div>
            
                <div class="col-lg-8 col-md-8"  style="padding-left:2px">
    
                    <div class="panel panel-default"  style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title"><small><span class="glyphicon glyphicon-stats"></span> Participação de <b><?php print $produtoEMS ?></b> por região/setor</small></h5>
                        </div>
                        <div class="panel-body" id="body-grafico">
                            
                            <div class="row">
                                
                                
                                <div class="col-lg-12">
                                    <!-- <div id="vcontainer3" style="min-width: 310px; height: 400px; margin: 0 auto"></div> -->
                                    <div id="graf-drill">

                                        <div id="div-aguarde" class="text-center" style="width: 96%; height: 400px; margin: 0 auto; position:absolute; background:rgba(255, 255, 255, 0.8); z-index:999">
                                            <div style="margin-top:100px">
                                                <img height="70" src="../public/img/loading2.gif" />
                                                <p><small><b>Processando...</b></small></p>
                                            </div>
                                        </div>   
                                        
                                        <!-- <div id="container9" style="width: 100%; height: 400px; margin: 0 auto"></div> -->
                                        <div id="gr">
                                        <!--<div id="trafego" class="charts" style="width: 100%; height: 447px; margin: 0 auto"></div>-->
                                        </div>
                                        
                                    </div>
                                    <!--<p class="pull-right"><small>Fonte: Audit Pharma</small></p>
                                    <p class="pull-left"><small>Linha pontilhada = Market Share Brasil (*)</small></p>
                                    -->
                                </div>
                            </div>

                            <div class="row" style="position:relative; top:-447px">
                                <div class="col-lg-7 text-left">
                                    <!--<h4 style="margin:0px;"><span id="prod-graf"><?php echo $dadosView['produtoLab'] ?></span></h4>
                                    <h5><small>Prescription Share - Visão por Setores - <b><?php echo $dadosView['trimestre'][1]['TRM00'] ?></b></small></h5>
                                -->
                                </div>
                                <div class="col-lg-5 text-right">
                                    <button style="" class="btn btn-sm btn-default" id="btnOpa"><< Voltar</button>
                                    <select class="input-sm" id="cmbProdutoLab" name="cmbProdutoLab">
                                    <?php   foreach($mercadoProdutos as $mercProd) {?>
                                        <option <?php echo ($dadosView['produtoLab'] == $mercProd['PRODUTO']) ? 'style="font-weight: bold;"' : 'style="color:#bbb"' ?> value="<?php echo $mercProd['ID_PRODUTO']  ?>" <?php echo ($dadosView['produtoLab'] == $mercProd['PRODUTO']) ? 'selected' : '' ?>><?php echo $mercProd['PRODUTO']  ?></option>
                                    <?php } ?>
                                    
                                    </select>
                                </div>
                            </div>

                        </div>
                        
                    </div>

                </div>


                
        
            </div>

            <?php 

            //$this->set('dados', 'dads');
            //$this->render('vTabela');

            echo $tabelaPxShare;
            ?>

            
    </div>

    

    
</div>
        </div>

</div>




</div>







<!--MODAL LISTA MÉDICOS-->
<div class="modal fade" id="modalListaMedico"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Médicos</h5>
      </div>
      
      <div class="modal-body max-height" style="overflow-x:auto">
      
      
      </div>
          

      <div class="modal-footer panel-footer">
      <a href="#" class="btn btn-primary btn-sm" data-setor="" data-produto="" data-mercado="" data-periodo="" id="btn-exportar-lista-medico" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Exportar em CSV
            </a> 
        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>

      </div>
    </div>
  </div>
</div>






<!--MODAL PESQUISAR-->
<div class="modal" id="modalFichaMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-backdrop="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha do Médico</h5>
      </div>
      
      <div class="modal-body max-height">
 
 		asdasd
 
      </div>

      <div class="modal-footer">
           <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""  id="btn-exportar-ficha-medico" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Exportar em CSV
            </a>   	
      
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>
<script src="<?php echo APP_CAMINHO ?>plugins/echarts.min.js"></script>


<script>

</script>





<script>
var btnVoltarGrafico = {id : "1", cor: "2"};
var vAudit = 'sh';


mostrarAudit('sh', 'table-ps');
 
$(".btn-group button").click(function() {
    $(".btn-group button").removeClass('btn-primary');
    $(this).toggleClass('btn-default btn-primary');
});

function mostrarAudit(audit, container) {
    vAudit = audit;
    $('#' + container + ' .audit').hide();
    $('#' + container + ' .ps-'+audit).show();
    $('#' + container + ' .radio-'+audit).prop('checked', true);
}






$(window).ready(function(e){
    resizePanelLeft();
});



refreshChart('', '', '');

$('#cmbProdutoLab').change(function(e){
    refreshChart('', '', '');
    $('#prod-graf').html($(':selected', this).text());
});

$('.listar-medico').unbind().click(function(e){
    e.preventDefault();
    listarMedico($(this));
});

function listarMedico(obj) {

    var idSetor = 0;
    var mercado = 0;
    var produto = 0;
    var periodo = 0;

    idSetor = obj.data('setor');
    mercado = obj.data('mercado');
    produto = obj.data('produto');
    periodo = obj.data('periodo');

    //console.log(idSetor + ' - ' + mercado + ' - ' + produto + ' - ' + periodo);
    ///console.log(obj.data());

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/listarMedicos',
        data: {
            setor: idSetor,
            mercado: mercado,
            produto: produto,
            periodo: periodo
        },
        
        beforeSend: function() {
            modalAguarde('show');
        },
        
        success: function(retorno) {
            modalAguarde('hide');
            $('#modalListaMedico .modal-body').html(retorno);

            $('#modalListaMedico #btn-exportar-lista-medico').attr('data-setor', idSetor);
            $('#modalListaMedico #btn-exportar-lista-medico').attr('data-mercado', mercado);
            $('#modalListaMedico #btn-exportar-lista-medico').attr('data-produto', produto);
            $('#modalListaMedico #btn-exportar-lista-medico').attr('data-periodo', periodo);


            

            $('#modalListaMedico').modal('show');

            $('#btn-exportar-lista-medico').unbind().click(function(s){
                s.preventDefault();
                window.location.href = "<?php print APP_CAMINHO ?>home/exportarListaMedico/"+idSetor+"/"+mercado+"/"+produto+"/"+periodo+"";
            });

            mostrarAudit(vAudit, 'tb-medico')
            $(".btn-group button").click(function() {
                $(".btn-group button").removeClass('btn-primary');
                $(this).toggleClass('btn-default btn-primary');
            });


            
        }
    });  
}

function exportarListaMedico(obj) {
    var idSetor = obj.data('setor');
    var mercado = obj.data('mercado');
    var produto = obj.data('produto');
    var periodo = obj.data('periodo');

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/listarMedicos',
        data: {
            setor: idSetor,
            mercado: mercado,
            produto: produto,
            periodo: periodo
        },
        
        beforeSend: function() {

        },
        
        success: function(retorno) {
            $('#modalListaMedico .modal-body').html(retorno);
            $('#modalListaMedico').modal('show');
        }
    });  
}

setEchartPie();

function setEchartPie() {

option = {
    toolbox: {
        feature: {
            saveAsImage: {
                show: true,
                title: 'Exportar',
                name: 'PX_SHARE_PARTICIPACAO_' + $('#cmbProdutoLab :selected').text().replace(' ', '_')
            }
        }
    },
    tooltip : {
        trigger: 'item',
        textStyle: {
            fontSize:10
        },
        formatter : function(params) {

           param = params;
           return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + param.color + ';"></span> Share: ' + param.data.name;
       },
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        top: '10%',
        textStyle: {
            fontSize:9
        },
        data:<?php echo $serieGrafPizzaE['legend'] ?>
    },
    series : [
        {
            name: 'Mercado',
            type: 'pie',
            center: ['72%', '50%'],
            radius: ['0', '80%'],
            data: <?php echo $serieGrafPizzaE['dados'] ?>,
            label: {
               show: true,
               normal: {
                    position: 'inner',
                    formatter: '{c}<?php echo $config['SUFIXO'] ?>',
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

var pizza = echarts.init(document.getElementById('graf-pizza'));
    pizza.setOption(option, true);
}

var trafegoPassar = '';

function setEchart(egraf) {

var option = {};
var trafego = '';



option = {
    title: {
        text: $('#cmbProdutoLab :selected').text(),
        subtext: 'Prescription Share - Visão por Setores - <?php echo $dadosView['trimestre'][1]['TRM00'] ?>',
        itemGap: 6,
        
    },
    tooltip : {
        trigger: 'axis',
        textStyle:{
            fontSize: 10
        },
        axisPointer : {            
            type : 'shadow'       
        },
        formatter : function(params) {
            param = params[0];
            return param.axisValue + '<br>' + '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + param.color + ';"></span> Participação: <b>' + param.data[1] + '</b><?php echo $config['SUFIXO'] ?>';
        },
    },
    toolbox: {
        top: 40,
        feature: {
            saveAsImage: {
                show: true,
                title: 'Exportar',
                name: 'PX_SHARE_'+$('#cmbProdutoLab :selected').text().replace(' ', '_')
            },
            
        }
    },
    grid: {
        left: '3%',
        right: '4%',
        bottom: '1%',
        top: '20%',
        containLabel: true,

    },
    xAxis : [
        {
            type : 'category',
            
            axisTick: {
                alignWithLabel: true
            },
            axisLabel: {
                interval:0,
                fontSize:9,
            }
           
        }
    ],
    yAxis : [{
        type : 'value'
    }],
    
        series: egraf.data,
    };


    $('#gr').hide();
    $('#gr').html('');
    $('#gr').html('<div id="trafego" class="charts" style="width: 100%; height: 444px; margin: 0 auto"></div>');
    $('#gr').show();

   


trafego = echarts.init(document.getElementById('trafego'));
trafego.setOption(option, true);

   
    trafego.on('click', function(param){
        var criterios = '';
        criterios = param.seriesId.split(',');
        if(criterios[1] > 0) {
            refreshChart(criterios[0], param.color);
        }
    });

    var trafego = '';

    $('#div-aguarde').hide();
}

function refreshChart(idSetor, cor) {
    var par = [];
    var jsonData = '';

    par = [{
        'mercado' : <?php echo $mercadoSelecionado['ID_MERCADO'] ?>,
        'produto' : $('#cmbProdutoLab').val(),
        'id'      : idSetor,
        'cor'     : cor,
    }];

    jsonData =  JSON.stringify(par);

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/dadosDrillEchart',
        dataType: 'JSON',
        data: {
            dados: jsonData
        },
        
        beforeSend: function() {

        },
        
        success: function(retorno) {
            console.log(retorno);
           
            $('#btnOpa').unbind().click(function(e){
                refreshChart(retorno.voltar,cor);
            });

            setEchart(retorno);
        }
    }); 

}

function abrrirSetor(obj) {
    var icon = '...';

    var label    = obj.text();
    var id_setor = obj.data('id');
    var place    = obj.data('container');

    existe = $('.n-' + id_setor).length;

    if(existe == 0) {

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/tabelaEquipe',
        data: {
            id_setor: id_setor,
            mercado:  <?php echo $dadosView['mercado']['ID_MERCADO'] ?>,
            produtos: '<?php echo $dadosView['produtos'] ?>'
        },
        
        beforeSend: function() {
            obj.text(label + icon);
        },
        
        success: function(retorno) {
            obj.text(label);

            $('.n-' + id_setor).remove();
            $('#' + place).last().after(retorno);
            
            $('.n-' + id_setor + ' a.abrir-setor').on('click', function(r){
                r.preventDefault();
                abrrirSetor($(this));
            });

            $('.n-' + id_setor + ' a.listar-medico').unbind().click(function(e){
                e.preventDefault();
                listarMedico($(this));
            });

            mostrarAudit(vAudit, 'table-ps');
            resizePanelLeft();
        }
    }); 

    } else {
        $('.n-' + id_setor).remove();
        resizePanelLeft();
    }
}

$('#table-ps .abrir-setor').on('click', function(e){
        e.preventDefault();
        
        var label = $(this).text();
        var id_setor = $(this).data('id');
        var place = $(this).data('container');

        abrrirSetor($(this));
});

$(document).ready(function(e) {

    $('#btnAlterarProduto').unbind().click(function(e){
        modalAguarde('show');
        $('#formMudarProduto').submit();
    });

    $('.cmbMercadoConc').click(function(e) {

    //if($(this).val() != "::SELECIONE O MERCADO::") {
        var merc = $(this).data('val');
        var conc = '';
        $('select[name="cmbConcorrentes[]"]').each(function (index, value) { 
            conc = conc + $('#' + $(this).attr('id')).val() + ',';
        });
        
        abrirMercado(merc, conc);

        //modalAguarde('show');
        //$('#formSelMercado').submit();
    //}
    });

	/* ativa os tooltips mostrando o nome dos 
	   produtos nas colunas da tabela */
	$('[data-toggle="tooltip"]').tooltip();
	
	/* pega os dados dos setores ao clicar
	   na coluna de um GD, GR ou GN */
	$('.abrirSetor').click(function(e) {
    	id = $(this).attr('cod');
	  	abrirSetor(id);
    });
	
	/* função que lista os médicos */
	listaMedico();

});



function resizePanelLeft() {
    var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
    $('.panel-left').css('height', (screenSize));
    $('.resize-too').css('height', (screenSize)-200);
   // a = $('#container-colab').height();
   // $('#container-setores').css('height', (screenSize -  a)-100);
   // $('#final').css('height', (screenSize)-100);
} 

function listaMedico() {
	
	$('.listarMedico').click(function(e) {

                //alert('<?php print APP_CAMINHO ?>home/listarMedicos');

		/* pega os campos */
	  	var produto = $(this).attr('prod');
		var periodo = $(this).attr('per');
		var setor   = $(this).attr('cod');	
	   	var mercado = '<?php print $nomeMercado ?>';
		
	  	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/listarMedicos',
		  	data: {
				txtMercado: mercado,
				txtProduto: produto,
				txtPeriodo: periodo,
				txtIdSetor: setor	
			},
			beforeSend: function() {
				modalAguarde('show');
			},
			
		  	success: function(retorno) {
				modalAguarde('hide');
				$('#modalListaMedico .modal-body').html(retorno);
				
				$('#modalListaMedico').on('shown.bs.modal', function () {
                    $('#modalListaMedico').animate({ scrollTop: 0 }, 'slow');
				});

				$('#modalListaMedico').modal('show');
				
				$('#modalListaMedico').on('hidden.bs.modal', function (e) {
				  $('body').css('padding', '0px');
				})
				
				$('.fichaMedico').click(function(e) {
                    fichaMedico($(this).attr('crm'));
                });
				
				$('input[name="txtMercado"]').val(mercado);
				$('input[name="txtProduto"]').val(produto);
				$('input[name="txtPeriodo"]').val(periodo);
				$('input[name="txtIdSetor"]').val(setor);

                mostrarAudit(vAudit);
		   	}
		});      
    });	
}




function pesquisarMedico(idSetor, campo, criterio) {
	
	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/pesquisarMedico',
		  	data: {
				idSetor: idSetor,
				campo: campo,
				criterio: criterio	
			},
			
			beforeSend: function() {
				$('#resultadoPesquisaMedico').html('<div class="loading-gif"><p class="text-center"><small >Processando</small></p><img class="center-block" src="<?php print APP_CAMINHO ?>public/img/loading.gif" /></div>');
			},
			
		  	success: function(retorno) {
				modalAguarde('hide');
				$('#resultadoPesquisaMedico').html(retorno);
				$('.fichaMedico').click(function(e) {
                    fichaMedico($(this).attr('crm'));
                });
		   	}
	});  
}

function fichaMedico(crm) {
	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/fichaMedico',
		  	data: {
				CRM: crm
			},
			
			beforeSend: function() {
				//modalAguarde('show');
			},
			
		  	success: function(retorno) {
				//modalAguarde('hide');
				$('#modalFichaMedico .modal-body').html(retorno);
				$('#modalFichaMedico').modal('show');
				
				$('#btnImprimirPDF').attr('href', '<?php print APP_CAMINHO ?>home/pdf/'+crm);
		   	}
	}); 
}

function abrirSetor(id) {
	
	if($('#'+id).html() != "") {
		
		$('td[cod="'+id+'"] .loading').show();
		$('#'+id).slideUp(100, function(e) {
			$('#'+id).html("");
			$('td[cod="'+id+'"] .loading').hide();
		});
		
	} else {
	
		$.ajax({
			type: "POST",
			url: '<?php print APP_CAMINHO ?>home/selecionarSetor',
			data: {
				idSetor: id,
				mercado: '<?php print $nomeMercado ?>'
			},
			beforeSend: function(e) {
				$('td[cod="'+id+'"] .loading').show();
			},
			success: function(retorno) {
				$('td[cod="'+id+'"] .loading').hide();
				$('#'+id).hide();
				$('#'+id).html(retorno);
				$('#'+id).slideDown(100);

				$('#'+id+' .abrirSetor').click(function(e) {
                    id = $(this).attr('cod');
	  				abrirSetor(id);
                });
				
                
				listaMedico();
				listaConquista();

                
			}
		});	
	
	}
}
</script>

