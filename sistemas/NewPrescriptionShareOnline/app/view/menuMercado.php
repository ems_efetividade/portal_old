<style>


    #nav-mercado li a {
        color: #fff;
    }

    #nav-mercado li a:hover {
        color: #000;
    }

    #nav-mercado li > a {
        position: relative;
        display: block;
        padding: 7px 11px;
    }

</style>

<form class="" id="formSelMercado" method="post" action="<?php echo APP_CAMINHO ?>home/abrir">
    <input type="hidden" name="mercado" value="0"/>
    <input type="hidden" name="concorrente" value="0"/>
</form>


<div class="col-lg-2 hidden-xs hidden-sm hidden-md panel-left" style="background: #2A3F54; color:#FFF">
    <br>
    <img class="center-block" height="90" src="../../../public/img/logoEfet.png" alt="">
    <h5 class="text-center" style="font-size:15px">Prescription Share Online</h5>
    <hr>
    <a class="btn btn-sm btn-block btn-default" href="<?php echo APP_CAMINHO ?>home/search">Pesquisar Médicos</a>
    <br>
    <?php
    if ($_SESSION['id_perfil'] > 4) {
        if ($_SESSION['id_un_negocio'] == 7 || $_SESSION['id_un_negocio'] == 1) {
            ?>
            <div class="form-group">
                <label for="linhaCmb" style="font-weight: 400">Linha:</label>
                <?php

                require_once('/../model/mLinha.php');
                $modelLinha = new mLinha();
                $linha = $modelLinha->listLinhaByIdBu($_SESSION['id_un_negocio']);
                ?>
                <select class="form-control" onchange="trocaLinha()" id="linhaCmb">
                    <?php
                    foreach ($linha as $value)
                        echo '<option value="' . $value['ID_LINHA'] . '"' . ($_SESSION['id_linha'] == $value['ID_LINHA'] ? "selected=\"yes\"" : "") . '>' . $value['NOME'] . '</option>';
                    ?>
                </select>
                <script>
                    function trocaLinha() {
                        linha = document.getElementById("linhaCmb").value;
                        retorno = requisicaoAjax('<?php echo APP_CAMINHO ?>home/trocaLinha/' + linha);
                        if (retorno == 1) {
                            location.reload();
                        }
                    }
                </script>
            </div>
            <?php
        }
    }
    ?>
    <div class="mCustomScrollbar resize-too" style="ovserflow:auto; max-height:600px;">
    <ul id="nav-mercado" class="nav nav-pills nav-stacked">
        <?php foreach ($listaMercado as $mercado) { ?>
            <li role="presentation"
                class="<?php echo(($mercado['ID_MERCADO'] == $mercadoSelecionado['ID_MERCADO']) ? 'active' : '') ?>">
                <a href="#" class="cmbMercado" data-val="<?php echo $mercado['ID_MERCADO']; ?>">
                    <small>
                        <?php echo $mercado['MERCADO']; ?>
                        <?php echo(($mercado['ID_MERCADO'] == $mercadoSelecionado['ID_MERCADO']) ? '<span class="pull-right glyphicon glyphicon-chevron-right"></span>' : '') ?>

                    </small>
                </a>
            </li>
        <?php } ?>

    </ul>
        </div>
</div>

<div class=" hidden-lg col-md-12" style="background: #2A3F54; color:#FFF">
    <img class="center-block" height="90" src="../../../public/img/logoEfet.png" alt="">
    <h5 class="text-center" style="font-size:15px">EFETIVIDADE</h5>
    <a class="btn btn-sm btn-block btn-default" href="<?php echo APP_CAMINHO ?>home/search">Pesquisar Médicos</a>
    <br>
    <select class="form-control" id="cmb-merc">
        <?php foreach ($listaMercado as $mercado) { ?>
            <option class="cmbMercado" <?php echo(($mercado['MERCADO'] == $mercadoSelecionado) ? 'selected' : '') ?>
                    value="<?php echo $mercado['ID_MERCADO']; ?>">
                <small><?php echo $mercado['MERCADO']; ?></option>
        <?php } ?>
    </select>
    <br>
</div>

<script>

    /* link que seleciona o mercado */
    $('.cmbMercado').click(function (e) {
        //if($(this).val() != "::SELECIONE O MERCADO::") {
        var merc = $(this).data('val');
        abrirMercado(merc, '');
        //}
    });

    $('#cmb-merc').change(function (e) {
        //if($(this).val() != "::SELECIONE O MERCADO::") {
        var merc = $(this).val();
        abrirMercado(merc, '');
        //}
    });


    /* combo que seleciona o mercado */
    $('#comboMercado').change(function (e) {

        if ($(this).val() != "::SELECIONE O MERCADO::") {
            modalAguarde('show');
            $('#formSelMercado').submit();
        }
    });


    function abrirMercado(mercado, concorrentes) {
        $('input[name="mercado"]').val(mercado);
        $('input[name="concorrente"]').val(concorrentes);

        modalAguarde('show');
        $('#formSelMercado').submit();
    }

</script>        