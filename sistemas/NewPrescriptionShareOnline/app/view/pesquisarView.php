<?php require_once('app/view/headerView.php'); ?>

<style>
table p {
	padding:0;
	margin:0px;	
}
.listarMedicos {
	cursor:pointer;
}	
.alert {
	padding:6px;
	margin:1px;	
}
.alert-default {
	color: #777777;
    background-image: -webkit-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: -o-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: linear-gradient(to bottom, #eeeeee 0, #dddddd 100%);
    background-repeat: repeat-x;
    border: 1px solid #ddd;	
}
.alert-none {	   
    border: 1px solid #eee;	
}

.alert-gray {
	background-color:#f1f1f1;	   
    border: 1px solid #e5e5e5;	
}

.alert-soft {
	background-color:#f9f9f9;	   
    border: 1px solid #e5e5e5;	
}
.alert-primary {
	color: #ffffff;
    background-image: -webkit-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: -o-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: linear-gradient(to bottom, #4c70ba 0, #3b5998 100%);
    background-repeat: repeat-x;
    border: 1px solid #3b5998;	
}

.up, .down, .middle {
	padding-left:16px;	
}
.up, .up-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/up.png) no-repeat left center;	
	color:#3c763d;
}

.down, .down-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/down.png) no-repeat left center;	
	color:#a94442;
}

.middle, .middle-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/middle.png) no-repeat left center;	
	color:#f0ad4e;
}

.up-min, .down-min {
	padding-left:13px;
	background-size: 13px 13px;
}

.plus {
	background:url(<?php echo APP_CAMINHO; ?>public/img/plus.png) no-repeat left left;	
}
.bold {
	font-weight:bold;	
}

.loading {
	display:none;	
}

.abrirSetor, .listarMedico {
	cursor:pointer;	
}

html {
  overflow-y: scroll;
}

body.modal-open,
.modal-open {
    margin-right: 0px;
    margin-right: 0; /* // fix */
}

/*
//.modal-open .navbar-fixed-top,
//.modal-open .navbar-fixed-bottom {
//padding-right: 17px;
//}
*/
.loading-gif {
   position:absolute;
   top:55%;
   left:50%;
   margin-top:-25px;
   margin-left:-25px;
}

.table tbody>tr>td{
    vertical-align: middle;
}


.bcg .panel {
	border:0px;
	-webkit-box-shadow: 0 0px 0px rgba(0,0,0,0);
}

.nav li a {
   /* color:#fff;*/
}

.nav li a:hover {
    color:#000;
}

.custom-border {
    border-left:1px solid #ddd;
}

</style>

<div style="padding-left:15px;padding-right:15px">



    <div class="row">

        <?php require_once('menuMercado.php') ?>

        <div class="col-lg-10 col-md-12 panel-right">
        
                <br>
            <!-- nome do mercado -->
            <div class="row" style="margin-bottom:10px;">
                <div class="col-lg-12">
                    <h3 style="margin-top:0px;">Médicos <small> (<?php echo appFunction::formatarMoeda($dados['count'],0) ?> médico(s) encontrado(s) - Prescription Share </small></h3>
                    <hr>
                </div>

                <div class="col-lg-12">
                    <form class="" id="formPesquisa" method="post" action="<?php echo APP_CAMINHO ?>home/pesquisar">
                        <input type="hidden" name="txtPagina" value="1" />


                        

                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-3" style="padding-right:2px;">
                            <select class="form-control input-sm" name="cmbCampo">
                            <?php foreach($campos as $campo) { ?>
                                <option <?php echo (($campo['CAMPO'] == $dadosPesquisa['campo']) ? 'selected' : ''); ?> value="<?php echo $campo['CAMPO'] ?>"><?php echo $campo['CAMPO'] ?></option>
                            <?php } ?>
                            
                            </select>
                        </div>
                        <div class="col-lg-8 col-md-7 col-sm-7 col-xs-5" style="padding-left:0px;padding-right:0px;">
                            <input type="text" class="form-control input-sm" name="txtCriterio" value="<?php echo $dadosPesquisa['criterio'] ?>">
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-4 text-left" style="padding-left:0px;padding-right:0px;">
                            <button type="button" id="btn-pesquisar" data-pag="1" class="btn-bloSck btn btn-primary btn-sm "><span class="glyphicon glyphicon-search"></span> Pesquisar</button>
                            <button type="button" id="btn-limpar" data-pag="1" class="btn-bloSck btn btn-default btn-sm "><span class="glyphicon glyphicon-erase"></span> Limpar</button>
                        </div>
                    </div>
                    </form>
                    <hr style="margin-bottom:2px;">
                </div>
                <div class="col-lg-12">
                <br>
                    <?php  if(count($dados['dados']) > 0) { $keys = array_keys($dados['dados'][1]) ?>
                     

                        <table class="table table-condensed table-striped table-hover" style="font-size:11px;">
                            <tr>
                            <?php foreach($keys as $key) { if(!is_numeric($key)) { if($key != "ID_CRM") { ?>
                                   <th><?php echo $key ?></th> 
                            <?php } } } ?>
                            <th class="text-center">Ficha do Médico</th>
                            </tr>

                            <?php $i=0; foreach($dados['dados'] as $dado) { ?>
                                <tr >
                                <?php foreach($keys as $key) { if(!is_numeric($key)) { if($key != "ID_CRM") { ?>
                                   <td><?php echo $dado[$key] ?></td> 
                                <?php } } }?>
                                <td class="text-center">
                                    <button onClick="fichaMedico('<?php echo $dado['ID_CRM'] ?>', '<?php echo $dado['NOME'] ?>', '<?php echo $dado['ESP'] ?>', 'TRM01|TRM00')" type="button" class="btn btn-xs btn-primary">
                                    <span class="glyphicon glyphicon-file"></span>
                                    </button>
                                </td>
                                </tr>
                            <?php }  ?>
                        </table>

                        <?php foreach($paginacao['LINKS'] as $pagina) { 
                            echo $pagina;   
                        } ?>

                        
                    <?php }  else { ?>
                        <br>
                           <h1 class="text-center text-danger" style="font-size:120px;"><span class="glyphicon glyphicon-thumbs-down"></span></h1>
                           <h1 class="text-center text-danger">Ops!! Nenhum registro encontrado.</h1>
                           <h4 class="text-center"><small>
                           Não foi encontrado nenhum registro em nosso banco de dados com os critérios solicitados.
                           </small></h4>
                        <br>
                           <h5 class="text-center"> 
                            <a href="<?php echo APP_CAMINHO ?>home/pesquisar">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            Voltar para a página de médicos</a>
                           
                           </h5>

                    <?php } ?>
                
                </div>
            </div>


        </div>

    </div>

</div>








<!--MODAL PESQUISAR-->
<div class="modal" id="modalFichaMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-backdrop="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha do Médico</h5>
      </div>
      
      <div class="modal-body max-height">

 
      </div>

      <div class="modal-footer">
           <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""  id="btn-exportar-ficha-medico" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Exportar em CSV
            </a>   	
      
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>



<script>
$(window).ready(function(e){
    resizePanelLeft();
});


$('#btn-limpar').unbind().click(function(e){
    e.preventDefault();
    $('input[name="txtPagina"]').val(1);
    $('input[name="txtCriterio"]').val('');
    $('#cmbCampo').val(null);

    modalAguarde('show');

    $('#formPesquisa').submit();
});

$('.link-pagina, #btn-pesquisar').unbind().click(function(e){
    e.preventDefault();
    var pagina = $(this).data('pag');
    $('input[name="txtPagina"]').val(pagina);
    modalAguarde('show');
    $('#formPesquisa').submit();
});


function resizePanelLeft() {
    var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
    $('.panel-left').css('height', (screenSize));
} 




function fichaMedico(crm, nome, esp, periodo) {
    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/fichaMedico',
        data: {
            crm: crm,
            nome: nome,
            esp: esp,
            periodo: periodo
        },
        
        beforeSend: function() {

        },
        
        success: function(retorno) {
            $('#modalFichaMedico .modal-body').html(retorno);
            $('#modalFichaMedico').modal('show');

            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-crm', crm);
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-periodo', periodo);
            $('#modalFichaMedico').modal('show');

            $('#btn-exportar-ficha-medico').unbind().click(function(s){
                s.preventDefault();
                window.location.href = "<?php print APP_CAMINHO ?>home/exportarFichaMedico/"+crm+"/"+periodo+"";
            });

        }
    });  
}


</script>
