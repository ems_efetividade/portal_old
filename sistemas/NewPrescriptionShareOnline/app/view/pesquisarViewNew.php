<?php require_once 'app/view/headerView.php';?>


<div style="padding-left:15px;padding-right:15px">

    <div class="row">

        <?php require_once 'menuMercado.php'?>

        <div class="col-lg-10 col-md-12 panel-right">

            <!-- nome do mercado -->
            <div class="row" style="margin-bottom:10px;">

                <div class="col-lg-12">

                    <input type="hidden" id="txtPagina" name="txtPagina" value="1" />

                    <div class="row">
                        <div class="col-lg-6 col-md-7 col-sm-7 col-xs-12 col-md-offset-3" style="padding-left:0px;padding-right:0px;">
                            <h2 class="leadd text-center">Pesquisa de Médicos</h2>

                            <input type="hidden" id="txt-pag" value="0">

                            <div style="margin-left:10px;margin-right:10px">
                                <input type="text" style="border-radius: 20px 20px 0px 0px !important" class="form-control input-lg" autocomplete="off" name="txtCriterio" onkeyup="googleSearch(this.value, event)" value="<?php echo $dadosPesquisa['criterio'] ?>">
                                <div id="pre-search" style="z-index: 1; position:absolute;">
                                    <div id="panel-busca" class="panel panel-default" style="border-radius: 0px 0px 20px 20px !important">
                                        <div class="panel-body" id="retorno-busca" ></div>
                                    </div>
                                </div>
                                <h4 class="text-center"><small><?php echo appFunction::formatarMoeda($totalmed, 0) ?> médico(s) no seu setor.</small></h4>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>

                <div class="col-lg-12">
                    <div id="div-resultado" style="font-family:Arial">
                        <?php echo $tb['html'] ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

<!--MODAL PESQUISAR-->
<div class="modal" id="modalFichaMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-backdrop="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header btn-primary">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha do Médico</h5>
            </div>

            <div class="modal-body max-height">
            </div>

            <div class="modal-footer">
                <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""  id="btn-exportar-ficha-medico" data-dismisss="modal">
                    <span class="glyphicon glyphicon-file"></span> Exportar em CSV
                </a>
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                    <span class="glyphicon glyphicon-remove"></span> Fechar
                </button>
            </div>
        </div>
    </div>
</div>



<script>
$(window).ready(function(e){
    resizePanelLeft();
});


function mostrarAudit(audit, container) {
    vAudit = audit;
    $('#' + container + ' .audit').hide();
    $('#' + container + ' .ps-'+audit).show();
    $('#' + container + ' .radio-'+audit).prop('checked', true);
}



$('#btn-limpar').unbind().click(function(e){
    e.preventDefault();
    $('input[name="txtPagina"]').val(1);
    $('input[name="txtCriterio"]').val('');
    $('#cmbCampo').val(null);

    modalAguarde('show');

    $('#formPesquisa').submit();
});

$('.link-pagina, #btn-pesquisar').unbind().click(function(e){
    e.preventDefault();
    var pagina = $(this).data('pag');
    $('input[name="txtPagina"]').val(pagina);
    modalAguarde('show');
    $('#formPesquisa').submit();
});


function resizePanelLeft() {
    var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? $('.panel-left').height() : $('.panel-right').height();
    $('.panel-left').css('height', (screenSize));
}




function fichaMedico(crm, nome, esp, periodo) {
    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO?>home/fichaMedico',
        data: {
            crm: crm,
            nome: nome,
            esp: esp,
            periodo: periodo
        },

        beforeSend: function() {

        },

        success: function(retorno) {
            $('#modalFichaMedico .modal-body').html(retorno);
            $('#modalFichaMedico').modal('show');

            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-crm', crm);
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-periodo', periodo);
            $('#modalFichaMedico').modal('show');

            $('#btn-exportar-ficha-medico').unbind().click(function(s){
                s.preventDefault();
                window.location.href = "<?php print APP_CAMINHO?>home/exportarFichaMedico/"+crm+"/"+periodo+"";
            });
            mostrarAudit('sh', 'tb-ficha');
        }
    });
}


$('input[name="txtCriterio"]').attr('style', "border-radius: 20px 20px 20px 20px !important");
$('#panel-busca').hide();

function googleSearch(criterio, e) {
    pagina = $('#txt-pag').val();

    $('input[name="txtCriterio"]').attr('style', "border-radius: 20px 20px 20px 20px !important");
    $('#panel-busca').hide();

    if (e.keyCode == 13) {
        buscarMedicos(criterio, pagina);
        return
    }

    if(criterio.length > 3) {


        $.ajax({
            type: "POST",
            url: '<?php print APP_CAMINHO?>home/pesquisaCriterio',
            data: {
                criterio: criterio,
            },
            dataType: 'json',
            beforeSend: function() {

            },

            success: function(retorno) {
                if(retorno) {
                    $('input[name="txtCriterio"]').attr('style', "border-radius: 20px 20px 0px 0px !important");
                    $('#panel-busca').width($('input[name="txtCriterio"]').innerWidth());
                    $('#panel-busca').show();

                    var html = '';
                    $.each(retorno, function(i, key){
                        chave = "'" + key.texto + "'";
                        html = html + '<p onClick="buscarMedicos('+chave+', 0)"><small><small>'+key.label+'</small></small></p>';
                    });
                    $('#retorno-busca').html(html)
                }
            }
        });
    }
}

function buscarMedicos(criterio, pagina) {



    $('input[name="txtCriterio"]').val(criterio);
    $.ajax({
            type: "POST",
            url: '<?php print APP_CAMINHO?>home/buscarMedicos',
            data: {
                criterio: criterio,
                pagina: pagina
            },
            dataType: "json",
            beforeSend: function() {
                modalAguarde('show');
            },

            success: function(retorno) {
                //console.log(retorno);
                $('input[name="txtCriterio"]').attr('style', "border-radius: 20px 20px 20px 20px !important");
                $('#panel-busca').hide();
                $('#div-resultado').html(retorno.html);
                resizePanelLeft();
                modalAguarde('hide');

            }
        });
}

$('input[name="txtCriterio"]').on('keyup', function (e) {
    if (e.keyCode == 13) {
        $('input[name="txtCriterio"]').attr('style', "border-radius: 20px 20px 20px 20px !important");
        $('#panel-busca').hide();
        buscarMedicos($(this).val(), 1);
    }
});

</script>
