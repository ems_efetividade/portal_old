<?php 

$confPag = array('conf' =>
    array(

        'max_por_pagina' => $dados['max_por_pag'],
        'pagina_atual'   => $dados['paginacao']['atual'],
        'total'          => $dados['paginacao']['total_registros'],

        'paginacao_label' => '<small>{pagina_atual} de {paginas}</small>',

        'primeiro' => array(
            'function' => 'onClick="buscarMedicos(\'' . $criterio . '\', 1)"',
            'label'    => '<small><span class="glyphicon glyphicon-step-backward"></span></small>'
        ),

        'proximo' => array(
            'function' => 'onClick="buscarMedicos(\'' . $criterio . '\', {proxima_pagina})"',
            'label'    => '<small><span class="glyphicon glyphicon-triangle-right"></span></small>'
            
        ),

        'anterior' => array(
            'function' => 'onClick="buscarMedicos(\'' . $criterio . '\', {pagina_anterior})"',
            'label'    => '<small><span class="glyphicon glyphicon-triangle-left"></span></small>'
        ),

        'ultimo' => array(
            'function' => 'onClick="buscarMedicos(\'' . $criterio . '\', {paginas})"',
            'label'    => '<small><span class="glyphicon glyphicon-step-forward"></span></small>'
        )
    )
);


appComp::get('table', array(
    'table' => array(
        'class' => $dados['classe']
    ),
    'header' => $dados['header'],
    'data' => $dados['data']
));

appComp::get('paginacao', $confPag);



?>





