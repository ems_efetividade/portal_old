<?php
require_once('appConexao.php');
require_once('../../plugins/mpdf57/mpdf.php');

/** 
 * Funções padrões para todo o sistema.
 */
class appFunction extends appConexao {

	
	function appFunction() {
		
	}
	/**
        * Método que retorna a informação da sessao
         */
	public function dadoSessao($chave) {
		@session_start();
		return $_SESSION[$chave];
	}

        public function validarSessao() {
            @session_start();
            if(!isset($_SESSION['id_setor'])) {
                header('Location: '.'https://'.SERVER_URL.'/login/');
            }
        }        
	
	public function nomeColaborador($nome) {
		$dados = explode(" ", $nome);
		return $dados[0];
	}
	
	public function periodoDia() {
		$hora = date('H');

		if ($hora < 12) {
			$cump = 'Bom Dia';	
		} else {
			if($hora > 12 && $hora < 18) {
				$cump = 'Boa Tarde';	
			} else {
				$cump = 'Boa Noite';	
			}
		}
		
		return $cump;
	}
	
	public function salvarEnquete($nota, $idSetor, $idColaborador, $obs) {
		$cn = new appConexao();
		$cn->executar("INSERT INTO FERRAMENTA_ENQUETE (NOTA, ID_SETOR, ID_COLABORADOR, OBSERVACAO, DATA_CADASTRO) VALUES (".$nota.", ".$idSetor.", ".$idColaborador.", '".$obs."', '".date('Y-m-d H:i:s')."')");
	}
	
	public function dataExtenso() {
		$mes[0] = 'Janeiro';
		$mes[1] = 'Fevereiro';
		$mes[2] = 'Março';
		$mes[3] = 'Abril';
		$mes[4] = 'Maio';
		$mes[5] = 'Junho';
		$mes[6] = 'Julho';
		$mes[7] = 'Agosto';
		$mes[8] = 'Setembro';
		$mes[9] = 'Outubro';
		$mes[10] = 'Novembro';
		$mes[11] = 'Dezembro';
		
		return date('j').' de '.$mes[date('m')-1]. ' de '. date('Y');
			
	}
	public function formatarData($data) {
		if($data != "") {
			
			$dataHora = explode(" ", $data);
			
			if(count($dataHora) > 0) {
				$campos = explode("-", $dataHora[0]);
				$hora = explode(".", $dataHora[1]);
			} else {
				$campos = explode("-", $data);
				$hora = '';
			}

			if(count($campos) == 1) {
				$campos = explode("/", $data);
				return $campos[2]."-".$campos[1]."-".$campos[0];
			} else {
				return $campos[2]."/".$campos[1]."/".$campos[0].' '.$hora[0];
			}
			
		}
	}
	
	public function criarUrl($dado) {
		$string = 'ÁÍÓÚÉÄÏÖÜËÀÌÒÙÈÃÕÂÎÔÛÊáíóúéäïöüëàìòùèãõâîôûêÇç';
		$remover = array(',', '!', '%', '(', ')');
		
		$url = $dado;
		$url = strtolower(str_replace(" ", "-", $dado));
		
		for($i=0;$i<count($remover);$i++) {
			$url = str_replace($remover[$i], "", $url);
		}

		$url = preg_replace( '/[`^~\'"]/', null, iconv( 'UTF-8', 'ASCII//TRANSLIT', $url));
		return strtolower($url);
	}
	
	public function imagemNoticia($imagem, $local, $w="", $h="") {
		
		$imagemFinal = '';
		
		if($imagem != "") {
			if($local == 0) {
				$pastaImgArtigo = APP_CAMINHO . appConf::folder_img_noticia;
				
				if(file_exists(appConf::folder_img_noticia.$imagem)) {
					$imagemFinal = $pastaImgArtigo.$imagem;
				} 
			} else {
				$imagemFinal = $imagem;	
			}
		}
		
		if($w != "" || $h != "") {
			$img = '<img style="margin-right:10px;margin-bottom:10px;" width="'.$w.'" height="'.$h.'" class="pull-left"  src="'.$imagemFinal.'" />';
		} else {
			$img = '<img style="margin-right:10px;margin-bottom:10px;"class="pull-left img-responsive img-thumbnaill"  src="'.$imagemFinal.'" />';
		}
		
		return $img;
		
		
	}
	
	public function criarUrlArtigo($idArtigo, $titulo) {
		return APP_CAMINHO.'noticia/ler/'.$idArtigo.'/'.$this->criarUrl($titulo);
	}
	
	public function extensaoArquivo($ext, $class="") {
		
		$arquivo = ROOT.'public\\img\\ext\\'.$ext.'.png';
		
		if(file_exists($arquivo)) {
			return APP_CAMINHO.'public/img/ext/'.$ext.'.png';
		} else {
			return APP_CAMINHO.'public/img/ext/blank.png';
		}
	}
	
	public function dataFisicoArquivo($arquivo) {
		$arquivoFisico = utf8_decode(appConf::folder_report.$arquivo);
		
		if(file_exists($arquivoFisico)) {
			return  date ("d/m/Y H:i:s", fileatime($arquivoFisico));
		}
	}
	
	public function tamanhoFisicoArquivo($arquivo) {
		
		$arquivoFisico = utf8_decode(appConf::folder_report.$arquivo);
		//return $arquivoFisico;
		if(file_exists($arquivoFisico)) {
			$tamanho = filesize($arquivoFisico);	
			return $this->formatarTamanho($tamanho);
			//return $arquivoFisico;
		}

	}
	
	
	public function download($diretorio, $arquivo) {
		$arquivoFisico = utf8_decode(appConf::folder_report.$diretorio.'\\'.$arquivo);
				
		if(file_exists($arquivoFisico)) {
			set_time_limit(0);	
			ini_set('memory_limit', '1024M'); 
			header("Cache-Control: private");
			header("Content-Type: application/stream");
			header("Content-Length: ".filesize($arquivoFisico));
			header("Content-Disposition: attachment; filename=".$arquivo);	
			readfile($arquivoFisico);
			
			$this->executar("EXEC proc_downloadArquivo '".$this->dadoSessao('id_setor')."', '".$this->dadoSessao('id_colaborador')."', '".utf8_encode($arquivo)."'");
			exit;
		} else {
			echo $arquivoFisico;	
		}
		
	}
	
	public function formatarTamanho($tamanho) {
		 $kb = 1024;
		   $mb = 1048576;
		   $gb = 1073741824;
		   $tb = 1099511627776;
	 
		   if($tamanho<$kb){
		 
			 return($tamanho." bytes");
	   
		   }else if($tamanho>=$kb&&$tamanho<$mb){
	  
			 $kilo = number_format($tamanho/$kb,2);
		 
			 return($kilo." KB");
	   
		   }else if($tamanho>=$mb&&$tamanho<$gb){
		  
			$mega = number_format($tamanho/$mb,2);
	   
			 return($mega." MB");
	   
		   }else if($tamanho>=$gb&&$tamanho<$tb){
		   
			$giga = number_format($tamanho/$gb,2);
		  
			return($giga." GB");
		  }
	}
	
	public function formatarListaArtigos($rs) {

		$artigo = '';
		
		for($i=1;$i<=count($rs);$i++) {
		
			$artigo .= '
			  <div class="media">

				<a href="#" class="pull-left img-responsive thumbnail abrir-artigo" artigo="21" "="">
					<img data-src="holder.js/100%x180" src="'.$this->validarImagemArtigo($rs[$i]['IMG']).'" style="height: 80px; width: 100px; display: block;">
				  </a>
				
				<div class="media-body">
				  <h4 class="media-heading"><strong><a href="'.$this->criarUrlArtigo($rs[$i]['ID_ARTIGO'], $rs[$i]['TITULO']).'" class="abrir-artigo" artigo="21">'.$rs[$i]['TITULO'].'</a></strong></h4>
				  <h4><small>'.$rs[$i]['DATA'].'</small></h4>
				  <h5>Por: '.$rs[$i]['ESCRITOR'].'</h5>
				</div>
			  </div>';
		  
		}
		
		return $artigo;
	}
	
	public function fotoColaborador($idColaborador=0) {
		
		if($idColaborador == 0) {
			$foto = APP_CAMINHO.'public/img/exemplo_foteoo.jpg';
		} else {
		
			$cn = new appConexao();
			
			$rs = $cn->executarQueryArray("SELECT FOTO FROM FOTO_PERFIL WHERE ID_COLABORADOR = ".$idColaborador);
			
			if(count($rs) > 0) {
				$foto = APP_CAMINHO.appConf::folder_profile.$rs[1]['FOTO'];
			} else {
				$foto = APP_CAMINHO.'public/img/exemplo_foteoo.jpg';
			}
			
		}
		return $foto;

	}
	
	public function formatarMoeda($numero, $decimal=1, $simbolo='') {
		
		if($numero == '' || is_null($numero)) {
			return '  &nbsp;';
		} else {
			return number_format($numero, $decimal, ",", ".").$simbolo;
		}
	}
	
	public function mesAtualPainel() {
		$rs = $this->executarQueryArray("SELECT MES FROM DASH_PRODUTIVIDADE GROUP BY MES");
		return $rs[1]['MES'];
	}
	


	public function comboSetor($setor) {

		$rsPerfil = $this->executarQueryArray("select NIVEL_PERFIL from vw_colaboradorSetor where SETOR = '".$setor."'");
		
		if($rsPerfil[1]['NIVEL_PERFIL'] == 0) {
			$rs = $this->executarQueryArray("SELECT * FROM vw_colaboradorSetor WHERE NIVEL_PERFIL = 1 ORDER BY SETOR ASC");
		} else {
		
		$rs = $this->executarQueryArray("SELECT * FROM vw_colaboradorSetor WHERE NIVEL = (SELECT ID_SETOR FROM SETOR WHERE SETOR = '".$setor."' AND NIVEL_PERFIL > 0) ORDER BY SETOR ASC");
		
		}
		
		$option = '<option value=""></option>';
		for($i=1;$i<=count($rs);$i++) {
			$option .= '<option value="'.$rs[$i]['SETOR'].'">'.$rs[$i]['SETOR'].' '.$rs[$i]['NOME'].'</option>';
		}
		
		if($rsPerfil[1]['NIVEL_PERFIL'] != 3) {
			$div = '<div id="combo'.$rsPerfil[1]['NIVEL_PERFIL'].'"></div>';
		} else {
			$div = '';
		}
		
		
		 $combo = '<div class="form-group">
		 			<label for="exampleInputEmail1">'.$rs[1]['PERFIL'].'</label>
					<select class="form-control input-md comboColaborador" container="combo'.$rsPerfil[1]['NIVEL_PERFIL'].'">
						'.$option.'
					</select>
				  </div>'.$div;

		return $combo;
		
		
	}
	
	
	
	
	
	
	
	public function comboUF($selecionar="", $habilitado='') {
		$rs = $this->executarQueryArray("SELECT * FROM ESTADO");
		
		$combo = '<select class="form-control" id="selUF" name="uf" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['SIGLA']) {
				$combo .= '<option value="'.$rs[$i]['SIGLA'].'" selected>'.$rs[$i]['SIGLA'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['SIGLA'].'">'.$rs[$i]['SIGLA'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
                        	
	}
	
	
	public function comboCidade($uf="", $selecionar="", $habilitado='') {
		if($uf != "") {
		$rs = $this->executarQueryArray("SELECT * FROM CIDADE WHERE ESTADOID = (SELECT ESTADOID FROM ESTADO WHERE SIGLA = '".$uf."')");
		
		$combo = '<select class="form-control" name="selCidade" id="selCidade" '.$habilitado.'>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['NOME']) {
				$combo .= '<option value="'.$rs[$i]['NOME'].'" selected>'.$rs[$i]['NOME'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['NOME'].'">'.$rs[$i]['NOME'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
		} else {
			return $combo = '<select class="form-control" id="selCidade"><option></option></select>';
		}
                        	
	}
	
	public function comboAeroporto($selecionar="", $habilitado='') {
		$rs = $this->executarQueryArray("SELECT * FROM AEROPORTO");
		
		$combo = '<select class="form-control" name="selAeroporto" id="selAeroporto" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['AEROPORTO']) {
				$combo .= '<option value="'.$rs[$i]['AEROPORTO'].'" selected>'.$rs[$i]['AEROPORTO'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['AEROPORTO'].'">'.$rs[$i]['AEROPORTO'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
                     
	}
	
	
	
	public function comboModeloIpad($selecionar="", $habilitado='') {
		$rs = $this->executarQueryArray("SELECT MODELO FROM IPAD GROUP BY MODELO ORDER BY MODELO ASC");
		
		$combo = '<select class="form-control" name="selModeloIpad" id="selModeloIpad" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['MODELO']) {
				$combo .= '<option value="'.$rs[$i]['MODELO'].'" selected>'.$rs[$i]['MODELO'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['MODELO'].'">'.$rs[$i]['MODELO'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
                     
	}
	
	public function comboConexaoIpad($selecionar="", $habilitado='') {
		$rs = $this->executarQueryArray("SELECT CONEXAO FROM IPAD GROUP BY CONEXAO ORDER BY CONEXAO ASC");
		
		$combo = '<select class="form-control" name="selConexao" id="selConexao" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['CONEXAO']) {
				$combo .= '<option value="'.$rs[$i]['CONEXAO'].'" selected>'.$rs[$i]['CONEXAO'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['CONEXAO'].'">'.$rs[$i]['CONEXAO'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
                     
	}
	
	
	public function comboPerfil($selecionar="", $habilitado='') {

		//$rs = $cn->executar_query_array("SELECT * FROM PERFIL WHERE NIVEL = 0 AND ADMIN = 0");
		$rs = $this->executarQueryArray("EXEC proc_listarPerfil '".$this->dadoSessao('id_setor')."'");
		
		$combo = '<select class="form-control required" name="selDepto" id="selDepto" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			if($rs[$i]['NIVEL'] == 0) {
				if($selecionar == $rs[$i]['ID_PERFIL']) {
					$combo .= '<option value="'.$rs[$i]['ID_PERFIL'].'" selected>'.$rs[$i]['PERFIL'].'</option>';
				} else {
					$combo .= '<option value="'.$rs[$i]['ID_PERFIL'].'">'.$rs[$i]['PERFIL'].'</option>';
				}
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
                        	
	}
	
	
	public function comboFuncao($perfil="", $selecionar="", $habilitado='') {
		if($perfil != "") {

		$rs = $this->executarQueryArray("SELECT * FROM FUNCAO WHERE ID_PERFIL = ".$perfil);
		
		$combo = '<select class="form-control required" name="selFuncao" id="selFuncao" '.$habilitado.'><option value=""></option>';
		
		for($i=1;$i<=count($rs);$i++) {
			
			if($selecionar == $rs[$i]['FUNCAO']) {
				$combo .= '<option value="'.$rs[$i]['FUNCAO'].'" selected>'.$rs[$i]['FUNCAO'].'</option>';
			} else {
				$combo .= '<option value="'.$rs[$i]['FUNCAO'].'">'.$rs[$i]['FUNCAO'].'</option>';
			}
		}
		
		$combo .= '</select>';
		
		return $combo;
		} else {
			return $combo = '<select class="form-control required" name="selFuncao" id="selFuncao"><option></option></select>';
		}
                        	
	}
	
	public function comboEstadoCivil($selecionar='', $habilitado='') {
		$select = '<select class="form-control" name="selEstadoCivil" id="selEstadoCivil" '.$habilitado.'>
					<option></option>
					<option value="CASADO">CASADO</option>
					<option value="DIVORCIADO">DIVORCIADO</option>
					<option value="SEPARADO">SEPARADO</option>
					<option value="SOLTEIRO">SOLTEIRO</option>
				   </select>';	
		
		
		$localizar = '<option value="'.strtoupper($selecionar).'">';
		$substituir = '<option value="'.strtoupper($selecionar).'" selected="selected">';
		
		$select = str_replace($localizar, $substituir, $select);
		
		return $select;
		
	}
	
	public function comboTamanhoCamiseta($selecionar='', $habilitado='') {
		$select = '<select class="form-control" name="selCamiseta" id="selCamiseta" '.$habilitado.'>
											<option></option>
											<option value="PP">PP</option>
											<option value="P">P</option>
											<option value="M">M</option>
											<option value="G">G</option>
											<option value="GG">GG</option>
											<option value="XG">XG</option>
										  </select>';	
		
		
		$localizar = '<option value="'.strtoupper($selecionar).'">';
		$substituir = '<option value="'.strtoupper($selecionar).'" selected="selected">';
		
		$select = str_replace($localizar, $substituir, $select);
		
		return $select;
		
	}
	
	public function comboSimNao($selecionar='', $nomeSel='', $habilitado='') {
		$select = '<select class="form-control" name="'.$nomeSel.'" id="'.$nomeSel.'" '.$habilitado.'>
											<option></option>
											<option value="S">SIM</option>
											<option value="N">NÃO</option>
											</select>';	
		
		
		$localizar = '<option value="'.strtoupper($selecionar).'">';
		$substituir = '<option value="'.strtoupper($selecionar).'" selected="selected">';
		
		$select = str_replace($localizar, $substituir, $select);
		
		return $select;
		
	}
	
	public function comboCarro($selecionar='', $habilitado='') {
		$select = '<select class="form-control" name="selCarro" id="selCarro" '.$habilitado.'>
											<option></option>
											<option value="EMPRESA">EMPRESA</option>
											<option value="PARTICULAR">PARTICULAR</option>
											</select>';	
		
		
		$localizar = '<option value="'.strtoupper($selecionar).'">';
		$substituir = '<option value="'.strtoupper($selecionar).'" selected="selected">';
		
		$select = str_replace($localizar, $substituir, $select);
		
		return $select;
		
	}


	public function iconTable($val=0, $config) {
        if($val != "") {
            if($val > 0) {
                $icon = '<span style="font-size:'.$config['TAM_ICON'].'px" class="glyphicon glyphicon-arrow-up text-success"></span>';
                $classText = 'text-success';
            } else {
                if($val == 0) {
                    $icon = '<span style="font-size:'.$config['TAM_ICON'].'px" class="glyphicon glyphicon-arrow-right text-warning"></span>';
                    $classText = 'text-warning';
                } else {
                    $icon = '<span style="font-size:'.$config['TAM_ICON'].'px" class="glyphicon glyphicon-arrow-down text-danger"></span>';
                    $classText = 'text-danger';
                }
            }

            return '<span class="text-center '.$classText.'">'.$icon.appFunction::formatarMoeda($val, $config['DECIMAL']).$config['SUFIXO'].'</span>';
        }
    }
	

	public function gerarPDF($html='', $css='', $nomeArquivo='') {
		ob_start();
		$html = urldecode($html);
		$nome_arquivo = $nomeArquivo;
		$arquivo_css = $css;
		
		//$stylesheet = file_get_contents('css/css_imprimir_print.css');
		$pdf = new mPDF('', 'A4', 0, 'Arial', 7, 7, 7, 15, 7, 10, 'P');
		$pdf->allow_charset_conversion=true;
		$pdf->showImageErrors = true;
		$pdf->charset_in='iso-8859-1';
		$pdf->SetDisplayMode('fullpage');
		$pdf->SetFooter('{DATE d/m/y H:i:s}||{PAGENO}');
		//$pdf->WriteHTML($stylesheet,1);
		if($arquivo_css != "") {
			$stylesheet = file_get_contents(APP_CAMINHO.'public/css/'.$arquivo_css);
			$pdf->WriteHTML($stylesheet,1); // The parameter 1 tells that this is css/style only and no body/html/text
		}
		$pdf->WriteHTML($html);
		$pdf->Output($nome_arquivo.'.pdf', 'D');
		//exit();
	}

	
}

?>