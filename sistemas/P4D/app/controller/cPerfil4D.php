<?php
require_once('lib/appController.php');
require_once('app/model/mPerfil4D.php');
require_once('app/view/formsPerfil4D.php');

class cPerfil4D extends appController
{

    private $modelPerfil4D = null;

    public function __construct()
    {
        $this->modelPerfil4D = new mPerfil4D();
    }

    public function main()
    {
        $this->render('vPerfil4D');
    }

    public function listarSetorByLinha($linha)
    {
        return  $this->modelPerfil4D->listarSetorByLinha($linha);
    }

    public function listarLinha()
    {
        return $this->modelPerfil4D->listarLinha();
    }

    public function listarEspecialidadeByLinha($linha)
    {
        return $this->modelPerfil4D->listarEspecialidadeByLinha($linha);
    }

    public function listarProdutoByMercado($mercado)
    {
        return $this->modelPerfil4D->listarProdutoByMercado($mercado);
    }

    public function listarMercadoByLinha($linha)
    {
        return $this->modelPerfil4D->listarMercadoByLinha($linha);
    }

    public function controlSwitch($acao)
    {
        switch ($acao) {
            case 'cadastrar':
                break;
            default:
                //$this->viewP4D();
                break;
        }
    }


}
