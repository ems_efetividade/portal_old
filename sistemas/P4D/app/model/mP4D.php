<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * @author 053189 - Jefferson Oliveira em 28/08/2017
*/
require_once('lib/appConexaoDW.php');
class mP4D extends Model {
    
    private $dw;

    public function __construct() {
        parent::__construct();
        $this->dw = new appConexaoDW();
    }


    public function getLinhas() {
        $query = 'EXEC proc_P4N_listarLinha';
        return $this->dw->executarQueryArray($query);
    }

    public function getMercados($linha='') {
        $query = "EXEC proc_P4N_listarMercado '".$linha."' ";
        return $this->dw->executarQueryArray($query);
    }

    public function getProdutos($mercado='') {
        $query = "SELECT * FROM P4D_FATO_PRODUTO WHERE MERCADO = '".$mercado."'";
        return $this->dw->executarQueryArray($query);
    }

    public function getSetores($linha='') {
        //$query = "EXEC proc_P4N_listarSetor '".$linha."' ";
        $query = "SELECT SETOR as LABEL, SETOR AS OPCAO, NIVEL_PERFIL AS ESPACO FROM VW_COLABORADORSETOR WHERE LINHA = '".$linha."' order by setor asc";
        
        return $this->cn->executarQueryArray($query);
    }

    public function getEspecs($linha='') {
        $query = "EXEC proc_P4N_listarEspecialidade'".$linha."' ";
        return $this->dw->executarQueryArray($query);
    }

    public function getCategoria() {
        $query = "SELECT * FROM P4D_DIM_PERFIL ORDER BY ORDEM ASC";
        return $this->dw->executarQueryArray($query);
    }

    public function TotalCategoria($dados=array()) {
        //$query = "SELECT PERFIL, COUNT(PERFIL) AS TOTAL FROM P4D_BASE_CONSULTA, () B  WHERE 1=1 ";

        foreach($dados as $dado) {
            $q1 .= ' AND A.' .  $dado['campo'] . ' IN (' . $dado['valores'] . ')';
            $q2 .= ' AND ' .  $dado['campo'] . ' IN (' . $dado['valores'] . ')';
        }

        //$query .= ' GROUP BY A.PERFIL';


        $query = 'SELECT
                    A.PERFIL,
                    COUNT(A.PERFIL) AS QTD,
                    (CAST(COUNT(A.PERFIL) AS FLOAT) / B.TOTAL) * 100 AS SHARE
                FROM 
                    P4D_BASE_CONSULTA A, (
                    
                        SELECT
                            COUNT(A.PERFIL) AS TOTAL
                        FROM 
                            P4D_BASE_CONSULTA A
                        WHERE 1=1
                            '.$q2.'
                    ) B
                WHERE 1=1
                    '.$q1.'
                GROUP BY
                    A.PERFIL,	
                    B.TOTAL';

        return $this->dw->executarQueryArray($query);
    }

    public function GraficoBolha($dados=array(), $produto='') {
        
        foreach($dados as $dado) {
            $q1 .= ' AND ' .  $dado['campo'] . ' IN (' . $dado['valores'] . ')';
        }

        $query = "SELECT TOP 1000
                CRM,
                NOME,
                ADOCAO * 100 AS ADOCAO,
                POTENCIAL * 100 AS POTENCIAL,
                PERFIL
            FROM 
                P4D_BASE_CONSULTA
            WHERE 1=1 
                ".$q1."
            ORDER BY PERFIL ASC";

        //return $query;
        $rs = $this->dw->executarQueryArray($query);
        return array('rs' => $rs, 'query' => $query);
    }

    public function Media($mercado, $produto) {
        $query = "SELECT * FROM P4D_FATO_MEDIA WHERE MERCADO = '".$mercado."' AND PRODUTO = '".$produto."'";
        $rs = $this->dw->executarQueryArray($query);
        return array('rs' => $rs, 'query' => $query);
    }

    public function ListagemMedicos($dados=array()) {
        
        foreach($dados as $dado) {
            $q1 .= ' AND ' .  $dado['campo'] . ' IN (' . $dado['valores'] . ')';
        }

        $query = "SELECT 
               CRM,
               NOME,
               ESPEC1,
               CIDADE,
               ADOCAO * 100 AS ADOCAO,
               POTENCIAL * 100 AS POTENCIAL,
               PERFIL
            FROM 
                P4D_BASE_CONSULTA
            WHERE 1=1 
                ".$q1."";

        
        return $this->dw->executarQueryArray($query);
    }


    
    
    
}