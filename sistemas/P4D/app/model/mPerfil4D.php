<?php
require_once('lib/appConexao.php');
require_once('/../../lib/appConexaoDW.php');

class mPerfil4D extends appConexao implements gridInterface
{

    public function __construct()
    {
        //$this->listarLinha();
    }

    public function listarProdutoByMercado($mercado)
    {
        $conectDW = new appConexaoDW();
        $sql = "proc_P4N_listarProduto  $mercado";
        return $retorno = $conectDW->executarQueryArray($sql);
    }

    public function listarMercadoByLinha($linha)
    {
        $conectDW = new appConexaoDW();
        $sql = "proc_P4N_listarMercado $linha";
        return $retorno = $conectDW->executarQueryArray($sql);
    }

    public function listarEspecialidadeByLinha($linha)
    {
        $conectDW = new appConexaoDW();
        $sql = "proc_P4N_listarEspecialidade $linha";
        return $retorno = $conectDW->executarQueryArray($sql);
    }

    public function listarLinha()
    {
        $conectDW = new appConexaoDW();
        $sql = "proc_P4N_listarLinha";
        return $retorno = $conectDW->executarQueryArray($sql);
    }

    public function listarSetorByLinha($linha)
    {
        $conectDW = new appConexaoDW();
        $sql = "proc_P4N_listarSetor $linha";
        return $retorno = $conectDW->executarQueryArray($sql);
    }


    public function listObj()
    {
        // TODO: Implement listObj() method.
    }

    public function loadObj()
    {
        // TODO: Implement loadObj() method.
    }

    public function save()
    {
        // TODO: Implement save() method.
    }

    public function delete()
    {
        // TODO: Implement delete() method.
    }

    public function updateObj()
    {
        // TODO: Implement updateObj() method.
    }

    public function pages($sql)
    {
        // TODO: Implement pages() method.
    }


}