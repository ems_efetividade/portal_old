<?php

class formsPerfil4D
{
    public function formVisualizar($objeto)
    {
        ?>
        <form id="formVisualizar">
            <div class="row">
                <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idEmpresa">Empresa</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeEmpresa() ?>" name="idempresa" id="idEmpresa">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idBu">Unidade de Negócio</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeBu() ?>"
                               name="idbu" id="idBu">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idLinha">Linha</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeLinha() ?>" name="idlinha" id="idLinha">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idMenu">Menu</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getNomeMenu() ?>" name="idmenu" id="idMenu">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Pefil</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getPerfil() ?>"
                               name="idPerfil" id="idPerfil">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Ativo</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getAtivoTratado() ?>"
                               name="ativo" id="ativo">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">Data Cadastro</label>
                        <input type="text" disabled class="form-control " readonly
                               value="<?php echo $objeto->getDataIn() ?>"
                               name="datain" id="dataIn">
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formCadastrar()
    {
        require_once("/../controller/cPerfil4D.php");
        $control = new cPerfil4D();
        $empresas = $control->listEmpresas();
        $menus = $control->listMenu();
        $perfis = $control->listPerfil();
        ?>
        <form id="formSalvar" action="<?php print appConf::caminho ?>ControleHeaderPermissoes/controlSwitch/salvar">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idEmpresa">Empresa</label>
                        <select class="form-control " name="idempresa" id="idEmpresa">.
                            <option value="0">Selecione...</option>
                            <?php
                            foreach ($empresas as $empresa) {
                                ?>
                                <option value="<?php echo $empresa[0] ?>"> <?php echo $empresa[1] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <script>
                    $('#idEmpresa').change(function () {
                        var empresaId = $('#idEmpresa').val();
                        var formData = new FormData();
                        formData.append('empresaId', empresaId);
                        var option = '<option value="0">Selecione...</option>';
                        $.ajax({
                            type: "POST",
                            url: "<?php print appConf::caminho ?>cPerfil4D/listBu",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                obj = $.parseJSON(retorno);
                                for (i in obj) {
                                    option += '<option value="' + obj[i].ID_UN_NEGOCIO + '">' + obj[i].NOME + '</option>';
                                }
                                document.getElementById('idBu').innerHTML = option;
                            }
                        });
                    });
                </script>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idBu">Unidade de negócio</label>
                        <select required class="form-control " name="idbu" id="idBu">
                        </select>
                    </div>
                </div>
                <script>
                    $('#idBu').change(function () {
                        var idBu = $('#idBu').val();
                        var formData = new FormData();
                        formData.append('buId', idBu);
                        var option = '<option value="0">Selecione...</option>';
                        $.ajax({
                            type: "POST",
                            url: "<?php print appConf::caminho ?>cPerfil4D/listLinha",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                obj = $.parseJSON(retorno);
                                console.log(obj);
                                for (i in obj) {
                                    option += '<option value="' + obj[i].ID_LINHA + '">' + obj[i].NOME + '</option>';
                                }
                                document.getElementById('idLinha').innerHTML = option;
                            }
                        });
                    });
                </script>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idLinha">Linha</label>
                        <select class="form-control " name="idlinha" id="idLinha">
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idMenu">Menu</label>
                        <select class="form-control " name="idmenu" id="idMenu">
                            <option value="0">Selecione...</option>
                            <?php
                            foreach ($menus as $menu) {
                                ?>
                                <option value="<?php echo $menu['ID'] ?>"><?php echo $menu['CAMPO'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <script>
                    $('#idMenu').change(function () {
                        var idBu = $('#idMenu').val();
                        if (idBu != 0) {
                            $('#idPerfil').prop("disabled", false);
                        } else {
                            $('#idPerfil').prop("disabled", true);
                        }
                    });
                </script>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Pefil</label>
                        <select disabled class="form-control " name="idperfil" id="idPerfil">
                            <?php
                            foreach ($perfis as $perfil) {
                                ?>
                                <option value="<?php echo $perfil['ID_PERFIL'] ?>"><?php echo $perfil['PERFIL'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Ativo</label>
                        <select class="form-control " name="ativo" id="ativo">
                            <option value="1">Ativo</option>
                            <option value="0">Inativo</option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formEditar($objeto)
    {
        require_once('/../controller/cPerfil4D.php');
        $control = new cPerfil4D();
        $empresas = $control->listEmpresas();
        $_POST['empresaId'] = $objeto->getIdEmpresa();
        $unidades = $control->arrayListBu();
        $_POST['buId'] = $objeto->getIdBu();
        $linhas = $control->arrayListLinha();
        $menus = $control->listMenu();
        $perfis = $control->listPerfil();
        ?>
        <form id="formEditar" action="<?php print appConf::caminho ?>ControleHeaderPermissoes/controlSwitch/atualizar">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="row">
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idEmpresa">Empresa</label>
                        <select class="form-control " name="idempresa" id="idEmpresa">.
                            <?php
                            foreach ($empresas as $empresa) {
                                ?>
                                <option
                                    <?php if ($empresa[0] == $objeto->getIdEmpresa()) {
                                        echo 'selected';
                                    } ?> value="<?php echo $empresa[0] ?>"> <?php echo $empresa[1] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <script>
                    $('#idEmpresa').change(function () {
                        var empresaId = $('#idEmpresa').val();
                        var formData = new FormData();
                        formData.append('empresaId', empresaId);
                        var option = '<option value="0">Selecione...</option>';
                        $.ajax({
                            type: "POST",
                            url: "<?php print appConf::caminho ?>cPerfil4D/listBu",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                obj = $.parseJSON(retorno);
                                for (i in obj) {
                                    option += '<option value="' + obj[i].ID_UN_NEGOCIO + '">' + obj[i].NOME + '</option>';
                                }
                                document.getElementById('idBu').innerHTML = option;
                            }
                        });
                    });
                </script>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idBu">Unidade de negócio</label>
                        <select required class="form-control " name="idbu" id="idBu">
                            <?php
                            foreach ($unidades as $unidade) {
                                ?>
                                <option
                                    <?php if ($unidade[0] == $objeto->getIdBu()) {
                                        echo 'selected';
                                    } ?> value="<?php echo $unidade[0] ?>"> <?php echo $unidade[1] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <script>
                    $('#idBu').change(function () {
                        var idBu = $('#idBu').val();
                        var formData = new FormData();
                        formData.append('buId', idBu);
                        var option = '<option value="0">Selecione...</option>';
                        $.ajax({
                            type: "POST",
                            url: "<?php print appConf::caminho ?>cPerfil4D/listLinha",
                            data: formData,
                            contentType: false,
                            processData: false,
                            success: function (retorno) {
                                obj = $.parseJSON(retorno);
                                console.log(obj);
                                for (i in obj) {
                                    option += '<option value="' + obj[i].ID_LINHA + '">' + obj[i].NOME + '</option>';
                                }
                                document.getElementById('idLinha').innerHTML = option;
                            }
                        });
                    });
                </script>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idLinha">Linha</label>
                        <select class="form-control " name="idlinha" id="idLinha">
                            <?php
                            foreach ($linhas as $linha) {
                                ?>
                                <option
                                    <?php if ($linha[0] == $objeto->getIdEmpresa()) {
                                        echo 'selected';
                                    } ?> value="<?php echo $linha[0] ?>"> <?php echo $linha[1] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="idMenu">Menu</label>
                        <select class="form-control " name="idmenu" id="idMenu">
                            <option value="0">Selecione...</option>
                            <?php
                            foreach ($menus as $menu) {
                                ?>
                                <option
                                    <?php if ($menu['ID'] == $objeto->getIdMenu()) {
                                        echo 'selected';
                                    } ?>
                                        value="<?php echo $menu['ID'] ?>"><?php echo $menu['CAMPO'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">Pefil</label>
                        <select class="form-control " name="idperfil" id="idPerfil">
                            <?php
                            foreach ($perfis as $perfil) {

                                ?>
                                <option value="<?php echo $perfil['ID_PERFIL'] ?>"
                                <?php
                                if($objeto->getIdPerfil()==$perfil['ID_PERFIL']){
                                    echo 'Selected';
                                }
                                ?>
                                >
                                    <?php echo $perfil['PERFIL'] ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="ativo">ATIVO</label>
                        <select class="form-control " name="ativo" id="ativo">
                            <option value="1">Ativo</option>
                            <option
                                <?php
                                if ($objeto->getAtivo() == 0) {
                                    echo 'selected';
                                }
                                ?>
                                    value="0">Inativo
                            </option>
                        </select>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}
