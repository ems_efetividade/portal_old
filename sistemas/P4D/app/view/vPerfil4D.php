<?php
require_once('header.php');

class vPerfil4D extends gridView
{

    public function __construct()
    {
        //$this->viewGrid();
        $this->viewP4D();
    }

    public function viewP4D()
    {
        require_once('/../controller/cPerfil4D.php');
        $control = new cPerfil4D();
        $control->controlSwitch('listar');
        $this->content();
    }

    public function content()
    {
        ?>
        <style>
            ul {
                list-style-type: none;
            }

            #ctn_explorer {
                margin-top: -70px;
                margin-left: 0px;
                padding: 0;
            }
        </style>
        <div id="ctn_explorer">
            <div class="col-md-2"
                 style="background: rgb(42, 63, 84);
                 color: rgb(255, 255, 255); min-height: 900px;
                 paddin:0;margin:0;">
                <br>
                <img width="90" class="center-block" src="/../../public/img/iconsFerramentas/logoEfet.png">

                <h5 class="text-center">
                    EFETIVIDADE
                </h5>
                <hr>
                <div style="overflow-x: auto;">
                    <?php
                    $this->carregarListas();
                    ?>
                </div>
            </div>
            <div id="conteudoPasta">
                <?php
                //$control->carregaPasta(0);
                ?>
            </div>
        </div>
        <script>
            $("#comboLinha").change(function () {
                alert('<?php echo appConf::caminho ?>');
            });
        </script>
        <?php
    }

    public function carregarListas()
    {
        $control = new cPerfil4D();
        $linhas = $control->listarLinha();

        if (is_array($linhas)) {
            echo "<select id='comboLinha' class='form-control'>";
            ?>
            <option id="0">SLECIONE...</option>
            <?php
            foreach ($linhas as $key => $linha) {
                ?>
                <option id="<?php echo $key ?>"><?php echo $linha[0] ?></option>
                <?php
            }
            echo "</select>";
        }
    }

    public function viewGrid()
    {
        $cols = array('ID', 'Empresa', 'BU', 'Linha', 'Perfil', 'Menu', 'Ativo');
        $array_metodo = array('Id', 'NomeEmpresa', 'NomeBu', 'NomeLinha', 'Perfil', 'NomeMenu', 'AtivoTratado');
        $colsPesquisaNome = array('Função Desativada');
        $colsPesquisa = array('id', 'idEmpresa', 'idBu', 'idLinha', 'idMenu', 'dataIn', 'ativo');
        gridView::setGrid_titulo('Controle de Acessos');
        gridView::setGrid_sub_titulo('Controle geral de acessos à ferramentas');
        gridView::setGrid_cols($cols);
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('ControleHeaderPermissoes');
        gridView::setGrid_metodo('listObj');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }
}