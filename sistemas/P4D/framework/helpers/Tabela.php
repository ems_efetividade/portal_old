<?php

class Tabela {
    
    private $data;
    private $configs = array('table-striped', 'table-condensed', 'table-bordered', 'table-hover');
    
    
    public function __construct($data, $conf='') {
        
        $this->data = $data;
        
        $this->configs = ($conf != '') ? $conf : $this->configs;
    }
    
    
    public function create() {
       
        $cabecalho = $this->data[0];
        $td = '';
        
        foreach($cabecalho as $key => $valor) {
            $td .= '<th>'.$key.'</th>';
        }
        
        $header = '<tr>'.$td.'</tr>';
        
        $tabela = '';
        $td = '';
        foreach($this->data as $dado) {
            $td .= '<tr>';
            foreach($dado as $key => $valor) {
               $td .= '<td>'.$valor.'</td>';
            }
            $td .= '</tr>';
        }
        
        $tabela = '<table class="table '.$this->setConfTable().'">'.$header.$td.'</table>';
        
        echo $tabela;
        
       /*  
        echo '<pre>';
        print_r($this->data);
        echo '</pre>';
        */
        
    }
    
    private function setConfTable() {
        
        $css = '';
        foreach($this->configs as $c) {
            $css .= $c.' ';
        }
        
        return $css;
        
    }
    
}