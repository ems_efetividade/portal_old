<?php

require_once('lib/appConf.php');

class appConexaoDW extends appConf
{
    private $cn;
    private $rs;

    public function __construct()
    {
        $this->cn = null;
        $this->rs = null;
    }

    private function abrir()
    {
        // PRODUÇÃO = 1
        // QAS = 2
        // PESSOAL = 3
        $maquina = 2;
        if ($maquina == 1) {
            //só para mestres do conselho Jedis
            $local = '172.16.147.33\SITES';
            $db = 'PNC';
            $user = 'sgpuser';
            $pass = '1qaz@WSX';
        } elseif ($maquina == 2) {
            //Uso para Jedis
            $local = 'ADIFENINA-QD9\SITES';
            $db = 'PNC_DW';
            $user = 'sgpuser';
            $pass = 'ems@123';
        } elseif ($maquina == 3) {
            //uso para padawans
            $local = 'DESKTOP-LRVCOTF\SITES';
            $db = 'PNC_DW';
            $user = 'sa';
            $pass = 'ems@123';
        } elseif ($maquina == 4) {
            //uso paara padawans gordos
            $local = 'localhost\SQLEXPRESS';
            $db = 'PNC_DW';
            $user = 'sa';
            $pass = 'ems@123';
        }
        $connectionInfo = array(
            "Database" => $db,
            "UID" => $user,
            "PWD" => $pass,
            'ReturnDatesAsStrings' => true
        );

        $this->cn = sqlsrv_connect($local, $connectionInfo);
        
        if ($this->cn === false) {
            //echo '<pre>';
            //die("Problema ao conectar! " . print_r(sqlsrv_errors(), true));
            // echo '</pre>';
        }
    }

    public function executar($str)
    {
        $this->abrir();
        $retorno = sqlsrv_query($this->cn, $str);
        $this->fechar();
        return $retorno;
    }

    public function executarQueryArray($str)
    {
        $this->abrir();
        $arr = array();
        $x = 1;
        try {
            $this->rs = sqlsrv_query($this->cn, $str);
            if ($this->rs) {
                do {
                    while ($row = sqlsrv_fetch_array($this->rs)) {
                        foreach ($row as $colName => $val) {
                            $arr[$x][$colName] = utf8_encode($row[$colName]);
                        }
                        $x++;
                    }
                } while (sqlsrv_next_result($this->rs));
                $this->fechar();
                return $arr;
                unset($arr);
            } else {
                // print_r(sqlsrv_errors());
            }
        } catch (Exception $ex) {
            $this->fechar();
            ///print "Houve um Erro ao conectar: <b><pre>" . print_r(sqlsrv_errors()) . "</pre></b><br/>";
            ///die();
        }
    }

    private function next()
    {
        return sqlsrv_next_result($this->rs);
    }

    private function fechar()
    {
        //$this->cn = null;
        sqlsrv_close($this->cn);
    }
}