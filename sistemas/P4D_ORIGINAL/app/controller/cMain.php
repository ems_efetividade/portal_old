<?php

class cMain extends Controller {
    
    private $mTabela;
    private $setor;
    private $maxReg;
    
    public function __construct() {
        parent::__construct();
        $this->mTabela = new mTabela();
        
        $this->setor = fnDadoSessao('setor');
        $this->maxReg = $this->mTabela->getConf('MAX_REGISTRO');
    }
    
    public function main() {

        $this->set('audit', $this->mTabela->getAuditoria());
        $this->set('filtros', $this->mTabela->getMenu());
        $this->set('categorias', $this->mTabela->getCategoria());
        $this->set('maxReg', $this->maxReg);
        $this->render('main/vMain');
    }
    
    public function menuLinha($linha) {
        $dados = $this->mTabela->getMenu($linha);
        
        $html='';
        foreach($dados['esp'] as $dado) {
            $html .= '<div class="filter-text">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="filtro" name="chkESP[]" value="'.trim($dado['ESP']).'"> '.$dado['ESP'].'
                        </label>
                    </div>
                </div>';
        }
        $arr['esp'] = $html;
        
        $html='';
        foreach($dados['merc'] as $dado) {
            $html .= '<div class="filter-text">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="filtro" name="chkMercado[]" value="'.trim($dado['MERCADO']).'"> '.$dado['MERCADO'].'
                        </label>
                    </div>
                </div>';
        }
        $arr['merc'] = $html;
        $arr['setor'] = $dados['setor'];
       
        echo json_encode($arr);
        
    }
    
    public function filtrar() {
        set_time_limit(0);
        ini_set('memory_limit','2000M');
        ini_set("max_execution_time", -1);
        ini_set('max_input_time', -1);
        ini_set('default_socket_timeout', 15);


        

        
        $cores = $this->mTabela->getCategoria();

        foreach($cores as $color) {
            $cor[$color['DESCRICAO']]['RGB'] = $color['COR_RGB'];
            $cor[$color['DESCRICAO']]['BS'] = $color['COR'];
        }

        
        
        $linhaDado    = (isset($_POST['cmbLinha'])) ? $_POST['cmbLinha'] : '';
        $arrSetor = (isset($_POST['chkSetor'])) ? $_POST['chkSetor'] : array($this->setor);
        $arrSetor = $_POST['chkSetor'];
        $arrEsp   = $_POST['chkESP'];
        $arrMerc  = $_POST['chkMercado'];
        
        //if(isset($arrSetor) && isset($arrEsp) && isset($arrMerc)) {
        
        $filtros['setor'] = (isset($arrSetor)) ? implode(', ', $arrSetor) : '';
        $filtros['esp']   = (isset($arrEsp))   ? implode(', ', $arrEsp)   : '';
        $filtros['merc']  = (isset($arrMerc))  ? implode(', ', $arrMerc)  : '';
        
        
        
        $criterio[] = $arrSetor;
        $criterio[] = $arrMerc;
        $criterio[] = $arrEsp;

        //$dados = $this->mTabela->getDados($criterio, $linhaDado);
        
        $ds = $this->mTabela->getDadosGrafico($criterio, $linhaDado);
        
        
        
        
        $dados['dados'] = $ds['data'];
        $dados['share'] = $this->mTabela->getDadosResumo($criterio, $linhaDado);
        $dados['medicos'] = $this->mTabela->getDadosMed($criterio, $linhaDado, $this->maxReg);
        $dados['medicosPerfil'] = $this->mTabela->getDadosMedPerfil($criterio, $linhaDado);
        $dados['query'] = '';


        $array = array();
        
        $perfil = $dados['dados'][1]['PERFIL'];
        $array['name'] = $perfil;
        
        $i=1;
        
        $xm = round($dados['dados'][1]['XM'],2);
        $ym = round($dados['dados'][1]['YM'],2);
        $a = array();
        
        $arrCorLegenda = array();
        $medicosPorc = array();
        $m = 0;
        while($i <= count($dados['dados'])) {
            $m++;
            $dado = $dados['dados'][$i];

            $perfil = $dado['PERFIL'];
            $array['name'] = $perfil;
            $array['index'] = $m;
            $arrCorLegenda[] = $perfil;
            $p = array();
            
            while($perfil == $dado['PERFIL']) {

               $x['color'] = $cor[$perfil]['RGB'];
               $x['id'] = $dado['CRM'];
               $x['classificacao'] = $dado['ID_CATEGORIA'];
               $x['name'] = $dado['CRM'].' '.$dado['NOME'];
               $x['x']    = round($dado['X'],2);
               $x['y']    = round($dado['Y'],2);
               
               $p[] = $x;
               $i++;
               
               $dado = $dados['dados'][$i];
            }
   
            $array['data'] = $p;
            
            $a[] = $array;
            $array = [];
        }
        
        
        $this->set('cor',$cor);
        $this->set('dados', $dados['medicosPerfil']);
        $v = $this->renderToString('main/vListaMedico');
      

        /*
        if($dados['medicos']) {

            $tbData = '<tr>';
            
            $colunas = array_keys($dados['medicos'][1]);
            foreach($colunas as $key=>$value) {
                if (is_numeric($value)) {
                    unset($colunas[$key]);
                } else {
                    $tbData .= '<th>'.$colunas[$key].'</th>';
                }
            }
            $colunas = array_values($colunas);
            $tbData .= '</tr>';
            
            
            foreach($dados['medicos'] as $dado) {     
                $tbData .= '<tr>';
                foreach($colunas as $col) {
                    
                    $tbData .= '<td>'.$dado[$col].'</td>';
                }
                $tbData .= '</tr>';
            }
        }

         */
        if($dados['medicos']) {
            
            $colunas = array_keys($dados['medicos'][1]);
            /*
            foreach($colunas as $key=>$value) {
                if (is_numeric($value)) {
                    unset($colunas[$key]);
                    unset($dados['medicos'][$key]);
                } 
            }
            $colunas = array_values($colunas);
            */
            $tbMerc = '';
            $this->set('colunas', $colunas);
            $this->set('dados',$dados['medicos']);
            $tbMerc = $this->renderToString('main/vListaMedicoMercado');
        }
/*
        $arrCor = array();
        foreach($cor as $c) {
            $arrCor[] = $c['RGB'];
        }
        */

        $arrCor = array();
        foreach($arrCorLegenda as $perf) {
           $arrCor[] = $cor[$perf]['RGB']; 
        }

        $u['cor'] = $arrCor;
        $u['filtro'] = $filtros;
        $u['dados'] = $a;
        $u['mediaX'] = $xm;
        $u['mediaY'] = $ym;
        $u['query'] = $ds['query'];
        $u['table'] = $v;
        $u['tbMercado'] = $tbMerc;
        $u['boxes'] = $dados['share'];
        $u['err'] = $linhaDado;
        
        ini_set("max_execution_time", 300);
        
        echo json_encode($u, JSON_NUMERIC_CHECK);
        
      //  }
    }
    
    
    public function fichaMedico(){
        
        $crm       = $_POST['crm'];
        $adocao    = $_POST['adocao'];
        $potencial = $_POST['potencial'];
        $class     = $_POST['classificacao'];
        $linha     = $_POST['linha'];
        
        $dados = $this->mTabela->fichaMedico($crm, $linha);
        $classificacao = $this->mTabela->getPerfil($class);
        $periodoAtu = $this->mTabela->getPeriodoPX('SEM00');
        
        $this->set('dados', $dados);
        $this->set('adocao', $adocao);
        $this->set('potencial', $potencial);
        $this->set('class', $classificacao);     
        $this->set('periodoPX', $periodoAtu);
        
        $this->render('main/vFichaMedico');
    }
    
    public function medicoPerfil($perfil='') {
        $linhaDado    = (isset($_POST['cmbLinha'])) ? $_POST['cmbLinha'] : '';
        $arrSetor = (isset($_POST['chkSetor'])) ? $_POST['chkSetor'] : array($this->setor);
        $arrEsp = $_POST['chkESP'];
        $arrMerc = $_POST['chkMercado'];
        
        $dados = $this->mTabela->getMedicosPerfil($arrSetor, $arrEsp, $arrMerc, $perfil, $linhaDado);
        
        $filtros['setor'] = implode(', ', $arrSetor);
        $filtros['esp'] = implode(', ', $arrEsp);
        $filtros['merc'] = implode(', ', $arrMerc);
        
        $this->set('perfil', $this->mTabela->getPerfil($perfil));
        $this->set('filtro', $filtros);
        $this->set('dados', $dados);
        $this->render('main/vListaMedicoPerfil');    
        
    }

    
    public function exportar($tipo='') {
        ini_set('memory_limit','2000M');
        $linhaDado    = (isset($_POST['cmbLinha'])) ? $_POST['cmbLinha'] : '';
        $arrSetor = (isset($_POST['chkSetor'])) ? $_POST['chkSetor'] : array($this->setor);
        $arrEsp = $_POST['chkESP'];
        $arrMerc = $_POST['chkMercado'];
        
        $filtros['setor'] = (isset($arrSetor)) ? implode(', ', $arrSetor) : '';
        $filtros['esp']   = (isset($arrEsp))   ? implode(', ', $arrEsp)   : '';
        $filtros['merc']  = (isset($arrMerc))  ? implode(', ', $arrMerc)  : '';

        $criterio[] = $arrSetor;
        $criterio[] = $arrMerc;
        $criterio[] = $arrEsp;
        
        if($tipo == 1) {
            $dados = $this->mTabela->getDadosMedicos($criterio, $linhaDado);
            $arquivo = 'P4D_MEDICOS';
        } else {
            $dados = $this->mTabela->getDadosMed($criterio, $linhaDado,0);
            $arquivo = 'P4D_MERCADO';
        }
        
        if($dados) {
        
            $cab = array_keys($dados[1]);

            foreach($cab as $i=>$cb) {
                if(is_numeric($cb)) {
                    unset($cab[$i]);
                }
            }

            $arquivo .=  '_'.date('Y_m_d_H_i_s').'.csv';

            $df = fopen(APP_EXPORT.'\\'.$arquivo, 'w');

            $a[1] = array('SETOR', $filtros['setor']);
            $a[2] = array('MERCADO', $filtros['merc']);
            $a[3] = array('ESPECIALIDADE', $filtros['esp']);

            fputcsv($df, $a[1],';');
            fputcsv($df, $a[2],';');
            fputcsv($df, $a[3],';');
            fputcsv($df, array(),';');

            fputcsv($df, $cab,';');

            foreach($dados as $row) {
                //$row = array_intersect_key($row, array_flip(array_filter(array_keys($row), 'is_numeric')));
                fputcsv($df, $row ,';', '"');
            }
            fclose($df);
            echo $arquivo;
        }
    }
    
    public function download($arquivo='') {
        
        if(file_exists(APP_EXPORT.'\\'.$arquivo)) {
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="'.$arquivo);
            header('Content-Type: application/x-msexcel');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize(APP_EXPORT.'\\'.$arquivo));
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Expires: 0');

            // Envia o arquivo para o cliente
            readfile(APP_EXPORT.'\\'.$arquivo);
            unlink(APP_EXPORT.'\\'.$arquivo);
        }        
    }
    
}