<?php
ini_set('session.cookie_httponly', 1);
ini_set('session.use_only_cookies', 1);
ini_set('session.cookie_secure',    1);

class Framework {
    
    protected static $controller   = 'cMain';
    protected static $method       = 'main';
    protected static $params       = [];

    public function __construct() {
        
       
    }
    
    public function parseUrl() {
        
        if(isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url']), FILTER_SANITIZE_URL));
            
        }
    }
    
    public function start() {
        
        self::init();
        self::autoload();
        
        // pega a url e gera um array
        $url = self::parseUrl();

       
        
        // verifica se existe o controlador
        if(file_exists(CONTROLLER_PATH . 'c'.ucfirst($url[0]).'.php')) {
            self::$controller = 'c'.ucfirst($url[0]);
            unset($url[0]);
        }
        
        // inclui o controlador na página
        require_once CONTROLLER_PATH . self::$controller.'.php';

        //instancia a classe do controlador
        self::$controller = new self::$controller;
        
        //verifica se existe o metodo na classe do controlador
        if(isset($url[1])) {
            if(method_exists(self::$controller, $url[1])) {
                self::$method = $url[1];
                unset($url[1]);
            }
        }
        
        // verifica se existe parametros
        self::$params = $url ? array_values($url) : [];
        
        // chama a classe com o metodo e os parametros
        call_user_func_array([self::$controller, self::$method], self::$params);
        
        
    }
    
    
   
    
    public function init() {
        define("DS", DIRECTORY_SEPARATOR);
        define("ROOT", getcwd() . DS);

        define("APP_PATH",          ROOT . 'app' . DS);
        define("FRAMEWORK_PATH",    ROOT . "framework" . DS);
        define("PUBLIC_PATH",       ROOT . "public" . DS);

        define("CONFIG_PATH",       APP_PATH . "config" . DS);
        define("CONTROLLER_PATH",   APP_PATH . "controller" . DS);
        define("MODEL_PATH",        APP_PATH . "model" . DS);
        define("VIEW_PATH",         APP_PATH . "view" . DS);
        define("INTER_PATH",        APP_PATH . "interface" . DS);

        define("CORE_PATH",         FRAMEWORK_PATH . "core" . DS);
        define('DB_PATH',           FRAMEWORK_PATH . "database" . DS);
        define("LIB_PATH",          FRAMEWORK_PATH . "libraries" . DS);
        define("HELPER_PATH",       FRAMEWORK_PATH . "helpers" . DS);
        define("UPLOAD_PATH",       PUBLIC_PATH . "uploads" . DS);
        
        define('APP_HOST', 'https://'. $_SERVER['HTTP_HOST']);
        
        @session_start();
        
        require_once APP_PATH . 'config/config.php';
        require_once DB_PATH . 'SQLServer.php';
    }
    
    private static function autoload(){
        spl_autoload_register(array(__CLASS__,'load'));
    }


    // Define a custom load method
    private static function load($classname){

        $file = '';
        
        switch (substr($classname, 0, 1)) {
            
            case 'c' : {
                $file = CONTROLLER_PATH;
                break;
            }
            
            case 'm': {
                $file = MODEL_PATH;
                break;
            }
            
            case 'i': {
                $file = INTER_PATH;
                break;
            }
            
        }
        
        if($classname == 'Controller' || $classname == 'Model') {
            $file = CORE_PATH;
        }
        
        require_once $file.$classname.'.php';
        
//        // Here simply autoload app&rsquo;s controller and model classes
//        if (substr($classname, -10) == "Controller"){
//            // Controller
//            require_once CURR_CONTROLLER_PATH . "$classname.class.php";
//        } elseif (substr($classname, -5) == "Model"){
//            // Model
//            require_once  MODEL_PATH . "$classname.class.php";
//        }

    }
    
}