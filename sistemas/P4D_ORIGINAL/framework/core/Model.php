<?php


class Model {
    
    public $cn;
    
    public function __construct() {    
       $this->cn = new appConexao();
    }
    
    public function createArrObjects($rs, $class) {
        
        $objects = array();
        if($rs) {
            foreach($rs as $row) {
                $object = new $class();
                $object->popular($row);
                $objects[] = $object;
            }
        }
        return $objects;
        
    }
    
    public function getError($rs) {
        if(is_array($rs)) {
            if(isset($rs[1]['ERRO'])) {
                return $rs[1]['ERRO'];
            }
        }
    }
    
    public function sqlError() {
        $errors = sqlsrv_errors();
        if(is_array($errors)) {
            return $errors[0][1].' '.$errors[0][2];
        }
    }
    
    public function replaceSingleQuote($text) {
        return str_replace("'", "`", $text);
    }
    
}