<?php


/* CONFIGURAÇÕES DO PROJETO */
require_once('..\\..\\lib\\appGlobalVar.php');

define("DS", DIRECTORY_SEPARATOR);
define("ROOT", getcwd() . DS);
define('SERVER_URL', $_SERVER['HTTP_HOST']);
define("APP_FOLDER", '/sistemas/PMA/');
define("APP_NAME",   "Programação Mensal de Atividades");

define('APP_URL', APP_HOST.APP_FOLDER);
define('APP_CSS', APP_URL.'public/css/');
define('APP_IMG', APP_URL.'public/img/');
define('APP_JS',  APP_URL.'public/js/');

//define('APP_DATE', '2017-07-10');
define('APP_DATE', date('Y-m-d'));

require_once HELPER_PATH . 'Functions.php';

?>