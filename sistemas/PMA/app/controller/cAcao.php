<?php

class cAcao extends Controller {
    
    private $mAcao;
    private $mCiclo;
    
    public function __construct() {
        parent::__construct();
        
        $this->mAcao = new mAvaliacaoAcao();
        $this->mCiclo = new mCiclo();
    }
    
    public function salvar() {
        
        $idAcao      = (appSanitize::filter($_POST['txtIdAcao']) == '') ? 0 : appSanitize::filter($_POST['txtIdAcao']);
        $idAvaliacao = appSanitize::filter($_POST['txtIdAvaliacao']);
        $relevancia  = appSanitize::filter($_POST['txtRelevancia']);
        $mensurar    = appSanitize::filter($_POST['txtMensurar']);
        $idCiclo     = appSanitize::filter($_POST['txtIdCiclo']);
        $idColabRep  = appSanitize::filter($_POST['txtIdColabRep']);
        $acao        = appSanitize::filter($_POST['txtAcao']);
        $requisito   = appSanitize::filter($_POST['txtRequisito']);
        $passos      = appSanitize::filter($_POST['txtPassos']);
        $prazo       = appSanitize::filter($_POST['cmbPrazo']);
        
        
        
        $this->mAcao->setIdAcao($idAcao);
        $this->mAcao->setIdAvaliacao($idAvaliacao);
        $this->mAcao->setIdCiclo($idCiclo);
        $this->mAcao->setIdColaboradorRep($idColabRep);
        $this->mAcao->setRequisito($requisito);
        $this->mAcao->setPassos($passos);
        $this->mAcao->setAcao($acao);
        $this->mAcao->setRelevancia($relevancia);
        $this->mAcao->setMensurar($mensurar);
        $this->mAcao->setPrazo($prazo);
        $this->mAcao->salvar();
        
        //echo $prazo;
        
        echo $this->listarPlanos($idColabRep, $idAvaliacao);
    }
    
    public function excluir() {
        $idAcao = appSanitize::filter($_POST['txtIdAcao']);
        
        $this->mAcao->setIdAcao($idAcao);
        $this->mAcao->selecionar();
        $idColabRep = $this->mAcao->getIdColaboradorRep();
        $this->mAcao->excluir();
        
        echo $this->listarPlanos($idColabRep);
    }
    
    
    public function salvarStatus() {

        
        $idAcao = appSanitize::filter($_POST['txtIdAcao']);
        $idStatus = appSanitize::filter($_POST['cmbStatus']);
        $observacao = appSanitize::filter($_POST['txtObservacao']);
        
        $x = new mAvaliacaoAcaoCiclo();
        
        $x->setIdCicloAcao($idAcao);
        $x->setIdStatusAcao($idStatus);
        $x->setObservacao($observacao);
        $x->salvar();
        
        $x->setIdCicloAcao($idAcao);
        $x->selecionar();
        
        $a['cor'] = $x->Status->getCor();
        $a['icone'] = $x->Status->getIcone();
        $a['status'] = $x->Status->getDescricao();
        $a['obs'] = $x->getObservacao();
        $a['data'] = fnFormatarData($x->getDataObservacao());
        
        echo json_encode($a);
    }
    
    public function formStatusAcao($idAcao) {
        $x = new mAvaliacaoAcaoCiclo();
        
        $x->setIdCicloAcao($idAcao);
        $x->selecionar();
        
        
        
        $this->set('acao', $x);
        echo $this->renderToString('main/avaliacao/vAcaoObservacao');
    }
    
    public function listarPlanos($idColabRep, $idAval=0) {
        $aval = new mAvaliacao();
        $aval->setIdAvaliacao($idAval);
        $aval->selecionar();
        
        $this->mAcao->setIdColaboradorRep($idColabRep);
        $planos = $this->mAcao->listar();
        
        $this->mCiclo->selecionarData(APP_DATE);
        $idCiclo = $this->mCiclo->getIdCiclo();
        
        $html = '';
        $this->set('statusAval', $aval->getIdStatus());
        foreach($planos as $plano) {
            
            $this->set('idCiclo', $idCiclo);
            $this->set('plano', $plano);
            $html .= $this->renderToString('main/avaliacao/vAcaoLista');
        }
        return $html;
    }
    
    
    
}