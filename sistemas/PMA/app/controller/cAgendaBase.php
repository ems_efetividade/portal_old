<?php

class cAgendaBase extends Controller {
    
    private $mAgendaBase;
    
    public function __construct() {
        parent::__construct();
        
        $this->mAgendaBase = new mAgendaBase();
    }
    
    
    public function listar($idSetor, $dataVisita) {
       
        
        
       
        $medicos = $this->mAgendaBase->listar($dataVisita, $idSetor);
        $tbb = '';
        $tb = '';
        
        $total = count($medicos);
        $endManha = '';
        $endTarde = '';
        
        foreach($medicos as $medico) {
            
            $endereco = $medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento(). ' - '.$medico->getBairro(). ' - '.$medico->getCidade().' - '.$medico->getUF();
            
            if ($medico->getOrdemVisita() == 1 && $medico->getPeriodoVisita() == 'M') {
                $endManha = $endereco;
            }
            
            if ($medico->getOrdemVisita() == 1 && $medico->getPeriodoVisita() == 'T') {
                $endTarde = $endereco;
            }
            
//            $tb .= '<tr class="text-muted">'
//                    
//                    . '<td style="word-wrap:break-word;"><small><small>'.$medico->getCRM().' '.$medico->getNome().'</small></small></td>'
//                    . '<td><small><small>'.$medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().'</small></small></td>'
//                    . '<td><small><small>'.$medico->getBairro().'</small></small></td>'
//                    . '<td><small><small>'.$medico->getCidade().'/'.$medico->getUF().'</small></small></td>'
//                    . '<td><small><small>('.$medico->getPeriodoVisita().$medico->getOrdemVisita().')</small></small></td>'
//                    . '</tr>';
            
            
          $tb .=   '<tr class="text-muted">
                    <td class="text-center" style="vertical-align: middle;">
                        <span class="label label-'.(($medico->getPeriodoVisita() == 'M') ? 'primary' : 'warning').'">'.$medico->getHorarioVisita().'</span> 
                    </td>
                    <td>
                        <div><small><strong>'.$medico->getNome() . ' ('.$medico->getCRM().')'.'</strong></small></div>
                        <small>
                            '.$medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().' - '.$medico->getBairro(). ' - CEP: '.$medico->getCEP().', '.$medico->getCidade().'/'.$medico->getUF().'
                        </small>
                    </td>
                   
              </tr>';
            
            
            
            
//            $tbb .= '<div class="row">
//                        <div class="col-lg-12">
//                            <div><span class="label label-'.(($medico->getPeriodoVisita() == 'M') ? "primary" : "warning").'">'.$medico->getHorarioVisita().'</span> <small><strong>'.$medico->getNome() . ' ('.$medico->getCRM().')'.'</strong></small></div>
//                            <div class="text-muted"><small>'.$medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().'</small></div>
//                            <div class="text-muted"><small>'.$medico->getBairro(). ' - CEP: '.$medico->getCEP().' - '. $medico->getCidade().'/'.$medico->getUF().'</small></div>
//                        </div>
//                    </div>      
//                    <hr style="margin:0px;margin-bottom: 5px; margin-top:5px">';
            
        }
        //$tb .= '</table>';
        
        
        $arr['end_manha'] = $endManha;
        $arr['end_tarde'] = $endTarde;
        $arr['total']     = $total;
        $arr['tabela']    = $tb;
        $arr['sql']       = "EXEC proc_pma_AgendaBaseListarVisita '0', '".$dataVisita."', '".$idSetor."'";
        
        echo json_encode($arr);

    }
    
}