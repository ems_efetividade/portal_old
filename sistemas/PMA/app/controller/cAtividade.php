<?php

class cAtividade extends Controller {
    
    private $mAtividade;
    private $mAvaliacao;
    
    public function __construct() {
        parent::__construct();
        $this->mAtividade = new mAtividade();
        $this->mAvaliacao = new mAvaliacao();
    }
    
    public function form($idAtividade=0, $dataEvento='') {     
        $this->mAtividade->setIdAtividade($idAtividade);
        $this->mAtividade->selecionar();
        
        $this->set('atividade', $this->mAtividade);
        $this->set('dataEvento', $dataEvento);
        $this->set('tipoAtividades', $this->mAtividade->Tipo->listar());
        $this->render('main/atividade/vAtividadeForm');
    }
    
    public function formTrocar($idAtividade=0) {
        $this->set('idAtividade', $idAtividade);
        $this->set('listaTipo', $this->mAtividade->Tipo->listar());
        $this->render('main/atividade/vAtividadeTrocarForm');
    }
    
    public function resumo($idAtividade=0) {

        $this->mAtividade->Ciclo->selecionarData(APP_DATE);
        $ciclo = $this->mAtividade->Ciclo;
        
        $this->mAtividade->setIdAtividade($idAtividade);
        $this->mAtividade->selecionar();
        
        $totalAvaliacao = $this->mAvaliacao->verificarAvaliacaoCiclo($this->mAtividade->getIdColaboradorRep(), $this->mAtividade->getIdCiclo());

        $this->set('totalAvaliacao', $totalAvaliacao);
        $this->set('cicloAtual', $ciclo);
        $this->set('atividade', $this->mAtividade);
        echo $this->renderToString('main/atividade/vAtividadeResumo');
    }
    
    public function salvar() {
        $_POST = appSanitize::filter($_POST);
        
        $idAtividade  = (isset($_POST['txtIdAtividade']))  ? $_POST['txtIdAtividade']    : 0;
        $idSetorGer   = (isset($_POST['txtIdSetorGer']))   ? $_POST['txtIdSetorGer']     : 0;
        $idColabGer   = (isset($_POST['txtIdColabGer']))   ? $_POST['txtIdColabGer']     : 0;
        $idSetorRep   = (isset($_POST['txtIdSetorRep']))   ? $_POST['txtIdSetorRep']     : 0;
        $idColabRep   = (isset($_POST['txtIdColabRep']))   ? $_POST['txtIdColabRep']     : 0;
        $idTipo       = (isset($_POST['cmbTipoEvento']))   ? $_POST['cmbTipoEvento']     : 0;
        $dataEvento   = (isset($_POST['txtDataEvento']))   ? $_POST['txtDataEvento']     : '';
        $objetivo     = (isset($_POST['txtObjetivo']))     ? $_POST['txtObjetivo']       : '';
        $endManha     = (isset($_POST['txtEndManha']))     ? $_POST['txtEndManha']       : ''; 
        $endTarde     = (isset($_POST['txtEndTarde']))     ? $_POST['txtEndTarde']       : '';
        
        
        $this->mAtividade->Ciclo->selecionarData($dataEvento);
        $idCiclo = $this->mAtividade->Ciclo->getIdCiclo();        
        
        $this->mAtividade->setIdAtividade($idAtividade);
        $this->mAtividade->setIdCiclo($idCiclo);
        $this->mAtividade->setIdTipo($idTipo);
        $this->mAtividade->setIdSetorGer($idSetorGer);
        $this->mAtividade->setIdSetorRep($idSetorRep);
        $this->mAtividade->setIdColaboradorGer($idColabGer);
        $this->mAtividade->setIdColaboradorRep($idColabRep);
        $this->mAtividade->setDataEvento($dataEvento);
        $this->mAtividade->setObjetivo($objetivo);
        $this->mAtividade->setEndManha($endManha);
        $this->mAtividade->setEndTarde($endTarde);
        
        echo $this->mAtividade->salvar();
        
    }
    
    public function trocar() {
        
        $idAtividade = appSanitize::filter($_POST['txtIdAtividade']);
        $idTipoTroca = appSanitize::filter($_POST['cmbTroca']);
        $motivo      = appSanitize::filter($_POST['txtMotivo']);
        
        $this->mAtividade->setIdAtividade($idAtividade);
        $this->mAtividade->setIdTipoTroca($idTipoTroca);
        $this->mAtividade->setMotivoTroca($motivo);
        $this->mAtividade->salvarTroca();
    }
    
    public function excluir($idAtividade=0) {
        $this->mAtividade->setIdAtividade($idAtividade);
        $this->mAtividade->excluir();
    }
    
    
    public function visualizarImpressao($id) {        
        $this->mAtividade->setIdAtividade($id);
        $this->mAtividade->selecionar();
        $eventos = array($this->mAtividade);
        
        $nomeArquivo = $this->mAtividade->ColabRep->getNome();

        
        $this->set('nomeReport', 'Dados da Atividade');
        $this->set('eventos', $eventos);
        $html = $this->renderToString('main/atividade/vAtividadeVisualizarImp');
        //fnGerarPDF($html,  $nomeArquivo);
        echo $html;
        
    }
    
    public function imprimir($id) {
        @session_start();
        $_SESSION['dld'] = 1;
        
        $this->mAtividade->setIdAtividade($id);
        $this->mAtividade->selecionar();
        $eventos = array($this->mAtividade);
        
        $nomeArquivo = $this->mAtividade->ColabRep->getNome();

        
        $this->set('nomeReport', 'Dados da Atividade');
        $this->set('eventos', $eventos);
        $html = $this->renderToString('main/atividade/vAtividadePDF');
        unset($_SESSION['dld']);
        fnGerarPDF($html,  $nomeArquivo);
        
    }
    
    public function imprimirCiclo($idSetor, $id) {
     
        
        $this->mAtividade->setIdCiclo($id);
        $this->mAtividade->setIdSetorGer($idSetor);
        $eventos = $this->mAtividade->listarAtividadesCiclo();
        
        $nomeArquivo = 'Atividades';

        $this->set('nomeReport', 'Calendário de Atividades');
        $this->set('eventos', $eventos);
        $html = $this->renderToString('main/atividade/vAtividadePDF');
        
        fnGerarPDF($html,  $nomeArquivo);
        
    }
    
    public function pesquisarRep() {
        $criterio = trim($_POST['txtCriterio']);
        
        $equipe = new mColaboradorSetor();
        $equipe->setIdSetor(fnDadoSessao('id_setor'));
        
        $reps = $equipe->pesquisarReps($criterio);

        
        $html = '';
        if(count($reps) > 0) {
            foreach($reps as $rep) {

                //$img = (file_exists('../../public/profile/'.$rep->getFoto())) ? '../../public/profile/'.$rep->getFoto() : '../../public/img/exemplo_foteoo.jpg';
                $img = fnFotoColaborador($rep->getFoto());

                $html .= '<tr>'
                        . '<td>'
                        . '<img src="'.$img.'" height="50" /> <small class="text-muted">'
                        . '<a href="#" cod="'.$rep->getIdSetor().'|'.$rep->getIdColaborador().'">'.$rep->getSetor().' '.$rep->getColaborador().'</a>'
                        . '</small>'
                        . '</td>'
                        . '</tr>';
            }
        } else {
            $html .= '<tr><td><small class="text-muted">Nenhum registro encontrado!</small></td></tr>';
        }
        echo '<table class="table table-condensed table-striped table-hover">'.$html.'</table>';
    }
    
    public function setSessionDownload($name, $value) {
        @session_start();
        $_SESSION[$name] = $value;
    }
    
    public function getCookieDownload() {
       
        echo $_COOKIE['dld'];
    }
    
}