<?php

class cAvaliacao extends Controller {

    private $mAvaliacao;
    private $mCiclo;
    private $mColabSetor;
    
    private $maxPagina;

    public function __construct() {
        parent::__construct();
        
        $this->maxPagina = 11;
        

        $this->mAvaliacao = new mAvaliacao();
        $this->mCiclo     = new mCiclo();
        $this->mColabSetor = new mColaboradorSetor();
    }

    public function main($pagina=0) {
        $this->listar(1);
    }

    public function listar($pagina=0) {
        $this->mAvaliacao->setIdSetorGer(fnDadoSessao('id_setor'));
        $total = $this->mAvaliacao->total();
        
        $pag = fnPaginacao($total, $this->maxPagina, $pagina);

        $avaliacoes = $this->mAvaliacao->listar($pag['INICIO'], $pag['FIM']);
        
        $this->set('avaliacoes', $avaliacoes);
        $this->set('paginacao', $pag);
        $tabela = $this->renderToString('main/avaliacao/vAvaliacaoTabela');
        
        $this->set('totalAval', $total);
        $this->set('tabelaAvaliacao', $tabela);
        $this->render('main/avaliacao/vAvaliacao');
    }
    
    public function formSelColab() {
        $this->mColabSetor->setIdSetor(fnDadoSessao('id_setor'));
        $this->mCiclo->selecionarCicloAtual();
        
        $this->set('ciclos', $this->mCiclo);
        $this->set('equipe', $this->mColabSetor->selecionarEquipe());
        echo $this->render('main/avaliacao/vAvaliacaoFormSelColab');
    }
    
    public function form($idAvaliacao=0, $idColabRep=0, $idCiclo=0, $idSetor=0) {

        $this->mCiclo->selecionarCicloAtual();
        
        $this->mAvaliacao->setIdAvaliacao($idAvaliacao);
        $this->mAvaliacao->selecionar();
        
        /* Se não existir avaliação... */
        if($idAvaliacao == 0) {
            
            /* coloca o id do colaborador na avaliacao e na acao */
            $this->mAvaliacao->setIdColabRep($idColabRep);
            $this->mAvaliacao->Acao->setIdColaboradorRep($idColabRep);
            
            $this->mAvaliacao->setIdCiclo($idCiclo);
            $this->mAvaliacao->Ciclo->selecionar();
            $this->mAvaliacao->setIdSetorRep($idSetor);
            
            $this->mAvaliacao->ColabRep->setIdColaborador($idColabRep);
            $this->mAvaliacao->ColabRep->selecionar();
            
            $this->mAvaliacao->SetorRep->setIdSetor($idSetor);
            $this->mAvaliacao->SetorRep->selecionar();
        }

        $idPerfil = $this->mAvaliacao->SetorRep->getIdPerfil();
        $questoes = new mAvaliacaoQuestao();
        $questoes->setIdPerfil($idPerfil);

        $resp     = new mAvaliacaoResposta();
        $this->set('aval', $this->mAvaliacao);
        $this->set('dadosProd', $this->getGraficoProdutividade($idSetor));
        $this->set('dadosPontuacao', $this->getDadosPontuacao($idSetor));
        $this->set('respostas', $resp->listar());
        $this->set('modulos', $questoes->listarModulo());
        $this->set('dados', $this->tabelaProdutividade($idSetor, $idAvaliacao));
        $this->set('pontuacao', $this->pontuacao($idSetor));
        $this->set('tabelaPlano', $this->listarPlanos());
        $this->set('cicloAtual', $this->mCiclo);
        $this->set('ultAval', $this->mAvaliacao->selecionarUltima());
        $this->render('main/avaliacao/vAvaliacaoForm');
    }

    public function salvar() {

        /* Capta os dados do formulario */
        $idAvaliacao = appSanitize::filter($_POST['txtIdAvaliacao']);
        $idSetorGer  = appSanitize::filter($_POST['txtIdSetorGer']);
        $idColabGer  = appSanitize::filter($_POST['txtIdColabGer']);
        $idColabRep  = appSanitize::filter($_POST['txtIdColabRep']);
        $idSetorRep  = appSanitize::filter($_POST['txtIdSetorRep']);
        $idCiclo     = appSanitize::filter($_POST['txtIdCiclo']);
        $questao     = appSanitize::filter($_POST['questao']);
        $aspPos      = appSanitize::filter($_POST['txtAspPos']);
        $lacunas     = appSanitize::filter($_POST['txtLacunas']);
        $feedPos     = appSanitize::filter($_POST['txtFeedPos']);
        $feedCorr    = appSanitize::filter($_POST['txtFeedCorr']);

        /* Capta as questões de multipla escolha e coloca num array */
        $arrFinal = [];
        if ($questao) {
            foreach ($questao as $resp) {
                $dados = explode("|", $resp[0]);
                $arr['ID_QUESTAO'] = $dados[0];
                $arr['RESPOSTA'] = $dados[1];
                $arrFinal[] = $arr;
            }
        }

        /* Inputa os valores na classe mAvaliacao */
        $this->mAvaliacao->setIdAvaliacao($idAvaliacao);
        $this->mAvaliacao->setIdSetorGer($idSetorGer);
        $this->mAvaliacao->setIdColabGer($idColabGer);
        $this->mAvaliacao->setIdSetorRep($idSetorRep);
        $this->mAvaliacao->setIdColabRep($idColabRep);
        $this->mAvaliacao->setIdCiclo($idCiclo);
        $this->mAvaliacao->setApectoPos($aspPos);
        $this->mAvaliacao->setLacunas($lacunas);
        $this->mAvaliacao->setFeedPos($feedPos);
        $this->mAvaliacao->setFeedCorr($feedCorr);
        $this->mAvaliacao->setArrResposta($arrFinal);

        /* Salva os dados e retorna o código da avaliação */
        $retorno = $this->mAvaliacao->salvar();
        echo json_encode($retorno);
    }
    
    public function fecharAvaliacao($idAvaliacao) {
        $this->mAvaliacao->setIdAvaliacao($idAvaliacao);
        $this->mAvaliacao->setIdStatus(2);
        $this->mAvaliacao->salvarStatus();
        $this->redirect(APP_URL.'avaliacao');
    }
    
    public function pesquisar() {
        
        $criterio = appSanitize::filter($_POST['txtCriterio']);
        $pagina   = appSanitize::filter($_POST['txtPagina']);
        
        $idSetorGer = fnDadoSessao('id_setor');
        
        $this->mAvaliacao->setIdSetorGer($idSetorGer);
        $total = $this->mAvaliacao->pesquisarTotal($criterio);
        
        $pag = fnPaginacao($total, $this->maxPagina, $pagina);
        
        
        $avals = $this->mAvaliacao->pesquisar($criterio, $pag['INICIO'], $pag['FIM']);

        

        $this->set('avaliacoes', $avals);
        $this->set('paginacao', $pag);
        $tabela = $this->renderToString('main/avaliacao/vAvaliacaoTabela');
        
        echo $tabela;
    }
    
    public function verificarAvaliacao($idColabSetor, $idCiclo) {
        echo $this->mAvaliacao->verificarAvaliacaoCiclo($idColabSetor, $idCiclo);
    }
    
    public function imprimir($idAvaliacao=0, $grafico='', $emBranco=0) {
        $this->mAvaliacao->setIdAvaliacao($idAvaliacao);
        $this->mAvaliacao->selecionar();
        
        $idPerfil = $this->mAvaliacao->SetorRep->getIdPerfil();
        $questoes = new mAvaliacaoQuestao();
        $questoes->setIdPerfil($idPerfil);

        $resp     = new mAvaliacaoResposta();        

        
        $nomeArquivo = 'RAC_'.$this->mAvaliacao->SetorRep->getSetor().'_'.$this->mAvaliacao->ColabRep->getNome().'_Ciclo_'.$this->mAvaliacao->Ciclo->getCiclo().'-'.$this->mAvaliacao->Ciclo->getAno();
        $nomeArquivo = str_replace(" ", "_", $nomeArquivo);
      
        $this->set('respostas', $resp->listar());
        $this->set('modulos',   $questoes->listarModulo());
        $this->set('dados',     $this->tabelaProdutividade($this->mAvaliacao->getIdSetorRep(), $idAvaliacao));
        $this->set('aval',      $this->mAvaliacao);
        
        $this->set('grafico', $grafico);
        $this->set('nomeReport', 'Relatório de Acompanhamento de Campo (RAC)');

        $html = $this->renderToString('main/avaliacao/vAvaliacaoImprimir');
        fnGerarPDF($html,  $nomeArquivo);
		if($grafico != "") {
			$arquivo = getcwd()."\\..\\..\\plugins\\exporting-server\\files\\".$grafico.".png";
			@unlink($arquivo);
		}
    }

    public function imprimirRAC($idAtividade=0, $grafico='') {
      
        $atividade = new mAtividade();
        $atividade->setIdAtividade($idAtividade);
        $atividade->selecionar();
        
        $aval = new mAvaliacao();
        $aval->setIdColabRep($atividade->getIdColaboradorRep());
        $ultAval = $aval->selecionarUltima();
        
        $idSetorRep = $atividade->getIdSetorRep();
        
        $setor = new mSetor();
        $setor->setIdSetor($idSetorRep);
        $setor->selecionar();
        
        $idPerfil = $setor->getIdPerfil();
        $questoes = new mAvaliacaoQuestao();
        $questoes->setIdPerfil($idPerfil);

        $resp     = new mAvaliacaoResposta();        

        
        $nomeArquivo = 'FormBranco';
        $nomeArquivo = str_replace(' ', '_', 'RAC_FORMULARIO_'.$atividade->SetorRep->getSetor().'_'.$atividade->ColabRep->getNome());

        $this->set('respostas', $resp->listar());
        $this->set('modulos',   $questoes->listarModulo());
        $this->set('dados',     $this->tabelaProdutividade($idSetorRep, 0));
        $this->set('aval',      $ultAval);
        $this->set('ativ', $atividade);
        
        $this->set('grafico', $grafico);
        $this->set('nomeReport', 'Relatório de Acompanhamento de Campo (RAC)');
        $html = $this->renderToString('main/avaliacao/vAvaliacaoBranco');
        //echo $html;
        fnGerarPDF($html,  $nomeArquivo);
		
		
		if($grafico != "") {
			$arquivo = getcwd()."\\..\\..\\plugins\\exporting-server\\files\\".$grafico.".png";
			@unlink($arquivo);
		};

    }
    
    public function formAcao($idAcao=0, $idCiclo=0) {
        $acao = new mAvaliacaoAcao();
        $acao->setIdAcao($idAcao);
        $acao->selecionar();
        
        $this->set('idCiclo', $idCiclo);
        $this->set('acao', $acao);
        echo $this->renderToString('main/avaliacao/vAcaoForm');
    }
    
    public function getSVG($idAval) {
        $this->mAvaliacao->setIdAvaliacao($idAval);
        $this->mAvaliacao->selecionar();
        echo json_encode($this->getGraficoProdutividade($this->mAvaliacao->getIdSetorRep()));        
    }
    
    public function getSVGSetor($idSetor=0) {
        echo json_encode($this->getGraficoProdutividade($idSetor));      
    }
    
    public function listarPlanos() {
        $planos = $this->mAvaliacao->Acao->listar();
        $this->mAvaliacao->Ciclo->selecionarData(APP_DATE);
        $idCiclo = $this->mAvaliacao->Ciclo->getIdCiclo();
        
        $html = '';
        foreach($planos as $plano) {
            $this->set('statusAval', $this->mAvaliacao->getIdStatus());
            $this->set('plano', $plano);
            $this->set('idCiclo', $idCiclo);
            $html .= $this->renderToString('main/avaliacao/vAcaoLista');
        }
        return $html;
    }
    
    
    public function getDadosPontuacao($idSetor) {
        $mSetor = new mSetor();
        $mSetor->setIdSetor($idSetor);
        $mSetor->selecionar();
        
        $setor = $mSetor->getSetor();
        
        $painel = new mAvaliacaoPainel();
        $painel->setSetor($setor);
        $rs = $painel->graficoPontuacao();
        
        $dado = $rs[1]['MEDIA'];
        $porcentagem = ($dado * 100) / 4;
        
        
        if($dado <= 1.49) {
            $cor = 'danger';
        } else {
            if($dado >= 1.50 && $dado <= 2.49) {
                $cor = 'warning';
            } else {
                if ($dado >= 2.50 && $dado <= 3.49) {
                    $cor = 'success';
                } else {
                    if($dado >= 3.50 && $dado <= 3.99) {
                        $cor = 'info';
                    } else {
                        $cor = 'primary';
                    }
                }
            }
        }
        
        $arr['MEDIA'] = $dado;
        $arr['COR'] = $cor;
        $arr['PORCENTAGEM'] = $porcentagem;
        
        return $arr;
        
        
        
    }
    public function getGraficoProdutividade($idSetor) {
        
        $mSetor = new mSetor();
        $mSetor->setIdSetor($idSetor);
        $mSetor->selecionar();
        
        $setor = $mSetor->getSetor();
        
        $painel = new mAvaliacaoPainel();
        
        
        $arrC = array();
        $rs = $painel->cabecalhoMeses();

       // print_r($this->painel->cabecalhoMeses());

        for($i=1;$i<=count($rs);$i++) {
                $arrC[$i-1] = $rs[$i]['MES'];
        }

       
        
        $data = '';
        $painel->setSetor($setor);
        $rs = $painel->graficoProdutividade();

        //print_r($rs);

        $arr_final = array();

        for($i=1;$i<=count($rs);$i++) {
                $arr = array();
                $arr['name'] = $rs[$i]['PILAR'];
                $arr['yAxis'] = $rs[$i]['EIXO'];
                if(@$pdf == '1') {
                        $arr['enableMouseTracking'] = false;
                        $arr['shadow'] = false;
                        $arr['animation'] = true;
                }

                for($x=1;$x<=13;$x++) {
                        $valor = $rs[$i]['M'.$x];
                        if($valor == '0') {
                                $valor = null;	
                        }
                        $arr['data'][] = ($valor);	
                }

                array_push($arr_final, $arr);
        }

        $a['cabecalho'] = json_encode($arrC);
        $a['dados']    = json_encode($arr_final, JSON_NUMERIC_CHECK);
        
        return $a;
        

    }
    
    public function tabelaProdutividade($idSetor, $idAvaliacao=0) {
        $mSetor = new mSetor();
        $mSetor->setIdSetor($idSetor);
        $mSetor->selecionar();
        
        $setor = $mSetor->getSetor();
        
        $painel = new mPainelAvaliacao();
        $dados = $painel->getDadosPilares($idAvaliacao, $setor);
        $retorno['dados'] = $dados;
        
        $arr_final = array();
        foreach($dados as $dado) {
            $arr = array();
            $arr['name']   = $dado->getPilar();
            $arr['yAxis']  = $dado->getEixo();
            $arr['data'][] = ($dado->getM1() == 0) ? null : $dado->getM1();
            $arr['data'][] = ($dado->getM2() == 0) ? null : $dado->getM2();
            $arr['data'][] = ($dado->getM3() == 0) ? null : $dado->getM3();
            $arr['data'][] = ($dado->getM4() == 0) ? null : $dado->getM4();
            $arr['data'][] = ($dado->getM5() == 0) ? null : $dado->getM5();
            $arr['data'][] = ($dado->getM6() == 0) ? null : $dado->getM6();
            $arr['data'][] = ($dado->getM7() == 0) ? null : $dado->getM7();
            $arr['data'][] = ($dado->getM8() == 0) ? null : $dado->getM8();
            $arr['data'][] = ($dado->getM9() == 0) ? null : $dado->getM9();
            $arr['data'][] = ($dado->getM10() == 0) ? null : $dado->getM10();
            $arr['data'][] = ($dado->getM11() == 0) ? null : $dado->getM11();
            $arr['data'][] = ($dado->getM12() == 0) ? null : $dado->getM12();
            $arr['data'][] = ($dado->getM13() == 0) ? null : $dado->getM13();
            array_push($arr_final, $arr);
        }
        
        $retorno['grafico'] = json_encode($arr_final, JSON_NUMERIC_CHECK);
        return $retorno;
    }
    
    private function arrumarArray($dados, $pilar, $brasil='') {
        foreach($dados as $prod) {
            $a['PILAR'] = $pilar;
            for($i=1;$i<=13;$i++) {
               $br = isset($brasil[1]['M'.$i]) ? $brasil[1]['M'.$i] : 0;
                
               $a['M'.$i]['VALOR'] = $prod['M'.$i]; 
               $a['M'.$i]['COR'] = $this->rankPilar($prod['M'.$i], $pilar, $br);
            }
            return $a;
        }
    }
    
    private function rankPilar($valor, $pilar, $brasil=0) {
        $classe = '';
        //echo $valor;
        switch($pilar) {
            
            case 'COB. OBJ':
            case 'PRODUTIVIDADE': {
                if($valor == '') {
                    $classe = 'muted';	
		} else {
                    if($valor > 100) {
                        $classe = 'success';	
                    } else {
                        $classe = 'danger';
                    }
		}
                break;
            }
            
            case 'MARKET SHARE':
            case 'PX SHARE': {
                
                if($valor == '' || is_null($valor)) {
                    $classe = 'muted';
		} else {
		
                    if($valor == $brasil) {
                            $classe = 'muted';	
                    } else {
                        if($valor >= $brasil) {
                            $classe = 'success';	
                        } else {
                            $classe =  'danger';
                        }
                    }
		}
                break;
            }
            
            default :{
                $classe = 'text-muted';
                break;
            }
        }
        
        return $classe;
        
    }
    
    public function pontuacao($idSetor) {
        $mSetor = new mSetor();
        $mSetor->setIdSetor($idSetor);
        $mSetor->selecionar();
        
        $setor = $mSetor->getSetor();
        $painel = new mAvaliacaoPainel();
        $painel->setSetor($setor);
        
        return $painel->dadosPontuacao();
    }
	
    
}
