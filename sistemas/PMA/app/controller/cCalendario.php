<?php

class cCalendario extends Controller {
     
    private $mCiclo;
    private $mCalendario;
    private $mAtividade;
    
    public function __construct() {
        parent::__construct();
        $this->mCiclo = new mCiclo();
        $this->mAtividade = new mAtividade();
        $this->mCalendario = new mCalendario();
    }
    
    
    public function main() {
        $this->abrirAgenda('', fnDadoSessao('id_setor'));
    }
    
    
    public function abrirAgenda($idCiclo='', $idSetor='') {
        
        #SELECIONA O CICLO
        if($idCiclo == '') {
            $this->mCiclo->selecionarData(APP_DATE);
        } else {
            $this->mCiclo->setIdCiclo($idCiclo);
            $this->mCiclo->selecionar();
        }
        
        $this->setSession('equipe', $this->getEquipe($idSetor));
        
        $this->set('idSetor', $idSetor);
        $this->set('ciclos', $this->mCiclo->listar(12));
        $this->set('cicloAtual', $this->mCiclo);
        $this->set('atividade', $this->listarAtividades($this->mCiclo->getIdCiclo(), $idSetor));
        $this->set('calendario', $this->mCalendario->gerarCalendario($this->mCiclo->getInicio(), $this->mCiclo->getFim()));
        $this->render('main/calendario/vCalendario');
    }
    
    public function listarAtividades($idCiclo, $idSetor) {
        $this->mAtividade->setIdSetorGer($idSetor);
        $this->mAtividade->setIdCiclo($idCiclo);
        $atividades = $this->mAtividade->listarAtividades();

        
        $ativ = [];
        foreach($atividades as $atividade) {
            $dataEvento = $atividade->getDataEvento();
            
            if(in_array($dataEvento, $ativ)) {
                array_push($ativ[$dataEvento], $atividade);
            } else {
                $ativ[$dataEvento][] = $atividade;
            }
            
           
        }
        return $ativ;
    }
    
    public function getCalendarioMes($mesAno) {
        $data = explode('|', $mesAno);
        echo $this->mCalendario->gerarCalendarioMes($data[0], $data[1]);
    }
    
    public function getEquipe($idSetor='') {
        $idSetor = ($idSetor == '') ? fnDadoSessao('id_setor') : $idSetor;
        $equipe = new mColaboradorSetor();
        $equipe->setIdSetor($idSetor);
        $array = $equipe->selecionarGerentes();
        
        $a = array();
        foreach($array as $setor) {
            $b['ID_SETOR'] = $setor->getIdSetor();
            $b['SETOR'] = $setor->getSetor();
            $b['ID_COL'] = $setor->getIdColaborador();
            $b['COLAB'] = $setor->getColaborador();
            
            $a[] = $b;
        }
        
        return $a;
    }
    
}
