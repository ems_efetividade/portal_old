<?php

class cEvento extends Controller {
    
    private $mCiclo;
    private $mCalendario;
    private $mTipoEvento;
    private $mEvento;
    private $mJustificativa;
    
    public function __construct() {
        $this->mCiclo = new mCiclo();
        $this->mTipoEvento = new mTipoEvento();
        $this->mEvento = new mEvento();
        $this->mJustificativa = new mJustificativa();
        
    }

    
    public function main() {
        
        
        $this->abrirAgenda('', fnDadoSessao('id_setor'));
    }
    
    public function formEvento($idEvento=0, $dataEvento='') {
        
        $this->mEvento->setIdEvento($idEvento);
        $this->mEvento->selecionar();
        
        $this->mCiclo->selecionarProxCiclo();
        $this->mCalendario = new mCalendario($this->mCiclo->getInicio(), $this->mCiclo->getFim());
        
        $this->set('calendario', $this->mCalendario->gShow());
        $this->set('ciclo', $this->mCiclo);
        
        $this->set('listaJus', $this->mJustificativa->listar());
        $this->set('evento', $this->mEvento);
        $this->set('tipoEvento', $this->mTipoEvento->listar());
        $this->Set('dataEvento', $dataEvento);
        $this->set('dataExtenso', $this->mCalendario->dataExtenso($dataEvento));
        $this->render('main/evento/vFormEvento');
    }
    
    public function formJustificativa($idEvento=0) {
        
        $this->set('idEvento', $idEvento);
        $this->set('listaJus', $this->mJustificativa->listar());
        $this->render('main/evento/vFormJustificar');
    }
    
    public function excluirEvento($idEvento=0) {
        $this->mEvento->setIdEvento($idEvento);
        $this->mEvento->excluir();
    }
    
    public function abrirAgenda($idCiclo=0, $idSetor=0) {

        if($idCiclo != 0) {
            $this->mCiclo->setIdCiclo($idCiclo);
            $this->mCiclo->selecionar();
            
        } else {
            $this->mCiclo->selecionarPorData();
        }
        
        $proxCiclo = new mCiclo();
        $proxCiclo->selecionarProxCiclo();
        
        $this->mCalendario = new mCalendario($this->mCiclo->getInicio(), $this->mCiclo->getFim());
        

        $this->set('idSetor', $idSetor);
        $this->set('evento', $this->mEvento);
        $this->set('cicloAtual', $this->mCiclo);
        $this->set('proxCiclo', $proxCiclo);
        $this->set('calendario', $this->mCalendario->gShow());
        $this->set('ciclos', $this->mCiclo->listarUltimos12());
        $this->set('equipe', $this->getEquipe());
        $this->render('main/evento/vEvento');
         
         
    }
    
    public function abrirEvento($idEvento) {
        $this->mEvento->setIdEvento($idEvento);
        $this->mEvento->selecionar();
        
        $this->mCiclo->selecionarPorData();
        
        $this->set('ciclo', $this->mCiclo);
        $this->set('evento', $this->mEvento);
        echo $this->renderToString('main/evento/vEventoResumo');
    }
    

    
    
    public function salvar() {
        
        $idEvento     = (isset($_POST['txtIdEvento']))     ? $_POST['txtIdEvento']       : 0;
        $idSetorGer   = (isset($_POST['txtIdSetorGer']))   ? $_POST['txtIdSetorGer']     : 0;
        $idColabGer   = (isset($_POST['txtIdColabGer']))   ? $_POST['txtIdColabGer']     : 0;
        $idSetorRep   = (isset($_POST['txtIdSetorRep']))   ? $_POST['txtIdSetorRep']     : 0;
        $idColabRep   = (isset($_POST['txtIdColabRep']))   ? $_POST['txtIdColabRep']     : 0;
        $idTipoEvento = (isset($_POST['cmbTipoEvento']))   ? $_POST['cmbTipoEvento']     : 0;
        $dataEvento   = (isset($_POST['txtDataEvento']))   ? $_POST['txtDataEvento']     : '';
        $objetivo     = (isset($_POST['txtObjetivo']))     ? $_POST['txtObjetivo']       : '';
        $endManha     = (isset($_POST['txtEndManha']))     ? $_POST['txtEndManha']       : ''; 
        $endTarde     = (isset($_POST['txtEndTarde']))     ? $_POST['txtEndTarde']       : '';
        $atualizarEnd = (isset($_POST['chkAtualizarEnd'])) ? $_POST['chkAtualizarEnd']   : 0;
        
        $this->mCiclo->selecionarPorData($dataEvento);
        $idCiclo = (isset($_POST['txtIdCiclo']))      ? $_POST['txtIdCiclo']    : $this->mCiclo->getIdCiclo();        
        
        $this->mEvento->setIdEvento($idEvento);
        $this->mEvento->setIdCiclo($idCiclo);
        $this->mEvento->setIdSetorGerente($idSetorGer);
        $this->mEvento->setIdColaboradorGerente($idColabGer);
        $this->mEvento->setIdSetorRep($idSetorRep);
        $this->mEvento->setIdColaboradorRep($idColabRep);
        $this->mEvento->setIdTipoEvento($idTipoEvento);
        $this->mEvento->setDataEvento($dataEvento);
        $this->mEvento->setEndManha($endManha);
        $this->mEvento->setEndTarde($endTarde);
        $this->mEvento->setObjetivo($objetivo);
        $this->mEvento->setAtualizarEndereco($atualizarEnd);
        
        $ok = $this->mEvento->salvar();
        
        if($ok != "") {
            $html .= '<ul>';
            foreach($ok as $erro) {
                $html .= '<li >'.$erro.'</li>';
            }
            $html .= '</ul>';
            echo $html;
        }
        
    }
    
    public function salvarJustificativa() {
        $idEvento = $_POST['txtIdEvento'];
        $idJustif = $_POST['cmbJustificativa'];
        $motivo   = $_POST['txtMotivo'];
        $bloquear = isset($_POST['chkBloquear']) ? $_POST['chkBloquear'] : 0;
        
        $this->mEvento->setIdEvento($idEvento);
        $this->mEvento->setIdJustificativa($idJustif);
        $this->mEvento->setMotivoJustificativa($motivo);
        $this->mEvento->setBloqueado($bloquear);
        $ok = $this->mEvento->justificar();
        
        if($ok != "") {
            $html .= '<ul>';
            foreach($ok as $erro) {
                $html .= '<li >'.$erro.'</li>';
            }
            $html .= '</ul>';
            echo $html;
        }
    }
    
  
    
    public function pesquisarRep() {
        $criterio = trim($_POST['txtCriterio']);
        
        $equipe = new mColaboradorSetor();
        $equipe->setIdSetor(fnDadoSessao('id_setor'));
        
        $reps = $equipe->pesquisarReps($criterio);

        
        $html = '';
        if(count($reps) > 0) {
            foreach($reps as $rep) {

                //$img = (file_exists('../../public/profile/'.$rep->getFoto())) ? '../../public/profile/'.$rep->getFoto() : '../../public/img/exemplo_foteoo.jpg';
                $img = fnFotoColaborador($rep->getFoto());

                $html .= '<tr>'
                        . '<td>'
                        . '<img src="'.$img.'" height="50" /> <small class="text-muted">'
                        . '<a href="#" cod="'.$rep->getIdSetor().'|'.$rep->getIdColaborador().'">'.$rep->getSetor().' '.$rep->getColaborador().'</a>'
                        . '</small>'
                        . '</td>'
                        . '</tr>';
            }
        } else {
            $html .= '<tr><td><small class="text-muted">Nenhum registro encontrado!</small></td></tr>';
        }
        echo '<table class="table table-condensed table-striped table-hover">'.$html.'</table>';
    }
    
    public function getEquipe($idSetor='') {
        
        $idSetor = ($idSetor == '') ? fnDadoSessao('id_setor') : $idSetor;
        
        $equipe = new mColaboradorSetor();
        $equipe->setIdSetor($idSetor);
        return $equipe->selecionarGerentes();
    }
    
    public function dataExtenso($data) {
        $this->mCalendario = new mCalendario();
        echo $this->mCalendario->dataExtenso($data);
    }
    
    public function imprimir($id) {
        $this->mEvento->setIdEvento($id);
        $this->mEvento->selecionar();
        $eventos = array($this->mEvento);
        $nomeArquivo = $this->mEvento->ColabRep->getNome();

        
        $this->set('nomeReport', 'Eventos - Dados do Evento');
        $this->set('eventos', $eventos);
        $html = $this->renderToString('main/evento/vImprimirEventoPDF');
        fnGerarPDF($html,  $nomeArquivo);
    }
    
    public function imprimirCiclo($idSetor, $id) {
        $this->mEvento->setIdCiclo($id);
        $this->mEvento->setIdSetorGerente($idSetor);
        $eventos = $this->mEvento->listarEventoCiclo();
        
        $nomeArquivo = 'AGENDA '.$this->mEvento->ColabGer->getNome();

        $this->set('nomeReport', 'Eventos - Agenda de Eventos');
        $this->set('eventos', $eventos);
        $html = $this->renderToString('main/evento/vImprimirEventoPDF');
        fnGerarPDF($html,  $nomeArquivo);
    }
    
}
