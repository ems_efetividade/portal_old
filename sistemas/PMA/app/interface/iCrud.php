<?php

interface iCrud {
    public function salvar();
    public function selecionar();
    public function excluir();
    public function listar();
}