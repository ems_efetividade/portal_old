<?php

class mAgenda extends mAgendaBase {
    
    private $idAtividade;

    public function __construct() {
        parent::__construct();
        
        $this->idAtividade = 0;
    }
    
    public function setIdAtividade($idAtividade) {
        $this->idAtividade = $idAtividade;
    }

    public function getPrimeiroEndereco($periodo='M', $tipo='ATUAL') {

        $rs = $this->cn->executarQueryArray("exec proc_pma_AgendaPrimeiroEndereco '".$this->idAtividade."', '".$periodo."'");
        return $rs[1][$tipo];
        //$this->popular($rs[1]);
        
        //return $this->getEnderecoCompleto();
    }
    
    public function getPrimeiroEnderecoAnterior($periodo='M') {
        $rs = $this->cn->executarQueryArray("SELECT  *  FROM [PMA_AGENDA] WHERE ID_ATIVIDADE = ".$this->idAtividade." AND PERIODO_VISITA = '".$periodo."' AND ORDEM_VISITA = 1 AND ATIVO = 0 ORDER BY DATA_CADASTRO DESC");
        $this->popular($rs[1]);
        
        return $this->getEnderecoCompleto();
    }    
        
    public function listar() {
        //$rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AGENDA WHERE ATIVO = 1 AND ID_ATIVIDADE = ".$this->idAtividade. " ORDER BY PERIODO_VISITA ASC, ORDEM_VISITA ASC");
        //echo "EXEC proc_pma_AgendaBaseListarVisita '".$this->idAtividade. "', '', ''";
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AgendaBaseListarVisita '".$this->idAtividade. "', '', ''");
        return $this->createArrObjects($rs, $this);
    }
    
    
}