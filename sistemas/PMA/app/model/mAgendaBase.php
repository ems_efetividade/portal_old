<?php

class mAgendaBase extends Model {
    
    private $idSetor;
    private $crm;
    private $nome;
    private $dataVisita;
    private $periodoVisita;
    private $ordemVisita;
    private $endereco;
    private $nro;
    private $complemento;
    private $bairro;
    private $cep;
    private $cidade;
    private $uf;
    private $primeiroEnd;
    
    public function __construct() {
        parent::__construct();
        $this->idSetor = '';
        $this->crm = '';
        $this->nome = '';
        $this->dataVisita = '';
        $this->periodoVisita = '';
        $this->ordemVisita = '';
        $this->endereco = '';
        $this->nro = '';
        $this->complemento = '';
        $this->bairro = '';
        $this->cep = '';
        $this->cidade = '';
        $this->uf = '';
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }

    public function getCrm() {
        return $this->crm;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getDataVisita() {
        return $this->dataVisita;
    }

    public function getPeriodoVisita() {
        return $this->periodoVisita;
    }

    public function getOrdemVisita() {
        return $this->ordemVisita;
    }
    
    public function getHorarioVisita() {
        return $this->periodoVisita.$this->ordemVisita;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getNro() {
        return $this->nro;
    }

    public function getComplemento() {
        return $this->complemento;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getUf() {
        return $this->uf;
    }

    public function getPrimeiroEnd() {
        return $this->primeiroEnd;
    }
    
    public function getEnderecoCompleto() {
        return trim($this->getEndereco().' '.$this->getNro().' '.$this->getComplemento().' '.$this->getBairro(). ' '.$this->getCEP().' '.$this->getCidade().' '.$this->getUF());
    }

        
    public function listar($dataVisita, $idSetor) {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AgendaBaseListarVisita '0', '".$dataVisita."', '".$idSetor."'");
        return $this->createArrObjects($rs, $this);
    }
        
    protected function popular($row) {
        $this->idSetor       = $row['ID_SETOR'];
        $this->crm           = $row['CRM'];
        $this->nome          = $row['NOME'];
        $this->dataVisita    = $row['DATA_VISITA'];
        $this->periodoVisita = $row['PERIODO_VISITA'];
        $this->ordemVisita   = $row['ORDEM_VISITA'];
        $this->endereco      = $row['ENDERECO'];
        $this->nro           = $row['NRO'];
        $this->complemento   = $row['COMPLEMENTO'];
        $this->bairro        = $row['BAIRRO'];
        $this->cep           = $row['CEP'];
        $this->cidade        = $row['CIDADE'];
        $this->uf            = $row['UF'];
        $this->primeiroEnd   = $row['PRIMEIRO'];
    }
    
}
