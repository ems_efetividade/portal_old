<?php

class mAtividade extends Model implements iCrud {
    private $idAtividade;
    private $idCiclo;
    private $idTipo;
    private $idTipoTroca;
    private $idStatus;
    private $idSetorGer;
    private $idColaboradorGer;
    private $idSetorRep;
    private $idColaboradorRep;
    private $dataEvento;
    private $objetivo;
    private $endManha;
    private $endTarde;
    private $cadCiclo;
    private $motivoTroca;
    private $dataTroca;
    private $dataCadastro;
    private $ordem;
    
    public $Agenda;
    public $Ciclo;
    public $Tipo;
    public $TipoTroca;
    public $Status;
    public $SetorRep;
    public $SetorGer;
    public $ColabRep;
    public $ColabGer;
    
       
    public function __construct() {
        parent::__construct();
        $this->idAtividade      = 0;
        $this->idCiclo          = 0;
        $this->idTipo           = 0;
        $this->idTipoTroca      = 0;
        $this->idStatus         = 0;
        $this->idSetorGer       = 0;
        $this->idColaboradorGer = 0;
        $this->idSetorRep       = 0;
        $this->idColaboradorRep = 0;
        $this->dataEvento       = '';
        $this->objetivo         = '';
        $this->endManha         = '';
        $this->endTarde         = '';
        $this->cadCiclo         = '';
        $this->motivoTroca      = '';
        $this->dataTroca        = '';
        $this->dataCadastro     = '';
        $this->ordem            = 0;
        
        $this->Agenda    = new mAgenda();
        $this->Ciclo     = new mCiclo();
        $this->Tipo      = new mAtividadeTipo();
        $this->TipoTroca = new mAtividadeTipo();
        $this->Status    = new mAtividadeStatus();
        $this->SetorGer  = new mSetor();
        $this->SetorRep  = new mSetor();
        $this->ColabGer  = new mColaborador();
        $this->ColabRep  = new mColaborador();
        
    }
    
    public function setIdAtividade($idAtividade) {
        $this->idAtividade = trim($idAtividade);
    }

    public function setIdCiclo($idCiclo) {
        $this->idCiclo = trim($idCiclo);
    }

    public function setIdTipo($idTipo) {
        $this->idTipo = trim($idTipo);
    }

    public function setIdTipoTroca($idTipoTroca) {
        $this->idTipoTroca = trim($idTipoTroca);
    }

    public function setIdStatus($idStatus) {
        $this->idStatus = trim($idStatus);
    }

    public function setIdSetorGer($idSetorGer) {
        $this->idSetorGer = trim($idSetorGer);
    }

    public function setIdColaboradorGer($idColaboradorGer) {
        $this->idColaboradorGer = trim($idColaboradorGer);
    }

    public function setIdSetorRep($idSetorRep) {
        $this->idSetorRep = trim($idSetorRep);
    }

    public function setIdColaboradorRep($idColaboradorRep) {
        $this->idColaboradorRep = trim($idColaboradorRep);
    }

    public function setDataEvento($dataEvento) {
        $this->dataEvento = trim($dataEvento);
    }

    public function setObjetivo($objetivo) {
        $this->objetivo = trim($objetivo);
    }

    public function setEndManha($endManha) {
        $this->endManha = trim($endManha);
    }

    public function setEndTarde($endTarde) {
        $this->endTarde = trim($endTarde);
    }

    public function setCadCiclo($cadCiclo) {
        $this->cadCiclo = trim($cadCiclo);
    }

    public function setMotivoTroca($motivoTroca) {
        $this->motivoTroca = trim($motivoTroca);
    }

    public function setDataTroca($dataTroca) {
        $this->dataTroca = trim($dataTroca);
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = trim($dataCadastro);
    }

    public function setOrdem($ordem) {
        $this->ordem = trim($ordem);
    }

    public function getIdAtividade() {
        return $this->idAtividade;
    }

    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getIdTipo() {
        return $this->idTipo;
    }

    public function getIdTipoTroca() {
        return $this->idTipoTroca;
    }

    public function getIdStatus() {
        return $this->idStatus;
    }

    public function getIdSetorGer() {
        return $this->idSetorGer;
    }

    public function getIdColaboradorGer() {
        return $this->idColaboradorGer;
    }

    public function getIdSetorRep() {
        return $this->idSetorRep;
    }

    public function getIdColaboradorRep() {
        return $this->idColaboradorRep;
    }

    public function getDataEvento() {
        return $this->dataEvento;
    }

    public function getObjetivo() {
        return $this->objetivo;
    }

    public function getEndManha() {
        //return ($this->endManha == '') ? $this->Agenda->getPrimeiroEndereco('M')  : $this->endManha;
        return $this->endManha;
    }

    public function getEndTarde() {
        //return ($this->endTarde == '') ? $this->Agenda->getPrimeiroEndereco('T')  : $this->endTarde;
        return $this->endTarde;
    }

    public function getCadCiclo() {
        return $this->cadCiclo;
    }

    public function getMotivoTroca() {
        return $this->motivoTroca;
    }

    public function getDataTroca() {
        return $this->dataTroca;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function getOrdem() {
        return $this->ordem;
    }
    
    public function getHabTroca() {
        $days_ago = date('Y-m-d', strtotime('-1 days', strtotime(APP_DATE)));
        return $days_ago;
    }
    
    public function getAtividadesAtivas() {
        $rs = $this->cn->executarQueryArray("proc_pma_AtividadeSelecionarAtiva '".$this->dataEvento."', '".$this->idSetorGer."'");
        return $rs[1];
    }

    public function editar() {
        
    }

    public function excluir() {
        $this->cn->executar("EXEC proc_pma_atividadeExcluir '".$this->idAtividade."'");
    }

    public function listar() {
        
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_atividadeSelecionar '".$this->idAtividade."'");
        $this->popular($rs[1]);
    }
    
    public function listarAtividades() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_atividadeListarAtividadeSetor '".$this->idCiclo."', '".$this->idSetorGer."'");
        return $this->createArrObjects($rs, $this);
    }
    
    public function listarAtividadesCiclo() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AtividadeListarCiclo '".$this->idSetorGer."', '".$this->idCiclo."'");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        $query = "EXEC proc_pma_AtividadeSalvar '".$this->idAtividade."', "
                                             . "'".$this->idCiclo."', "
                                             . "'".$this->idTipo."', "
                                             . "'".$this->idTipoTroca."', "
                                             . "'".$this->idSetorGer."', "
                                             . "'".$this->idSetorRep."', "
                                             . "'".$this->idColaboradorGer."', "
                                             . "'".$this->idColaboradorRep."', "
                                             . "'".$this->dataEvento."', "
                                             . "'".utf8_decode($this->objetivo)."', "
                                             . "'".utf8_decode($this->endManha)."', "
                                             . "'".utf8_decode($this->endTarde)."', "
                                             . "'".$this->cadCiclo."' ";
        //echo $query;
        $rs = $this->cn->executarQueryArray($query);
        return $this->getError($rs);
    }
    
    public function salvarTroca() {
        $this->cn->executarQueryArray("EXEC proc_pma_AtividadeSalvarTroca '".$this->idAtividade."', '".$this->idTipoTroca."', '".$this->motivoTroca."'");
    }
    
    protected function popular($row) {
        if($row) {
            
            
            $this->setIdAtividade($row['ID_ATIVIDADE']);
            $this->setIdCiclo($row['ID_CICLO']);
            $this->setIdTipo($row['ID_TIPO']);
            $this->setIdTipoTroca($row['ID_TIPO_TROCA']);
            $this->setIdStatus($row['ID_STATUS']);
            $this->setIdSetorGer($row['ID_SETOR_GER']);
            $this->setIdSetorRep($row['ID_SETOR_REP']);
            $this->setIdColaboradorGer($row['ID_COLABORADOR_GER']);
            $this->setIdColaboradorRep($row['ID_COLABORADOR_REP']);
            $this->setDataEvento($row['DATA_EVENTO']);
            $this->setObjetivo($row['OBJETIVO']);
            $this->setEndManha($row['END_MANHA']);
            $this->setEndTarde($row['END_TARDE']);
            $this->setCadCiclo($row['CAD_CICLO']);
            $this->setMotivoTroca($row['MOTIVO_TROCA']);
            $this->setDataTroca($row['DATA_TROCA']);
            $this->setDataCadastro($row['DATA_CADASTRO']);
            $this->setOrdem($row['ORDEM']);
            
            
            $this->Tipo->setIdTipo($this->idTipo);
            $this->Tipo->selecionar();

            $this->TipoTroca->setIdTipo($this->idTipoTroca);
            $this->TipoTroca->selecionar();

            $this->Status->setIdStatus($this->idStatus);
            $this->Status->selecionar();


            $this->SetorGer->setIdSetor($this->idSetorGer);
            $this->SetorRep->setIdSetor($this->idSetorRep);
            $this->SetorGer->selecionar();
            $this->SetorRep->selecionar();

            $this->ColabGer->setIdColaborador($this->idColaboradorGer);
            $this->ColabRep->setIdColaborador($this->idColaboradorRep);
            $this->ColabGer->selecionar();
            $this->ColabRep->selecionar();

            $this->Ciclo->setIdCiclo($this->idCiclo);
            $this->Ciclo->selecionar();
            
            $this->Agenda->setIdAtividade($this->idAtividade);
            
        }
    
    }

    
}