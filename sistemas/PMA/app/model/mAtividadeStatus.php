<?php

class mAtividadeStatus extends Model implements iCrud {
    
    private $idStatus;
    private $descricao;
    private $icone;
    private $cor;
    
    public function __construct() {
        parent::__construct();
        
        $this->idStatus  = 0;
        $this->descricao = '';
        $this->icone     = '';
        $this->cor       = '';
    }
    
    public function setIdStatus($idStatus) {
        $this->idStatus = $idStatus;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setIcone($icone) {
        $this->icone = $icone;
    }

    public function getIdStatus() {
        return $this->idStatus;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getIcone() {
        return $this->icone;
    }
    
    public function getCor() {
        return $this->cor;
    }    

    public function excluir() {
        
    }

    public function listar() {
        
    }

    public function salvar() {
        
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AtividadeStatusSelecionar '".$this->idStatus."'");
        $this->popular($rs[1]);
    }

    protected function popular($row) {
        $this->idStatus  = $row['ID_STATUS'];
        $this->descricao = $row['DESCRICAO'];
        $this->icone     = $row['ICONE'];
        $this->cor       = $row['COR'];
    }
    
}

