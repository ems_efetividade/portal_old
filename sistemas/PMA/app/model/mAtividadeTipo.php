<?php

class mAtividadeTipo extends Model implements iCrud {
    
    private $idTipo;
    private $descricao;
    private $acomp;
    private $icone;
    private $ativo;
    private $ordem;
    
    public function __construct() {
        parent::__construct();
        
        $this->idTipo    = 0;
        $this->descricao = 0;
        $this->acomp     = 0;
        $this->icone     = '';
        $this->ativo     = 0;
        $this->ordem     = 0;   
    }
    
    public function setIdTipo($idTipo) {
        $this->idTipo = $idTipo;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setAcomp($acomp) {
        $this->acomp = $acomp;
    }
    
    public function setIcone($icone) {
        $this->icone = $icone;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    
    public function getIdTipo() {
        return $this->idTipo;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getAcomp() {
        return $this->acomp;
    }
    
    public function getIcone() {
        return $this->icone;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    
    public function excluir() {
        
    }

    public function listar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AtividadeTipoListar");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AtividadeTipoSelecionar '".$this->idTipo."'");
        $this->popular($rs[1]);
    }

    protected function popular($row) {
        $this->idTipo    = $row['ID_TIPO'];
        $this->descricao = $row['DESCRICAO'];
        $this->acomp     = $row['ACOMP'];
        $this->icone     = $row['ICONE'];
        $this->ativo     = $row['ATIVO'];
        $this->ordem     = $row['ORDEM'];
    }
    
}