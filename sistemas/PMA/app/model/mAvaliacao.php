<?php

class mAvaliacao extends Model implements iCrud {
    
    
    private $idAvaliacao;
    private $idCiclo;
    private $idSetorGer;
    private $idColabGer;
    private $idSetorRep;
    private $idColabRep;
    private $dataCadastro;
    private $apectoPos;
    private $lacunas;
    private $feedPos;
    private $feedCorr;
    private $dataUltSalva;
    private $idStatus;
    private $arrResp;
    
    public $SetorRep;
    public $SetorGer;
    public $ColabRep;
    public $ColabGer;
    public $Ciclo;
    public $Status;
    
    public $Acao;
    
    public function __construct() {
        parent::__construct();
        $this->idAvaliacao  = 0;
        $this->idCiclo      = 0;
        $this->idSetorGer   = 0;
        $this->idColabGer   = 0;
        $this->idSetorRep   = 0;
        $this->idColabRep   = 0;
        $this->idStatus     = 0;
        $this->dataCadastro = '';
        $this->apectoPos    = '';
        $this->lacunas      = '';
        $this->feedPos      = '';
        $this->feedCorr     = '';   
        $this->dataUltSalva = '';
        
        $this->SetorGer  = new mSetor();
        $this->SetorRep  = new mSetor();
        $this->ColabGer  = new mColaborador();
        $this->ColabRep  = new mColaborador();
        
        $this->Ciclo = new mCiclo();
        $this->Acao = new mAvaliacaoAcao();
        $this->Status = new mAvaliacaoStatus();
    }
    
    public function setArrResposta($arr) {
        $this->arrResp = $arr;
    }
    public function getDataUltSalva() {
        return $this->dataUltSalva;
    }

    public function setDataUltSalva($dataUltSalva) {
        $this->dataUltSalva = $dataUltSalva;
    }

    public function setIdAvaliacao($idAvaliacao) {
        $this->idAvaliacao = $idAvaliacao;
    }
    
    public function setIdStatus($idStatus) {
        $this->idStatus = $idStatus;
    }

    
    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function setIdSetorGer($idSetorGer) {
        $this->idSetorGer = $idSetorGer;
    }

    public function setIdColabGer($idColabGer) {
        $this->idColabGer = $idColabGer;
    }

    public function setIdSetorRep($idSetorRep) {
        $this->idSetorRep = $idSetorRep;
    }

    public function setIdColabRep($idColabRep) {
        $this->idColabRep = $idColabRep;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

    public function setApectoPos($apectoPos) {
        $this->apectoPos = $apectoPos;
    }

    public function setLacunas($lacunas) {
        $this->lacunas =$lacunas;
    }

    public function setFeedPos($feedPos) {
        $this->feedPos = $feedPos;
    }

    public function setFeedCorr($feedCorr) {
        $this->feedCorr =$feedCorr;
    }

    public function getIdAvaliacao() {
        return $this->idAvaliacao;
    }

    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getIdSetorGer() {
        return $this->idSetorGer;
    }

    public function getIdColabGer() {
        return $this->idColabGer;
    }

    public function getIdSetorRep() {
        return $this->idSetorRep;
    }

    public function getIdColabRep() {
        return $this->idColabRep;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function getApectoPos() {
        return $this->apectoPos;
    }

    public function getLacunas() {
        return $this->lacunas;
    }

    public function getFeedPos() {
        return $this->feedPos;
    }

    public function getFeedCorr() {
        return $this->feedCorr;
    }
    
    public function getIdStatus() {
        return $this->idStatus;
    }

        
    public function getPorcentagem() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoPercentualResposta '".$this->idAvaliacao."'");
        $porc = $rs[1]['PERC_RESPONDIDO'];
        
        switch ($porc) {
            
            case ($porc >= 100 && $rs[1]['STATUS'] == 1) : {
            $cor = 'warning';    
             break;   
            }
                    
            case ($porc <= 99.9) : {
                $cor = 'danger';
                break;
            }
            
            default: {
                $cor = 'success';
                break;
            }
        }
        
        $a['LABEL'] = fnFormatarMoedaBRL($porc, 1).'%';
        $a['VALOR'] = $porc;
        $a['COR']   = $cor;
        
        return $a;
    }
    
    public function getQuestaoResposta($idQuestao=0) {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoSelecionarResposta '".$this->idAvaliacao."', '".$idQuestao."'");
        return $rs[1]['ID_RESPOSTA'];
    }

    public function excluir() {
        $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoExcluir '".$this->idAvaliacao."'");
    }
    
    public function verificarAvaliacaoCiclo($idColaborador, $idCiclo) {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoVerificarCiclo '".$idColaborador. "' '".$idCiclo."'");
        return $rs[1]['TOTAL'];
    }

    public function total() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoContarAvalGerente '".$this->idSetorGer."'");
        return $rs[1]['TOTAL'];
    }
    
    public function listar($inicio=1, $fim=10) {
        $rs = $this->cn->executarQueryArray("exec proc_pma_AvaliacaoListar '".$this->idSetorGer."', '".$inicio."', '".$fim."'");
        return $this->createArrObjects($rs, $this);
    }
    
    public function salvarStatus() {
        $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoSetStatus '".$this->getIdAvaliacao()."', '".$this->getIdStatus()."'");
    }

    public function salvar() {
        $arrRetorno['id']    = '';
        $arrRetorno['error'] = '';
        
        $query = "EXEC proc_pma_avaliacaoSalvar "
                . "'".$this->replaceSingleQuote($this->getIdAvaliacao())."', "
                . "'".$this->replaceSingleQuote($this->getIdCiclo())."', "
                . "'".$this->replaceSingleQuote($this->getIdSetorGer())."', "
                . "'".$this->replaceSingleQuote($this->getIdColabGer())."', "
                . "'".$this->replaceSingleQuote($this->getIdSetorRep())."', "
                . "'".$this->replaceSingleQuote($this->getIdColabRep())."', "
                . "'".utf8_decode($this->replaceSingleQuote($this->getApectoPos()))."', "
                . "'".utf8_decode($this->replaceSingleQuote($this->getLacunas()))."', "
                . "'".utf8_decode($this->replaceSingleQuote($this->getFeedPos()))."', "
                . "'".utf8_decode($this->replaceSingleQuote($this->getFeedCorr()))."'";
        
        $rs = $this->cn->executarQueryArray($query);
        
        
        
        if($this->sqlError() == '' && $this->getError($rs) == '') {
            
            $idAval = $rs[1]['ID_AVALIACAO'];
            
            if($this->arrResp) {
                $this->cn->executar("DELETE FROM PMA_AVALIACAO_QUESTAO_RESPOSTA WHERE ID_AVALIACAO = ".$idAval);
                foreach($this->arrResp as $resp) {
                    $this->cn->executar("INSERT INTO PMA_AVALIACAO_QUESTAO_RESPOSTA (ID_AVALIACAO, ID_QUESTAO, ID_RESPOSTA) VALUES (".$idAval.", ".$resp['ID_QUESTAO'].", ".$resp['RESPOSTA'].")");
                }
            }
            $this->idAvaliacao = $idAval;
            $arrRetorno['id'] = $idAval;
            $arrRetorno['porc'] = $this->getPorcentagem();
        } else {
            $arrRetorno['error'] = $this->sqlError().' '.$this->getError($rs);
        }
        
        return $arrRetorno;
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO WHERE ID_AVALIACAO = ".$this->idAvaliacao);
        $this->popular($rs[1]);
    }
    
    public function selecionarUltima() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoSelecionarUltima '".$this->idColabRep."'");
        
        $aval = new mAvaliacao();
        $aval->setIdAvaliacao($rs[1]['ID_AVALIACAO']);
        $aval->selecionar();
        
        return $aval;

    }
    
    public function pesquisarTotal($criterio) {
        $rsTotal = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoPesquisarTotal '".$criterio."', '".$this->getIdSetorGer()."'");
        return $rsTotal[1]['TOTAL'];
    }
    
    public function pesquisar($criterio, $inicio=1, $fim=10) {        
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoPesquisar '".$criterio."', '".$this->getIdSetorGer()."', ".$inicio.", ".$fim."");
        return $this->createArrObjects($rs, $this);
    }
    
    protected function popular($row) {
        $this->setIdAvaliacao($row['ID_AVALIACAO']);
        $this->setIdCiclo($row['ID_CICLO']);
        $this->setIdSetorGer($row['ID_SETOR_GER']);
        $this->setIdColabGer($row['ID_COLABORADOR_GER']);
        $this->setIdSetorRep($row['ID_SETOR_REP']);
        $this->setIdColabRep($row['ID_COLABORADOR_REP']);
        $this->setDataCadastro($row['DATA_CADASTRO']);
        $this->setApectoPos($row['ASPECTO_POS']);
        $this->setLacunas($row['LACUNAS']);
        $this->setFeedPos($row['FEEDBACK_POS']);
        $this->setFeedCorr($row['FEEDBACK_CORR']);
        $this->setDataUltSalva($row['DATA_ULT_SALVA']);
        $this->setIdStatus($row['ID_STATUS']);
        
        
        $this->Ciclo->setIdCiclo($this->getIdCiclo());
        $this->Ciclo->selecionar();
        
        $this->SetorGer->setIdSetor($this->getIdSetorGer());
        $this->SetorGer->selecionar();
        
        $this->ColabGer->setIdColaborador($this->getIdColabGer());
        $this->ColabGer->selecionar();
        
        $this->SetorRep->setIdSetor($this->getIdSetorRep());
        $this->SetorRep->selecionar();
        
        $this->ColabRep->setIdColaborador($this->getIdColabRep());
        $this->ColabRep->selecionar();
        
        $this->Acao->setIdColaboradorRep($this->idColabRep);
        
        $this->Status->setIdStatus($this->getIdStatus());
        $this->Status->selecionar();
        
        
    }

    
}