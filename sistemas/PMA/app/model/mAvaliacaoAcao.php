<?php

class mAvaliacaoAcao extends Model implements iCrud {
    private $idAcao;
    private $idAvaliacao;
    private $idCiclo;
    private $idColaboradorRep;
    private $acao;
    private $relevancia;
    private $mensurar;
    private $requisito;
    private $passos;
    private $prazo;
    private $concluido;
    private $dataConclusao;
    
    public $Acao;
    
    public function __construct() {
        parent::__construct();
        $this->idAcao           = 0;
        $this->idAvaliacao      = 0;
        $this->idColaboradorRep = 0;
        $this->acao             = '';
        $this->requisito        = '';
        $this->passos           = '';
        $this->prazo            = '';
        $this->concluido        = 0;
        $this->dataConclusao    = '';
        $this->idCiclo          = 0;
        $this->relevancia = '';
        $this->mensurar = '';
        
        $this->Acao = new mAvaliacaoAcaoCiclo();
        
    }
    
    public function setIdAcao($idAcao) {
        $this->idAcao = $idAcao;
    }

    public function setIdAvaliacao($idAvaliacao) {
        $this->idAvaliacao = $idAvaliacao;
    }

    public function setIdColaboradorRep($idColaboradorRep) {
        $this->idColaboradorRep = $idColaboradorRep;
    }

    public function setAcao($acao) {
        $this->acao = $acao;
    }

    public function setRequisito($requisito) {
        $this->requisito = $requisito;
    }

    public function setPassos($passos) {
        $this->passos = $passos;
    }

    public function setPrazo($prazo) {
        $this->prazo = $prazo;
    }

    public function setConcluido($concluido) {
        $this->concluido = $concluido;
    }

    public function setDataConclusao($dataConclusao) {
        $this->dataConclusao = $dataConclusao;
    }

    public function getIdAcao() {
        return $this->idAcao;
    }

    public function getIdAvaliacao() {
        return $this->idAvaliacao;
    }

    public function getIdColaboradorRep() {
        return $this->idColaboradorRep;
    }

    public function getAcao() {
        return $this->acao;
    }

    public function getRequisito() {
        return $this->requisito;
    }

    public function getPassos() {
        return $this->passos;
    }

    public function getPrazo() {
        return $this->prazo;
    }

    public function getConcluido() {
        return $this->concluido;
    }

    public function getDataConclusao() {
        return $this->dataConclusao;
    }
    
    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function getRelevancia() {
        return $this->relevancia;
    }

    public function getMensurar() {
        return $this->mensurar;
    }

    public function setRelevancia($relevancia) {
        $this->relevancia = $relevancia;
    }

    public function setMensurar($mensurar) {
        $this->mensurar = $mensurar;
    }

        
    
    public function excluir() {
        
        $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoAcaoExcluir ".$this->idAcao."");
    }

    public function listar() {
        
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AcaoListar ".$this->idColaboradorRep."");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        $query = "EXECUTE proc_pma_AvaliacaoAcaoSalvar
                    ".$this->getIdAcao()."
                   ,".$this->getIdCiclo()."
                   ,".$this->getIdAvaliacao()."
                   ,".$this->getIdColaboradorRep()."
                   ,'".utf8_decode($this->getAcao())."'
                   ,'".utf8_decode($this->getRelevancia())."'
                   ,'".utf8_decode($this->getMensurar())."'
                   ,'".utf8_decode($this->getRequisito())."'
                   ,'".utf8_decode($this->getPassos())."'
                   ,".$this->getPrazo()."";
        
        
        
        
        $this->cn->executarQueryArray($query);
        //echo $query;
        //print_r(sqlsrv_errors());
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoAcaoSelecionar ".$this->idAcao."");
        $this->popular($rs[1]);
    }

    
    protected function popular($row) {
        $this->setIdAcao($row['ID_ACAO']);
        $this->setIdCiclo($row['ID_CICLO']);
        $this->setIdAvaliacao($row['ID_AVALIACAO']);
        $this->setIdColaboradorRep($row['ID_COLABORADOR_REP']);
        $this->setAcao($row['ACAO']);
        $this->setConcluido($row['CONCLUIDO']);
        $this->setDataConclusao($row['DATA_CONCLUSAO']);
        $this->setPassos($row['PASSOS']);
        $this->setPrazo($row['PRAZO']);
        $this->setRequisito($row['REQUISITO']);
        $this->setRelevancia($row['RELEVANCIA']);
        $this->setMensurar($row['MENSURAR']);
        
        
        $this->Acao->setIdAcao($this->getIdAcao());
        
    }
}