<?php

class mAvaliacaoAcaoCiclo extends Model {
    private $idCicloAcao;
    private $idStatusAcao;
    private $idCiclo;
    private $idAcao;
    private $observacao;
    private $dataObservacao;
    
    public $Ciclo;
    public $Status;
    
    public function __construct() {
        parent::__construct();
        $this->idStatusAcao = 0;
        $this->idCicloAcao  = 0;
        $this->idCiclo      = 0;
        $this->idAcao       = 0;
        $this->observacao   = '';
        $this->dataObservacao = '';
        
        $this->Status = new mAvaliacaoAcaoStatus();
        $this->Ciclo = new mCiclo();
    }
    
    public function setIdCicloAcao($idCicloAcao) {
        $this->idCicloAcao = $idCicloAcao;
    }
    
    public function setIdStatusAcao($idStatusAcao) {
        $this->idStatusAcao = $idStatusAcao;
    }

    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function setIdAcao($idAcao) {
        $this->idAcao = $idAcao;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }
    
    public function setDataObservacao($dataObservacao) {
        $this->dataObservacao = $dataObservacao;
    }

        
    
    public function getIdCicloAcao() {
        return $this->idCicloAcao;
    }

    public function getIdStatusAcao() {
        return $this->idStatusAcao;
    }
    
    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getIdAcao() {
        return $this->idAcao;
    }

    public function getObservacao() {
        return $this->observacao;
    }
    
    public function getDataObservacao() {
        return $this->dataObservacao;
    }

    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoAcaoCicloSelecionar '".$this->idCicloAcao."'");
        $this->popular($rs[1]);
    }
    
    public function salvar() {
        $query = "EXEC proc_pma_AvaliacaoAcaoSetStatus '".$this->idCicloAcao."', '".$this->idStatusAcao."', '".utf8_decode($this->observacao)."'";
        $this->cn->executar($query);
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_AvaliacaoAcaoCicloListar '".$this->idAcao."'");
        return $this->createArrObjects($rs, $this);
    }

    protected function popular($row) {
        $this->setIdCicloAcao($row['ID_ACAO_CICLO']);
        $this->setIdStatusAcao($row['ID_STATUS_ACAO']);
        $this->setIdAcao($row['ID_ACAO']);
        $this->setIdCiclo($row['ID_CICLO']);
        $this->setObservacao($row['OBSERVACAO']);
        $this->setDataObservacao($row['DATA_OBSERVACAO']);
        
        $this->Status->setIdStatusAcao($this->getIdStatusAcao());
        $this->Status->selecionar();
        
        $this->Ciclo->setIdCiclo($this->getIdCiclo());
        $this->Ciclo->selecionar();
        
    }
    
    
}