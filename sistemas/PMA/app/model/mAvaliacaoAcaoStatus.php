<?php

class mAvaliacaoAcaoStatus extends Model {
    
    private $idStatusAcao;
    private $descricao;
    private $icone;
    private $cor;
    
    public function __construct() {
        parent::__construct();
        $this->idStatusAcao = 0;
        $this->descricao = '';
        $this->icone = '';
        $this->cor = '';
    }
    
    public function setIdStatusAcao($idStatusAcao) {
        $this->idStatusAcao = $idStatusAcao;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setIcone($icone) {
        $this->icone = $icone;
    }

    public function setCor($cor) {
        $this->cor = $cor;
    }

    public function getIdStatusAcao() {
        return $this->idStatusAcao;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getIcone() {
        return $this->icone;
    }

    public function getCor() {
        return $this->cor;
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_ACAO_STATUS WHERE ID_STATUS_ACAO = ".$this->idStatusAcao);
        $this->popular($rs[1]);
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_ACAO_STATUS");
        return $this->createArrObjects($rs, $this);
    }
    
    protected function popular($row) {
       $this->setIdStatusAcao($row['ID_STATUS_ACAO']);
       $this->setDescricao($row['DESCRICAO']);
       $this->setIcone($row['ICONE']);
       $this->setCor($row['COR']);
    }

    
}