<?php

class mAvaliacaoQuestao extends Model implements iCrud {
    private $idQuestao;
    private $idQuestaoPai;
    private $idPerfil;
    private $questao;
    private $ativo;
    
    public function __construct() {
        parent::__construct();
        
        $this->idQuestao = 0;
        $this->idQuestaoPai = 0 ;
        $this->questao = '';
        $this->idPerfil = 0;
        $this->ativo = 0;
    }
    
    public function setIdQuestao($idQuestao) {
        $this->idQuestao = $idQuestao;
    }

    public function setIdQuestaoPai($idQuestaoPai) {
        $this->idQuestaoPai = $idQuestaoPai;
    }

    public function setQuestao($questao) {
        $this->questao = $questao;
    }
    
    public function setIdPerfil($idPerfil) {
        $this->idPerfil = $idPerfil;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    
    public function getIdQuestao() {
        return $this->idQuestao;
    }

    public function getIdQuestaoPai() {
        return $this->idQuestaoPai;
    }

    public function getQuestao() {
        return $this->questao;
    }
    
    public function getIdPerfil() {
        return $this->idPerfil;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    
    public function excluir() {
        
    }

    public function listar() {
        
    }
    
    public function listarModulo() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_QUESTAO WHERE ID_QUESTAO_PAI = 0 AND ATIVO = 1 AND ID_PERFIL = ".$this->idPerfil);
        return $this->createArrObjects($rs, $this);
    }
    
    public function listarQuestao() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_QUESTAO WHERE ID_QUESTAO_PAI = ".$this->idQuestao." AND ATIVO = 1");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        
    }

    public function selecionar() {
        
    }

    
    protected function popular($row) {
        $this->setIdQuestao($row['ID_QUESTAO']);
        $this->setIdQuestaoPai($row['ID_QUESTAO_PAI']);
        $this->setQuestao($row['QUESTAO']);
        $this->setAtivo($row['ATIVO']);
        $this->setIdPerfil($row['ID_PERFIL']);
    }
}