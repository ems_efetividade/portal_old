<?php

class mAvaliacaoResposta extends Model implements iCrud {
    
    private $idResposta;
    private $descricao;
    private $valor;
    private $observacao;
    
    
    public function __construct() {
        parent::__construct();
        
        $this->idResposta = 0;
        $this->descricao  = 0;
        $this->valor      = 0;
        $this->observacao = '';
    }
    
    public function setIdResposta($idResposta) {
        $this->idResposta = $idResposta;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setValor($valor) {
        $this->valor = $valor;
    }

    public function setObservacao($observacao) {
        $this->observacao = $observacao;
    }

    public function getIdResposta() {
        return $this->idResposta;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getValor() {
        return $this->valor;
    }

    public function getObservacao() {
        return $this->observacao;
    }

    public function excluir() {
        
    }

    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_RESPOSTA");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        
    }

    public function selecionar() {
        
    }

    protected function popular($row) {
        $this->setIdResposta($row['ID_RESPOSTA']);
        $this->setDescricao($row['DESCRICAO']);
        $this->setObservacao($row['OBSERVACAO']);
        $this->setValor($row['VALOR']);
    }
    
}