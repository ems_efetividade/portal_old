<?php

class mAvaliacaoStatus extends Model {
    
    private $idStatus;
    private $descricao;
    
    public function __construct() {
        parent::__construct();
        $this->idStatus = 0;
        $this->descricao = '';
    }
    
    public function setIdStatus($idStatus) {
        $this->idStatus = $idStatus;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function getIdStatus() {
        return $this->idStatus;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function salvar() {
        
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_AVALIACAO_STATUS WHERE ID_STATUS = ".$this->getIdStatus());
        $this->popular($rs[1]);
    }
    
    public function excluir() {
        
    }
    
    public function listar() {
        
    }
    
    protected  function popular($row) {
        $this->setIdStatus($row['ID_STATUS']);
        $this->setDescricao($row['DESCRICAO']);
    }
    
}
