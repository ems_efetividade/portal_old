<?php


class mCalendario extends Model implements iCrud {
    private $data;
    private $dia;
    private $mes;
    private $ano;
    private $diaSemana;
    private $nomeDiaSemana;
    private $nomeMes;
    private $diaUtil;
    private $feriado;
    
    public function __construct() {
        parent::__construct();
        $this->data = '';
        $this->dia = '';
        $this->mes = '';
        $this->ano = '';
        $this->diaSemana = '';
        $this->nomeDiaSemana = '';
        $this->nomeMes = '';
        $this->diaUtil = '';
        $this->feriado = '';   
    }
    
    public function setData($data) {
        $this->data = $data;
    }

    public function setDia($dia) {
        $this->dia = $dia;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAno($ano) {
        $this->ano = $ano;
    }

    public function setDiaSemana($diaSemana) {
        $this->diaSemana = $diaSemana;
    }

    public function setNomeDiaSemana($nomeDiaSemana) {
        $this->nomeDiaSemana = $nomeDiaSemana;
    }

    public function setNomeMes($nomeMes) {
        $this->nomeMes = $nomeMes;
    }

    public function setDiaUtil($diaUtil) {
        $this->diaUtil = $diaUtil;
    }

    public function setFeriado($feriado) {
        $this->feriado = $feriado;
    }

    public function getData() {
        return $this->data;
    }

    public function getDia() {
        return $this->dia;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getAno() {
        return $this->ano;
    }

    public function getDiaSemana() {
        return $this->diaSemana;
    }

    public function getNomeDiaSemana() {
        return $this->nomeDiaSemana;
    }

    public function getNomeMes() {
        return $this->nomeMes;
    }

    public function getDiaUtil() {
        return $this->diaUtil;
    }

    public function getFeriado() {
        return $this->feriado;
    }

    
    public function listarCalendario($dataInicio, $dataFim) {
        
    }
    
    public function getLimiteJustificar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_calendarioGetDataLimiteJust");
        return $rs[1]['DATA'];
    }
    
    public function gerarCalendario($dataInicio='', $dataFim='') {
        #Retorna os dias entre os períodos passados;
        $rs   = $this->cn->executarQueryArray("EXEC proc_pma_calendarioGerar '".$dataInicio."', '".$dataFim."'");
        $dias = $this->createArrObjects($rs, $this);
        
        #zera as variavies
        $calendario       = array();
        $calendarioSemana = array();
        $colunaInicial    = 0;
        
        #Pega o dia da semana da data de inicio
        $arr = explode("-", $dataInicio);
        $data = getdate(@mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
        
        #adiciona no array da semana
        while($colunaInicial < $data['wday']) {
            $calendarioSemana[] = new mCalendario();
            $colunaInicial++;
        }
        
        #cria o calendario
        foreach($dias as $ddia) {
            $calendarioSemana[] = $ddia;
            $colunaInicial++;
            
            if($colunaInicial == 7) {
                $calendario[] = $calendarioSemana;
                $calendarioSemana = [];
                $colunaInicial = 0;
            }
        }
        
        $calendario[] = $calendarioSemana;
        return $calendario;
    }
    
    
    public function gerarCalendarioMes($mes='', $ano='') {
        $mes = ($mes == '') ? date('m') : $mes;
        $ano = ($ano == '') ? date('Y') : $ano;
        
        
        $prox = (($mes+1) == 13) ? '1|'.($ano+1) : ($mes+1).'|'.$ano; 
        $ant = (($mes-1) == 0) ? '12|'.($ano-1) : ($mes-1).'|'.$ano;
        
        $rs   = $this->cn->executarQueryArray("SELECT * FROM PMA_CALENDARIO WHERE MES = '".$mes."' AND ANO = '".$ano."'");

        $html = '<div style="background-color: #FFF"><small><table class="text-muted table table-condensed table-bordered">
                    <tr class="active">
                        <th class="text-center">
                            <a href="#" class="btn-calendar" data-month="'.$ant.'"><span class="glyphicon glyphicon-triangle-left"></span></a>
                        </th>
                        <th colspan="5" class="text-center">{NOME_MES}</th>
                        <th class="text-center">
                            <a href="#" class="btn-calendar" data-month="'.$prox.'"><span class="glyphicon glyphicon-triangle-right"></span></a>
                        </th>
                    </tr>
                    <tr class="active">
                        <th>D</th>
                        <th>S</th>
                        <th>T</th>
                        <th>Q</th>
                        <th>Q</th>
                        <th>S</th>
                        <th>S</th>
                    </tr>
                    <tr>';
        
        $c = 1;
        $nomeMes = '';
        foreach($rs as $u => $row) {
           
            if($u == 1) {
                
                $nomeMes = utf8_decode($row['NOME_MES']).'/'.$row['ANO'];
                
                for($i=1;$i<$row['DIA_SEMANA_NUM'];$i++) {
                    $html .= '<td>&nbsp;</td>';
                }
                $c = $i;
                if($c > 7) {
                    $html .= '</tr><tr>';
                    $c = 1;
                }
            }
            
            $dia = $row['DIA'];
            $cls='';
            
            
            if($row['DIA_SEMANA_NUM'] == 7 || $row['DIA_SEMANA_NUM'] == 1) {
                $cls = 'active';
            }
            
            if($row['DIA_UTIL'] == 0 && ($row['DIA_SEMANA_NUM'] != 7 && $row['DIA_SEMANA_NUM'] != 1)) {
                $cls = ' warning';
            }
            
            if($row['DATA'] == date('Y-m-d')) {
                $dia = '<b>'.$row['DIA'].'</b>';
                $cls = 'text-primary';
            }
            
            
            
            $html .= '<td class="text-center '.$cls.'">'.$dia.'</td>';
            $c++;
            
            if($c > 7) {
                $html .= '</tr><tr>';
                $c = 1;
            } 
        }
        
        for($i=$c;$i<=7;$i++) {
            $html .= '<td>&nbsp;</td>';
        }
        
        $html .= '</tr></table></small></div>';
        
        return str_replace('{NOME_MES}', $nomeMes, $html);
        //return $html;
        
    }
    
    public function excluir() {
        
    }

    public function listar() {
        
    }

    public function salvar() {
        
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_calendarioSelecionar '".$this->data."'");
        $this->popular($rs[1]);
    }

    protected function popular($row) {
        $this->data          = $row['DATA'];
        $this->dia           = $row['DIA'];
        $this->mes           = $row['MES'];
        $this->ano           = $row['ANO'];
        $this->diaSemana     = $row['DIA_SEMANA_NUM'];
        $this->nomeDiaSemana = $row['DIA_SEMANA'];
        $this->nomeMes       = $row['NOME_MES'];
        $this->diaUtil       = $row['DIA_UTIL'];
        $this->feriado       = $row['FERIADO'];
    }
    
}