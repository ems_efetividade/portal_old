<?php

class mCalendario extends Model {
    private $data;
    private $dia;
    private $mes;
    private $ano;
    private $semana;
    private $util;
    private $feriado;
    
    private $dataInicio;
    private $dataFim;
    private $calendarioAno;
    
    public function __construct($dataInicio='', $dataFim='') {
        parent::__construct();
        
        $this->dataInicio = $dataInicio;
        $this->dataFim   = $dataFim;
        
        
    }
    
    public function setData($data) {
        $this->data = $data;
    }

    public function setDia($dia) {
        $this->dia = $dia;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAno($ano) {
        $this->ano = $ano;
    }

    public function setSemana($semana) {
        $this->semana = $semana;
    }

    public function setUtil($util) {
        $this->util = $util;
    }

    public function setFeriado($feriado) {
        $this->feriado = $feriado;
    }

    public function setDataIncio($dataIncio) {
        $this->dataIncio = $dataIncio;
    }

    public function setDataFim($dataFim) {
        $this->dataFim = $dataFim;
    }

    public function getData() {
        return $this->data;
    }

    public function getDia() {
        return $this->dia;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getAno() {
        return $this->ano;
    }

    public function getSemana() {
        return $this->semana;
    }

    public function getUtil() {
        return $this->util;
    }

    public function getFeriado() {
        return $this->feriado;
    }

        
    
    public function getCalendario() {
        $this->calendarioAno = $this->cn->executarQueryArray("SELECT DATA, DIA_UTIL, FERIADO FROM PMA_CALENDARIO WHERE DATA BETWEEN '".$this->dataInicio."' AND '".$this->dataFim."'");
        
        
        $calendario = array();
        $calendarioSemana = array();
        
        $colunaInicial = 0;
        $data = $this->explodeData($this->dataInicio);
        
        
        while($colunaInicial < $data['wday']) {
            $calendarioSemana[] = $this->criarObjeto('');
            $colunaInicial++;
        }
        
        while(strtotime($this->dataInicio) <= strtotime($this->dataFim)) {
            
            $data = $this->explodeData($this->dataInicio);
            $calendarioSemana[] = $this->criarObjeto($data);
            
            
            $colunaInicial++;
            if($colunaInicial == 7 || strtotime($this->dataInicio) == strtotime($this->dataFim)) {
                $calendario[] = $calendarioSemana;
                $calendarioSemana = [];
                $colunaInicial = 0;
            }
            
            
            $this->dataInicio = date('Y-m-d', strtotime($this->dataInicio. ' +1 day'));
        }
        
        return $calendario;
    }
    
    public function listarAtividade() {
        
    }
    
    private function criarObjeto($data='') {
        $c = new mCalendario();
        if($data != '') {
            
            $util = 1;
            $feriado = '';
            $dia = $this->searchArray($this->dataInicio);
            
            if($data['wday'] == 0 || $data['wday'] == 6 || $dia['DIA_UTIL'] == 0) {
                $util = 0;
                $feriado = $dia['FERIADO'];
            }
            
            
            
            
            $c->setData($this->dataInicio);
            $c->setDia($data['mday']);
            $c->setMes($data['mon']);
            $c->setAno($data['year']);
            $c->setSemana($data['wday']);
            $c->setFeriado($feriado);
            $c->setUtil($util);
        }
        return $c;
    }
    
    private function explodeData($data) {
        $arr = explode("-", $data);
        $a = getdate(mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
        return $a;
    }
    
    private function searchArray($data) {
        foreach($this->calendarioAno as $cal) {
            if($cal['DATA'] == $data) {
                return $cal;
                break;
            }
        }
    }
}