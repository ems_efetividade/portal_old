<?php

class mCalendario extends Model {
    
    private $dataInicio;
    private $dataFinal;
    
    private $nomeMes;
    private $nomeSemana;
    
    public function __construct($dataInicio='', $dataFinal='') {
        parent::__construct();
        
        $this->dataInicio = $dataInicio;
        $this->dataFinal  = $dataFinal;
        
        $this->nomeMes[1]  = "Janeiro";
        $this->nomeMes[2]  = "Fevereiro";
        $this->nomeMes[3]  = "Março";
        $this->nomeMes[4]  = "Abril";
        $this->nomeMes[5]  = "Maio";
        $this->nomeMes[6]  = "Junho";
        $this->nomeMes[7]  = "Julho";
        $this->nomeMes[8]  = "Agosto";
        $this->nomeMes[9]  = "Setembro";
        $this->nomeMes[10] = "Outubro";
        $this->nomeMes[11] = "Novembro";
        $this->nomeMes[12] = "Dezembro";
        
        
        $this->nomeSemana[0] = 'Domingo';
        $this->nomeSemana[1] = 'Segunda-Feira';
        $this->nomeSemana[2] = 'Terça-Feira';
        $this->nomeSemana[3] = 'Quarta-Feira';
        $this->nomeSemana[4] = 'Quinta-Feira';
        $this->nomeSemana[5] = 'Sexta-Feira';
        $this->nomeSemana[6] = 'Sábado';
    }

    public function gShow() {
       
        $col = 0;
        
        $semana = $this->getDiaSemana($this->dataInicio);
        
        
        
        $arr = [];
        while($col < $semana) {
            $data['DATA']     = '';
            $data['SEMANA']   = '';
            $data['DIA']      = '';
            $data['MES']      = '';
            $data['ANO']      = '';
            $data['MES_NOME'] = '';
            $data['DIA_UTIL'] = '';
            $data['FERIADO'] = '';
            
            
            $arr[] = $data;
            
            $col++;
        }
        
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_CALENDARIO WHERE DATA BETWEEN '".$this->dataInicio."' AND '".$this->dataFinal."'");

        //echo "SELECT * FROM PMA_CALENDARIO WHERE DATA BETWEEN '".$this->dataInicio."' AND '".$this->dataFinal."'";
        
        while(strtotime($this->dataInicio) <= strtotime($this->dataFinal)) {
            
            //$key = array_search($this->dataInicio, array_column($rs, 'DATA'));
            $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_CALENDARIO WHERE DATA = '".$this->dataInicio."'");
            
            $d = explode('-', $this->dataInicio);
            $semana = $this->getDiaSemana($this->dataInicio);
            $nomeMes = $this->getNomeMes($d[1],1);

            $data['DATA']     = $this->dataInicio;
            $data['SEMANA']   = $semana;
            $data['DIA']      = $d[2];
            $data['MES']      = $d[1];
            $data['ANO']      = $d[0];
            $data['MES_NOME'] = $nomeMes;
            $data['DIA_UTIL'] = $rs[1]['DIA_UTIL'];
            $data['FERIADO'] =  $rs[1]['FERIADO'];


            $arr[] = $data;

                
          
            $col++;

            if($col == 7 || strtotime($this->dataInicio) == strtotime($this->dataFinal)) {
                $calendario[] = $arr;
                $arr = [];
                $col = 0;
            }
            
            
            
            $this->dataInicio = date('Y-m-d', strtotime($this->dataInicio. ' +1 day'));
            
        }
        
        return $calendario;
        
        
        
    }
    
    public function dataExtenso($data) {
        $semana = $this->getDiaSemana($data);
        $arr = explode("-", $data);
        
        return $this->nomeSemana[$semana].', '.$arr[2].' de '.$this->nomeMes[(int)$arr[1]].' de '.$arr[0];
    }
    
    private function getDiaSemana($data='') {
        $arr = explode("-", $data);
        $week = getdate(mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
        return $week['wday'];
    }
    
    private function getNomeMes($mes, $abrev=0) {
        $mes = (int) $mes;
        return ($abrev == 0) ? $this->nomeMes[$mes] : substr($this->nomeMes[$mes], 0, 3);
        
    }
    
    
    
    
    
    
}