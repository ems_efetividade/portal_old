<?php

class mCiclo extends Model implements iCrud {
    
    private $idCiclo;
    private $ciclo;
    private $ano;
    private $inicio;
    private $fim;
    private $abertura;
    private $fechamento;
    private $diasUteis;
    private $idStatusCiclo;
    private $statusCiclo;
    
    
    function __construct() {
        parent::__construct();
        
        $this->idCiclo    = 0;
        $this->ciclo      = 0;
        $this->ano        = 0;
        $this->inicio     = '';
        $this->fim        = '';
        $this->abertura   = '';
        $this->fechamento = '';
        $this->diasUteis  = 0;
        $this->statusCiclo = 0;
        $this->idStatusCiclo = 1;
        
        
        $this->statusCiclo = array(1 => 'Fechado', 
                                   2 => 'Ativo', 
                                   3 => 'Aberto');
        
        
    }
    
    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function setCiclo($ciclo) {
        $this->ciclo = $ciclo;
    }

    public function setAno($ano) {
        $this->ano = $ano;
    }

    public function setInicio($inicio) {
        $this->inicio = $inicio;
    }

    public function setFim($fim) {
        $this->fim = $fim;
    }

    public function setAbertura($abertura) {
        $this->abertura = $abertura;
    }

    public function setFechamento($fechamento) {
        $this->fechamento = $fechamento;
    }

    public function setDiasUteis($diasUteis) {
        $this->diasUteis = $diasUteis;
    }

    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getCiclo() {
        return $this->ciclo;
    }

    public function getAno() {
        return $this->ano;
    }

    public function getInicio() {
        return $this->inicio;
    }

    public function getFim() {
        return $this->fim;
    }

    public function getAbertura() {
        return $this->abertura;
    }

    public function getFechamento() {
        return $this->fechamento;
    }

    public function getDiasUteis() {
        return $this->diasUteis;
    }
    
    public function getIdStatusCiclo() {
        return $this->idStatusCiclo;
    }
    
    public function getStatusCiclo() {
       return $this->statusCiclo[$this->idStatusCiclo];
    }
    
    public function getNomeCiclo() {
        return $this->ciclo.'/'.$this->ano;
    }
    
    public function getDiasRestantes() {
        $inicio = new DateTime(date('Y-m-d H:i:s'));
        $fim = new DateTime($this->fim);
        
        $dias = $inicio->diff($fim);
        return $dias->days;
    }

    public function excluir() {
        
    }

    public function listar($max=0) {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_cicloListar '".$max."' ");
        return $this->createArrObjects($rs, $this);
    }

    public function salvar() {
        
    }
    
    public function dataNoCiclo($data) {
        $data = strtotime(date($data));
        $inicio = strtotime($this->getInicio());
        $fim = strtotime($this->getFim());
        
        if(($data >= $inicio) && ($data <= $fim)) {
            return 1;
        } else {
            return 0;
        }
    }
    
    public function validarDiaCicloAtual($data) {
        
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_cicloSelecionar '".$this->idCiclo."' ");
        $this->popular($rs[1]);
    }
    
    public function selecionarCicloAtual() {
        $this->selecionarData(APP_DATE);
    }
    
    public function selecionarData($data='') {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_cicloSelecionarData '".$data."'");
        $this->popular($rs[1]);
    }

        
    
    
    protected function popular($row) {
        $this->idCiclo    = $row['ID_CICLO'];
        $this->ciclo      = $row['CICLO'];
        $this->ano        = $row['ANO'];
        $this->inicio     = $row['INICIO'];
        $this->fim        = $row['FIM'];
        $this->abertura   = $row['ABERTURA'];
        $this->fechamento = $row['FECHAMENTO'];
        $this->diasUteis  = $row['DIAS_UTEIS'];
        $this->setStatusCiclo();
    }
    
    private function setStatusCiclo() {
        $data   = strtotime(date(APP_DATE));
        $inicio = strtotime($this->getInicio());
        $fim    = strtotime($this->getFim());
        
        $idStatus = 1;
        
        if($data > $fim) {
            $idStatus = 1;
        } else {
        
            if(($data >= $inicio) && ($data <= $fim)) {
                $idStatus = 2;
            } else {
                $idStatus = 3;
            }
        }
        
        $this->idStatusCiclo = $idStatus;
    }
    
    

    

    
    
}
