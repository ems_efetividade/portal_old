<?php

class mColaborador extends Model {
    
    private $idColaborador;
    private $nome;
    private $foto;
    
    public function __construct() {
        parent::__construct();
        $this->idColaborador = 0;
        $this->nome = '';
        $this->foto = '';
    }

    public function getFoto() {
        return $this->foto;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }

        public function getIdColaborador() {
        return $this->idColaborador;
    }

    public function getNome() {
        return $this->nome;
    }
    
    public function getNomeAbrev() {
        $arr = explode(" ", $this->nome);
        return trim($arr[0].' '.$arr[count($arr)-1]);
    }

    public function setIdColaborador($idColaborador=0) {
        $this->idColaborador = $idColaborador;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT C.ID_COLABORADOR, C.NOME, F.FOTO FROM COLABORADOR C LEFT JOIN FOTO_PERFIL F ON F.ID_COLABORADOR = C.ID_COLABORADOR WHERE C.ID_COLABORADOR = ".$this->idColaborador);
        $this->popular($rs[1]);
    }

    protected function popular($row){
        $this->idColaborador = $row['ID_COLABORADOR'];
        $this->nome = $row['NOME'];
        $this->foto = $row['FOTO'];
    }
    
}