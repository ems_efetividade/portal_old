<?php


class mColaboradorSetor extends Model {
    
    private $idSetor;
    private $idColaborador;
    private $setor;
    private $colaborador;
    private $email;
    private $foneCorp;
    private $fonePart;
    private $foto;
    
    public function __construct() {
        parent::__construct();
        
        $this->idSetor = 0;
        $this->idColaborador = 0;
        $this->setor = '';
        $this->colaborador = '';
        $this->email = '';
        $this->foneCorp = '';
        $this->fonePart = '';
        $this->foto = '';
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }

    public function getIdColaborador() {
        return $this->idColaborador;
    }

    public function getSetor() {
        return $this->setor;
    }

    public function getColaborador() {
        return $this->colaborador;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setIdSetor($idSetor) {
        $this->idSetor = $idSetor;
    }

    public function setIdColaborador($idColaborador) {
        $this->idColaborador = $idColaborador;
    }

    public function setSetor($setor) {
        $this->setor = $setor;
    }

    public function setColaborador($colaborador) {
        $this->colaborador = $colaborador;
    }

    public function setEmail($email) {
        $this->email = $email;
    }
    public function getFoneCorp() {
        return $this->foneCorp;
    }

    public function getFonePart() {
        return $this->fonePart;
    }

    public function setFoneCorp($foneCorp) {
        $this->foneCorp = $foneCorp;
    }

    public function setFonePart($fonePart) {
        $this->fonePart = $fonePart;
    }

    public function getFoto() {
        return $this->foto;
    }

    public function setFoto($foto) {
        $this->foto = $foto;
    }

    public function selecionarSetor() {
        
    }
    
    public function selecionarColaborador() {
        
    }
    
    public function selecionarEquipe() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM vw_ColaboradorSetor WHERE NIVEL = ".$this->idSetor);
        return $this->createArrObjects($rs, $this);
    }
    
    public function selecionarGerentes() {
        //$rs = $this->cn->executarQueryArray("SELECT * FROM vw_ColaboradorSetor WHERE NIVEL = ".$this->idSetor." AND ID_PERFIL <> 1");
        $rs = $this->cn->executarQueryArray("WITH V AS (
                                                SELECT * FROM vw_colaboradorSetor WHERE ID_SETOR = ".$this->idSetor."
                                                UNION ALL
                                                SELECT VV.* FROM vw_colaboradorSetor VV INNER JOIN V ON V.ID_SETOR = VV.NIVEL
                                            )
                                            SELECT * FROM V WHERE ID_PERFIL <> 1 AND ID_SETOR <> ".$this->idSetor." ORDER BY SETOR ASC");
        return $this->createArrObjects($rs, $this);
    }
    
    public function selecionarReps() {
        $rs = $this->cn->executarQueryArray("WITH V AS (
                                                SELECT * FROM vw_colaboradorSetor WHERE ID_SETOR = ".$this->idSetor."
                                                UNION ALL
                                                SELECT VV.* FROM vw_colaboradorSetor VV INNER JOIN V ON V.ID_SETOR = VV.NIVEL
                                            )
                                            SELECT * FROM V WHERE ID_PERFIL = 1 ORDER BY SETOR ASC");
        return $this->createArrObjects($rs, $this);
    }
    
    public function pesquisarReps($criterio='') {
        
        
        $rs = $this->cn->executarQueryArray("WITH V AS (
                                                SELECT * FROM vw_colaboradorSetor WHERE ID_SETOR = ".$this->idSetor."
                                                UNION ALL
                                                SELECT VV.* FROM vw_colaboradorSetor VV INNER JOIN V ON V.ID_SETOR = VV.NIVEL
                                            )
                                            SELECT TOP 10 * FROM V WHERE SETOR + ' ' + NOME LIKE '%".$criterio."%' AND ID_PERFIL = 1 ORDER BY SETOR ASC");
        return $this->createArrObjects($rs, $this);
    }    
    
    protected function popular($row) {
        $this->idSetor       = $row['ID_SETOR'];
        $this->idColaborador = $row['ID_COLABORADOR'];
        $this->setor         = $row['SETOR'];
        $this->colaborador   = $row['NOME'];
        $this->email         = $row['EMAIL'];
        $this->foneCorp      = $row['FONE_CORPORATIVO'];
        $this->fonePart      = $row['FONE_PARTICULAR'];
        $this->foto = $row['FOTO'];
    }
    
}
