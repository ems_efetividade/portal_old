<?php

class mEvento extends Model {
    
    private $idEvento;
    private $idJustificativa;
    private $idStatusEvento;
    private $idTipoEvento;
    private $idCiclo;
    private $idSetorGerente;
    private $idColaboradorGerente;
    private $idSetorRep;
    private $idColaboradorRep;
    private $dataEvento;
    private $endManha;
    private $endTarde;
    private $objetivo;
    private $cadNoCiclo;
    private $motivoJustificativa;
    private $dataJustificativa;
    private $atualizarEndereco;
    private $dataCadastro;
    private $ordem;
    private $bloqueado;
    
    private $AgendaBase;
    
    public $SetorRep;
    public $SetorGer;
    
    public $ColabRep;
    public $ColabGer;
    
    public $TipoEvento;
    public $Status;
    public $Just;
    public $Agenda;
    public $Endereco;
    public $Ciclo;
    
    
    public function __construct() {
        parent::__construct();
        
        $this->idEvento = 0;
        $this->idStatusEvento = 1;
        $this->idJustificativa = 0;
        $this->idTipoEvento = 0;
        $this->idCiclo = 0;
        $this->idSetorGerente = 0;
        $this->idColaboradorGerente = 0;
        $this->idSetorRep = 0;
        $this->idColaboradorRep = 0;
        $this->dataEvento = '';
        $this->objetivo = '';
        $this->cadNoCiclo = 0;
        $this->motivoJustificativa = '';
        $this->dataJustificativa = '';
        $this->atualizarEndereco = 0;
        $this->dataCadastro = '';
        $this->substituido = 0;
        $this->dataSubstituido = '';
        
        $this->SetorGer = new mSetor();
        $this->SetorRep = new mSetor();
        
        $this->ColabGer  = new mColaborador();
        $this->ColabRep  = new mColaborador();
        
        $this->TipoEvento = new mTipoEvento();
        $this->Status = new mEventoStatus();
        $this->Just = new mJustificativa();
        $this->Agenda = new mAgenda();
        $this->Endereco = new mEventoEndereco();
        $this->Ciclo = new mCiclo();
    }
    
    public function getBloqueado() {
        return $this->bloqueado;
    }

    public function setBloqueado($bloqueado) {
        $this->bloqueado = $bloqueado;
    }

    
    public function setEndManha($endManha) {
        $this->endManha = $endManha;
    }
    
    public function setEndTarde($endTarde) {
        $this->endTarde = $endTarde;
    }    
    
    public function getCadNoCiclo() {
        return $this->cadNoCiclo;
    }

    public function getMotivoJustificativa() {
        return $this->motivoJustificativa;
    }

    public function getDataJustificativa() {
        return $this->dataJustificativa;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function setCadNoCiclo($cadNoCiclo) {
        $this->cadNoCiclo = $cadNoCiclo;
    }

    public function setMotivoJustificativa($motivoJustificativa) {
        $this->motivoJustificativa = $motivoJustificativa;
    }

    public function setDataJustificativa($dataJustificativa) {
        $this->dataJustificativa = $dataJustificativa;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

        
    public function getIdEvento() {
        return $this->idEvento;
    }

    public function getIdStatusEvento() {
        return $this->idStatusEvento;
    }

    public function getIdJustificativa() {
        return $this->idJustificativa;
    }

    public function getIdTipoEvento() {
        return $this->idTipoEvento;
    }

    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getIdSetorGerente() {
        return $this->idSetorGerente;
    }

    public function getIdColaboradorGerente() {
        return $this->idColaboradorGerente;
    }

    public function getIdSetorRep() {
        return $this->idSetorRep;
    }

    public function getIdColaboradorRep() {
        return $this->idColaboradorRep;
    }

    public function getDataEvento() {
        return $this->dataEvento;
    }

    public function getObjetivo() {
        return $this->objetivo;
    }


    public function getAtualizarEndereco() {
        return $this->atualizarEndereco;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function setIdStatusEvento($idStatusEvento) {
        $this->idStatusEvento = $idStatusEvento;
    }

    public function setIdJustificativa($idJustificativa) {
        $this->idJustificativa = $idJustificativa;
    }

    public function setIdTipoEvento($idTipoEvento) {
        $this->idTipoEvento = $idTipoEvento;
    }

    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function setIdSetorGerente($idSetorGerente) {
        $this->idSetorGerente = $idSetorGerente;
    }

    public function setIdColaboradorGerente($idColaboradorGerente) {
        $this->idColaboradorGerente = $idColaboradorGerente;
    }

    public function setIdSetorRep($idSetorRep) {
        $this->idSetorRep = $idSetorRep;
    }

    public function setIdColaboradorRep($idColaboradorRep) {
        $this->idColaboradorRep = $idColaboradorRep;
    }

    public function setDataEvento($dataEvento) {
        $this->dataEvento = $dataEvento;
    }

    public function setObjetivo($objetivo) {
        $this->objetivo = $objetivo;
    }

    public function setLancAbertura($lancAbertura) {
        $this->lancAbertura = $lancAbertura;
    }

    public function setJustificativa($justificativa) {
        $this->justificativa = $justificativa;
    }

    public function setAtualizarEndereco($atualizarEndereco) {
        $this->atualizarEndereco = $atualizarEndereco;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

        
    public function salvar() {
        //echo $this->query();
        
        $validar = $this->validar();
        
        if(count($validar) <= 0) {
            
            $rs = $this->cn->executarQueryArray($this->query());
            $idEvento = $rs[1]['ID'];

            $this->AgendaBase = new mAgendaBase();
            $this->AgendaBase->setIdSetor($this->idSetorRep);
            $this->AgendaBase->setDataVisita($this->dataEvento);

            $medicos = $this->AgendaBase->listar();

            $this->Agenda->setIdEvento($idEvento);
            $this->Agenda->excluir();       

            foreach($medicos as $medico) {
                $this->Agenda->setCRM($medico->getCRM());
                $this->Agenda->setNome($medico->getNome());
                $this->Agenda->setOrdemVisita($medico->getOrdemVisita());
                $this->Agenda->setPeriodoVisita($medico->getPeriodoVisita());
                $this->Agenda->salvar();

            }

            if($this->endManha != '' && $this->endTarde != '') {
                $this->Endereco->setIdEvento($idEvento);
                $this->Endereco->setEnderecoManha($this->endManha);
                $this->Endereco->setEnderecoTarde($this->endTarde);
                $this->Endereco->salvar();
            }
            
        } else {
            return $validar;
        }
         
         
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO WHERE ID_EVENTO = ".$this->idEvento);
        $this->popular($rs[1]);
    }
    
    public function selecionarEvento($data='', $idSetor='') {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO WHERE ID_SETOR_GER = ".$idSetor." AND DATA_EVENTO = '".$data."'");
        $this->createArrObjects($rs, $this);
    }
    
    public function excluir() {
        $this->cn->executar("DELETE FROM PMA_EVENTO WHERE ID_EVENTO = ".$this->idEvento);
        $this->Agenda->setIdEvento($this->idEvento);
        $this->Agenda->excluir();
    }
    
    public function listar() {
        
    }
    
    public function listarEventoCiclo() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO WHERE ID_SETOR_GER = ".$this->idSetorGerente." AND ID_CICLO = '".$this->idCiclo."' ORDER BY DATA_EVENTO, DATA_CADASTRO DESC");
        return $this->createArrObjects($rs, $this);
    }
    
    public function listarEventos($idSetor, $data) {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO WHERE ID_SETOR_GER = ".$idSetor." AND DATA_EVENTO = '".$data."' ORDER BY DATA_CADASTRO DESC");
        return $this->createArrObjects($rs, $this);
    }
    
    public function justificar() {
        
        $validar = $this->validarJustificativa();
        
        if(count($validar) <= 0) {
            $query = utf8_decode("UPDATE PMA_EVENTO SET ID_JUSTIFICATIVA = ".$this->idJustificativa.", MOTIVO_JUSTIFICATIVA = '".$this->motivoJustificativa."', BLOQUEAR = ".$this->bloqueado.", ID_STATUS_EVENTO = 2, DATA_JUSTIFICATIVA = GETDATE() WHERE ID_EVENTO = ".$this->idEvento);
            $this->cn->executarQueryArray($query);
        } else {
            return $validar;
        }
    }
    
        
    public function existeEvento($dataEvento, $idSetor) {

        $rs = $this->cn->executarQueryArray("SELECT 
                                                ISNULL(SUM(CASE WHEN ID_JUSTIFICATIVA = 1 THEN 1 ELSE 0 END),0) JUSTIF,
                                                ISNULL(SUM(CASE WHEN ID_JUSTIFICATIVA = 0 THEN 1 ELSE 0 END),0) ABERTO,
                                                COUNT(*) AS TOTAL
                                        FROM PMA_EVENTO WHERE DATA_EVENTO = '".$dataEvento."' AND ID_SETOR_GER = ".$idSetor." AND BLOQUEAR = 0");
        return ($rs[1]['ABERTO'] == 0 && $rs[1]['TOTAL'] > 0) ? 1 : 0;
    }
    
    protected  function popular($row) {
        $this->idEvento             = $row['ID_EVENTO'];
        $this->idStatusEvento       = $row['ID_STATUS_EVENTO'];
        $this->idJustificativa      = $row['ID_JUSTIFICATIVA'];
        $this->idTipoEvento         = $row['ID_TIPO_EVENTO'];
        $this->idCiclo              = $row['ID_CICLO'];
        $this->idSetorGerente       = $row['ID_SETOR_GER'];
        $this->idColaboradorGerente = $row['ID_COLABORADOR_GER'];
        $this->idSetorRep           = $row['ID_SETOR_REP'];
        $this->idColaboradorRep     = $row['ID_COLABORADOR_REP'];
        $this->dataEvento           = $row['DATA_EVENTO'];
        $this->objetivo             = $row['OBJETIVO'];
        $this->cadNoCiclo           = $row['CAD_NO_CICLO'];
        $this->motivoJustificativa  = $row['MOTIVO_JUSTIFICATIVA'];
        $this->dataJustificativa    = $row['DATA_JUSTIFICATIVA'];
        $this->atualizarEndereco    = $row['ATUALIZAR_ENDERECO'];
        $this->dataCadastro         = $row['DATA_CADASTRO'];
        $this->ordem                = $row['ORDEM'];
        $this->bloqueado            = $row['BLOQUEADO'];
        
        $this->SetorGer->setIdSetor($this->idSetorGerente);
        $this->SetorRep->setIdSetor($this->idSetorRep);
        $this->SetorGer->selecionar();
        $this->SetorRep->selecionar();
        
        $this->ColabGer->setIdColaborador($this->idColaboradorGerente);
        $this->ColabRep->setIdColaborador($this->idColaboradorRep);
        $this->ColabGer->selecionar();
        $this->ColabRep->selecionar();
        
        $this->TipoEvento->setIdTipoEvento($this->idTipoEvento);
        $this->TipoEvento->selecionar();
        
        $this->Status->setIdStatusEvento($this->idStatusEvento);
        $this->Status->selecionar();
        
        $this->Just->setIdJustificativa($this->idJustificativa);
        $this->Just->selecionar();
        
        $this->Agenda->setIdEvento($this->idEvento);
        
        $this->Endereco->setIdEvento($this->idEvento);
        $this->Endereco->selecionar();
        
        $this->Ciclo->setIdCiclo($this->idCiclo);
        $this->Ciclo->selecionar();
    }
    
    private function validar() {
        $this->TipoEvento->setIdTipoEvento($this->idTipoEvento);
        $this->TipoEvento->selecionar();
        
        $erro = [];
        
        if($this->idTipoEvento == 0) {
            $erro[] = 'Selecione o Tipo de Evento.';
        }
        
        if($this->TipoEvento->getAcomp() == 1) {
            
            if($this->idSetorRep == 0) {
                $erro[] = 'Selecione o setor/colaborador a ser acompanhado.';
            }
            
            if($this->endManha == '') {
                $erro[] = "O campo Endereço Manha deve ser preenchido.";
            } 
            
            if($this->endTarde == '') {
                $erro[] = "O campo Endereço Tarde deve ser preenchido.";
            }             
            
        }
        
        if($this->objetivo == '') {
            $erro[] = "O campo Objetivo deve ser preenchido.";
        }
        
        return $erro;
    }
    
    private function validarJustificativa() {
        $erro = [];
        
        if($this->idJustificativa == 0) {
            $erro[] = "Selecione a justificativa.";
        }
        
        if($this->motivoJustificativa == '') {
            $erro[] = "Descreva o motivo da justificativa.";
        }
        
        return $erro;
    }
    
    private function query() {
        $query[0] = "INSERT INTO [PMA_EVENTO]
                        ([ID_JUSTIFICATIVA]
                        ,[ID_STATUS_EVENTO]
                        ,[ID_TIPO_EVENTO]
                        ,[ID_CICLO]
                        ,[ID_SETOR_GER]
                        ,[ID_SETOR_REP]
                        ,[ID_COLABORADOR_GER]
                        ,[ID_COLABORADOR_REP]
                        ,[DATA_EVENTO]
                        ,[OBJETIVO]
                        ,[CAD_NO_CICLO]
                        ,[ATUALIZAR_ENDERECO]
                        ,[DATA_CADASTRO]
                        ,[ORDEM])
                  VALUES
                        (".$this->idJustificativa."
                        ,".$this->idStatusEvento."
                        ,".$this->idTipoEvento."
                        ,".$this->idCiclo."
                        ,".$this->idSetorGerente."
                        ,".$this->idSetorRep."
                        ,".$this->idColaboradorGerente."
                        ,".$this->idColaboradorRep."
                        ,'".$this->dataEvento."'
                        ,'".$this->objetivo."'
                        ,".$this->cadNoCiclo."
                        ,".$this->atualizarEndereco."
                        ,GETDATE()
                        ,1)
                        
                    SELECT @@IDENTITY AS ID";
        
        $query[1] = "UPDATE [PMA_EVENTO]
                        SET [ID_JUSTIFICATIVA] = ".$this->idJustificativa."
                           ,[ID_STATUS_EVENTO] = ".$this->idStatusEvento."
                           ,[ID_TIPO_EVENTO] = ".$this->idTipoEvento."
                           ,[ID_CICLO] = ".$this->idCiclo."
                           ,[ID_SETOR_GER] = ".$this->idSetorGerente."
                           ,[ID_SETOR_REP] = ".$this->idSetorRep."
                           ,[ID_COLABORADOR_GER] = ".$this->idColaboradorGerente."
                           ,[ID_COLABORADOR_REP] = ".$this->idColaboradorRep."
                           ,[DATA_EVENTO] = '".$this->dataEvento."'
                           ,[OBJETIVO] = '".$this->objetivo."'
                           ,[ATUALIZAR_ENDERECO] = ".$this->atualizarEndereco."
                       WHERE [ID_EVENTO] = ".$this->idEvento.""
                . " SELECT ".$this->idEvento." AS ID";
        
        return ($this->idEvento == 0) ? utf8_decode($query[0]) : utf8_decode($query[1]);
    }
    
}

