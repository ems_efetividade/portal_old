<?php

class mEventoDados extends Model {
    private $idEvento;
    private $idTipoEvento;
    private $idStatusEvento;
    private $idJustificativa;
    private $idSetorRep;
    private $idColabRep;
    private $objetivo;
    private $dataJustificativa;
    private $motivoJustificativa;
    private $cadNoCiclo;
    private $atualizarEndereco;
    private $dataCadastro;
    private $ordem;
    
    public $Status;
    public $Tipo;
    public $Justificativa;
    public $Setor;
    public $Colab;
    
    public function __construct() {
        parent::__construct();
        
        $this->idEvento            = 0;
        $this->idTipoEvento        = 0;
        $this->idStatusEvento      = 1;
        $this->idJustificativa     = 0;
        $this->idSetorRep          = 0;
        $this->idColabRep          = 0;
        $this->objetivo            = '';
        $this->dataJustificativa   = '';
        $this->motivoJustificativa = '';
        $this->cadNoCiclo          = 0;
        $this->atualizarEndereco   = 0;
        $this->dataCadastro        = '';
        $this->ordem               = 0;
        
        $this->Status        = new mEventoStatus();
        $this->Tipo          = new mTipoEvento();
        $this->Justificativa = new mJustificativa();
        $this->Setor         = new mSetor();
        $this->Colab         = new mColaborador();
    }
    


    
    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function setIdTipoEvento($idTipoEvento) {
        $this->idTipoEvento = $idTipoEvento;
    }

    public function setIdStatusEvento($idStatusEvento) {
        $this->idStatusEvento = $idStatusEvento;
    }

    public function setIdJustificativa($idJustificativa) {
        $this->idJustificativa = $idJustificativa;
    }

    public function setIdSetorRep($idSetorRep) {
        $this->idSetorRep = $idSetorRep;
    }

    public function setIdColabRep($idColabRep) {
        $this->idColabRep = $idColabRep;
    }

    public function setObjetivo($objetivo) {
        $this->objetivo = $objetivo;
    }

    public function setDataJustificativa($dataJustificativa) {
        $this->dataJustificativa = $dataJustificativa;
    }

    public function setMotivoJustificativa($motivoJustificativa) {
        $this->motivoJustificativa = $motivoJustificativa;
    }

    public function setCadNoCiclo($cadNoCiclo) {
        $this->cadNoCiclo = $cadNoCiclo;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }
    

    public function setAtualizarEndereco($atualizarEndereco) {
        $this->atualizarEndereco = $atualizarEndereco;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }
    
    public function getIdEvento() {
        return $this->idEvento;
    }

    public function getIdTipoEvento() {
        return $this->idTipoEvento;
    }

    public function getIdStatusEvento() {
        return $this->idStatusEvento;
    }

    public function getIdJustificativa() {
        return $this->idJustificativa;
    }

    public function getIdSetorRep() {
        return $this->idSetorRep;
    }

    public function getIdColabRep() {
        return $this->idColabRep;
    }

    public function getObjetivo() {
        return $this->objetivo;
    }

    public function getDataJustificativa() {
        return $this->dataJustificativa;
    }

    public function getMotivoJustificativa() {
        return $this->motivoJustificativa;
    }

    public function getCadNoCiclo() {
        return $this->cadNoCiclo;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }
    
    public function getAtualizarEndereco() {
        return $this->atualizarEndereco;
    }

    public function getOrdem() {
        return $this->ordem;
    }    

    public function salvar() {
        $this->cn->executarQueryArray($this->query());
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO_DADOS WHERE ID_EVENTO = ".$this->idEvento." ORDER BY DATA_CADASTRO DESC");
        return $this->createArrObjects($rs, $this);
    }
        
    protected function popular($row) {
        $this->idEvento            = $row['ID_EVENTO'];
        $this->idTipoEvento        = $row['ID_TIPO_EVENTO'];
        $this->idStatusEvento      = $row['ID_STATUS_EVENTO'];
        $this->idJustificativa     = $row['ID_JUSTIFICATIVA'];
        $this->idSetorRep          = $row['ID_SETOR_REP'];
        $this->idColabRep          = $row['ID_COLAB_REP'];
        $this->objetivo            = $row['OBJETIVO'];
        $this->dataJustificativa   = $row['DATA_JUSTIFICATIVA'];
        $this->motivoJustificativa = $row['MOTIVO_JUSTIFICATIVA'];
        $this->cadNoCiclo          = $row['CAD_NO_CICLO'];
        $this->atualizarEndereco   = $row['ATUALIZAR_ENDERECO'];
        $this->dataCadastro        = $row['DATA_CADASTRO'];
        $this->ordem               = $row['ORDEM'];
        
        $this->Status->setIdStatusEvento($this->idStatusEvento);
        $this->Status->selecionar();
        
        $this->Tipo->setIdTipoEvento($this->idTipoEvento);
        $this->Tipo->selecionar();
        
        $this->Justificativa->setIdJustificativa($this->idJustificativa);
        $this->Justificativa->selecionar();
        
        $this->Setor->setIdSetor($this->idSetorRep);
        $this->Setor->selecionar();
        
        $this->Colab->setIdColaborador($this->idColabRep);
        $this->Colab->selecionar();
    }
    
    private function query() {
        $query[0] = "INSERT INTO [PMA_EVENTO_DADOS]
                            ([ID_EVENTO]
                            ,[ID_TIPO_EVENTO]
                            ,[ID_STATUS_EVENTO]
                            ,[ID_JUSTIFICATIVA]
                            ,[ID_SETOR_REP]
                            ,[ID_COLAB_REP]
                            ,[OBJETIVO]
                            ,[DATA_JUSTIFICATIVA]
                            ,[MOTIVO_JUSTIFICATIVA]
                            ,[CAD_NO_CICLO]
                            ,[DATA_CADASTRO]
                            ,[ATUALIZAR_ENDERECO]
                            ,[ORDEM])
                      VALUES
                            (".$this->idEvento."
                            ,".$this->idTipoEvento."
                            ,".$this->idStatusEvento."
                            ,".$this->idJustificativa."
                            ,".$this->idSetorRep."
                            ,".$this->idColabRep."
                            ,'".$this->objetivo."'
                            ,'".$this->dataJustificativa."'
                            ,'".$this->motivoJustificativa."'
                            ,".$this->cadNoCiclo."
                            ,GETDATE()
                            ,".$this->atualizarEndereco."
                            ,".$this->ordem.")";
        
        return $query[0];
        
    }
}