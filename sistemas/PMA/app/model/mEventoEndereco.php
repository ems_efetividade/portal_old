<?php

class mEventoEndereco extends Model {
    
    private $idEvento;
    private $enderecoManha;
    private $enderecoTarde;
    private $ativo;
    private $dataCadastro;
    
    
    public function __construct() {
        parent::__construct();
        
        $this->idEvento = 0;
        $this->enderecoManha = '';
        $this->enderecoTarde = '';
        $this->ativo = 0;
        $this->dataCadastro = '';
    }
    
    public function getIdEvento() {
        return $this->idEvento;
    }

    public function getEnderecoManha() {
        return $this->enderecoManha;
    }

    public function getEnderecoTarde() {
        return $this->enderecoTarde;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function getDataCadastro() {
        return $this->dataCadastro;
    }

    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function setEnderecoManha($enderecoManha) {
        $this->enderecoManha = $enderecoManha;
    }

    public function setEnderecoTarde($enderecoTarde) {
        $this->enderecoTarde = $enderecoTarde;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

    public function setDataCadastro($dataCadastro) {
        $this->dataCadastro = $dataCadastro;
    }

        
    public function salvar() {
        $this->cn->executar($this->query());
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM [PMA_EVENTO_ENDERECO] WHERE ID_EVENTO = ".$this->idEvento." AND ATIVO = 1");
        $this->popular($rs[1]);
    }   
    
    public function excluir() {
        
    }
    
    public function listar() {
        
    }
    
    protected function popular($row) {
        $this->idEvento      = $row['ID_EVENTO'];
        $this->enderecoManha = $row['ENDERECO_MANHA'];
        $this->enderecoTarde = $row['ENDERECO_TARDE'];
        $this->ativo         = $row['ATIVO'];
        $this->dataCadastro  = $row['DATA_CADASTRO'];
    }
    
    private function query() {
        $q[0] = "   UPDATE [PMA_EVENTO_ENDERECO] SET ATIVO = 0 WHERE ID_EVENTO = ".$this->idEvento."
                    
                    INSERT INTO [PMA_EVENTO_ENDERECO]
                        ([ID_EVENTO]
                        ,[ENDERECO_MANHA]
                        ,[ENDERECO_TARDE]
                        ,[DATA_CADASTRO]
                        ,[ATIVO])
                  VALUES
                        (".$this->idEvento."
                        ,'".$this->enderecoManha."'
                        ,'".$this->enderecoTarde."'
                        ,GETDATE()
                        ,1)";
        
        return utf8_decode($q[0]);
    }
}
