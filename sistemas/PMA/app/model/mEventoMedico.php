<?php

class mEventoMedico extends Model {
    
    private $idEvento;
    private $CRM;
    private $nome;
    private $endereco;
    private $numero;
    private $bairro;
    private $cidade;
    private $periodoVisita;
    private $ordemVisita;
    
    public function __construct() {
        parent::__construct();
        
        $this->idEvento = 0;
        $this->CRM = '';
        $this->nome = '';
        $this->endereco = '';
        $this->numero = '';
        $this->bairro = '';
        $this->cidade = '';
        $this->periodoVisita = '';
        $this->ordemVisita = '';
    }
    
    public function getIdEvento() {
        return $this->idEvento;
    }

    public function getCRM() {
        return $this->CRM;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getPeriodoVisita() {
        return $this->periodoVisita;
    }

    public function getOrdemVisita() {
        return $this->ordemVisita;
    }

    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function setCRM($CRM) {
        $this->CRM = $CRM;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setPeriodoVisita($periodoVisita) {
        $this->periodoVisita = $periodoVisita;
    }

    public function setOrdemVisita($ordemVisita) {
        $this->ordemVisita = $ordemVisita;
    }

        
    public function salvar() {
        
    }
    
    public function selecionar() {
        
    }
    
    public function excluir() {
        
    }
    
    public function listar() {
        
    }
    
    protected function popular($row) {
        $this->idEvento = $row['ID_EVENTO'];
        $this->CRM = $row['CRM'];
        $this->nome = $row['NOME'];
        $this->endereco = $row['ENDERECO'];
        $this->numero = $row['NUMERO'];
        $this->bairro = $row['BAIRRO'];
        $this->cidade = $row['CIDADE'];
        $this->periodoVisita = $row['PERIODO_VISITA'];
        $this->ordemVisita = $row['ORDEM_VISITA'];
    }
    
}

