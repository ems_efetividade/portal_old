<?php

class mEventoStatus extends Model {
    
    private $idStatusEvento;
    private $descricao;
    private $icone;
    private $cor;
    
    public function __construct() {
        parent::__construct();
        
        $this->idStatusEvento = 0;
        $this->descricao = '';
        $this->icone = '';
        $this->cor = '';
    }
    
    public function getCor() {
        return $this->cor;
    }

    public function setCor($cor) {
        $this->cor = $cor;
    }

        public function getIcone() {
        return $this->icone;
    }

    public function setIcone($icone) {
        $this->icone = $icone;
    }

        
    public function getIdStatusEvento() {
        return $this->idStatusEvento;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function setIdStatusEvento($idStatusEvento) {
        $this->idStatusEvento = $idStatusEvento;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function salvar() {
        
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO_STATUS WHERE ID_STATUS_EVENTO = ".$this->idStatusEvento);
        $this->popular($rs[1]);
    }
    
    public function excluir() {
        
    }
    
    public function listar() {
        
    }
    
    protected  function popular($row) {
        $this->idStatusEvento = $row['ID_STATUS_EVENTO'];
        $this->descricao      = $row['DESCRICAO'];
        $this->icone          = $row['ICONE'];
        $this->cor = $row['COR'];
    }
    
}
