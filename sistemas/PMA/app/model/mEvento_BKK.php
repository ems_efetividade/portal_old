<?php

class mEvento extends Model {
    private $idEvento;
    private $idCiclo;
    private $idSetorGer;
    private $idColabGer;
    private $dataEvento;
    
    public $Dados;
    
    public function __construct() {
        parent::__construct();
        
        $this->idEvento   = 0;
        $this->idCiclo    = 0;
        $this->idSetorGer = 0;
        $this->idColabGer = 0;
        $this->dataEvento = '';
        
        $this->Dados = new mEventoDados();
    }
    
    public function getIdEvento() {
        return $this->idEvento;
    }

    public function getIdCiclo() {
        return $this->idCiclo;
    }

    public function getIdSetorGer() {
        return $this->idSetorGer;
    }

    public function getIdColabGer() {
        return $this->idColabGer;
    }

    public function getDataEvento() {
        return $this->dataEvento;
    }

    public function setIdEvento($idEvento) {
        $this->idEvento = $idEvento;
    }

    public function setIdCiclo($idCiclo) {
        $this->idCiclo = $idCiclo;
    }

    public function setIdSetorGer($idSetorGer) {
        $this->idSetorGer = $idSetorGer;
    }

    public function setIdColabGer($idColabGer) {
        $this->idColabGer = $idColabGer;
    }

    public function setDataEvento($dataEvento) {
        $this->dataEvento = $dataEvento;
    }
    
    public function salvar() {
        
        $rs = $this->cn->executarQueryArray("SELECT COUNT(*) FROM PMA_EVENTO WHERE ID_s");
        
        $rs = $this->cn->executarQueryArray($this->query());
        return $rs[1]['ID'];
    }

    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_EVENTO WHERE ID_EVENTO = ".$this->idEvento);
        $this->popular($rs[1]);
    }
    
    protected function popular($row) {
        $this->idEvento   = $row['ID_EVENTO'];
        $this->idCiclo    = $row['ID_CICLO'];
        $this->idSetorGer = $row['ID_SETOR_GER'];
        $this->idColabGer = $row['ID_COLABORADOR_GER'];
        $this->dataEvento = $row['DATA_EVENTO'];
        
        $this->Dados->setIdEvento($this->idEvento);
    }
    
    private function query() {
        $query[0] = "INSERT INTO [PMA_EVENTO]
                            ([ID_CICLO]
                            ,[ID_SETOR_GER]
                            ,[ID_COLABORADOR_GER]
                            ,[DATA_EVENTO])
                      VALUES
                            (".$this->idCiclo."
                            ,".$this->idSetorGer."
                            ,".$this->idColabGer."
                            ,'".$this->dataEvento."')"
                . " SELECT @@IDENTITY AS ID";
        
        $query[1] = "UPDATE [PMA_EVENTO]
                        SET [ID_CICLO] = ".$this->idCiclo."
                           ,[ID_SETOR_GER] = ".$this->idSetorGer."
                           ,[ID_COLABORADOR_GER] = ".$this->idColabGer."
                           ,[DATA_EVENTO] = '".$this->dataEvento."'
                      WHERE [ID_EVENTO] = ".$this->idEvento."";
        
        return ($this->idEvento == 0) ? $query[0] : $query[1];
    }
    
    
}
