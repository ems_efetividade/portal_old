<?php

class mJustificativa extends Model {
    
    private $idJustificativa;
    private $descricao;
    private $ativo;
    
    public function __construct() {
        parent::__construct();
        
        $this->idJustificativa = 0;
        $this->descricao = '';
        $this->ativo = 0;
    }
    
    public function getIdJustificativa() {
        return $this->idJustificativa;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getAtivo() {
        return $this->ativo;
    }

    public function setIdJustificativa($idJustificativa) {
        $this->idJustificativa = $idJustificativa;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setAtivo($ativo) {
        $this->ativo = $ativo;
    }

        
    public function salvar() {
        
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_JUSTIFICATIVA WHERE ID_JUSTIFICATIVA = ".$this->idJustificativa);
        $this->popular($rs[1]);
    }
    
    public function excluir() {
        
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_JUSTIFICATIVA WHERE ATIVO = 1 ORDER BY DESCRICAO ASC");
        return $this->createArrObjects($rs, $this);
    }
    
    protected function popular($row) {
        $this->idJustificativa = $row['ID_JUSTIFICATIVA'];
        $this->descricao       = $row['DESCRICAO'];
        $this->ativo           = $row['ATIVO'];
    }
    
}
