<?php


class mLinha extends Model {
    
    private $idLinha;
    private $nome;
    private $sigla;
    private $codigo;
    
    public function __construct() {
        parent::__construct();
        $this->idLinha = 0;
        $this->nome = '';
        $this->sigla = '';
        $this->codigo = '';
    }
    
    public function setIdLinha($idLinha) {
        $this->idLinha = $idLinha;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setSigla($sigla) {
        $this->sigla = $sigla;
    }

    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function getIdLinha() {
        return $this->idLinha;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getSigla() {
        return $this->sigla;
    }

    public function getCodigo() {
        return $this->codigo;
    }


    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM LINHA WHERE ID_LINHA = ".$this->idLinha);
        $this->popular($rs[1]);
    }
    
    protected function popular($row) {
        $this->setIdLinha($row['ID_LINHA']);
        $this->setNome($row['NOME']);
        $this->setSigla($row['SIGLA']);
        $this->setCodigo($row['CODIGO']);
    }
}

