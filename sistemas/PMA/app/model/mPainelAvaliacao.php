<?php

class mPainelAvaliacao extends Model {
    
    private $pilar;
    private $pontuacao;
    private $media;
    private $mes;
    private $ano;
    private $eixo;

    private $m1;
    private $m2;
    private $m3;
    private $m4;
    private $m5;
    private $m6;
    private $m7;
    private $m8;
    private $m9;
    private $m10;
    private $m11;
    private $m12;
    private $m13;
    
    private $r1;
    private $r2;
    private $r3;
    private $r4;
    private $r5;
    private $r6;
    private $r7;
    private $r8;
    private $r9;
    private $r10;
    private $r11;
    private $r12;
    private $r13;
    
    private $arrMes;


    
    public function __construct() {
        parent::__construct();
        $this->pilar = '';
        $this->pontuacao = '';
        $this->media = 0;
        $this->mes = 0;
        $this->ano = 0;
        $this->arrMes = '';
        $this->eixo = 0;
        
        $this->m1 = 0;
        $this->m2 = 0;
        $this->m3 = 0;
        $this->m4 = 0;
        $this->m5 = 0;
        $this->m6 = 0;
        $this->m7 = 0;
        $this->m8 = 0;
        $this->m9 = 0;
        $this->m10 = 0;
        $this->m11 = 0;
        $this->m12 = 0;
        $this->m13 = 0;
        
        $this->r1 = 0;
        $this->r2 = 0;
        $this->r3 = 0;
        $this->r4 = 0;
        $this->r5 = 0;
        $this->r6 = 0;
        $this->r7 = 0;
        $this->r8 = 0;
        $this->r9 = 0;
        $this->r10 = 0;
        $this->r11 = 0;
        $this->r12 = 0;
        $this->r13 = 0;
    }
    
    public function setPilar($pilar) {
        $this->pilar = $pilar;
    }

    public function setPontuacao($pontuacao) {
        $this->pontuacao = $pontuacao;
    }

    public function setMedia($media) {
        $this->media = $media;
    }

    public function setMes($mes) {
        $this->mes = $mes;
    }

    public function setAno($ano) {
        $this->ano = $ano;
    }
    
    public function setEixo($eixo) {
        $this->eixo = $eixo;
    }

    public function setM1($m1) {
        $this->m1 = $m1;
    }

    public function setM2($m2) {
        $this->m2 = $m2;
    }

    public function setM3($m3) {
        $this->m3 = $m3;
    }

    public function setM4($m4) {
        $this->m4 = $m4;
    }

    public function setM5($m5) {
        $this->m5 = $m5;
    }

    public function setM6($m6) {
        $this->m6 = $m6;
    }

    public function setM7($m7) {
        $this->m7 = $m7;
    }

    public function setM8($m8) {
        $this->m8 = $m8;
    }

    public function setM9($m9) {
        $this->m9 = $m9;
    }

    public function setM10($m10) {
        $this->m10 = $m10;
    }

    public function setM11($m11) {
        $this->m11 = $m11;
    }

    public function setM12($m12) {
        $this->m12 = $m12;
    }

    public function setM13($m13) {
        $this->m13 = $m13;
    }

    public function setR1($r1) {
        $this->r1 = $r1;
    }

    public function setR2($r2) {
        $this->r2 = $r2;
    }

    public function setR3($r3) {
        $this->r3 = $r3;
    }

    public function setR4($r4) {
        $this->r4 = $r4;
    }

    public function setR5($r5) {
        $this->r5 = $r5;
    }

    public function setR6($r6) {
        $this->r6 = $r6;
    }

    public function setR7($r7) {
        $this->r7 = $r7;
    }

    public function setR8($r8) {
        $this->r8 = $r8;
    }

    public function setR9($r9) {
        $this->r9 = $r9;
    }

    public function setR10($r10) {
        $this->r10 = $r10;
    }

    public function setR11($r11) {
        $this->r11 = $r11;
    }

    public function setR12($r12) {
        $this->r12 = $r12;
    }

    public function setR13($r13) {
        $this->r13 = $r13;
    }

    public function getPilar() {
        return $this->pilar;
    }

    public function getPontuacao() {
        return $this->pontuacao;
    }

    public function getMedia() {
        return $this->media;
    }

    public function getMes() {
        return $this->mes;
    }

    public function getAno() {
        return $this->ano;
    }
    
    public function getEixo() {
        return $this->eixo;
    }

    
    public function getM1() {
        return $this->m1;
    }

    public function getM2() {
        return $this->m2;
    }

    public function getM3() {
        return $this->m3;
    }

    public function getM4() {
        return $this->m4;
    }

    public function getM5() {
        return $this->m5;
    }

    public function getM6() {
        return $this->m6;
    }

    public function getM7() {
        return $this->m7;
    }

    public function getM8() {
        return $this->m8;
    }

    public function getM9() {
        return $this->m9;
    }

    public function getM10() {
        return $this->m10;
    }

    public function getM11() {
        return $this->m11;
    }

    public function getM12() {
        return $this->m12;
    }

    public function getM13() {
        return $this->m13;
    }

    public function getR1() {
        return $this->r1;
    }

    public function getR2() {
        return $this->r2;
    }

    public function getR3() {
        return $this->r3;
    }

    public function getR4() {
        return $this->r4;
    }

    public function getR5() {
        return $this->r5;
    }

    public function getR6() {
        return $this->r6;
    }

    public function getR7() {
        return $this->r7;
    }

    public function getR8() {
        return $this->r8;
    }

    public function getR9() {
        return $this->r9;
    }

    public function getR10() {
        return $this->r10;
    }

    public function getR11() {
        return $this->r11;
    }

    public function getR12() {
        return $this->r12;
    }

    public function getR13() {
        return $this->r13;
    }

    public function getDadosPilares($idAvaliacao=0, $setor='') {
        $rs = $this->cn->executarQueryArray("EXEC proc_pma_avaliacaoDadosPilares '".$idAvaliacao."', '".$setor."'");
        return $this->createArrObjects($rs, $this);
    }
    
    public function getMeses() {
        return $this->arrMes;
    }

    protected function popular($row) {
        $this->pilar     = $row['PILAR'];
        $this->pontuacao = $row['PONTUACAO'];
        $this->media     = $row['MEDIA'];
        $this->mes       = $row['MES'];
        $this->ano       = $row['ANO'];
        $this->eixo      = $row['EIXO'];
        
        $this->m1  = $row['M1'];
        $this->m2  = $row['M2'];
        $this->m3  = $row['M3'];
        $this->m4  = $row['M4'];
        $this->m5  = $row['M5'];
        $this->m6  = $row['M6'];
        $this->m7  = $row['M7'];
        $this->m8  = $row['M8'];
        $this->m9  = $row['M9'];
        $this->m10 = $row['M10'];
        $this->m11 = $row['M11'];
        $this->m12 = $row['M12'];
        $this->m13 = $row['M13'];
        
        $this->r1  = $row['R1'];
        $this->r2  = $row['R2'];
        $this->r3  = $row['R3'];
        $this->r4  = $row['R4'];
        $this->r5  = $row['R5'];
        $this->r6  = $row['R6'];
        $this->r7  = $row['R7'];
        $this->r8  = $row['R8'];
        $this->r9  = $row['R9'];
        $this->r10 = $row['R10'];
        $this->r11 = $row['R11'];
        $this->r12 = $row['R12'];
        $this->r13 = $row['R13'];
        
        
        $m = $this->mes;
        $a = $this->ano;
        
        $mes[1]  = "Janeiro";
        $mes[2]  = "Fevereiro";
        $mes[3]  = "Março";
        $mes[4]  = "Abril";
        $mes[5]  = "Maio";
        $mes[6]  = "Junho";
        $mes[7]  = "Julho";
        $mes[8]  = "Agosto";
        $mes[9]  = "Setembro";
        $mes[10] = "Outubro";
        $mes[11] = "Novembro";
        $mes[12] = "Dezembro";
        
        $this->arrMes = [];
        for($i=1;$i<=13;$i++) {
            $this->arrMes['M'.$i] = substr($mes[$m], 0, 3).'/'.($a-2000);
            $m--;
            if($m==0) {
                $m = 12;
                $a--;
            }
        }
       
    }
    
}