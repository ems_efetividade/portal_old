<?php

class mSetor extends Model {
    
    private $idSetor;
    private $idPerfil;
    private $idLinha;
    private $nivel;
    private $setor;
    private $senha;
    private $nome;
    
    private $linha;
    private $perfil;
   
    
    
    
    public function __construct() {
        parent::__construct();
        
        $this->idSetor  = 0;
        $this->idLinha  = 0;
        $this->idPerfil = 0;
        $this->nivel    = 0;
        $this->senha    = '';
        $this->setor    = '';
        $this->nome     = '';
        
        $this->linha = '';
        $this->perfil = '';
      
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdPerfil($valor) {
        $this->idPerfil = $valor;
    }
    
    public function setIdLinha($valor) {
        $this->idLinha = $valor;
    }
    
    public function setNivel($valor) {
        $this->nivel = $valor;
    }
    
    public function setSenha($valor) {
        $this->senha = $valor;
    }
    
    public function setSetor($valor) {
        $this->setor = $valor;
    }
    
    public function setNome($valor) {
        $this->nome = $valor;
    }
    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdPerfil() {
        return $this->idPerfil;
    }
    
    public function getIdLinha() {
        return $this->idLinha;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getSenha() {
        return $this->senha;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getNome() {
        return $this->nome;
    }
    
    public function getLinha() {
        return $this->linha;
    }

    public function getPerfil() {
        return $this->perfil;
    }

        
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT
                                                    S.ID_SETOR,
                                                    L.ID_LINHA,
                                                    L.ID_PERFIL,
                                                    S.NIVEL,
                                                    S.SENHA,
                                                    S.SETOR,
                                                    S.NOME,
                                                    L.LINHA,
                                                    L.PERFIL
                                                FROM vw_colaboradorSetor L
                                                INNER JOIN SETOR S ON S.ID_SETOR = L.ID_SETOR
                                                WHERE S.ID_SETOR = ".$this->idSetor);
        
        $this->popular($rs[1]);
    }

    public function selecionarPorSetor() {
        $rs = $this->cn->executarQueryArray("SELECT
                                                    S.ID_SETOR,
                                                    L.ID_LINHA,
                                                    L.ID_PERFIL,
                                                    S.NIVEL,
                                                    S.SENHA,
                                                    S.SETOR,
                                                    S.NOME,
                                                    L.LINHA,
                                                    L.PERFIL
                                                FROM vw_colaboradorSetor L
                                                INNER JOIN SETOR S ON S.ID_SETOR = L.ID_SETOR
                                                WHERE S.SETOR = ".$this->setor);
        
        $this->popular($rs[1]);
    }
    
    public function selecionarEquipe() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM SETOR WHERE NIVEL = ".$this->idSetor);
        return $this->createArrObjects($rs, $this);
    }
    
    protected function popular($row) {
        $this->idSetor  = $row['ID_SETOR'];
        $this->idLinha  = $row['ID_LINHA'];
        $this->idPerfil = $row['ID_PERFIL'];
        $this->nivel    = $row['NIVEL'];
        $this->senha    = $row['SENHA'];
        $this->setor    = $row['SETOR'];
        $this->nome     = $row['NOME'];
        $this->linha    = $row['LINHA'];
        $this->perfil   = $row['PERFIL'];
       
    }

}