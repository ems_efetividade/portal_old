<?php

class mTipoEvento extends Model {
    
    private $idTipoEvento;
    private $descricao;
    private $acomp;
    
    public function __construct() {
        parent::__construct();
        
        $this->idTipoEvento = 0;
        $this->descricao = '';
        $this->acomp = 0;
    }
    
    public function getIdTipoEvento() {
        return $this->idTipoEvento;
    }

    public function getDescricao() {
        return $this->descricao;
    }

    public function getAcomp() {
        return $this->acomp;
    }

    public function setIdTipoEvento($idTipoEvento) {
        $this->idTipoEvento = $idTipoEvento;
    }

    public function setDescricao($descricao) {
        $this->descricao = $descricao;
    }

    public function setAcomp($acomp) {
        $this->acomp = $acomp;
    }

        
    public function salvar() {
        
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_TIPO_EVENTO WHERE ID_TIPO_EVENTO = ".$this->idTipoEvento);
        $this->popular($rs[1]);
    }
    
    public function excluir() {
        
    }
    
    public function listar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_TIPO_EVENTO ORDER BY DESCRICAO ASC");
        return $this->createArrObjects($rs, $this);
    }
    
    protected  function popular($row) {
        $this->idTipoEvento = $row['ID_TIPO_EVENTO'];
        $this->descricao    = $row['DESCRICAO'];
        $this->acomp        = $row['ACOMP'];
    }
    
    
    
}