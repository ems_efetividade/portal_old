<?php


class mVisitacao extends Model {
    private $idSetor;
    private $CRM;
    private $nome;
    private $esp;
    private $dataVisita;
    private $motivoVisita;
    private $acompVisita;
    private $dataRevisita;
    private $motivoRevisita;
    private $acompRevisita;
    private $endereco;
    private $numero;
    private $complemento;
    private $bairro;
    private $cep;
    private $cidade;
    private $UF;
    
    public function __construct() {
        parent::__construct();
        
        $this->idSetor = '';
        $this->CRM = '';
        $this->nome = '';
        $this->esp = '';
        $this->dataVisita = '';
        $this->motivoVisita = '';
        $this->acompVisita = '';
        $this->dataRevisita = '';
        $this->motivoRevisita = '';
        $this->acompRevisita = '';
        $this->endereco = '';
        $this->numero = '';
        $this->complemento = '';
        $this->bairro = '';
        $this->cep = '';
        $this->cidade = '';
        $this->UF = '';
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }

    public function getCRM() {
        return $this->CRM;
    }

    public function getNome() {
        return $this->nome;
    }

    public function getEsp() {
        return $this->esp;
    }

    public function getDataVisita() {
        return $this->dataVisita;
    }

    public function getMotivoVisita() {
        return $this->motivoVisita;
    }

    public function getAcompVisita() {
        return $this->acompVisita;
    }

    public function getDataRevisita() {
        return $this->dataRevisita;
    }

    public function getMotivoRevisita() {
        return $this->motivoRevisita;
    }

    public function getAcompRevisita() {
        return $this->acompRevisita;
    }

    public function getEndereco() {
        return $this->endereco;
    }

    public function getNumero() {
        return $this->numero;
    }

    public function getComplemento() {
        return $this->complemento;
    }

    public function getBairro() {
        return $this->bairro;
    }

    public function getCep() {
        return $this->cep;
    }

    public function getCidade() {
        return $this->cidade;
    }

    public function getUF() {
        return $this->UF;
    }

    public function setIdSetor($idSetor) {
        $this->idSetor = $idSetor;
    }

    public function setCRM($CRM) {
        $this->CRM = $CRM;
    }

    public function setNome($nome) {
        $this->nome = $nome;
    }

    public function setEsp($esp) {
        $this->esp = $esp;
    }

    public function setDataVisita($dataVisita) {
        $this->dataVisita = $dataVisita;
    }

    public function setMotivoVisita($motivoVisita) {
        $this->motivoVisita = $motivoVisita;
    }

    public function setAcompVisita($acompVisita) {
        $this->acompVisita = $acompVisita;
    }

    public function setDataRevisita($dataRevisita) {
        $this->dataRevisita = $dataRevisita;
    }

    public function setMotivoRevisita($motivoRevisita) {
        $this->motivoRevisita = $motivoRevisita;
    }

    public function setAcompRevisita($acompRevisita) {
        $this->acompRevisita = $acompRevisita;
    }

    public function setEndereco($endereco) {
        $this->endereco = $endereco;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
    }

    public function setComplemento($complemento) {
        $this->complemento = $complemento;
    }

    public function setBairro($bairro) {
        $this->bairro = $bairro;
    }

    public function setCep($cep) {
        $this->cep = $cep;
    }

    public function setCidade($cidade) {
        $this->cidade = $cidade;
    }

    public function setUF($UF) {
        $this->UF = $UF;
    }
    
    public function selecionar() {
        $rs = $this->cn->executarQueryArray("SELECT * FROM PMA_VISITACAO WHERE ID_SETOR =".$this->idSetor." AND CRM = '".$this->CRM."'");
        $this->popular($rs[1]);
    }

    
    protected function popular($row) {
        $this->idSetor        = $row['ID_SETOR'];
        $this->CRM            = $row['CRM'];
        $this->nome           = $row['NOME'];
        $this->esp            = $row['ESP'];
        $this->dataVisita     = $row['DATA_VISITA'];
        $this->motivoVisita   = $row['MOTIVO_VISITA'];
        $this->acompVisita    = $row['ACOMP_VISITA'];
        $this->dataRevisita   = $row['DATA_REVISITA'];
        $this->motivoRevisita = $row['MOTIVO_REVISITA'];
        $this->acompRevisita  = $row['ACOMP_REVISITA'];
        $this->endereco       = $row['ENDERECO'];
        $this->numero         = $row['NRO'];
        $this->complemento    = $row['COMPLEMENTO'];
        $this->bairro         = $row['BAIRRO'];
        $this->cep            = $row['CEP'];
        $this->cidade         = $row['CIDADE'];
        $this->UF             = $row['UF'];
    }

    


}
