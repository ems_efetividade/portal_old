<style>
    #tbMedico h5, #tbMedico p {
        margin:0px; margin-bottom:5px;  
    }
    #modalEvento td {
        white-sspace:nowrap;
    }
    #modalEvento .tab-pane {
        padding-top: 10px;
        height: 310px;
    }
    #modalEvento label {
        font-size: 13px;
    }
    #modalEvento .tab-pane {
        padding:20px;
        border-bottom: 1px solid #ddd;
        border-left: 1px solid #ddd;
        border-right: 1px solid #ddd;
    }
    #modalEvento .label {
        border-radius: 8px !important;
    }
</style>
<?php $medicos = $atividade->Agenda->listar();  ?>
<form id="formAtividade" method="POST" action="<?php echo APP_URL ?>atividade/salvar">
    <input type="hidden" name="txtIdAtividade" value="<?php echo $atividade->getIdAtividade() ?>"/>
    <input type="hidden" name="txtIdSetorGer"  value="<?php echo fnDadoSessao('id_setor'); ?>"/>
    <input type="hidden" name="txtIdColabGer"  value="<?php echo fnDadoSessao('id_colaborador'); ?>"/>
    <input type="hidden" name="txtIdSetorRep" class="habilitar" value="<?php echo $atividade->getIdSetorRep() ?>"/>
    <input type="hidden" name="txtIdColabRep" class="habilitar" value="<?php echo $atividade->getIdColaboradorRep() ?>"/>
    <input type="hidden" name="txtDataEvento"  value="<?php echo ($atividade->getDataEvento() == '') ? $dataEvento : $atividade->getDataEvento(); ?>"/>
    <div class="row">
        <div class="col-lg-8">
            <h4 style="margin-top:5px">
                <b><span class="glyphicon glyphicon-calendar"></span> <span id="diaExtenso"><?php echo fnDataExtenso($dataEvento) ?></span></b>
            </h4>
        </div>
    </div>
    <hr style="margin-bottom:10px;margin-top:10px">
    <div>
        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-justified" role="tablist">
            <li role="presentation" class="active">
                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                    <b><small><span class="glyphicon glyphicon-pencil"></span> Dados da Atividade</SMALL></b>
                </a>
            </li>
            <li role="presentation">
                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
                    <b><SMALL><span class="glyphicon glyphicon-user"></span> Agenda médica do colaborador (<span class="total-medico"><?php echo count($medicos); ?></span>)</small></b>
                </a>
            </li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="home">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group form-group-sm">
                                    <label for="exampleInputEmail1"><small>Qual o tipo da atividade?</small></label>
                                    <select class="form-control" name="cmbTipoEvento" placeholder="wow">
                                        <option value="0"></option>
                                        <?php
                                        foreach ($tipoAtividades as $i => $tipo) {
                                            
                                            ?>
                                            <option <?php echo (($tipo->getIdTipo() == $atividade->getIdTipo()) ? 'selected' : '') ?> value="<?php echo $tipo->getIdTipo() ?>" bloq="<?php echo $tipo->getAcomp() ?>"><?php echo $tipo->getDescricao() ?></option>
                                        <?php } ?>
                                    </select>
                                </div> 
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group form-group-sm">
                                    <label for="exampleInputEmail1"><small>Qual será o colaborador?</small></label>
                                    <input type="text" class="form-control habilitar" id="txtColaborador" placeholder="" autocomplete="off" name="txtColaborador" value="<?php echo $atividade->SetorRep->getSetor() . ' ' . $atividade->ColabRep->getNome() ?>">
                                    <div id="pre" style="position: absolute;z-index: 1000; width: 93%;display: none; max-height: 200px; overflow: auto">
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group form-group-sm">
                                    <label for="exampleInputEmail1"><small>Qual é o objetivo dessa atividade?</small></label>
                                    <textarea class="form-control" maxlength="2000" rows="6" name="txtObjetivo"><?php echo $atividade->getObjetivo() ?></textarea>
                                </div>          
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group form-group-sm">
                                            <label for="exampleInputEmail1"><small>Endereço Médico (Manhã/Tarde)</small></label>
                                            <p>
                                                <textarea disabled class="form-control" rows="2" id="endMedManha" name="txtEndManhaMed"><?php echo $atividade->Agenda->getPrimeiroEndereco('M') ?></textarea>
                                            </p>
                                            <textarea disabled class="form-control" rows="2" id="endMedTarde" name="txtEndTardeMed"><?php echo $atividade->Agenda->getPrimeiroEndereco('T') ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="form-group form-group-sm">
                                            <label for="exampleInputEmail1"><small>Endereço Encontro (Manhã/Tarde)</small></label>
                                            <p>
                                                <textarea class="form-control habilitar" rows="2" name="txtEndManha"><?php echo $atividade->getEndManha() ?></textarea>
                                            </p>
                                            <textarea class="form-control habilitar" rows="2" name="txtEndTarde"><?php echo $atividade->getEndTarde() ?></textarea>
                                        </div>      
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="profile">
                <div id="divMed" class="" style="max-height: 280px;overflow-y: auto; ">
                    <table id="tbMedico" class="table table-striped table-condensed">
                        <tbody>
                            <tr>
                                <th><small>Período</small></th>
                                <th><small>Médico</small></th>
                            </tr>
                            <?php foreach ($medicos as $medico) { ?>
                                <tr >
                                    <td class="text-center" style="vertical-align: middle;">
                                        <span class="label label-<?php echo ($medico->getPeriodoVisita() == 'M') ? 'primary' : 'warning' ?>"><?php echo $medico->getHorarioVisita(); ?></span> 
                                    </td>
                                    <td class="text-muted">
                                        <div><small><strong><?php echo $medico->getNome() . ' (' . $medico->getCRM() . ')' ?></strong></small></div>
                                        <small>
                                            <?php echo $medico->getEndereco() . ', ' . $medico->getNro() . ' - ' . $medico->getComplemento() . ' - ' . $medico->getBairro() . ' - CEP: ' . $medico->getCEP() . ', ' . $medico->getCidade() . '/' . $medico->getUF() ?>
                                        </small>
                                    </td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="settings">...</div>
        </div>
    </div>
</form>    
<script>
    $('#tbMedico a').click(function (e) {
        e.preventDefault();
    });
    $('select[name="cmbTipoEvento"]').change(function (e) {
        $('.habilitar').removeAttr('disabled');
        v = $('option:selected', this).attr('bloq');
        if (v == 0) {
            $('.habilitar').attr('disabled', 'disabled');
        }
    });
    $('#txtColaborador').keyup(function (e) {
        clearTimeout($(this).data('timer'));
        $('#pre').hide();
        criterio = $(this).val();
        if (criterio.length >= 2) {
            $(this).data('timer', setTimeout(function () {
                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>atividade/pesquisarRep',
                    data: {
                        txtCriterio: criterio
                    },
                    beforeSend: function (e) {
                    },
                    success: function (retorno) {
                        if (retorno == '') {
                            $('#pre').hide();
                        } else {
                            $('#pre .panel-body').html(retorno);
                            $('#pre').fadeIn(100);
                            $('#pre a').click(function (e) {
                                valor = $(this).attr('cod').split('|');
                                $('input[name="txtIdSetorRep"]').val(valor[0]);
                                $('input[name="txtIdColabRep"]').val(valor[1]);
                                $('#txtColaborador').val($(this).text());
                                $('#pre').hide();
                                gerarAgenda();
                            });
                        }
                    }
                });
            }, 300));
        }
    });
    function gerarAgenda() {
        idSetor = $('input[name="txtIdSetorRep"]').val();
        dataVisita = $('input[name="txtDataEvento"]').val();
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>agendaBase/listar/' + idSetor + '/' + dataVisita,
            beforeSend: function (e) {
            },
            success: function (retorno) {
                dados = $.parseJSON(retorno);

                $('#endMedManha').val(dados.end_manha);
                $('#endMedTarde').val(dados.end_tarde);
                //alert(dados.tabela);
                //$('#tbMedico tbody').html(dados.tabela);
                
                
                $("#tbMedico > tbody:last").html(dados.tabela);
                $('.total-medico').html(dados.total);
            }
        });
    }
</script>