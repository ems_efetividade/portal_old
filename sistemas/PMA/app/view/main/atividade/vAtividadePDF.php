<?php $imgFolder = str_replace('profile', 'img', EMS_FOTO_PROFILE); ?>
<head>
    <meta charset='utf-8'>
    <link href="<?php echo APP_URL ?>framework/libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo APP_URL ?>public/css/default.css" rel="stylesheet">
    <style>
         p { margin-bottom:4px;padding-bottom:4px }
        .body {font-size: 14px;}

        .bold { font-weight: bold; }
        .h4 {font-size:17px;}
        .td { padding-bottom: 4px; padding-top: 4px }
        .td-border {border-bottom: 1px solid #ddd; }
        .td-background { background-color: #f9f9f9 !important; }
        hr {margin:0px; border-top: 1px solid #eee}
        table {margin-top: 5px;}
        #tb-medico .td { padding-bottom: 8px; padding-top: 8px }
        .label {border:0px; padding:5px; border-radius: 10px;}
        .div-td {float:left; padding-bottom: 8px; padding-top: 8px}
        .div-tr { border-top: 1px solid #eee }
    </style>  
    
</head>
<htmlpageheader name="myHTMLHeader">  
<div>
    <table style="width:100%; padding:0px;">
        <tr>   
            <td width="13%" class="text-center">
                <img src="<?php echo getcwd() ?>\\..\\..\\public\img\iconsFerramentas\logoEfet.png" height="60"  />
                <p class="text-muted" style="margin:0px;padding:0px; font-size:7px;">E F E T I V I D A D E</p>    
            </td>
            <td width="87%" class="text-left">
                <h4 class="bold"><?php echo $nomeReport; ?></h4>
                <h5 class="text-muted"><small><?php echo APP_NAME ?></small></h5>
            </td>
        </tr>
    </table>
</div>
<div> 
    <div style="border-bottom: 1px solid #666; margin-top: 0px;margin-bottom: 40px;"></div>
</div>
</htmlpageheader>

<sethtmlpageheader name="myHTMLHeader"  value="on" show-this-page="1" />

<?php $dataAnt = ''; foreach($eventos as $evento) { ?>
<?php if($dataAnt != $evento->getDataEvento()) { ?>
<h5 class="well well-sm"><img src="public/img/calendar.png" style="margin-right:5px" height="15" /><span class="bold "><?php echo fnDataExtenso($evento->getDataEvento()) ?> </span></h5>
<?php } ?>
<table class="table">
    <tr>
        <td width="150" style="vertical-align: top">
            <?php if($evento->Tipo->getAcomp() == 1) { ?>
            <img style="border:0px solid #ddd;padding:3px;" class="center-block" height="150" src="<?php echo str_replace(EMS_URL.'/public/img/', (str_replace('profile', 'img', EMS_FOTO_PROFILE)), str_replace(EMS_URL.'/public/profile/', EMS_FOTO_PROFILE, fnFotoColaborador($evento->ColabRep->getFoto()))) ?>" />
            <?php } else { ?>
            <img src="public/img/calendar-512.png"  height="100" style="padding:15px;" class="img-responsive" />
            <?php } ?>
        </td>
        <td width="800px" style="vertical-align: top">
            <h4 class="bold"><b><?php echo ($evento->Tipo->getAcomp() == 1) ? $evento->SetorRep->getSetor().' '.$evento->ColabRep->getNome() : $evento->Tipo->getDescricao() ?></b></h4>
            
            
            <table class="table" style="font-size: 16px;">
                <?php if($evento->Tipo->getAcomp() == 1) { ?>
                
                <tr>
                    <td width="110" class="td text-left boald"><p>End. Manhã:</p></td>
                    <td class="td text-justify"><p class="text-muted"><?php echo ($evento->getEndManha() == '') ? $evento->Agenda->getPrimeiroEndereco('M') : $evento->getEndManha(); ?></p></td>
                </tr>
                <tr>
                    <td class="td boald"><p>End. Tarde:</p></td>
                    <td class="td text-justify"><p class="text-muted"><?php echo ($evento->getEndTarde() == '') ? $evento->Agenda->getPrimeiroEndereco('T') : $evento->getEndTarde(); ?></p></td>
                </tr>
                <tr>
                    <td width="110" class="td text-left boald"><p>Acomp. por:</p></td>
                    <td class="td text-justify"><p class="text-muted"><?php echo $evento->SetorGer->getSetor().' '.$evento->ColabGer->getNome(); ?></p></td>
                </tr>
                <?php } ?>
                <tr>
                    <td class="td bolad"><p>Objetivo:</p></td>
                    <td class="td text-justify"><p class="text-muted"><?php echo $evento->getObjetivo() ?></p></td>
                </tr>
                
                <tr>
                    <td class="td bold"><p>Status:</p></td>
                    <td class="td text-left">
                        <table>
                            <tr>
                                <td class="td">
                                    <div>
                                        <p class="<?php echo $evento->Status->getCor() ?>">
                                        <?php echo $evento->Status->getDescricao().' '.(($evento->TipoTroca->getDescricao() != "") ? '('.$evento->TipoTroca->getDescricao().')' : '') ?>
                                        </p> 
                                    </div> 
                                    
                                </td>
                            </tr>
                            <?php if($evento->getDataTroca() != "") { ?>
                            <tr>
                                <td class="td">
                                    <p><i><small class="text-muted">em <?php echo fnFormatarData($evento->getDataTroca()); ?></small></i></p>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td class="td">
                                    <small class="text-muted"><?php echo $evento->getMotivoTroca(); ?></small>
                                </td>
                            </tr>
                            
                        </table>
                           
                        
                        
                    </td>
                </tr>
            </table>
            
        </td>
        
    </tr>
</table>

    <?php $medicos = $evento->Agenda->listar();  ?>
    <?php if(count($medicos) > 0) {  ?>

    <p class="bold">Agenda Médica do dia</p>
    
    <?php foreach($medicos as $i => $medico) { $back = ((($i%2) == 1) ? 'td-background':'') ?>
    
    <div class="div-tr text-muted <?php echo $back ?>">
        <div class="div-td" style="width: 30px;padding:15px">
            <div style="" class="label label-<?php echo ($medico->getPeriodoVisita() == 'M') ? 'primary' : 'warning' ?>">
                <small>
                <?php echo $medico->getHorarioVisita(); ?>
                    </small>
            </div> 
        </div>
        <div class="div-td">
            <div class="bold"><small><?php echo $medico->getNome() . ' ('.$medico->getCRM().')' ?></small></div>
            <small><small>
                <?php echo $medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().' - '.$medico->getBairro(). ' - CEP: '.$medico->getCEP().', '.$medico->getCidade().'/'.$medico->getUF()  ?>
            </small></small>
        </div>
    </div>
    
    <?php } ?>
    
    
    

    <?php } ?>
    <br>
<?php 
    $dataAnt = $evento->getDataEvento();
    } ?>