
<style>
    #modalResumo .tab-pane {
        padding-top: 10px;
        height: 150px;
    }
    
    #modalResumo .modal-body {
        padding:0px;
    }
    
    #modalResumo .navbar a:hover {
        background-color: #ddd;
    }
    
    #modalResumo .navbar {
        margin-bottom: 0px;
    }
    
    #modalResumo .navbar {
        min-height: 0px;
        border-top: 0px;
    }
    
    #modalResumo .navbar a {
        padding-top: 10px;
        padding-bottom: 10px;
        border-right: 1px solid #ddd;
    } 
    
    #modalResumo .dropdown-menu a {
        padding-top: 5px;
        padding-bottom: 5px;
    }
    
    #modalResumo .navbar-collapse {
        padding-left: 0px;
    }
</style>


<nav class="navbar navbar-default">
  <div class="container-flsuid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    
   

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <?php if (fnDadoSessao('id_setor') == $atividade->getIdSetorGer()) { ?>  
          
            <!-- SE A DATA DO EVENTO ESTIVER NO CICLO OU A DATA DO EVENTO FOR MAIOR OU IGUAL AO DIA ATUAL, HABILITA APENAS O 'IMPRIMIR'  -->
          <?php if($atividade->Ciclo->getIdStatusCiclo() <> 1) { ?>  



              <!-- SE O CICLO ATUAL FOR DIFERENTE DO CICLO DA ATIVIDADE, DESABILITA 'EDITAR' E 'EXCLUIR'  -->
              <?php  if($atividade->Ciclo->getIdStatusCiclo() == 3) { //if($cicloAtual->getIdCiclo() != $atividade->getIdCiclo()) { ?>
              <li>
                  <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalEvento" data-data="<?php echo $atividade->getDataEvento() ?>" data-id="<?php echo $atividade->getIdAtividade() ?>"><small><span class="glyphicon glyphicon-pencil"></span> Editar</small></a>
              </li>

              <li>
                  <a href="#" data-dissmiss="modal" data-toggle="modal" data-target="#modalExcluir" data-id="<?php echo $atividade->getIdAtividade() ?>"><small><span class="glyphicon glyphicon-trash"></span> Excluir</small></a>
              </li>
              <?php } ?>

              <!-- SE O CICLO ATUAL FOR O MESMO DO CICLO DA ATIVIDADE, DESABILITA 'TROCAR'  -->
              <?php if(($atividade->Ciclo->getIdStatusCiclo() == 2) && $atividade->getIdStatus() != 2 && fnJustificar($atividade->getDataEvento()) == 1 && $totalAvaliacao == 0) { ?>
              <li>
                  <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalJustificar" data-id="<?php echo $atividade->getIdAtividade() ?>"><small><span class="glyphicon glyphicon-refresh"></span> Trocar</small></a>
              </li>
              <?php } ?>


              <?php if(($atividade->Ciclo->getIdStatusCiclo() == 2) && $totalAvaliacao == 0 && $atividade->getIdStatus() != 2 && $atividade->Tipo->getAcomp() == 1 && fnDadoSessao('perfil') == 3) { ?>
              <li>
                  <a href="<?php echo APP_URL ?>avaliacao/form/0/<?php echo $atividade->getIdColaboradorRep() ?>/<?php echo $atividade->getIdCiclo() ?>/<?php echo $atividade->getIdSetorRep() ?>" ><small><span class="glyphicon glyphicon-adjust"></span> Avaliar</small></a>
              </li>
              <?php } ?>

          <?php } ?>
            
        <?php } ?>
            
        <li>
            <a href="#"  id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"><small><span class="glyphicon glyphicon-print"></span> Imprimir<span class="caret"></span></small></a>
            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                <li><a href="#" id="btnImprimir"> <small><span class="glyphicon glyphicon-calendar"></span> Resumo do Evento</small></a></li>
                <?php if (fnDadoSessao('id_setor') == $atividade->getIdSetorGer() && fnDadoSessao('perfil') == 3) { ?>  
                <li><a href="#" id="btnFormRAC" data-setor="<?php echo $atividade->getIdAtividade(); ?>"><small><span class="glyphicon glyphicon-file"></span> Formulário RAC</small></a></li>
                <?php } ?>
              </ul>
        </li>
        
        
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>




<div style="padding:15px;max-height: 300px; overflow: auto">
    
    
    
    
    
    
<!--    ACOMPANHAMENTO DE CAMPO-->
<?php  if($atividade->Tipo->getAcomp() == 1) { ?>    

    
<div class="row">
    
    <div class="col-lg-3 col-sm-4 col-md-2 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:3px;">
                <img src="<?php echo  fnFotoColaborador($atividade->ColabRep->getFoto()) ?>" class="center-block img-responsive" />
            </div>
        </div>
    </div>
        
    <div class="col-lg-9 col-sm-8 col-md-10 col-xs-12">
        
        <h4 class="hidden-xs"><b><?php echo $atividade->SetorRep->getSetor().' '.$atividade->ColabRep->getNome() ?></b></h4>
        <h5 class="hidden-lg hidden-md hidden-sm"><b><?php echo $atividade->SetorRep->getSetor().' '.$atividade->ColabRep->getNome() ?></b></h5>
        <p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo fnDataExtenso($atividade->getDataEvento()); ?></p>
        
        
            <table class="table table-condensed">
               
                <tr>
                    <td width="22%" class="text-left"><small><b>End. Manhã:</b></small></td>
                    <td width="78%" class="text-left">
                        <small>
                            <?php if($atividade->Agenda->getPrimeiroEndereco('M', 'ANTERIOR') != '') { ?>
                            <span   data-html="true" data-title="<span style='font-size:11px'><b>Endereço Anterior:</b></span>" data-placement="bottom" data-toggle="popover" data-content="<span style='font-size:11px'><span class='text-muted'><?php echo $atividade->Agenda->getPrimeiroEndereco('M', 'ANTERIOR') ?></small></span>" class="text-warning glyphicon glyphicon-refresh"></span>                      
                            <?php } ?>
                            <span class="text-muted">
                                <?php  echo ($atividade->getEndManha() != "") ? $atividade->getEndManha() : $atividade->Agenda->getPrimeiroEndereco('M') ?>
                            </span>
                        </small>
                    </td>
                </tr>
                <tr>
                    <td><small><b>End. Tarde:</b></small></td>
                    <td class="text-left">
                        <small>
                            <?php if($atividade->Agenda->getPrimeiroEndereco('T', 'ANTERIOR') != '') { ?>
                                <span data-trigger="focus" data-title="<span style='font-size:11px'><b>Endereço Anterior:</b></span>"  data-html="true" data-placement="bottom" data-toggle="popover" data-content="<span style='font-size:11px'><span class='text-muted'><?php echo $atividade->Agenda->getPrimeiroEndereco('T', 'ANTERIOR') ?></small></span>" class="text-warning glyphicon glyphicon-refresh"></span>                      
                            <?php } ?>
                            <span class="text-muted">
                                <?php echo ($atividade->getEndTarde() != "") ? $atividade->getEndTarde() : $atividade->Agenda->getPrimeiroEndereco('T') ?>
                            </span>
                        </small>
                    </td>
                </tr>

                <tr>
                    <td width="22%"><small><b>Objetivo:</b></small></td>
                    <td width="78%" class="text-justify">
                        <small>
                            <span class="text-muted">
                                <?php echo $atividade->getObjetivo() ?>
                            </span>
                        </small>
                    </td>
                </tr>
                
                <tr>
                    <td width="22%"><small><b>Status:</b></small></td>
                    <td width="78%" class="text-left">
                        <div class="<?php echo $atividade->Status->getCor() ?>">
                            <small>
                                <span class="<?php echo $atividade->Status->getIcone() ?>"></span> 
                                <?php echo $atividade->Status->getDescricao().' '.(($atividade->TipoTroca->getDescricao() != "") ? '('.$atividade->TipoTroca->getDescricao().')' : '') ?>
                            </small>
                        </div>    
                    </td>
                </tr>
                <?php if($atividade->getMotivoTroca() != '') { ?>
                <tr>
                    <td width="22%"><small><b>Justificativa:</b></small></td>
                    <td width="78%">
                        <small class="text-muted">
                            <?php echo $atividade->getMotivoTroca(); ?>
                        </small>
                    </td>
                </tr>
                <?php } ?>
            </table>
 

    </div>

</div>
<!-- OUTRO EVENTO -->    
<?php } else { ?>

<div class="row">
    
    <div class="col-lg-3 col-sm-4 col-md-2 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:3px;">
                <img src="<?php echo APP_URL ?>public/img/calendar-512.png" style="padding:15px;" class="img-responsive" />
            </div>
        </div>
    </div>
        
    
    <div class="col-lg-9 col-sm-8 col-md-10 col-xs-12">
        <h4 class="hidden-xs"><b><?php echo $atividade->Tipo->getDescricao() ?></b></h4>
        <h5 class="hidden-lg hidden-md hidden-sm"><b><?php echo $atividade->Tipo->getDescricao() ?></b></h5>
        <p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo fnDataExtenso($atividade->getDataEvento()); ?></p>
        
        
            <table class="table table-condensed">
               
                <tr>
                    <td width="22%"><small><b>Objetivo:</b></small></td>
                    <td width="78%" class="text-justify"><small><span class="text-muted"><?php echo $atividade->getObjetivo() ?></span></small></td>
                </tr>
                
                <tr>
                    <td width="22%"><small><b>Status:</b></small></td>
                    <td width="78%" class="text-left">
                    <div class="<?php echo $atividade->Status->getCor() ?>"><small><span class="<?php echo $atividade->Status->getIcone() ?>"></span> <?php echo $atividade->Status->getDescricao().' '.(($atividade->TipoTroca->getDescricao() != "") ? '('.$atividade->TipoTroca->getDescricao().')' : '') ?></small></div>    
                    <small class="text-muted"><?php echo $atividade->getMotivoTroca(); ?></small>
                    </td>
                </tr>
            </table>
 

    </div>

</div>
    
<?php } ?>



          
</div>


<script>

$(function () {
    //

    $('[data-toggle="popover"]').mouseover(function(e) {
        $(this).popover('show');
    });

    $('[data-toggle="popover"]').mouseout(function(e) {
        $(this).popover('hide');
    });
});


//$('#btnImprimir').unbind().click(function(e) {
   // e.preventDefault();
    //location.href = '<?php echo APP_URL ?>atividade/imprimir/<?php echo $atividade->getIdAtividade() ?>';
    //setTimeout(alertFunc, 3);
    
    //$('#modalResumo').modal('hide');
   // $('#myModal').modal('show');
    
//});

</script>
<script src="<?php echo APP_URL ?>public/js/imprimirGraficoRAC.js"></script>