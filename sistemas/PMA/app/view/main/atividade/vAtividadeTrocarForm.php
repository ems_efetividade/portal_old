<form id="formJustificar" method="post" action="<?php echo APP_URL ?>atividade/trocar">
<input type="hidden" name="txtIdAtividade" value="<?php echo $idAtividade ?>" />

<div class="row">
    
    <div class="col-lg-12">
        
        
        <div class="row">
            <div class="col-lg-12">
                
                <div class="form-group form-group-sm">
                    <label for="exampleInputEmail1"><small>Qual o motivo da troca?</small></label>
                    <select class="form-control" name="cmbTroca">
                        <option value="0"></option>
                        <?php 
                            foreach($listaTipo as $i => $tipo) { 
                                if($i == 5) {
                                    echo '<option value="0"></option>';
                                }
                        ?>
                            <option value="<?php echo $tipo->getIdTipo() ?>"><?php echo $tipo->getDescricao() ?></option>
                        <?php } ?>
                    </select>
                </div> 
                
            </div>
            <div class="col-lg-12">
                <div class="form-group form-group-sm">
                    <label for="exampleInputEmail1"><small>Faça uma breve descrição:</small></label>
                    <textarea class="form-control" rows="7" name="txtMotivo"></textarea>
                </div> 
            </div>
        </div>
        
        
        
        
        
        
    </div>
    
    
    
</div>
</form>