
<?php $dataAnt = ''; foreach($eventos as $evento) { ?>
<?php if($dataAnt != $evento->getDataEvento()) { ?>
<h4 class="well well-sm"><img src="<?php echo APP_IMG ?>calendar.png" style="margin-right:5px" height="15" /><strong><?php echo fnDataExtenso($evento->getDataEvento()) ?> </strong></h4>
<?php } ?>
<table>
    <tr>
        <td width="150" style="vertical-align: top">
            <?php if($evento->Tipo->getAcomp() == 1) { ?>
            <img style="border:0px solid #ddd;padding:3px;" class="center-block" height="150" src="<?php echo  fnFotoColaborador($evento->ColabRep->getFoto()) ?>" />
            <?php } else { ?>
            <img src="<?php echo APP_IMG ?>calendar-512.png"  height="100" style="padding:15px;" class="img-responsive" />
            <?php } ?>
        </td>
        <td width="800px" style="vertical-align: top">
            <table class="table table-condensed">
                
                <tr>
                    <td colspan="2">
                        <b><?php echo ($evento->Tipo->getAcomp() == 1) ? $evento->SetorRep->getSetor().' '.$evento->ColabRep->getNome() : $evento->Tipo->getDescricao() ?></b>
                    </td>
                </tr>
                
                <?php if($evento->Tipo->getAcomp() == 1) { ?>
                
                <tr>
                    <td width="110" class="text-left"><small>End. Manhã:</small></td>
                    <td class="td text-justify"><span class="text-muted"><small><?php echo ($evento->getEndManha() == '') ? $evento->Agenda->getPrimeiroEndereco('M'): $evento->getEndManha(); ?></small></span></td>
                </tr>
                <tr>
                    <td class=""><small>End. Tarde:</small></td>
                    <td class="text-justify"><span class="text-muted"><small><?php echo ($evento->getEndTarde() == '') ? $evento->Agenda->getPrimeiroEndereco('T') : $evento->getEndTarde(); ?></small></span></td>
                </tr>
                <tr>
                    <td width="110" class="td text-left boald"><small>Acomp. por:</small></td>
                    <td class="td text-justify"><span class="text-muted"><small><?php echo $evento->SetorGer->getSetor().' '.$evento->ColabGer->getNome(); ?></small></span></td>
                </tr>
                <?php } ?>
                <tr>
                    <td class="td bolad"><small>Objetivo:</small></td>
                    <td class="td text-justify"><span class="text-muted"><?php echo $evento->getObjetivo() ?></small></span></td>
                </tr>
                
                <tr>
                    <td class="td bold"><small>Status:</small></td>
                    <td class="td text-left">
                        <table>
                            <tr>
                                <td class="td">
                                    <div>
                                        <p class="<?php echo $evento->Status->getCor() ?>"><small>
                                        <?php echo $evento->Status->getDescricao().' '.(($evento->TipoTroca->getDescricao() != "") ? '('.$evento->TipoTroca->getDescricao().')' : '') ?>
                                        </small></p> 
                                    </div> 
                                    
                                </td>
                            </tr>
                            <?php if($evento->getDataTroca() != "") { ?>
                            <tr>
                                <td class="td">
                                    <p><i><small class="text-muted">em <?php echo fnFormatarData($evento->getDataTroca()); ?></small></i></p>
                                </td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td class="td">
                                    <small class="text-muted"><?php echo $evento->getMotivoTroca(); ?></small>
                                </td>
                            </tr>
                            
                        </table>
                           
                        
                        
                    </td>
                </tr>
            </table>
        </td>
        
    </tr>
</table>



    <?php $medicos = $evento->Agenda->listar();  ?>
    <?php if(count($medicos) > 0) {  ?>

<p><b>Agenda Médica do dia (<small class="text-muted"><?php echo count($medicos) ?> médico(s)</small>)</b> </p>
    <table class="table table-condensed table-striped">
    <?php foreach($medicos as $i => $medico) { $back = ((($i%2) == 1) ? 'td-background':'') ?>
    
        <tr>
            <td>
                <div style="" class="label label-<?php echo ($medico->getPeriodoVisita() == 'M') ? 'primary' : 'warning' ?>">
                    <small>
                    <?php echo $medico->getHorarioVisita(); ?>
                    </small>
                </div> 
            </td>
            <td>
                <b><small><?php echo $medico->getNome() . ' ('.$medico->getCRM().')' ?></small></b>
                <div><small><small><?php echo $medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().' - '.$medico->getBairro(). ' - CEP: '.$medico->getCEP().', '.$medico->getCidade().'/'.$medico->getUF()  ?></small></small></div>
            </td>
        </tr>
    
    
<!--    <div class="div-tr text-muted <?php echo $back ?>">
        <div class="div-td" style="width: 30px;padding:15px">
            <div style="" class="label label-<?php echo ($medico->getPeriodoVisita() == 'M') ? 'primary' : 'warning' ?>">
                <small>
                <?php echo $medico->getHorarioVisita(); ?>
                    </small>
            </div> 
        </div>
        <div class="div-td">
            <div class="bold"><small><?php echo $medico->getNome() . ' ('.$medico->getCRM().')' ?></small></div>
            <small><small>
                <?php echo $medico->getEndereco().', '.$medico->getNro().' - '.$medico->getComplemento().' - '.$medico->getBairro(). ' - CEP: '.$medico->getCEP().', '.$medico->getCidade().'/'.$medico->getUF()  ?>
            </small></small>
        </div>
    </div>-->
    
    <?php } ?>
    
    
    </table>

    <?php } ?>
    <br>
<?php 
    $dataAnt = $evento->getDataEvento();
    } ?>