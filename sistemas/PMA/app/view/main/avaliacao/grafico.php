<?php $imgName = md5(round(microtime(true) * 1000)); ?>

<div id="container2" style="display: none"></div>
<canvas id="canvass" width="1000px" height="600px" style="display: none"></canvas> 


<!--


<link href="<?php echo APP_URL ?>framework/libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<script src="<?php echo APP_URL ?>framework/libraries/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo EMS_URL ?>/plugins/highchart/js/highcharts.js"></script>
<script src="<?php echo EMS_URL ?>/plugins/highchart/js/highcharts-more.js"></script>
<script src="<?php echo EMS_URL ?>/plugins/highchart/js/modules/exporting.src.js"></script>

<script type="text/javascript" src="https://canvg.github.io/canvg/rgbcolor.js"></script> 
<script type="text/javascript" src="https://canvg.github.io/canvg/StackBlur.js"></script>
<script type="text/javascript" src="https://canvg.github.io/canvg/canvg.js"></script> 

<script src="<?php echo APP_URL ?>public/js/imprimirGraficoRAC.js"></script>
<script src="<?php echo APP_URL ?>public/js/calendario.js"></script>-->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/canvg/1.4/rgbcolor.min.js"></script>
<!-- Optional if you want blur -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/stackblur-canvas/1.4.1/stackblur.min.js"></script>
<!-- Main canvg code -->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/canvg/dist/browser/canvg.min.js"></script>

<script id="runscript">
    
var EMS_URL = '<?php echo EMS_URL ?>';
var APP_URL = '<?php echo APP_URL ?>';
    
$(document).ready(function(e){
    var conf = {
            exporting: {
                    enabled:false
           },
                chart: {
                    renderTo: 'container2',
                    defaultSeriesType: 'spline'
                },
                title: {
                    text: 'Evolução dos Pilares'
                },
                xAxis: {
                    labels: {},
                    categories: <?php echo $dados['cabecalho'] ?>
                },
                    yAxis: [{
                            title: {
                                    text: 'Produtividade / Cob. Objetivo'
                            },
                            labels: {
                                format: '{value}',
                            },
                                },{
                                        title: {
                                                text: 'Presc. Share / Market Share'
                                        },
                            labels: {
                                format: '{value}',
                            },
                            opposite: true
                    }],
                    plotOptions: {
                        series:{
                                animation:true
                        },
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: <?php echo $dados['dados'] ?>
            }
        
        var a = new Highcharts.Chart(conf);        
        //b = exportarGraficoLinha(a, 900, 900);
        
        
        var svg = a.getSVG();
        //console.log(svg);
        canvg(document.getElementById('canvass'), svg);
        p = document.getElementById('canvass');

        var theImage = p.toDataURL('image/png');

        $.ajax({
            type: "POST",
            url: EMS_URL+'/plugins/exporting-server/index.php',
            data: {
                base64: theImage
            },
            success: function (data) {
              location.href = APP_URL+'avaliacao/imprimir/' + <?php echo $id ?> + '/' + data;
            }
        });
        
});

        
        
        

</script>
