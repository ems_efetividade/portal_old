<?php

function fotoPerfil($foto='') {
    $caminho = appConf::caminho;
    $retorno = 'public/img/exemplo_foteoo.jpg';
    
    if($foto != '') {
        if(file_exists('public/profile/'.$foto)) {
            $retorno = 'public/profile/'.$foto;
        } else {
            $retorno = 'public/img/exemplo_foteoo.jpg';
        }
    }
    
    return $caminho.$retorno;
    
}

function shortName1($nome) {
    $data = explode(" ", $nome);
    return $data[0].' '.$data[(count($data)-1)];
}

function shortName($nome) {
    $data = explode(" ", $nome);
    return $data[0].' '.$data[1].' '.$data[(count($data)-1)];
}

function ranking($valor, $icone) {
    if($valor > 0) {
        $arr['COR'] = 'success';
        $arr['ICONE'] = ($icone == 1) ? 'glyphicon glyphicon-arrow-up' : '';
    } else {
        $arr['COR'] =  'danger';
        $arr['ICONE'] = ($icone == 1) ? 'glyphicon glyphicon-arrow-down' : '';
    }
    
    return $arr;
}