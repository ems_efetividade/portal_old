<?php
require_once('functions.php');

$icon[0] = 'glyphicon glyphicon-circle-arrow-down';
$icon[1] = 'glyphicon glyphicon-circle-arrow-up';
$icon[2] = 'glyphicon glyphicon-circle-arrow-right';

?>

<style>


</style>

<table id="" class="tb-pma-dados table table-condensed table-striped table-bordered table-hover">

    <tr class="active">
        <?php foreach ($headers as $key => $col) { ?>
            <th class="strip-0 <?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?>">
                <?php echo (isset($cab[$col['COLUNA']])) ? $cab[$col['COLUNA']] : $col['LABEL'] ?>
            </th>
        <?php }
        ?>
    </tr>

    <?php
    if (is_array($dados))
        foreach ($dados as $i => $dado) {
            ?>
            <tr>
                <?php foreach ($headers as $key => $col) { ?>
                    <td class=" strip-<?php echo ($i%2) ?>  hand c-<?php echo $dado['E_' . $col['COLUNA']] ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?> <?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?>">
                        <b>
                    <span class="btn-modal-produto" data-id="<?php echo $dado['ID_PILAR'] ?>"
                          data-setor="<?php echo $dado['SETOR'] ?>" data-pilar="<?php echo $dado['PILAR'] ?>">
                        <span class="pull-left <?php echo ($col['ICONE'] == 1 && $dado[$col['COLUNA']] != 0) ? $icon[$dado['E_' . $col['COLUNA']]] : '' ?>"></span>
                        <?php echo ($col['NUMERICO'] == 1) ? appFunction::formatarMoeda($dado[$col['COLUNA']], $dado['DECIMAL']) : $dado[$col['COLUNA']] ?>
                    </span>
                        </b>
                    </td>
                <?php } ?>
            </tr>
        <?php } ?>
</table>