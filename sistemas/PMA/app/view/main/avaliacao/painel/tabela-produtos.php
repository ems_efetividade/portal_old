<?php 
    require_once('functions.php');

    $icon[0] = 'glyphicon glyphicon-circle-arrow-down';
    $icon[1] = 'glyphicon glyphicon-circle-arrow-up';
    $icon[2] = 'glyphicon glyphicon-circle-arrow-right';
?>

<link   href="<?php echo APP_URL ?>app/view/main/avaliacao/painel/painel.css" rel="stylesheet" type="text/css"/>

<div id="pma-painel">

<h4 class="text-muted" ><b>Indicadores de <span id="tit-tabela"></span></b></h4>

<table class="table table-condensed table-striped table-bordered table-hover" style="font-size: 11px;">
    
    <tr class="active">
        <?php foreach($headers as $key => $col) { ?>
            <th class="<?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?>">
                <?php echo (isset($cab[$col['COLUNA']])) ? $cab[$col['COLUNA']] : $col['LABEL'] ?>
            </th>
        <?php } ?>
    </tr>
    
    <?php foreach($dados as $dado) { ?>
    <tr>
        <?php foreach($headers as $key => $col) { ?>
            <td class="c-<?php echo $dado['E_'.$col['COLUNA']] ?> <?php echo ($col['NUMERICO'] == 1) ? 'text-center' : '' ?> <?php echo ($col['BORDA'] == 1) ? 'td-border' : '' ?>">
                <span class="pull-left <?php echo ($col['ICONE'] == 1 && $dado[$col['COLUNA']] != '') ? $icon[$dado['E_'.$col['COLUNA']]]  :  '' ?>"></span>
                <?php echo ($col['NUMERICO'] == 1) ? appFunction::formatarMoeda($dado[$col['COLUNA']],$dado['DECIMAL']) :$dado[$col['COLUNA']] ?>
                
            </td>
        <?php } ?>
    </tr>
   <?php } ?>
</table>

        </div>