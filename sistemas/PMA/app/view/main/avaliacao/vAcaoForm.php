<?php validarSessao();  ?>
<form id="formAcao" method="post" action="<?php echo APP_URL ?>acao/salvar">
    
    <input type="hidden" name="txtIdAcao" value="<?php echo $acao->getIdAcao() ?>" />
    <input type="hidden" name="txtIdAvaliacao" value="0" />
    <input type="hidden" name="txtIdColabRep" value="0" />
    <input type="hidden" name="txtIdCiclo" value="<?php echo $idCiclo ?>" />
    
    
    <div class="row">
        <div class="col-lg-6">
            
            <div class="form-group">
                <label for="exampleInputEmail1">Qual o objetivo dessa ação?</label>
                <textarea class="form-control input-sm" rows="3" name="txtAcao"><?php echo $acao->getAcao() ?></textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Qual a relevância da ação?</label>
                <textarea class="form-control input-sm" rows="3" name="txtRelevancia"><?php echo $acao->getRelevancia() ?></textarea>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Como mensurar essa ação?</label>
                <textarea class="form-control input-sm" rows="3" name="txtMensurar"><?php echo $acao->getMensurar() ?></textarea>
            </div>
            
            <div class="form-group">
                <label for="exampleInputEmail1">Qual o prazo?</label>
                <select class="form-control input-sm" name="cmbPrazo">
                  <option value="1" <?php echo (($acao->getPrazo() == 1) ? 'selected' : ''); ?>>Próximo Ciclo</option>
                  <option value="2" <?php echo (($acao->getPrazo() == 2) ? 'selected' : ''); ?>>Próximos 2 Ciclos</option>
                  <option value="3" <?php echo (($acao->getPrazo() == 3) ? 'selected' : ''); ?>>Próximos 3 Ciclos</option>

                </select>
            </div>
            
            
        </div>
        <div class="col-lg-6">
            
            <label for="exampleInputEmail1">Quais são os requisitos para atingir o objetivo dessa ação?</label>
            <div class="input-group">
                <input type="text" id="txtAddObj" class="form-control input-sm" placeholder="">
                <span class="input-group-btn">
                    <button id="btnAddObj" class="btn btn-success btn-sm" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                </span>
            </div>
            <p></p>
    
            <div class="panel panel-default">
                <div class="panel-body" style="height: 120px; overflow: auto">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul id="ul-obj">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    
    
            <label for="exampleInputEmail1">Quais os passos a serem seguidos?</label>
            <div class="input-group">
                <input type="text" id="txtAddReq" class="form-control input-sm" placeholder="">
                <span class="input-group-btn">
                    <button id="btnAddReq" class="btn btn-success btn-sm" type="button"><span class="glyphicon glyphicon-plus"></span></button>
                </span>
            </div>
            <p></p>
    
            <div class="panel panel-default">
                <div class="panel-body" style="height: 120px; overflow: auto">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul id="ul-req">

                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <input type="hidden" name="txtRequisito" value="<?php echo $acao->getRequisito() ?>" />
            </div>

            <div class="form-group">
                <input type="hidden" name="txtPassos" value="<?php echo $acao->getPassos() ?>" />
            </div>

        </div>
    </div>

</form>

<script>
    
function txtArea(txtArea, obj) {
    $('input[name="'+txtArea+'"]').val('');
    $('.'+obj+'').each(function() {
        var txt = $('input[name="'+txtArea+'"]').val();
        
        txt = (txt != '') ? txt + ';' : '';
        $('input[name="'+txtArea+'"]').val(txt +  $(this).text());
    });
}

function addList(value, ulID) {
    if(value != '') {
        var li = '<li><small><span class="txt'+ulID+'">'+value+'</span></small> <small>(<a class="remover'+ulID+'" href="#"><span class="text-danger glyphicon glyphicon-remove"></span></a>)</small></li>';
        $('#ul-'+ulID).append(li);
    }
}


var requisitos = $('input[name="txtRequisito"]').val();
var passos = $('input[name="txtPassos"]').val();

requisitos = requisitos.split(';');
$.each(requisitos, function(key, value) {
    addList(value, 'obj');
});

passos = passos.split(';');
$.each(passos, function(key, value) {
    addList(value, 'req');
});

    $('.removerobj').unbind().click(function(e) {
        $(this).closest('li').remove();
        txtArea('txtRequisito', 'txtobj');
    });
    
    $('.removerreq').unbind().click(function(e) {
        $(this).closest('li').remove();
        txtArea('txtPassos', 'txtreq');
    });


$('#btnAddObj').unbind().click(function(e) {
    e.preventDefault();
    value = $('#txtAddObj').val();
    addList(value, 'obj');
    txtArea('txtRequisito', 'txtobj');
    $('#txtAddObj').val('');
    
    $('.removerObj').unbind().click(function(e) {
        $(this).closest('li').remove();
        txtArea('txtRequisito', 'txtobj');
    });
    
});


$('#btnAddReq').unbind().click(function(e) {
    e.preventDefault();
    value = $('#txtAddReq').val();
    addList(value, 'req');
    txtArea('txtPassos', 'txtreq');
    $('#txtAddReq').val('');
    
    $('.removerReq').unbind().click(function(e) {
        $(this).closest('li').remove();
        txtArea('txtPassos', 'txtreq');
    });
});
    
    

</script>