<?php validarSessao();  ?>
<style>
    .table tbody>tr>td {
    vertical-align: middle;
}

.strike {
    text-decoration: line-through;
}
</style>
<div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="row">
                <div class="col-lg-7">
                    <a href="#"  data-toggle="collapse" aria-expanded="false" aria-controls="c<?php echo $plano->getIdAcao() ?>" data-target="#c<?php echo $plano->getIdAcao() ?>">
                        <span class="caret"></span>
                    
                    <b><?php echo $plano->getAcao() ?></b>
                    </a>
                    
                </div>
                <div class="col-lg-3">
                    <small><?php echo (($plano->getConcluido() == 1) ? 'Concluído ('.fnFormatarData($plano->getDataConclusao()).')' :  'Em Desenvolvimento'); ?></small>
                </div>
                <div class="col-lg-2 text-right">
                    <?php $statusAval ?>
                    <?php if($plano->getIdCiclo() == $idCiclo && $statusAval == 1) { ?>
                    <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-id="<?php echo $plano->getIdAcao() ?>" data-ciclo="<?php echo $plano->getIdCiclo() ?>" data-target="#modalAcao">Editar</button>
                    <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-id="<?php echo $plano->getIdAcao() ?>" data-target="#modalExcluir">Excluir</button>
                    <?php } ?>
                </div>
            </div>
            </div>
    </div>
    <p></p>
    <div id="c<?php echo $plano->getIdAcao() ?>" class="row collapse" style="background-color: #fff;">
        <div class="col-lg-4" style="padding-right: 2px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <small>
                        <b>
                            <span class="glyphicon glyphicon-ok"></span>
                            Planejameto
                        </b>
                    </small>
                </div>
                
                <div class="panel-body" style="height: 150px;overflow: auto">                        
                    <div><small><b>Relevância: </b></small><small class="text-muted"><?php echo $plano->getRelevancia() ?></small></div>
                    
                    
                    <div><small><b>Mensuração: </b></small><small class="text-muted"><?php echo $plano->getMensurar() ?></small></div>
                    
                </div>
            </div>
        </div>
        <div class="col-lg-4" style="padding-left:2px;padding-right: 2px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <small>
                        <b>
                            <span class="glyphicon glyphicon-ok"></span>
                            Requisitos
                        </b>
                    </small>
                </div>
                
                <div class="panel-body" style="height: 150px;overflow: auto">                        
                    <ul>
                        <?php 
                            $lista = explode(";", $plano->getRequisito());
                            foreach($lista as $item) {
                        ?>

                        <li>
                            <small><?php echo $item ?></small>
                        </li>

                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-4"  style="padding-left: 2px;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <small><b><span class="glyphicon glyphicon-ok"></span> Passos</b></small>
                </div>
                <div class="panel-body" style="height: 150px;overflow: auto">
            
                                          
            <ul>
                <?php 
                    $lista = explode(";", $plano->getPassos());
                    foreach($lista as $item) {
                ?>

                <li>
                    <small><?php echo $item ?></small>
                </li>
                
                <?php } ?>


            </ul>
             </div></div>   
        </div>
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <small><b>Prazo/Status</b></small>
                </div>
                
                <div class="panel-body">
            
            <table class="table table-condensed table-striped">
                <tr>
                    <th><small>Ciclo</small></th>
                    <th><small>Status</small></th>
                    <th><small>Observação do Gerente</small></th>
                    <th><small>Data</small></th>
                    <th><small>Obs</small></th>
                </tr>
                
                <?php 
                    $acoes = $plano->Acao->listar();
                    foreach($acoes as $acao) {
                ?>
                
                <?php 
                            $cls = '';
                            if($plano->getConcluido() == 1 && $acao->getDataObservacao() == '') {
                                $cls = 'strike text-muted';
                            }
                        
                        ?>
                <tr class="<?php echo $cls ?>">
                    <td width="5%"><small><?php echo $acao->Ciclo->getNomeCiclo() ?></small></td>
                    <td width="15%">
                        
                        
                        
                       
                        
                        <small><small>
                            <div id="d-cor<?php echo $acao->getIdCicloAcao(); ?>" class="<?php echo $acao->Status->getCor() ?>">
                                <span id="d-icon<?php echo $acao->getIdCicloAcao(); ?>" class="<?php echo $acao->Status->getIcone() ?>"></span>
                                <span id="d-status<?php echo $acao->getIdCicloAcao(); ?>"><?php echo $acao->Status->getDescricao() ?></span>
                            </div>
                        </small></small>
                            
                            
                        
                            
                    </td>
                    <td width="60%" class="text-muted">
                        <i>
                        <small id="d-obs<?php echo $acao->getIdCicloAcao(); ?>" >
                           <?php echo $acao->getObservacao(); ?>
                        </small>
                        </i>
                    </td>
                    <td width="15%">
                        <small id="d-data<?php echo $acao->getIdCicloAcao(); ?>">
                        <?php echo fnFormatarData($acao->getDataObservacao()); ?>
                        </small>
                    </td>
                    <td width="5%">
                        <?php if($acao->Ciclo->getIdCiclo() == $idCiclo && $plano->getConcluido() == 0) { ?>
                        <button id="" type="button" data-toggle="modal" data-target="#modalStatusAcao" data-id="<?php echo $acao->getIdCicloAcao(); ?>" class="btnEditarStatus btn btn-primary btn-xs"><small>Status</small></button>
                        <?php } ?>
                    </td>
                </tr>
                
                    <?php } ?>
                
            </table>
                </div></div>

        <hr>

        </div>

    </div>
                                
</div>