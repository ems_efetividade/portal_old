<form id="formStatus" method="post" action="<?php echo APP_URL ?>acao/salvarStatus">
    
    <input type="hidden" name="txtIdAcao" value="<?php echo $acao->getIdCicloAcao(); ?>" />
    
    <div class="form-group">
        <label for="exampleInputEmail1">Qual o status dessa ação?</label>
        <select class="form-control input-sm" name="cmbStatus">
            
            <?php foreach($acao->Status->listar() as $status) { ?>
          <option <?php echo (($status->getIdStatusAcao() == $acao->getIdStatusAcao()) ? "selected" : "") ?> value="<?php echo $status->getIdStatusAcao() ?>"><?php echo $status->getDescricao() ?></option>
            <?php } ?>
         
        </select>
    </div>
    
    

    <div class="form-group">
        <label for="exampleInputEmail1">Quais os passos a serem seguidos?</label>
        <textarea class="form-control input-sm" rows="4" name="txtObservacao"><?php echo $acao->getObservacao(); ?></textarea>
    </div>
    
    <div class="checkbox">
    <label>
      <input type="checkbox"> Finalizar essa ação.
    </label>
  </div>


    
</form>