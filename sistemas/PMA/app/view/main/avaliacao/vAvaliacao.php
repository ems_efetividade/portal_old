<?php include_once VIEW_PATH . 'main/vHeader.php';  ?>
<style>
    .progress {margin-bottom: 0px;}
    .progress {position:relative;}
    .progress span {position:absolute;left:0;width:100%;text-align:center;z-index:2;font-weight: bold;}
    .cor1 {color:#888 !important;}
    .cor2 {color:#FFF !important;}
    .my-group .form-control{
        width:50%;
    }
</style>
<div class="container-fluid">
    <div class="row">

        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH . 'main/vMenu.php'); ?>
            <br>
            <small>
            <div id="calendar" class="csollapse" >
            <?php 
                $calendarioMes = new mCalendario();
                echo $calendarioMes->gerarCalendarioMes();
            ?>
            </div>
            </small>
        </div>

        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-12">
                    <h3><span class="glyphicon glyphicon-calendar"></span> RAC - Relatório de Acompanhamento de Campo <small>(<?php echo $totalAval ?> relatório(s))</small></h3>
                </div>
            </div>
            <hr>

            <div class="row">
                <div class="col-lg-2">
                    <p>
                        <a class="btn btn-sm btn-block btn-success" id="btnSelColab" href="#">
                            <span class="glyphicon glyphicon-plus"></span> Novo Relatório</a>
                    </p>
                </div>
              
                <div class="col-lg-10">
                    
                    <div class="input-group input-group-sm">
                        <form id="formPesquisar" action="<?php echo APP_URL ?>avaliacao/pesquisar" method="post">
                            <input type="text" class="form-control input-sm" placeholder="Pesquisar..." name="txtCriterio">
                            <input type="hidden" name="txtPagina" value="1" />
                        </form>
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="btnPesquisar">Pesquisar</button>
                        </span>
                    </div>
                </div>
            </div>

            <div id="listaAvaliacao">
                <?php echo $tabelaAvaliacao; ?>
            </div>
        </div>
    </div>
</div>

<?php include_once VIEW_PATH . 'main/export.php'; ?>

<script src="<?php echo APP_URL ?>public/js/imprimirGraficoRAC.js"></script>
<script src="<?php echo APP_URL ?>public/js/calendario.js"></script>

<div class="modal fade" id="modalSelColab" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Selecionar Colaborador</h5>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Principais Lacunas <a href="#" class="pop" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-html="true"  data-content="<div class='div-popover'><small class='text-muted'></small></div>"><span class="glyphicon glyphicon-comment"></span></a></label>
                    <textarea class="form-control" rows="6" name="txtLacunas"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <a href="#" class="btn btn-primary" id="btnAbrirForm"><span class="glyphicon glyphicon-ok"></span> Selecionar</a>
            </div>
        </div>
    </div>
</div>



<script>

    

    resizePanelLeft();
    
    
    $('input[name="txtCriterio"]').keypress(function(event){
        if(event.keyCode == 13) {
            //event.preventDefault();
            $('#btnPesquisar').click();
            return false;
          }
    })
    


    $('#btnSelColab').unbind().click(function (e) {
        $.post('<?php echo APP_URL ?>avaliacao/formSelColab/', function (data) {
            $('#modalSelColab .modal-body').html(data);
            $('#modalSelColab').modal('show');


            $('#btnAbrirForm').unbind().click(function (e) {
                var ciclo = $('#cmbCiclo').val();
                var colab = $('#cmbColaborador').val();

                colab2 = colab.split('|');

                $.post('<?php echo APP_URL ?>avaliacao/verificarAvaliacao/' + colab2[1] + '/' + ciclo, function (data) {
                    var existe = data;

                    if (existe == 0) {
                        link = '<?php echo APP_URL ?>avaliacao/form/0/' + colab2[1] + '/' + ciclo + '/' + colab2[0];
                        location.href = link;
                    } else {
                        alert('Já existe um relatório para esse colaborador neste ciclo!');
                    }
                });





            });


        });
    });


    $('#btnPesquisar').unbind().click(function (e) {
        $('input[name="txtPagina"]').val(1);
        $.ajax({
            type: "POST",
            url: $('#formPesquisar').attr('action'),
            data: $('#formPesquisar').serialize(),
            beforeSend: function (e) {

            },
            success: function (retorno) {
                $('#listaAvaliacao').html(retorno);
            }
        });
    });



</script>

