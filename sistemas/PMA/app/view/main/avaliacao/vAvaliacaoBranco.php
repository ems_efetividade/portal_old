<?php $imgFolder = str_replace('profile', 'img', EMS_FOTO_PROFILE); ?>
<?php
$linhas = 0;
for($x=0;$x<=11;$x++) {
    $linha .= '<div class="linha"></div>';
}

?>
<head>
    <meta charset='utf-8'>
    <link href="<?php echo APP_URL ?>framework/libraries/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo APP_URL ?>public/css/default.css" rel="stylesheet">
    <style>
         p { margin-bottom:4px;padding-bottom:4px }
        .body {font-size: 10px;}

        .bold { font-weight: bold; }
        .h4 {font-size:17px;}
        .td { padding-bottom: 4px; padding-top: 4px }
        .td-border {border-bottom: 1px solid #ddd; }
        .td-background { background-color: #f9f9f9 !important; }
        hr {margin:0px; border-top: 1px solid #eee}
        table {margin-top: 5px;}
        #tb-medico .td { padding-bottom: 8px; padding-top: 8px }
        .label {border:0px; padding:5px; border-radius: 10px;}
        .div-td {float:left; padding-bottom: 5px; padding-top: 1px}
        .div-tr { border-top: 1px solid #eee }
        
        .panell {
            padding:10px; width: 112px; border: 1px solid #ddd; float:left;
        }
        #tb-pilares {font-size: 10px; font-weight: bold; text-align: center}
        #tb-questao div {padding-bottom: 5px;}
        .well {margin-bottom:5px}
        .texto {font-size: 5px;}
        .linha {width: 100%;height: 30px; border-top: 1px solid #ddd;}
        .bloquotes {border:1px solid #ddd; background-color: #f9f9f9;padding:10px;margin-bottom: 10px;}

        
        .tb-pma-dados {font-size:10px !important;color: #73879C; width:100%}
        .tb-pma-dados td, #pma-painel .tb-pma-dados th {padding:5px}
        .tb-pma-dados td {font-weight:bold}
        .tb-pma-dados .strip-0 {}
        
        .tb-pma-dados .strip-0 {background-color:#f9f9f9;}
        .tb-pma-dados .border-0 {border-top:1px solid #ddd;}




        #pma-painel {
    /* font-family: "arial"; */
    /*font-family: "open_sansregular";*/
    height:100%;
    font-family: 'Roboto', "arial";
    color: #73879C;
}


#pma-painel .panel-left { 
    /*border-right: 1px solid #ddd; */
    /*background: #2A3F54;*/ 
    color: #fff;
}

#pma-painel .panel-right {
    padding:0px;
}

#pma-painel .chk {
    margin:0px !important;  
}
    
#pma-painel .thick-border {
    border-right: 2px solid #ddd;
}

#pma-painel .hr {
    margin-top:6px;
    border-bottom:1px solid #ddd;
    margin-bottom: 6px;
}

#pma-painel .alert {
    margin-bottom:2px; 
    padding:2px
}

#pma-painel .alert-default {
    color: #777;
    background-color: #e5e5e5;
    border-color: #CCC;
}

#pma-painel .panel {
    margin:2px;
}

#pma-painel .panel-body {
    padding:10px;
}

#pma-painel .tb td, #pma-painel .tb th {
    white-space: nowrap;
}

#pma-painel .table {
    margin-bottom: 0px;
    font-size: 12px
}

#pma-painel .table > tbody > tr > td {
     vertical-align: middle;
}

#pma-painel .progress {
    margin-bottom: 0px;
    height: 15px;
    position: relative;
}

#pma-painel .nopad {
    padding:0px;
}
#pma-painel .progress-bar-title {
    position: absolute;
    text-align: center;
    line-height: 15px; /* line-height should be equal to bar height */
    overflow: hidden;
    color: #FFF;
    right: 0;
    left: 0;
    top: 0;
    font-weight: 600;
}

#pma-painel .progress-bar-title-white {
    color: #FFF;
}

#pma-painel .progress-bar-title-black {
    color: #777;
}

#pma-painel .padding {
    padding-right: 10px;
    padding-left: 10px;
}

#pma-painel .bg-default {
    color: #000;
    background-color: #ddd;
}

#pma-painel .box {
    font-size:35px;
    font-weight: 600;
}


#nav-dash a {
    color:#73879C;
}

#pma-painel .th {
    font-weight: bold;
}

#pma-painel .nav-menu-right {
    border-bottom: 1px solid #ddd; 
    background: #f1f1f1;
    margin-bottom: 5px;
}




#pma-painel.td-pad {
        padding-left:15px;
    }

    #pma-painel  .td-border {
        border-right: 3px solid #ddd !important;   
    }


    #pma-painel  .c-1 {
        color:green;
    }

    #pma-painel  .c-0 {
        color: red;
    }
    
    #pma-painel   .c-2 {
        color: orange;
    }

    #pma-painel   .hand {
        cursor:pointer;
    }

    </style>  
    
</head>
  <?php
$a[1]['ICONE'] = 'thumb-down.png';
$a[1]['COR'] = 'danger';
$a[2]['ICONE'] = 'alert.png';
$a[2]['COR'] = 'warning';
$a[3]['ICONE'] = 'ok.png';
$a[3]['COR'] = 'success';
$a[4]['ICONE'] = 'medal.png';
$a[4]['COR'] = 'info';

  ?>

<htmlpageheader name="myHTMLHeader">
<div>
    <table style="width:100%; padding:0px;">
    <tr>
        <td width="13%" class="text-center">
            <img src="<?php echo getcwd() ?>\\..\\..\\public\img\iconsFerramentas\logoEfet.png" height="60"  />
            <p class="text-muted" style="margin:0px;padding:0px; font-size:7px;">E F E T I V I D A D E</p>    
        </td>
        <td width="87%" class="text-left">
            <h4 class="bold"><?php echo $nomeReport; ?></h4>
            <h5 class="text-muted"><small><?php echo APP_NAME ?></small></h5>
        </td>
    </tr>
    </table>
</div>
<div> 
    <div style="border-bottom: 1px solid #666; margin-top: 0px;margin-bottom: 40px;"></div>
</div>


<div id="pma-painel">
</htmlpageheader>

<sethtmlpageheader name="myHTMLHeader"  value="on" show-this-page="1" />




<div>
    
<div class="div-td" style="padding-top:10px;width: 100%;margin-righet: 1%;">    

<div class="well well-sm bold" ><img src="public/img/check.png" style="margin-right:0px" />Dados do Colaborador</div>

<div style="float:left; width: 20%;padding-left:10px">
    <img src="<?php echo str_replace(EMS_URL.'/public/img/', (str_replace('profile', 'img', EMS_FOTO_PROFILE)), str_replace(EMS_URL.'/public/profile/', EMS_FOTO_PROFILE, fnFotoColaborador($aval->ColabRep->getFoto()))) ?>"  />
</div>

<div style="float:left;font-size:11px;">
    <div class="bold" style="font-size:13px;"><?php echo $ativ->ColabRep->getNome(); ?></div>
    <div class="text-muted">FUNÇÃO: <?php echo $ativ->SetorRep->getPerfil(); ?></div>
    <div class="text-muted">SETOR:  <?php echo $ativ->SetorRep->getSetor(); ?> (<?php echo $ativ->SetorRep->getNome(); ?>)</div>
    <div class="text-muted">LINHA:  <?php echo $ativ->SetorRep->getLinha(); ?></div>
</div>
</div>


<div style="clear: both"></div>
<div>
<?php $rank = $novo['rank'][1] ?>

<div class="div-td" style="width: 39%;margin-right:1%;">
<div class="well well-sm bold" ><img src="public/img/check.png" style="margin-right:5px" />Pontuação</div>

<table class="tb-pma-dados table" style="font-size:12px;">
<tr>
    <td class="strip-0">Pilar</td>
    <td class="strip-0 text-center">Valor</td>
    <td class="strip-0 text-center">Nota</td>
</tr>
    <?php foreach($novo['pontuacao'] as $i => $ponto) { ?>
    <tr>
        <td class="sstrip-<?php echo ($i%2) ?> border-0 text-muted"><?php echo $ponto['PILAR'] ?></td>
        <td class="sstrip-<?php echo ($i%2) ?> border-0 text-<?php echo $a[$ponto['PONTO']]['COR'] ?> text-center">
            <?php echo appFunction::formatarMoeda($ponto['M00'], $ponto['DECIMAL']) ?><?php echo $ponto['SUFIXO'] ?>    
        </td>
        <td class="sstrip-<?php echo ($i%2) ?> border-0 text-center">
            <div class="label label-<?php echo $a[$ponto['PONTO']]['COR'] ?>">
                <div style="padding:5px">&nbsp;&nbsp;<?php echo $ponto['PONTO'] ?>&nbsp;&nbsp;</div>
            </div>
        </td>
    </tr>
    <?php } ?>
</table>
</div>

<div class="div-td" style="width: 59%;margin-left: 1%;">
<div class="well well-sm bold" ><img src="public/img/check.png" style="margin-right:5px" />Ranking</div>

<table style="width:100%">
    <tr>
        <td class="text-center" style="height:120px;width:50%;bordser:1px solid #ddd">

            <table style="width:100%;color: #73879C;">
                <tr>
                    <td class="text-center"><div style="font-size:12px;font-weight:bold;"><?php echo $rank['LABEL_BRASIL'] ?></div></td>
                </tr>
                <tr>
                    <td class="text-center" styele="paddding-bottom:3px;width:70%;border-bottom:1px solid #ddd">
                    <img width="170" src="<?php echo getcwd() ?>\\..\\..\\plugins\exporting-server\files\<?php echo $grafico ?>_rnk1.png" stysle="width:100%"  />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" swtyle="padding-top:6px;">
                        <table  style="width:125px;font-size:12px;">
                            <tr>
                                <td style="width:33%;bborder:1px solid #ddd" class="text-center"><?php echo $rank['COUNT_BRASIL'] ?>º</td>
                                <td style="width:33%;bborder:1px solid #ddd;font-weight:bold;font-size:14px;" class="text-center"><?php echo $rank['RNK_BRASIL'] ?>º</td>
                                <td style="width:33%;bborder:1px solid #ddd" class="text-center">1º</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>


            
            <!-- <div style="border-bottom:1px solid #ddd;width:100px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div> -->
        </td>

        <td class="text-center" style="heigsht:120px;width:50%;bosrder:1px solid #ddd">
        <table style="width:100%;color: #73879C;">
                <tr>
                    <td class="text-center"><div style="font-size:12px;font-weight:bold;"><?php echo $rank['LABEL_REGIONAL'] ?></div></td>
                </tr>
                <tr>
                    <td class="text-center" styele="paddding-bottom:3px;width:70%;border-bottom:1px solid #ddd">
                    <img width="170" src="<?php echo getcwd() ?>\\..\\..\\plugins\exporting-server\files\<?php echo $grafico ?>_rnk2.png" stysle="width:100%"  />
                    </td>
                </tr>
                <tr>
                    <td class="text-center" swtyle="padding-top:6px;">
                        <table  style="width:125px;font-size:12px;">
                            <tr>
                                <td style="width:33%;bborder:1px solid #ddd" class="text-center"><?php echo $rank['COUNT_REGIONAL'] ?>º</td>
                                <td style="width:33%;bborder:1px solid #ddd;font-weight:bold;font-size:14px;" class="text-center"><?php echo $rank['RNK_REGIONAL'] ?>º</td>
                                <td style="width:33%;bborder:1px solid #ddd" class="text-center">1º</td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

      

</div>
<div class="well well-sm bold" ><img src="public/img/check.png" style="margin-right:5px" />Evolução dos Pilares</div>
<div class="panell" style="margin-top:10px; width: 100%; border: 0px solid #ddd">

        <img hesight="400" src="<?php echo getcwd() ?>\\..\\..\\plugins\exporting-server\files\<?php echo $grafico ?>.png" stysle="width:100%"  />
        
        <div id="pma-painel">
            <?php echo str_replace('<table', '<table style="padding:0px; font-size:9px;" ', $novo['tabela']) ?>
        </div>

        </div>
    </div>
</div>
    
</div>



<h4 class="well well-sm bold"><img src="public/img/check.png" style="margin-right:5px" />Análise do Painel de Bordo</h4>
<br>
<h5 class="bold">Aspectos Positivos</h5>
<?php if($aval->getApectoPos() != '') { ?>
<h5 class="text-muted"><small><span class="bold"><?php echo $aval->SetorGer->getSetor().' '.$aval->ColabGer->getNome() ?></span> em <span class="bold"><?php echo fnFormatarData($aval->getDataUltSalva()) ?></span> escreveu:</small></h5>
<div class="text-muted bloquotes"><small><small><i>"<?php echo  nl2br($aval->getApectoPos()) ?>"</i></small></small></div>

<?php } ?>
<?php echo $linha; ?>

<h5 class="bold">Principais Lacunas</h5>
<?php if($aval->getLacunas() != '') { ?>
<h5 class="text-muted"><small><span class="bold"><?php echo $aval->SetorGer->getSetor().' '.$aval->ColabGer->getNome() ?></span> em <span class="bold"><?php echo fnFormatarData($aval->getDataUltSalva()) ?></span> escreveu:</small></h5>
<div class="text-muted bloquotes"><small><small><i>"<?php echo  nl2br($aval->getLacunas()) ?>"</i></small></small></div>

<?php } ?>
<?php echo $linha; ?>


<divider></divider>
<h4 class="well well-sm bold"><img src="public/img/check.png" style="margin-right:5px" />Análise TecnicoComportamental</h4>
<br>
<?php 
    $nresp = count($respostas); 
    $wQ = 45.5;
    $wR = ((100 - $wQ) / $nresp)-0.01;
    
?>
<?php foreach ($modulos as $i => $modulo) { $c = (($i%2) ? 'td-background' : ''); ?>

<div style="width:100%;" >
    
    <div style="width: 48%; float: left; border-top: 1px solid #e5e5e5" class="td-background">
        <div class="bold <?php echo $c; ?>">
            <div style="height: 35px; line-height: 35px; float: left;padding: 0px; margin: 0px">
                <small><?php echo  ($i+1).'. '. $modulo->getQuestao() ?></small>
            </div>
        </div>
    </div>
    <div style="width: 52%; float: left" class="td-background">
        <?php foreach ($respostas as $resposta) { ?>
            <div style="height: 35px;  width:25%;   float: left; border-top: 1px solid #e5e5e5" class="text-center bold">
                <small><small><?php echo $resposta->getDescricao() ?></small></small>
            </div>
        <?php } ?>
    </div>
    
    <div style="clear: both"></div>
 
    <?php foreach ($modulo->listarQuestao() as $u => $questao) { ?>
    <div style="width: 48%; float: left; border-top: 1px solid #e5e5e5">
        <div class="text-muted">
            <div style="height: 35px; float: left;margin-bottom:5px; padding-top: 5px;">
                &nbsp;&nbsp;<small><b><?php echo ($i + 1) . '.' . ($u + 1) . '. ' . $questao->getQuestao() ?></b></small>
            </div>
        </div>
    </div>
    
    <div style="width: 52%; float: left;  border-top: 1px solid #e5e5e5">
        <?php foreach ($respostas as $resposta) { ?>
            <div style="width: 25%; height: 35px;  float: left; margin-bottom:5px; padding-top: 5px;" class="text-center">
                <img style="padding-top: 14px" src="public/img/unchecked.png" height="13" />
            </div>
        <?php } ?>
    </div>
    <div style="clear: both"></div>
    
    <?php } ?>   
</div>
<div style="clear: both"></div>
<?php } ?>



<divider></divider>
<h4 class="well well-sm bold"><img src="public/img/check.png" style="margin-right:5px" />Feedback do Período</h4>
<br>
<h5 class="bold">Feedback Positivo</h5>
<?php if($aval->getFeedPos() != '') { ?>
<h5 class="text-muted"><small><span class="bold"><?php echo $aval->SetorGer->getSetor().' '.$aval->ColabGer->getNome() ?></span> em <span class="bold"><?php echo fnFormatarData($aval->getDataUltSalva()) ?></span> escreveu:</small></h5>
<div class="text-muted bloquotes"><small><small><i>"<?php echo  nl2br($aval->getFeedPos()) ?>"</i></small></small></div>
<?php } ?>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>


<h5 class="bold">Feedback Corretivo</h5>
<?php if($aval->getFeedCorr() != '') { ?>
<h5 class="text-muted"><small><span class="bold"><?php echo $aval->SetorGer->getSetor().' '.$aval->ColabGer->getNome() ?></span> em <span class="bold"><?php echo fnFormatarData($aval->getDataUltSalva()) ?></span> escreveu:</small></h5>
<div class="text-muted bloquotes"><small><small><i>"<?php echo  nl2br($aval->getFeedCorr()) ?>"</i></small></small></div>
<?php } ?>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>


<divider></divider>
<h4 class="well well-sm bold"><img src="public/img/check.png" style="margin-right:5px" />Estabelecimento de Metas e Plano de Ação</h4>
<br>
<?php for($x=1;$x<=3;$x++) { ?>

<div class="bold">Plano de Ação <?php echo $x; ?>:</div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>
<div class="linha"></div>

<div style="width: 48%;float: left; padding-right: 2%;"><div class="bold">Requisitos</div></div>
<div style="width: 48%;float: left; padding-left: 2%;"><div class="bold">Passos</div></div>
<div style="clear: both"></div>
<div style="width: 48%;float: left; padding-right: 2%;">
    <?php echo $linha ?>
    <div class="linha"></div>
    <div class="linha"></div>
    <div class="linha"></div>
</div>
<div style="width: 48%;float: left; padding-left: 2%;">
    <?php echo $linha ?>
        <div class="linha"></div>
    <div class="linha"></div>
    <div class="linha"></div>
</div>
<div style="clear: both"></div>
<table width="100%">
    <tr>
        <td><div class="bold">Prazo</div></td>
        <td>&nbsp;<img src="public/img/unchecked.png" height="13" /> 1 Ciclo</td>
        <td>&nbsp;<img src="public/img/unchecked.png" height="13" /> 2 Ciclos</td>
        <td>&nbsp;<img src="public/img/unchecked.png" height="13" /> 3 Ciclos</td>
    </tr>
</table>


<?php if(($x+1) <= 3) { ?>
<divider></divider>
<?php }?>
  <?php }?>


 <?php if (count($aval->Acao->listar()) > 0) { ?>
 
    <?php foreach($aval->Acao->listar() as $plano) { ?>
    <divider></divider>
    <h5 class="bold"><?php echo $plano->getAcao() ?></h5>
    <div style="float:left;width: 50%">
        <div class="text-muted bold">Requisitos</div>
        <ul>
                <?php 
                    $lista = explode(";", $plano->getRequisito());
                    foreach($lista as $item) {
                ?>

                <li>
                    <small><small><?php echo $item ?></small></small>
                </li>
                
                <?php } ?>
                    
        </ul>
    </div>
    <div style="float:left;width: 50%">
        <div class="text-muted bold">Passos</div>
        <ul>
                <?php 
                    $lista = explode(";", $plano->getPassos());
                    foreach($lista as $item) {
                ?>

                <li>
                    <small><small><?php echo $item ?></small></small>
                </li>
                
                <?php } ?>


            </ul>
    </div>
    <br>
    <div>
        
        <div class="bold">
            <div class="div-td" style="width: 10%">
                <small>Ciclo</small>
            </div>
            
            <div class="div-td" style="width: 20%">
                <small>Status</small>
            </div>
            
            <div class="div-td" style="width: 50%">
                <small>Observação do Avaliador</small>
            </div>
            
            <div class="div-td" style="width: 20%">
                <small>Data da Observação</small>
            </div>
        </div>
        
        <?php 
            $acoes = $plano->Acao->listar();
            foreach($acoes as $acao) {
        ?>
        <div class="">
            <div class="div-td" style="width: 10%">
                <small><?php echo $acao->Ciclo->getNomeCiclo() ?></small>
            </div>
            
            <div class="div-td" style="width: 20%">
                <div class="<?php echo $acao->Status->getCor() ?>">
                    <small><small><?php echo $acao->Status->getDescricao() ?></small></small>
                </div>
            </div>
            
            <div class="div-td" style="width: 50%;font-size:10px">
                <i>
                <small id="d-obs<?php echo $acao->getIdCicloAcao(); ?>" >
                   <?php echo  nl2br($acao->getObservacao()); ?>
                </small>
                </i>
            </div>
            
            <div class="div-td" style="width: 20%">
             <small id="d-data<?php echo $acao->getIdCicloAcao(); ?>">
                <?php echo fnFormatarData($acao->getDataObservacao()); ?>
                </small>
            </div>
        </div>
        <?php } ?>
    </div>
    <br>
    <hr>
<?php } ?>
<?php } ?>
