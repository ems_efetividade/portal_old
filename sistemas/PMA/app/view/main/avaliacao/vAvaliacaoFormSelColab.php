<form id="formAvaliacao" method="post" action="<?php //echo APP_URL   ?>avaliacao/salvar">

    <div class="form-group">
        <label for="exampleInputEmail1">Colaborador </label>
        <select class="form-control" id="cmbColaborador">
            <option></option>
            <?php foreach($equipe as $rep) { ?>
            <option value="<?php echo $rep->getIdSetor().'|'.$rep->getIdColaborador() ?>"><?php echo $rep->getSetor().' '. $rep->getColaborador(); ?></option>
            <?php } ?>
        </select>
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Ciclo</label>
        <select class="form-control" id="cmbCiclo">
            <option></option>
            <?php //foreach($ciclos as $ciclo) { ?>
            <option value="<?php echo $ciclos->getIdCiclo() ?>"><?php echo $ciclos->getNomeCiclo(); ?></option>
            <?php //} ?>
        </select>
    </div>

</form>