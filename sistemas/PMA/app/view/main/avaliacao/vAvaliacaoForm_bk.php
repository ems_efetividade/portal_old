<?php include_once VIEW_PATH . 'main/vHeader.php';  ?>
<style>
    body {background-color: #g1g1g1;}
    label {font-size: 13px;}
    label {font-size: 12px;}
    .progress {margin-bottom: 0px;}
    .tab-pane {padding:10px;}
    .table {margin-bottom: 3px;}
    .carousel-control {background-color: none !important;opacity: none;width: 0;color:#000;}
    .carousel-control:hover {color:#000;}
    .carousel-control.left, carousel-control.right {background-image: none !important;}
    .badge {border-radius: 8px !important;}
    .hr {margin: 0px;margin-bottom: 5px;}
    .panel {margin-bottom: 4px;}
    .progress {position:relative;}
    .progress .span {position:absolute;left:0;width:100%;text-align:center;z-index:2;font-weight: bold;}
    .div-popover{max-height: 150px;overflow: auto}
    .pop {color:#ccc;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH . 'main/vMenu.php'); ?>
            
            
            
            <br>
            <?php if ( $cicloAtual->getIdCiclo() == $aval->getIdCiclo() && $aval->getIdStatus() != 2 ) { ?>

            
            
                <a href="#" class="btn btn-success btn-lg btn-block" id="btnSalvarAvaliacao"><span class="glyphicon glyphicon-floppy-disk"></span>  Salvar Avaliação</a>
                
                <div id="finalizar" style="display: none">
                    <p></p>
                <a href="#" class="btn btn-primary btn-lg btn-block" data-id="<?php echo $aval->getIdAvaliacao() ?>" id="btnFinalizarAvaliacao"><span class="glyphicon glyphicon-lock"></span>  Finalizar RAC</a>
                </div>
                
            <?php } else { ?>
                <a href="#" class="btnImprimir btn btn-primary btn-block" data-id="<?php echo $aval->getIdAvaliacao() ?>"><span class="glyphicon glyphicon-print"></span>  Imprimir</a>
            <?php } ?>
                
                <br>
                            <small>
            <div id="calendar" class="csollapse" >
            <?php 
                $calendarioMes = new mCalendario();
                echo $calendarioMes->gerarCalendarioMes();
            ?>
            </div>
            </small>
        </div>
        <div class="col-lg-10">
            <div class="row">
                <div class="col-lg-10">
                    <h3><span class="glyphicon glyphicon-calendar"></span> Relatório de Acompanhamento de Campo</h3>
                    <h4><small><?php echo $aval->SetorRep->getSetor() ?> <?php echo $aval->ColabRep->getNome() ?></small></h4>
                </div>
                <div class="col-lg-2">
                    <h5 class="text-center"><small>Preenchimento desta Avaliação</small></h5>
                    <div class="progress " style="height: 20px">
                    <div id="barra-total" class="progress-bar progress-bar-striped progress-bar-<?php echo $aval->getPorcentagem()['COR'] ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $aval->getPorcentagem()['VALOR'] ?>%;">
                        <small><span id="barra-total-valor" class="<?php echo (($aval->getPorcentagem()['VALOR'] <= 48) ? 'cor1' : 'cor2') ?>"><?php echo fnFormatarMoedaBRL($aval->getPorcentagem()['VALOR'],0) ?>%</span></small>
                    </div>
                  </div>
                </div>
            </div>

            <hr>
            <?php  if(count($dados['dados']) > 0) { ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="#" data-toggle="collapse" data-target="#dados-colab">
                        <b>Informações do Colaborador (Painel de Bordo) </b><span class="caret"></span>
                    </a>
                </div>
            </div>
            
            <div class="row collapse" id="dados-colab">
                <div class="col-lg-4" style="padding-right: 2px;">
                    <div class="panel panel-default">
                        <div class="panel-body" style="height:149px">
                            <div class="row">
                                <div class="col-lg-4">
                                    <img widsth="100%" hseight="100" class="img-responsive" src="<?php echo fnFotoColaborador($aval->ColabRep->getFoto()) ?>">
                                </div>
                                <div class="col-lg-8">
                                    <div><b><small><?php echo $aval->ColabRep->getNome() ?> </small></b></div>
                                    <div class="text-muted">
                                        <small><small><b>FUNÇÃO:</b> <?php echo $aval->SetorRep->getPerfil() ?></small></small>
                                    </div>
                                    <div class="text-muted">
                                        <small><small><b>SETOR:</b> <?php echo $aval->SetorRep->getSetor() ?> (<?php echo $aval->SetorRep->getNome() ?>)</small></small>
                                    </div>
  
                                    <div class="text-muted">
                                        <small><small><b>LINHA:</b> <?php echo $aval->SetorRep->getLinha() ?></small></small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <?php 
                        
                            $a[1]['ICONE'] = 'glyphicon glyphicon-thumbs-down';
                            $a[1]['COR'] = 'danger';
                            $a[2]['ICONE'] = 'glyphicon glyphicon-exclamation-sign';
                            $a[2]['COR'] = 'warning';
                            $a[3]['ICONE'] = 'glyphicon glyphicon-thumbs-up';
                            $a[3]['COR'] = 'success';
                            $a[4]['ICONE'] = 'glyphicon glyphicon-star';
                            $a[4]['COR'] = 'info';
                            //foreach($pontuacao as $i =>  $pont) { 
                            foreach($dados['dados'] as $i => $pont) {
                        ?>
                        
                        <div class="col-lg-6" style="<?php echo (($i%2) ? 'padding-left:2px' : 'padding-right:2px') ?>">
                            <div class="panel panel-default">
                                <div class="panel-body text-center alert-<?php echo $a[$pont->getPontuacao()]['COR'] ?>">
                                    <div><small><b><?php echo $pont->getPilar() ?></b></small></div>
                                    <h3 style="margin:5px">
                                        <span class="<?php echo $a[$pont->getPontuacao()]['ICONE'] ?>"></span>
                                        <?php echo $pont->getPontuacao() ?>
                                    </h3>
                                    
                                </div>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-lg-12" style="">
                            <div class="panel panel-default">
                                <div class="panel-body text-center">
                                    <h5 style="margin:5px" class="text-muted"><b>PONTUAÇÃO FINAL</b></h5>
                                    
                                    <div class="progress" style="height: 40px">
                                        <div class="progress-bar progress-bar-striped progress-bar-<?php echo fnCorPontuacao($pont->getMedia()) ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($pont->getMedia() / 4) * 100 ?>%;">
                                            <h3 style="margin:5px">
                                                <span class="span text-muted ">
                                                    <span class="<?php echo $a[floor($pont->getMedia())]['ICONE'] ?>"></span>
                                                    <?php echo $pont->getMedia(); ?>
                                                    
                                                </span>
                                            </h3>
                                        </div>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-8" style="padding-left: 2px;">
                    <div class="panel panel-default">
                        
                        <div class="panel-body">
                            
                            <div id="container" style="width: 699px; height: 270px;"></div>
                            
                            
                            <p></p>
                            <small><small>
                                    
                                    <table class="table table-condensed table-striped">
                                        
                                        <tr>
                                            <td class="text-muted"><b>PILARES</b></td>
                                            <?php $x = $dados['dados'][0]->getMeses();  for($i=13;$i>=1;$i--)  { ?>
                                            <td class="text-muted"><b><?php echo $x['M'.$i]; ?></b></td>
                                            <?php } ?>
                                        </tr>
                                        
                                        
                                        <?php 
                                            $cor[1] = 'text-success';
                                            $cor[0] = 'text-danger';
                                            foreach($dados['dados'] as $dado) { 
                                        ?>
                                        <tr>
                                            
                                            <td class="text-muted">
                                                <b><?php echo $dado->getPilar() ?></b>
                                            </td>
                                            
                                            <td class="text-center <?php echo $cor[$dado->getR1()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM1(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR2()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM2(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR3()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM3(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR4()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM4(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR5()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM5(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR6()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM6(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR7()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM7(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR8()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM8(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR9()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM9(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR10()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM10(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR11()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM11(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR12()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM12(),1) ?>%</b></td>
                                            <td class="text-center <?php echo $cor[$dado->getR13()] ?>"><b><?php echo fnFormatarMoedaBRL($dado->getM13(),1) ?>%</b></td>
                                        </tr>
                                        <?php } ?>
                                    </table>
                                    
                                    
                                    
                                    

                                    </small></small>
                            
                        </div>
                    </div>
                    
                    
                </div>
                
                
            </div>
        <?php } ?>
            <br>

            <form id="formAvaliacao" method="post" action="<?php echo APP_URL ?>avaliacao/salvar">

                <input type="hidden" name="txtIdAvaliacao" value="<?php echo $aval->getIdAvaliacao() ?>" />
                <input type="hidden" name="txtIdSetorGer"  value="<?php echo fnDadoSessao('id_setor') ?>" />
                <input type="hidden" name="txtIdColabGer"  value="<?php echo fnDadoSessao('id_colaborador') ?>" />
                <input type="hidden" name="txtIdColabRep"  value="<?php echo $aval->getIdColabRep() ?>" />
                <input type="hidden" name="txtIdSetorRep"  value="<?php echo $aval->getIdSetorRep() ?>" />
                <input type="hidden" name="txtIdCiclo"     value="<?php echo $aval->getIdCiclo() ?>" />

                <div>

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs " id="myTab" role="tablist">
                        
                        <li role="presentation" class="active">
                            <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" id="tabQuestao">
                                <b><ssmall><span class="glyphicon glyphicon-menu-right"></span> Análise Técnico/Comportamental</ssmall> </b>
                            </a>
                        </li>
                        
                        <li role="presentation">
                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
                                <b>
                                    <ssmall>
                                        <span class="glyphicon glyphicon-menu-right"></span>
                                        Análise do Painel de Bordo 
                                    </ssmall>
                                </b>
                            </a>
                        </li>
                        
                        <li role="presentation">
                            <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">
                                <b><ssmall><span class="glyphicon glyphicon-menu-right"></span> Feedback do Período</ssmall> </b>
                            </a>
                        </li>
                        
                        <li role="presentation" >
                            <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">
                                <b><ssmall><span class="glyphicon glyphicon-menu-right"></span> Estabelecimento de Metas e Plano de Ação </ssmall></b>
                            </a>
                        </li>
                        
                    </ul>
                    
                    <!-- Tab panes -->
                    <div class="tab-content">
                        
                        <div role="tabpanel" class="tab-pane fade in" id="home">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        
                                        <label for="exampleInputEmail1">Aspectos Positivos <a href="#" class="pop" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-html="true"  data-content="<div class='div-popover'><small class='text-muted'><?php echo $ultAval->getApectoPos() ?></small></div>"><span class="glyphicon glyphicon-comment"></span></a></label>
                                        <textarea class="form-control" rows="6" name="txtAspPos"><?php echo $aval->getApectoPos() ?></textarea>
                                        
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Principais Lacunas - �?reas de Desenvolvimento <a href="#" class="pop" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-html="true"  data-content="<div class='div-popover'><small class='text-muted'><?php echo nl2br($ultAval->getLacunas()) ?></small></div>"><span class="glyphicon glyphicon-comment"></span></a></label>
                                        <textarea class="form-control" rows="6" name="txtLacunas"><?php echo $aval->getLacunas() ?></textarea>
                                        <br>
                                    </div>
                                </div>

                            </div>

                        </div>
                        
                        <div role="tabpanel" class="tab-pane fade in  active" id="profile">

                            <div class="carousel slide" id="carousel-example-captions" data-wrap="false" data-interval="false" data-ride="carousel"> 

                                <div class="carousel-inner" role="listbox"> 

                                    <?php foreach ($modulos as $i => $modulo) { ?>
                                        <div class="item <?php echo (($i == 0) ? 'active' : '') ?>"> 
                                            <div class="div-table" style="padding-left:  18px;padding-right:  18px;">
                                                <table class="table table-cosndensed table-striped">

                                                    <tr>
                                                        <th style="vertical-align: middle; width:60%"><?php echo ($i + 1). '. ' . $modulo->getQuestao() ?> <span class="text-muted"> - (<?php echo ($i + 1).' de '.count($modulos) ?>)</span></th>
                                                        <?php foreach ($respostas as $resposta) { ?>
                                                        <th class="text-center"><small><?php echo $resposta->getDescricao() ?> </small></th>
                                                        <?php } ?>
                                                    </tr>

                                                    <?php foreach ($modulo->listarQuestao() as $u => $questao) { ?>
                                                        <tr>
                                                            <td><small><?php echo ($i + 1) . '.' . ($u + 1) . '. ' . $questao->getQuestao() ?></small></td>
                                                            <?php foreach ($respostas as $resposta) { ?>
                                                                <td style="vertical-align: middle;" class="text-center">

                                                                    <input type="radio" name="questao[<?php echo $questao->getIdQuestao() ?>][]" id="optionsRadios1" value="<?php echo $questao->getIdQuestao() ?>|<?php echo $resposta->getIdResposta() ?>" <?php echo (($aval->getQuestaoResposta($questao->getIdQuestao())) == $resposta->getIdResposta() ? 'checked' : '') ?>>
                                                                </td>
                                                            <?php } ?>
                                                        </tr>

                                                    <?php } ?>

                                                </table>
                                            </div>
                                        </div> 
                                    <?php } ?>

                                </div> 
                                
                                <a href="#carousel-example-captions" class="left carousel-control" role="button" data-slide="prev"> 
                                    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> 
                                    <span class="sr-only">Previous</span> 
                                </a> 
                                <a href="#carousel-example-captions" class="right carousel-control" role="button" data-slide="next"> 
                                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span> 
                                    <span class="sr-only">Next</span> 
                                </a> 
                                
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="messages">


                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Feedback Positivo <a href="#" class="pop" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-html="true"  data-content="<div class='div-popover'><small class='text-muted'><?php echo nl2br($ultAval->getFeedPos()) ?></small></div>"><span class="glyphicon glyphicon-comment"></span></a></label>
                                        <textarea class="form-control" rows="6" name="txtFeedPos"><?php echo $aval->getFeedPos() ?></textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">�?reas de Melhoria <a href="#" class="pop" data-container="body" data-trigger="focus"  data-toggle="popover" data-placement="top" data-html="true"  data-content="<div class='div-popover'><small class='text-muted'><?php echo nl2br($ultAval->getFeedCorr()) ?></small></div>"><span class="glyphicon glyphicon-comment"></span></a></label>
                                        <textarea class="form-control" rows="6" name="txtFeedCorr"><?php echo $aval->getFeedCorr() ?></textarea>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="settings">
                            
                            <?php if($aval->getIdStatus() != 2) { ?>
                            <p>
                                <button data-toggle="modal" id="btnAddAcao" data-target="#modalAcao" data-id="0" data-ciclo="<?php echo $aval->getIdCiclo() ?>" type="button" class="btn btn-success btn-sm"><span class="glyphicon glyphicon-plus"></span> Novo Plano de Ação</button>
                            </p>
                            <?php } ?>
                            
                            <div id="div-planos">
                                <?php echo $tabelaPlano ?>
                            </div>
                            
                        </div>
                    </div>

                </div>
            </form>



        </div>
    </div>
</div>




<div class="modal fade" id="modalAcao" tabindex="0" style="z-index: 100000" data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span>  Ação</h5>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarAcao"><span class="glyphicon glyphicon-trash"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalExcluir" tabindex="0" style="z-index: 100000" data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span>  Ação</h5>
      </div>
      <div class="modal-body text-center">
          <h3 class="text-danger">Deseja Excluir essa ação?</h3>
          <h5 class="text-muted">Essa operação não poderá ser desfeita!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger" id="btnExcluirAcao"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalFecharAvaliacao" tabindex="0" style="z-index: 100000" data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span>  Ação</h5>
      </div>
      <div class="modal-body text-center">
          <h4 class="text-primary">O relatório está 100% completo.</h4>
          <h4 class="text-primary"><b>Deseja salvar definitivamente esse relatório?</b></h4>
          <h5 class="text-muted">Essa operação não poderá ser desfeita e você não poderá mais alterar esse relatório!</h5>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarDef"><span class="glyphicon glyphicon-lock"></span> Finalizar Avaliação</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalAlertaSalvar" tabindex="0" style="z-index: 100000" data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span>  Ação</h5>
      </div>
      <div class="modal-body text-center">
          <h4 class="tfext-muted">Essa avaliação ainda não foi salva. Para adicionar planos de ação, por favor, salve a avaliação primeiro.</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalStatusAcao" tabindex="0" style="z-index: 100000" data-backdrop="true" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-ok"></span>  Status</h5>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarStatus"><span class="glyphicon glyphicon-trash"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>

<img src="" id="img-graf" />
<div id="asd"></div>

<script src="<?php echo EMS_URL ?>/plugins/highchart/js/highcharts.js"></script>


<script>

    if('<?php echo $dados['grafico'] ?>' != '') {

        
        var conf = {
                chart: {
                    defaultSeriesType: 'spline'
                },
                title: {
                    text: 'Evolução dos Pilares'
                },
                xAxis: {
                    labels: {},
                    categories: <?php echo $dadosProd['cabecalho'] ?>
                },
                    yAxis: [{
                            title: {
                                    text: 'Produtividade / Cob. Objetivo'
                            },
                            labels: {
                                format: '{value}',
                            },
                                },{
                                        title: {
                                                text: 'Presc. Share / Market Share'
                                        },
                            labels: {
                                format: '{value}',
                            },
                            opposite: true
                    }],
                    plotOptions: {
                        series:{
                                animation:true
                        },
                        line: {
                            dataLabels: {
                                enabled: true
                            },
                            enableMouseTracking: true
                        }
                    },
                    series: <?php echo $dados['grafico'] ?>
            }
        
        
        $('#container').highcharts(conf);
        
    }
</script>


<script>

    var EMS_URL = '<?php echo EMS_URL ?>';
    var APP_URL = '<?php echo APP_URL ?>';

$('.pop').popover();

    $('#btnTeste').click(function(e) {
        svgSize = $('#container').highcharts().getSVG();
        //$('#asd').html(x);
        
        
        var canvas = document.createElement('canvas');
        canvas.width = svgSize.width;
        canvas.height = svgSize.height;
        var ctx = canvas.getContext('2d');

        var img = document.createElement('img');
        img.setAttribute('src', 'data:image/svg+xml;base64,' + btoa(unescape(encodeURIComponent(svgData))));
        img.onload = function() {
            ctx.drawImage(img, 0, 0);
            window.open(canvas.toDataURL('image/png'));
        };
        
    });
    
    resizePanelLeft();
    
    $('#modalExcluir').on('show.bs.modal', function (e) {
        modal = $(this);
        var button = $(e.relatedTarget) // Button that triggered the modal
        var idAcao = button.data('id');
        
        $('#btnExcluirAcao').unbind().click(function(e) {
                
                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>acao/excluir',
                    data: {
                       txtIdAcao: idAcao
                    },
                    beforeSend: function (e) {
                        aguarde('show');
                    },
                    success: function (retorno) {
                        $(modal).modal('hide');
                        aguarde('hide');   
                        $('#div-planos').html(retorno);
                    }
                });
            });
        
    });
    
   
    
    $('#modalAcao').on('show.bs.modal', function (e) {
        var idAvaliacao = $('input[name="txtIdAvaliacao"]').val();
        var idColabRep  = $('input[name="txtIdColabRep"]').val();
        
        if(idAvaliacao == '') {
            $('#modalAlertaSalvar').modal('show');
            e.preventDefault();
        }
        
        
        modal = $(this);
        var button = $(e.relatedTarget) // Button that triggered the modal
        var idAcao = button.data('id');
        var idCiclo = button.data('ciclo');
        
        
        
        
        
      
        

        $.post('<?php echo APP_URL ?>avaliacao/formAcao/'+idAcao+'/'+idCiclo, function( data ) {
            
            $('.modal-body', modal).html(data);
            $('input[name="txtIdAvaliacao"]', modal).val(idAvaliacao);
            $('input[name="txtIdColabRep"]', modal).val(idColabRep);
            
            $('#btnSalvarAcao').unbind().click(function(e) {
                //$('#formAcao').submit();
                $.ajax({
                    type: "POST",
                    url: $('#formAcao').attr('action'),
                    data: $('#formAcao').serialize(),
                    beforeSend: function (e) {
                        aguarde('show');
                    },
                    success: function (retorno) {
                        $(modal).modal('hide');
                        aguarde('hide');   
                        $('#div-planos').html(retorno);
                       

                    }
                });
            });
        });
        
       
    });
    
    
    
    $('#modalStatusAcao').on('show.bs.modal', function (event) {
        modal = $(this);
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idAcao = button.data('id');
       // var idEvento = button.data('id');
       
       var idAvaliacao = $('input[name="txtIdAvaliacao"]').val();
      
        
        $.post('<?php echo APP_URL ?>acao/formStatusAcao/'+idAcao, function( data ) {
            
            $('.modal-body', modal).html(data);
            
            
            $('#btnSalvarStatus').unbind().click(function(e) {
                //$('#formAcao').submit();
                $.ajax({
                    type: "POST",
                    url: $('#formStatus').attr('action'),
                    data: $('#formStatus').serialize(),
                    beforeSend: function (e) {
                        aguarde('show');
                    },
                    success: function (retorno) {

                        dados = $.parseJSON(retorno);
                        
                        $('#d-cor'+idAcao).removeClass();
                        $('#d-icon'+idAcao).removeClass();
                        
                        $('#d-cor'+idAcao).addClass(dados.cor);
                        $('#d-icon'+idAcao).addClass(dados.icone);
                        $('#d-status'+idAcao).html(dados.status);
                        $('#d-obs'+idAcao).html(dados.obs);
                        $('#d-data'+idAcao).html(dados.data);
                        
                        
                        $(modal).modal('hide');
                        aguarde('hide');   
                        
                        

                    }
                });
            });
        });
        
       
    });
    
    
    /*
    $('#btnFinalizarAvaliacao').unbind().click(function(e) {
        var id = $(this).data('id');
        $('#modalFecharAvaliacao').modal('show');
        $('#btnSalvarDef').unbind().click(function(e){
            location.href = '<?php echo APP_URL ?>avaliacao/fecharAvaliacao/'+id;
        });
    });
    */
    
    if('<?php echo $aval->getPorcentagem()['VALOR'] ?>' == '100') {
        $('#finalizar').show();
        $('#btnFinalizarAvaliacao').unbind().click(function(e) {
            $('#modalFecharAvaliacao').modal('show');
            $('#btnSalvarDef').unbind().click(function(e){
                location.href = '<?php echo APP_URL ?>avaliacao/fecharAvaliacao/<?php echo $aval->getIdAvaliacao() ?>';
            });
        });
    }

    $('#btnSalvarAvaliacao').unbind().click(function (e) {

        $.ajax({
            type: "POST",
            url: $('#formAvaliacao').attr('action'),
            data: $('#formAvaliacao').serialize(),
            beforeSend: function (e) {
                aguarde('show');
            },
            success: function (retorno) {
                
                var dados = $.parseJSON(retorno);
                aguarde('hide');
                
                if(dados.error == '') {
                    
                    $('#barra-total').removeClass('progress-bar-primary');
                    $('#barra-total').removeClass('progress-bar-success');
                    $('#barra-total').addClass('progress-bar-'+dados.porc['COR']);
  
                    $('#barra-total').css('width', dados.porc['VALOR']+'%');
                    $('#barra-total-valor').html(dados.porc['LABEL']);
                    $('input[name="txtIdAvaliacao"]').val(dados.id);
                    
                    
                    
                    
                    if(dados.porc['VALOR'] == 100) {
                        
                        //alert(dados.porc['VALOR']);
                        $('#finalizar').fadeIn();
                        
                        $('#btnFinalizarAvaliacao').unbind().click(function(e) {
                            $('#modalFecharAvaliacao').modal('show');
                            $('#btnSalvarDef').unbind().click(function(e){
                                location.href = '<?php echo APP_URL ?>avaliacao/fecharAvaliacao/'+dados.id;
                            });
                        });
                        
                        //$('#modalFecharAvaliacao').modal('show');
                        //$('#btnSalvarDef').unbind().click(function(e){
                        //    location.href = '<?php echo APP_URL ?>avaliacao/fecharAvaliacao/'+dados.id;
                        //});
                    } else {
                        $('#finalizar').fadeOut();
                    }
                    
                    
                } else {
                    alert(dados.error);
                }
                
                
            }
        });


        //$('#formAvaliacao').submit();
    });
    
    /*
    $('#myTab a').click(function(e) {
        e.preventDefault();
        $(this).tab('show');
      });

      // store the currently selected tab in the hash value
      $("ul.nav-tabs > li > a").on("shown.bs.tab", function(e) {
        var id = $(e.target).attr("href").substr(1);
        window.location.hash = id;
      });

      // on load of the page: switch to the currently selected tab
      var hash = window.location.hash;
      $('#myTab a[href="' + hash + '"]').tab('show');
    
    */
function formatNumber (num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
</script>

<script src="<?php echo APP_URL ?>public/js/imprimirGraficoRAC.js"></script>
<script src="<?php echo APP_URL ?>public/js/calendario.js"></script>