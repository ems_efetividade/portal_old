<?php if (count($avaliacoes) == 0) { ?>
<br>
<div class="alert alert-warning">
    <h5>
        &nbsp; Nenhum relatório encontrado.
    </h5>
</div>

<?php } else { ?>

<table class="table table-condensed table-striped">
    <tr>
        <th><small>Ciclo</small></th>
        <th><small>Colaborador</small></th>
        <th><small>Avaliado Por:</small></th>
        <th><small>Data Últ. Alteração</small></th>
        <th><small>Status</small></th>
        <th><small>Preenchimento da Aval.</small></th>
        <th><small>Ação</small></th>
    </tr>

    <?php foreach ($avaliacoes as $aval) { ?>
        <tr>
            <td><small><?php echo $aval->Ciclo->getCiclo() . '/' . $aval->Ciclo->getAno() ?></small></td>
            <td><small><a href="<?php echo APP_URL ?>avaliacao/form/<?php echo $aval->getIdAvaliacao() ?>"><?php echo $aval->SetorRep->getSetor() . ' ' . $aval->ColabRep->getNome() ?></a></small></td>
            <td><small><?php echo $aval->SetorGer->getSetor() . ' ' . $aval->ColabGer->getNome() ?></small></td>
            <td><small><?php echo fnFormatarData($aval->getDataUltSalva()) ?></small></td>
            <td><small><?php echo ($aval->getIdStatus() == 1) ? '<span class="text-warning">'.$aval->Status->getDescricao().'</span>' : '<span class="text-success">'.$aval->Status->getDescricao().'</span>' ?></small></td>
            <td>
                <div class="progress " style="height: 20px">
                    <div class="progress-bar progress-bar-striped progress-bar-<?php echo $aval->getPorcentagem()['COR'] ?>" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $aval->getPorcentagem()['VALOR'] ?>%;">
                        <small><span class="<?php echo (($aval->getPorcentagem()['VALOR'] <= 48) ? 'cor1' : 'cor2') ?>"><?php echo $aval->getPorcentagem()['LABEL'] ?></span></small>
                    </div>
                </div>
            </td>
            <td>
<!--                <a href="#" class="btn btn-warning btn-xs btnImprimir" data-id="<?php echo $aval->getIdAvaliacao() ?>"><span class="glyphicon glyphicon-eye-open" ></span></a>-->
                <a href="#" class="btn btn-primary btn-xs btnImprimir" data-id="<?php echo $aval->getIdAvaliacao() ?>"><span class="glyphicon glyphicon-print" ></span></a>
                
            </td>
        </tr>
    <?php } ?>
</table>

<?php } ?>
<small>
<?php
echo $paginacao['HTML'];
?>
</small>
<script>
$('.paginacao').unbind().click(function(e) {
       var pag = $(this).attr('pag');
       $('input[name="txtPagina"]').val(pag);
       $.ajax({
            type: "POST",
            url: $('#formPesquisar').attr('action'),
            data: $('#formPesquisar').serialize(),
            beforeSend: function (e) {

            },
            success: function (retorno) {
                $('#listaAvaliacao').html(retorno);
            }
        });
    });
</script>