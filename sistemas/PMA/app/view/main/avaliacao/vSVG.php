<?php include_once VIEW_PATH . 'main/vHeader.php';  ?>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>

<script>
    Highcharts.chart('container', {
        chart: {
                            defaultSeriesType: 'spline'
                    },
                    title: {
                            text: 'Evolução dos Pilares'
                    },
                    xAxis: {
                    labels: {

                    },
                    categories: <?php echo $dadosProd['cabecalho'] ?>
            },
            yAxis: [{
                    title: {
                            text: 'Produtividade / Cob. Objetivo'
                    },
                    labels: {
            format: '{value}',
        },
            },{
                    title: {
                            text: 'Presc. Share / Market Share'
                    },
        labels: {
            format: '{value}',
        },
                    opposite: true
            }],
            plotOptions: {
                    series:{
                            animation:true
                    },
                    line: {
                            dataLabels: {
                                    enabled: true
                            },
                            enableMouseTracking: true
                    }
            },
            series: <?php echo $dados['grafico'] ?>

           
    });
    
    
    svgSize = $('#container').highcharts().getSVG();
    $('#svg').text(svgSize);
</script>


<div id="container" style="width: 100%; height: 400px; margin: 0 auto;display: none"></div>
<div id="svg"></div>