<?php 


?>
<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    .modal-dialog-center {
        margin-top: 25%;
    }
    .progress { margin-bottom: 0px; height: 15px;}
    #tbAgenda td {height: 120px; width: 14%; padding: 2px;}
    #tbAgenda h5 {margin:0px; margin-bottom:5px}

    .alert {margin-bottom:2px; padding:2px}
    .bg-primary p {margin-bottom: 2px; padding:3px}
    .transp { opacity: 0.5; }
    #tbAgenda a:hover {text-decoration: none}
    .bg-danger-soft {
        background-color: rgba(242, 222, 222, 0.4);
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH . 'main/vMenu.php'); ?>
            <br>
            <small>
            <div id="calendar" class="csollapse" >
            <?php 
                $calendarioMes = new mCalendario();
                echo $calendarioMes->gerarCalendarioMes();
            ?>
            </div>
            </small>


        </div>
        <div class="col-lg-10">





            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-calendar"></span> Agenda de Atividades</small></h3>





                </div>

                <div class="col-lg-4">

                </div>    

                <div class="col-lg-2">
                    <h3>
                        <select class="form-control input-sm" id="cmbCiclo">
                            <?php foreach ($ciclos as $ciclo) { ?>
                                <option <?php echo (($ciclo->getIdCiclo() == $cicloAtual->getIdCiclo()) ? "selected" : '') ?> value="<?php echo $ciclo->getIdCiclo() ?>"><b>Ciclo <?php echo $ciclo->getCiclo() . '/' . $ciclo->getAno() ?> </b></option>
                            <?php } ?>
                        </select>
                    </h3>
                </div>


            </div>

            <div class="row">
                <div class="col-lg-6">

                    <?php
                    $css['icon'] = 'thumbs-up';
                    $css['color'] = 'success';
                    $css['text'] = 'Você tem ' . $cicloAtual->getDiasRestantes() . ' dias para programar as atividades para o próximo ciclo';


                    if ($cicloAtual->getDiasRestantes() <= 5) {
                        $css['icon'] = 'exclamation-sign';
                        $css['color'] = 'danger';
                    }

                    if ($cicloAtual->getIdStatusCiclo() == 1) {
                        $css['icon'] = 'stop';
                        $css['color'] = 'muted';
                        $css['text'] = 'Ciclo fechado em ' . fnFormatarData($cicloAtual->getFim());
                    }

                    if ($cicloAtual->getIdStatusCiclo() == 3) {
                        $css['text'] = "Ciclo aberto para lançamentos!";
                    }
                    ?>

                    <div class="text-<?php echo $css['color'] ?>">
                        <span class="glyphicon glyphicon-<?php echo $css['icon'] ?>"></span>
                        <small><?php echo $css['text'] ?></small>
                    </div>
                </div>

                <div class="col-lg-6 text-right">

                    <?php
                    $css['icon'] = 'thumbs-up';
                    $css['color'] = 'success';
                    if ($cicloAtual->getDiasRestantes() <= 5) {
                        $css['icon'] = 'exclamation-sign';
                        $css['color'] = 'danger';
                    }
                    ?>

                    <div class="text-success">
<!--                        <small>Seu calendário está 97% concluído.</small>-->
                    </div>
                </div>
            </div>


            <hr>




            <div class="row">
                <div class="col-lg-4">

                    <p>
                        <select class="form-control input-sm" id="cmbSetor">
                            <option <?php echo (($idSetor == fnDadoSessao('id_setor')) ? 'selected' : '') ?> value="<?php echo fnDadoSessao('id_setor') ?>"><?php echo fnDadoSessao('setor') . ' ' . fnDadoSessao('nome') ?></option>
                            <?php foreach ($_SESSION['equipe'] as $setor) { ?>
                                <option <?php echo (($idSetor == $setor['ID_SETOR']) ? 'selected' : '') ?> value="<?php echo $setor['ID_SETOR'] ?>"><?php echo $setor['SETOR'] . ' ' . $setor['COLAB'] ?></option>
                            <?php } ?>
                        </select>
                    </p> 



                </div>

                <div class="col-lg-6">

                </div>

                <div class="col-lg-2">







                    <div class="pull-right">
                        <a class="btn btn-info btn-block btn-sm" id="btnImprimirAgenda" href="#"><span class="glyphicon glyphicon-print"></span> Imprimir Calendario</a>
                    </div>
                </div>


            </div>



            <table id="tbAgenda" class="table table-bordered">

                <tr class="active">
                    <th class="text-center"><small>Domingo</small></th>
                    <th class="text-center"><small>Segunda-Feira</small></th>
                    <th class="text-center"><small>Terça-Feira</small></th>
                    <th class="text-center"><small>Quarta-Feira</small></th>
                    <th class="text-center"><small>Quinta-Feira</small></th>
                    <th class="text-center"><small>Sexta-Feira</small></th>
                    <th class="text-center"><small>Sábado</small></th>

                </tr>
                <?php foreach ($calendario as $semana) { ?>
                    <tr>
                        <?php
                        foreach ($semana as $dia) {

                            $habilitarDia = 1;
                            $atividades = 0;

                            /* VERIFICA SE O DIA DO MES ESTÁ DENTRO DO CICLO ATUAL. SE ESTIVER, DESABILITA-O PARA LANCAMENTO */
                            if ($cicloAtual->dataNoCiclo($dia->getData()) == 1 && count($atividade[$dia->getData()]) > 0) {
                                $habilitarDia = 0;
                            }

                            if (isset($atividade[$dia->getData()])) {
                                $atividades = $atividade[$dia->getData()][0]->getAtividadesAtivas();
                            }
                            ?>
                            <td class="<?php echo (($atividades == 0) ? 'bg-danger-soft' : '') ?> <?php echo (($dia->getDiaUtil() == 0) ? 'active' : '') ?> <?php echo ((strtotime($dia->getData()) == strtotime(APP_DATE)) ? "bg-success" : '') ?>">

                                <div class="row">
                                    <div class="col-lg-8 text-left">
                                        <h5><small><?php echo (($dia->getDiaUtil() == 0) ? $dia->getFeriado() : '') ?></small></h5>
                                    </div>
                                    <div class="col-lg-4 text-right">
                                        <h5>

                                            <?php //if($cicloAtual->getIdStatusCiclo() <> 1 && $atividades == 0 && $dia->getDiaUtil() == 1) {  ?>
                                            
                                            <?php if (($cicloAtual->getIdStatusCiclo() == 2 && $atividades['A'] == 0 && $dia->getDiaUtil() == 1) && ($atividades['C'] != null) || ($cicloAtual->getIdStatusCiclo() == 3) && $dia->getDiaUtil() == 1 && ($idSetor == fnDadoSessao('id_setor'))) { ?>

                                                <a href="#" class="" data-toggle="modal" data-target="#modalEvento" data-data="<?php echo $dia->getData() ?>" data-id="0">


                                                    <p style="margin:0px;" class="">
                                                        <b><?php echo $dia->getDia() ?></b>
                                                    </p>
                                                    <p style="margin:0px;">
                                                        <small> 
                                                            <?php echo fnNomeMes($dia->getData(), 1) ?>
                                                        </small>
                                                    </p>
                                                </a>
                                            <?php } else { ?>


                                                <p style="margin:0px;">
                                                    <b>
                                                        <span class="text-muted">
                                                            <?php echo $dia->getDia() ?>
                                                        </span>
                                                    </b>
                                                </p>
                                                <p style="margin:0px;">
                                                    <b>
                                                        <small> 
                                                            <span class="text-muted">
                                                                <?php echo fnNomeMes($dia->getData(), 1) ?>
                                                            </span>
                                                        </small>
                                                    </b>
                                                </p>

                                            <?php } ?>
                                        </h5>
                                    </div>
                                </div>

                                <div style="max-hseight: 100px; ovserflow: auto">
                                    <?php
                                    if (isset($atividade[$dia->getData()])) {
                                        foreach ($atividade[$dia->getData()] as $ev) {
                                            ?>

                                            <div class="" data-toggle="modal" data-target="#modalResumo" data-id="<?php echo $ev->getIdAtividade() ?>">

                                                <a href="#"  data-toggle="tooltip" data-placement="top" title="<?php echo ($ev->Tipo->getAcomp() == 1) ? $ev->TipoTroca->getDescricao() : $ev->getObjetivo(); ?>">

                                                    <div class="<?php echo ($ev->Tipo->getAcomp() == 1) ? $ev->Status->getCor() : ($ev->Status->getIdStatus() != 1) ? $ev->Status->getCor() : 'alert alert-success'; ?>">
                                                        <p>
                                              
                                                            <small>
                                                                <small>
                                                                    <span class="<?php echo $ev->Tipo->getIcone() ?>"></span> 

                                                                    <?php echo substr(($ev->Tipo->getAcomp() == 1) ? $ev->SetorRep->getSetor() . ' ' . $ev->ColabRep->getNomeAbrev() : $ev->Tipo->getDescricao(), 0, 20); ?>
                                                                </small>
                                                            </small>
                        
                                                        </p>
                                                    </div>
                                                </a>

                                            </div>

                                        <?php
                                        }
                                    }
                                    ?>
                                </div>



                            </td>
    <?php } ?> 
                    </tr>

<?php } ?> 

            </table>

            <br>


        </div>
    </div>
</div>


<div class="modal fade" id="modalEvento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Atividade</h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnSalvarEvento"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalResumo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Resumo da Atividade</h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalJustificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-refresh"></span>  Trocar Atividade</h5>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnSalvarJustificativa"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddEventoJust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
            </div>
            <div class="modal-body">
                Deseja adicionar outro evento?
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Não</button>
                <button type="button" class="btn btn-primary" id="btnSalvarJustificativa"><span class="glyphicon glyphicon-ok"></span> Sim</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalExcluir" tabindex="0" style="z-index: 100000" data-backdrop="false" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header btn-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span>  Excluir Evento</h5>
            </div>
            <div class="modal-body">
                <h4 class="text-center text-muted">Deseja excluir esse evento?</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-danger" id="btnExcluirEvento"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="modalImprimir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header btn-primary">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-print"></span>  Dados da Atividade - Imprimir</h5>
            </div>
            <div class="modal-body" style="overflow: auto; max-height: 400px">
                <h4>Deseja trocar esse evento?</h4>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
                <button type="button" class="btn btn-primary" id="btnImprimirEvento" data-dismiss="modal"><span class="glyphicon glyphicon-print"></span> Imprimir</button>
            </div>
        </div>
    </div>
</div>

<?php include_once VIEW_PATH . 'main/export.php'; ?>

<script>


    var EMS_URL = '<?php echo EMS_URL ?>';
    var APP_URL = '<?php echo APP_URL ?>';



    $('#btnImprimirAgenda').click(function (e) {
        $('#modalAguarde').modal('show');
        location.href = '<?php echo APP_URL ?>atividade/imprimirCiclo/<?php echo $idSetor; ?>/<?php echo $cicloAtual->getIdCiclo() ?>';
            setTimeout(alertFunc, 5000);
                
            });
            
            function alertFunc() {
                $('#modalAguarde').modal('hide');
            }






            $('#tbAgenda a').click(function (e) {
                e.preventDefault();
            });

            resizePanelLeft();
            $('[data-toggle="tooltip"]').tooltip();

            $('#cmbCiclo, #cmbSetor').change(function (e) {
                var idSetor = $('#cmbSetor').val();
                var idCiclo = $('#cmbCiclo').val();


                window.location = '<?php echo APP_URL ?>calendario/abrirAgenda/' + idCiclo + '/' + idSetor;
            });

            $('#btnSalvarEvento').click(function (e) {
                $.ajax({
                    type: "POST",
                    url: $('#formAtividade').attr('action'),
                    data: $('#formAtividade').serialize(),
                    beforeSend: function (e) {

                    },
                    success: function (retorno) {
                        //alert(retorno);
                        //console.log(retorno);
                        if ($.trim(retorno) == "") {
                            location.reload();
                        } else {
                            $('#error-data').html(retorno);
                            $('#modalErro').modal('show');
                        }

                    }
                });
            });

            $('#modalEvento').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var dataEvento = button.data('data');
                var idEvento = button.data('id');

                modal = $(this);

                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>atividade/form/' + idEvento + '/' + dataEvento,
                    beforeSend: function (e) {

                    },
                    success: function (retorno) {
                        $('#modalEvento .modal-body').html(retorno);
                    }
                });
            });



            $('#modalJustificar').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget); // Button that triggered the modal
                //var dataEvento = button.data('data');
                var idEvento = button.data('id');



                modal = $(this);

                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>atividade/formTrocar/' + idEvento,
                    beforeSend: function (e) {

                    },
                    success: function (retorno) {
                        $('.modal-body', modal).html(retorno);

                        $('#btnSalvarJustificativa').unbind().click(function (e) {
                            $.ajax({
                                type: "POST",
                                url: $('#formJustificar').attr('action'),
                                data: $('#formJustificar').serialize(),
                                beforeSend: function (e) {

                                },
                                success: function (retorno) {
                                    if ($.trim(retorno) == "") {
                                        location.reload();
                                    } else {
                                        $('#error-data').html(retorno);
                                        $('#modalErro').modal('show');
                                    }
                                }
                            });
                        });
                    }
                });
            });


            $('#modalResumo').on('show.bs.modal', function (event) {
                modal = $(this);
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idEvento = button.data('id')

                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>atividade/resumo/' + idEvento,
                    beforeSend: function (e) {

                    },
                    success: function (retorno) {
                        $('.modal-body', modal).html(retorno);
                        
                        
                        
                        
                        $('#btnImprimir').unbind().click(function(e){
                              //  $('#modalResumo').modal('hide');
                                    
                             
                                $.ajax({
                                    type: "POST",
                                    url: '<?php echo APP_URL ?>atividade/visualizarImpressao/' + idEvento,
                                    beforeSend: function (e) {

                                    },
                                    success: function (retorno) {
                                        $('#modalImprimir .modal-body').html(retorno);
                                        $('#modalImprimir').modal('show');
                                        
                                        $('#btnImprimirEvento').unbind().click(function(e){
                                            
                                            $('#modalAguarde').modal('show');
                                            location.href = '<?php echo APP_URL ?>atividade/imprimir/'+idEvento;
                                            setTimeout(alertFunc, 5000);
                                            //setTimeout(function(){
                                            //    $('#modalAguarde').modal('hide');
                                            //  }, 2000);
                                                                                          
                                        });
                                        
                                    }
                                });
                                

                            });
                        
                        
                        
                        
                        
                    }
                });
            });

            $('#modalExcluir').on('show.bs.modal', function (event) {
                var button = $(event.relatedTarget) // Button that triggered the modal
                var idEvento = button.data('id')

                modal = $(this);

                $('#btnExcluirEvento').unbind().click(function (e) {
                    $.ajax({
                        type: "POST",
                        url: '<?php echo APP_URL ?>atividade/excluir/' + idEvento,
                        beforeSend: function (e) {

                        },
                        success: function (retorno) {
                            location.reload();
                        }
                    });
                });

            });


            $('.modal').on('hidden.bs.modal', function () {
                $('body').css('padding', '0px');
            });



$('.modal').on('hidden.bs.modal', function (event) {
    $('.modal-body', this).html('');
});

</script>
<script src="<?php echo APP_URL ?>public/js/imprimirGraficoRAC.js"></script>
<script src="<?php echo APP_URL ?>public/js/calendario.js"></script>
