<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    .modal-dialog-center {
    margin-top: 25%;
}
    .progress { margin-bottom: 0px; height: 15px;}
    #tbAgenda td {height: 120px; width: 14%; padding: 2px;}
    #tbAgenda h5 {margin:0px; margin-bottom:5px}
    
    .alert {margin-bottom:2px; padding:2px}
    .bg-primary p {margin-bottom: 2px; padding:3px}
    .transp { opacity: 0.5; }
    #tbAgenda a:hover {text-decoration: none}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH.'main/vMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            
            
            
            

            <div class="row">
                <div class="col-lg-6">
                    <h3><span class="glyphicon glyphicon-calendar"></span> Calendário Ciclo <?php echo $cicloAtual->getCiclo() ?><small> (<?php echo fnFormatarData($cicloAtual->getInicio()).' a '.fnFormatarData($cicloAtual->getFim())  ?>)</small></h3>
                </div>
                
                <div class="col-lg-4">
                  
                </div>    
                
                <div class="col-lg-2">
                    <h3>
                    <select class="form-control input-sm" id="cmbCiclo">
                        <?php foreach($ciclos as $ciclo) { ?>
                        <option <?php echo (($ciclo->getIdCiclo() == $cicloAtual->getIdCiclo()) ? "selected" : '') ?> value="<?php echo $ciclo->getIdCiclo() ?>"><b>Ciclo <?php echo $ciclo->getCiclo().'/'.$ciclo->getAno() ?> </b></option>
                        <?php } ?>
                    </select>
                        </h3>
                </div>
                
                
            </div>
            
            
            <hr>
            
            
            
            
            <div class="row">
                <div class="col-lg-4">
                    
                    <p>
                        <select class="form-control input-sm" id="cmbSetor">
                            <option <?php echo (($idSetor == fnDadoSessao('id_setor')) ? 'selected' : '') ?> value="<?php echo fnDadoSessao('id_setor') ?>"><?php echo fnDadoSessao('setor').' '.fnDadoSessao('nome') ?></option>
                            <?php foreach($equipe as $setor) { ?>
                            <option <?php echo (($idSetor == $setor->getIdSetor()) ? 'selected' : '') ?> value="<?php echo $setor->getIdSetor() ?>"><?php echo $setor->getSetor().' '.$setor->getColaborador() ?></option>
                            <?php } ?>
                        </select>
                    </p> 
                    
            
<!--                    <div class="btn-group">
                        <button type="button" class="btn-block btn btn-success" data-toggle="modal" data-target="#modalEvento" data-data="<?php echo APP_DATE ?>" data-id="0">
                            <span class="glyphicon glyphicon-plus"></span> Novo Evento
                        </button>
                        
                    </div>-->

              
                </div>
                <div class="col-lg-8">
                        <div class="pull-right">
                            <a class="btn btn-info btn-sm" href="<?php echo APP_URL ?>evento/imprimirCiclo/<?php echo $idSetor;  ?>/<?php echo $cicloAtual->getIdCiclo() ?>"><span class="glyphicon glyphicon-print"></span> Imprimir Calendario</a>
                        </div>
                </div>

                
            </div>
            
            

            
            
            <table id="tbAgenda" class="table table-bordered">
                
                <tr class="active">
                    <th class="text-center"><small>Domingo</small></th>
                    <th class="text-center"><small>Segunda-Feira</small></th>
                    <th class="text-center"><small>Terça-Feira</small></th>
                    <th class="text-center"><small>Quarta-Feira</small></th>
                    <th class="text-center"><small>Quinta-Feira</small></th>
                    <th class="text-center"><small>Sexta-Feira</small></th>
                    <th class="text-center"><small>Sábado</small></th>
                    
                </tr>
                
                <?php foreach($calendario as $semana) { ?>
                <tr>
                    <?php foreach($semana as $dia) { ?>
                    
                    <td class="<?php echo (($dia['DIA_UTIL'] == 0) ? "active" : '') ?> <?php echo ((strtotime($dia['DATA']) == strtotime(APP_DATE)) ? "bg-success" : '') ?>">
                        <div class="row">
                            <div class="col-lg-8 text-left">
                                <h5><small><?php echo (($dia['DIA_UTIL'] == 0) ? $dia['FERIADO'] : '') ?></small></h5>
                            </div>
                            <div class="col-lg-4 text-right">
                                <h5>
                                    <?php if( ( ($dia['DIA_UTIL'] == 1)   && ($cicloAtual->diaHabilitado($dia['DATA']) == 1) ) || (($evento->existeEvento($dia['DATA'], $idSetor) == 1) ) )  { ?>
                                    <a href="#" class="" data-toggle="modal" data-target="#modalEvento" data-data="<?php echo $dia['DATA'] ?>" data-id="0">
                                      <b><?php echo $dia['DIA'] ?><small> <?php echo $dia['MES_NOME'] ?></small></b>
                                    </a>
                                    <?php } else {?>
                                    <span class="text-muted"><b><?php echo $dia['DIA'] ?><small> <?php echo $dia['MES_NOME'] ?></small></b></span>
                                    <?php } ?>
                                </h5>
                            </div>
                        </div>
                    <?php 
                        $eventos = $evento->listarEventos($idSetor, $dia['DATA']);
                        foreach($eventos as $ev) {
                    ?>
                        
                        <div class="" data-toggle="modal" data-target="#modalResumo" data-id="<?php echo $ev->getIdEvento() ?>">
                            
                            <a href="#"  data-toggle="tooltip" data-placement="top" title="<?php echo ($ev->TipoEvento->getAcomp() == 1) ? $ev->Just->getDescricao() : $ev->getObjetivo(); ?>">
                                
                                <div class="<?php echo ($ev->TipoEvento->getAcomp() == 1) ? $ev->Status->getCor() : ($ev->Just->getIdJustificativa() != 0) ?  $ev->Status->getCor() : 'bg-primary'   ?>">
                                    <p>
                                        <b>
                                            <small>
                                                <small>
                                                    <span class="<?php echo $ev->Status->getIcone()  ?> "></span> 
                                                    
                                                    <?php echo  substr(($ev->TipoEvento->getAcomp() == 1) ?  $ev->SetorRep->getSetor().' '.$ev->ColabRep->getNomeAbrev() : $ev->TipoEvento->getDescricao(),0,20) ; ?>
                                                </small>
                                            </small>
                                        </b>
                                    </p>
                                </div>
                            </a>
                            
                        </div>
                        
                        <?php } ?>
                    </td>
                    <?php } ?>
                </tr>
                
                <?php } ?>
                
            </table>
            
            <br>
            
            
        </div>
    </div>
</div>


<div class="modal fade" id="modalEvento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarEvento"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalResumo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Dados do Eventos</h5>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalJustificar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarJustificativa"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
        <button type="button" class="btn btn-primary" id="btnSalvarJustificativa2"><span class="glyphicon glyphicon-ok"></span> Salvar e adicionar evento</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalAddEventoJust" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
      </div>
      <div class="modal-body">
          Deseja adicionar outro evento?
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Não</button>
        <button type="button" class="btn btn-primary" id="btnSalvarJustificativa"><span class="glyphicon glyphicon-ok"></span> Sim</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="modalExcluir" tabindex="0" data-backdrop="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span>  Excluir Evento</h5>
      </div>
      <div class="modal-body">
          <h2 class="text-center text-danger"><span class="glyphicon glyphicon-trash"></span> Deseja excluir esse evento?</h2>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger" id="btnExcluirEvento"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalErro" tabindex="-1" style="z-index: 100000"  data-backdrop="false" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-warning">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-info-sign"></span>  Atenção</h5>
      </div>
      <div class="modal-body">
          <h4 class="text-warning">
              <b>Houve alguns erros ao inserir os dados:</b>
          </h4>
          <span id="error-data"></span>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalSubstituir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
      </div>
      <div class="modal-body">
          <h4>Deseja trocar esse evento?</h4>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modalEvento" data-dismiss="modal" data-id="0" data-data="2014-10-10" id="btnSalvarSubstituir"><span class="glyphicon glyphicon-refresh"></span> Trocar Evento</button>
      </div>
    </div>
  </div>
</div>

<script>
    
     $('#tbAgenda a').click(function(e) {
         e.preventDefault();
    });
    
    resizePanelLeft();
    $('[data-toggle="tooltip"]').tooltip();
    
    $('#cmbCiclo, #cmbSetor').change(function(e) {
        var idSetor = $('#cmbSetor').val();
        var idCiclo = $('#cmbCiclo').val();
        
        
        window.location = '<?php echo APP_URL ?>evento/abrirAgenda/'+idCiclo+'/'+idSetor;
    });
    
    $('#btnSalvarEvento').click(function(e) {
        $.ajax({
            type: "POST",
            url: $('#formEvento').attr('action'),
            data: $('#formEvento').serialize(),
            beforeSend: function(e) {

            },

            success: function(retorno) {
                //console.log(retorno);
                if($.trim(retorno) == "") {
                    location.reload();
                } else {
                    $('#error-data').html(retorno);
                    $('#modalErro').modal('show');
                }
               
            }
        });
    });
    
    $('#modalEvento').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var dataEvento = button.data('data');
        var idEvento = button.data('id');
        
      
        
        modal = $(this);
        
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>evento/formEvento/'+idEvento+'/'+dataEvento,
            beforeSend: function(e) {

            },

            success: function(retorno) {
               $('#modalEvento .modal-body').html(retorno);
            }
        });
    });
    
    
    $('#modalSubstituir').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var idEvento = button.data('id');
        var dataEvento = button.data('data');
        
        
        
        $('#btnSalvarSubstituir').attr('data-data', dataEvento);
        
      
        
        $('#btnSalvarSubstituir').unbind().click(function(e) {
            
            $.ajax({
                type: "POST",
                url: '<?php echo APP_URL ?>evento/substituirEvento/'+idEvento,
                beforeSend: function(e) {

                },

                success: function(retorno) {
                    
                   
                }
            });
            
        });
        
        
    });
    
    $('#modalJustificar').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        //var dataEvento = button.data('data');
        var idEvento = button.data('id');
        
      
        
        modal = $(this);
        
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>evento/formJustificativa/'+idEvento,
            beforeSend: function(e) {

            },

            success: function(retorno) {
                $('.modal-body', modal).html(retorno);
               
                $('#btnSalvarJustificativa').unbind().click(function(e) {
                    $.ajax({
                        type: "POST",
                        url: $('#formJustificar').attr('action'),
                        data: $('#formJustificar').serialize(),
                        beforeSend: function(e) {

                        },

                        success: function(retorno) {
                           if($.trim(retorno) == "") {
                                location.reload();
                            } else {
                                $('#error-data').html(retorno);
                                $('#modalErro').modal('show');
                            }
                        }
                    });
                });
            }
        });
    });
    
    
    $('#modalResumo').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idEvento = button.data('id')
        
        modal = $(this);
        
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>evento/abrirEvento/'+idEvento,
            beforeSend: function(e) {

            },

            success: function(retorno) {
               $('#modalResumo .modal-body').html(retorno);
            }
        });
    });
    
    $('#modalExcluir').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var idEvento = button.data('id')
        
        modal = $(this);
        
        $('#btnExcluirEvento').unbind().click(function(e) {
            $.ajax({
                type: "POST",
                url: '<?php echo APP_URL ?>evento/excluirEvento/'+idEvento,
                beforeSend: function(e) {

                },

                success: function(retorno) {
                   location.reload();
                }
            });
        });
        
    });
    
    
    $('.modal').on('hidden.bs.modal', function () {
        $('body').css('padding', '0px');
    });
	
	
    
</script>