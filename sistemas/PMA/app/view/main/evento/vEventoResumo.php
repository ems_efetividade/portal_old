<?php


?>


<style>
    #modalResumo .tab-pane {
        padding-top: 10px;
        height: 150px;
    }
    
    #modalResumo .modal-body {
        padding:0px;
    }
    
    #modalResumo .navbar a:hover {
        background-color: #ddd;
    }
    
    #modalResumo .navbar {
        margin-bottom: 0px;
    }
    
    #modalResumo .navbar {
        min-height: 0px;
        border-top: 0px;
    }
    
    #modalResumo .navbar a {
        padding-top: 10px;
        padding-bottom: 10px;
        border-right: 1px solid #ddd;
    } 
    #modalResumo .navbar-collapse {
        padding-left: 0px;
    }
</style>


<nav class="navbar navbar-default">
  <div class="container-flsuid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <?php if($ciclo->diaHabilitado($evento->getDataEvento()) == 1) { ?>
        <li>
            <a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalEvento" data-data="<?php echo $evento->getDataEvento() ?>" data-id="<?php echo $evento->getIdEvento() ?>"><small><span class="glyphicon glyphicon-pencil"></span> Editar</small></a>
        </li>
        <li><a href="#" data-dissmiss="modal" data-toggle="modal" data-target="#modalExcluir" data-id="<?php echo $evento->getIdEvento() ?>"><small><span class="glyphicon glyphicon-trash"></span> Excluir</small></a></li>
        <?php } else { ?>
            <?php if ( ($evento->Ciclo->getCiclo() >= $ciclo->getCiclo()) && ($evento->getIdJustificativa() == 0)   ) { ?>
                <li><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalJustificar" data-id="<?php echo $evento->getIdEvento() ?>"><small><span class="glyphicon glyphicon-ban-circle"></span> Justificar</small></a></li>
<!--                <li><a href="#" data-dismiss="modal" data-toggle="modal" data-target="#modalSubstituir" data-id="<?php echo $evento->getIdEvento() ?>" data-data="<?php echo $evento->getDataEvento() ?>"><small><span class="glyphicon glyphicon-refresh"></span> Trocar Evento</small></a></li>-->
                
            <?php  }  ?>
        <?php  }  ?>
                
        
        <li><a href="<?php echo APP_URL ?>evento/imprimir/<?php echo $evento->getIdEvento() ?>"><small><span class="glyphicon glyphicon-print"></span> Imprimir</small></a></li>
        
        
      </ul>
      
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>




<div style="padding:15px;max-height: 300px; overflow: auto">
    
<!--    ACOMPANHAMENTO DE CAMPO-->
<?php if($evento->TipoEvento->getAcomp() == 1) { ?>    

    
<div class="row">
    
    <div class="col-lg-3 col-sm-4 col-md-2 col-xs-12">
        
                
                <div class="panel panel-default">
                    <div class="panel-body" style="padding:3px;">
                        <img src="<?php echo  fnFotoColaborador($evento->ColabRep->getFoto()) ?>" class="center-block img-responsive" />
                    </div>
                </div>
               
<!--                <img src="<?php echo APP_URL ?>public/img/calendar-512.png" style="padding:15px;" class="img-responsive" />-->
                
            </div>
        
    
    <div class="col-lg-9 col-sm-8 col-md-10 col-xs-12">
        <h4 class="hidden-xs"><b><?php echo $evento->SetorRep->getSetor().' '.$evento->ColabRep->getNome() ?></b></h4>
        <h5 class="hidden-lg hidden-md hidden-sm"><b><?php echo $evento->SetorRep->getSetor().' '.$evento->ColabRep->getNome() ?></b></h5>
        <p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo fnDataExtenso($evento->getDataEvento()); ?></p>
        
        
            <table class="table table-condensed">
               
                <tr>
                    <td width="22%" class="text-left"><small><b>End. Manhã:</b></small></td>
                    <td width="78%" class="text-justify"><small><span class="text-muted"><?php echo $evento->Endereco->getEnderecoManha(); ?></span></small></td>
                </tr>
                <tr>
                    <td><small><b>End. Tarde:</b></small></td>
                    <td class="text-justify"><small><span class="text-muted"><?php echo $evento->Endereco->getEnderecoTarde(); ?></span></small></td>
                </tr>

                <tr>
                    <td width="22%"><small><b>Objetivo:</b></small></td>
                    <td width="78%" class="text-justify"><small><span class="text-muted"><?php echo $evento->getObjetivo() ?></span></small></td>
                </tr>
                
                <tr>
                    <td width="22%"><small><b>Status:</b></small></td>
                    <td width="78%" class="text-left">
                    <div class="<?php echo $evento->Status->getCor() ?>"><small><span class="<?php echo $evento->Status->getIcone() ?>"></span> <?php echo $evento->Status->getDescricao().' '.(($evento->Just->getDescricao() != "") ? '('.$evento->Just->getDescricao().')' : '') ?></small></div>    
                    <small class="text-muted"><?php echo $evento->getMotivoJustificativa(); ?></small>
                    </td>
                </tr>
            </table>
 

    </div>

</div>
<!-- OUTRO EVENTO -->    
<?php } else { ?>

<div class="row">
    
    <div class="col-lg-3 col-sm-4 col-md-2 col-xs-12">
        <div class="panel panel-default">
            <div class="panel-body" style="padding:3px;">
                <img src="<?php echo APP_URL ?>public/img/calendar-512.png" style="padding:15px;" class="img-responsive" />
            </div>
        </div>
    </div>
        
    
    <div class="col-lg-9 col-sm-8 col-md-10 col-xs-12">
        <h4 class="hidden-xs"><b><?php echo $evento->TipoEvento->getDescricao() ?></b></h4>
        <h5 class="hidden-lg hidden-md hidden-sm"><b><?php echo $evento->TipoEvento->getDescricao() ?></b></h5>
        <p class="text-muted"><span class="glyphicon glyphicon-calendar"></span> <?php echo fnDataExtenso($evento->getDataEvento()); ?></p>
        
        
            <table class="table table-condensed">
               
                <tr>
                    <td width="22%"><small><b>Objetivo:</b></small></td>
                    <td width="78%" class="text-justify"><small><span class="text-muted"><?php echo $evento->getObjetivo() ?></span></small></td>
                </tr>
                
                <tr>
                    <td width="22%"><small><b>Status:</b></small></td>
                    <td width="78%" class="text-left">
                    <div class="<?php echo $evento->Status->getCor() ?>"><small><span class="<?php echo $evento->Status->getIcone() ?>"></span> <?php echo $evento->Status->getDescricao().' '.(($evento->Just->getDescricao() != "") ? '('.$evento->Just->getDescricao().')' : '') ?></small></div>    
                    <small class="text-muted"><?php echo $evento->getMotivoJustificativa(); ?></small>
                    </td>
                </tr>
            </table>
 

    </div>

</div>
    
<?php } ?>

          
</div>

<!--<p class="text-muted"><small><span class="glyphicon glyphicon-map-marker"></span>  <b>End. Manhã:</b> TRAVESSA FRANCISCO DE LEONARDO TRUDA - CENTRO HISTORICO - PORTO ALEGRE / RS</small></p>
        <p class="text-muted"><small><span class="glyphicon glyphicon-map-marker"></span>  <b>End. Tarde:</b> TRAVESSA FRANCISCO DE LEONARDO TRUDA - CENTRO HISTORICO - PORTO ALEGRE / RS</small></p>-->