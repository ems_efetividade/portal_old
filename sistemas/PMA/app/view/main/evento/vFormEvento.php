
<style>
    #tbMedico h5, #tbMedico p {
        margin:0px; margin-bottom:5px;
        
    }
    
    #modalEvento .tab-pane {
        padding-top: 10px;
        height: 310px;
    }
    
</style>
<?php $medicos = $evento->Agenda->listar(); ?>
<form id="formEvento" method="POST" action="<?php echo APP_URL ?>evento/salvar">
    
    <input type="hidden" name="txtIdEvento" value="<?php echo $evento->getIdEvento() ?>"/>
    <input type="hidden" name="txtIdSetorGer" value="<?php echo fnDadoSessao('id_setor') ?>"/>
    <input type="hidden" name="txtIdColabGer" value="<?php echo fnDadoSessao('id_colaborador') ?>"/>
    <input type="hidden" name="txtIdSetorRep" class="habilitar" value="<?php echo $evento->getIdSetorRep() ?>"/>
    <input type="hidden" name="txtIdColabRep" class="habilitar" value="<?php echo $evento->getIdColaboradorRep() ?>"/>
    <input type="hidden" name="txtDataEvento" value="<?php echo $dataEvento ?>"/>
    
    
<div class="row">

    <div class="col-lg-8">

        <h4 style="margin-top:5px">
            <b><span class="glyphicon glyphicon-calendar"></span> <span id="diaExtenso"><?php echo $dataExtenso ?></span></b>
                <!-- Single button -->
            <div class="btn-group">
                <span class="dropdown-toggle caret" data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample"></span>
            </div>
                
                
        </h4>
        <div class="collapse" id="collapseExample" style="position:absolute;z-index: 1000;background: #fff;">
            
                        <table id="tbCiclo" class="table table-bordered tsable-condensed">
                            <tr>
                                <th colspan="7" class="active text-center">Ciclo <?php echo $ciclo->getCiclo() ?></th>
                            </tr>
                    <tr class="active">
                        <th class="text-center"><small>D</small></th>
                        <th class="text-center"><small>S</small></th>
                        <th class="text-center"><small>T</small></th>
                        <th class="text-center"><small>Q</small></th>
                        <th class="text-center"><small>Q</small></th>
                        <th class="text-center"><small>S</small></th>
                        <th class="text-center"><small>S</small></th>

                    </tr>

                    <?php foreach($calendario as $semana) { ?>
                    <tr>
                        <?php foreach($semana as $dia) { ?>
                        <td class="text-muted">
                            <small>
                                <a href="#" data-data="<?php echo $dia['DATA'] ?>"><?php echo $dia['DIA'].'/<small>'.$dia['MES_NOME'].'</small>';  ?></a>
                            </small></td>
                        <?php } ?>
                    </tr>
                    <?php } ?>
                        </table>
                </div>
        
    </div>
    
</div>
    <hr style="margin-bottom:10px;margin-top:10px">

<div>

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
      <li role="presentation" class="active">
          <a href="#home" aria-controls="home" role="tab" data-toggle="tab">
              <b><small><span class="glyphicon glyphicon-pencil"></span> Dados do Evento</SMALL></b>
          </a>
      </li>
      <li role="presentation">
          <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">
              <b><SMALL><span class="glyphicon glyphicon-user"></span> Agenda Médica (<span class="total-medico"><?php echo count($medicos) ?></span>)</small></b>
          </a>
      </li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
      <div role="tabpanel" class="tab-pane active" id="home">
          
          
          
          
          <div class="row">
              <div class="col-lg-6">
                  <div class="row">
                      <div class="col-lg-12">
                          <div class="form-group form-group-sm">
                            <label for="exampleInputEmail1"><small>Tipo de Evento</small></label>
                            <select class="form-control" name="cmbTipoEvento">
                                <option value="0"></option>
                                <?php foreach($tipoEvento as $tipo) { ?>
                                <option <?php echo (($tipo->getIdTipoEvento() == $evento->getIdTipoEvento()) ? 'selected' : '') ?> value="<?php echo $tipo->getIdTipoEvento() ?>" bloq="<?php echo $tipo->getAcomp() ?>"><?php echo $tipo->getDescricao(); ?></option>
                                <?php } ?>
                            </select>
                            </div> 
                        </div>
                      
                      <div class="col-lg-12">
                        <div class="form-group form-group-sm">
                            <label for="exampleInputEmail1"><small>Objetivo</small></label>
                            <textarea class="form-control" maxlength="2000" rows="10" name="txtObjetivo"><?php echo $evento->getObjetivo() ?></textarea>
                        </div>          
                    </div>
                      
                  </div>
              </div>
              <div class="col-lg-6">
                  <div class="row">
    
        <div class="col-lg-12">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group form-group-sm">
                        <label for="exampleInputEmail1"><small>Colaborador</small></label>
                        <input type="text" class="form-control habilitar" id="txtColaborador" autocomplete="off" name="txtColaborador" value="<?php echo trim($evento->SetorRep->getSetor().' '.$evento->ColabRep->getNome()) ?>">
                        
                        <div id="pre" style="position: absolute;z-index: 1000; width: 93%;display: none; max-height: 200px; overflow: auto">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group form-group-sm">
                        
                        

                        
                        <label for="exampleInputEmail1"><small>Endereço Manha</small></label>
                        <textarea class="form-control habilitar" rows="3" name="txtEndManha"><?php echo $evento->Endereco->getEnderecoManha() ?></textarea>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="form-group form-group-sm">
                        <label for="exampleInputEmail1"><small>Endereço Tarde</small></label>
                        <textarea class="form-control habilitar" rows="3" name="txtEndTarde"><?php echo $evento->Endereco->getEnderecoTarde() ?></textarea>
                    </div>        
                </div>
                
                <div class="col-lg-12">
                    <div class="form-group form-group-sm">
                    <div class="checkbox" style="margin:0px">
                            <label>
                                <input type="checkbox" value="1" name="chkAtualizarEnd" class="habilitar" <?php echo (($evento->getAtualizarEndereco() == 1) ?  'checked' : '') ?>>
                              <small class="text-muted"><small>Copiar endereço do primeiro médico de cada perído.</small></small>
                            </label>
                          </div>
                        </div>     
                </div>
                
       
            </div>
            
        </div>
    </div>  
              </div>
          </div>
          
          
          
          
          
      </div>
      <div role="tabpanel" class="tab-pane" id="profile">
          <div id="divMed" style="max-height: 305px;overflow: auto; ">

                    
                    
                    <table id="tbMedico" class="table table-striped table-condensed">
                        
                        <?php foreach($medicos as $medico) { ?>
                        <tr class="text-muted">
                            <td><small><?php echo $medico->getPeriodoVisita().$medico->getOrdemVisita(); ?></small></td>
                            <td><small><?php echo $medico->getCRM().' '.$medico->getNome(); ?></small></td>
                        </tr>
                        <?php } ?>
                    </table>
                    
                </div>
      </div>
      
    <div role="tabpanel" class="tab-pane" id="settings">...</div>
  </div>

</div>



    
</form>    

<script>

    $('#tbMedico a').click(function(e) {
        e.preventDefault();
    });
    
    $('#tbCiclo a').click(function(e) {
        dia = $(this).data('data');
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>evento/dataExtenso/'+dia,
            
            beforeSend: function(e) {

            },
            success: function(retorno) {
                $('input[name=txtDataEvento]').val(dia);
                $('#diaExtenso').html(retorno);
                $('#collapseExample').collapse('hide');
                
                gerarAgenda();
            }
        });
    });

    $('select[name="cmbTipoEvento"]').change(function(e) {
        $('.habilitar').removeAttr('disabled');
        v = $('option:selected', this).attr('bloq');
        
        if(v == 0) {
            $('.habilitar').attr('disabled', 'disabled');
        }
    });

    $('#txtColaborador').keyup(function(e) {
        clearTimeout($(this).data('timer'));
        $('#pre').hide();
    
        criterio = $(this).val();

        

        if (criterio.length >= 2) {
            $(this).data('timer', setTimeout(function() {
                $.ajax({
                    type: "POST",
                    url: '<?php echo APP_URL ?>evento/pesquisarRep',
                    data: {
                        txtCriterio: criterio
                    },
                    beforeSend: function(e) {

                    },
                    success: function(retorno) {



                        if(retorno =='') {
                            $('#pre').hide();
                        } else {
                            $('#pre .panel-body').html(retorno);
                            $('#pre').fadeIn(100);

                            $('#pre a').click(function(e) {
                                
                                valor = $(this).attr('cod').split('|');
                                
                                $('input[name="txtIdSetorRep"]').val(valor[0]);
                                $('input[name="txtIdColabRep"]').val(valor[1]);
                                
                                
                                $('#txtColaborador').val($(this).text());
                                $('#pre').hide();
                                
                                gerarAgenda();
                            });
                        }

                    }
                });
            }, 300));
        }
    });
    
    function gerarAgenda() {
        
        idSetor = $('input[name="txtIdSetorRep"]').val();
        dataVisita = $('input[name="txtDataEvento"]').val();
    
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>agenda/listarAgenda/'+idSetor+'/'+dataVisita,
            beforeSend: function(e) {

            },
            success: function(retorno) {
                dados = $.parseJSON(retorno);
                
                $('#tbMedico').html(dados.tabela);
                $('.total-medico').html(dados.total);
            }
        });
    }

</script>