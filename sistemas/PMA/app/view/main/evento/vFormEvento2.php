<?php include_once VIEW_PATH . 'main/vHeader.php'; ?>

<style>
    
    .progress { margin-bottom: 0px; height: 15px;}
    #tbAgenda td {height: 120px; width: 14%}
    #tbAgenda h5 {margin:0px; margin-bottom:5px}
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once(VIEW_PATH.'main/vMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            
            <div class="row">
                <div class="col-lg-9">
                    <h3><span class="glyphicon glyphicon-calendar"></span> Agenda de Eventos - Quinta-Feira, 30 de Março de 2017</h3>
                </div>
                
                <div class="col-lg-3">
                   
                </div>                
            </div>
            
            
            <hr>
         

            <form>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Colaborador</label>
                            <input type="text" class="form-control" name="txtColaborador">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Endereço Manha</label>
                            <textarea class="form-control" rows="2" name="txtObjetivo"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Endereço Tarde</label>
                            <textarea class="form-control" rows="2" name="txtObjetivo"></textarea>
                        </div>        
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Objetivo</label>
                            <textarea class="form-control" rows="3" name="txtObjetivo"></textarea>
                        </div>          
                    </div>
                    <div class="col-lg-12">

                        

                        <p><strong>Médicos para esse dia</strong></p>
                                <table class="table table-striped">
                                    <tr>
                                        <th>Médicos do Dia</th>
                                    </tr>
                                    <?php for($i=1;$i<=10;$i++) { ?>
                                <tr>
                                    <td><small><strong>ABRELINO ARPINI</strong></small></td>
                                    <td><small>RUA DOS ANDRADAS, 1781, CENTRO HISTORICO, 90020013</small></td>
                                    <td><small>PORTO ALEGRE</small></td>
                                    <td><small>RS</small></td>
                                    </tr>
                                    <?php } ?>

                                </table>
                         


                    </div>
                </div>
            </form>    
        </div>
    </div>
</div>


<div class="modal fade" id="modalEvento" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-calendar"></span>  Adicionar/Editar Eventos</h5>
      </div>
      <div class="modal-body">
       
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>

<script>
    resizePanelLeft();
    
    $('#cmbCiclo').change(function(e) {
        window.location = '<?php echo APP_URL ?>evento/abrirAgenda/'+$(this).val();
    });
    
    
    $('#modalEvento').on('show.bs.modal', function (event) {
        
        modal = $(this);
        
        $.ajax({
            type: "POST",
            url: '<?php echo APP_URL ?>evento/formEvento/',
            beforeSend: function(e) {

            },

            success: function(retorno) {
               $('#modalEvento .modal-body').html(retorno);
            }
        });
    });
    
</script>