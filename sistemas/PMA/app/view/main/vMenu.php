<style>
    
    
    
    .main-menu {
        margin: 0 -15px;
    }
    
    .main-menu li {
        --text-align: center;
    }
   
    .main-menu a {
        color: #4f5b5e;
        color: #fff;
        text-decoration: none;
    }
    .main-menu a:hover {
        background: #bbb;
        color: #000;
    }
    
    .main-menu a {
        position: relative;
        display: block;
        padding: 10px 15px
    }
    
    .main-menu .glyphicon {
        padding-right: 13px;
    }
</style>
<br>
<img class="center-block" height="90" src="<?php echo EMS_URL ?>\\..\\public\img\iconsFerramentas\logoEfet.png" alt="">
<h4 class="text-center" style="color:#fff">
    <small><?php echo APP_NAME ?></small>
</h4>
<hr>



<small>
<ul class="main-menu list-unstyled">
<!--    <li>
        <a href="<?php echo APP_URL ?>evento"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a>
    </li>-->
    
    <li>
        <a href="<?php echo APP_URL ?>calendario"><span class="glyphicon glyphicon-calendar"></span>Agenda de Atividades</a>
    </li>
    
    <li>
        <a href="<?php echo APP_URL ?>avaliacao"><span class="glyphicon glyphicon-file"></span> Relatorio de Acompanhamento</a>
    </li>
    
    
    
<!--    <li>
        <a data-toggle="collapse" href="#calendar"><span class="glyphicon glyphicon-calendar"></span> Calendário</a>
    </li>    -->
</ul>
    </small>


<script>
$('.btn-calendar').unbind().click(function(e) {
    e.preventDefault();
    $.post('<?php echo APP_URL ?>calendario/getCalendarioMes/'+$(this).data('month'), function( data ) {
        $('#calendar').html(data);
        getCalendar();
    });
});

function getCalendar() {
    $('.btn-calendar').unbind().click(function(e) {
        e.preventDefault();
        $.post('<?php echo APP_URL ?>calendario/getCalendarioMes/'+$(this).data('month'), function( data ) {
            $('#calendar').html(data);
            getCalendar();
        });
    });
}
</script>