<?php

class View {
    
    public $appDados = array();
    
    public function set($nome, $valor) {
        $this->appDados[$nome] = $valor;	
    }
    
    public function get($nome='') {

        if ($nome == '') {
            return $this->appDados;
        }
        else {
            if (isset($this->appDados[$nome]) && ($this->appDados[$nome] != '')) {
                return $this->appDados[$nome];
            }
            else {
                return '';
            }
        }
    }    
    
    public function render($arquivo) {
        foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }

        if (file_exists(VIEW_PATH."{$arquivo}.php")) {
            include VIEW_PATH."{$arquivo}.php";
        } else {
            echo 'O arquivo '.VIEW_PATH.$arquivo.'.php não existe';
        }
    }    
    
    
    public function renderToString($arquivo) {
        
        foreach($this->get() as $chave => $item) {
                $$chave = $item;
            }

            if (file_exists(VIEW_PATH."{$arquivo}.php")) {
                ob_start();
                include(VIEW_PATH."{$arquivo}.php");
                return ob_get_clean();
            }
		
    }
    
    public function renderToString2($arquivo) {
        
        foreach($this->get() as $chave => $item) {
                $$chave = $item;
            }

            if (file_exists(VIEW_PATH."{$arquivo}.php")) {
                ob_start();
                include(VIEW_PATH."{$arquivo}.php");
                $var = ob_get_contents(); 
                ob_end_clean();
                return $var;
            }
		
	}
}