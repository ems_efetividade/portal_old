<?php

require_once('../../plugins/mpdf57/mpdf.php');


function fnPaginacao($total=0, $maxPorPagina=10, $pagina=0) {
    $paginas = ceil($total / $maxPorPagina);
    
    $paginaAtual = ($pagina == 0) ? 1 : $pagina;
    
    $registro = (($paginaAtual * $maxPorPagina) - $maxPorPagina);
    
    $inicio = $registro + 1;
    $fim =  ($pagina * $maxPorPagina);
    
    $retorno['TOTAL'] = $total;
    $retorno['PAGINAS']   = $paginas;
    $retorno['PAG_ATUAL'] = $paginaAtual;
    $retorno['INICIO']    = $inicio;
    $retorno['FIM']       = $fim;
    $retorno['REGISTRO']  = $registro;
    
    
    $html = '<nav aria-label="Page navigation" >
                <ul class="pagination" style="margin:0px">';
    
    
    for($i=1;$i<=$paginas;$i++) {
        $sel = ($i == $pagina) ? '<span class="sr-only">(current)</span>' : '';
        $html .= '<li class="'.(($sel != '') ? 'active' : '').'"><a href="#" class="paginacao" pag="'.$i.'">'.$i.$sel.'</a></li>';
    }
    
    $html .=    '</ul>
      </nav>';
    
    
    $retorno['HTML'] = $html;
    
    return $retorno;
}


function fnDadoSessao($chave) {
    @session_start();
    return $_SESSION[$chave];
}



function fnJustificar($data) {
    $calendario = new mCalendario();    
    $dataMax = strtotime($calendario->getLimiteJustificar());
    $dataA = strtotime($data);
    return ($dataA >= $dataMax) ? 1 : 0;
}

function fnFormatarData($data) {
    if($data != "") {

        $dataHora = explode(" ", $data);

        if(count($dataHora) > 0) {
            $campos = explode("-", $dataHora[0]);
            $hora = explode(".", $dataHora[1]);
        } else {
            $campos = explode("-", $data);
            $hora = '';
        }

        if(count($campos) == 1) {
                $campos = explode("/", $data);
                return trim($campos[2])."-".trim($campos[1])."-".trim($campos[0]);
        } else {
                return trim($campos[2])."/".trim($campos[1])."/".trim($campos[0]).' '.$hora[0];
        }
    }
}

function fnFormatarMoedaBRL($valor=0, $decimal=2) {
    $valor  = ($valor != null) ? $valor : 0;
    return number_format($valor, $decimal, ",", ".");
}

function fnFormatarMoedaSQL($valor) {
    return str_replace(",", ".", str_replace(".", "", $valor));
}

function fnFormatarURL($url) {
    return strtolower(str_replace(' ', '-', $url));
}

function fnNomeMes($data='', $abrev=0) {
    $mes['01']  = "Janeiro";
    $mes['02']  = "Fevereiro";
    $mes['03']  = "Março";
    $mes['04']  = "Abril";
    $mes['05']  = "Maio";
    $mes['06']  = "Junho";
    $mes['07']  = "Julho";
    $mes['08']  = "Agosto";
    $mes['09']  = "Setembro";
    $mes['10'] = "Outubro";
    $mes['11'] = "Novembro";
    $mes['12'] = "Dezembro";
    
    $arr = explode('-', $data);
    
    return ($abrev == 0) ? $mes[$arr[1]].'/'.$arr[0] : substr($mes[$arr[1]],0,3);
}

function fnFotoColaborador($foto='') {
    $caminho = EMS_URL.'/public/profile/';
    $arqFoto = EMS_URL.'/public/img/exemplo_foteoo.jpg';
    
    
    if($foto != '') {
        if (file_exists(EMS_FOTO_PROFILE.$foto)) {
            $arqFoto = $caminho.$foto;
        } 
    }
    
    return $arqFoto;
}

function fnDataExtenso($data='') {
    $nomeMes[1]  = "Janeiro";
    $nomeMes[2]  = "Fevereiro";
    $nomeMes[3]  = "Março";
    $nomeMes[4]  = "Abril";
    $nomeMes[5]  = "Maio";
    $nomeMes[6]  = "Junho";
    $nomeMes[7]  = "Julho";
    $nomeMes[8]  = "Agosto";
    $nomeMes[9]  = "Setembro";
    $nomeMes[10] = "Outubro";
    $nomeMes[11] = "Novembro";
    $nomeMes[12] = "Dezembro";


    $nomeSemana[0] = 'Domingo';
    $nomeSemana[1] = 'Segunda-Feira';
    $nomeSemana[2] = 'Terça-Feira';
    $nomeSemana[3] = 'Quarta-Feira';
    $nomeSemana[4] = 'Quinta-Feira';
    $nomeSemana[5] = 'Sexta-Feira';
    $nomeSemana[6] = 'Sábado';
    
    
    $arr = explode("-", $data);
    $week = getdate(mktime(0, 0, 0, $arr[1], $arr[2], $arr[0]));
    $semana = $week['wday'];
    
    $arr = explode("-", $data);
        
    return $nomeSemana[$semana].', '.$arr[2].' de '.$nomeMes[(int)$arr[1]].' de '.$arr[0];
}

function fnCorPontuacao($dado) {
    $cor = '';
    if($dado <= 1.49) {
            $cor = 'danger';
        } else {
            if($dado >= 1.50 && $dado <= 2.49) {
                $cor = 'warning';
            } else {
                if ($dado >= 2.50 && $dado <= 3.49) {
                    $cor = 'success';
                } else {
                    if($dado >= 3.50 && $dado <= 3.99) {
                        $cor = 'info';
                    } else {
                        $cor = 'primary';
                    }
                }
            }
        }
        
    return $cor;
}

function fnGerarPDF($html='', $nomeArquivo='') {
        

    ob_start();
    $html = urldecode($html);
    $nome_arquivo = $nomeArquivo;
    $arquivo_css = $css;

    //$stylesheet = file_get_contents('css/css_imprimir_print.css');
    //$pdf = new mPDF('', 'A4', 0, 'Arial', 7, 7, 7, 15, 7, 10, 'P');
    $pdf = new mPDF('','A4','','','15','15','30','16','9', '9');
    $pdf->allow_charset_conversion=true;
    $pdf->showImageErrors = false;
    $pdf->charset_in='iso-8859-1';
    $pdf->debug = false;
    $pdf->SetDisplayMode('fullpage');
    $pdf->SetFooter('{DATE d/m/y H:i:s}||{PAGENO}');
    //$pdf->WriteHTML($stylesheet,1);
    if($arquivo_css != "") {
            $stylesheet = file_get_contents(appConf::caminho.'public/css/'.$arquivo_css);
            $pdf->WriteHTML($stylesheet,1); // The parameter 1 tells that this is css/style only and no body/html/text
    }
    
    $pages = explode('<divider></divider>', $html);
    
    
    
    foreach($pages as $page) {
        if($page != '') {
            $pdf->addPage();
            $pdf->WriteHTML($page);
        }
        
    }
    $pdf->Output($nome_arquivo.'.pdf', 'D');
    
    $_SESSION['dld'] = 0;
    
}