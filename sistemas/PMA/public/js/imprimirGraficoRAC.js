$('.btnImprimir').unbind().click(function (e) {

    e.preventDefault();
    aval = $(this).data('id');
    $.ajax({
        type: "POST",
        url: APP_URL+'avaliacao/getSVG/' + aval,
        dataType: 'json',
        beforeSend: function() {
           
        },
        success: function (data) {
            
            theImage = null;
            rnk = {
                obj1: null,
                obj2: null
            } 
            if(data.grafico.DADOS != '[]') {
                a = gerarGrafico(data.grafico);
                rnk  = gerarGraficoRanking(data.rank);
                var svg = a.getSVG();
                canvg(document.getElementById('canvas'), svg);
                p = document.getElementById('canvas');
                var theImage = p.toDataURL('image/png');
            }
            $.ajax({
                type: "POST",
                url: EMS_URL+'/plugins/exporting-server/index.php',
                data: {
                    base64: theImage,
                    rnk1: rnk.obj1,
                    rnk2: rnk.obj2,
                },
                success: function (data) {
                    location.href = APP_URL + 'avaliacao/imprimir/' + aval + '/' + data;                       
                }
            });
        },

        error: function (data) {

        }
    });
});

function download(aval, data) {
    location.href = APP_URL+'avaliacao/imprimir/' + aval + '/' + data;
}


function verSeExiste(data) {
    $.ajax({
        type: "POST",
        url: APP_URL+'avaliacao/verificarExiste/' + data,
        success: function (ex) {
        //console.log(ex);
        return ex;
           //if(ex == 1) {
            //setTimeout(verSeExiste(data), 1000);
           //}
        }
    });
}

$('#btnFormRAC').unbind().click(function (e) {
    theImage = null;
            rnk = {
                obj1: null,
                obj2: null
            } 

    e.preventDefault();
    aval = $(this).data('setor');
    
    $.ajax({
        type: "POST",
        url: APP_URL+'avaliacao/getSVGSetor/' + aval,
        dataType: 'json',
        success: function (data) {

            a = gerarGrafico(data.grafico);
            rnk  = gerarGraficoRanking(data.rank);
            
            var svg = a.getSVG();
            ////console.log(svg);
            canvg(document.getElementById('canvas'), svg);
            p = document.getElementById('canvas');
            
            var theImage = p.toDataURL('image/png');

            $.ajax({
                type: "POST",
                url: EMS_URL+'/plugins/exporting-server/index.php',
                data: {
                    base64: theImage,
                    rnk1: rnk.obj1,
                    rnk2: rnk.obj2,
                },
                success: function (data) {
                    $('#modalAguarde').modal('show');
                    location.href = APP_URL+'avaliacao/imprimirRAC/' + aval + '/' + data;
                   // location.href = APP_URL+'avaliacao/imprimir/' + aval + '/' + data+'/1';
                    setTimeout(alertFunc, 5000);

                   //console.log(data);
                   //location.href = APP_URL+'avaliacao/imprimir/' + aval + '/' + data;
                }
            });
            
           
            //a = gerarGrafico(data.grafico);
            //b = exportarGraficoLinha(a, 900, 900);
            
            //$('#modalAguarde').modal('show');
            //location.href = APP_URL+'avaliacao/imprimirRAC/' + aval + '/' + b;
            //setTimeout(alertFunc, 5000);
        },
        error: function (data) {

        }
    });
});

function gerarGraficoRanking(data) {
    dados = data[1];

    ////console.log(dados);

    var opts = {
        angle: 0, // The span of the gauge arc
        lineWidth: 0.44, // The line thickness
        radiusScale: 1, // Relative radius
        pointer: {
            length: 0.5, // // Relative to gauge radius
            strokeWidth: 0.033, // The thickness
            color: '#000000' // Fill color
        }, staticLabels: {
            font: "10px sans-serif", // Specifies font
            labels: [100, 130, 150, 220.1, 260, 300], // Print labels at these values
            color: "#000000", // Optional: Label text color
            fractionDigits: 1  // Optional: Numerical precision. 0=round off.
        },

        limitMax: false, // If false, max value increases automatically if value > maxValue
        percentColors: [
            [0, "#c9302c"],
            [0.25, "#ec971f"],
            [0.75, "#ec971f"],
            [1.0, "#5cb85c"]],
        limitMin: false, // If true, the min value of the gauge will be fixed
        colorStart: '#000', // Colors
        colorStop: '#8FC0DA', // just experiment with them
        strokeColor: '#E0E0E0', // to see which ones work best for you
        generateGradient: false,
        highDpiSupport: false, // High resolution support

    };
    


        var target = document.getElementById('foo'); // your canvas element
        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
        gauge.minValue = -(dados.COUNT_BRASIL); // set max gauge value
        gauge.maxValue = -1;  // Prefer setter over gauge.minValue = 0
        gauge.animationSpeed = 1; // set animation speed (32 is default value)
        gauge.set(-(dados.RNK_BRASIL)); // set actual value
        //gauge.setTextField(document.getElementById("preview-textfield"));

    

        var target2 = document.getElementById('foo1');
        var gauge2 = new Gauge(target2).setOptions(opts);
        gauge2.minValue = -(dados.COUNT_REGIONAL); // set max gauge value
        gauge2.maxValue = -1;  // Prefer setter over gauge.minValue = 0
        gauge2.animationSpeed = 1; // set animation speed (32 is default value)
        gauge2.set(-(dados.RNK_REGIONAL)); // set actual value
        //gauge2.setTextField(document.getElementById("preview-textfield2"));


        canvas = document.getElementById('foo');
        canvas1 = document.getElementById('foo1');
        
        r = Canvas2Image.convertToPNG(canvas);
        r1 = Canvas2Image.convertToPNG(canvas1);


        retorno = {
            'obj1': $(r).attr('src'),
            'obj2': $(r1).attr('src')
        }

        //console.log(retorno);

        return retorno;

}

function gerarGrafico(dados) {
   // //console.log(dados);
   ////console.log(dados.EIXOS);

/*
    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container2',
            defaultSeriesType: 'spline'
        },
        title: {
            text: 'Evolução dos Pilares'
        },
        xAxis: {
            labels: {
            },
            categories: $.parseJSON(dados.cabecalho)
        },
        yAxis: [{
                title: {
                    text: 'Produtividade / Cob. Objetivo'
                },
                labels: {
                    format: '{value}',
                },
            }, {
                title: {
                    text: 'Presc. Share / Market Share'
                },
                labels: {
                    format: '{value}',
                },
                opposite: true
            }],
        plotOptions: {
            series: {
                animation: true
            },
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: true
            }
        },
        series: $.parseJSON(dados.dados)
    });
*/

chart = new Highcharts.Chart({
    chart: {
        defaultSeriesType: 'spline',
        renderTo: 'container2',
        height: 300,
        width: 700
    },
    legend: {
        align: 'center',
        verticalAlign: 'bottom',
        layout: 'horizontal',
        itemStyle: {
            color: '#000000',
            fontWeight: 'bold',
            fontSize: '10px'
        }

    },

    title: {
        text: null

    },
    xAxis: {

        labels: {
            style: {
                fontSize: '9px'
            }
        },
        categories: $.parseJSON(dados.CAB)
    },
    tooltip: {
        pointFormat: '<b>{series.name}: </b>{point.y:,.1f}%'
    },
    yAxis: [{
            align: 'middle',
            title: {
                style: {
                    fontSize: '10px'
                },
                text: dados.EIXOS.e0
            },
            labels: {

                format: '{value}'
            },
        }, {

            title: {
                style: {
                    fontSize: '10px'
                },
                text: dados.EIXOS.e1
            },
            labels: {
                format: '{value}',

            },
            opposite: true
        }],

    plotOptions: {
        series: {
            animation: true
        },
        line: {
            dataLabels: {
                enabled: true,

            },
            enableMouseTracking: true
        }
    },
    series: $.parseJSON(dados.DADOS)



});
    return chart;
}


function exportarGraficoLinha(chart, w, h) {
    var rdata;
    var exportUrl = EMS_URL+'/plugins/exporting-server/index.php';



    var nome_arquivo = '';
    var rdata = '';

    var d = new Date();
    var n = d.getTime();
    nome_arquivo = n.toString();

    var obj = {}, chart;
    obj.svg = chart.getSVG();
    obj.type = 'image/png';
    obj.width = w;
    //obj.scale = 2;
    obj.async = true;
    obj.filename = nome_arquivo;
    obj.sourceWidth = w;
    obj.sourceHeight = h;

    $.ajax({
        type: "POST",
        url: exportUrl,
        data: obj,
        cache: false,
        async: false,
        crossDomain: true,
        success: function (data) {
			//console.log(data);
			//console.log(data.status);
			//console.log(data.statusText);
			////console.log(data);
            //alert(data);
            rdata = data;
            //return rdata;
            //return nome_arquivo;
        },
        error: function (data) {
			//console.log(data);
			//console.log(data.status);
			//console.log(data.statusText);
           //alert(data);
            //alert("----"+data.status);
            //alert(data.statusText);
        }
    });

    var obj = {}
    d = rdata.split('.');
    return d[0];
}
    