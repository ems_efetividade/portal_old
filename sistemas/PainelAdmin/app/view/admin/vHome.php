<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

require_once('includes.php');

    if(appFunction::dadoSessao('nivel_admin') != 2) {
        header('Location: '.EMS_URL);
    }
?>

<style>
    .panel-left { background: #ddd; }
    .progress { margin-bottom: 0px; height: 15px;}
</style>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/menuAdmin.php'); ?>
        </div>
        <div class="col-lg-10 panel-right">
            <div class="row">
                <div class="col-lg-9">
                    <h2><b><span class="glyphicon glyphicon-file"></span> Aceite de Documentos</b> <small> - Documentos Cadastrados</small></h2>
                </div>
                <div class="col-lg-3 text-right">
                    <br>
                    <p>
                        <a href="<?php echo appConf::caminho ?>admin/editar" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span> Adicionar Documento</a>
                    </p>
                </div>
            </div>

            <hr>
    
            <table class="table table-striped">
                <tr>
                    <th><small>Documento</small></th>
                    <th class="text-center"><small>Extensão</small></th>
<!--                    <th class="text-center">Assinaturas</th>-->
                    <th class="text-center"><small>Assinaturas %</small></th>
                    <th class="text-center"><small>Status</small></th>
                    <th class="text-center"><small>Expira em</small></th>
                    <th class="text-right"><small>Ações</small></th>
                </tr>
                <?php foreach($documentos as $documento) { ?>
                <tr>
                    <td ><small><?php echo $documento->getTitulo() ?></small></td>
                    <td class="text-center"><small><?php echo $documento->getExtensao() ?></small></td>
<!--                    <td class="text-center">
                        <?php $resp = $documento->getTotalRespostas(); ?>
                        <div class="progress">
                            <div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo ($resp[1]['ASSINADO']/$resp[1]['TOTAL']) * 100 ?>%;">
                              
                            </div>
                        </div>
                    </td>-->
                    <td class="text-center"><small><?php echo ($resp[1]['ASSINADO'] == 0) ? 0.00 : appFunction::formatarMoedaBRL(($resp[1]['ASSINADO']/$resp[1]['TOTAL']) * 100) ?>%</small></td>
                    <td class="text-center"><?php echo ($documento->getAtivo() == 0) ? '<span class="label label-default">Inativo</span>' : '<span class="label label-success">Ativo</span>' ?></td>
                    <td class="text-center"><small><?php echo appFunction::formatarData($documento->getDataExpirar()) ?> (<?php echo $documento->getTempoExpirar() ?>)</small></td>
                    <td class="text-right">
                        
                        <a class="btn btn-xs btn-warning" href="<?php echo appConf::caminho ?>admin/editar/<?php echo $documento->getIdDocumento() ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;</a>
                        <a class="btn btn-xs btn-primary" href="<?php echo appConf::caminho ?>admin/verDocumento/<?php echo $documento->getIdDocumento() ?>" ><span class="glyphicon glyphicon-eye-open"></span>&nbsp;</a>
                        <a class="btn btn-xs btn-danger" href="#" data-toggle="modal" data-target="#modalExcluir" data-id="<?php echo $documento->getIdDocumento() ?>"><span class="glyphicon glyphicon-remove"></span>&nbsp;</a>
                    </td>
                </tr>
                <?php } ?>
            </table>
            
            
        </div>
    </div>
</div>



<div class="modal fade" id="modalExcluir" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção</h4>
      </div>
      <div class="modal-body">
        <h4 class="text-center">Deseja excluir esse documento ?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="button" class="btn btn-danger" id="btnExcluirDocumento">Excluir</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalErro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Atenção</h4>
      </div>
      <div class="modal-body">
          <h4 class="text-center"></h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
      </div>
    </div>
  </div>
</div>


<script>
    
    resizePanelLeft();
$('#modalExcluir').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var id = button.data('id') // Extract info from data-* attributes


    $('#btnExcluirDocumento').unbind().click(function(e) {
        
        $.ajax({
            type: "POST",
            url: '<?php echo appConf::caminho ?>admin/excluirDocumento/'+id,
            success: function(retorno) {
                if($.trim(retorno) != "") {
                    $('#modalExcluir').modal('hide');
                    $('#modalErro .modal-body h4').html(retorno);
                    $('#modalErro').modal('show');
                } else {
                    window.location = '<?php echo appConf::caminho ?>admin';
                }
            }
        });
        
    });

});
</script>