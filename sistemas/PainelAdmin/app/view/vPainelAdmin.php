<?php
require_once('header.php');

class vPainelAdmin extends gridView
{


    public function __construct()
    {
        $this->viewGrid();
    }

    public function viewGrid()
    {
        $cols = array('ID', 'NOME', 'ENDERECO', 'COMPLEMENTO', 'TELEFONE');
        $colsPesquisaNome = array('ID', 'NOME', 'ENDERECO', 'COMPLEMENTO', 'TELEFONE');
        $colsPesquisa = array('id', 'nome', 'endereco', 'complemento', 'telefone');
        $array_metodo = array('Id', 'Nome', 'Endereco', 'Complemento', 'Telefone');
        gridView::setGrid_titulo("Painel Administrador");
        gridView::setGrid_sub_titulo("Painel integrado de gerenciamento");
        gridView::setGrid_cadastrar(0);
        gridView::renderGrid();
        $this->painelPadrao();
    }

    public function painelPadrao()
    {
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                </div>
            </div>
        </div>
        <?php
    }

}