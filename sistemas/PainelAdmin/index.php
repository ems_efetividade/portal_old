<?php
/**
 * 2018 EMS Pharma - Desenvolvido por:  Jean Silva - Jefferson Oliveira
 */

function validarSessao() {
    @session_start();
    if(!isset($_SESSION['id_setor'])) {
        header('Location: '.'https://'.$_SERVER[HTTP_HOST].'/login/');
    }
}

validarSessao();


class Framework {
    
    protected static $controller   = 'cPainelAdmin';
    protected static $method       = 'main';
    protected static $params       = [];
    protected static $controller_path = 'app\\controller\\';

    public function __construct() {}
    
    static function parseUrl() {
        if(isset($_GET['url'])) {
            return $url = explode('/', filter_var(rtrim($_GET['url']), FILTER_SANITIZE_URL));
        }
    }
    
    static function start() {
        // pega a url e gera um array
        $url = self::parseUrl();
        // verifica se existe o controlador
        if(file_exists(self::$controller_path . 'c'.ucfirst($url[0]).'.php')) {
            self::$controller = 'c'.ucfirst($url[0]);
            unset($url[0]);  
        } else {
            //echo 'nao achou -'.  $url.'.php';
        }

        // inclui o controlador na página
        require_once self::$controller_path . self::$controller.'.php';

        
        //instancia a classe do controlador
        self::$controller = new self::$controller;
        
        //verifica se existe o metodo na classe do controlador
        if(isset($url[1])) {
            if(method_exists(self::$controller, $url[1])) {
                self::$method = $url[1];
                unset($url[1]);
            }
        }
        
        // verifica se existe parametros
        self::$params = $url ? array_values($url) : [];

        // chama a classe com o metodo e os parametros
        call_user_func_array([self::$controller, self::$method], self::$params);
    }
}

Framework::start();

?>