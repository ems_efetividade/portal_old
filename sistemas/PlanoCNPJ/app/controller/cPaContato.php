<?php
require_once('lib/appController.php');
require_once('app/model/mPaContato.php');


class cPaContato extends appController {

    private $modelPaContato = null;

    public function __construct(){
        $this->modelPaContato = new mPaContato();
    }

    public function main(){

    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaContato();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaContato();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaContato();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $this->modelPaContato->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $this->modelPaContato->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        return $this->modelPaContato->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        return $this->modelPaContato->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $this->modelPaContato->updateObj();
    }
}
