<?php
require_once('lib/appController.php');
require_once('app/model/mPaContatoEmail.php');
require_once('app/view/formsPaContatoEmail.php');

class cPaContatoEmail extends appController {

    private $modelPaContatoEmail = null;

    public function __construct(){
        $this->modelPaContatoEmail = new mPaContatoEmail();
    }

    public function main(){
        $this->render('vPaContatoEmail');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaContatoEmail();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaContatoEmail();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaContatoEmail();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmail = $_POST['fkemail'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoEmail->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoEmail->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoEmail->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoEmail->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoEmail->updateObj();
    }
}
