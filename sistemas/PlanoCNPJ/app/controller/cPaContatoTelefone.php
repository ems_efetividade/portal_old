<?php
require_once('lib/appController.php');
require_once('app/model/mPaContatoTelefone.php');
require_once('app/view/formsPaContatoTelefone.php');

class cPaContatoTelefone extends appController {

    private $modelPaContatoTelefone = null;

    public function __construct(){
        $this->modelPaContatoTelefone = new mPaContatoTelefone();
    }

    public function main(){
        $this->render('vPaContatoTelefone');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaContatoTelefone();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaContatoTelefone();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaContatoTelefone();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTelefone->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTelefone->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoTelefone->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoTelefone->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTelefone->updateObj();
    }
}
