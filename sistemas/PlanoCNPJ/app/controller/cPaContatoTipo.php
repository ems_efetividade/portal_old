<?php
require_once('lib/appController.php');
require_once('app/model/mPaContatoTipo.php');


class cPaContatoTipo extends appController {

    private $modelPaContatoTipo = null;

    public function __construct(){
        $this->modelPaContatoTipo = new mPaContatoTipo();
    }

    public function main(){

    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $descricao = $_POST['descricao'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTipo->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTipo->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoTipo->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaContatoTipo->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $descricao = $_POST['descricao'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaContatoTipo->updateObj();
    }
}
