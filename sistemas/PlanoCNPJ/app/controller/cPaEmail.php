<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmail.php');
require_once('app/view/formsPaEmail.php');

class cPaEmail extends appController {

    private $modelPaEmail = null;

    public function __construct(){
        $this->modelPaEmail = new mPaEmail();
    }

    public function main(){
        $this->render('vPaEmail');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmail();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmail();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmail();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $email = $_POST['email'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmail->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $email = $_POST['email'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmail->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $email = $_POST['email'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmail->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $email = $_POST['email'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmail->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $email = $_POST['email'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmail->updateObj();
    }
}
