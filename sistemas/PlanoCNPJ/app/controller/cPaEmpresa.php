<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresa.php');
require_once('app/view/formsPaEmpresa.php');

class cPaEmpresa extends appController
{

    private $modelPaEmpresa = null;

    public function __construct()
    {
        $this->modelPaEmpresa = new mPaEmpresa();
    }

    public function controlSwitch($acao)
    {
        $pesquisa = $_POST['isPesquisa'];
        if ($pesquisa == '1')
            $acao = 'pesquisar';
        switch ($acao) {
            case 'salvarPma':
                $this->salvarPma();
                break;
            case 'home':
                $forms = new formsPaEmpresa();
                $forms->formInicio();
                break;
            case 'cadastrar':
                $forms = new formsPaEmpresa();
                if ($_POST['idEmpresa'] > 0) {
                    $forms->formCadastrarEmpresa($_POST['idEmpresa']);
                    return;
                }
                $forms->formCadastrarEmpresa();
                break;
            case 'adicionais':
                $idEmpresa = $this->salvarEmpresa();
                $forms = new formsPaEmpresa();
                if ($idEmpresa < 1) {
                    return;
                }
                $forms->formDadosAdicionais($idEmpresa);
                break;
            case 'usuarios':
                $idEmpresa = $this->salvarDadosAdcionais();
                $forms = new formsPaEmpresa();
                if (isset($_POST['editar']))
                    if ($_POST['editar'] > 0)
                        $this->updateContatos($idEmpresa, $_POST['editar']);
                $contatos = $this->listarContatosByIdEmpresa($idEmpresa);
                $forms->formUsuarios($idEmpresa, $contatos);
                break;
            case 'adicionarContato':
                $this->salvarContatos();
                $idEmpresa = $_POST['idEmpresa'];
                $retorno = $this->listarContatosByIdEmpresa($idEmpresa);
                echo json_encode($retorno);
                break;
            case 'excluirContatos':
                $forms = new formsPaEmpresa();
                $idEmpresa = $_POST['idEmpresa'];
                $idContato = $_POST['idContato'];
                $this->excluirContato($idContato);
                $retorno = $this->listarContatosByIdEmpresa($idEmpresa);
                echo json_encode($retorno);
                break;
            case 'editarContato':
                $forms = new formsPaEmpresa();
                $idEmpresa = $_POST['idEmpresa'];
                $idContato = $_POST['idContato'];
                $retorno = $this->listarContatosByIdEmpresa($idEmpresa);
                echo json_encode($retorno);
                break;
            case 'ficha':
                $forms = new formsPaEmpresa();
                $forms->formFichaEmpresaVisualizar();
                break;
            case 'agendar':
                $forms = new formsPaEmpresa();
                $forms->formAtividades();
                break;
            case 'atividades':
                $forms = new formsPaEmpresa();
                $forms->viewAtividades();
                break;
            case 'contatos':
                $forms = new formsPaEmpresa();
                $forms->viewContatos();
                break;
            case 'home':
                $forms = new formsPaEmpresa();
                $forms->formView();
                break;
            case 'visualizarEmpresa':
                $forms = new formsPaEmpresa();
                $forms->formVisualizarEmpresa();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresa();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresa();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                if ($_POST['operador'] == '')
                    $_POST['pesquisa'] = 1;
                $this->main();
                break;
                break;
            case 'carregaDados':
                return $this->carregaDados();
                break;
            default:
                $this->main();
                break;
        }
    }

    public function loadAtividade($idAtividade)
    {
        $retorno = $this->modelPaEmpresa->loadAtividade($idAtividade);
        return $retorno;
    }

    public function nomeResponsavel($idEmpresa=0)
    {
        if($idEmpresa==0) {
            $idEmpresa = $_POST['idEmpresa'];
        }
        $retorno = $this->modelPaEmpresa->getNomeResposavelByIdEmpresa($idEmpresa);
        echo $retorno[1][0];
    }

    public function alterarResponsavel()
    {
        $idEmpresa = $_POST['idEmpresa'];
        $idResponsavel = $_POST['idResponsavel'];
        $fkuser = $_SESSION['id_colaborador'];
        $this->modelPaEmpresa->alterarResponsavel($idResponsavel, $idEmpresa, $fkuser);
    }

    public function listarAtividades($idUser)
    {
        $retorno = $this->modelPaEmpresa->listarAtividades($idUser);
        return $retorno;
    }

    public function salvarPma()
    {
        $_POST['msg'] = $this->limpaCaracteres($_POST['msg']);
        $_POST['contatos'] = explode(',', $_POST['contatos']);
        return $this->modelPaEmpresa->salvarPma();
    }

    public function dadosAdicionais($idEmpresa)
    {
        return $this->modelPaEmpresa->dadosAdicionais($idEmpresa);
    }

    public function updateContatos($idEmpresa, $editar)
    {
        return $this->modelPaEmpresa->updateContatos($idEmpresa, $editar);
    }

    public function salvarEmpresa()
    {
        if (isset($_POST['editar'])) {
            if ($_POST['editar'] > 0) {
                $empresaDados = $this->loadDadosEmpresabyId($_POST['editar']);
                $fkUser = $_SESSION['id_colaborador'];
                $cep = $empresaDados['empresa'][1]['CEP'];
                $complemento = $empresaDados['empresa'][1]['COMPLEMENTO'];
                $cnpj = $empresaDados['empresa'][1]['CNPJ'];
                $cidade = $empresaDados['empresa'][1]['CIDADE'];
                $bairro = $empresaDados['empresa'][1]['BAIRRO'];
                $logradouro = $empresaDados['empresa'][1]['LOGRADOURO'];
                $estado = $empresaDados['empresa'][1]['ESTADO'];
                $fantasia = $empresaDados['empresa'][1]['FANTASIA'];
                $razao = $empresaDados['empresa'][1]['RAZAO_SOCIAL'];
                $numero = $empresaDados['empresa'][1]['NUMERO'];
                $whatsapp = $empresaDados['telefone'][1]['WHATSAPP'];
                $telefone = $empresaDados['telefone'][1]['TEL'];
                $email = $empresaDados['email'][1]['EMAIL'];
                $_POST['id'] = $_POST['editar'];
                $this->delete();
                unset($_POST['id']);
                return $idEmpresa = $this->modelPaEmpresa->salvarEmpresa($fkUser, $whatsapp, $complemento, $cnpj, $cidade, $bairro, $logradouro, $estado, $fantasia, $razao, $telefone, $email, $numero, $cep);
            }
        }
        if (isset($_POST['idEmpresa'])) {
            $idEmpresaEditar = $_POST['idEmpresa'];
        }
        if ($idEmpresaEditar > 0) {
            $_POST['id'] = $idEmpresaEditar;
            $this->delete();
            unset($_POST['id']);
        }
        $whatsapp = $_POST['whatsapp'];
        $complemento = $this->limpaCaracteres($_POST['complemento']);
        $cnpj = $this->limpaCaracteres($_POST['cnpj']);
        $cidade = $this->limpaCaracteres($_POST['cidade']);
        $bairro = $this->limpaCaracteres($_POST['bairro']);
        $logradouro = $this->limpaCaracteres($_POST['logradouro']);
        $estado = $this->limpaCaracteres($_POST['estado']);
        $fantasia = $this->limpaCaracteres($_POST['fantasia']);
        $razao = $this->limpaCaracteres($_POST['razao']);
        $telefone = $this->limpaCaracteres($_POST['telefone']);
        $email = $this->limpaCaracteres($_POST['email']);
        $numero = $this->limpaCaracteres($_POST['numero']);
        $cep = $this->limpaCaracteres($_POST['cep']);
        $fkUser = $_SESSION['id_colaborador'];
        $idEmpresa = $this->modelPaEmpresa->salvarEmpresa($fkUser, $whatsapp, $complemento, $cnpj, $cidade, $bairro, $logradouro, $estado, $fantasia, $razao, $telefone, $email, $numero, $cep);
        if ($idEmpresaEditar > 0) {
            $dados = $this->dadosAdicionais($idEmpresaEditar);
            $fkUser = $fkUser;
            $idEmpresa = $idEmpresa;
            if (is_array($dados[8][1])) {
                $consultoria = $dados[8][1]['FK_CONSULTORIA'];
                $situacao = $dados[8][1]['FK_SITUACAO'];
            } else {
                $consultoria = 0;
                $situacao = 0;
            }
            if (is_array($dados[7][1])) {
                $objetivo = $dados[7][1]['OBJETIVO'];
                $previsao = $dados[7][1]['PRAZO'];
            } else {
                $objetivo = 0;
                $previsao = 0;
            }
            if (is_array($dados[6][1])) {
                $relacionamento = $dados[6][1]['FK_REGUA'];
            } else {
                $relacionamento = 0;
            }
            if (is_array($dados[6][1])) {
                $responsavel = $dados[1][1]['FK_RESPONSAVEL'];
            } else {
                $responsavel = 0;
            }
            if (is_array($dados[0][1])) {
                $nMedicos = $dados[0][1]['NUMERO_MEDICOS'];
                $nPacientes = $dados[0][1]['NUMERO_PACIENTES'];
                $consultasDia = $dados[0][1]['PACIENTES_DIA'];
            } else {
                $nMedicos = 0;
                $nPacientes = 0;
                $consultasDia = 0;
            }
            if (is_array($dados[2][1])) {
                $valorConsulta = $dados[2][1]['VALOR'];
                $porcAtendimento = $dados[2][1]['PERCENTUAL'];
            } else {
                $valorConsulta = 0;
                $porcAtendimento = 0;
            }
            foreach ($dados[3] as $item) {
                $arrayPlataforma[] = $item['FK_PLATAFORMA'];
            }
            foreach ($dados[5] as $item) {
                $arrayEspecialidade[] = $item['FK_ESPECIALIDADE'];
            }
            if (is_array($dados[4])) {
                foreach ($dados[4] as $item) {
                    $arrayConvenio[] = $item['FK_CONVENIO'];
                }
            }
            $retorno = $this->modelPaEmpresa->getRelacionamentoById($relacionamento);
            $relacionamento[0] = $retorno[1]['POTENCIAL'];
            $relacionamento[1] = $retorno[1]['RELACIONAMENTO'];
            $this->modelPaEmpresa->salvarDadosAdcionais($consultoria, $situacao, $objetivo, $previsao, $relacionamento, $fkUser, $idEmpresa, $responsavel, $nMedicos, $nPacientes, $consultasDia, $valorConsulta, $porcAtendimento, $arrayPlataforma, $arrayEspecialidade, $arrayConvenio);
            $this->updateContatos($idEmpresa, $idEmpresaEditar);
        }
        return $idEmpresa;
    }

    public function limpaCaracteres($valor)
    {
        $valor = trim($valor);
        $valor = str_replace(".", "", $valor);
        $valor = str_replace(",", "", $valor);
        $valor = str_replace("-", "", $valor);
        $valor = str_replace("/", "", $valor);
        $valor = str_replace("(", "", $valor);
        $valor = str_replace(")", "", $valor);
        $valor = str_replace("'", "´", $valor);
        return $valor;
    }

    public function salvarDadosAdcionais()
    {
        $previsao = $_POST['previsao'];
        $objetivo = $_POST['objetivo'];
        $situacao = $_POST['situacao'];
        $consultoria = $_POST['consultoria'];
        $fkUser = $_SESSION['id_colaborador'];
        $idEmpresa = $this->limpaCaracteres($_POST['idEmpresa']);
        $responsavel = $_POST['responsavel'];
        $relacionamento = $_POST['relacionamento'];
        $nMedicos = $this->limpaCaracteres($_POST['nMedicos']);
        $nPacientes = $this->limpaCaracteres($_POST['nPacientes']);
        $consultasDia = $this->limpaCaracteres($_POST['consultasDia']);
        $valorConsulta = $_POST['valorConsulta'];
        $porcAtendimento = $this->limpaCaracteres($_POST['porcAtendimento']);
        $arrayPlataforma = explode(",", $_POST['arrayPlataforma']);
        $arrayEspecialidade = explode(",", $_POST['arrayEspecialidade']);
        $arrayConvenio = explode(",", $_POST['arrayConvenio']);
        if (isset($_POST['editar']))
            if ($_POST['editar'] > 0) {
                $idEmpresa = $this->salvarEmpresa();
            }
        return $this->modelPaEmpresa->salvarDadosAdcionais($consultoria, $situacao, $objetivo, $previsao, $relacionamento, $fkUser, $idEmpresa, $responsavel, $nMedicos, $nPacientes, $consultasDia, $valorConsulta, $porcAtendimento, $arrayPlataforma, $arrayEspecialidade, $arrayConvenio);
    }

    public function informacoesContatos()
    {
        $ids = $_POST['contatos'];
        $contatos = $this->modelPaEmpresa->informacoesContatos($ids);
        echo json_encode($contatos);

    }


    public function listarContatosByIdEmpresa($idEmpresa)
    {
        require_once('/../model/mContato.php');
        $contatos = $this->modelPaEmpresa->listarContatosByIdEmpresa($idEmpresa);
        $arrayContatos = Array();
        if (is_array($contatos))
            foreach ($contatos as $contato) {
                $crm = $this->modelPaEmpresa->getCrmByIdContato($contato['ID']);
                $email = $this->modelPaEmpresa->getEmailByIdContato($contato['ID']);
                $telefone = $this->modelPaEmpresa->getTelefoneByIdContato($contato['ID']);
                $objeto = new mContato();
                $objeto->setId($contato['ID']);
                $objeto->setNome($contato['NOME']);
                $objeto->setDataNasc($contato['DATA_NASC']);
                $objeto->setContatoTipo($contato['DESCRICAO']);
                $objeto->setDecisor($contato['DECISOR']);
                $objeto->setTelefone($telefone[1][0]);
                $objeto->setEmail($email[1][0]);
                $objeto->setWhatsapp($telefone[1][1]);
                $objeto->setCrm($crm[1][1]);
                $objeto->setUf($crm[1][0]);
                $objeto->setResidente($crm[1][2]);
                $arrayContatos[] = $objeto;
            }
        return $arrayContatos;
    }

    public function salvarContatos()
    {
        $verificadorMedico = $this->limpaCaracteres($_POST['verificadorMedico']);
        $idEmpresa = $this->limpaCaracteres($_POST['idEmpresa']);
        $tipoContato = $this->limpaCaracteres($_POST['tipoContato']);
        $decisor = $this->limpaCaracteres($_POST['decisor']);
        $medico = $this->limpaCaracteres($_POST['medico']);
        $residente = $this->limpaCaracteres($_POST['residente']);
        $estadoMedico = $this->limpaCaracteres($_POST['estadoMedico']);
        $crm = trim($_POST['crm']);
        $whatsapp = $_POST['whatsapp'];
        $telefone = $this->limpaCaracteres($_POST['telefone']);
        $email = $_POST['email'];
        $dataNasc = $_POST['dataNasc'];
        $nome = $this->limpaCaracteres($_POST['nome']);
        $fkUser = $_SESSION['id_colaborador'];
        return $this->modelPaEmpresa->salvarContatos($verificadorMedico, $idEmpresa, $fkUser, $dataNasc, $nome, $email, $telefone, $whatsapp, $crm, $estadoMedico, $residente, $medico, $decisor, $tipoContato);
    }

    public function loadObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresa->loadObj();
    }

    public function excluirContato($idContato)
    {
        $this->modelPaEmpresa->excluirContato($idContato);
    }

    public function updateObj()
    {

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresa->updateObj();
    }

    public function delete()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresa->delete();
    }

    public function main()
    {

        $this->render('vPaEmpresa');
    }

    public function carregaDados()
    {
        require_once('cPaPlataforma.php');
        require_once('cPaConvenio.php');
        require_once('cPaEspecialidade.php');
        require_once('cPaContatoTipo.php');
        unset($_POST);
        $cEspecialidade = new cPaEspecialidade();
        $cPlataforma = new cPaPlataforma();
        $cConvenio = new cPaConvenio();
        $cContato = new cPaContatoTipo();
        $dados['convenio'] = $cConvenio->listObj();
        $dados['tipocontato'] = $cContato->listObj();
        $dados['especialidade'] = $cEspecialidade->listObj();
        $dados['plataforma'] = $cPlataforma->listObj();
        $dados['colaborador'] = $this->listColaboradores();
        $dados['estado'] = $this->listEstados();
        $dados['ugn'] = $this->listUgn();
        $dados['consultoria'] = $this->listConsultoria();
        $dados['situacao'] = $this->listSituacao();
        return $dados;
    }

    public function listColaboradores()
    {
        return $this->modelPaEmpresa->listColaboradores();
    }

    public function listEstados()
    {
        return $this->modelPaEmpresa->listEstados();
    }

    public function listUgn()
    {
        return $this->modelPaEmpresa->listUgn();
    }

    public function listConsultoria()
    {
        return $this->modelPaEmpresa->listConsultoria();
    }

    public function listSituacao()
    {
        return $this->modelPaEmpresa->listSituacao();
    }

    public function getIdUgnByCep($cep)
    {
        return $this->modelPaEmpresa->getIdUgnByCep($cep);
    }

    public function listUgnByIdColaborador($idColaborador)
    {
        $retorno = $this->modelPaEmpresa->listUgnByIdColaborador($idColaborador);
        $ugns = array();
        if (is_array($retorno))
            foreach ($retorno as $value) {
                if (!in_array($value['SETOR'][3], $ugns))
                    $ugns[$value['SETOR'][3]] = $value['SETOR'][3];
            }
        return $ugns;
    }

    public function listIdEmpresaByUgn($ugn)
    {
        return $this->modelPaEmpresa->listIdEmpresaByUgn($ugn);
    }

    public function formatarCep($string)
    {
        $string = substr($string, 0, 5) . '-' . substr($string, 5, 3);
        return $string;
    }

    public function listCnpjByIds($listIdEmpresa)
    {
        if (!is_array($listIdEmpresa))
            return;
        $ids = '';
        foreach ($listIdEmpresa as $id) {
            if ($ids != '') {
                $ids .= ',';
            }
            $ids .= $id;
        }
        if ($ids == '')
            return;
        return $this->modelPaEmpresa->listCnpjByIds($ids);
    }

    public function formatarCnpj($cnpj_cpf)
    {
        if (strlen(preg_replace("/\D/", '', $cnpj_cpf)) === 11) {
            $response = preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
        } else {
            $response = preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
        }
        return $response;
    }

    public function countContatosByIdEmpresa($idEmpresa)
    {
        return $this->modelPaEmpresa->countContatosByIdEmpresa($idEmpresa);
    }

    public function countCnpjByUgn($ugn)
    {
        return $this->modelPaEmpresa->countCnpjByUgn($ugn);
    }

    public function countContatosByUgn($ugn)
    {
        return $this->modelPaEmpresa->countContatosByUgn($ugn);
    }

    public function countContatos()
    {
        return $this->modelPaEmpresa->countContatos();
    }

    public function countCnpj()
    {
        return $this->modelPaEmpresa->countCnpj();
    }

    public function listIdEmpresaByColaborador($idColaborador)
    {
        $retorno = $this->modelPaEmpresa->listIdEmpresaByColaborador($idColaborador);
        foreach ($retorno as $empresa) {
            $ids[] = $empresa['FK_EMPRESA'];
        }
        return $ids;
    }

    public function buscaCNPJ($cnpj)
    {
        if ($this->verificaCNPJ($cnpj) >= 1) {
            echo 1;
            return;
        }
        $service_url = 'https://www.receitaws.com.br/v1/cnpj/' . $cnpj;
        $headers = array(
            'Content-Type: application/json',
        );
        $url = $service_url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
        echo $result;
    }

    public function verificaCNPJ($crm)
    {
        $retorno = $this->modelPaEmpresa->verificaCNPJ($crm);
        return $retorno[1][0];
    }

    public function localizarIdCrm($crm)
    {
        return $this->modelPaEmpresa->localizarIdCrm($crm);
    }

    public function loadDadosEmpresabyId($id)
    {
        return $this->modelPaEmpresa->loadDadosEmpresabyId($id);
    }

    public function loadDadosEmpresa($idEmpresa)
    {
        require_once('/../model/mPaEmpresaConsultas.php');
        require_once('/../model/mPaEmpresaEmail.php');
        require_once('/../model/mPaEmpresaConvenio.php');
        require_once('/../model/mPaEmpresaEspecialidade.php');
        require_once('/../model/mPaEmpresaPlataforma.php');
        require_once('/../model/mPaEmpresaRegua.php');
        require_once('/../model/mPaEmpresaResponsavel.php');
        require_once('/../model/mPaEmpresaTelefone.php');
        require_once('/../model/mPaTelefone.php');
        require_once('/../model/mPaEmail.php');
        require_once('/../model/mPaRegua.php');
        require_once('/../model/mPaContatoEmail.php');
        require_once('/../model/mPaContatoTelefone.php');
        require_once('/../model/mPaContato.php');
        require_once('/../model/mPaCrm.php');
        require_once('/../model/mPaMedico.php');
        require_once('/../model/mPaContatoTipo.php');
        $mTipoContato = new mPaContatoTipo();
        $mMedico = new mPaMedico();

        $mCrm = new mPaCrm();
        $mContatoEmail = new mPaContatoEmail();
        $mContato = new mPaContato();
        $mContatoTelefone = new mPaContatoTelefone();
        $mTelefone = new mPaTelefone();
        $mRegua = new mPaRegua();
        $mEmail = new mPaEmail();
        $empresaTelefone = new mPaEmpresaTelefone();
        $empresaResponsavel = new mPaEmpresaResponsavel();
        $empresaRegua = new mPaEmpresaRegua();
        $empresaPlataforma = new mPaEmpresaPlataforma();
        $empresaEspecialidade = new mPaEmpresaEspecialidade();
        $empresaConvenio = new mPaEmpresaConvenio();
        $empresaCosultas = new mPaEmpresaConsultas();
        $empresaEmail = new mPaEmpresaEmail();
        unset($_POST);
        //encontra email empresa
        $_POST['fkempresa'] = $idEmpresa;
        $listEmail = $empresaEmail->listObj();
        $emailEm = "";
        foreach ($listEmail as $emailEmpresa) {
            $_POST['id'] = $emailEmpresa->getFkEmail();
            $o = $mEmail->loadObj();
            $emailEm = $o->getEmail();
        }
        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $listTel = $empresaTelefone->listObj();
        $telefone = '';
        $whats = '';

        if (is_array($listTel)) {
            foreach ($listTel as $tel) {
                unset($_POST);
                $_POST['id'] = $tel->getFkTelefone();
                $o = $mTelefone->loadObj();
                $telefone = $o->getTel();
                $whats = $o->getWhatsapp();
            }
        }

        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $consulta = $empresaCosultas->loadObj();
        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $regua = $empresaRegua->loadObj();
        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $resp = $empresaResponsavel->loadObj();
        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $plata = $empresaPlataforma->listObj();

        $plataformaA;
        if (is_array($plata))
            foreach ($plata as $plataforma) {
                $plataformaA [] = $plataforma->getFkPlataforma();
            }
        unset($_POST);
        $especialidadea;
        $_POST['fkempresa'] = $idEmpresa;
        $esp = $empresaEspecialidade->listObj();
        if (is_array($esp))
            foreach ($esp as $especialidade) {
                $especialidadea [] = $especialidade->getFkEspecialidade();
            }
        unset($_POST);
        $convenio;
        $_POST['fkempresa'] = $idEmpresa;
        $con = $empresaConvenio->listObj();
        if (is_array($con))
            foreach ($con as $conve) {
                $convenio [] = $conve->getFkConvenio();
            }
        unset($_POST);
        $_POST['fkempresa'] = $idEmpresa;
        $cont = $mContato->listObj();
        $contatos = '';

        foreach ($cont as $contato) {
            $contatos [] = $contato;
            unset($_POST);
            $_POST['fkcontato'] = $contato->getId();
            $o = $mContatoTelefone->loadObj();
            $tele = '';
            if ($o->getId() > 0) {
                $_POST['id'] = $o->getFkTelefone();
                $tele = $mTelefone->loadObj();
            }
            $contatoTel [] = $tele;
            unset($_POST);
            $_POST['fkcontato'] = $contato->getId();
            $o = $mContatoEmail->loadObj();
            unset($_POST);
            $email = '';
            if ($o->getId() > 0) {
                $_POST['id'] = $o->getFkEmail();
                $email = $mEmail->loadObj();
            }
            $contatoEmail [] = $email;
            unset($_POST);
            $_POST['fkcontato'] = $contato->getId();
            $o = $mMedico->listObj();
            $medicos = $o;
            $array_medico[] = $o;
            if (is_array($medicos)) {
                foreach ($medicos as $medico) {
                    unset($_POST);
                    $_POST['id'] = $medico->getFkCrm();
                    $crms [] = $mCrm->loadObj();
                }
            } else {
                $crms [] = '';
            }

        }
        $dados['crm'] = $crms;
        $dados ['medicos'] = $array_medico;
        $dados ['emailcontato'] = $contatoEmail;
        $dados ['contato'] = $contatos;
        $dados ['telContato'] = $contatoTel;
        $dados ['convenio'] = $convenio;
        $dados ['especialidade'] = $especialidadea;
        $dados ['plataforma'] = $plataformaA;
        $dados ['resp'] = $resp;
        $dados ['regua'] = $regua;
        $dados ['consultas'] = $consulta;
        $dados ['emailempresa'] = $emailEm;
        $dados ['tel'] = $telefone;
        $dados ['whats'] = $whats;
        return $dados;
    }

    public function filtrarUgn($arrayUgn)
    {
        $ugn = '';
        if (isset($_POST['ugn'])) {
            foreach ($_POST['ugn'] as $value) {
                if ($ugn != '') {
                    $ugn .= ',';
                }
                $ugn .= $value;
            }
        } else {
            foreach ($arrayUgn as $key => $value) {
                if ($ugn != '') {
                    $ugn .= ',';
                }
                $ugn .= $key;
            }
        }
        return $ugn;
    }

    public function arrayUgn()
    {

        $ugns = $this->listUgn();
        foreach ($ugns as $ugn) {
            $arrayUgn[$ugn['CODIGO']] = $ugn['UGN'];
        }
        return $arrayUgn;
    }

    public function buscarCrm($crm)
    {
        echo $this->modelPaEmpresa->buscarCrm($crm);
    }

    public function listObjFiltrado()
    {
        if ($_SESSION['id_perfil'] == 2 || $_SESSION['id_perfil'] == 3) {
            $_POST['fkresponsavel'] = $_SESSION['id_colaborador'];
            require_once('/../model/mPaEmpresaResponsavel.php');
            $resp = new mPaEmpresaResponsavel();
            $lista = $resp->listObj();
            if (is_array($lista)) {
                $ids = '';
                $i = 0;
                foreach ($lista as $item) {
                    if ($i >= 1) {
                        $ids .= ',';
                    }
                    $ids .= $item->getFkEmpresa();
                    $i++;
                }
            }
            $_POST['idList'] = $ids;
            if (is_null($ids))
                return;
            return $this->modelPaEmpresa->listObjFiltrado();
        }
        return $this->modelPaEmpresa->listObj();
    }

    public function tirarAcentos($string)
    {
        return preg_replace(array("/(á|à|ã|â|ä)/", "/(Á|À|Ã|Â|Ä)/", "/(é|è|ê|ë)/", "/(É|È|Ê|Ë)/", "/(í|ì|î|ï)/", "/(Í|Ì|Î|Ï)/", "/(ó|ò|õ|ô|ö)/", "/(Ó|Ò|Õ|Ô|Ö)/", "/(ú|ù|û|ü)/", "/(Ú|Ù|Û|Ü)/", "/(ñ)/", "/(Ñ)/"), explode(" ", "a A e E i I o O u U n N"), $string);
    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);
        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresa->listObj();
    }

    public function save()
    {
        require_once('/../model/mPaEmpresaConsultas.php');
        require_once('/../model/mPaEmpresaEmail.php');
        require_once('/../model/mPaEmpresaConvenio.php');
        require_once('/../model/mPaEmpresaEspecialidade.php');
        require_once('/../model/mPaEmpresaPlataforma.php');
        require_once('/../model/mPaEmpresaRegua.php');
        require_once('/../model/mPaEmpresaResponsavel.php');
        require_once('/../model/mPaEmpresaTelefone.php');
        require_once('/../model/mPaTelefone.php');
        require_once('/../model/mPaEmail.php');
        require_once('/../model/mPaRegua.php');
        require_once('/../model/mPaContatoEmail.php');
        require_once('/../model/mPaContatoTelefone.php');
        require_once('/../model/mPaContato.php');
        require_once('/../model/mPaCrm.php');
        require_once('/../model/mPaMedico.php');
        require_once('/../model/mPaContatoTipo.php');
        $mTipoContato = new mPaContatoTipo();
        $mMedico = new mPaMedico();
        $mCrm = new mPaCrm();
        $mContatoEmail = new mPaContatoEmail();
        $mContato = new mPaContato();
        $mContatoTelefone = new mPaContatoTelefone();
        $mTelefone = new mPaTelefone();
        $mRegua = new mPaRegua();
        $mEmail = new mPaEmail();
        $empresaTelefone = new mPaEmpresaTelefone();
        $empresaResponsavel = new mPaEmpresaResponsavel();
        $empresaRegua = new mPaEmpresaRegua();
        $empresaPlataforma = new mPaEmpresaPlataforma();
        $empresaEspecialidade = new mPaEmpresaEspecialidade();
        $empresaConvenio = new mPaEmpresaConvenio();
        $empresaCosultas = new mPaEmpresaConsultas();
        $empresaEmail = new mPaEmpresaEmail();

        $cnpj = $this->limpaCaracteres($_POST['cnpj']);
        $razao = $this->limpaCaracteres($_POST['razao']);
        $editado = $_POST['editado'];
        $fantasia = $this->limpaCaracteres($_POST['fantasia']);
        $cepEmpresa = $this->limpaCaracteres($_POST['cepEmpresa']);
        $bairroEmpresa = $_POST['bairroEmpresa'];
        $cidadeEmpresa = $this->limpaCaracteres($_POST['cidadeEmpresa']);
        $logradouroEmpresa = $this->limpaCaracteres($_POST['logradouroEmpresa']);
        $numeroEmpresa = $_POST['numeroEmpresa'];
        $complementoEmpresa = $_POST['complementoEmpresa'];
        $emailEmpresa = $_POST['emailEmpresa'];
        $estadoEmpresa = $_POST['estadoEmpresa'];
        $telefoneEmpresa = $this->limpaCaracteres($_POST['telefoneEmpresa']);
        $whatsappEmpresa = $_POST['whatsappEmpresa'];
        $numeromedicos = $_POST['numeromedicos'];
        $numeropacientes = $_POST['numeropacientes'];
        $pacientesdia = $_POST['pacientesdia'];
        $valorConsulta = $_POST['valorConsulta'];
        $porcAtendimento = $_POST['porcAtendimento'];
        $btnSimAtendePar = $_POST['btnSimAtendePar'];
        $btnSimCon = $_POST['btnSimCon'];
        $arrayConvenio = $_POST['arrayConvenio'];
        $relacionamentoID = $_POST['relacionamentoID'];
        $responsavel = $_POST['responsavel'];
        $arrayPlataforma = $_POST['arrayPlataforma'];
        $arrayEspecialidade = $_POST['arrayEspecialidade'];
        $arraynome = $_POST['arraynome'];
        $arraydecisor = $_POST['arraydecisor'];
        $arraytipo = $_POST['arraytipo'];
        $arraymedico = $_POST['arraymedico'];
        $arrayresidente = $_POST['arrayresidente'];
        $arraycrm = $_POST['arraycrm'];
        $arrayemail = $_POST['arrayemail'];
        $arraytelefone = $_POST['arraytelefone'];
        $arraywhatsapp = $_POST['arraywhatsapp'];
        $arraydtnascimento = $_POST['arraydtnascimento'];
        $fk_usu = $_SESSION['id_colaborador'];

        //tratar arrays
        $arrayPlataforma = explode(",", $arrayPlataforma);
        $arrayEspecialidade = explode(",", $arrayEspecialidade);
        $arrayConvenio = explode(",", $arrayConvenio);
        $arraymedico = explode(",", $arraymedico);
        $arraynome = explode(",", $arraynome);
        $arraydecisor = explode(",", $arraydecisor);
        $arraycrm = explode(",", $arraycrm);
        $arraytipo = explode(",", $arraytipo);
        $arraywhatsapp = explode(",", $arraywhatsapp);
        $arraydtnascimento = explode(",", $arraydtnascimento);
        $arrayresidente = explode(",", $arrayresidente);
        $arrayemail = explode(",", $arrayemail);
        $arraytelefone = explode(",", $arraytelefone);
        //tratar crm
        foreach ($arraycrm as $value) {
            $uf [] = $value[0] . $value[1];
            $crm [] = $value[2] . $value[3] . $value[4] . $value[5] . $value[6] . $value[7] . $value[8];
        }
        unset($_POST);
        $_POST['relacionamento'] = $relacionamentoID[1];
        $_POST['potencial'] = $relacionamentoID[0];
        $relacionamento = $mRegua->loadObj();
        $relacionamento = $relacionamento->getId();
        unset($_POST);
        $_POST['cnpj'] = $cnpj;
        $_POST['ativo'] = 1;
        $empresa = $this->modelPaEmpresa->loadObj();
        //verifica se empresa existe antes de cadastrar
        if ($empresa->getId() >= 1) {
            if ($editado == 1) {
                $this->modelPaEmpresa->atualizarCnpj($empresa->getId());
            } else {
                echo "CNPJ já Cadastrado";
                return;
            }
        }
        //guarda os dados da empresa
        $this->modelPaEmpresa->saveEmpresa($cnpj, $razao, $fantasia, $cepEmpresa, $cidadeEmpresa, $bairroEmpresa, $logradouroEmpresa, $numeroEmpresa, $complementoEmpresa, $estadoEmpresa, $numeromedicos, $numeropacientes, $pacientesdia, $fk_usu);
        unset($_POST);
        $_POST['cnpj'] = $cnpj;
        $_POST['ativo'] = 1;
        $empresa = $this->modelPaEmpresa->loadObj();
        if ($empresa->getId() >= 1) {
            //aqui guarda o tel da empresa
            $_POST['tel'] = $telefoneEmpresa;
            $_POST['principal'] = 1;
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            $_POST['whatsapp'] = $whatsappEmpresa;
            $mTelefone->save();
            unset($_POST);
            $_POST['tel'] = $telefoneEmpresa;
            $objTelefone = $mTelefone->loadObj();
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fktelefone'] = $objTelefone->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['principal'] = 1;
            $empresaTelefone->save();
            unset($_POST);
            //guardar e-mail
            $_POST['email'] = $emailEmpresa;
            $_POST['fkuser'] = $fk_usu;
            $mEmail->save();
            unset($_POST);
            $_POST['email'] = $emailEmpresa;
            $objEmail = $mEmail->loadObj();
            $_POST['fkemail'] = $objEmail->getId();;
            $_POST['fkempresa'] = $empresa->getId();;
            $_POST['principal'] = 1;
            $_POST['fkuser'] = $fk_usu;
            $empresaEmail->save();
            unset($_POST);
            if ($responsavel > 0) {
                $_POST['fkresponsavel'] = $responsavel;
            } else {
                $_POST['fkresponsavel'] = $fk_usu;
            }
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            $empresaResponsavel->save();
            unset($_POST);
            $_POST['fkregua'] = $relacionamento;
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            $empresaRegua->save();
            unset($_POST);
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            foreach ($arrayPlataforma as $idPlataforma) {
                $_POST['fkplataforma'] = $idPlataforma;
                $empresaPlataforma->save();
            }
            unset($_POST);
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            foreach ($arrayEspecialidade as $idEspecialidade) {
                $_POST['fkespecialidade'] = $idEspecialidade;
                $empresaEspecialidade->save();
            }
            unset($_POST);
            $_POST['fkempresa'] = $empresa->getId();
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            foreach ($arrayConvenio as $idConvenio) {
                $_POST['fkconvenio'] = $idConvenio;
                $empresaConvenio->save();
            }
            unset($_POST);
            $_POST['valor'] = $valorConsulta;
            $_POST['percentual'] = $porcAtendimento;
            $_POST['fkuser'] = $fk_usu;
            $_POST['ativo'] = 1;
            $_POST['fkempresa'] = $empresa->getId();
            if ($valorConsulta != "")
                $empresaCosultas->save();
            unset($_POST);
            $limite = count($arraynome);
            for ($i = 0; $i < $limite; $i++) {
                $_POST['descricao'] = $arraytipo[$i];
                $tipoContato = $mTipoContato->loadObj();
                unset($_POST);
                if ($arraydecisor[$i] == "Sim") {
                    $arraydecisor[$i] = 1;
                } else {
                    $arraydecisor[$i] = 0;
                }
                $_POST['nome'] = $arraynome[$i];
                $_POST['fkuser'] = $fk_usu;
                $_POST['ativo'] = 1;
                $_POST['datanasc'] = $arraydtnascimento[$i];
                $_POST['fkcontatotipo'] = $tipoContato->getId();
                $_POST['fkempresa'] = $empresa->getId();
                $_POST['decisor'] = $arraydecisor[$i];
                $mContato->save();
                $objContato = $mContato->loadObj();
                unset($_POST);
                $_POST['uf'] = $uf[$i];
                $_POST['numero'] = $crm[$i];
                $_POST['fkuser'] = $fk_usu;
                $_POST['ativo'] = 1;
                $mCrm->save();
                unset($_POST);
                $_POST['uf'] = $uf[$i];
                $_POST['numero'] = $crm[$i];
                $_POST['fkuser'] = $fk_usu;
                $_POST['ativo'] = 1;
                if ($crm[$i] == '')
                    $objCrm = 0;
                else
                    $objCrm = $mCrm->loadObj();
                unset($_POST);
                if ($arrayemail[$i] != '') {
                    $_POST['email'] = $arrayemail[$i];
                    $_POST['ativo'] = 1;
                    $_POST['fkuser'] = $fk_usu;
                    $mEmail->save();
                    $objEmail = $mEmail->loadObj();
                }
                unset($_POST);
                if ($arraywhatsapp[$i] == "Sim") {
                    $arraywhatsapp[$i] = 1;
                } else {
                    $arraywhatsapp[$i] = 0;
                }
                $_POST['tel'] = $arraytelefone[$i];
                $_POST['principal'] = 1;
                $_POST['fkuser'] = $fk_usu;
                $_POST['ativo'] = 1;
                $_POST['whatsapp'] = $arraywhatsapp[$i];
                $mTelefone->save();
                $objTelefone = '';
                if ($arraytelefone[$i] != '') {
                    $objTelefone = $mTelefone->loadObj();
                }
                unset($_POST);
                if ($arrayresidente[$i] == "Sim") {
                    $arrayresidente[$i] = 1;
                } else {
                    $arrayresidente[$i] = 0;
                }
                $_POST['fkcontato'] = $objContato->getId();
                $_POST['principal'] = 1;
                $_POST['ativo'] = 1;
                $_POST['fkuser'] = $fk_usu;
                if (is_object($objEmail)) {
                    $_POST['fkemail'] = $objEmail->getId();
                    $mContatoEmail->save();
                }
                if (is_object($objTelefone)) {
                    $_POST['fktelefone'] = $objTelefone->getId();
                    $mContatoTelefone->save();
                }
                if ($objCrm != 0) {
                    $_POST['fkcrm'] = $objCrm->getId();
                    $_POST['residente'] = $arrayresidente[$i];
                    $mMedico->save();
                }
                unset($_POST);
            }
            unset($_POST);
        } else {
            echo "Houve uma falha ao inserir";
            return;
        }
        //var_dump($empresa);
    }
}
