<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaConsultas.php');
require_once('app/view/formsPaEmpresaConsultas.php');

class cPaEmpresaConsultas extends appController {

    private $modelPaEmpresaConsultas = null;

    public function __construct(){
        $this->modelPaEmpresaConsultas = new mPaEmpresaConsultas();
    }

    public function main(){
        $this->render('vPaEmpresaConsultas');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaConsultas();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaConsultas();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaConsultas();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $this->modelPaEmpresaConsultas->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $this->modelPaEmpresaConsultas->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        return $this->modelPaEmpresaConsultas->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        return $this->modelPaEmpresaConsultas->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $this->modelPaEmpresaConsultas->updateObj();
    }
}
