<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaContato.php');

class cPaEmpresaContato extends appController {

    private $modelPaEmpresaContato = null;

    public function __construct(){
        $this->modelPaEmpresaContato = new mPaEmpresaContato();
    }

    public function main(){

    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaContato();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaContato();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaContato();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaContato->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaContato->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaContato->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaContato->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaContato->updateObj();
    }
}
