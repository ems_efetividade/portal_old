<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaConvenio.php');
require_once('app/view/formsPaEmpresaConvenio.php');

class cPaEmpresaConvenio extends appController {

    private $modelPaEmpresaConvenio = null;

    public function __construct(){
        $this->modelPaEmpresaConvenio = new mPaEmpresaConvenio();
    }

    public function main(){
        $this->render('vPaEmpresaConvenio');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaConvenio();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaConvenio();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaConvenio();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmpresa = $_POST['fkempresa'];
        $fkConvenio = $_POST['fkconvenio'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaConvenio->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkConvenio = $_POST['fkconvenio'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaConvenio->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkConvenio = $_POST['fkconvenio'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaConvenio->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkConvenio = $_POST['fkconvenio'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaConvenio->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkConvenio = $_POST['fkconvenio'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaConvenio->updateObj();
    }
}
