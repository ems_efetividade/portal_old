<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaEmail.php');
require_once('app/view/formsPaEmpresaEmail.php');

class cPaEmpresaEmail extends appController {

    private $modelPaEmpresaEmail = null;

    public function __construct(){
        $this->modelPaEmpresaEmail = new mPaEmpresaEmail();
    }

    public function main(){
        $this->render('vPaEmpresaEmail');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaEmail();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaEmail();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaEmail();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmail = $_POST['fkemail'];
        $fkEmpresa = $_POST['fkempresa'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaEmail->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkEmpresa = $_POST['fkempresa'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaEmail->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkEmpresa = $_POST['fkempresa'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaEmail->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkEmpresa = $_POST['fkempresa'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaEmail->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmail = $_POST['fkemail'];
        $fkEmpresa = $_POST['fkempresa'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaEmail->updateObj();
    }
}
