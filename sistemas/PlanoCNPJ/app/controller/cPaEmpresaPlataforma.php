<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaPlataforma.php');
require_once('app/view/formsPaEmpresaPlataforma.php');

class cPaEmpresaPlataforma extends appController {

    private $modelPaEmpresaPlataforma = null;

    public function __construct(){
        $this->modelPaEmpresaPlataforma = new mPaEmpresaPlataforma();
    }

    public function main(){
        $this->render('vPaEmpresaPlataforma');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaPlataforma();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaPlataforma();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaPlataforma();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmpresa = $_POST['fkempresa'];
        $fkPlataforma = $_POST['fkplataforma'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaPlataforma->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkPlataforma = $_POST['fkplataforma'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaPlataforma->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkPlataforma = $_POST['fkplataforma'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaPlataforma->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkPlataforma = $_POST['fkplataforma'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaPlataforma->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkPlataforma = $_POST['fkplataforma'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaPlataforma->updateObj();
    }
}
