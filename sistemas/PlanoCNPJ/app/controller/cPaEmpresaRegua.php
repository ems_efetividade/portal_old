<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaRegua.php');
require_once('app/view/formsPaEmpresaRegua.php');

class cPaEmpresaRegua extends appController {

    private $modelPaEmpresaRegua = null;

    public function __construct(){
        $this->modelPaEmpresaRegua = new mPaEmpresaRegua();
    }

    public function main(){
        $this->render('vPaEmpresaRegua');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaRegua();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaRegua();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaRegua();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkRegua = $_POST['fkregua'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaRegua->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkRegua = $_POST['fkregua'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaRegua->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkRegua = $_POST['fkregua'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaRegua->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkRegua = $_POST['fkregua'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaRegua->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkRegua = $_POST['fkregua'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaRegua->updateObj();
    }
}
