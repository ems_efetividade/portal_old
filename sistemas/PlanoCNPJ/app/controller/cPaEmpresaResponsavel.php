<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaResponsavel.php');
require_once('app/view/formsPaEmpresaResponsavel.php');

class cPaEmpresaResponsavel extends appController {

    private $modelPaEmpresaResponsavel = null;

    public function __construct(){
        $this->modelPaEmpresaResponsavel = new mPaEmpresaResponsavel();
    }

    public function main(){
        $this->render('vPaEmpresaResponsavel');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaResponsavel();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaResponsavel();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaResponsavel();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkResponsavel = $_POST['fkresponsavel'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaResponsavel->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkResponsavel = $_POST['fkresponsavel'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaResponsavel->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkResponsavel = $_POST['fkresponsavel'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaResponsavel->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkResponsavel = $_POST['fkresponsavel'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEmpresaResponsavel->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkResponsavel = $_POST['fkresponsavel'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEmpresaResponsavel->updateObj();
    }
}
