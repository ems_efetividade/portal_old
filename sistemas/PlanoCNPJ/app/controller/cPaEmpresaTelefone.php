<?php
require_once('lib/appController.php');
require_once('app/model/mPaEmpresaTelefone.php');
require_once('app/view/formsPaEmpresaTelefone.php');

class cPaEmpresaTelefone extends appController {

    private $modelPaEmpresaTelefone = null;

    public function __construct(){
        $this->modelPaEmpresaTelefone = new mPaEmpresaTelefone();
    }

    public function main(){
        $this->render('vPaEmpresaTelefone');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaEmpresaTelefone();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaEmpresaTelefone();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaEmpresaTelefone();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $this->modelPaEmpresaTelefone->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $this->modelPaEmpresaTelefone->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        return $this->modelPaEmpresaTelefone->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        return $this->modelPaEmpresaTelefone->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $this->modelPaEmpresaTelefone->updateObj();
    }
}
