<?php
require_once('lib/appController.php');
require_once('app/model/mPaEspecialidade.php');

class cPaEspecialidade extends appController {

    private $modelPaEspecialidade = null;

    public function __construct(){
        $this->modelPaEspecialidade = new mPaEspecialidade();
    }

    public function main(){
        //$this->render('vPaEspecialidade');
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEspecialidade->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEspecialidade->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEspecialidade->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaEspecialidade->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaEspecialidade->updateObj();
    }
}
