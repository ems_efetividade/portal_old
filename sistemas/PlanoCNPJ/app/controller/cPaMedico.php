<?php
require_once('lib/appController.php');
require_once('app/model/mPaMedico.php');
require_once('app/view/formsPaMedico.php');

class cPaMedico extends appController {

    private $modelPaMedico = null;

    public function __construct(){
        $this->modelPaMedico = new mPaMedico();
    }

    public function main(){
        $this->render('vPaMedico');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaMedico();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaMedico();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaMedico();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $this->modelPaMedico->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $this->modelPaMedico->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        return $this->modelPaMedico->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        return $this->modelPaMedico->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $this->modelPaMedico->updateObj();
    }
}
