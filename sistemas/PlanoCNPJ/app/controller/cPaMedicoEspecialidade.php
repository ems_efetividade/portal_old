<?php
require_once('lib/appController.php');
require_once('app/model/mPaMedicoEspecialidade.php');
require_once('app/view/formsPaMedicoEspecialidade.php');

class cPaMedicoEspecialidade extends appController {

    private $modelPaMedicoEspecialidade = null;

    public function __construct(){
        $this->modelPaMedicoEspecialidade = new mPaMedicoEspecialidade();
    }

    public function main(){
        $this->render('vPaMedicoEspecialidade');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaMedicoEspecialidade();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaMedicoEspecialidade();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaMedicoEspecialidade();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkMedico = $_POST['fkmedico'];
        $fkEspecialidade = $_POST['fkespecialidade'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaMedicoEspecialidade->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkMedico = $_POST['fkmedico'];
        $fkEspecialidade = $_POST['fkespecialidade'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaMedicoEspecialidade->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkMedico = $_POST['fkmedico'];
        $fkEspecialidade = $_POST['fkespecialidade'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaMedicoEspecialidade->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkMedico = $_POST['fkmedico'];
        $fkEspecialidade = $_POST['fkespecialidade'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaMedicoEspecialidade->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkMedico = $_POST['fkmedico'];
        $fkEspecialidade = $_POST['fkespecialidade'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaMedicoEspecialidade->updateObj();
    }
}
