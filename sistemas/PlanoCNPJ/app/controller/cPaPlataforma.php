<?php
require_once('lib/appController.php');
require_once('app/model/mPaPlataforma.php');


class cPaPlataforma extends appController {

    private $modelPaPlataforma = null;

    public function __construct(){
        $this->modelPaPlataforma = new mPaPlataforma();
    }

    public function main(){

    }



    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaPlataforma->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaPlataforma->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaPlataforma->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        return $this->modelPaPlataforma->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $this->modelPaPlataforma->updateObj();
    }
}
