<?php
require_once('lib/appController.php');
require_once('app/model/mPaRegua.php');
require_once('app/view/formsPaRegua.php');

class cPaRegua extends appController {

    private $modelPaRegua = null;

    public function __construct(){
        $this->modelPaRegua = new mPaRegua();
    }

    public function main(){
        $this->render('vPaRegua');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaRegua();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaRegua();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaRegua();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $relacionamento = $_POST['relacionamento'];
        $potencial = $_POST['potencial'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaRegua->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $relacionamento = $_POST['relacionamento'];
        $potencial = $_POST['potencial'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaRegua->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $relacionamento = $_POST['relacionamento'];
        $potencial = $_POST['potencial'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaRegua->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $relacionamento = $_POST['relacionamento'];
        $potencial = $_POST['potencial'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        return $this->modelPaRegua->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $relacionamento = $_POST['relacionamento'];
        $potencial = $_POST['potencial'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];

        $this->modelPaRegua->updateObj();
    }
}
