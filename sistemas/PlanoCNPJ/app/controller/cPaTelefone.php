<?php
require_once('lib/appController.php');
require_once('app/model/mPaTelefone.php');
require_once('app/view/formsPaTelefone.php');

class cPaTelefone extends appController {

    private $modelPaTelefone = null;

    public function __construct(){
        $this->modelPaTelefone = new mPaTelefone();
    }

    public function main(){
        $this->render('vPaTelefone');
    }

    public function controlSwitch($acao){
        $pesquisa = $_POST['isPesquisa'];
        if($pesquisa=='1')
            $acao = 'pesquisar';
        switch ($acao){
            case 'cadastrar':
                $forms = new formsPaTelefone();
                $forms->formCadastrar();
                break;
            case 'salvar':
                $this->save();
                break;
            case 'carregar':
                $forms = new formsPaTelefone();
                $objeto = $this->loadObj();
                $forms->formVisualizar($objeto);
                break;
            case 'editar':
                $forms = new formsPaTelefone();
                $objeto = $this->loadObj();
                $forms->formEditar($objeto);
                break;
            case 'atualizar':
                $this->updateObj();
                break;
            case 'excluir':
                $this->delete();
                break;
            case 'pesquisar':
                $this->main();
                break;
            default:
                $this->main();
                break;
        }
    }


    public function save(){
        $_POST = appSanitize::filter($_POST);

        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $this->modelPaTelefone->save();
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $this->modelPaTelefone->delete();
    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        return $this->modelPaTelefone->listObj();
    }

    public function loadObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        return $this->modelPaTelefone->loadObj();
    }

    public function updateObj(){

        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $this->modelPaTelefone->updateObj();
    }
}
