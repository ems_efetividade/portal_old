<?php


class mContato
{
    public $id;
    public $nome;
    public $dataNasc;
    public $contatoTipo;
    public $decisor;
    public $telefone;
    public $email;
    public $whatsapp;
    public $crm;
    public $uf;
    public $residente;

    public function __construct($id='', $nome='', $dataNasc='', $contatoTipo='', $decisor='', $telefone='', $email='', $whatsapp='', $crm='', $uf='', $residente='')
    {
        $this->id = $id;
        $this->nome = $nome;
        $this->dataNasc = $dataNasc;
        $this->contatoTipo = $contatoTipo;
        $this->decisor = $decisor;
        $this->telefone = $telefone;
        $this->email = $email;
        $this->whatsapp = $whatsapp;
        $this->crm = $crm;
        $this->uf = $uf;
        $this->residente = $residente;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome)
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getDataNasc()
    {
        return $this->dataNasc;
    }

    /**
     * @param mixed $dataNasc
     */
    public function setDataNasc($dataNasc)
    {
        $this->dataNasc = $dataNasc;
    }

    /**
     * @return mixed
     */
    public function getContatoTipo()
    {
        return $this->contatoTipo;
    }

    /**
     * @param mixed $contatoTipo
     */
    public function setContatoTipo($contatoTipo)
    {
        $this->contatoTipo = $contatoTipo;
    }

    /**
     * @return mixed
     */
    public function getDecisor()
    {
        return $this->decisor;
    }

    /**
     * @param mixed $decisor
     */
    public function setDecisor($decisor)
    {
        $this->decisor = $decisor;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getWhatsapp()
    {
        return $this->whatsapp;
    }

    /**
     * @param mixed $whatsapp
     */
    public function setWhatsapp($whatsapp)
    {
        $this->whatsapp = $whatsapp;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return mixed
     */
    public function getUf()
    {
        return $this->uf;
    }

    /**
     * @param mixed $uf
     */
    public function setUf($uf)
    {
        $this->uf = $uf;
    }

    /**
     * @return mixed
     */
    public function getResidente()
    {
        return $this->residente;
    }

    /**
     * @param mixed $residente
     */
    public function setResidente($residente)
    {
        $this->residente = $residente;
    }

    /**
     * mContato constructor.
     * @param $id
     * @param $nome
     * @param $dataNasc
     * @param $contatoTipo
     * @param $decisor
     * @param $telefone
     * @param $email
     * @param $whatsapp
     * @param $crm
     * @param $uf
     * @param $residente
     */



}