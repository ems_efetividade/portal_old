<?php
require_once('lib/appConexao.php');

class mPaContato extends appConexao implements gridInterface {

    private $id;
    private $nome;
    private $fkUser;
    private $ativo;
    private $dataIn;
    private $dataNasc;
    private $fkContatoTipo;
    private $fkEmpresa;
    private $decisor;

    public function __construct(){
        $this->id;
        $this->nome;
        $this->fkUser;
        $this->ativo;
        $this->dataIn;
        $this->dataNasc;
        $this->fkContatoTipo;
        $this->fkEmpresa;
        $this->decisor;
    }

    public function getId(){
        return $this->id;
    }

    public function getNome(){
        return $this->nome;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function getDataNasc(){
        return $this->dataNasc;
    }

    public function getFkContatoTipo(){
        return $this->fkContatoTipo;
    }

    public function getFkEmpresa(){
        return $this->fkEmpresa;
    }

    public function getDecisor(){
        return $this->decisor;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setNome($Nome){
        $this->nome = $Nome;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function setDataNasc($DataNasc){
        $this->dataNasc = $DataNasc;
    }

    public function setFkContatoTipo($FkContatoTipo){
        $this->fkContatoTipo = $FkContatoTipo;
    }

    public function setFkEmpresa($FkEmpresa){
        $this->fkEmpresa = $FkEmpresa;
    }

    public function setDecisor($Decisor){
        $this->decisor = $Decisor;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $sql = "select count(id) from PA_CONTATO";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($dataNasc, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_NASC IN ('$dataNasc') ";
            $verif = true;
        }
        if(strcmp($fkContatoTipo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO_TIPO LIKE '%$fkContatoTipo%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($decisor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DECISOR LIKE '%$decisor%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $sql = "INSERT INTO PA_CONTATO ([NOME],[FK_USER],[DATA_NASC],[FK_CONTATO_TIPO],[FK_EMPRESA],[DECISOR])";
        $sql .=" VALUES ('$nome','$fkUser','$dataNasc','$fkContatoTipo','$fkEmpresa','$decisor')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $sql = "DELETE FROM PA_CONTATO WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="NOME = '$nome' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($dataNasc, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_NASC IN ('$dataNasc') ";
            $verif = true;
        }
        if(strcmp($fkContatoTipo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_CONTATO_TIPO = '$fkContatoTipo' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_EMPRESA = '$fkEmpresa' ";
            $verif = true;
        }
        if(strcmp($decisor, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DECISOR = '$decisor' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "DATA_NASC ";
        $sql .= ",";
        $sql .= "FK_CONTATO_TIPO ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "DECISOR ";
        $sql .= " FROM PA_CONTATO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($dataNasc, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_NASC IN ('$dataNasc') ";
            $verif = true;
        }
        if(strcmp($fkContatoTipo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO_TIPO LIKE '%$fkContatoTipo%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($decisor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DECISOR LIKE '%$decisor%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];
        $sql = "UPDATE  PA_CONTATO";
        $sql .=" SET ";
        $sql .="NOME = '$nome' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" , ";
        $sql .="DATA_NASC = '$dataNasc' ";
        $sql .=" , ";
        $sql .="FK_CONTATO_TIPO = '$fkContatoTipo' ";
        $sql .=" , ";
        $sql .="FK_EMPRESA = '$fkEmpresa' ";
        $sql .=" , ";
        $sql .="DECISOR = '$decisor' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $nome = $_POST['nome'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $dataNasc = $_POST['datanasc'];
        $fkContatoTipo = $_POST['fkcontatotipo'];
        $fkEmpresa = $_POST['fkempresa'];
        $decisor = $_POST['decisor'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "NOME ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "DATA_NASC ";
        $sql .= ",";
        $sql .= "FK_CONTATO_TIPO ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "DECISOR ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_CONTATO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($nome, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="NOME LIKE '%$nome%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($dataNasc, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_NASC IN ('$dataNasc') ";
            $verif = true;
        }
        if(strcmp($fkContatoTipo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_CONTATO_TIPO = $fkContatoTipo ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_EMPRESA = $fkEmpresa ";
            $verif = true;
        }
        if(strcmp($decisor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DECISOR LIKE '%$decisor%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
     
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaContato();
        $o->setId($param[0]);
        $o->setNome($param[1]);
        $o->setFkUser($param[2]);
        $o->setAtivo($param[3]);
        $o->setDataIn($param[4]);
        $o->setDataNasc($param[5]);
        $o->setFkContatoTipo($param[6]);
        $o->setFkEmpresa($param[7]);
        $o->setDecisor($param[8]);

        return $o;

    }
}