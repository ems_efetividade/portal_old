<?php
require_once('lib/appConexao.php');

class mPaContatoTelefone extends appConexao implements gridInterface {

    private $id;
    private $ddd;
    private $fkTelefone;
    private $fkContato;
    private $principal;
    private $fkUser;
    private $ativo;
    private $dataIn;

    public function __construct(){
        $this->id;
        $this->ddd;
        $this->fkTelefone;
        $this->fkContato;
        $this->principal;
        $this->fkUser;
        $this->ativo;
        $this->dataIn;
    }

    public function getId(){
        return $this->id;
    }

    public function getDdd(){
        return $this->ddd;
    }

    public function getFkTelefone(){
        return $this->fkTelefone;
    }

    public function getFkContato(){
        return $this->fkContato;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setDdd($Ddd){
        $this->ddd = $Ddd;
    }

    public function setFkTelefone($FkTelefone){
        $this->fkTelefone = $FkTelefone;
    }

    public function setFkContato($FkContato){
        $this->fkContato = $FkContato;
    }

    public function setPrincipal($Principal){
        $this->principal = $Principal;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from PA_CONTATO_TELEFONE";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_TELEFONE LIKE '%$fkTelefone%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "INSERT INTO PA_CONTATO_TELEFONE ([DDD],[FK_TELEFONE],[FK_CONTATO],[PRINCIPAL],[FK_USER])";
        $sql .=" VALUES ('$ddd','$fkTelefone','$fkContato','$principal','$fkUser')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM PA_CONTATO_TELEFONE WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DDD = '$ddd' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_TELEFONE = '$fkTelefone' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_CONTATO = '$fkContato' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="PRINCIPAL = '$principal' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "DDD ";
        $sql .= ",";
        $sql .= "FK_TELEFONE ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM PA_CONTATO_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_TELEFONE = $fkTelefone ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO = $fkContato ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;
        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  PA_CONTATO_TELEFONE";
        $sql .=" SET ";
        $sql .="DDD = '$ddd' ";
        $sql .=" , ";
        $sql .="FK_TELEFONE = '$fkTelefone' ";
        $sql .=" , ";
        $sql .="FK_CONTATO = '$fkContato' ";
        $sql .=" , ";
        $sql .="PRINCIPAL = '$principal' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $fkTelefone = $_POST['fktelefone'];
        $fkContato = $_POST['fkcontato'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "DDD ";
        $sql .= ",";
        $sql .= "FK_TELEFONE ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_CONTATO_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_TELEFONE LIKE '%$fkTelefone%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaContatoTelefone();
        $o->setId($param[0]);
        $o->setDdd($param[1]);
        $o->setFkTelefone($param[2]);
        $o->setFkContato($param[3]);
        $o->setPrincipal($param[4]);
        $o->setFkUser($param[5]);
        $o->setAtivo($param[6]);
        $o->setDataIn($param[7]);

        return $o;

    }
}