<?php
require_once('lib/appConexao.php');

class mPaEmpresa extends appConexao implements gridInterface
{
    private $id;
    private $cnpj;
    private $razaoSocial;
    private $fantasia;
    private $cep;
    private $cidade;
    private $bairro;
    private $logradouro;
    private $numero;
    private $complemento;
    private $estado;
    private $numeroMedicos;
    private $numeroPacientes;
    private $pacientesDia;
    private $fkUser;
    private $ativo;
    private $dataIn;

    public function __construct()
    {
        $this->id;
        $this->cnpj;
        $this->razaoSocial;
        $this->fantasia;
        $this->cep;
        $this->cidade;
        $this->bairro;
        $this->logradouro;
        $this->numero;
        $this->complemento;
        $this->estado;
        $this->numeroMedicos;
        $this->numeroPacientes;
        $this->pacientesDia;
        $this->fkUser;
        $this->ativo;
        $this->dataIn;
    }

    public function listarAtividades($idUser)
    {
        $sql = "SELECT * FROM PA_EMPRESA_ATIVIDADE  where FK_USER = $idUser ORDER BY DATA_ATIVIDADE";
        return $this->executarQueryArray($sql);
    }

    public function loadAtividade($idAtividade)
    {
        $sql = "SELECT * FROM PA_EMPRESA_ATIVIDADE  where ID = $idAtividade";
        return $this->executarQueryArray($sql);
    }

    public function getNomeResposavelByIdEmpresa($id)
    {
        $sql = "SELECT NOME FROM COLABORADOR C INNER JOIN PA_EMPRESA_RESPONSAVEL ER ON ER.FK_RESPONSAVEL = C.ID_COLABORADOR WHERE ER.FK_EMPRESA = $id";
        return $this->executarQueryArray($sql);
    }

    public function alterarResponsavel($id, $idEmpresa, $fkUser)
    {
        $sql = "UPDATE PA_EMPRESA_RESPONSAVEL SET FK_RESPONSAVEL = $id ,FK_USER = $fkUser WHERE FK_EMPRESA = $idEmpresa";
        return $this->executar($sql);
    }

    public function salvarPma()
    {
        session_start();
        $idEmpresa = $_POST['idEmpresa'];
        $data = $_POST['data'];
        $obser = $_POST['msg'];
        $idColaborador = $_POST['resp'];
        $contatos = $_POST['contatos'];
        $idsetor = $this->getSetorByIdColaborados($idColaborador);
        $idsetor = $idsetor[1][0];
        $empresa = $this->loadDadosEmpresabyId($idEmpresa);
        $sql = "SELECT ID_CICLO FROM PMA_CICLO WHERE INICIO <= '$data' AND FIM >= '$data'";
        $ciclo = $this->executarQueryArray($sql);
        $idCiclo = $ciclo[1]['ID_CICLO'];
        $empresaNome = $empresa['empresa'][1]['FANTASIA'];
        $sql = "INSERT INTO PMA_ATIVIDADE (ID_CICLO, ID_TIPO, ID_STATUS, ID_SETOR_GER, ID_COLABORADOR_GER, DATA_EVENTO, OBJETIVO,ID_TIPO_TROCA)";
        $sql .= " VALUES ($idCiclo,44,1,$idsetor,$idColaborador,'$data','$obser',0) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idPma = $this->executarQueryArray($sql);
        $idPma = $idPma[1][0];
        $sql = "INSERT INTO PA_EMPRESA_ATIVIDADE (DESCRICAO, DATA_ATIVIDADE, FK_EMPRESA, FK_USER, FK_PMA)";
        $sql .= " VALUES ('$obser','$data',$idEmpresa,$idColaborador,$idPma) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idAtividade = $this->executarQueryArray($sql);
        $idAtividade = $idAtividade[1][0];
        $sql = '';
        if (is_array($contatos)) {
            foreach ($contatos as $contato) {
                $sql .= "INSERT INTO PA_CONTATO_ATIVIDADE (FK_ATIVIDADE, FK_CONTATO, FK_USER)";
                $sql .= " VALUES ($idAtividade,$contato,$idColaborador); ";
            }
            $this->executarQueryArray($sql);
        }
    }

    public function informacoesContatos($ids)
    {
        $sql = "SELECT C.NOME, PE.EMAIL, PT.TEL FROM PA_CONTATO C LEFT JOIN PA_CONTATO_EMAIL CE ON CE.FK_CONTATO = C.ID LEFT JOIN PA_EMAIL PE on CE.FK_EMAIL = PE.ID LEFT JOIN PA_CONTATO_TELEFONE PCT on C.ID = PCT.FK_CONTATO LEFT JOIN PA_TELEFONE PT on PCT.FK_TELEFONE = PT.ID WHERE C.ID IN ($ids)";
        return $this->executarQueryArray($sql);
    }

    public function listConsultoria()
    {
        $sql = "SELECT * FROM PA_CONSULTORIA";
        return $this->executarQueryArray($sql);
    }

    public function listSituacao()
    {
        $sql = "SELECT * FROM PA_CONSULTORIA_SITUACAO";
        return $this->executarQueryArray($sql);
    }

    public function atualizaDadosAdicionaisByIdEmpresa($idEmpresa)
    {
        $sql = "SELECT NUMERO_MEDICOS,NUMERO_PACIENTES,PACIENTES_DIA FROM PA_EMPRESA WHERE ID = $idEmpresa";
        $dadosEmpresa = $this->executarQueryArray($sql);
        $sql = "SELECT FK_RESPONSAVEL FROM PA_EMPRESA_RESPONSAVEL WHERE FK_EMPRESA = $idEmpresa";
        $responsavel = $this->executarQueryArray($sql);
        $sql = "SELECT VALOR, PERCENTUAL FROM PA_EMPRESA_CONSULTAS WHERE FK_EMPRESA = $idEmpresa";
        $consultas = $this->executarQueryArray($sql);
        $sql = "SELECT FK_PLATAFORMA FROM PA_EMPRESA_PLATAFORMA WHERE FK_EMPRESA = $idEmpresa";
        $plataforma = $this->executarQueryArray($sql);
        $sql = "SELECT FK_CONVENIO FROM PA_EMPRESA_CONVENIO WHERE FK_EMPRESA = $idEmpresa";
        $convenio = $this->executarQueryArray($sql);
        $sql = "SELECT FK_ESPECIALIDADE FROM PA_EMPRESA_ESPECIALIDADE WHERE FK_EMPRESA = $idEmpresa";
        $especialidade = $this->executarQueryArray($sql);
        $sql = "SELECT FK_REGUA FROM PA_EMPRESA_REGUA WHERE FK_EMPRESA = $idEmpresa";
        $regua = $this->executarQueryArray($sql);
    }

    public function dadosAdicionais($idEmpresa)
    {
        $dados = '';
        $sql = "SELECT NUMERO_MEDICOS,NUMERO_PACIENTES,PACIENTES_DIA FROM PA_EMPRESA WHERE ID = $idEmpresa";
        $dados[] = $dadosEmpresa = $this->executarQueryArray($sql);
        $sql = "SELECT FK_RESPONSAVEL FROM PA_EMPRESA_RESPONSAVEL WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $responsavel = $this->executarQueryArray($sql);
        $sql = "SELECT VALOR, PERCENTUAL FROM PA_EMPRESA_CONSULTAS WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $consultas = $this->executarQueryArray($sql);
        $sql = "SELECT FK_PLATAFORMA FROM PA_EMPRESA_PLATAFORMA WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $plataforma = $this->executarQueryArray($sql);
        $sql = "SELECT FK_CONVENIO FROM PA_EMPRESA_CONVENIO WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $convenio = $this->executarQueryArray($sql);
        $sql = "SELECT FK_ESPECIALIDADE FROM PA_EMPRESA_ESPECIALIDADE WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $especialidade = $this->executarQueryArray($sql);
        $sql = "SELECT FK_REGUA FROM PA_EMPRESA_REGUA WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $regua = $this->executarQueryArray($sql);
        $sql = "SELECT OBJETIVO,PRAZO FROM PA_EMPRESA_OBJETIVO WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $regua = $this->executarQueryArray($sql);
        $sql = "SELECT FK_CONSULTORIA,FK_SITUACAO FROM PA_EMPRESA_CONSULTORIA WHERE FK_EMPRESA = $idEmpresa";
        $dados[] = $regua = $this->executarQueryArray($sql);
        return $dados;
    }

    public function excluirContato($idContato)
    {
        $sql = "UPDATE PA_CONTATO SET ATIVO = 0 WHERE ID = $idContato";
        $this->executar($sql);
    }

    public function loadDadosEmpresabyId($id)
    {
        $sql = "SELECT CNPJ,RAZAO_SOCIAL,FANTASIA,CEP,CIDADE,BAIRRO,LOGRADOURO,NUMERO,COMPLEMENTO,ESTADO,FK_USER FROM PA_EMPRESA WHERE ID = $id";
        $empresa = $this->executarQueryArray($sql);
        $result['empresa'] = $empresa;
        $sql = "SELECT FK_TELEFONE FROM PA_EMPRESA_TELEFONE WHERE FK_EMPRESA = $id";
        $fkTelefone = $this->executarQueryArray($sql);
        $fkTelefone = $fkTelefone[1][0];
        $sql = "SELECT TEL,WHATSAPP FROM PA_TELEFONE WHERE ID = $fkTelefone";
        $telefone = $this->executarQueryArray($sql);
        $result['telefone'] = $telefone;
        $sql = "SELECT FK_EMAIL FROM PA_EMPRESA_EMAIL WHERE FK_EMPRESA = $id";
        $fkEmail = $this->executarQueryArray($sql);
        $fkEmail = $fkEmail[1][0];
        $sql = "SELECT EMAIL FROM PA_EMAIL WHERE ID = $fkEmail";
        $email = $this->executarQueryArray($sql);
        $result['email'] = $email;

        return $result;
    }

    public function salvarContatos($verificadorMedico, $idEmpresa, $fkUser, $dataNasc, $nome, $email, $telefone, $whatsapp, $crm, $estadoMedico, $residente, $medico, $decisor, $tipoContato)
    {
        $sql = "INSERT INTO PA_CONTATO (NOME, FK_USER, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR) VALUES ('$nome',$fkUser,'$dataNasc',$tipoContato,$idEmpresa,$decisor) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idContato = $this->executarQueryArray($sql);
        $idContato = $idContato[1][0];
        $sql = "INSERT INTO PA_CRM (UF, NUMERO, FK_USER) VALUES ('$estadoMedico','$crm',$fkUser) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idCrm = $this->executarQueryArray($sql);
        $idCrm = $idCrm[1][0];
        $sql = "INSERT INTO PA_MEDICO (FK_CRM, FK_CONTATO,FK_USER,RESIDENTE) VALUES($idCrm,$idContato,$fkUser,$residente)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $sql = "INSERT INTO PA_EMAIL (EMAIL,FK_USER) VALUES ('$email',$fkUser) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idEmail = $this->executarQueryArray($sql);
        $idEmail = $idEmail[1][0];
        $sql = "INSERT INTO PA_CONTATO_EMAIL (FK_EMAIL,FK_CONTATO,FK_USER ) VALUES ($idEmail,$idContato,$fkUser)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $sql = "INSERT INTO PA_TELEFONE (TEL,FK_USER,WHATSAPP) VALUES ('$telefone',$fkUser,$whatsapp) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idTelefone = $this->executarQueryArray($sql);
        $idTelefone = $idTelefone[1][0];
        $sql = "INSERT INTO PA_CONTATO_TELEFONE (FK_TELEFONE, FK_CONTATO, FK_USER) VALUES ($idTelefone,$idContato,$fkUser)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $sql = "INSERT INTO PA_MEDICO_PENDENTE (FK_CONTATO,FK_EMPRESA,SITUACAO,FK_USER) VALUES ($idContato,$idEmpresa,$verificadorMedico,$fkUser)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        return $idContato;
    }

    public function salvarEmpresa($fkUser, $whatsapp, $complemento, $cnpj, $cidade, $bairro, $logradouro, $estado, $fantasia, $razao, $telefone, $email, $numero, $cep)
    {
        $sql = "INSERT INTO PA_EMPRESA (CNPJ,RAZAO_SOCIAL,FANTASIA,CEP,CIDADE,BAIRRO,LOGRADOURO,NUMERO,COMPLEMENTO,ESTADO,FK_USER) ";
        $sql .= "VALUES ('$cnpj','$razao','$fantasia','$cep','$cidade','$bairro','$logradouro','$numero','$complemento','$estado',$fkUser)";
        $sql .= " SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idEmpresa = $this->executarQueryArray($sql);
        $idEmpresa = $idEmpresa[1][0];
        $sql = "INSERT INTO PA_TELEFONE (TEL,WHATSAPP,FK_USER) VALUES ('$telefone',$whatsapp,$fkUser) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idTelefone = $this->executarQueryArray($sql);
        $idTelefone = $idTelefone[1][0];
        $sql = "INSERT INTO PA_EMAIL (EMAIL,FK_USER) VALUES ('$email',$fkUser) SELECT SCOPE_IDENTITY()";
        $sql = utf8_decode($sql);
        $idEmail = $this->executarQueryArray($sql);
        $idEmail = $idEmail[1][0];
        $sql = "INSERT INTO PA_EMPRESA_TELEFONE(FK_EMPRESA, FK_TELEFONE, FK_USER) VALUES($idEmpresa,$idTelefone,$fkUser)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $sql = "INSERT INTO PA_EMPRESA_EMAIL(FK_EMAIL, FK_EMPRESA, FK_USER) VALUES ($idEmail,$idEmpresa,$fkUser)";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        return $idEmpresa;

    }

    public function salvarDadosAdcionais($consultoria, $situacao, $objetivo, $previsao, $relacionamento, $fkUser, $idEmpresa, $responsavel, $nMedicos, $nPacientes, $consultasDia, $valorConsulta, $porcAtendimento, $arrayPlataforma, $arrayEspecialidade, $arrayConvenio)
    {
        $sql = "INSERT INTO PA_EMPRESA_CONSULTORIA (FK_EMPRESA,FK_CONSULTORIA,FK_SITUACAO,FK_USER) VALUES ($idEmpresa,$consultoria,$situacao,$fkUser)";
        $this->executar($sql);
        $sql = "INSERT INTO PA_EMPRESA_OBJETIVO (OBJETIVO,FK_EMPRESA,PRAZO, FK_USER)  VALUES ($objetivo,$idEmpresa,'$previsao',$fkUser)";
        $this->executar($sql);
        $sql = "UPDATE PA_EMPRESA SET NUMERO_MEDICOS = $nMedicos,NUMERO_PACIENTES = $nPacientes,PACIENTES_DIA = $consultasDia WHERE ID = $idEmpresa";
        $this->executar($sql);
        $sql = "INSERT INTO PA_EMPRESA_RESPONSAVEL (FK_RESPONSAVEL, FK_EMPRESA, FK_USER) VALUES ($responsavel,$idEmpresa,$fkUser)";
        $this->executar($sql);
        if ($valorConsulta != '') {
            $sql = "INSERT INTO PA_EMPRESA_CONSULTAS (VALOR, PERCENTUAL, FK_USER, FK_EMPRESA) values ('$valorConsulta','$porcAtendimento',$fkUser,$idEmpresa)";
            $this->executar($sql);
        }
        foreach ($arrayPlataforma as $plataforma) {
            $sql = "INSERT INTO PA_EMPRESA_PLATAFORMA (FK_EMPRESA, FK_PLATAFORMA, FK_USER) VALUES ($idEmpresa,$plataforma,$fkUser)";
            $this->executar($sql);
        }
        if (is_array($arrayConvenio)) {
            foreach ($arrayConvenio as $convenio) {
                $sql = "INSERT INTO PA_EMPRESA_CONVENIO (FK_EMPRESA, FK_CONVENIO, FK_USER)  VALUES ($idEmpresa,$convenio,$fkUser)";
                $this->executar($sql);
            }
        }
        foreach ($arrayEspecialidade as $especialidade) {
            $sql = "INSERT INTO PA_EMPRESA_ESPECIALIDADE (FK_EMPRESA, FK_ESPECIALIDADE, FK_USER)  VALUES ($idEmpresa,$especialidade,$fkUser)";
            $this->executar($sql);
        }
        $sql = "SELECT ID FROM PA_REGUA WHERE POTENCIAL = '$relacionamento[0]' AND RELACIONAMENTO = $relacionamento[1]";
        $fkregua = $this->executarQueryArray($sql);
        $fkregua = $fkregua[1][0];
        $sql = "INSERT INTO PA_EMPRESA_REGUA (FK_REGUA, FK_EMPRESA, FK_USER) VALUES($fkregua,$idEmpresa,$fkUser)";
        $this->executar($sql);
        return $idEmpresa;
    }

    public function getRelacionamentoById($id)
    {
        $sql = "SELECT POTENCIAL,RELACIONAMENTO FROM PA_REGUA WHERE ID = $id";
        return $this->executarQueryArray($sql);
    }

    public function updateContatos($idEmpresa, $editar)
    {
        $sql = " UPDATE PA_CONTATO SET FK_EMPRESA = $idEmpresa WHERE FK_EMPRESA = $editar";
        $this->executar($sql);
    }

    public function listarContatosByIdEmpresa($idEmpresa)
    {
        $sql = "SELECT PA_CONTATO.ID, PA_CONTATO.NOME, PA_CONTATO.DATA_NASC, PA_CONTATO_TIPO.DESCRICAO, PA_CONTATO.DECISOR ";
        $sql .= " FROM PA_CONTATO INNER JOIN PA_CONTATO_TIPO ON PA_CONTATO.FK_CONTATO_TIPO = PA_CONTATO_TIPO.ID ";
        $sql .= " WHERE FK_EMPRESA = $idEmpresa AND PA_CONTATO.ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function getCrmByIdContato($idContato)
    {
        $sql = "SELECT UF, NUMERO,RESIDENTE FROM PA_MEDICO PM JOIN PA_CRM PC on PM.FK_CRM = PC.ID WHERE FK_CONTATO = $idContato";
        return $this->executarQueryArray($sql);
    }

    public function getTelefoneByIdContato($idContato)
    {
        $sql = "SELECT FK_TELEFONE FROM PA_CONTATO_TELEFONE WHERE FK_CONTATO = $idContato";
        $idTelefone = $this->executarQueryArray($sql);
        $idTelefone = $idTelefone[1][0];
        if ($idTelefone > 0) {
            $sql = "SELECT TEL,WHATSAPP FROM PA_TELEFONE WHERE ID = $idTelefone";
            return $this->executarQueryArray($sql);
        }
        return '';
    }

    public function getEmailByIdContato($idContato)
    {
        $sql = "SELECT FK_EMAIL FROM PA_CONTATO_EMAIL WHERE FK_CONTATO = $idContato";
        $idEmail = $this->executarQueryArray($sql);
        $idEmail = $idEmail[1][0];
        if ($idEmail > 0) {
            $sql = "SELECT EMAIL FROM PA_EMAIL WHERE ID = $idEmail";
            return $this->executarQueryArray($sql);
        }
        return '';
    }

    public function atualizarCnpj($id)
    {
        $sql = "update PA_EMPRESA set ativo = 0 where ID = $id";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }

    public function verificaCNPJ($cnpj)
    {
        $sql = "SELECT ID FROM PA_EMPRESA WHERE CNPJ = '$cnpj' AND ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function localizarIdCrm($crm)
    {
        $sql = "SELECT ID FROM PS_DIM_CRM WHERE CRM = '$crm'";
        return $this->executarQueryArray($sql);
    }

    public function countContatosByIdEmpresa($idEmpresa)
    {
        $sql = "SELECT COUNT(ID) AS CONTATOS FROM PA_CONTATO WHERE ATIVO = 1 AND FK_EMPRESA = $idEmpresa";
        return $this->executarQueryArray($sql);
    }

    public function countCnpjByUgn($ugn)
    {
        $sql = "SELECT count(distinct(E.ID)) AS CNPJ FROM PA_EMPRESA E INNER JOIN PA_CEP_BRICK C ON C.CEP = E.CEP INNER JOIN SETOR_BRICK S ON S.BRICK = C.BRICK INNER JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1)WHERE E.ATIVO = 1 AND U.CODIGO IN($ugn)";
        return $this->executarQueryArray($sql);
    }

    public function countCnpj()
    {
        $sql = "SELECT count(distinct(E.ID)) AS CNPJ FROM PA_EMPRESA E INNER JOIN PA_CEP_BRICK C ON C.CEP = E.CEP INNER JOIN SETOR_BRICK S ON S.BRICK = C.BRICK INNER JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1)WHERE E.ATIVO = 1";
        return $this->executarQueryArray($sql);
    }

    public function countContatosByUgn($ugn)
    {
        $sql = "SELECT COUNT(DISTINCT(C.ID)) AS CONTATOS FROM PA_CONTATO C JOIN PA_EMPRESA E ON E.ID = C.FK_EMPRESA JOIN PA_CEP_BRICK B ON B.CEP = E.CEP JOIN SETOR_BRICK S ON S.BRICK = B.BRICK JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1) WHERE E.ATIVO = 1 AND U.CODIGO IN($ugn)";
        return $this->executarQueryArray($sql);
    }

    public function countContatos()
    {
        $sql = "SELECT COUNT(DISTINCT(C.ID)) AS CONTATOS FROM PA_CONTATO C JOIN PA_EMPRESA E ON E.ID = C.FK_EMPRESA JOIN PA_CEP_BRICK B ON B.CEP = E.CEP JOIN SETOR_BRICK S ON S.BRICK = B.BRICK JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1) WHERE E.ATIVO = 1 ";
        return $this->executarQueryArray($sql);
    }

    public function getIdUgnByCep($cep)
    {
        $sql = "SELECT U.CODIGO FROM PA_EMPRESA E INNER JOIN PA_CEP_BRICK C ON C.CEP = E.CEP INNER JOIN SETOR_BRICK S ON S.BRICK = C.BRICK INNER JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1) WHERE E.ATIVO = 1 AND C.CEP = '$cep' GROUP BY U.CODIGO";
        $retorno = $this->executarQueryArray($sql);
        return $retorno[1]['CODIGO'];
    }

    public function listCnpjByIds($ids)
    {
        $sql = "SELECT DISTINCT(E.ID),U.CODIGO,E.CNPJ,E.RAZAO_SOCIAL,E.FANTASIA,E.CIDADE,E.CEP,E.DATA_IN FROM PA_EMPRESA E";
        $sql .= " JOIN PA_CEP_BRICK C ON C.CEP = E.CEP JOIN SETOR_BRICK S ON S.BRICK = C.BRICK";
        $sql .= " JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1) WHERE E.ATIVO = 1 AND E.ID IN($ids)";
        return $this->executarQueryArray($sql);
    }

    public function listCnpjById($idEmpresa)
    {
        $sql = "SELECT ID,RAZAO_SOCIAL,FANTASIA,CIDADE,DATA_IN FROM PA_EMPRESA WHERE ATIVO = 1 AND ID = $idEmpresa";
        return $this->executarQueryArray($sql);
    }

    public function listIdEmpresaByColaborador($idColaborador)
    {
        $sql = "SELECT PR.FK_EMPRESA FROM PA_EMPRESA_RESPONSAVEL PR INNER JOIN PA_EMPRESA PE on PE.ID = PR.FK_EMPRESA WHERE PE.ATIVO = 1 AND PR.FK_RESPONSAVEL = $idColaborador";
        return $this->executarQueryArray($sql);
    }

    public function listIdEmpresaByUgn($ugn)
    {
        $sql = "SELECT U.CODIGO,E.ID,E.CNPJ,E.RAZAO_SOCIAL,E.FANTASIA,E.CIDADE,E.CEP,E.DATA_IN FROM PA_EMPRESA E INNER JOIN PA_CEP_BRICK C ON C.CEP = E.CEP INNER JOIN SETOR_BRICK S ON S.BRICK = C.BRICK INNER JOIN UGN U ON U.CODIGO = SUBSTRING(CAST(S.SETOR AS VARCHAR(8)), 4, 1) WHERE E.ATIVO = 1 AND U.CODIGO IN($ugn) GROUP BY U.CODIGO,E.ID,E.CNPJ,E.RAZAO_SOCIAL,E.FANTASIA,E.CIDADE,E.CEP,E.DATA_IN ORDER BY E.FANTASIA";
        return $this->executarQueryArray($sql);
    }

    public function listUgn()
    {
        $sql = "SELECT ID_UGN AS ID,CODIGO,UGN FROM UGN";
        return $this->executarQueryArray($sql);
    }

    public function listUgnByIdColaborador($idColaborador)
    {
        $sql = "SELECT SETOR FROM vw_colaboradorSetor WHERE ID_COLABORADOR = $idColaborador";
        return $this->executarQueryArray($sql);
    }

    public function getSetorByIdColaborados($idColaborador)
    {
        $sql = "SELECT ID_SETOR FROM vw_colaboradorSetor WHERE ID_COLABORADOR = $idColaborador";
        return $this->executarQueryArray($sql);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($Id)
    {
        $this->id = $Id;
    }

    public function getCnpj()
    {
        return $this->cnpj;
    }

    public function setCnpj($Cnpj)
    {
        $this->cnpj = $Cnpj;
    }

    public function getRazaoSocial()
    {
        return $this->razaoSocial;
    }

    public function setRazaoSocial($RazaoSocial)
    {
        $this->razaoSocial = $RazaoSocial;
    }

    public function getFantasia()
    {
        return $this->fantasia;
    }

    public function setFantasia($Fantasia)
    {
        $this->fantasia = $Fantasia;
    }

    public function getCep()
    {
        return $this->cep;
    }

    public function setCep($Cep)
    {
        $this->cep = $Cep;
    }

    public function getCidade()
    {
        return $this->cidade;
    }

    public function setCidade($Cidade)
    {
        $this->cidade = $Cidade;
    }

    public function getBairro()
    {
        return $this->bairro;
    }

    public function setBairro($Bairro)
    {
        $this->bairro = $Bairro;
    }

    public function getLogradouro()
    {
        return $this->logradouro;
    }

    public function setLogradouro($Logradouro)
    {
        $this->logradouro = $Logradouro;
    }

    public function getNumero()
    {
        return $this->numero;
    }

    public function setNumero($Numero)
    {
        $this->numero = $Numero;
    }

    public function getComplemento()
    {
        return $this->complemento;
    }

    public function setComplemento($Complemento)
    {
        $this->complemento = $Complemento;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function setEstado($Estado)
    {
        $this->estado = $Estado;
    }

    public function getNumeroMedicos()
    {
        return $this->numeroMedicos;
    }

    public function setNumeroMedicos($NumeroMedicos)
    {
        $this->numeroMedicos = $NumeroMedicos;
    }

    public function getNumeroPacientes()
    {
        return $this->numeroPacientes;
    }

    public function setNumeroPacientes($NumeroPacientes)
    {
        $this->numeroPacientes = $NumeroPacientes;
    }

    public function getPacientesDia()
    {
        return $this->pacientesDia;
    }

    public function setPacientesDia($PacientesDia)
    {
        $this->pacientesDia = $PacientesDia;
    }

    public function getFkUser()
    {
        return $this->fkUser;
    }

    public function setFkUser($FkUser)
    {
        $this->fkUser = $FkUser;
    }

    public function getAtivo()
    {
        return $this->ativo;
    }

    public function setAtivo($Ativo)
    {
        $this->ativo = $Ativo;
    }

    public function getDataIn()
    {
        return $this->dataIn;
    }

    public function setDataIn($DataIn)
    {
        $this->dataIn = $DataIn;
    }

    public function listEstados()
    {
        $sql = "select * from ESTADO";
        $result = $this->executarQueryArray($sql);
        return $result;
    }


    public function buscarCrm($crm)
    {
        $crm = trim($crm);
        $uf = $crm[0].$crm[1];
        $crm = $crm[2].$crm[3].$crm[4].$crm[5].$crm[6].$crm[7].$crm[8];
        $sql = "select nome from PA_MEDICO_CONSULTA where uf = '$uf' AND CRM = '$crm'";
        $result = $this->executarQueryArray($sql);
        return $result[1][0];
    }

    public function listColaboradores()
    {
        $sql = "SELECT ID_COLABORADOR, NOME FROM vw_colaboradorSetor WHERE ID_UN_NEGOCIO = 1 AND ID_COLABORADOR IS NOT NULL AND ID_PERFIL IN (2, 3) GROUP BY ID_COLABORADOR, NOME";
        $result = $this->executarQueryArray($sql);
        return $result;
    }

    public function save()
    {
        // TODO: Implement save() method.
    }

    public function saveEmpresa($cnpj, $razaoSocial, $fantasia, $cep, $cidade, $bairro, $logradouro, $numero, $complemento, $estado, $numeroMedicos, $numeroPacientes, $pacientesDia, $fkUser)
    {
        $sql = "INSERT INTO PA_EMPRESA([CNPJ], [RAZAO_SOCIAL], [FANTASIA], [CEP], [CIDADE], [BAIRRO], [LOGRADOURO], [NUMERO], [COMPLEMENTO], [ESTADO], [NUMERO_MEDICOS], [NUMERO_PACIENTES], [PACIENTES_DIA], [FK_USER])";
        $sql .= " VALUES('$cnpj', '$razaoSocial', '$fantasia', '$cep', '$cidade', '$bairro', '$logradouro', '$numero', '$complemento', '$estado', '$numeroMedicos', '$numeroPacientes', '$pacientesDia', '$fkUser')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }

    public function delete()
    {
        $id = $_POST['id'];
        $sql = "UPDATE PA_EMPRESA SET ATIVO = 0 WHERE ID = $id";
        $sql = utf8_decode($sql);
        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function ajustar($idEmpresa, $IdAntigo) {
        $sql = "EXEC proc_pa_ajustarCadastro ".$idEmpresa.", ".$IdAntigo;
        $this->executar($sql);
    }

    public function deleteDefault()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM PA_EMPRESA WHERE ";

        $verif = false;
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ID = '$id' ";
            $verif = true;
        }
        if (strcmp($cnpj, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CNPJ = '$cnpj' ";
            $verif = true;
        }
        if (strcmp($razaoSocial, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "RAZAO_SOCIAL = '$razaoSocial' ";
            $verif = true;
        }
        if (strcmp($fantasia, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "FANTASIA = '$fantasia' ";
            $verif = true;
        }
        if (strcmp($cep, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CEP = '$cep' ";
            $verif = true;
        }
        if (strcmp($cidade, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "CIDADE = '$cidade' ";
            $verif = true;
        }
        if (strcmp($bairro, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "BAIRRO = '$bairro' ";
            $verif = true;
        }
        if (strcmp($logradouro, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "LOGRADOURO = '$logradouro' ";
            $verif = true;
        }
        if (strcmp($numero, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NUMERO = '$numero' ";
            $verif = true;
        }
        if (strcmp($complemento, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "COMPLEMENTO = '$complemento' ";
            $verif = true;
        }
        if (strcmp($estado, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ESTADO = '$estado' ";
            $verif = true;
        }
        if (strcmp($numeroMedicos, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NUMERO_MEDICOS = '$numeroMedicos' ";
            $verif = true;
        }
        if (strcmp($numeroPacientes, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "NUMERO_PACIENTES = '$numeroPacientes' ";
            $verif = true;
        }
        if (strcmp($pacientesDia, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "PACIENTES_DIA = '$pacientesDia' ";
            $verif = true;
        }
        if (strcmp($fkUser, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "FK_USER = '$fkUser' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $sql .= " AND ";
            }
            $sql .= "ATIVO = '$ativo' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if ($multiplos == 1)
            echo 1;
    }

    public function loadObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = "";
        $fantasia = "";
        $cep = "";
        $cidade = "";
        $bairro = "";
        $logradouro = "";
        $numero = "";
        $complemento = "";
        $estado = "";
        $numeroMedicos = "";
        $numeroPacientes = "";
        $pacientesDia = "";
        $fkUser = "";
        $ativo = $_POST['ativo'];
        $dataIn = "";

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CNPJ ";
        $sql .= ",";
        $sql .= "RAZAO_SOCIAL ";
        $sql .= ",";
        $sql .= "FANTASIA ";
        $sql .= ",";
        $sql .= "CEP ";
        $sql .= ",";
        $sql .= "CIDADE ";
        $sql .= ",";
        $sql .= "BAIRRO ";
        $sql .= ",";
        $sql .= "LOGRADOURO ";
        $sql .= ",";
        $sql .= "NUMERO ";
        $sql .= ",";
        $sql .= "COMPLEMENTO ";
        $sql .= ",";
        $sql .= "ESTADO ";
        $sql .= ",";
        $sql .= "NUMERO_MEDICOS ";
        $sql .= ",";
        $sql .= "NUMERO_PACIENTES ";
        $sql .= ",";
        $sql .= "PACIENTES_DIA ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM PA_EMPRESA ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID = $id ";
            $verif = true;
        }
        if (strcmp($cnpj, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CNPJ LIKE '%$cnpj%' ";
            $verif = true;
        }
        if (strcmp($razaoSocial, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "RAZAO_SOCIAL LIKE '%$razaoSocial%' ";
            $verif = true;
        }
        if (strcmp($fantasia, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FANTASIA LIKE '%$fantasia%' ";
            $verif = true;
        }
        if (strcmp($cep, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CEP LIKE '%$cep%' ";
            $verif = true;
        }
        if (strcmp($cidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CIDADE LIKE '%$cidade%' ";
            $verif = true;
        }
        if (strcmp($bairro, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "BAIRRO LIKE '%$bairro%' ";
            $verif = true;
        }
        if (strcmp($logradouro, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOGRADOURO LIKE '%$logradouro%' ";
            $verif = true;
        }
        if (strcmp($numero, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO LIKE '%$numero%' ";
            $verif = true;
        }
        if (strcmp($complemento, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "COMPLEMENTO LIKE '%$complemento%' ";
            $verif = true;
        }
        if (strcmp($estado, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ESTADO LIKE '%$estado%' ";
            $verif = true;
        }
        if (strcmp($numeroMedicos, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO_MEDICOS LIKE '%$numeroMedicos%' ";
            $verif = true;
        }
        if (strcmp($numeroPacientes, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO_PACIENTES LIKE '%$numeroPacientes%' ";
            $verif = true;
        }
        if (strcmp($pacientesDia, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "PACIENTES_DIA LIKE '%$pacientesDia%' ";
            $verif = true;
        }
        if (strcmp($fkUser, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE ".$where;
        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }

    public function montaObj($param)
    {

        $o = new mPaEmpresa();
        $o->setId($param[0]);
        $o->setCnpj($param[1]);
        $o->setRazaoSocial($param[2]);
        $o->setFantasia($param[3]);
        $o->setCep($param[4]);
        $o->setCidade($param[5]);
        $o->setBairro($param[6]);
        $o->setLogradouro($param[7]);
        $o->setNumero($param[8]);
        $o->setComplemento($param[9]);
        $o->setEstado($param[10]);
        $o->setNumeroMedicos($param[11]);
        $o->setNumeroPacientes($param[12]);
        $o->setPacientesDia($param[13]);
        $o->setFkUser($param[14]);
        $o->setAtivo($param[15]);
        $o->setDataIn($param[16]);

        return $o;

    }

    public function updateObj()
    {
        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  PA_EMPRESA";
        $sql .= " SET ";
        $sql .= "CNPJ = '$cnpj' ";
        $sql .= " , ";
        $sql .= "RAZAO_SOCIAL = '$razaoSocial' ";
        $sql .= " , ";
        $sql .= "FANTASIA = '$fantasia' ";
        $sql .= " , ";
        $sql .= "CEP = '$cep' ";
        $sql .= " , ";
        $sql .= "CIDADE = '$cidade' ";
        $sql .= " , ";
        $sql .= "BAIRRO = '$bairro' ";
        $sql .= " , ";
        $sql .= "LOGRADOURO = '$logradouro' ";
        $sql .= " , ";
        $sql .= "NUMERO = '$numero' ";
        $sql .= " , ";
        $sql .= "COMPLEMENTO = '$complemento' ";
        $sql .= " , ";
        $sql .= "ESTADO = '$estado' ";
        $sql .= " , ";
        $sql .= "NUMERO_MEDICOS = '$numeroMedicos' ";
        $sql .= " , ";
        $sql .= "NUMERO_PACIENTES = '$numeroPacientes' ";
        $sql .= " , ";
        $sql .= "PACIENTES_DIA = '$pacientesDia' ";
        $sql .= " , ";
        $sql .= "FK_USER = '$fkUser' ";
        $sql .= " , ";
        $sql .= "ATIVO = '$ativo' ";
        $sql .= " , ";
        $sql .= "DATA_IN = '$dataIn' ";
        $sql .= " WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = 1;
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CNPJ ";
        $sql .= ",";
        $sql .= "RAZAO_SOCIAL ";
        $sql .= ",";
        $sql .= "FANTASIA ";
        $sql .= ",";
        $sql .= "CEP ";
        $sql .= ",";
        $sql .= "CIDADE ";
        $sql .= ",";
        $sql .= "BAIRRO ";
        $sql .= ",";
        $sql .= "LOGRADOURO ";
        $sql .= ",";
        $sql .= "NUMERO ";
        $sql .= ",";
        $sql .= "COMPLEMENTO ";
        $sql .= ",";
        $sql .= "ESTADO ";
        $sql .= ",";
        $sql .= "NUMERO_MEDICOS ";
        $sql .= ",";
        $sql .= "NUMERO_PACIENTES ";
        $sql .= ",";
        $sql .= "PACIENTES_DIA ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_EMPRESA ";

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID = $id ";
            $verif = true;
        }
        if (strcmp($cnpj, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CNPJ LIKE '%$cnpj%' ";
            $verif = true;
        }
        if (strcmp($razaoSocial, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "RAZAO_SOCIAL LIKE '%$razaoSocial%' ";
            $verif = true;
        }
        if (strcmp($fantasia, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FANTASIA LIKE '%$fantasia%' ";
            $verif = true;
        }
        if (strcmp($cep, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CEP LIKE '%$cep%' ";
            $verif = true;
        }
        if (strcmp($cidade, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CIDADE LIKE '%$cidade%' ";
            $verif = true;
        }
        if (strcmp($bairro, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "BAIRRO LIKE '%$bairro%' ";
            $verif = true;
        }
        if (strcmp($logradouro, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "LOGRADOURO LIKE '%$logradouro%' ";
            $verif = true;
        }
        if (strcmp($numero, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO LIKE '%$numero%' ";
            $verif = true;
        }
        if (strcmp($complemento, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "COMPLEMENTO LIKE '%$complemento%' ";
            $verif = true;
        }
        if (strcmp($estado, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ESTADO LIKE '%$estado%' ";
            $verif = true;
        }
        if (strcmp($numeroMedicos, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO_MEDICOS LIKE '%$numeroMedicos%' ";
            $verif = true;
        }
        if (strcmp($numeroPacientes, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO_PACIENTES LIKE '%$numeroPacientes%' ";
            $verif = true;
        }
        if (strcmp($pacientesDia, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "PACIENTES_DIA LIKE '%$pacientesDia%' ";
            $verif = true;
        }
        if (strcmp($fkUser, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO = $ativo ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE ".$where;

        $sql = $this->pages($sql);
        $_POST['pesquisa'] = 0;
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();
        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }

    public function pages($sql)
    {
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual == '')
            $atual = 1;
        if ($max == '')
            $max = 10;
        if ($operador == '+') {
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] = $atual = $atual + 10;
        } else if ($operador == '-') {
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        if ($_POST['pesquisa'] != 1)
            $paginacao .= " AND row <= ".$max;
        $paginacao .= " AND ATIVO = 1";
        return $paginacao;
    }

    public function countRows()
    {
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = 1;
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from PA_EMPRESA";
        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ID LIKE '%$id%' ";
            $verif = true;
        }
        if (strcmp($cnpj, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CNPJ LIKE '%$cnpj%' ";
            $verif = true;
        }
        if (strcmp($razaoSocial, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "RAZAO_SOCIAL LIKE '%$razaoSocial%' ";
            $verif = true;
        }
        if (strcmp($fantasia, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FANTASIA LIKE '%$fantasia%' ";
            $verif = true;
        }
        if (strcmp($cep, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CEP LIKE '%$cep%' ";
            $verif = true;
        }
        if (strcmp($cidade, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "CIDADE LIKE '%$cidade%' ";
            $verif = true;
        }
        if (strcmp($bairro, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "BAIRRO LIKE '%$bairro%' ";
            $verif = true;
        }
        if (strcmp($logradouro, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "LOGRADOURO LIKE '%$logradouro%' ";
            $verif = true;
        }
        if (strcmp($numero, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO LIKE '%$numero%' ";
            $verif = true;
        }
        if (strcmp($complemento, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "COMPLEMENTO LIKE '%$complemento%' ";
            $verif = true;
        }
        if (strcmp($estado, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ESTADO LIKE '%$estado%' ";
            $verif = true;
        }
        if (strcmp($numeroMedicos, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO_MEDICOS LIKE '%$numeroMedicos%' ";
            $verif = true;
        }
        if (strcmp($numeroPacientes, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "NUMERO_PACIENTES LIKE '%$numeroPacientes%' ";
            $verif = true;
        }
        if (strcmp($pacientesDia, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "PACIENTES_DIA LIKE '%$pacientesDia%' ";
            $verif = true;
        }
        if (strcmp($fkUser, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO = $ativo ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= " WHERE ".$where;
        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function listObjFiltrado()
    {
        $_POST = appSanitize::filter($_POST);
        $idList = $_POST['idList'];
        $id = $_POST['id'];
        $cnpj = $_POST['cnpj'];
        $razaoSocial = $_POST['razaosocial'];
        $fantasia = $_POST['fantasia'];
        $cep = $_POST['cep'];
        $cidade = $_POST['cidade'];
        $bairro = $_POST['bairro'];
        $logradouro = $_POST['logradouro'];
        $numero = $_POST['numero'];
        $complemento = $_POST['complemento'];
        $estado = $_POST['estado'];
        $numeroMedicos = $_POST['numeromedicos'];
        $numeroPacientes = $_POST['numeropacientes'];
        $pacientesDia = $_POST['pacientesdia'];
        $fkUser = $_POST['fkuser'];
        $ativo = 1;
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "CNPJ ";
        $sql .= ",";
        $sql .= "RAZAO_SOCIAL ";
        $sql .= ",";
        $sql .= "FANTASIA ";
        $sql .= ",";
        $sql .= "CEP ";
        $sql .= ",";
        $sql .= "CIDADE ";
        $sql .= ",";
        $sql .= "BAIRRO ";
        $sql .= ",";
        $sql .= "LOGRADOURO ";
        $sql .= ",";
        $sql .= "NUMERO ";
        $sql .= ",";
        $sql .= "COMPLEMENTO ";
        $sql .= ",";
        $sql .= "ESTADO ";
        $sql .= ",";
        $sql .= "NUMERO_MEDICOS ";
        $sql .= ",";
        $sql .= "NUMERO_PACIENTES ";
        $sql .= ",";
        $sql .= "PACIENTES_DIA ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_EMPRESA ";

        if (strcmp($idList, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID IN($idList) ";
            $verif = true;
        }

        if (strcmp($id, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ID = $id ";
            $verif = true;
        }
        if (strcmp($cnpj, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= " (CNPJ LIKE '%$cnpj%' ";
            $verif = true;
        }
        if (strcmp($razaoSocial, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "RAZAO_SOCIAL LIKE '%$razaoSocial%' ";
            $verif = true;
        }
        if (strcmp($fantasia, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FANTASIA LIKE '%$fantasia%' ";
            $verif = true;
        }
        if (strcmp($cep, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CEP LIKE '%$cep%' ";
            $verif = true;
        }
        if (strcmp($cidade, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "CIDADE LIKE '%$cidade%' )";
            $verif = true;
        }
        if (strcmp($bairro, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "BAIRRO LIKE '%$bairro%' ";
            $verif = true;
        }
        if (strcmp($logradouro, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "LOGRADOURO LIKE '%$logradouro%' ";
            $verif = true;
        }
        if (strcmp($numero, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO LIKE '%$numero%' ";
            $verif = true;
        }
        if (strcmp($complemento, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "COMPLEMENTO LIKE '%$complemento%' ";
            $verif = true;
        }
        if (strcmp($estado, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "ESTADO LIKE '%$estado%' ";
            $verif = true;
        }
        if (strcmp($numeroMedicos, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO_MEDICOS LIKE '%$numeroMedicos%' ";
            $verif = true;
        }
        if (strcmp($numeroPacientes, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "NUMERO_PACIENTES LIKE '%$numeroPacientes%' ";
            $verif = true;
        }
        if (strcmp($pacientesDia, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "PACIENTES_DIA LIKE '%$pacientesDia%' ";
            $verif = true;
        }
        if (strcmp($fkUser, "") != 0) {
            if ($verif) {
                $where .= " OR ";
            }
            $where .= "FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if (strcmp($ativo, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "ATIVO = $ativo ";
            $verif = true;
        }
        if (strcmp($dataIn, "") != 0) {
            if ($verif) {
                $where .= " AND ";
            }
            $where .= "DATA_IN IN('$dataIn') ";
            $verif = true;
        }
        if ($verif)
            $sql .= "WHERE ".$where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        //$_POST['numRows'] = $this->countRows();
        if (is_array($arrayResult))
            foreach ($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
}