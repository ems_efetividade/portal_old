<?php
require_once('lib/appConexao.php');

class mPaEmpresaConsultas extends appConexao implements gridInterface {

    private $id;
    private $valor;
    private $percentual;
    private $fkUser;
    private $ativo;
    private $dataIn;
    private $fkEmpresa;

    public function __construct(){
        $this->id;
        $this->valor;
        $this->percentual;
        $this->fkUser;
        $this->ativo;
        $this->dataIn;
        $this->fkEmpresa;
    }

    public function getId(){
        return $this->id;
    }

    public function getValor(){
        return $this->valor;
    }

    public function getPercentual(){
        return $this->percentual;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function getFkEmpresa(){
        return $this->fkEmpresa;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setValor($Valor){
        $this->valor = $Valor;
    }

    public function setPercentual($Percentual){
        $this->percentual = $Percentual;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function setFkEmpresa($FkEmpresa){
        $this->fkEmpresa = $FkEmpresa;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $sql = "select count(id) from PA_EMPRESA_CONSULTAS";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($valor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="VALOR LIKE '%$valor%' ";
            $verif = true;
        }
        if(strcmp($percentual, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PERCENTUAL LIKE '%$percentual%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $sql = "INSERT INTO PA_EMPRESA_CONSULTAS ([VALOR],[PERCENTUAL],[FK_USER],[FK_EMPRESA])";
        $sql .=" VALUES ('$valor','$percentual','$fkUser','$fkEmpresa')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $sql = "DELETE FROM PA_EMPRESA_CONSULTAS WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($valor, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="VALOR = '$valor' ";
            $verif = true;
        }
        if(strcmp($percentual, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="PERCENTUAL = '$percentual' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_EMPRESA = '$fkEmpresa' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "VALOR ";
        $sql .= ",";
        $sql .= "PERCENTUAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= " FROM PA_EMPRESA_CONSULTAS ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($valor, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="VALOR LIKE '%$valor%' ";
            $verif = true;
        }
        if(strcmp($percentual, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PERCENTUAL LIKE '%$percentual%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA = $fkEmpresa ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];
        $sql = "UPDATE  PA_EMPRESA_CONSULTAS";
        $sql .=" SET ";
        $sql .="VALOR = '$valor' ";
        $sql .=" , ";
        $sql .="PERCENTUAL = '$percentual' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" , ";
        $sql .="FK_EMPRESA = '$fkEmpresa' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $valor = $_POST['valor'];
        $percentual = $_POST['percentual'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $fkEmpresa = $_POST['fkempresa'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "VALOR ";
        $sql .= ",";
        $sql .= "PERCENTUAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_EMPRESA_CONSULTAS ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($valor, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="VALOR LIKE '%$valor%' ";
            $verif = true;
        }
        if(strcmp($percentual, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="PERCENTUAL LIKE '%$percentual%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaEmpresaConsultas();
        $o->setId($param[0]);
        $o->setValor($param[1]);
        $o->setPercentual($param[2]);
        $o->setFkUser($param[3]);
        $o->setAtivo($param[4]);
        $o->setDataIn($param[5]);
        $o->setFkEmpresa($param[6]);

        return $o;

    }
}