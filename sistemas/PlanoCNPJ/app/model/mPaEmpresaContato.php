<?php
require_once('lib/appConexao.php');

class mPaEmpresaContato extends appConexao implements gridInterface {

    private $id;
    private $fkContato;
    private $fkEmpresa;
    private $fkUsu;
    private $ativo;
    private $dataIn;

    public function __construct(){
        $this->id;
        $this->fkContato;
        $this->fkEmpresa;
        $this->fkUsu;
        $this->ativo;
        $this->dataIn;
    }

    public function getId(){
        return $this->id;
    }

    public function getFkContato(){
        return $this->fkContato;
    }

    public function getFkEmpresa(){
        return $this->fkEmpresa;
    }

    public function getFkUsu(){
        return $this->fkUsu;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setFkContato($FkContato){
        $this->fkContato = $FkContato;
    }

    public function setFkEmpresa($FkEmpresa){
        $this->fkEmpresa = $FkEmpresa;
    }

    public function setFkUsu($FkUsu){
        $this->fkUsu = $FkUsu;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "select count(id) from PA_EMPRESA_CONTATO";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($fkUsu, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USU LIKE '%$fkUsu%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "INSERT INTO PA_EMPRESA_CONTATO ([FK_CONTATO],[FK_EMPRESA],[FK_USU],[ATIVO],[DATA_IN])";
        $sql .=" VALUES ('$fkContato','$fkEmpresa','$fkUsu','$ativo','$dataIn')";
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $sql = "DELETE FROM PA_EMPRESA_CONTATO WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_CONTATO = '$fkContato' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_EMPRESA = '$fkEmpresa' ";
            $verif = true;
        }
        if(strcmp($fkUsu, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USU = '$fkUsu' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "FK_USU ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= " FROM PA_EMPRESA_CONTATO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($fkUsu, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USU LIKE '%$fkUsu%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $sql = "UPDATE  PA_EMPRESA_CONTATO";
        $sql .=" SET ";
        $sql .="FK_CONTATO = '$fkContato' ";
        $sql .=" , ";
        $sql .="FK_EMPRESA = '$fkEmpresa' ";
        $sql .=" , ";
        $sql .="FK_USU = '$fkUsu' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkContato = $_POST['fkcontato'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkUsu = $_POST['fkusu'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "FK_USU ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_EMPRESA_CONTATO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($fkUsu, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USU LIKE '%$fkUsu%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaEmpresaContato();
        $o->setId($param[0]);
        $o->setFkContato($param[1]);
        $o->setFkEmpresa($param[2]);
        $o->setFkUsu($param[3]);
        $o->setAtivo($param[4]);
        $o->setDataIn($param[5]);

        return $o;

    }
}