<?php
require_once('lib/appConexao.php');

class mPaEmpresaTelefone extends appConexao implements gridInterface {

    private $id;
    private $fkEmpresa;
    private $fkTelefone;
    private $ativo;
    private $fkUser;
    private $dataIn;
    private $principal;

    public function __construct(){
        $this->id;
        $this->fkEmpresa;
        $this->fkTelefone;
        $this->ativo;
        $this->fkUser;
        $this->dataIn;
        $this->principal;
    }

    public function getId(){
        return $this->id;
    }

    public function getFkEmpresa(){
        return $this->fkEmpresa;
    }

    public function getFkTelefone(){
        return $this->fkTelefone;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setFkEmpresa($FkEmpresa){
        $this->fkEmpresa = $FkEmpresa;
    }

    public function setFkTelefone($FkTelefone){
        $this->fkTelefone = $FkTelefone;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function setPrincipal($Principal){
        $this->principal = $Principal;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $sql = "select count(id) from PA_EMPRESA_TELEFONE";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_TELEFONE LIKE '%$fkTelefone%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $sql = "INSERT INTO PA_EMPRESA_TELEFONE ([FK_EMPRESA],[FK_TELEFONE],[FK_USER],[PRINCIPAL])";
        $sql .=" VALUES ('$fkEmpresa','$fkTelefone','$fkUser','$principal')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $sql = "DELETE FROM PA_EMPRESA_TELEFONE WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_EMPRESA = '$fkEmpresa' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_TELEFONE = '$fkTelefone' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="PRINCIPAL = '$principal' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "FK_TELEFONE ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= " FROM PA_EMPRESA_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_EMPRESA LIKE '%$fkEmpresa%' ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_TELEFONE LIKE '%$fkTelefone%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];
        $sql = "UPDATE  PA_EMPRESA_TELEFONE";
        $sql .=" SET ";
        $sql .="FK_EMPRESA = '$fkEmpresa' ";
        $sql .=" , ";
        $sql .="FK_TELEFONE = '$fkTelefone' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" , ";
        $sql .="PRINCIPAL = '$principal' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkEmpresa = $_POST['fkempresa'];
        $fkTelefone = $_POST['fktelefone'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $principal = $_POST['principal'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_EMPRESA ";
        $sql .= ",";
        $sql .= "FK_TELEFONE ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_EMPRESA_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($fkEmpresa, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_EMPRESA = $fkEmpresa ";
            $verif = true;
        }
        if(strcmp($fkTelefone, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_TELEFONE = $fkTelefone ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        //$sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaEmpresaTelefone();
        $o->setId($param[0]);
        $o->setFkEmpresa($param[1]);
        $o->setFkTelefone($param[2]);
        $o->setAtivo($param[3]);
        $o->setFkUser($param[4]);
        $o->setDataIn($param[5]);
        $o->setPrincipal($param[6]);

        return $o;

    }
}