<?php
require_once('lib/appConexao.php');

class mPaMedico extends appConexao implements gridInterface {

    private $id;
    private $fkCrm;
    private $fkContato;
    private $ativo;
    private $fkUser;
    private $dataIn;
    private $residente;

    public function __construct(){
        $this->id;
        $this->fkCrm;
        $this->fkContato;
        $this->ativo;
        $this->fkUser;
        $this->dataIn;
        $this->residente;
    }

    public function getId(){
        return $this->id;
    }

    public function getFkCrm(){
        return $this->fkCrm;
    }

    public function getFkContato(){
        return $this->fkContato;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function getResidente(){
        return $this->residente;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setFkCrm($FkCrm){
        $this->fkCrm = $FkCrm;
    }

    public function setFkContato($FkContato){
        $this->fkContato = $FkContato;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function setResidente($Residente){
        $this->residente = $Residente;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $sql = "select count(id) from PA_MEDICO";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkCrm, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CRM LIKE '%$fkCrm%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($residente, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESIDENTE LIKE '%$residente%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $sql = "INSERT INTO PA_MEDICO ([FK_CRM],[FK_CONTATO],[FK_USER],[RESIDENTE])";
        $sql .=" VALUES ('$fkCrm','$fkContato','$fkUser','$residente')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $sql = "DELETE FROM PA_MEDICO WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($fkCrm, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_CRM = '$fkCrm' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_CONTATO = '$fkContato' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($residente, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="RESIDENTE = '$residente' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_CRM ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "RESIDENTE ";
        $sql .= " FROM PA_MEDICO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($fkCrm, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CRM LIKE '%$fkCrm%' ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_CONTATO LIKE '%$fkContato%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($residente, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="RESIDENTE LIKE '%$residente%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];
        $sql = "UPDATE  PA_MEDICO";
        $sql .=" SET ";
        $sql .="FK_CRM = '$fkCrm' ";
        $sql .=" , ";
        $sql .="FK_CONTATO = '$fkContato' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" , ";
        $sql .="RESIDENTE = '$residente' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $fkCrm = $_POST['fkcrm'];
        $fkContato = $_POST['fkcontato'];
        $ativo = $_POST['ativo'];
        $fkUser = $_POST['fkuser'];
        $dataIn = $_POST['datain'];
        $residente = $_POST['residente'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "FK_CRM ";
        $sql .= ",";
        $sql .= "FK_CONTATO ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "RESIDENTE ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_MEDICO ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($fkCrm, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_CRM = $fkCrm ";
            $verif = true;
        }
        if(strcmp($fkContato, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_CONTATO = $fkContato ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($residente, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="RESIDENTE LIKE '%$residente%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaMedico();
        $o->setId($param[0]);
        $o->setFkCrm($param[1]);
        $o->setFkContato($param[2]);
        $o->setAtivo($param[3]);
        $o->setFkUser($param[4]);
        $o->setDataIn($param[5]);
        $o->setResidente($param[6]);

        return $o;

    }
}