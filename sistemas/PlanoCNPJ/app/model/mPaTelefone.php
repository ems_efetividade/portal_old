<?php
require_once('lib/appConexao.php');

class mPaTelefone extends appConexao implements gridInterface {

    private $id;
    private $ddd;
    private $tel;
    private $principal;
    private $fkUser;
    private $ativo;
    private $dataIn;
    private $whatsapp;

    public function __construct(){
        $this->id;
        $this->ddd;
        $this->tel;
        $this->principal;
        $this->fkUser;
        $this->ativo;
        $this->dataIn;
        $this->whatsapp;
    }

    public function getId(){
        return $this->id;
    }

    public function getDdd(){
        return $this->ddd;
    }

    public function getTel(){
        return $this->tel;
    }

    public function getPrincipal(){
        return $this->principal;
    }

    public function getFkUser(){
        return $this->fkUser;
    }

    public function getAtivo(){
        return $this->ativo;
    }

    public function getDataIn(){
        return $this->dataIn;
    }

    public function getWhatsapp(){
        return $this->whatsapp;
    }

    public function setId($Id){
        $this->id = $Id;
    }

    public function setDdd($Ddd){
        $this->ddd = $Ddd;
    }

    public function setTel($Tel){
        $this->tel = $Tel;
    }

    public function setPrincipal($Principal){
        $this->principal = $Principal;
    }

    public function setFkUser($FkUser){
        $this->fkUser = $FkUser;
    }

    public function setAtivo($Ativo){
        $this->ativo = $Ativo;
    }

    public function setDataIn($DataIn){
        $this->dataIn = $DataIn;
    }

    public function setWhatsapp($Whatsapp){
        $this->whatsapp = $Whatsapp;
    }

    public function countRows(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $sql = "select count(id) from PA_TELEFONE";
        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($tel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TEL LIKE '%$tel%' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($whatsapp, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="WHATSAPP LIKE '%$whatsapp%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $arrayResult = $this->executarQueryArray($sql);
        return $arrayResult[1][0];
    }

    public function save(){
        $_POST = appSanitize::filter($_POST);

        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];
        $sql = utf8_decode($sql);
        $sql = "INSERT INTO PA_TELEFONE ([DDD],[TEL],[PRINCIPAL],[FK_USER],[WHATSAPP])";
        $sql .=" VALUES ('$ddd','$tel','$principal','$fkUser','$whatsapp')";
        $sql = utf8_decode($sql);
        $this->executar($sql);
    }
    public function pages($sql){
        $_POST = appSanitize::filter($_POST);
        $atual = $_POST['atual'];
        $max = $_POST['max'];
        $operador = $_POST['operador'];
        if ($atual=='')
            $atual = 1;
        if($max=='')
            $max=10;
        if($operador=='+'){
            $_POST['max'] = $max = $max + 10;
            $_POST['atual'] =$atual = $atual+10;
        }
        else if($operador=='-'){
            $_POST['max'] = $max = $max - 10;
            $_POST['atual'] = $atual = $atual - 10;
        }
        $paginacao = "WITH resultado AS (".$sql;
        $paginacao .= ") SELECT * FROM resultado WHERE row >= ".$atual;
        $paginacao .= " AND row <= " .$max." ";
        return $paginacao;
    }

    public function delete(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $sql = "DELETE FROM PA_TELEFONE WHERE ";

        $verif = false;
        if(strcmp($id, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ID = '$id' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="DDD = '$ddd' ";
            $verif = true;
        }
        if(strcmp($tel, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="TEL = '$tel' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="PRINCIPAL = '$principal' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="FK_USER = '$fkUser' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="ATIVO = '$ativo' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($whatsapp, "") != 0){
            if($verif){
                $sql .=" AND ";
            }
            $sql .="WHATSAPP = '$whatsapp' ";
            $verif = true;
        }

        $this->executar($sql);
        $multiplos = $_POST['multiplos'];
        if($multiplos==1)
            echo 1;
    }
    public function loadObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "DDD ";
        $sql .= ",";
        $sql .= "TEL ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "WHATSAPP ";
        $sql .= " FROM PA_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ID = $id ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($tel, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="TEL LIKE '%$tel%' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($whatsapp, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="WHATSAPP LIKE '%$whatsapp%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE " .$where;

        $retorno = $this->executarQueryArray($sql);
        return $this->montaObj($retorno[1]);
    }
    public function updateObj(){
        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];
        $sql = "UPDATE  PA_TELEFONE";
        $sql .=" SET ";
        $sql .="DDD = '$ddd' ";
        $sql .=" , ";
        $sql .="TEL = '$tel' ";
        $sql .=" , ";
        $sql .="PRINCIPAL = '$principal' ";
        $sql .=" , ";
        $sql .="FK_USER = '$fkUser' ";
        $sql .=" , ";
        $sql .="ATIVO = '$ativo' ";
        $sql .=" , ";
        $sql .="DATA_IN = '$dataIn' ";
        $sql .=" , ";
        $sql .="WHATSAPP = '$whatsapp' ";
        $sql .=" WHERE id = '$id'";
        $this->executar($sql);

    }

    public function listObj(){
        $_POST = appSanitize::filter($_POST);

        $id = $_POST['id'];
        $ddd = $_POST['ddd'];
        $tel = $_POST['tel'];
        $principal = $_POST['principal'];
        $fkUser = $_POST['fkuser'];
        $ativo = $_POST['ativo'];
        $dataIn = $_POST['datain'];
        $whatsapp = $_POST['whatsapp'];

        $verif = false;

        $sql = "SELECT ";
        $sql .= "ID ";
        $sql .= ",";
        $sql .= "DDD ";
        $sql .= ",";
        $sql .= "TEL ";
        $sql .= ",";
        $sql .= "PRINCIPAL ";
        $sql .= ",";
        $sql .= "FK_USER ";
        $sql .= ",";
        $sql .= "ATIVO ";
        $sql .= ",";
        $sql .= "DATA_IN ";
        $sql .= ",";
        $sql .= "WHATSAPP ";
        $sql .= ", ROW_NUMBER() OVER(ORDER BY ID ) as row";
        $sql .= " FROM PA_TELEFONE ";

        if(strcmp($id, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ID LIKE '%$id%' ";
            $verif = true;
        }
        if(strcmp($ddd, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="DDD LIKE '%$ddd%' ";
            $verif = true;
        }
        if(strcmp($tel, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="TEL LIKE '%$tel%' ";
            $verif = true;
        }
        if(strcmp($principal, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="PRINCIPAL LIKE '%$principal%' ";
            $verif = true;
        }
        if(strcmp($fkUser, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="FK_USER LIKE '%$fkUser%' ";
            $verif = true;
        }
        if(strcmp($ativo, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="ATIVO LIKE '%$ativo%' ";
            $verif = true;
        }
        if(strcmp($dataIn, "") != 0){
            if($verif){
                $where .=" AND ";
            }
            $where .="DATA_IN IN ('$dataIn') ";
            $verif = true;
        }
        if(strcmp($whatsapp, "") != 0){
            if($verif){
                $where .=" OR ";
            }
            $where .="WHATSAPP LIKE '%$whatsapp%' ";
            $verif = true;
        }
        if($verif)
            $sql .= "WHERE ".$where;
        $sql = $this->pages($sql);
        $arrayResult = $this->executarQueryArray($sql);
        $_POST['numRows'] = $this->countRows();

        if(is_array($arrayResult))
            foreach($arrayResult as $retorno)
                $arrayObj[] = $this->montaObj($retorno);
        return $arrayObj;
    }
    public function montaObj($param){

        $o = new mPaTelefone();
        $o->setId($param[0]);
        $o->setDdd($param[1]);
        $o->setTel($param[2]);
        $o->setPrincipal($param[3]);
        $o->setFkUser($param[4]);
        $o->setAtivo($param[5]);
        $o->setDataIn($param[6]);
        $o->setWhatsapp($param[7]);

        return $o;

    }
}