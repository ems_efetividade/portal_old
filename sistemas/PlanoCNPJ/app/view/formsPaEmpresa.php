<?php

class formsPaEmpresa
{
    public function formInicio()
    {
        $control = new cPaEmpresa();
        $col = $control->listColaboradores();
        $idColaborador = $_SESSION['id_colaborador'];
        $perfil = $_SESSION['id_perfil'];
        $arrayUgn = $control->arrayUgn();
        $ugn = $control->filtrarUgn($arrayUgn);
        switch ($perfil) {
            case '4':
                $cnpjs = $control->listIdEmpresaByUgn($ugn);
                break;
            case '7':
                $cnpjs = $control->listIdEmpresaByUgn($ugn);
                break;
            case '8':
                $cnpjs = $control->listIdEmpresaByUgn($ugn);
                break;
            case '3':
                $listIdEmpresa = $control->listIdEmpresaByColaborador($idColaborador);
                $ugnColaborador = $control->listUgnByIdColaborador($idColaborador);
                $ugn = $control->filtrarUgn($ugnColaborador);
                if (isset($_POST['ugn'])) {
                    $cnpjs = $control->listIdEmpresaByUgn($ugn);
                } else {
                    $cnpjs = $control->listCnpjByIds($listIdEmpresa);
                    $ugn = '';
                }
                break;
            case '2':
                $listIdEmpresa = $control->listIdEmpresaByColaborador($idColaborador);
                $ugnColaborador = $control->listUgnByIdColaborador($idColaborador);
                $ugn = $control->filtrarUgn($ugnColaborador);
                if (isset($_POST['ugn'])) {
                    $cnpjs = $control->listIdEmpresaByUgn($ugn);
                } else {
                    $cnpjs = $control->listCnpjByIds($listIdEmpresa);
                    $ugn = '';
                }
                break;
        }
        ?>
        <div class="modal fade in" id="resumoAtividademd" role="dialog"
             style="display: none;background: rgba(000,000,000,0.3);z-index:100001 ">
            <div class="modal-dialog" id="resumoAtividade" style="display: none">
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="text-center text-primary">Alterar Responsável</h3>
                    </div>
                    <div class="modal-body" id="resumoAtividademsg">
                        <label>Responsável Atual</label>
                        <span id="NomeResponsavel"></span>
                        <input type="hidden" id="idEmpresaR"/>
                        <label for="responsavel" class="inp">
                            <input type="hidden"
                                <?php
                                if (is_array($dadosEditar[1][1])) {
                                    ?>
                                    value="<?php echo $dadosEditar[1][1]['FK_RESPONSAVEL'] ?> "
                                    <?php
                                }
                                ?>
                                   id="responsavelId"/>
                            <input type="text"
                                   onkeyup="buscaNomeNovo()"
                                   name="responsavel"
                                   id="responsavel"
                                   autocomplete="off"
                                <?php

                                foreach ($col as $colaborador) {
                                    if ($dadosEditar[1][1]['FK_RESPONSAVEL'] == $colaborador['ID_COLABORADOR']) {
                                        ?>
                                        value="<?php echo $control->tirarAcentos($colaborador['NOME']) ?> "
                                        <?php
                                    }
                                }

                                ?>
                                   placeholder="&nbsp;">
                            <span class="label" for="responsavel"
                                  style="font-size: 16px;">Novo Responsável</span>
                            <span class="border"></span>
                        </label>
                        <div id="comboNomes"
                             style="display:none;
                                    min-height: 0;
                                    max-height: 200px;
                                    overflow: hidden;
                                    z-index: 1000;
                                    box-shadow: 2px 5px  #d6d6d6;
                                    border:1px solid #e6e6e6;
                                    position:absolute;
                                    background-color: white;
                                    padding:10px;
                                    width:90%;">
                            <table id="listanovo">
                                <?php
                                foreach ($col as $colaborador) {
                                    echo '<tr style="cursor:pointer;display: none;" onclick="SelecionarNome(\'' . $control->tirarAcentos($colaborador['NOME']) . '\',\'' . $control->tirarAcentos($colaborador['ID_COLABORADOR']) . '\')" ><td>' . $control->tirarAcentos($colaborador['NOME']) . '</td></tr>';
                                }
                                ?>
                            </table>
                        </div>
                        <br>
                        <br>
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="salvarResposavel()" class="btn btn-success" data-dismiss="modal">
                            Confirmar
                        </button>
                        <button type="button" onclick="CancelaResposavel()" class="btn btn-danger" data-dismiss="modal">
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                            <img src="/../../public/img/l.png" width="100%"/>
                        </div>
                        <div class="col-xs-12 text-right">
                            <ul class="nav nav-pills">
                                <li role="presentation" class="active"><a href="#"
                                                                          onclick="location.reload()">NEGÓCIOS</a>
                                </li>
                                <li role="presentation" class="disabled"><a href="#">CONTATOS</a></li>
                                <li role="presentation" class="disabled"><a href="#">ATIVIDADES</a></li>
                                <li role="presentation" class="disabled"><a href="#">TÁTICO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <div class="row" style="color: #73879C; display:none;">
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>CNPJ</b></small></span>
                                <div>
                                    <small class="text-muted">
                                        <?php
                                        $arrayU = explode(',', $ugn);
                                        foreach ($arrayU as $value) {
                                            echo $arrayUgn[$value];
                                        }
                                        ?>
                                    </small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo $control->countCnpjByUgn($ugn)[1][0] ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>CNPJ</b></small></span>
                                <div>
                                    <small class="text-muted">BRASIL</small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo $control->countCnpj()[1][0] ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>CONTATOS</b></small></span>
                                <div>
                                    <small class="text-muted">
                                        <?php
                                        $arrayU = explode(',', $ugn);
                                        foreach ($arrayU as $value) {
                                            echo $arrayUgn[$value];
                                        }
                                        ?>
                                    </small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo $control->countContatosByUgn($ugn)[1][0] ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>CONTATOS</b></small></span>
                                <div>
                                    <small class="text-muted">BRASIL</small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo $control->countContatos()[1][0] ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>ATIVIDADES</b></small></span>
                                <div>
                                    <small class="text-muted"><?php echo $arrayUgn[$ugn] ?></small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo 0 ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-md-2">
                        <div class="panel panel-default">
                            <div class=" panel-body text-center">
                                <span><small><b>ATIVIDADES</b></small></span>
                                <div>
                                    <small class="text-muted">BRASIL</small>
                                </div>
                                <div class="box">
                                    <b>
                                        <div><?php echo 0 ?></div>
                                    </b>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="text-right col-xs-12">
                <form method="post">
                    <ul class="ks-cboxtags">
                        <?php
                        $arrayU = explode(',', $ugn);
                        foreach ($arrayUgn as $key => $ugn) {
                            if (is_array($ugnColaborador)) {
                                if (!in_array($key, $ugnColaborador))
                                    continue;
                            }
                            if ($key == 0)
                                continue;
                            ?>
                            <li>
                                <input type="checkbox"
                                    <?php
                                    foreach ($arrayU as $value) {
                                        if ($value == $key)
                                            echo 'checked';
                                    }
                                    ?>
                                       id="ugn<?php echo $key ?>"
                                       class="ugn"
                                       name="ugn[]"
                                       value="<?php echo $key ?>"/>
                                <label for="ugn<?php echo $key ?>"
                                       style="border-radius: 5px; padding-top: 3px; padding-bottom: 6px; width: ;">
                                    <?php echo $ugn ?>
                                </label>
                            </li>
                            <?php
                        }
                        ?>
                        <button class="btn btn-primary"
                                style="border-radius: 5px; padding: 9px">
                            <span class="glyphicon glyphicon-refresh"></span>
                        </button>
                    </ul>
                </form>
            </div>
            <div class="col-xs-12">
                <?php
                if (!is_array($cnpjs)) { ?>
                    <div class="alert alert-danger text-center" role="alert">Nenhum Resgistro encontrado.</div>
                    <?php
                } else {
                    ?>
                    <div class="tableFixHead">
                        <table class="table table-hover" id="lista">
                            <thead>
                            <tr style="background-color: #f5faf6">
                                <th style="padding: 0px 8px;">
                                    <label for="empresa" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridEmp()"
                                               name="empresa"
                                               id="empresa"
                                               placeholder="&nbsp;">
                                        <span class="label" for="empresa" style="color: #545b62">
                                        <span class="glyphicon glyphicon-search"></span>&nbsp;Empresa</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th style="padding: 0px 8px;">
                                    <label for="txtCnpj" class="inp">
                                        <input type="text"
                                               class="maskCnpj"
                                               name="txtCnpj"
                                               id="txtCnpj"
                                               onkeyup="buscaGridCnpj()"
                                               placeholder="&nbsp;">
                                        <span class="label" for="txtCnpj" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;CNPJ</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-left" style="padding: 0px 8px;">
                                    <label for="cidade" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridCid()"
                                               name="cidade"
                                               id="cidade"
                                               placeholder="&nbsp;">
                                        <span class="label" for="cidade" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;Cidade</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-center" style="padding: 0px 8px;">
                                    <label for="ugn" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridUgn()"
                                               name="ugn"
                                               id="ugn"
                                               placeholder="&nbsp;">
                                        <span class="label" for="ugn" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;UGN</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-right" style="padding: 0px 8px 5px 0px; z-index:100000;">
                                    <button class="btn btn-success btn-sm"
                                            onclick="Cadastrar()"
                                            id="btnCadastrar">
                                        <span class="glyphicon glyphicon-plus"></span>&nbsp;Novo
                                    </button>
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($cnpjs as $cnpj) {
                                ?>
                                <tr>
                                    <td><strong><?php echo $cnpj['FANTASIA'] ?></strong><br>
                                        <small style="font-size:12px"><?php echo $cnpj['RAZAO_SOCIAL'] ?></small>
                                    </td>
                                    <td><?php echo $control->formatarCnpj($cnpj['CNPJ']) ?></td>
                                    <td class="text-left"><?php echo $cnpj['CIDADE'] ?></td>
                                    <td class="text-center">
                                        <?php
                                        echo $arrayUgn[$cnpj['CODIGO']];
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group btn-group-sm" role="group">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">AÇÃO <span
                                                        class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li onclick="Ficha(<?php echo $cnpj['ID'] ?>)">
                                                    <a href="#"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Visualizar</a>
                                                </li>
                                                <li>
                                                    <a href="#"
                                                       onclick="TrocarResponsavel(<?php echo $cnpj['ID'] ?>)"><span
                                                                class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;Responsável
                                                    </a>
                                                </li>
                                                <?php
                                                $editar = false;
                                                if (is_array($listIdEmpresa)) {
                                                    if (in_array($cnpj['ID'], $listIdEmpresa)) {
                                                        $editar = true;
                                                    }
                                                }
                                                if ($editar || $perfil == 7 || $perfil == 4) {
                                                    ?>
                                                    <li><a href="#"
                                                           onclick="Cadastrar(<?php echo $cnpj['ID'] ?>)"><span
                                                                    class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Editar</a>
                                                    </li>
                                                    <?php
                                                    if ($perfil == 7 || $perfil == 4) {
                                                        ?>
                                                        <li>
                                                            <a href="#"
                                                               onclick="ExcluirEmpresa(<?php echo $cnpj['ID'] ?>)"><span
                                                                        class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Excluir
                                                            </a>
                                                        </li>


                                                        <?php
                                                    }
                                                    ?>
                                                    <li><a href="#"
                                                           onclick="InserirAtividade(<?php echo $cnpj['ID'] ?>)"><span
                                                                    class="glyphicon glyphicon-calendar"></span>&nbsp;&nbsp;Atividade</a>
                                                    </li>
                                                    <?php
                                                }
                                                ?>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
            <div class="col-xs-12" style="display: none">
                <nav>
                    <ul class="pager">
                        <li class="paggerPoint" onclick="primeiraPagina()"><a>Primeiro</a></li>
                        <li class="paggerPoint" onclick="recuarPagina()"><a>Anterior</a></li>
                        <li>
                            <small>1/1</small>
                        </li>
                        <li class="paggerPoint" onclick="avancarPagina()"><a>Próximo</a></li>
                        <li class="paggerPoint" onclick="ultimaPagina()"><a>Último</a></li>
                    </ul>
                </nav>
            </div>
        </div>
        <?php
    }

    public function formCadastrarEmpresa($idEmpresa = '')
    {
        if ($idEmpresa > 0) {
            require_once('/../controller/cPaEmpresa.php');
            $control = new cPaEmpresa();
            $dadosEmpresa = $control->loadDadosEmpresabyId($idEmpresa);
        }
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('d/m/Y');
        ?>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                <img src="/../../public/img/l.png" width="100%"/>
            </div>
            <div class="col-sm-12" style="padding:0;margin: 0">
                <br><br>
                <ul class="progressbar">
                    <li class="atual" style="width: 10%">Cadastro CNPJ</li>
                    <li class="" style="width: 40%;padding-left: 10%">Dados Adicionais</li>
                    <li class="" style="width: 40%;padding-right:10%">Contatos</li>
                    <li class="" id="ultimoStep" style="width: 10%">Ficha Empresa</li>
                </ul>
                <br><br><br><br>
            </div>
            <div class="row" id="primeiraParte">
                <input type="hidden" id="idEmpresa" value="<?php echo $idEmpresa ?>">
                <div class="col-sm-4 text-left">
                    <label for="inp" class="inp">
                        <input disabled type="text" value="<?php echo $_SESSION['nome'] ?>" id="inp"
                               placeholder="&nbsp;">
                        <span class="label">
                            <?php
                            if ($idEmpresa > 0) {
                                echo "Editado por";
                            } else {
                                echo "Cadastrado por";
                            }
                            ?>
                        </span>
                        <span class="border"></span>
                    </label>
                </div>
                <div class="col-sm-3 text-left">
                    <label for="inp" class="inp">
                        <input disabled type="text" value="<?php echo $date ?>" id="inp" placeholder="&nbsp;">
                        <span class="label">
                            <?php
                            if ($idEmpresa > 0) {
                                echo "Data Edição";
                            } else {
                                echo "Data Cadastro";
                            }
                            ?>
                        </span>
                        <span class="border"></span>
                    </label>
                </div>
                <div class="col-sm-3 text-left">
                    <label for="txtCnpj" class="inp">
                        <input type="text" autocomplete="off"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][0] ?>"
                                disabled
                                <?php
                            }
                            ?>
                               class="maskCnpj" name="txtCnpj" id="txtCnpj"
                               placeholder="&nbsp;">
                        <span class="label" for="txtCnpj">CNPJ</span>
                        <span class="border" for="txtCnpj"></span>
                    </label>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-md">
                        <label for="buscarDados" style="color: white">.</label>
                        <span class="btn form-control btn-info" id="buscarDados" onclick="buscarDados()">
                        <span class="glyphicon glyphicon-search">
                        </span>
                            Verificar
                        </span>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <label for="txtrazaosocial" class="inp">
                        <input type="text" autocomplete="off" name="txtrazaosocial"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][1] ?>"
                                <?php
                            }
                            ?>
                               id="txtrazaosocial" placeholder="&nbsp;">
                        <span class="label" for="txtrazaosocial">Razão Social</span>
                        <span class="border" for="txtrazaosocial"></span>
                    </label>
                </div>
                <div class="col-sm-6 text-left">
                    <label for="txtfantasia" class="inp">
                        <input type="text" autocomplete="off" name="txtfantasia"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][2] ?>"
                                <?php
                            }
                            ?>
                               id="txtfantasia" placeholder="&nbsp;">
                        <span class="label" for="txtfantasia">Nome Fantasia</span>
                        <span class="border" for="txtfantasia"></span>
                    </label>
                </div>
                <div class="col-sm-3 text-left">
                    <label for="txtcep" class="inp">
                        <input type="text" autocomplete="off" class="maskCep" name="txtcep"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][3] ?>"

                                <?php
                            }
                            ?>
                               id="txtcep" placeholder="&nbsp;">
                        <span class="label" for="txtcep">CEP</span>
                        <span class="border" for="txtcep"></span>
                    </label>
                </div>
                <div class="col-sm-3 text-left">
                    <label for="txtcidade" class="inp">
                        <input type="text"
                               name="txtcidade"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][4] ?>"

                                <?php
                            }
                            ?>
                               id="txtcidade" placeholder="&nbsp;"
                               autocomplete="off">
                        <span class="label" for="txtcidade">Cidade</span>
                        <span class="border" for="txtcidade"></span>
                    </label>
                </div>
                <div class="col-sm-6 text-left">
                    <label for="txtbairro" class="inp">
                        <input type="text"
                               autocomplete="off"
                               name="txtbairro"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][5] ?>"

                                <?php
                            }
                            ?>
                               id="txtbairro"
                               placeholder="&nbsp;">
                        <span class="label" for="txtbairro">Bairro</span>
                        <span class="border" for="txtbairro"></span>
                        <span class="border" for="txtbairro"></span>
                    </label>
                </div>
                <div class="col-sm-8 col-md-9 text-left">
                    <label for="txtlogradouro" class="inp">
                        <input type="text"
                               autocomplete="off"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][6] ?>"

                                <?php
                            }
                            ?>
                               name="txtlogradouro"
                               id="txtlogradouro"
                               placeholder="&nbsp;">
                        <span class="label" for="txtlogradouro">Endereço</span>
                        <span class="border" for="txtlogradouro"></span>
                    </label>
                </div>
                <div class="col-sm-2 text-left">
                    <label for="txtnumero" class="inp">
                        <input type="text"
                               name="txtnumero"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][7] ?>"

                                <?php
                            }
                            ?>
                               autocomplete="off"
                               id="txtnumero"
                               placeholder="&nbsp;">
                        <span class="label" for="txtnumero">Número</span>
                        <span class="border" for="txtnumero"></span>
                    </label>
                </div>
                <div class="col-sm-2 col-md-1 text-left">
                    <label for="txtestado" class="inp">
                        <input type="text"
                               name="txtestado"
                               maxlength="2"
                               autocomplete="off"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][9] ?>"

                                <?php
                            }
                            ?>
                               size="2"
                               id="txtestado"
                               placeholder="&nbsp;">
                        <span class="label" for="txtestado">UF</span>
                        <span class="border" for="txtestado"></span>
                    </label>
                </div>
                <div class="col-sm-12 text-left">
                    <label for="txtcomplemento" class="inp">
                        <input type="text"
                               name="txtcomplemento"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['empresa'][1][8] ?>"

                                <?php
                            }
                            ?>
                               autocomplete="off"
                               id="txtcomplemento"
                               placeholder="&nbsp;">
                        <span class="label" for="txtcomplemento">Complemento</span>
                        <span class="border" for="txtcomplemento"></span>
                    </label>
                </div>
                <div class="col-sm-5 text-left">
                    <label for="txtemail" class="inp">
                        <input type="email"
                               name="txtemail"
                               id="txtemail"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['email'][1][0] ?>"

                                <?php
                            }
                            ?>
                               autocomplete="off"
                               placeholder="&nbsp;">
                        <span class="label" for="txtemail">E-mail</span>
                        <span class="border" for="txtemail"></span>
                    </label>
                </div>
                <div class="col-sm-4 text-left">
                    <label for="txttelefone" class="inp">
                        <input type="text" class="maskTel"
                               name="txttelefone"
                               id="txttelefone"
                            <?php
                            if ($idEmpresa > 0) {
                                ?>
                                value="<?php echo $dadosEmpresa['telefone'][1][0] ?>"

                                <?php
                            }
                            ?>
                               autocomplete="off"
                               maxlength="15"
                               placeholder="&nbsp;">
                        <span class="label" for="txttelefone">Telefone</span>
                        <span class="border" for="txttelefone"></span>
                    </label>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-md">
                        <label for="whatsapp" style="color:white">.df</label>
                        <button
                                onclick="marcarWhatsapp()"
                                name="whatsapp"
                            <?php
                            if ($idEmpresa > 0) {
                                if ($dadosEmpresa['telefone'][1][1] == 1) {
                                    ?>
                                    class="form-control btn btn-default btn-success"
                                    <?php
                                } else {
                                    ?>
                                    class="form-control btn btn-default"
                                    <?php
                                }
                            } else {
                                ?>
                                class="form-control btn btn-default"
                                <?php
                            }
                            ?>
                                id="whatsapp"
                        ><?php
                            if ($idEmpresa > 0) {
                                if ($dadosEmpresa['telefone'][1][1] == 1) {
                                    ?>✓ WhatsApp<?php
                                } else {
                                    ?>WhatsApp<?php
                                }
                            } else {
                                ?>WhatsApp<?php
                            }
                            ?></button>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <button class="btn btn-danger" onclick="location.reload()"><span
                                class="glyphicon glyphicon-remove"> </span>&nbsp;Cancelar
                    </button>
                </div>
                <div class="col-xs-6 text-right"
                    <?php
                    if ($idEmpresa == '') { ?>
                        style="display: none"
                    <?php } ?>
                     id="avancarInicio">
                    <button onclick="DadosAdicionais(<?php echo $idEmpresa ?>)" class="btn btn-info">Avançar&nbsp;
                        <span class="glyphicon glyphicon-menu-right"></span></button>
                </div>
            </div>
        </div>
        <?php
    }

    public function formDadosAdicionais($idEmpresa)
    {
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dadosEditar = $control->dadosAdicionais($idEmpresa);
        $dados = $control->controlSwitch('carregaDados');
        ?>
        <div class="row">
            <input type="hidden" id="empEditar" value="<?php echo $idEmpresa ?>"/>
            <div class="modal fade text-center" id="relacionaModal" role="dialog">
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 for="relacionamento">Relacionamento</h4>
                        </div>
                        <div class="modal-body">
                            <button style="margin-bottom: 3px" id="A1" class="relacionamento btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 1) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">A1
                            </button>
                            <button style="margin-bottom: 3px" id="B1" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 2) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">B1
                            </button>
                            <button style="margin-bottom: 3px" id="C1" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 3) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">C1
                            </button>
                            <br>
                            <button style="margin-bottom: 3px" id="A2" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 4) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">A2
                            </button>
                            <button style="margin-bottom: 3px" id="B2" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 5) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">B2
                            </button>
                            <button style="margin-bottom: 3px" id="C2" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 6) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">C2
                            </button>
                            <br>
                            <button style="margin-bottom: 3px" id="A3" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 7) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">A3
                            </button>
                            <button style="margin-bottom: 3px" id="B3" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 8) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">B3
                            </button>
                            <button style="margin-bottom: 3px" id="C3" class="relacionamento  btn-lg
                    <?php
                            if (is_array($dadosEditar[6][1])) {
                                if (($dadosEditar[6][1]['FK_REGUA']) == 9) {
                                    ?> btn-success<?php
                                }
                            }
                            ?>" onclick="Relacionamento(this)">C3
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                <img src="/../../public/img/l.png" width="100%"/>
            </div>
            <input type="hidden" id="idEmpresa" value="<?php echo $idEmpresa ?>">
            <div class="col-sm-12" style="padding:0px;margin: 0px">
                <br><br>
                <ul class="progressbar">
                    <li class="active" style="width: 10%">Cadastro CNPJ</li>
                    <li class="atual" style="width: 40%;padding-left: 10%">Dados Adicionais</li>
                    <li class="" style="width: 40%;padding-right:10%">Contatos</li>
                    <li class="" id="ultimoStep" style="width: 10%">Ficha Empresa</li>
                </ul>
                <br><br><br><br>
            </div>
            <div class="col-xs-12 col-md-10">
                <label for="responsavel" class="inp">
                    <input type="hidden"
                        <?php
                        if (is_array($dadosEditar[1][1])) {
                            ?>
                            value="<?php echo $dadosEditar[1][1]['FK_RESPONSAVEL'] ?> "
                            <?php
                        }
                        ?>
                           id="responsavelId"/>
                    <input type="text"
                           onkeyup="buscaNome()"
                           name="responsavel"
                           id="responsavel"
                           autocomplete="off"
                        <?php
                        if (is_array($dadosEditar[1][1])) {
                            foreach ($dados['colaborador'] as $colaborador) {
                                if ($dadosEditar[1][1]['FK_RESPONSAVEL'] == $colaborador['ID_COLABORADOR']) {
                                    ?>
                                    value="<?php echo $control->tirarAcentos($colaborador['NOME']) ?> "
                                    <?php
                                }
                            }
                        }
                        ?>
                           placeholder="&nbsp;">
                    <span class="label" for="responsavel"
                          style="font-size: 16px;">Responsável</span>
                    <span class="border"></span>
                </label>
                <div id="comboNomes"
                     style="display:none;
                                    min-height: 0;
                                    max-height: 200px;
                                    overflow: hidden;
                                    z-index: 1000;
                                    box-shadow: 2px 5px  #d6d6d6;
                                    border:1px solid #e6e6e6;
                                    position:absolute;
                                    background-color: white;
                                    padding:10px;
                                    width:90%;">
                    <table id="lista">
                        <?php
                        foreach ($dados['colaborador'] as $colaborador) {
                            echo '<tr style="cursor:pointer;display: none;" onclick="SelecionarNome(\'' . $control->tirarAcentos($colaborador['NOME']) . '\',\'' . $control->tirarAcentos($colaborador['ID_COLABORADOR']) . '\')" ><td>' . $control->tirarAcentos($colaborador['NOME']) . '</td></tr>';
                        }
                        ?>
                    </table>
                </div>
            </div>
            <div class="col-xs-12 col-md-2 text-right" style="padding-top: 25px">
                <div class="form-group form-group-sm">
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#relacionaModal">
                        <span class="glyphicon glyphicon-th"></span> Relacionamento
                    </button>
                </div>
            </div>
            <div class="col-sm-12 invisible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="numeroMedicos" class="inp">
                    <input type="number"
                           min='0'
                           oninput="this.value = Math.abs(this.value)"
                           name="numeromedicos"
                           id="numeroMedicos"
                           placeholder="&nbsp;"
                        <?php
                        if (is_array($dadosEditar[0][1])) {
                            ?>
                            value="<?php echo $dadosEditar[0][1]['NUMERO_MEDICOS'] ?>"
                            <?php
                        }
                        ?>
                    >
                    <span class="label" for="numeroMedicos">Qtd. Médicos</span>
                    <span class="border" for="numeroMedicos"></span>
                </label>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="numeroPacientes" class="inp">
                    <input type="number"
                           min='0'
                           oninput="this.value = Math.abs(this.value)"
                           name="numeroPacientes"
                        <?php
                        if (is_array($dadosEditar[0][1])) {
                            ?>
                            value="<?php echo $dadosEditar[0][1]['NUMERO_PACIENTES'] ?>"
                            <?php
                        }
                        ?>
                           id="numeroPacientes"
                           placeholder="&nbsp;">
                    <span class="label" for="numeroPacientes">Pacientes P/ Mês</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="pacientesdia" class="inp">
                    <input type="number"
                           min='0'
                           oninput="this.value = Math.abs(this.value)"
                           name="pacientesdia"
                           id="pacientesdia"
                        <?php
                        if (is_array($dadosEditar[0][1])) {
                            ?>
                            value="<?php echo $dadosEditar[0][1]['PACIENTES_DIA'] ?>"
                            <?php
                        }
                        ?>
                           placeholder="&nbsp;">
                    <span class="label" for="pacientesdia">Consultas p/ Dia</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-sm-12 invisible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="padding: 30px 0 0px 0">
                    <div class="col-xs-10"
                         style="font-size: 16px;margin-top: 3px">
                        <label for="AtendeParticular" style="font-size: 16px;">Atende Particular</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="AtendeParticular"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                   type="checkbox"
                                <?php
                                if (is_array($dadosEditar[2][1])) {
                                    ?>
                                    checked
                                    <?php
                                }
                                ?>
                                   onchange="CheckConsultas(this)">
                            <label for="AtendeParticular"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="valorConsulta" class="inp consultasCheck"
                    <?php
                    if (!is_array($dadosEditar[2][1])) {
                        ?>
                        style="display: none"
                        <?php
                    }
                    ?>>
                    <input type="text"
                           class="maskMoney consultasCheck"
                           name="valorConsulta"
                           id="valorConsulta"
                        <?php
                        if (is_array($dadosEditar[2][1])) {
                            ?>
                            value="<?php echo $dadosEditar[2][1]['VALOR'] ?>"
                            <?php
                        } else {
                            ?>
                            disabled
                            <?php
                        }
                        ?>
                           placeholder="&nbsp;">
                    <span class="label" for="valorConsulta">Valor Consulta</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="porcAtendimento" class="inp consultasCheck"
                    <?php
                    if (!is_array($dadosEditar[2][1])) {
                        ?>
                        style="display: none"
                        <?php
                    }
                    ?>>
                    <input type="number"
                           min='0'
                           oninput="this.value = Math.abs(this.value)"
                           name="porcAtendimento"
                           id="porcAtendimento"
                        <?php
                        if (is_array($dadosEditar[2][1])) {
                            ?>
                            value="<?php echo $dadosEditar[2][1]['PERCENTUAL'] ?>"
                            <?php
                        } else {
                            ?>
                            disabled
                            <?php
                        }
                        ?>
                           class="consultasCheck"
                           placeholder="&nbsp;">
                    <span class="label" for="porcAtendimento">% do Atendimento</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-sm-12 invisible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="padding: 25px 0 0px 0">
                    <div class="col-xs-10"
                         style="font-size: 16px;margin-top: 1px">
                        <label for="consultoria" style="font-size: 16px;">Consultoria</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="consultoria"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                <?php
                                if (is_array($dadosEditar[4][1])) {
                                    ?>
                                    checked
                                    <?php
                                }
                                ?>
                                   type="checkbox"
                                   onchange="Consultoria(this)">
                            <label for="consultoria"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4" style="padding-top: 10px">
                <div class="select select--white" id="selectConsultoria"
                    <?php
                    if (!is_array($dadosEditar[8][1]))
                        echo 'style="display:none;"';
                    ?>
                     onclick="AbrirSelect(this)">
                    <span class="placeholder">
                        <?php
                        if (is_array($dadosEditar[8][1])) {
                            foreach ($dados['consultoria'] as $consultoria) {
                                if ($consultoria['ID'] == $dadosEditar[8][1]['FK_CONSULTORIA'])
                                    echo $consultoria['NOME'];
                            }
                        } else {
                            echo "Consultoria";
                        }
                        ?>
                    </span>
                    <ul>
                        <?php
                        foreach ($dados['consultoria'] as $consultoria) {
                            ?>
                            <li onclick="Selecionar(this)"
                                data-value="<?php echo $consultoria['ID'] ?>"><?php echo $consultoria['NOME'] ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="hidden" id="valueConsultoria"
                        <?php
                        if (is_array($dadosEditar[8][1])) {
                            ?>
                            value="<?php echo $dadosEditar[8][1]['FK_SITUACAO'] ?>"
                            <?php
                        }
                        ?>
                           name="changemetoo"/>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4" style="padding-top: 10px">
                <div class="select select--white" id="selectSituacao"
                    <?php
                    if (!is_array($dadosEditar[8][1]))
                        echo 'style="display:none;"';
                    ?>
                     onclick="AbrirSelect(this)">
                    <span class="placeholder">
                        <?php
                        if (is_array($dadosEditar[8][1])) {
                            foreach ($dados['situacao'] as $situacao) {
                                if ($situacao['ID'] == $dadosEditar[8][1]['FK_SITUACAO'])
                                    echo $situacao['DESCRICAO'];
                            }

                        } else {
                            echo "Situação";
                        }
                        ?></span>
                    <ul>
                        <?php
                        foreach ($dados['situacao'] as $situacao) {
                            ?>
                            <li onclick="Selecionar(this)"
                                data-value="<?php echo $situacao['ID'] ?>"><?php echo $situacao['DESCRICAO'] ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="hidden" id="valueSituacao"
                        <?php
                        if (is_array($dadosEditar[8][1])) {
                            ?>
                            value="<?php echo $dadosEditar[8][1]['FK_SITUACAO'] ?>"
                            <?php
                        }
                        ?>
                           name="changemetoo"/>
                </div>
            </div>
            <div class="col-sm-12 invisible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-sm-4">
                <div class="row" style="padding: 30px 0 0px 0">
                    <div class="col-xs-10"
                         style="font-size: 16px;margin-top: 1px">
                        <label for="convenios" style="font-size: 16px;">Atende Convênio</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="convenios"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                <?php
                                if (is_array($dadosEditar[8][1])) {
                                    ?>
                                    checked
                                    <?php
                                }
                                ?>
                                   type="checkbox"
                                   onchange="Convenios(this)">
                            <label for="convenios"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="objetivo" class="inp ">
                    <input type="number"
                           min='0'
                           oninput="this.value = Math.abs(this.value)"
                           name="objetivo"
                           id="objetivo"
                        <?php
                        if (is_array($dadosEditar[7][1])) {
                            ?>
                            value="<?php echo $dadosEditar[7][1]['OBJETIVO'] ?>"
                            <?php
                        }
                        ?>
                           placeholder="&nbsp;">
                    <span class="label" for="objetivo">% de Objetivo </span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-xs-12 col-sm-4">
                <label for="previsao" class="inp ">
                    <input type="date"
                           class=""
                           name="previsao"
                           id="previsao"
                        <?php
                        if (is_array($dadosEditar[7][1])) {
                            ?>
                            value="<?php echo $dadosEditar[7][1]['PRAZO'] ?>"
                            <?php
                        }
                        ?>
                           placeholder="&nbsp;">
                    <span class="label" for="previsao">Previsão Objetivo</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-xs-12">
                <br>
            </div>
            <div class="col-xs-12 col-md-4" style="height: 180px; overflow: auto">
                <b>
                    <span style="font-size: 16px;">Plataforma</span>
                </b>
                <ul class="ks-cboxtags" style="border: 1px solid #d6d6d6;">
                    <?php
                    foreach ($dados['plataforma'] as $plataforma) {
                        ?>
                        <li>
                            <input type="checkbox"
                                <?php
                                foreach ($dadosEditar[3] as $value) {
                                    if ($plataforma->getId() == $value[0]) {
                                        ?>
                                        checked
                                        <?php
                                    }
                                }
                                ?>
                                   id="plataforma_<?php echo $plataforma->getId() ?>"
                                   class="plataforma"
                                   name="plataforma[]"
                                   value="<?php echo $plataforma->getId() ?>"/>
                            <label for="plataforma_<?php echo $plataforma->getId() ?>" style="width:100%">
                                <?php echo $plataforma->getNome() ?>
                            </label>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-xs-12 visible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-md-4" style="height: 180px; overflow: auto">
                <b>
                        <span style="font-size: 16px;">
                            Especialidade
                        </span>
                </b>
                <ul class="ks-cboxtags" style="border: 1px solid #d6d6d6;">
                    <?php
                    foreach ($dados['especialidade'] as $especialidade) {
                        ?>
                        <li>
                            <input type="checkbox" class="especialidade"
                                <?php
                                foreach ($dadosEditar[5] as $value) {
                                    if ($especialidade->getId() == $value[0]) {
                                        ?>
                                        checked
                                        <?php
                                    }
                                }
                                ?>
                                   id="especialidade_<?php echo $especialidade->getId() ?>"
                                   name="especialidade[]"
                                   value="<?php echo $especialidade->getId() ?>">
                            <label for="especialidade_<?php echo $especialidade->getId() ?>" style="width:100%">
                                <?php echo $especialidade->getNome() ?>
                            </label>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="col-xs-12 visible-xs">
                <br>
            </div>
            <div class="col-xs-12 col-md-4" id="DivcheckboxPlano"
                 style="height: 180px;overflow: auto; <?php if (!is_array($dadosEditar[4][1])) { ?>display:
                         none;<?php } ?>">
                <b>
                    <span style="font-size: 16px;">
                        Convênios
                    </span>
                </b>
                <ul class="ks-cboxtags" style=" border: 1px solid #d6d6d6; ">
                    <?php
                    foreach ($dados['convenio'] as $convenio) {
                        ?>
                        <li>
                            <input type="checkbox" id="convenio_<?php echo $convenio->getId() ?>"
                                <?php
                                foreach ($dadosEditar[4] as $value) {
                                    if ($convenio->getId() == $value[0]) {
                                        ?>
                                        checked
                                        <?php
                                    }
                                }
                                ?>
                                   class="convenio" name="convenio[]"
                                   value="<?php echo $convenio->getId() ?>">
                            <label for="convenio_<?php echo $convenio->getId() ?>" style="width:100%">
                                <?php echo strtoupper($convenio->getNome()) ?>
                            </label>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>

            <hr>
            <div class="col-xs-12">
                <br>
                <div class="row">
                    <div class="col-xs-6">
                    </div>
                    <div class="col-xs-6 text-right">
                        <button onclick="Contatos(<?php echo $idEmpresa ?>)" class="btn btn-info">Avançar <span
                                    class="glyphicon glyphicon-menu-right"></span></button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function formUsuarios($idEmpresa, $contatos = '')
    {
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dados = $control->controlSwitch('carregaDados');
        $sis = new appFunction();
        ?>
        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
            <img src="/../../public/img/l.png" width="100%"/>
        </div>
        <input type="hidden" value="<?php echo $idEmpresa ?>" id="idEmpresa"/>
        <div class="col-sm-12" style="padding:0;margin: 0">
            <br><br>
            <ul class="progressbar">
                <li class="active" style="width: 10%">Cadastro CNPJ</li>
                <li class="active" style="width: 40%;padding-left: 10%">Dados Adicionais</li>
                <li class="atual" style="width: 40%;padding-right:10%">Contatos</li>
                <li class="" id="ultimoStep" style="width: 10%">Ficha Empresa</li>
            </ul>
            <br><br><br><br>
        </div>
        <div class="row" id="terceiraParte">
            <div class="col-xs-4" style="padding-top: 13px">
                <div class="select select--white" id="selectTipo" onclick="AbrirSelect(this)">
                    <span class="placeholder">Tipo Contato</span>
                    <ul>
                        <?php
                        foreach ($dados['tipocontato'] as $contato) {
                            ?>
                            <li onclick="Selecionar(this)"
                                data-value="<?php echo $contato->getId() ?>"><?php echo $contato->getDescricao() ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <input type="hidden" id="tipoContato" name="changemetoo"/>
                </div>
            </div>
            <div class="col-md-3 col-lg-2">
                <div class="row" style="padding: 25px 0 0px 0">
                    <div class="col-xs-10 col-sm-10"
                         style="font-size: 16px;margin-top: 3">
                        <label for="decisor" style="font-size: 16px;">Decisor</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="decisor"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                   type="checkbox">
                            <label for="decisor"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-2">
                <div class="row" style="padding: 25px 0 0px 0">
                    <div class="col-xs-10"
                         style="font-size: 16px;margin-top: 3">
                        <label for="medico" style="font-size: 16px;">Médico</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="medico"
                                   onclick="isMedico(this)"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                   type="checkbox">
                            <label for="medico"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-2 ismedico" style="display: none">
                <div class="row" style="padding: 25px 0 0px 0">
                    <div class="col-xs-10"
                         style="font-size: 16px;margin-top: 3">
                        <label for="residente" style="font-size: 16px;">Residente</label>
                    </div>
                    <div class="col-xs-2">
                        <div class="switch__container pull-right">
                            <input id="residente"
                                   class="switch switch--shadow"
                                   style="align: right;"
                                   type="checkbox">
                            <label for="residente"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-2 ismedico" style="padding-top: 13px;display: none">
                        <div class="select select--white" id="selectUf" onclick="AbrirSelect(this)">
                            <span class="placeholder">UF</span>
                            <ul>
                                <?php
                                foreach ($dados['estado'] as $estado) {
                                    ?>
                                    <li onclick="Selecionar(this)"
                                        data-value="<?php echo $estado[1] ?>"><?php echo $estado[1] ?></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <input type="hidden" id="estadoMedico" name="uf"/>
                        </div>
                    </div>
                    <div class="col-sm-2 ismedico" style="display: none">
                        <label for="crm" class="inp">
                            <input type="text"
                                   name="crm"
                                   id="crm"
                                   placeholder="&nbsp;">
                            <span class="label" for="crm">CRM</span>
                            <span class="border"></span>
                        </label>
                    </div>
                    <div class="col-sm-2 ismedico" style="display: none">
                        <div class="form-group form-group-md">
                            <label for="complemento" style="color:white">.df</label>
                            <button onclick="BuscaCrm()"
                                    name="whatsapp"
                                    class="form-control btn btn-success"
                            ><span class="glyphicon glyphicon-search"></span> Buscar Médico
                            </button>
                        </div>
                    </div>
                    <div class="col-sm-6 text-left">
                        <label for="txtnomecontato" class="inp">
                            <input type="text"
                                   name="txtnomecontato"
                                   id="txtnomecontato"
                                   placeholder="&nbsp;">
                            <span class="label" for="txtnomecontato">Nome</span>
                            <span class="border"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="col-sm-2 text-left">
                <label for="dataNasc" class="inp">
                    <input type="date"
                           name="dataNasc"
                           id="dataNasc"
                           placeholder="&nbsp;">
                    <span class="label" for="dataNasc">Data Nascimento</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-sm-4 text-left">
                <label for="txtemail" class="inp">
                    <input type="text"
                           name="txtemail"
                           id="txtemail"
                           placeholder="&nbsp;">
                    <span class="label" for="txtemail">E-mail</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-sm-4 text-left">
                <label for="txtTelefone" class="inp">
                    <input type="email"
                           name="txtTelefone"
                           id="txtTelefone"
                           class="maskTel"
                           placeholder="&nbsp;">
                    <span class="label" for="txtTelefone">Telefone</span>
                    <span class="border"></span>
                </label>
            </div>
            <div class="col-sm-2 text-left">
                <div class="form-group form-group-md">
                    <label for="complemento" style="color:white">.df</label>
                    <button
                            onclick="marcarWhatsapp()"
                            name="whatsapp"
                            class="form-control btn btn-default"
                            id="whatsapp"
                    >WhatsApp
                    </button>
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <button class="btn btn-success btn-sm"
                        onclick="SalvarContato()"
                        id="btnCadastrar">
                    <span class="glyphicon glyphicon-plus"></span>&nbsp;Salvar Contato
                </button>

            </div>
            <div class="col-sm-12 text-left" style="max-height:280px; overflow: auto;">
                <br>
                <table class="table table-condensed table-hover" id="tableContatos">
                    <thead>
                    <tr class="active">
                        <th>Nome</th>
                        <th>Decisor</th>
                        <th>Tipo</th>
                        <th>Residente</th>
                        <th>Contato</th>
                        <th>Nascimento</th>
                        <th class="text-right">Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    if (is_array($contatos))
                        foreach ($contatos as $o) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo $o->getNome();
                                    if ($o->getCrm() > 0) {
                                        echo "<br><b>CRM: </b>" . $o->getUf() . $o->getCrm();
                                    }
                                    ?></td>
                                <td>
                                    <?php if ($o->getDecisor() == 1) {
                                        echo "Sim";
                                    } else {
                                        echo "Não";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $o->getContatoTipo();
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($o->getResidente() == 1) {
                                        echo "Sim";
                                    } else {
                                        echo "Não";
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    echo $o->getEmail();
                                    ?>
                                    <br>
                                    <?php
                                    echo $o->getTelefone();
                                    ?>
                                    <?php
                                    if ($o->getWhatsapp() == 1) {
                                        ?>
                                        <img width="14"
                                             src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/>
                                        <?php
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php if ($o->getDataNasc() != '1900-01-01') {
                                        echo '-';
                                    } else {
                                        echo substr($sis->fn_formatarData($o->getDataNasc()), 0, 10);
                                    } ?>
                                </td>
                                <td class="text-right">
                                    <div class="btn-group btn-group-sm" role="group">
                                        <button type="button" class="btn btn-info dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">AÇÃO
                                            <span class="caret"></span></button>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="#" onclick="EditarContato(<?php echo $o->getId() ?>)"><span
                                                            class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Editar</a>
                                            </li>
                                            <li><a href="#" onclick="ExcluirContato(<?php echo $o->getId() ?>)"><span
                                                            class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Excluir</a>
                                            </li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                            <?php
                            $i++;
                        }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-6">
            </div>
            <div class="col-xs-6 text-right">
                <button onclick="Ficha(<?php echo $idEmpresa ?>)" class="btn btn-info">Avançar <span
                            class="glyphicon glyphicon-menu-right"></span></button>
            </div>
        </div>
        <script>

        </script>
        <?php
    }

    public function formFichaEmpresaVisualizar()
    {
        $idEmpresa = $_POST['idEmpresa'];
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dados = $control->controlSwitch('carregaDados');
        $dadosEmpresa = $control->loadDadosEmpresabyId($idEmpresa);
        $dadosAdicionais = $control->dadosAdicionais($idEmpresa);
        $contatos = $control->listarContatosByIdEmpresa($idEmpresa);
        ?>
        <div class="modal" id="modalFichaMedico" tabindex="-1"
             role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" aria-backdrop="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha
                            do Médico</h5>
                    </div>
                    <div class="modal-body max-height">
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""
                           id="btn-exportar-ficha-medico" data-dismisss="modal">
                            <span class="glyphicon glyphicon-file"></span> Exportar em CSV
                        </a>

                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Fechar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                <img src="/../../public/img/l.png" width="100%"/>
            </div>
            <div class="col-sm-12" style="border:   1px solid rgba(236,236,236,0.7)">
                <div class="row" id="quartaParte">
                    <div class="col-xs-6">
                        <br>
                        <fieldset style=" padding: 5px;">
                            <legend>Empresa</legend>
                            <div class="row">
                                <div class="col-xs-9">
                                    <strong>
                        <span style="font-size: 22px"><?php echo $dadosEmpresa['empresa'][1]['FANTASIA'] ?>
                        </span>
                                    </strong>
                                    <br>
                                    <small><?php echo $dadosEmpresa['empresa'][1]['RAZAO_SOCIAL'] ?></small>
                                    <br>
                                    <?php echo $control->formatarCnpj($dadosEmpresa['empresa'][1]['CNPJ']) ?>
                                    <br>
                                </div>
                                <div class="col-xs-3">
                                    <?php
                                    $relacionamento = '';
                                    switch ($dadosAdicionais[6][1]['FK_REGUA']) {
                                        case 1:
                                            $relacionamento = 'A1';
                                            break;
                                        case 2:
                                            $relacionamento = 'B1';
                                            break;
                                        case 3:
                                            $relacionamento = 'C1';
                                            break;
                                        case 4:
                                            $relacionamento = 'A2';
                                            break;
                                        case 5:
                                            $relacionamento = 'B2';
                                            break;
                                        case 6:
                                            $relacionamento = 'C2';
                                            break;
                                        case 7:
                                            $relacionamento = 'A3';
                                            break;
                                        case 8:
                                            $relacionamento = 'B3';
                                            break;
                                        case 9:
                                            $relacionamento = 'C3';
                                            break;
                                    }
                                    ?>
                                    <span style="font-size: 40px;color: #00CC00"><?php echo $relacionamento ?></span>
                                </div>
                                <div class="col-xs-12">
                                    <b>Responsável: </b> <?php echo $control->nomeResponsavel($idEmpresa) ?>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Endereço</label><br>
                                    <span> <?php echo $dadosEmpresa['empresa'][1]['LOGRADOURO'] . ' - ' . $dadosEmpresa['empresa'][1]['NUMERO'] ?></span><br>
                                    <span><?php
                                        if ($dadosEmpresa['empresa'][1]['COMPLEMENTO'] != '')
                                            echo $dadosEmpresa['empresa'][1]['COMPLEMENTO'] . '<br>';
                                        ?></span>
                                    <span> <?php echo $dadosEmpresa['empresa'][1]['BAIRRO'] . ' <br>' . $dadosEmpresa['empresa'][1]['CIDADE'] . ' - ' . $dadosEmpresa['empresa'][1]['ESTADO'] ?></span><br>
                                    <span> <?php echo $control->formatarCep($dadosEmpresa['empresa'][1]['CEP']) ?></span>
                                </div>
                                <div class="col-xs-6">
                                    <label>Contato</label><br>
                                    <span> <?php echo $dadosEmpresa['telefone'][1]['TEL'] ?>&nbsp;&nbsp;
                                <?php
                                if ($dadosEmpresa['telefone'][1]['WHATSAPP'] == 1){ ?>
                                <img width="14"
                                     src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></span><br>
                                    <?php } else {
                                        echo '<br>';
                                    } ?>
                                    <span><?php echo $dadosEmpresa['email'][1]['EMAIL'] ?></span>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-6">
                                    <label>Dados Atendimento</label><br>
                                    <span> <?php echo $dadosAdicionais[0][1]['NUMERO_MEDICOS'] . ' Médicos' ?></span><br>
                                    <span><?php echo $dadosAdicionais[0][1]['NUMERO_PACIENTES'] . ' Pacientes Por Mês' ?></span><br>
                                    <span> <?php echo $dadosAdicionais[0][1]['PACIENTES_DIA'] . " Consultas Por Dia" ?></span><br>
                                </div>
                                <div class="col-xs-6">
                                    <label>Dados Consultas</label><br>
                                    <?php if ($dadosAdicionais[2][1]['PERCENTUAL'] > 0) { ?>
                                        <span> <?php echo $dadosAdicionais[2][1]['PERCENTUAL'] . '% Particular' ?></span>
                                        <br>
                                    <?php } ?>
                                    <?php if ($dadosAdicionais[2][1]['PERCENTUAL'] > 0) { ?>
                                        <span> <?php echo 100 - $dadosAdicionais[2][1]['PERCENTUAL'] . '% Convênio' ?></span>
                                        <br>
                                    <?php } else { ?>
                                        <span>100% Convênio</span>
                                        <br>
                                    <?php } ?>
                                    <?php if ($dadosAdicionais[2][1]['VALOR'] > 0) { ?>
                                        <span> <?php echo '<b>Valor: </b>R$ ' . $dadosAdicionais[2][1]['VALOR'] ?></span>
                                        <br>
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-12">
                                    <label>Objetivo de Crescimento</label><br>
                                    <?php
                                    $sis = new appFunction();
                                    if (is_array($dadosAdicionais[7][1])) { ?>
                                        <span>Objetivo de crescimento de <?php echo $dadosAdicionais[7][1]['OBJETIVO'] . '% até ' . substr($sis->fn_formatarData($dadosAdicionais[7][1]['PRAZO']), 0, 10) ?></span>
                                    <?php } else {
                                        echo 'Não definido';
                                    }
                                    ?>
                                    <br>
                                </div>
                                <br>

                                <div class="col-xs-12">
                                    <br>
                                    <?php
                                    if (is_array($dadosAdicionais[8][1])) { ?>
                                        <label>Consultoria</label><br>
                                        <?php
                                        foreach ($dados['consultoria'] as $consult) {
                                            if ($dadosAdicionais[8][1]['FK_CONSULTORIA'] == $consult['ID']) {
                                                ?>
                                                <span><?php echo $consult['NOME'] ?></span>
                                                <?php
                                            }

                                        }
                                        ?>
                                        <?php
                                        foreach ($dados['situacao'] as $consult) {
                                            if ($dadosAdicionais[8][1]['FK_SITUACAO'] == $consult['ID']) {
                                                ?>
                                                <br>
                                                <span><?php echo '<strong>Situação</strong><br>' . $consult['DESCRICAO'] ?></span>
                                                <br>
                                                <?php
                                            }

                                        }
                                        ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-xs-4">
                                    <table class="table table-striped" style="font-size: 13px;">
                                        <tr>
                                            <th>Plataformas</th>
                                        </tr>
                                        <?php
                                        foreach ($dados['plataforma'] as $dado) {
                                            foreach ($dadosAdicionais[3] as $value)
                                                if ($value[FK_PLATAFORMA] == $dado->getId())
                                                    echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                                <div class="col-xs-4">
                                    <table class="table table-striped" style="font-size: 13px;">
                                        <tr>
                                            <th>Especialidades</th>
                                        </tr>
                                        <?php
                                        foreach ($dados['especialidade'] as $dado) {
                                            foreach ($dadosAdicionais[5] as $value)
                                                if ($value[FK_ESPECIALIDADE] == $dado->getId())
                                                    echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                        }
                                        ?>
                                    </table>
                                </div>
                                <div class="col-xs-4">
                                    <table class="table table-striped" style="font-size: 13px;">
                                        <?php
                                        if ($dadosAdicionais[4][1]) {
                                            ?>
                                            <tr>
                                                <th>Convênios</th>
                                            </tr>
                                            <?php
                                        }
                                        foreach ($dados['convenio'] as $dado) {
                                            foreach ($dadosAdicionais[4] as $value)
                                                if ($value['FK_CONVENIO'] == $dado->getId())
                                                    echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                        }

                                        ?>
                                    </table>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                    <div class="col-xs-6">
                        <br>
                        <fieldset style=" padding: 5px;">
                            <legend>Contatos</legend>
                            <div style="height: 450px; overflow:auto;">
                                <?php
                                $i = 0;
                                foreach ($contatos as $o) {
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-7 text-left">
                                            <?php
                                            echo '<b>' . $o->getNome() . '</b>'; ?>

                                            <?php
                                            if ($o->getCrm() > 0) {
                                                ?>
                                                <span class="glyphicon glyphicon-list-alt"
                                                      onclick="FichaMedico('<?php echo $control->localizarIdCrm($o->getUf() . $o->getCrm())[1][0]; ?>', '<?php echo $o->getNome() ?>', '', 'TRM01|TRM00')"></span>
                                                <?php
                                            }
                                            ?>
                                            <?php
                                            if ($o->getCrm() > 0) {
                                                echo '<br><b>CRM: </b>' . $o->getUf() . $o->getCrm();
                                                if (is_object($dadosEditar['medicos'][$i][0])) {
                                                    if ($o->getResidente() == 1) {
                                                        echo " - Residente";
                                                    }
                                                }
                                                echo '<br>';
                                            } else {
                                                echo '<br>';
                                            }
                                            if ($o->getDecisor() == 1) {
                                                echo "Decisor - ";
                                            } else {
                                                echo "Não Decisor - ";
                                            }
                                            echo $o->getContatoTipo() . '<br>';
                                            ?>
                                        </div>
                                        <div class="col-sm-5 text-left">
                                            <?php
                                            echo $o->getEmail() . "<br>";
                                            echo $o->getTelefone();
                                            if ($o->getWhatsapp() == 1) { ?>
                                                <img width="14"
                                                     src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></span>
                                                <?php
                                            }
                                            echo ' <br>';
                                            $sis = new appFunction();
                                            if ($o->getDataNasc() != '1900-01-01')
                                                echo substr($sis->fn_formatarData($o->getDataNasc()), 0, 10);
                                            $i++; ?>
                                        </div>
                                    </div>
                                    <br>
                                    <?php
                                }
                                ?>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xs-6">

            </div>
            <div class="col-xs-6 text-right">
                <span class="btn btn-danger" onclick="location.reload()">Voltar</span>
            </div>
        </div>
        <?php
    }

    public function formAtividades()
    {
        $idEmpresa = $_POST['id'];
        $idAtividade = -1;
        $acao = $_POST['acao'];
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dados = $control->controlSwitch('carregaDados');
        $dadosEmpresa = $control->loadDadosEmpresabyId($idEmpresa);
        $dadosAdicionais = $control->dadosAdicionais($idEmpresa);
        $contatos = $control->listarContatosByIdEmpresa($idEmpresa);
        if ($idAtividade > 0) {
            $atividade = $control->loadAtividade($idAtividade);
        }
        ?>
        <div class="modal fade in" id="resumoAtividademd" role="dialog"
             style="display: none;background: rgba(000,000,000,0.3)">
            <div class="modal-dialog" id="resumoAtividade" style="display: none">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h3 class="text-center text-primary">Confirmar Atividade</h3>
                    </div>
                    <div class="modal-body" id="resumoAtividademsg">
                    </div>
                    <div class="modal-footer">
                        <button type="button" onclick="salvarAtividade()" class="btn btn-success" data-dismiss="modal">
                            Confirmar
                        </button>
                        <button type="button" onclick="CancelaAtividade()" class="btn btn-danger" data-dismiss="modal">
                            Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                            <img src="/../../public/img/l.png" width="100%"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <fieldset>
                        <div class="col-xs-9">
                            <br>
                            <legend id="lblacao">Agendar Atividade</legend>
                        </div>
                        <div class="col-xs-3">
                            <br>
                            <div class="btn-group btn-group-justified btn-group btn-group-md" role="group">
                                <a onclick="alternaBotao('phone')" id="phone"
                                   class="btn btn-default btnatividades active" role="button"><span
                                            class="glyphicon glyphicon-phone"></span></a>
                                <a onclick="alternaBotao('mailer')" id="mailer"
                                   class="btn btn-default btnatividades" role="button"><span
                                            class="glyphicon glyphicon-envelope"></span></a>
                                <a onclick="alternaBotao('visit')" id="visit"
                                   class="btn btn-default btnatividades" role="button"><span
                                            class="glyphicon glyphicon-user"></span></a>
                                <a style="display: none" href="#" class="btn btn-default " role="button"><span
                                            class="glyphicon glyphicon-copy"></span></a>
                            </div>
                        </div>
                    </fieldset>
                </div>
                <div class="row">
                    <div class="modal" id="modalFichaMedico" tabindex="-1"
                         role="dialog" aria-labelledby="myModalLabel"
                         aria-hidden="true" aria-backdrop="false">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header btn-primary">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <h5 class="modal-title" id="myModalLabel"><span
                                                class="glyphicon glyphicon-plus"></span> Ficha
                                        do Médico</h5>
                                </div>
                                <div class="modal-body max-height">
                                </div>
                                <div class="modal-footer">
                                    <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""
                                       id="btn-exportar-ficha-medico" data-dismisss="modal">
                                        <span class="glyphicon glyphicon-file"></span> Exportar em CSV
                                    </a>

                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                        <span class="glyphicon glyphicon-remove"></span> Fechar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-9" style="border:   1px solid rgba(236,236,236,0.7)">
                        <div class="row" id="quartaParte">
                            <div class="col-xs-6">
                                <br>
                                <fieldset style=" padding: 5px;">
                                    <legend>Empresa</legend>
                                    <div class="row">
                                        <div class="col-xs-9">
                                            <strong>
                        <span style="font-size: 22px"><?php echo $dadosEmpresa['empresa'][1]['FANTASIA'] ?>
                        </span>
                                            </strong>
                                            <br>
                                            <small><?php echo $dadosEmpresa['empresa'][1]['RAZAO_SOCIAL'] ?></small>
                                            <br>
                                            <?php echo $control->formatarCnpj($dadosEmpresa['empresa'][1]['CNPJ']) ?>
                                        </div>
                                        <div class="col-xs-3">
                                            <?php
                                            $relacionamento = '';
                                            switch ($dadosAdicionais[6][1]['FK_REGUA']) {
                                                case 1:
                                                    $relacionamento = 'A1';
                                                    break;
                                                case 2:
                                                    $relacionamento = 'B1';
                                                    break;
                                                case 3:
                                                    $relacionamento = 'C1';
                                                    break;
                                                case 4:
                                                    $relacionamento = 'A2';
                                                    break;
                                                case 5:
                                                    $relacionamento = 'B2';
                                                    break;
                                                case 6:
                                                    $relacionamento = 'C2';
                                                    break;
                                                case 7:
                                                    $relacionamento = 'A3';
                                                    break;
                                                case 8:
                                                    $relacionamento = 'B3';
                                                    break;
                                                case 9:
                                                    $relacionamento = 'C3';
                                                    break;
                                            }

                                            ?>
                                            <span style="font-size: 40px;color: #00CC00"><?php echo $relacionamento ?></span>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php
                                            foreach ($dados['colaborador'] as $dado) {
                                                if ($dado['ID_COLABORADOR'] == $dadosAdicionais[6][1]['FK_RESPONSAVEL']) {
                                                    echo '<b>Responsável: </b>' . $dado['NOME'];
                                                    break;
                                                }
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label>Endereço</label><br>
                                            <span> <?php echo $dadosEmpresa['empresa'][1]['LOGRADOURO'] . ' - ' . $dadosEmpresa['empresa'][1]['NUMERO'] ?></span><br>
                                            <span><?php
                                                if ($dadosEmpresa['empresa'][1]['COMPLEMENTO'] != '')
                                                    echo $dadosEmpresa['empresa'][1]['COMPLEMENTO'] . '<br>';
                                                ?></span>
                                            <span> <?php echo $dadosEmpresa['empresa'][1]['BAIRRO'] . ' <br>' . $dadosEmpresa['empresa'][1]['CIDADE'] . ' - ' . $dadosEmpresa['empresa'][1]['ESTADO'] ?></span><br>
                                            <span> <?php echo $control->formatarCep($dadosEmpresa['empresa'][1]['CEP']) ?></span>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Contato</label><br>
                                            <span> <?php echo $dadosEmpresa['telefone'][1]['TEL'] ?>&nbsp;&nbsp;
                                <?php
                                if ($dadosEmpresa['telefone'][1]['WHATSAPP'] == 1){ ?>
                                <img width="14"
                                     src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></span><br>
                                            <?php } else {
                                                echo '<br>';
                                            } ?>
                                            <span><?php echo $dadosEmpresa['email'][1]['EMAIL'] ?></span>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <label>Dados Atendimento</label><br>
                                            <span> <?php echo $dadosAdicionais[0][1]['NUMERO_MEDICOS'] . ' Médicos' ?></span><br>
                                            <span><?php echo $dadosAdicionais[0][1]['NUMERO_PACIENTES'] . ' Pacientes Por Mês' ?></span><br>
                                            <span> <?php echo $dadosAdicionais[0][1]['PACIENTES_DIA'] . " Consultas Por Dia" ?></span><br>
                                        </div>
                                        <div class="col-xs-6">
                                            <label>Dados Consultas</label><br>
                                            <?php if ($dadosAdicionais[2][1]['PERCENTUAL'] > 0) { ?>
                                                <span> <?php echo $dadosAdicionais[2][1]['PERCENTUAL'] . '% Particular' ?></span>
                                                <br>
                                            <?php } ?>
                                            <?php if ($dadosAdicionais[2][1]['PERCENTUAL'] > 0) { ?>
                                                <span> <?php echo 100 - $dadosAdicionais[2][1]['PERCENTUAL'] . '% Convênio' ?></span>
                                                <br>
                                            <?php } else { ?>
                                                <span>100% Convênio</span>
                                                <br>
                                            <?php } ?>
                                            <?php if ($dadosAdicionais[2][1]['VALOR'] > 0) { ?>
                                                <span> <?php echo '<b>Valor: </b>R$ ' . $dadosAdicionais[2][1]['VALOR'] ?></span>
                                                <br>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <label>Objetivo de Crescimento</label><br>
                                            <?php
                                            $sis = new appFunction();
                                            if (is_array($dadosAdicionais[7][1])) { ?>
                                                <span>Objetivo de crescimento de <?php echo $dadosAdicionais[7][1]['OBJETIVO'] . '% até ' . substr($sis->fn_formatarData($dadosAdicionais[7][1]['PRAZO']), 0, 10) ?></span>
                                            <?php } else {
                                                echo 'Não definido';
                                            }
                                            ?>
                                            <br>
                                        </div>
                                        <br>

                                        <div class="col-xs-12">
                                            <br>
                                            <?php
                                            if (is_array($dadosAdicionais[8][1])) { ?>
                                                <label>Consultoria</label><br>
                                                <?php
                                                foreach ($dados['consultoria'] as $consult) {
                                                    if ($dadosAdicionais[8][1]['FK_CONSULTORIA'] == $consult['ID']) {
                                                        ?>
                                                        <span><?php echo $consult['NOME'] ?></span>
                                                        <?php
                                                    }

                                                }
                                                ?>
                                                <?php
                                                foreach ($dados['situacao'] as $consult) {
                                                    if ($dadosAdicionais[8][1]['FK_SITUACAO'] == $consult['ID']) {
                                                        ?>
                                                        <br>
                                                        <span><?php echo '<strong>Situação</strong><br>' . $consult['DESCRICAO'] ?></span>
                                                        <br>
                                                        <?php
                                                    }

                                                }
                                                ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="row">
                                        <div class="col-xs-4">
                                            <table class="table table-striped" style="font-size: 13px;">
                                                <tr>
                                                    <th>Plataformas</th>
                                                </tr>
                                                <?php
                                                foreach ($dados['plataforma'] as $dado) {
                                                    foreach ($dadosAdicionais[3] as $value)
                                                        if ($value[FK_PLATAFORMA] == $dado->getId())
                                                            echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                                }
                                                ?>
                                            </table>
                                        </div>
                                        <div class="col-xs-4">
                                            <table class="table table-striped" style="font-size: 13px;">
                                                <tr>
                                                    <th>Especialidades</th>
                                                </tr>
                                                <?php
                                                foreach ($dados['especialidade'] as $dado) {
                                                    foreach ($dadosAdicionais[5] as $value)
                                                        if ($value[FK_ESPECIALIDADE] == $dado->getId())
                                                            echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                                }
                                                ?>
                                            </table>
                                        </div>
                                        <div class="col-xs-4">
                                            <table class="table table-striped" style="font-size: 13px;">
                                                <?php
                                                if ($dadosAdicionais[4][1]) {
                                                    ?>
                                                    <tr>
                                                        <th>Convênios</th>
                                                    </tr>
                                                    <?php
                                                }
                                                foreach ($dados['convenio'] as $dado) {
                                                    foreach ($dadosAdicionais[4] as $value)
                                                        if ($value['FK_CONVENIO'] == $dado->getId())
                                                            echo '<tr><td>' . $dado->getNome() . '</td></tr>';
                                                }

                                                ?>
                                            </table>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>
                            <div class="col-xs-6">
                                <br>
                                <fieldset style=" padding: 5px;">
                                    <legend>Contatos</legend>
                                    <div style="height: 450px; overflow:auto;">
                                        <?php
                                        $i = 0;
                                        foreach ($contatos as $o) {
                                            ?>
                                            <div class="row listMedicos" style="cursor:pointer"
                                                 id="div<?php echo $o->getId(); ?>"
                                                 onclick="marcarMedico(this)">
                                                <div class="col-sm-7 text-left">
                                                    <?php
                                                    echo '<b>' . $o->getNome() . '</b>'; ?>

                                                    <?php
                                                    if ($o->getCrm() > 0) {
                                                        ?>
                                                        <span class="glyphicon glyphicon-list-alt"
                                                              onclick="FichaMedico('<?php echo $control->localizarIdCrm($o->getUf() . $o->getCrm())[1][0]; ?>', '<?php echo $o->getNome() ?>', '', 'TRM01|TRM00')"></span>
                                                        <?php
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($o->getCrm() > 0) {
                                                        echo '<br><b>CRM: </b>' . $o->getUf() . $o->getCrm();
                                                        if (is_object($dadosEditar['medicos'][$i][0])) {
                                                            if ($o->getResidente() == 1) {
                                                                echo " - Residente";
                                                            }
                                                        }
                                                        echo '<br>';
                                                    } else {
                                                        echo '<br>';
                                                    }
                                                    if ($o->getDecisor() == 1) {
                                                        echo "Decisor - ";
                                                    } else {
                                                        echo "Não Decisor - ";
                                                    }
                                                    echo $o->getContatoTipo() . '<br>';
                                                    ?>
                                                </div>
                                                <div class="col-sm-5 text-left">
                                                    <?php
                                                    echo $o->getEmail() . "<br>";
                                                    echo $o->getTelefone();
                                                    if ($o->getWhatsapp() == 1) { ?>
                                                        <img width="14"
                                                             src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></span>
                                                        <?php
                                                    }
                                                    echo ' <br>';
                                                    $sis = new appFunction();
                                                    if ($o->getDataNasc() != '1900-01-01')
                                                        echo substr($sis->fn_formatarData($o->getDataNasc()), 0, 10);
                                                    $i++; ?>
                                                </div>
                                            </div>
                                            <br>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </fieldset>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <h4 id="titulo">Agendar Ligação</h4>
                        <br>
                        <div class="form-group form-group-sm">
                            <label for="previsao" class="inp ">
                                <input type="date"
                                       class=""
                                       name="dataAgenda"
                                       id="dataAgenda"
                                       placeholder="&nbsp;">
                                <span class="label" for="previsao">Data</span>
                                <span class="border"></span>
                            </label>
                        </div>
                        <div class="form-group form-group-sm">
                            <label for="responsavel" class="inp">
                                <input type="hidden"
                                    <?php
                                    if (is_array($dadosEditar[1][1])) {
                                        ?>
                                        value="<?php echo $dadosEditar[1][1]['FK_RESPONSAVEL'] ?> "
                                        <?php
                                    }
                                    ?>
                                       id="responsavelId"/>
                                <input type="text"
                                       onkeyup="buscaNome()"
                                       name="responsavel"
                                       id="responsavel"
                                       autocomplete="off"
                                    <?php
                                    if (is_array($dadosEditar[1][1])) {
                                        foreach ($dados['colaborador'] as $colaborador) {
                                            if ($dadosEditar[1][1]['FK_RESPONSAVEL'] == $colaborador['ID_COLABORADOR']) {
                                                ?>
                                                value="<?php echo $control->tirarAcentos($colaborador['NOME']) ?> "
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                       placeholder="&nbsp;">
                                <span class="label" for="responsavel"
                                      style="font-size: 16px;">Responsável</span>
                                <span class="border"></span>
                            </label>
                            <div id="comboNomes"
                                 style="display:none;
                                    min-height: 0;
                                    max-height: 200px;
                                    overflow: hidden;
                                    z-index: 1000;
                                    box-shadow: 2px 5px  #d6d6d6;
                                    border:1px solid #e6e6e6;
                                    position:absolute;
                                    background-color: white;
                                    padding:10px;
                                    width:90%;">
                                <table id="lista">
                                    <?php
                                    foreach ($dados['colaborador'] as $colaborador) {
                                        echo '<tr style="cursor:pointer;display: none;" onclick="SelecionarNome(\'' . $control->tirarAcentos($colaborador['NOME']) . '\',\'' . $control->tirarAcentos($colaborador['ID_COLABORADOR']) . '\')" ><td>' . $control->tirarAcentos($colaborador['NOME']) . '</td></tr>';
                                    }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <input type="hidden" value="<?php echo $idEmpresa ?>" id="idEmpresa"/>
                        <div class="form-group form-group-sm">
                            <label for="gestores" style="color:#9098a9;">Observação</label>
                            <textarea class="form-control" id="observacao" rows="6"></textarea>
                        </div>
                        <div class="form-group form-group-sm">
                            <span class="btn btn-success form-control" onclick="salvarPma()">Salvar</span>
                            <br>
                            <br>
                            <span class="btn btn-danger form-control" onclick="location.reload() ">Cancelar</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
        <br>
        <?php

    }

    public function viewContatos()
    {
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $idColaborador = $_SESSION['id_colaborador'];
        $arrayUgn = $control->arrayUgn();
        $ugn = 2;
        if (isset($_SESSION['ugn'])) {
            $ugn = $_SESSION['ugn'];
        }
        if ($_SESSION['id_perfil'] == '7') {
            $listIdEmpresa = $control->listIdEmpresaByUgn($ugn);
        } else {
            $listIdEmpresa = $control->listIdEmpresaByColaborador($idColaborador);
        }
        $cnpjs = $control->listCnpjByIds($listIdEmpresa);
        $dados = $control->controlSwitch('carregaDados')
        ?>
        <div class="row" style="margin-top: 50px">
            <div class="col-xs-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                            <img src="/../../public/img/l.png" width="100%"/>
                        </div>
                        <div class="col-sm-12 text-right">
                            <ul class="nav nav-pills">
                                <li role="presentation"><a href="#" onclick="location.reload()">NEGÓCIOS</a></li>
                                <li role="presentation" class="disabled"><a href="#">CONTATOS</a></li>
                                </li>
                                <li role="presentation"><a href="#" onclick="Atividades()">ATIVIDADES</a></li>
                                <li role="presentation" class="disabled"><a href="#">TÁTICO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        if (!is_array($cnpjs)) { ?>
                            <div class="alert alert-danger text-center" role="alert">Nenhum Resgistro encontrado.</div>
                            <?php
                        } else {
                            ?>
                            <table class="table table-hover">
                                <thead>
                                <tr style="background-color: #f5faf6">
                                    <th>Empresa</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($cnpjs as $cnpj) {
                                    $dadosEditar = $control->loadDadosEmpresa($cnpj['ID']);
                                    ?>
                                    <tr data-toggle="collapse" style="cursor:pointer;"
                                        data-target="#<?php echo $cnpj['ID'] ?>">
                                        <td><strong><?php echo $cnpj['FANTASIA'] ?></strong><br>
                                            <small style="font-size:12px"><?php echo $cnpj['RAZAO_SOCIAL'] ?></small>
                                            <br>
                                            <sub><?php echo $control->formatarCnpj($cnpj['CNPJ']) ?></sub>
                                        </td>
                                    </tr>
                                    <tr id="<?php echo $cnpj['ID'] ?>" class="collapse">
                                        <td>
                                            <div class="row" style="font-size: 15px">
                                                <?php
                                                $i = 0;
                                                foreach ($dadosEditar['contato'] as $o) {
                                                    ?>
                                                    <div class="col-xs-4" style="cursor: pointer"
                                                         onclick="FichaMedico('<?php echo $control->localizarIdCrm($dadosEditar['crm'][$i]->getUf() . $dadosEditar['crm'][$i]->getNumero())[1][0] ?>', '<?php echo $o->getNome() ?>', '', 'TRM01|TRM00')">
                                                        <?php
                                                        echo '<br><b>' . $o->getNome() . '</b>';
                                                        if (is_object($dadosEditar['crm'][$i])) {
                                                            if ($dadosEditar['crm'][$i]->getId() > 0) {
                                                                echo '<br><b>CRM: </b>' . $dadosEditar['crm'][$i]->getUf() . $dadosEditar['crm'][$i]->getNumero();
                                                                if (is_object($dadosEditar['medicos'][$i][0])) {
                                                                    if ($dadosEditar['medicos'][$i][0]->getResidente() == 1) {
                                                                        echo " - Residente";
                                                                    }
                                                                }

                                                            }

                                                        }
                                                        echo '<br>';
                                                        if ($o->getDecisor() == 1) {
                                                            echo "Decisor - ";
                                                        } else {
                                                            echo "Não Decisor - ";
                                                        }
                                                        foreach ($dados['tipocontato'] as $tipos) {
                                                            if ($tipos->getId() == $o->getFkContatoTipo()) {
                                                                echo $tipos->getDescricao() . '<br>';
                                                            }
                                                        }
                                                        if (is_object($dadosEditar['emailcontato'][$i])) {
                                                            if ($dadosEditar['emailcontato'][$i]->getId() > 0)
                                                                if ($dadosEditar['emailcontato'][$i]->getEmail() != '')
                                                                    echo $dadosEditar['emailcontato'][$i]->getEmail() . "<br>";
                                                                else
                                                                    echo '<br>';

                                                        }
                                                        if (is_object($dadosEditar['telContato'][$i])) {
                                                            if ($dadosEditar['telContato'][$i]->getId() > 0)
                                                                if ($dadosEditar['telContato'][$i]->getTel() != '')
                                                                    echo $dadosEditar['telContato'][$i]->getTel();
                                                                else
                                                                    echo '<br>';

                                                        }
                                                        if (is_object($dadosEditar['telContato'][$i])) {
                                                            if ($dadosEditar['telContato'][$i]->getWhatsapp() == 1) { ?>
                                                                <img width="14"
                                                                     src="https://lowbpmingressos.com.br/wp-content/uploads/2015/12/WhatsApp-icone-3.png"/></span>
                                                                <br>
                                                                <?php
                                                            } else
                                                                echo '<br>';
                                                        }
                                                        $i++;
                                                        ?>
                                                    </div>
                                                    <?php

                                                }
                                                ?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        <?php } ?>
                    </div>
                    <div class="col-xs-12" style="display: none">
                        <nav>
                            <ul class="pager">
                                <li class="paggerPoint" onclick="primeiraPagina()"><a>Primeiro</a></li>
                                <li class="paggerPoint" onclick="recuarPagina()"><a>Anterior</a></li>
                                <li>
                                    <small>1/1</small>
                                </li>
                                <li class="paggerPoint" onclick="avancarPagina()"><a>Próximo</a></li>
                                <li class="paggerPoint" onclick="ultimaPagina()"><a>Último</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="modalFichaMedico" tabindex="-1"
             role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true" aria-backdrop="false">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header btn-primary">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha
                            do Médico</h5>
                    </div>
                    <div class="modal-body max-height">
                    </div>
                    <div class="modal-footer">
                        <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""
                           id="btn-exportar-ficha-medico" data-dismisss="modal">
                            <span class="glyphicon glyphicon-file"></span> Exportar em CSV
                        </a>

                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                            <span class="glyphicon glyphicon-remove"></span> Fechar
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function viewAtividades()
    {
        $control = new cPaEmpresa();
        $idColaborador = $_SESSION['id_colaborador'];
        $perfil = $_SESSION['id_perfil'];
        $arrayUgn = $control->arrayUgn();
        $ugn = $control->filtrarUgn($arrayUgn);
        $listaAtividades = $control->listarAtividades($idColaborador);
        switch ($perfil) {
            case '7':
                $cnpjs = $control->listIdEmpresaByUgn($ugn);
                break;
            case '8':
                $cnpjs = $control->listIdEmpresaByUgn($ugn);
                break;
            case '3':
                $listIdEmpresa = $control->listIdEmpresaByColaborador($idColaborador);
                $ugnColaborador = $control->listUgnByIdColaborador($idColaborador);
                $ugn = $control->filtrarUgn($ugnColaborador);
                if (isset($_POST['ugn'])) {
                    $cnpjs = $control->listIdEmpresaByUgn($ugn);
                } else {
                    $cnpjs = $control->listCnpjByIds($listIdEmpresa);
                    $ugn = '';
                }
                break;
        }
        ?>
        <div class="row" style="margin-top: 50px">
            <div class="col-xs-12">
                <div class="page-header">
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-3 col-lg-2">
                            <img src="/../../public/img/l.png" width="100%"/>
                        </div>
                        <div class="col-sm-12 text-right">
                            <ul class="nav nav-pills">
                                <li role="presentation"><a href="#" onclick="location.reload()">NEGÓCIOS</a></li>
                                <li role="presentation" class="disabled"><a href="#">CONTATOS</a></li>
                                <li role="presentation" class="active"><a href="#" onclick="Atividades()">ATIVIDADES</a>
                                </li>
                                <li role="presentation" class="disabled"><a href="#">TÁTICO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12">
                <?php
                if (!is_array($listaAtividades)) { ?>
                    <div class="alert alert-danger text-center" role="alert">Nenhum Resgistro encontrado.</div>
                    <?php
                } else {
                    ?>
                    <div class="tableFixHead">
                        <table class="table table-hover" id="lista">
                            <thead>
                            <tr style="background-color: #f5faf6">
                                <th style="padding: 0px 8px;">
                                </th>
                                <th style="padding: 0px 8px;">
                                    <label for="empresa" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridEmp()"
                                               name="empresa"
                                               id="empresa"
                                               placeholder="&nbsp;">
                                        <span class="label" for="empresa" style="color: #545b62">
                                        <span class="glyphicon glyphicon-search"></span>&nbsp;Empresa</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th style="padding: 0px 8px;">
                                    <label for="txtCnpj" class="inp">
                                        <input type="text"
                                               class="maskCnpj"
                                               name="txtCnpj"
                                               id="txtCnpj"
                                               onkeyup="buscaGridCnpj()"
                                               placeholder="&nbsp;">
                                        <span class="label" for="txtCnpj" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;CNPJ</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-left" style="padding: 0px 8px;">
                                    <label for="cidade" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridCid()"
                                               name="cidade"
                                               id="cidade"
                                               placeholder="&nbsp;">
                                        <span class="label" for="cidade" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;Cidade</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-center" style="padding: 0px 8px;">
                                    <label for="ugn" class="inp">
                                        <input type="text"
                                               onkeyup="buscaGridUgn()"
                                               name="ugn"
                                               id="ugn"
                                               placeholder="&nbsp;">
                                        <span class="label" for="ugn" style="color: #545b62">
                                            <span class="glyphicon glyphicon-search"></span>&nbsp;Data</span>
                                        <span class="border"></span>
                                    </label>
                                </th>
                                <th class="text-right" style="padding: 0px 8px 5px 0px; z-index:100000;">
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            foreach ($listaAtividades as $cnpj) {
                                $empresa = $control->loadDadosEmpresabyId($cnpj['FK_EMPRESA']);
                                ?>

                                <tr>
                                    <td class="text-center">
                                        <?php
                                        if ($cnpj['SITUACAO'] == 1) {
                                            ?>
                                            <span class="glyphicon glyphicon-unchecked"></span>
                                            <?php
                                        } else if ($cnpj['SITUACAO'] == 2) {
                                            ?>
                                            <span class="glyphicon glyphicon-check text-success"></span>
                                            <?php
                                        }
                                        ?>
                                    </td>
                                    <td><strong><?php echo $empresa['empresa'][1]['FANTASIA'] ?></strong><br>
                                        <small style="font-size:12px"><?php echo $empresa['empresa'][1]['RAZAO_SOCIAL'] ?></small>
                                    </td>
                                    <td><?php echo $control->formatarCnpj($empresa['empresa'][1]['CNPJ']) ?></td>
                                    <td class="text-left"><?php echo $empresa['empresa'][1]['CIDADE'] ?></td>
                                    <td class="text-center">
                                        <?php
                                        echo $cnpj['DATA_ATIVIDADE'];
                                        ?>
                                    </td>
                                    <td class="text-right">
                                        <div class="btn-group btn-group-sm" role="group">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                    data-toggle="dropdown"
                                                    aria-haspopup="true" aria-expanded="false">AÇÃO <span
                                                        class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu pull-right">
                                                <li onclick="Ficha(<?php echo $cnpj['ID'] ?>)">
                                                    <a href="#"><span class="glyphicon glyphicon-eye-open"></span>&nbsp;&nbsp;Visualizar</a>
                                                </li>
                                                <?php
                                                $editar = false;
                                                if (is_array($listIdEmpresa)) {
                                                    if (in_array($cnpj['ID'], $listIdEmpresa)) {
                                                        $editar = true;
                                                    }
                                                }
                                                if ($cnpj['SITUACAO'] == 1) {
                                                    if ($editar || $perfil == 3) {
                                                        ?>
                                                        <li><a href="#"
                                                               onclick="ExcluirAtividade(<?php echo $cnpj['ID'] ?>)"><span
                                                                        class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;Excluir</a>
                                                        </li>
                                                        <li><a href="#"
                                                               onclick="InserirAtividade(<?php echo $cnpj['ID'] ?>)"><span
                                                                        class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;Editar</a>
                                                        </li>
                                                        <li><a href="#"
                                                               onclick="ReportarAtividade(<?php echo $cnpj['ID'] ?>)"><span
                                                                        class="glyphicon glyphicon-comment"></span>&nbsp;&nbsp;Reportar</a>
                                                        </li>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>
                <?php } ?>
            </div>
        </div>
        <br>
        <br>
        <br>
        <?php
    }

//***************************Antigos************************************

    public function formEditar($objeto)
    {
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('d/m/Y');
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dados = $control->controlSwitch('carregaDados');
        $dadosEditar = $control->loadDadosEmpresa($objeto->getId());
        ?>
        <script>
            var isMedico = false;

            function salvarDados() {
                //dados empresa
                cnpj = $('#txtCnpj').val();
                razao = $('#txtrazaosocial').val();
                fantasia = $('#txtfantasia').val();
                cepEmpresa = $('#txtcep').val();
                cidadeEmpresa = $('#txtcidade').val();
                estadoEmpresa = $('#txtestado').val();
                bairroEmpresa = $('#txtbairro').val();
                logradouroEmpresa = $('#txtlogradouro').val();
                numeroEmpresa = $('#txtnumero').val();
                complementoEmpresa = $('#txtcomplemento').val();
                emailEmpresa = $('#txtemail').val();
                telefoneEmpresa = $('#txttelefone').val();
                botao = $("#whatsapp");
                //validações empresa
                if (cnpj == '') {
                    alert("O campo CNPJ é obrigatório");
                    return;
                }
                if (razao == '') {
                    alert("O campo Razão Social é obrigatório");
                    return;
                }
                if (fantasia == '') {
                    alert("O campo Nome Fantasia é Obrigatório");
                    return;
                }
                if (cepEmpresa == '') {
                    alert("O campo CEP é Obrigatório");
                    return;
                }
                if (cidadeEmpresa == '') {
                    alert("O campo Cidade é Obrigatório");
                    return;
                }
                if (estadoEmpresa == '') {
                    alert("O campo UF é Obrigatório");
                    return;
                }
                if (bairroEmpresa == '') {
                    alert("O campo Bairro é Obrigatório");
                    return;
                }
                if (logradouroEmpresa == '') {
                    alert("O campo Endereço é Obrigatório");
                    return;
                }
                if (numeroEmpresa == '') {
                    alert("O campo Número é Obrigatório");
                    return;
                }
                if (emailEmpresa == '') {
                    alert("O campo E-mail é Obrigatório");
                    return;
                }
                if (telefoneEmpresa == '') {
                    alert("O campo Telefone é Obrigatório");
                    return;
                }
                if (botao[0].innerText == "WhatsApp") {
                    whatsappEmpresa = "0"
                } else {
                    whatsappEmpresa = "1"
                }
                //Adicionais empresa
                numeromedicos = $('#numeroMedicos').val();
                numeropacientes = $('#numeroPacientes').val();
                pacientesdia = $('#pacientesDia').val();
                btnSimAtendePar = $('#btnSim');
                valorConsulta = $('#valorConsulta').val();
                porcAtendimento = $('#porcAtendimento').val();
                relacionamento = $('.relacionamento');
                gestores = document.getElementById("gestores");
                btnSimCon = $('#btnSimCon');
                plataforma = $('.plataforma');
                especialidade = $('.especialidade');
                convenio = $('.convenio');
                //validações adicionais
                matriz = false;
                responsavel = 0;
                if (numeromedicos == '') {
                    alert("O campo Quantidade de Médicos é obrigatório");
                    return;
                }
                // if (numeropacientes == '') {
                //     alert("O campo Pacientes por Mês é obrigatório");
                //     return;
                // }
                // if (pacientesdia == '') {
                //     alert("O campo Consultas Por Dia é obrigatório");
                //     return;
                // }
                if (btnSimAtendePar[0].classList.contains('btn-success')) {
                    btnSimAtendePar = 1;
                    // if (valorConsulta == '') {
                    //     alert("O campo Valor Consulta é obrigatório");
                    //     return;
                    // }
                    // if (porcAtendimento == '') {
                    //     alert("O campo % do Atendimento é obrigatório");
                    //     return;
                    // }
                } else {
                    btnSimAtendePar = 0;
                }
                arrayConvenio = [];
                if (btnSimCon[0].classList.contains('btn-success')) {
                    btnSimCon = 1;
                    for (k = 0; k < convenio.length; k++) {
                        if ($("#" + convenio[k].id).prop("checked")) {
                            arrayConvenio.push(convenio[k].value);
                        }
                    }
                    // if (arrayConvenio.length == 0) {
                    //     alert("O campo Convênio é Obrigatório");
                    //     return;
                    // }
                } else {
                    btnSimCon = 0;
                }
                relacionamentoID = 0;
                for (j = 0; j < relacionamento.length; j++) {
                    if (relacionamento[j].classList.contains('btn-success')) {
                        matriz = false;
                        relacionamentoID = relacionamento[j].id;
                    }
                }
                if (relacionamentoID == 0) {
                    alert("O campo Relacionamento é Obrigatório");
                    return;
                }
                if (gestores.options[gestores.selectedIndex].value != '') {
                    responsavel = gestores.options[gestores.selectedIndex].value;
                }
                if (responsavel == 0) {
                    alert("Selecione um responsável pelo CNPJ");
                    return;
                }
                arrayPlataforma = [];
                arrayEspecialidade = [];
                for (k = 0; k < especialidade.length; k++) {
                    if ($("#" + especialidade[k].id).prop("checked")) {
                        arrayEspecialidade.push(especialidade[k].value);
                    }
                }
                for (k = 0; k < plataforma.length; k++) {
                    if ($("#" + plataforma[k].id).prop("checked")) {
                        arrayPlataforma.push(plataforma[k].value);
                    }
                }
                if (arrayPlataforma.length == 0) {
                    alert("O campo Plataforma é Obrigatório");
                    return;
                }
                if (arrayEspecialidade.length == 0) {
                    alert("O campo Especialidade é Obrigatório");
                    return;
                }
                //dados de contato
                var table = document.getElementById("tbcontatos");
                listaCh = table.children[0].children;
                listaTd = [];
                for (k = 1; k < listaCh.length; k++) {
                    if (listaCh[k]) {
                        listaTd.push(listaCh[k]);
                    }
                }
                if (listaTd.length < 1) {
                    alert("Adicione ao menos um contato a sua lista.");
                    return;
                }
                arraynome = [];
                arraydecisor = [];
                arraytipo = [];
                arraymedico = [];
                arrayresidente = [];
                arraycrm = [];
                arrayemail = [];
                arraytelefone = [];
                arraywhatsapp = [];
                arraydtnascimento = [];
                arraydtnascimento = [];
                for (k = 0; k < listaTd.length; k++) {
                    str = listaTd[k].children[0].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraynome.push(str);
                    str = listaTd[k].children[1].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraydecisor.push(str);
                    str = listaTd[k].children[2].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraytipo.push(str);
                    str = listaTd[k].children[3].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arraymedico.push(str);
                    str = listaTd[k].children[4].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arrayresidente.push(str);
                    str = listaTd[k].children[5].textContent;
                    str = str.trim();
                    str = str.replace("-", ' ');
                    arraycrm.push(str);
                    str = listaTd[k].children[6].textContent;
                    str = str.trim();
                    str = str.replace("-", ' ');
                    arrayemail.push(str);
                    str = listaTd[k].children[7].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraytelefone.push(str);
                    str = listaTd[k].children[8].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arraywhatsapp.push(str);
                    str = listaTd[k].children[9].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.trim();
                    if (str != "" && str != "-") {
                        str = str.split("/");
                        str = str[2] + "-" + str[1] + "-" + str[0];
                    } else {
                        str = '';
                    }
                    arraydtnascimento.push(str);
                }
                var formdata = new FormData();
                formdata.append('cnpj', cnpj)
                formdata.append('razao', razao.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase())
                formdata.append('fantasia', fantasia.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase())
                formdata.append('cepEmpresa', cepEmpresa)
                formdata.append('bairroEmpresa', bairroEmpresa.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase())
                formdata.append('cidadeEmpresa', cidadeEmpresa.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase())
                formdata.append('estadoEmpresa', estadoEmpresa)
                formdata.append('logradouroEmpresa', logradouroEmpresa.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase())
                formdata.append('numeroEmpresa', numeroEmpresa)
                formdata.append('complementoEmpresa', complementoEmpresa)
                formdata.append('emailEmpresa', emailEmpresa)
                formdata.append('telefoneEmpresa', telefoneEmpresa)
                formdata.append('whatsappEmpresa', whatsappEmpresa)
                formdata.append('numeromedicos', numeromedicos)
                formdata.append('numeropacientes', numeropacientes)
                formdata.append('pacientesdia', pacientesdia)
                formdata.append('valorConsulta', valorConsulta)
                formdata.append('porcAtendimento', porcAtendimento)
                formdata.append('btnSimAtendePar', btnSimAtendePar)
                formdata.append('btnSimCon', btnSimCon)
                formdata.append('arrayConvenio', arrayConvenio)
                formdata.append('relacionamentoID', relacionamentoID)
                formdata.append('responsavel', responsavel)
                formdata.append('arrayPlataforma', arrayPlataforma)
                formdata.append('arrayEspecialidade', arrayEspecialidade)
                formdata.append('arraynome', arraynome)
                formdata.append('arraydecisor', arraydecisor)
                formdata.append('arraytipo', arraytipo)
                formdata.append('arraymedico', arraymedico)
                formdata.append('arrayresidente', arrayresidente)
                formdata.append('arraycrm', arraycrm)
                formdata.append('arrayemail', arrayemail)
                formdata.append('arraytelefone', arraytelefone)
                formdata.append('arraywhatsapp', arraywhatsapp)
                formdata.append('arraydtnascimento', arraydtnascimento)
                formdata.append('editado', 1)

                $.ajax({
                    type: 'POST',
                    url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/save/',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (retorno) {
                        if (retorno == "") {
                            location.reload();
                            return;
                        } else
                            alert(retorno);

                    }
                });
            }

            $(document).ready(function () {
                $("#footerEditar").html('<button type="button" onclick="salvarDados()" class="btn btn-success" >Salvar</button><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalMsgBoxConfirmEditar">Cancelar</button>');
                $(".isMedico").hide();
                $("#primeira").click(function () {
                    $("#primeiraParte").show();
                    $("#segundaParte").hide();
                    $("#terceiraParte").hide();
                    $("#primeira").addClass('active');
                    $("#segunda").removeClass('active');
                    $("#terceira").removeClass('active');
                });
                $("#segunda").click(function () {
                    $("#segundaParte").show();
                    $("#primeiraParte").hide();
                    $("#terceiraParte").hide();
                    $("#segunda").addClass('active');
                    $("#primeira").removeClass('active');
                    $("#terceira").removeClass('active');
                });
                $("#terceira").click(function () {
                    $("#terceiraParte").show();
                    $("#primeiraParte").hide();
                    $("#segundaParte").hide();
                    $("#terceira").addClass('active');
                    $("#primeira").removeClass('active');
                    $("#segunda").removeClass('active');
                });
            });
            $("#whatsapp").click(function () {
                botao = $("#whatsapp");
                check = '&check; WhatsApp';
                if (botao[0].innerText == "WhatsApp") {
                    botao.addClass('btn-success');
                    botao.html(check);
                } else {
                    botao.removeClass('btn-success');
                    botao.text("WhatsApp");
                }
            });
            $('.relacionamento').click(function () {
                $('.relacionamento').removeClass('btn-success');
                botao = this.id;
                $('#' + botao).addClass('btn-success');
            });
            $('.consulta').click(function () {
                $('.consulta').removeClass('btn-success');
                $('.consulta').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSim") {
                    $(".consultaDis").prop('disabled', false);
                    $('#' + botao).addClass('btn-success');
                } else {
                    $(".consultaDis").prop('disabled', true);
                    $('#' + botao).addClass('btn-danger');
                }

            });
            $('.consultaCon').click(function () {
                $('.consultaCon').removeClass('btn-success');
                $('.consultaCon').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimCon") {
                    $(".checkboxPlano").show();
                    $('#' + botao).addClass('btn-success');
                } else {
                    $(".checkboxPlano").hide();
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.decisor').click(function () {
                $('.decisor').removeClass('btn-success');
                $('.decisor').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimDec") {
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.medico').click(function () {
                $('.medico').removeClass('btn-success');
                $('.medico').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimMed") {
                    $('.isMedico').show();
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('.isMedico').hide();
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.residente').click(function () {
                $('.residente').removeClass('btn-success');
                $('.residente').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimResi") {
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $("#whatsappContato").click(function () {
                botao = $("#whatsappContato");
                check = '&check; WhatsApp';
                if (botao[0].innerText == "WhatsApp") {
                    botao.addClass('btn-success');
                    botao.html(check);
                } else {
                    botao.removeClass('btn-success');
                    botao.text("WhatsApp");
                }
            });


            $('#addContato').click(function () {
                var table = document.getElementById("tbcontatos");
                var tipo = document.getElementById("cmbtipocontato");
                var decisor = document.getElementById("btnSimDec");
                var medico = document.getElementById("btnSimMed");
                var residente = document.getElementById("btnSimResi");
                var ufCrm = document.getElementById("txtUf");
                var crm = document.getElementById("txtCrm");
                var nome = document.getElementById("txtnomecontato");
                var nascimento = document.getElementById("txtdataContato");
                var email = document.getElementById("txtemailcontato");
                var telefone = document.getElementById("txttelefonecontato");
                var whatsapp = document.getElementById("whatsappContato");
                if (nome.value == '') {
                    alert('Informe o nome do contato');
                    return;
                }
                // if (nascimento.value == '') {
                //     alert('Informe a data de nascimento');
                //     return;
                // }
                // if (email.value == '') {
                //     alert('Informe o email');
                //     return;
                // }
                // if (telefone.value == '') {
                //     alert('Informe o telefone');
                //     return;
                // }
                if (tipo.options[tipo.selectedIndex].value == '') {
                    alert('Informe o tipo do contato');
                    return;
                }
                if (decisor.classList.contains('btn-success'))
                    decisor = "Sim"
                else
                    decisor = "Não"
                if (medico.classList.contains('btn-success')) {
                    medico = "Sim"
                    if (!isMedico) {
                        alert("Verificação de CRM obrigatória para inclusão de médicos");
                        return;
                    }
                    if (ufCrm.value == '' || crm.value == '') {
                        alert("informe o CRM e UF")
                        return;
                    }
                } else
                    medico = "Não"
                if (residente.classList.contains('btn-success'))
                    residente = "Sim"
                else
                    residente = "Não"
                if (whatsapp.classList.contains('btn-success'))
                    whatsapp = "Sim"
                else
                    whatsapp = "Não"

                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);
                var cell7 = row.insertCell(6);
                var cell8 = row.insertCell(7);
                var cell9 = row.insertCell(8);
                var cell10 = row.insertCell(9);
                var cell11 = row.insertCell(10);
                var cell12 = row.insertCell(11);
                cell3.innerHTML = tipo.options[tipo.selectedIndex].text;
                cell2.innerHTML = decisor;
                cell4.innerHTML = medico;
                cell5.innerHTML = residente;
                cell6.innerHTML = ufCrm.value.toUpperCase() + crm.value;
                cell1.innerHTML = nome.value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();
                data = nascimento.value.trim();
                if (data != "") {
                    data = data.split("-");
                    data = data[2] + "/" + data[1] + "/" + data[0];
                } else {
                    data = "-";
                }
                cell10.innerHTML = data;
                cell7.innerHTML = email.value;
                cell8.innerHTML = telefone.value;
                cell9.innerHTML = whatsapp;
                cell11.innerHTML = '<span class="btn btn-xs btn-warning" onclick="editarlinha(this)"><span class="glyphicon glyphicon-pencil"></span></span><span class="btn btn-xs btn-danger" onclick="excluirlinha(this)"><span class="glyphicon glyphicon-remove"></span></span>';
                cell11.classList.add("text-right");
                $('.decisor').removeClass('btn-success');
                $('.decisor').removeClass('btn-danger');
                $('#btnNaoDec').addClass('btn-danger');
                $('.medico').removeClass('btn-success');
                $('.medico').removeClass('btn-danger');
                $('#btnNaoMed').addClass('btn-danger');
                $('.residente').removeClass('btn-danger');
                $('.residente').removeClass('btn-success');
                $('#btnNaoResi').addClass('btn-danger');
                botao = $("#whatsappContato");
                botao.removeClass('btn-success');
                botao.text("WhatsApp");
                $('.isMedico').hide();
                tipo.value = '';
                ufCrm.value = "";
                crm.value = "";
                nome.value = "";
                nascimento.value = "";
                email.value = "";
                telefone.value = "";
                isMedico = false;
            });

            function excluirlinha(elemento) {
                linha = elemento.parentElement.parentElement;
                tabela = elemento.parentElement.parentElement.parentElement;
                tabela.removeChild(linha);
            }

            function editarlinha(elemento) {
                linha = elemento.parentElement.parentElement;
                tabela = elemento.parentElement.parentElement.parentElement;
                tabela.removeChild(linha);
                var tipo = document.getElementById("cmbtipocontato");
                valorTipo = linha.children[2].textContent.trim();
                if (valorTipo != "") {
                    tipo = tipo.children;
                    for (j = 0; j < tipo.length; j++)
                        if (tipo[j].text == valorTipo) {
                            document.getElementById("cmbtipocontato").value = tipo[j].value;
                        }
                }
                var decisor = document.getElementById("btnSimDec");
                deciso = linha.children[1].textContent.trim();
                if (deciso == "Sim") {
                    $('.decisor').removeClass('btn-danger');
                    $('.decisor').removeClass('btn-success');
                    $('#btnSimDec').addClass('btn-success');
                } else {
                    $('.decisor').removeClass('btn-danger');
                    $('.decisor').removeClass('btn-success');
                    $('#btnNaoDec').addClass('btn-danger');
                }
                var medico = document.getElementById("btnSimMed");
                reside = linha.children[3].textContent.trim();
                if (reside == "Sim") {
                    $('.medico').removeClass('btn-danger');
                    $('.medico').removeClass('btn-success');
                    $('#btnSimMed').addClass('btn-success');
                    $('.isMedico').show();
                } else {
                    $('.medico').removeClass('btn-danger');
                    $('.medico').removeClass('btn-success');
                    $('#btnNaoMed').addClass('btn-danger');
                    $('.isMedico').hide();
                }
                var residente = document.getElementById("btnSimResi");
                reside = linha.children[4].textContent.trim();
                if (reside == "Sim") {
                    $('.residente').removeClass('btn-danger');
                    $('.residente').removeClass('btn-success');
                    $('#btnSimResi').addClass('btn-success');
                } else {
                    $('.residente').removeClass('btn-danger');
                    $('.residente').removeClass('btn-success');
                    $('#btnNaoResi').addClass('btn-danger');
                }
                var ufCrm = document.getElementById("txtUf");
                crmdados = linha.children[5].textContent.trim();
                if (crmdados.length == 9) {
                    ufCrm.value = crmdados[0] + crmdados[1];
                    var crm = document.getElementById("txtCrm");
                    crm.value = crmdados[2] + crmdados[3] + crmdados[4] + crmdados[5] + crmdados[6] + crmdados[7] + crmdados[8];
                }
                var nome = document.getElementById("txtnomecontato");
                nome.value = linha.children[0].textContent;
                var nascimento = document.getElementById("txtdataContato");
                data = linha.children[9].textContent.trim();
                data = data.split("/");
                nascimento.value = data[2] + "-" + data[1] + "-" + data[0];
                var email = document.getElementById("txtemailcontato");
                email.value = linha.children[6].textContent.trim();
                var telefone = document.getElementById("txttelefonecontato");
                telefone.value = linha.children[7].textContent.trim();
                var whatsapp = document.getElementById("whatsappContato");
                wt = linha.children[8].textContent.trim();
                if (wt == "Sim") {
                    botao = $("#whatsappContato");
                    botao.addClass('btn-success');
                    botao.text("✓ WhatsApp");
                }
            }
        </script>
        <ul class="nav nav-tabs">
            <li id="primeira" class="active"><a>Cadastro CNPJ</a></li>
            <li id="segunda"><a>Dados Adicionais</a></li>
            <li id="terceira"><a>Contatos</a></li>
        </ul>
        <br>
        <form id="formEditar">
            <div class="row" id="primeiraParte">
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkUser">Responsável</label>
                        <input
                                value="<?php echo $_SESSION['nome']; ?>"
                                type="text"
                                class="form-control "
                                name="fkuser"
                                disabled
                                id="fkUser">
                    </div>
                </div>

                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">Data Cadastro</label>
                        <input
                                disabled
                                type="text"
                                value="<?php echo $date ?>"
                                class="form-control "
                                name="datain"
                                id="dataIn">
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cnpj">CNPJ</label>
                        <input
                                value="<?php echo $objeto->getCnpj() ?>"
                                type="text"
                                class="form-control maskCnpj"
                                name="txtCnpj"
                                id="txtCnpj">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="buscarDados" style="color: white">.</label>
                        <span class="btn form-control btn-info"
                              id="buscarDados">
                        <span class="glyphicon glyphicon-search">
                        </span>
                            Verificar
                        </span>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="razaoSocial">Razão Social</label>
                        <input
                                value="<?php echo $objeto->getRazaoSocial() ?>"
                                type="text"
                                class="form-control "
                                name="txtrazaosocial"
                                id="txtrazaosocial">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fantasia">Nome Fantasia</label>
                        <input
                                value="<?php echo $objeto->getFantasia() ?>"
                                type="text"
                                class="form-control "
                                name="txtfantasia"
                                id="txtfantasia">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cep">CEP</label>
                        <input
                                value="<?php echo $objeto->getCep() ?>"
                                type="text"
                                class="form-control maskCEP"
                                name="txtcep"
                                id="txtcep">
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cidade">Cidade</label>
                        <input
                                value="<?php echo $objeto->getCidade() ?>"
                                type="text"
                                class="form-control "
                                name="txtcidade"
                                id="txtcidade">
                    </div>
                </div>
                <div class="col-sm-1 text-left">
                    <div class="form-group form-group-sm">
                        <label for="estado">UF</label>
                        <input
                                value="<?php echo $objeto->getEstado() ?>"
                                type="text"
                                class="form-control "
                                name="txtestado"
                                maxlength="2"
                                size="2"
                                id="txtestado">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="bairro">Bairro</label>
                        <input
                                value="<?php echo $objeto->getBairro() ?>"
                                type="text"
                                class="form-control "
                                name="txtbairro"
                                id="txtbairro">
                    </div>
                </div>
                <div class="col-sm-10 text-left">
                    <div class="form-group form-group-sm">
                        <label for="logradouro">Endereço</label>
                        <input
                                value="<?php echo $objeto->getLogradouro() ?>"
                                type="text"
                                class="form-control "
                                name="txtlogradouro"
                                id="txtlogradouro">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="numero">Número</label>
                        <input
                                value="<?php echo $objeto->getNumero() ?>"
                                type="text"
                                class="form-control "
                                name="txtnumero"
                                id="txtnumero">
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">Complemento</label>
                        <input
                                value="<?php echo $objeto->getComplemento() ?>"
                                type="text"
                                class="form-control "
                                name="txtcomplemento"
                                id="txtcomplemento">
                    </div>
                </div>

                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">E-mail</label>
                        <input
                                value="<?php echo $dadosEditar['emailempresa'] ?>"
                                type="email"
                                class="form-control "
                                name="txtemail"
                                id="txtemail">
                    </div>
                </div>
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">Telefone</label>
                        <input
                                value="<?php echo $dadosEditar['tel'] ?>"
                                type="text"
                                class="form-control maskTel"
                                name="txttelefone"
                                id="txttelefone">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento" style="color:white">.df</label>
                        <button

                                name="whatsapp"
                                class="form-control btn btn-default
                                <?php
                                if ($dadosEditar['whats'][0] == 1) {
                                    echo 'btn-success';
                                }
                                ?>
                                "
                                id="whatsapp"
                                onclick="return false"
                        ><?php
                            if ($dadosEditar['whats'][0] == 1) {
                                echo '&check;';
                            } ?>WhatsApp
                        </button>

                    </div>
                </div>
            </div>
            <div class="row" id="segundaParte" style="display: none">
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-7 text-left">
                                <div class="row">
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="numeroMedicos">Qtd. Médicos</label>
                                            <input
                                                    value="<?php echo $objeto->getNumeroMedicos() ?>"
                                                    type="number"
                                                    class="form-control "
                                                    name="numeromedicos"
                                                    id="numeroMedicos">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="numeroPacientes">Pacientes P/ Mês</label>
                                            <input
                                                    value="<?php echo $objeto->getNumeroPacientes() ?>"
                                                    type="number"
                                                    class="form-control "
                                                    name="numeropacientes"
                                                    id="numeroPacientes">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="pacientesDia">Consultas p/ Dia</label>
                                            <input
                                                    value="<?php echo $objeto->getPacientesDia() ?>"
                                                    type="number"
                                                    class="form-control "
                                                    name="pacientesdia"
                                                    id="pacientesDia">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="relacionamento">Atende Particular?</label><br>
                                            <?php
                                            if ($dadosEditar['consultas']->getId() > 0) {
                                                ?>
                                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                        id="btnSim" class="consulta btn btn-success"
                                                        onclick="return false">SIM
                                                </button>
                                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                        id="btnNao"
                                                        class="consulta btn "
                                                        onclick="return false">NÃO
                                                </button>
                                                <?php
                                            } else {
                                                ?>
                                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                        id="btnSim" class="consulta btn"
                                                        onclick="return false">SIM
                                                </button>
                                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                        id="btnNao"
                                                        class="consulta btn btn-danger"
                                                        onclick="return false">NÃO
                                                </button>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="valorConsulta">Valor Consulta</label>
                                            <input
                                                    value="<?php echo $dadosEditar['consultas']->getValor(); ?>"
                                                    type="text"
                                                    class="form-control consultaDis maskMoney"
                                                    name="valorConsulta"
                                                    id="valorConsulta">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="porcAtendimento">% do Atendimento</label>
                                            <input
                                                    value="<?php echo $dadosEditar['consultas']->getPercentual(); ?>"
                                                    type="text"
                                                    class="form-control consultaDis "
                                                    name="porcAtendimento"
                                                    id="porcAtendimento">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 text-left" style="padding: 0">
                                <div class="form-group form-group-sm">
                                    <label for="relacionamento">Relacionamento</label><br>
                                    <button style="margin-bottom: 3px" id="A1" class="r1 relacionamento btn"
                                            onclick="return false">A1
                                    </button>
                                    <button style="margin-bottom: 3px" id="B1" class="r2 relacionamento btn"
                                            onclick="return false">B1
                                    </button>
                                    <button style="margin-bottom: 3px" id="C1" class="r3 relacionamento btn"
                                            onclick="return false">C1
                                    </button>
                                    <br>
                                    <button style="margin-bottom: 3px" id="A2" class="r4 relacionamento btn"
                                            onclick="return false">A2
                                    </button>
                                    <button style="margin-bottom: 3px" id="B2" class="r5 relacionamento btn"
                                            onclick="return false">B2
                                    </button>
                                    <button style="margin-bottom: 3px" id="C2" class="r6 relacionamento btn"
                                            onclick="return false">C2
                                    </button>
                                    <br>
                                    <button style="margin-bottom: 3px" id="A3" class="r7 relacionamento btn"
                                            onclick="return false">A3
                                    </button>
                                    <button style="margin-bottom: 3px" id="B3" class="r8 relacionamento btn"
                                            onclick="return false">B3
                                    </button>
                                    <button style="margin-bottom: 3px" id="C3" class="r9 relacionamento btn"
                                            onclick="return false">C3
                                    </button>
                                    <script>
                                        $(document).ready(function () {
                                            $('.r<?php echo $dadosEditar['regua']->getFkRegua() ?>').addClass('btn-success');
                                        })
                                    </script>
                                </div>
                            </div>
                            <div class="col-sm-3 text-left">
                                <div class="row">
                                    <div class="col-sm-12 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="gestores">Atribuir Responsável</label>
                                            <select id="gestores" class="form-control" name="gestores">
                                                <option value="">Selecione...</option>
                                                <?php
                                                foreach ($dados['colaborador'] as $colaborador) {
                                                    ?>
                                                    <option
                                                            value="<?php echo $colaborador['ID_COLABORADOR'] ?>"
                                                        <?php
                                                        if ($dadosEditar['resp']->getFkResponsavel() == $colaborador['ID_COLABORADOR']) {
                                                            echo 'selected="yes"';
                                                        }
                                                        ?>
                                                    >
                                                        <?php echo $colaborador['SETOR'] . " - " . $colaborador['NOME'] ?></option>
                                                    <?php

                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-sm-12 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="relacionamento">Atende Convênio?</label><br>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnSimCon"
                                                    class="consultaCon btn
                                                    <?php
                                                    if (is_array($dadosEditar['convenio'])) {
                                                        echo 'btn-success';
                                                    }
                                                    ?>"
                                                    onclick="return false">SIM
                                            </button>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnNaoCon"
                                                    class="consultaCon btn
                                                    <?php
                                                    if (!is_array($dadosEditar['convenio'])) {
                                                        echo 'btn-danger';
                                                    }
                                                    ?>"
                                                    onclick="return false">NÃO
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-5 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="" style="display: ;"
                                           for="relacionamento">Plataforma</label>
                                    <div class=""
                                         style="display: ;border: 1px solid #d6d6d6; height: 300px; padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['plataforma'] as $plataforma) {
                                            ?>
                                            <input type="checkbox"
                                                   id="plataforma_<?php echo $plataforma->getId() ?>"
                                                <?php
                                                if (is_array($dadosEditar['plataforma'])) {
                                                    if (in_array($plataforma->getId(), $dadosEditar['plataforma'])) {
                                                        echo 'checked';
                                                    }
                                                }
                                                ?>
                                                   class="plataforma" name="plataforma[]"
                                                   value="<?php echo $plataforma->getId() ?>">
                                            <label> <?php echo $plataforma->getNome() ?></label><br><?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="" style="display: ;"
                                           for="relacionamento">Especialidade</label>
                                    <div class=""
                                         style="border: 1px solid #d6d6d6;
                                         height: 300px;
                                         padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['especialidade'] as $especialidade) {
                                            ?>
                                            <input type="checkbox" class="especialidade"
                                                <?php
                                                if (is_array($dadosEditar['especialidade'])) {
                                                    if (in_array($especialidade->getId(), $dadosEditar['especialidade'])) {
                                                        echo 'checked';
                                                    }
                                                }
                                                ?>
                                                   id="especialidade_<?php echo $especialidade->getId() ?>"
                                                   name="especialidade[]"
                                                   value="<?php echo $especialidade->getId() ?>">
                                            <label><?php echo $especialidade->getNome() ?></label><br><?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="checkboxPlano"
                                        <?php
                                        if (!is_array($dadosEditar['convenio'])) {
                                            echo 'style="display: none"';
                                        }
                                        ?>
                                           for="relacionamento">Convênios</label>
                                    <div class="checkboxPlano"
                                         style="
                                         <?php
                                         if (!is_array($dadosEditar['convenio'])) {
                                             echo 'display: none;';
                                         }
                                         ?>
                                                 border: 1px solid #d6d6d6; height: 100px; padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['convenio'] as $convenio) {
                                            ?>
                                            <input type="checkbox" id="convenio_<?php echo $convenio->getId() ?>"
                                                <?php
                                                if (is_array($dadosEditar['convenio'])) {
                                                    if (in_array($convenio->getId(), $dadosEditar['convenio'])) {
                                                        echo 'checked';
                                                    }
                                                }
                                                ?>
                                                   class="convenio" name="convenio[]"
                                                   value="<?php echo $convenio->getId() ?>">
                                            <label><?php echo $convenio->getNome() ?></label><br><?php
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="terceiraParte">
                <div class="col-sm-12 text-left">
                    <div class="row">
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="cmbtipocontato">Tipo Contato</label>
                                <select id="cmbtipocontato" class="form-control" name="cmbtipocontato">
                                    <option value="">Selecione...</option>
                                    <?php
                                    foreach ($dados['tipocontato'] as $contato) {
                                        ?>
                                        <option
                                        value="<?php echo $contato->getId() ?>"><?php echo $contato->getDescricao() ?></option><?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Decisor</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimDec"
                                        class="decisor btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoDec"
                                        class="decisor btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Médico</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimMed"
                                        class="medico btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoMed"
                                        class="medico btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Residente</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimResi"
                                        class="residente btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoResi"
                                        class="residente btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="txtUF">UF</label>
                                <input id="txtUf"
                                       name="txtUf"
                                       type="text"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="txtCrm">CRM</label>
                                <input name="txtCrm"
                                       id="txtCrm"
                                       type="number"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button name="buscarMedico"
                                        class="form-control btn btn-success"
                                        id="buscarMedico"
                                        onclick="return false">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <script>
                            $("#buscarMedico").click(function () {
                                var crm = $("#txtCrm").val();
                                var uf = $("#txtUf").val();
                                var adicionar = 7 - crm.length;
                                for (var i = 0; i < adicionar; i++) crm = '0' + crm;
                                crm = crm.slice(0, 7);
                                retorno = requisicaoAjax('https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/buscarCrm/' + uf + crm);
                                $("#txtCrm").val(crm);
                                if (retorno === '') {
                                    alert('Médico Ainda Não Cadastrado!\nEnvie os dados ao time de efetividade para inclusão.\nefetividade@ems.com.br');
                                    isMedico = false;
                                } else {
                                    alert('Médico encontrado.')
                                    $("#txtnomecontato").val(retorno);
                                    isMedico = true;
                                }
                            });
                        </script>
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <div class="form-group form-group-sm">
                                <label for="fkUser">Nome</label>
                                <input type="text"
                                       name="txtnomecontato"
                                       id="txtnomecontato"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-3 text-left">
                            <div class="form-group form-group-sm">
                                <label for="fkUser">Dt. Nascimento</label>
                                <input type="date"
                                       id="txtdataContato"
                                       name="txtdataContato"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-5 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento">E-mail</label>
                                <input
                                        type="email"
                                        class="form-control"
                                        name="txtemailcontato"
                                        id="txtemailcontato">
                            </div>
                        </div>
                        <div class="col-sm-4 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento">Telefone</label>
                                <input
                                        type="text"
                                        class="form-control maskTel"
                                        name="txttelefonecontato"
                                        id="txttelefonecontato">
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button
                                        name="whatsappContato"
                                        class="form-control btn btn-default"
                                        id="whatsappContato"
                                        onclick="return false"
                                >WhatsApp
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button
                                        name="addContato"
                                        class="form-control btn btn-success"
                                        id="addContato"
                                        onclick="return false"
                                ><b>&plus;</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                </script>
                <div class="col-sm-12 text-left">
                    <div style="overflow: auto">
                        <table class="table table-striped table-responsive" style="font-size: 13px;"
                               id="tbcontatos">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Decisor</th>
                                <th>Tipo</th>
                                <th>Médico</th>
                                <th>Residente</th>
                                <th>CRM</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th>WhatsApp</th>
                                <th>Nascimento</th>
                                <th class="text-right">Ações</th>
                            </tr>
                            <?php
                            $i = 0;
                            foreach ($dadosEditar['contato'] as $o) {
                                ?>
                                <tr>
                                    <td><?php echo $o->getNome() ?></td>
                                    <td>
                                        <?php if ($o->getDecisor() == 1) {
                                            echo "Sim";
                                        } else {
                                            echo "Não";
                                        }
                                        ?>
                                    </td>

                                    <td><?php
                                        foreach ($dados['tipocontato'] as $tipos) {
                                            if ($tipos->getId() == $o->getFkContatoTipo()) {
                                                echo $tipos->getDescricao();
                                            }
                                        }
                                        ?></td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['crm'][$i]))
                                            echo "Sim";
                                        else
                                            echo "Não";

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['medicos'][$i][0])) {
                                            if ($dadosEditar['medicos'][$i][0]->getResidente() == 1) {
                                                echo "Sim";
                                            } else {
                                                echo "Não";
                                            }
                                        } else {
                                            echo "-";
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['crm'][$i])) {
                                            if ($dadosEditar['crm'][$i]->getId() > 0)
                                                echo $dadosEditar['crm'][$i]->getUf() . $dadosEditar['crm'][$i]->getNumero();
                                        } else {
                                            echo '-';
                                        }

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['emailcontato'][$i])) {
                                            if ($dadosEditar['emailcontato'][$i]->getId() > 0)
                                                echo $dadosEditar['emailcontato'][$i]->getEmail();
                                        } else
                                            echo '-';

                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['telContato'][$i])) {
                                            if ($dadosEditar['telContato'][$i]->getId() > 0)
                                                echo $dadosEditar['telContato'][$i]->getTel();
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (is_object($dadosEditar['telContato'][$i])) {
                                            if ($dadosEditar['telContato'][$i]->getWhatsapp() == 1) {
                                                echo "Sim";
                                            } else {
                                                echo "Não";
                                            }
                                        } else {
                                            echo "-";
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($o->getDataNasc() == '1900-01-01') {
                                            echo '-';
                                        } else
                                            echo substr($sis->fn_formatarData($o->getDataNasc()), 0, 10);
                                        ?>
                                    </td>
                                    <td class="text-right"><span class="btn btn-xs btn-warning"
                                                                 onclick="editarlinha(this)"><span
                                                    class="glyphicon glyphicon-pencil"></span></span><span
                                                class="btn btn-xs btn-danger" onclick="excluirlinha(this)"><span
                                                    class="glyphicon glyphicon-remove"></span></span></td>
                                </tr>
                                <?php
                                $i++;
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }

    public function formVisualizar($objeto)
    {
        ?>
        <form id="formVisualizar">
        <div class="row">
            <input type="hidden" name="id" value="<?php echo $objeto->getId() ?>">
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="cnpj">CNPJ</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getCnpj() ?>"
                           name="cnpj" id="cnpj">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="razaoSocial">RAZAOSOCIAL</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getRazaoSocial() ?>" name="razaosocial" id="razaoSocial">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fantasia">FANTASIA</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getFantasia() ?>" name="fantasia" id="fantasia">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="cep">CEP</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getCep() ?>"
                           name="cep" id="cep">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="cidade">CIDADE</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getCidade() ?>" name="cidade" id="cidade">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="bairro">BAIRRO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getBairro() ?>" name="bairro" id="bairro">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="logradouro">LOGRADOURO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getLogradouro() ?>" name="logradouro" id="logradouro">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="numero">NUMERO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getNumero() ?>" name="numero" id="numero">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="complemento">COMPLEMENTO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getComplemento() ?>" name="complemento" id="complemento">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="estado">ESTADO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getEstado() ?>" name="estado" id="estado">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="numeroMedicos">NUMEROMEDICOS</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getNumeroMedicos() ?>" name="numeromedicos"
                           id="numeroMedicos">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="numeroPacientes">NUMEROPACIENTES</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getNumeroPacientes() ?>" name="numeropacientes"
                           id="numeroPacientes">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="pacientesDia">PACIENTESDIA</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getPacientesDia() ?>" name="pacientesdia" id="pacientesDia">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="fkUser">FKUSER</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getFkUser() ?>" name="fkuser" id="fkUser">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="ativo">ATIVO</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getAtivo() ?>"
                           name="ativo" id="ativo">
                </div>
            </div>
            <div class="col-sm-6 text-left">
                <div class="form-group form-group-sm">
                    <label for="dataIn">DATAIN</label>
                    <input type="text" disabled class="form-control " readonly
                           value="<?php echo $objeto->getDataIn() ?>" name="datain" id="dataIn">
                </div>
            </div>
        </div>
        </form><?php
    }

    public function formCadastrar()
    {
        date_default_timezone_set('America/Sao_Paulo');
        $date = date('d/m/Y');
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        $dados = $control->controlSwitch('carregaDados');
        ?>
        <script>
            var isMedico = false;

            function salvarDados() {
                //dados empresa
                cnpj = $('#txtCnpj').val();
                razao = $('#txtrazaosocial').val();
                fantasia = $('#txtfantasia').val();
                cepEmpresa = $('#txtcep').val();
                cidadeEmpresa = $('#txtcidade').val();
                estadoEmpresa = $('#txtestado').val();
                bairroEmpresa = $('#txtbairro').val();
                logradouroEmpresa = $('#txtlogradouro').val();
                numeroEmpresa = $('#txtnumero').val();
                complementoEmpresa = $('#txtcomplemento').val();
                emailEmpresa = $('#txtemail').val();
                telefoneEmpresa = $('#txttelefone').val();
                botao = $("#whatsapp");
                //validações empresa
                if (cnpj == '') {
                    alert("O campo CNPJ é obrigatório");
                    return;
                }
                if (razao == '') {
                    alert("O campo Razão Social é obrigatório");
                    return;
                }
                if (fantasia == '') {
                    alert("O campo Nome Fantasia é Obrigatório");
                    return;
                }
                if (cepEmpresa == '') {
                    alert("O campo CEP é Obrigatório");
                    return;
                }
                if (cidadeEmpresa == '') {
                    alert("O campo Cidade é Obrigatório");
                    return;
                }
                if (estadoEmpresa == '') {
                    alert("O campo UF é Obrigatório");
                    return;
                }
                if (bairroEmpresa == '') {
                    alert("O campo Bairro é Obrigatório");
                    return;
                }
                if (logradouroEmpresa == '') {
                    alert("O campo Endereço é Obrigatório");
                    return;
                }
                if (numeroEmpresa == '') {
                    alert("O campo Número é Obrigatório");
                    return;
                }
                if (emailEmpresa == '') {
                    alert("O campo E-mail é Obrigatório");
                    return;
                }
                if (telefoneEmpresa == '') {
                    alert("O campo Telefone é Obrigatório");
                    return;
                }
                if (botao[0].innerText == "WhatsApp") {
                    whatsappEmpresa = "0"
                } else {
                    whatsappEmpresa = "1"
                }
                //Adicionais empresa
                numeromedicos = $('#numeroMedicos').val();
                numeropacientes = $('#numeroPacientes').val();
                pacientesdia = $('#pacientesDia').val();
                btnSimAtendePar = $('#btnSim');
                valorConsulta = $('#valorConsulta').val();
                porcAtendimento = $('#porcAtendimento').val();
                relacionamento = $('.relacionamento');
                gestores = document.getElementById("gestores");
                btnSimCon = $('#btnSimCon');
                plataforma = $('.plataforma');
                especialidade = $('.especialidade');
                convenio = $('.convenio');
                //validações adicionais
                matriz = false;
                responsavel = 0;
                if (numeromedicos == '') {
                    alert("O campo Quantidade de Médicos é obrigatório");
                    return;
                }
                // if (numeropacientes == '') {
                //     alert("O campo Pacientes por Mês é obrigatório");
                //     return;
                // }
                // if (pacientesdia == '') {
                //     alert("O campo Consultas Por Dia é obrigatório");
                //     return;
                // }
                if (btnSimAtendePar[0].classList.contains('btn-success')) {
                    btnSimAtendePar = 1;
                    // if (valorConsulta == '') {
                    //     alert("O campo Valor Consulta é obrigatório");
                    //     return;
                    // }
                    // if (porcAtendimento == '') {
                    //     alert("O campo % do Atendimento é obrigatório");
                    //     return;
                    // }
                } else {
                    btnSimAtendePar = 0;
                }
                arrayConvenio = [];
                if (btnSimCon[0].classList.contains('btn-success')) {
                    btnSimCon = 1;
                    for (k = 0; k < convenio.length; k++) {
                        if ($("#" + convenio[k].id).prop("checked")) {
                            arrayConvenio.push(convenio[k].value);
                        }
                    }
                    // if (arrayConvenio.length == 0) {
                    //     alert("O campo Convênio é Obrigatório");
                    //     return;
                    // }
                } else {
                    btnSimCon = 0;
                }
                relacionamentoID = 0;
                for (j = 0; j < relacionamento.length; j++) {
                    if (relacionamento[j].classList.contains('btn-success')) {
                        matriz = false;
                        relacionamentoID = relacionamento[j].id;
                    }
                }
                if (relacionamentoID == 0) {
                    alert("O campo Relacionamento é Obrigatório");
                    return;
                }
                if (gestores.options[gestores.selectedIndex].value != '') {
                    responsavel = gestores.options[gestores.selectedIndex].value;
                }
                if (responsavel == 0) {
                    alert("Selecione um responsável pelo CNPJ");
                    return;
                }
                arrayPlataforma = [];
                arrayEspecialidade = [];
                for (k = 0; k < especialidade.length; k++) {
                    if ($("#" + especialidade[k].id).prop("checked")) {
                        arrayEspecialidade.push(especialidade[k].value);
                    }
                }
                for (k = 0; k < plataforma.length; k++) {
                    if ($("#" + plataforma[k].id).prop("checked")) {
                        arrayPlataforma.push(plataforma[k].value);
                    }
                }
                if (arrayPlataforma.length == 0) {
                    alert("O campo Plataforma é Obrigatório");
                    return;
                }
                if (arrayEspecialidade.length == 0) {
                    alert("O campo Especialidade é Obrigatório");
                    return;
                }
                //dados de contato
                var table = document.getElementById("tbcontatos");
                listaCh = table.children[0].children;
                listaTd = [];
                for (k = 1; k < listaCh.length; k++) {
                    if (listaCh[k]) {
                        listaTd.push(listaCh[k]);
                    }
                }
                if (listaTd.length < 1) {
                    alert("Adicione ao menos um contato a sua lista.");
                    return;
                }
                arraynome = [];
                arraydecisor = [];
                arraytipo = [];
                arraymedico = [];
                arrayresidente = [];
                arraycrm = [];
                arrayemail = [];
                arraytelefone = [];
                arraywhatsapp = [];
                arraydtnascimento = [];
                arraydtnascimento = [];
                for (k = 0; k < listaTd.length; k++) {
                    str = listaTd[k].children[0].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraynome.push(str);
                    str = listaTd[k].children[1].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraydecisor.push(str);
                    str = listaTd[k].children[2].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraytipo.push(str);
                    str = listaTd[k].children[3].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arraymedico.push(str);
                    str = listaTd[k].children[4].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arrayresidente.push(str);
                    str = listaTd[k].children[5].textContent;
                    str = str.trim();
                    str = str.replace("-", ' ');
                    arraycrm.push(str);
                    str = listaTd[k].children[6].textContent;
                    str = str.trim();
                    str = str.replace("-", ' ');
                    arrayemail.push(str);
                    str = listaTd[k].children[7].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    arraytelefone.push(str);
                    str = listaTd[k].children[8].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.replace("-", ' ');
                    arraywhatsapp.push(str);
                    str = listaTd[k].children[9].textContent;
                    str = str.replace(/\s{2,}/g, ' ');
                    str = str.trim();
                    if (str != "" && str != "-") {
                        str = str.split("/");
                        str = str[2] + "-" + str[1] + "-" + str[0];
                    } else {
                        str = '';
                    }
                    arraydtnascimento.push(str);
                }
                var formdata = new FormData();
                formdata.append('cnpj', cnpj)
                formdata.append('razao', razao)
                formdata.append('fantasia', fantasia)
                formdata.append('cepEmpresa', cepEmpresa)
                formdata.append('bairroEmpresa', bairroEmpresa)
                formdata.append('cidadeEmpresa', cidadeEmpresa)
                formdata.append('estadoEmpresa', estadoEmpresa)
                formdata.append('logradouroEmpresa', logradouroEmpresa)
                formdata.append('numeroEmpresa', numeroEmpresa)
                formdata.append('complementoEmpresa', complementoEmpresa)
                formdata.append('emailEmpresa', emailEmpresa)
                formdata.append('telefoneEmpresa', telefoneEmpresa)
                formdata.append('whatsappEmpresa', whatsappEmpresa)
                formdata.append('numeromedicos', numeromedicos)
                formdata.append('numeropacientes', numeropacientes)
                formdata.append('pacientesdia', pacientesdia)
                formdata.append('valorConsulta', valorConsulta)
                formdata.append('porcAtendimento', porcAtendimento)
                formdata.append('btnSimAtendePar ', btnSimAtendePar)
                formdata.append('btnSimCon', btnSimCon)
                formdata.append('arrayConvenio', arrayConvenio)
                formdata.append('relacionamentoID', relacionamentoID)
                formdata.append('responsavel', responsavel)
                formdata.append('arrayPlataforma', arrayPlataforma)
                formdata.append('arrayEspecialidade', arrayEspecialidade)
                formdata.append('arraynome', arraynome)
                formdata.append('arraydecisor', arraydecisor)
                formdata.append('arraytipo', arraytipo)
                formdata.append('arraymedico', arraymedico)
                formdata.append('arrayresidente', arrayresidente)
                formdata.append('arraycrm', arraycrm)
                formdata.append('arrayemail', arrayemail)
                formdata.append('arraytelefone', arraytelefone)
                formdata.append('arraywhatsapp', arraywhatsapp)
                formdata.append('arraydtnascimento', arraydtnascimento)

                $.ajax({
                    type: 'POST',
                    url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/save/',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function (retorno) {
                        if (retorno == '') {
                            location.reload();
                        } else {
                            alert(retorno);
                        }
                    }
                });
            }

            $(document).ready(function () {
                $("#salvarInserir").hide();
                $("#salvarEditar").hide();
                $("#footerCadastrar").html('<button type="button" onclick="salvarDados()" id="btnSalvardados" style="display:none" class="btn btn-success" >Salvar</button><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalMsgBoxConfirm">Cancelar</button>');
                $(".isMedico").hide();
                $("#primeira").click(function () {
                    $("#primeiraParte").show();
                    $("#segundaParte").hide();
                    $("#terceiraParte").hide();
                    $("#primeira").addClass('active');
                    $("#segunda").removeClass('active');
                    $("#terceira").removeClass('active');
                });
                $("#segunda").click(function () {
                    $("#segundaParte").show();
                    $("#primeiraParte").hide();
                    $("#terceiraParte").hide();
                    $("#segunda").addClass('active');
                    $("#primeira").removeClass('active');
                    $("#terceira").removeClass('active');
                });
                $("#terceira").click(function () {
                    $("#terceiraParte").show();
                    $("#primeiraParte").hide();
                    $("#segundaParte").hide();
                    $("#terceira").addClass('active');
                    $("#primeira").removeClass('active');
                    $("#segunda").removeClass('active');
                });
            });
            $("#whatsapp").click(function () {
                botao = $("#whatsapp");
                check = '&check; WhatsApp';
                if (botao[0].innerText == "WhatsApp") {
                    botao.addClass('btn-success');
                    botao.html(check);
                } else {
                    botao.removeClass('btn-success');
                    botao.text("WhatsApp");
                }
            });
            $('.relacionamento').click(function () {
                $('.relacionamento').removeClass('btn-success');
                botao = this.id;
                $('#' + botao).addClass('btn-success');
            });
            $('.consulta').click(function () {
                $('.consulta').removeClass('btn-success');
                $('.consulta').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSim") {
                    $(".consultaDis").prop('disabled', false);
                    $('#' + botao).addClass('btn-success');
                } else {
                    $(".consultaDis").prop('disabled', true);
                    $('#' + botao).addClass('btn-danger');
                }

            });
            $('.consultaCon').click(function () {
                $('.consultaCon').removeClass('btn-success');
                $('.consultaCon').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimCon") {
                    $(".checkboxPlano").show();
                    $('#' + botao).addClass('btn-success');
                } else {
                    $(".checkboxPlano").hide();
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.decisor').click(function () {
                $('.decisor').removeClass('btn-success');
                $('.decisor').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimDec") {
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.medico').click(function () {
                $('.medico').removeClass('btn-success');
                $('.medico').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimMed") {
                    $('.isMedico').show();
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('.isMedico').hide();
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $('.residente').click(function () {
                $('.residente').removeClass('btn-success');
                $('.residente').removeClass('btn-danger');
                botao = this.id;
                if (botao == "btnSimResi") {
                    $('#' + botao).addClass('btn-success');
                } else {
                    $('#' + botao).addClass('btn-danger');
                }
            });
            $("#whatsappContato").click(function () {
                botao = $("#whatsappContato");
                check = '&check; WhatsApp';
                if (botao[0].innerText == "WhatsApp") {
                    botao.addClass('btn-success');
                    botao.html(check);
                } else {
                    botao.removeClass('btn-success');
                    botao.text("WhatsApp");
                }
            });

            function solicitarAjax(valor) {
                $.ajax({
                    type: "POST",
                    url: 'https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/buscaCNPJ/' + valor,
                    crossDomain: true,
                    dataType: "text",
                    contentType: false,
                    processData: false,
                    success: function (retorno) {
                        if (retorno === "Too many requests, please try again later.") {
                            msgBox('Número de consultas excedidas<br>Aguarde, sua solicitação será reenviada em <span id="numberCountdown" align="center"></span> segundos.')
                            segundos = 45;

                            function contagemTempo() {
                                document.getElementById('numberCountdown').innerText = segundos;
                                segundos = segundos - 1;
                                if (segundos != -1) {
                                    setTimeout(function () {
                                        contagemTempo();
                                    }, 1000);
                                } else {
                                    $('#modalMsgBox').modal('hide');
                                }
                            }

                            contagemTempo();
                            setTimeout(function () {
                                solicitarAjax(valor);
                            }, 45000);
                        } else {
                            retorno = $.parseJSON(retorno);
                            if (retorno.status === "ERROR") {
                                alert(retorno.message);
                            } else {
                                document.getElementById('txtrazaosocial').value = retorno.nome;
                                document.getElementById('txtfantasia').value = retorno.fantasia;
                                document.getElementById('txtcep').value = retorno.cep;
                                document.getElementById('txtcidade').value = retorno.municipio;
                                document.getElementById('txtnumero').value = retorno.numero;
                                document.getElementById('txtbairro').value = retorno.bairro;
                                document.getElementById('txttelefone').value = retorno.telefone;
                                document.getElementById('txtemail').value = retorno.email;
                                document.getElementById('txtlogradouro').value = retorno.logradouro;
                                $('#segunda').show();
                                $('#terceira').show();
                                $('#btnSalvardados').show();
                            }
                        }
                    }
                });
            }

            $("#buscarDados").click(function () {
                valor = document.getElementById('txtCnpj').value;
                valor = valor.replace(/\./g, '');
                valor = valor.replace(/\//g, '');
                valor = valor.replace(/-/g, '');
                solicitarAjax(valor);
            });
            var i = 1;

            $('#addContato').click(function () {
                var table = document.getElementById("tbcontatos");
                var tipo = document.getElementById("cmbtipocontato");
                var decisor = document.getElementById("btnSimDec");
                var medico = document.getElementById("btnSimMed");
                var residente = document.getElementById("btnSimResi");
                var ufCrm = document.getElementById("txtUf");
                var crm = document.getElementById("txtCrm");
                var nome = document.getElementById("txtnomecontato");
                var nascimento = document.getElementById("txtdataContato");
                var email = document.getElementById("txtemailcontato");
                var telefone = document.getElementById("txttelefonecontato");
                var whatsapp = document.getElementById("whatsappContato");
                if (nome.value == '') {
                    alert('Informe o nome do contato');
                    return;
                }
                // if (nascimento.value == '') {
                //     alert('Informe a data de nascimento');
                //     return;
                // }
                // if (email.value == '') {
                //     alert('Informe o email');
                //     return;
                // }
                // if (telefone.value == '') {
                //     alert('Informe o telefone');
                //     return;
                // }
                if (tipo.options[tipo.selectedIndex].value == '') {
                    alert('Informe o tipo do contato');
                    return;
                }
                if (decisor.classList.contains('btn-success'))
                    decisor = "Sim";
                else
                    decisor = "Não";
                if (medico.classList.contains('btn-success')) {
                    medico = "Sim";
                    if (!isMedico) {
                        alert("Verificação de CRM obrigatória para inclusão de médicos");
                        return;
                    }
                    if (ufCrm.value == '' || crm.value == '') {
                        alert("infome o CRM e UF")
                        return;
                    }
                } else
                    medico = "Não"
                if (residente.classList.contains('btn-success'))
                    residente = "Sim"
                else
                    residente = "Não"
                if (whatsapp.classList.contains('btn-success'))
                    whatsapp = "Sim"
                else
                    whatsapp = "Não"

                var row = table.insertRow(1);
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);
                var cell3 = row.insertCell(2);
                var cell4 = row.insertCell(3);
                var cell5 = row.insertCell(4);
                var cell6 = row.insertCell(5);
                var cell7 = row.insertCell(6);
                var cell8 = row.insertCell(7);
                var cell9 = row.insertCell(8);
                var cell10 = row.insertCell(9);
                var cell11 = row.insertCell(10);
                var cell12 = row.insertCell(11);
                cell3.innerHTML = tipo.options[tipo.selectedIndex].text;

                cell2.innerHTML = decisor;

                cell4.innerHTML = medico;

                cell5.innerHTML = residente;

                cell6.innerHTML = ufCrm.value.toUpperCase() + crm.value;

                cell1.innerHTML = nome.value.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toUpperCase();

                data = nascimento.value.trim();
                if (data != "") {
                    data = data.split("-");
                    data = data[2] + "/" + data[1] + "/" + data[0];
                } else {
                    data = "-";
                }
                cell10.innerHTML = data;

                cell7.innerHTML = email.value;

                cell8.innerHTML = telefone.value;

                cell9.innerHTML = whatsapp;

                cell11.innerHTML = '<span class="btn btn-xs btn-warning" onclick="editarlinha(this)"><span class="glyphicon glyphicon-pencil"></span></span><span class="btn btn-xs btn-danger" onclick="excluirlinha(this)"><span class="glyphicon glyphicon-remove"></span></span>';
                cell11.classList.add("text-right");
                $('.decisor').removeClass('btn-success');
                $('.decisor').removeClass('btn-danger');
                $('#btnNaoDec').addClass('btn-danger');
                $('.medico').removeClass('btn-success');
                $('.medico').removeClass('btn-danger');
                $('#btnNaoMed').addClass('btn-danger');
                $('.residente').removeClass('btn-danger');
                $('.residente').removeClass('btn-success');
                $('#btnNaoResi').addClass('btn-danger');
                botao = $("#whatsappContato");
                botao.removeClass('btn-success');
                botao.text("WhatsApp");
                $('.isMedico').hide();
                tipo.value = '';
                ufCrm.value = "";
                crm.value = "";
                nome.value = "";
                nascimento.value = "";
                email.value = "";
                telefone.value = "";
                isMedico = false;
            });

            function excluirlinha(elemento) {
                linha = elemento.parentElement.parentElement;
                tabela = elemento.parentElement.parentElement.parentElement;
                tabela.removeChild(linha);
            }

            function editarlinha(elemento) {
                linha = elemento.parentElement.parentElement;
                tabela = elemento.parentElement.parentElement.parentElement;
                tabela.removeChild(linha);
                var tipo = document.getElementById("cmbtipocontato");
                valorTipo = linha.children[2].textContent.trim();
                if (valorTipo != "") {
                    tipo = tipo.children;
                    for (j = 0; j < tipo.length; j++)
                        if (tipo[j].text == valorTipo) {
                            document.getElementById("cmbtipocontato").value = tipo[j].value;
                        }
                }
                var decisor = document.getElementById("btnSimDec");
                deciso = linha.children[1].textContent.trim();
                if (deciso == "Sim") {
                    $('.decisor').removeClass('btn-danger');
                    $('.decisor').removeClass('btn-success');
                    $('#btnSimDec').addClass('btn-success');
                } else {
                    $('.decisor').removeClass('btn-danger');
                    $('.decisor').removeClass('btn-success');
                    $('#btnNaoDec').addClass('btn-danger');
                }
                var medico = document.getElementById("btnSimMed");
                reside = linha.children[3].textContent.trim();
                if (reside == "Sim") {
                    $('.medico').removeClass('btn-danger');
                    $('.medico').removeClass('btn-success');
                    $('#btnSimMed').addClass('btn-success');
                    $('.isMedico').show();
                } else {
                    $('.medico').removeClass('btn-danger');
                    $('.medico').removeClass('btn-success');
                    $('#btnNaoMed').addClass('btn-danger');
                    $('.isMedico').hide();
                }
                var residente = document.getElementById("btnSimResi");
                reside = linha.children[4].textContent.trim();
                if (reside == "Sim") {
                    $('.residente').removeClass('btn-danger');
                    $('.residente').removeClass('btn-success');
                    $('#btnSimResi').addClass('btn-success');
                } else {
                    $('.residente').removeClass('btn-danger');
                    $('.residente').removeClass('btn-success');
                    $('#btnNaoResi').addClass('btn-danger');
                }
                var ufCrm = document.getElementById("txtUf");
                crmdados = linha.children[5].textContent.trim();
                if (crmdados.length == 9) {
                    ufCrm.value = crmdados[0] + crmdados[1];
                    var crm = document.getElementById("txtCrm");
                    crm.value = crmdados[2] + crmdados[3] + crmdados[4] + crmdados[5] + crmdados[6] + crmdados[7] + crmdados[8];
                }
                var nome = document.getElementById("txtnomecontato");
                nome.value = linha.children[0].textContent;
                var nascimento = document.getElementById("txtdataContato");
                data = linha.children[9].textContent.trim();
                data = data.split("/");
                nascimento.value = data[2] + "-" + data[1] + "-" + data[0];
                var email = document.getElementById("txtemailcontato");
                email.value = linha.children[6].textContent.trim();
                var telefone = document.getElementById("txttelefonecontato");
                telefone.value = linha.children[7].textContent.trim();
                var whatsapp = document.getElementById("whatsappContato");
                wt = linha.children[8].textContent.trim();
                if (wt == "Sim") {
                    botao = $("#whatsappContato");
                    botao.addClass('btn-success');
                    botao.text("✓ WhatsApp");
                }
            }
        </script>
        <ul class="nav nav-tabs">
            <li id="primeira" class="active"><a>Cadastro CNPJ</a></li>
            <li id="segunda" style="display: none;"><a>Dados Adicionais</a></li>
            <li id="terceira" style="display: none;"><a>Contatos</a></li>
        </ul>
        <br>
        <form id="formSalvar">
            <div class="row" id="primeiraParte">
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fkUser">Responsável</label>
                        <input
                                value="<?php echo $_SESSION['nome']; ?>"
                                type="text"
                                class="form-control "
                                name="fkuser"
                                disabled
                                id="fkUser">
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="dataIn">Data Cadastro</label>
                        <input
                                disabled
                                type="text"
                                value="<?php echo $date ?>"
                                class="form-control "
                                name="datain"
                                id="dataIn">
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cnpj">CNPJ</label>
                        <input
                                type="text"
                                class="form-control maskCnpj"
                                name="txtCnpj"
                                id="txtCnpj">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cnpj" style="color: white">.</label>
                        <span class="btn form-control btn-info"
                              id="buscarDados">
                        <span class="glyphicon glyphicon-search">
                        </span>
                            Verificar
                        </span>
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="razaoSocial">Razão Social</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtrazaosocial"
                                id="txtrazaosocial">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="fantasia">Nome Fantasia</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtfantasia"
                                id="txtfantasia">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cep">CEP</label>
                        <input
                                type="text"
                                class="form-control maskCEP"
                                name="txtcep"
                                id="txtcep">
                    </div>
                </div>
                <div class="col-sm-3 text-left">
                    <div class="form-group form-group-sm">
                        <label for="cidade">Cidade</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtcidade"
                                id="txtcidade">
                    </div>
                </div>
                <div class="col-sm-1 text-left">
                    <div class="form-group form-group-sm">
                        <label for="estado">UF</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtestado"
                                maxlength="2"
                                size="2"
                                id="txtestado">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="bairro">Bairro</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtbairro"
                                id="txtbairro">
                    </div>
                </div>
                <div class="col-sm-10 text-left">
                    <div class="form-group form-group-sm">
                        <label for="logradouro">Endereço</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtlogradouro"
                                id="txtlogradouro">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="numero">Número</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtnumero"
                                id="txtnumero">
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">Complemento</label>
                        <input
                                type="text"
                                class="form-control "
                                name="txtcomplemento"
                                id="txtcomplemento">
                    </div>
                </div>
                <div class="col-sm-6 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">E-mail</label>
                        <input
                                type="email"
                                class="form-control "
                                name="txtemail"
                                id="txtemail">
                    </div>
                </div>
                <div class="col-sm-4 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento">Telefone</label>
                        <input
                                type="text"
                                class="form-control maskTel"
                                name="txttelefone"
                                id="txttelefone">
                    </div>
                </div>
                <div class="col-sm-2 text-left">
                    <div class="form-group form-group-sm">
                        <label for="complemento" style="color:white">.df</label>
                        <button
                                name="whatsapp"
                                class="form-control btn btn-default"
                                id="whatsapp"
                                onclick="return false"
                        >WhatsApp
                        </button>

                    </div>
                </div>
            </div>
            <div class="row" id="segundaParte" style="display: none">
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-7 text-left">
                                <div class="row">
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="numeroMedicos">Qtd. Médicos</label>
                                            <input
                                                    type="number"
                                                    class="form-control "
                                                    name="numeromedicos"
                                                    id="numeroMedicos">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="numeroPacientes">Pacientes P/ Mês</label>
                                            <input
                                                    type="number"
                                                    class="form-control "
                                                    name="numeropacientes"
                                                    id="numeroPacientes">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="pacientesDia">Consultas p/ Dia</label>
                                            <input
                                                    type="number"
                                                    class="form-control "
                                                    name="pacientesdia"
                                                    id="pacientesDia">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="relacionamento">Atende Particular?</label><br>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnSim" class="consulta btn"
                                                    onclick="Particular(1)">SIM
                                            </button>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnNao"
                                                    class="consulta btn btn-danger"
                                                    onclick="Particular(0)">NÃO
                                            </button>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="valorConsulta">Valor Consulta</label>
                                            <input
                                                    type="text"
                                                    class="form-control consultaDis maskMoney    "
                                                    disabled="true"
                                                    name="valorConsulta"
                                                    id="valorConsulta">
                                        </div>
                                    </div>
                                    <div class="col-sm-4 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="porcAtendimento">% do Atendimento</label>
                                            <input
                                                    disabled="true"
                                                    type="text"
                                                    class="form-control consultaDis "
                                                    name="porcAtendimento"
                                                    id="porcAtendimento">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-2 text-left" style="padding: 0">
                                <div class="form-group form-group-sm">
                                    <label for="relacionamento">Relacionamento</label><br>
                                    <button style="margin-bottom: 3px" id="A1" class="relacionamento btn"
                                            onclick="return false">A1
                                    </button>
                                    <button style="margin-bottom: 3px" id="B1" class="relacionamento btn"
                                            onclick="return false">B1
                                    </button>
                                    <button style="margin-bottom: 3px" id="C1" class="relacionamento btn"
                                            onclick="return false">C1
                                    </button>
                                    <br>
                                    <button style="margin-bottom: 3px" id="A2" class="relacionamento btn"
                                            onclick="return false">A2
                                    </button>
                                    <button style="margin-bottom: 3px" id="B2" class="relacionamento btn"
                                            onclick="return false">B2
                                    </button>
                                    <button style="margin-bottom: 3px" id="C2" class="relacionamento btn"
                                            onclick="return false">C2
                                    </button>
                                    <br>
                                    <button style="margin-bottom: 3px" id="A3" class="relacionamento btn"
                                            onclick="return false">A3
                                    </button>
                                    <button style="margin-bottom: 3px" id="B3" class="relacionamento btn"
                                            onclick="return false">B3
                                    </button>
                                    <button style="margin-bottom: 3px" id="C3" class="relacionamento btn"
                                            onclick="return false">C3
                                    </button>
                                </div>
                            </div>
                            <div class="col-sm-3 text-left">
                                <div class="row">
                                    <div class="col-sm-12 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="gestores">Atribuir Responsável</label>
                                            <select id="gestores" class="form-control" name="gestores">
                                                <option value="">Selecione...</option>
                                                <?php
                                                foreach ($dados['colaborador'] as $colaborador) {
                                                    ?>
                                                    <option
                                                            value="<?php echo $colaborador['ID_COLABORADOR'] ?>">
                                                        <?php echo $colaborador['SETOR'] . " - " . $colaborador['NOME'] ?></option>
                                                    <?php

                                                }
                                                ?>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="col-sm-12 text-left">
                                        <div class="form-group form-group-sm">
                                            <label for="relacionamento">Atende Convênio?</label><br>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnSimCon"
                                                    class="consultaCon btn"
                                                    onclick="return false">SIM
                                            </button>
                                            <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                                    id="btnNaoCon"
                                                    class="consultaCon btn btn-danger"
                                                    onclick="return false">NÃO
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="form-group form-group-sm">
                        <div class="row">
                            <div class="col-sm-5 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="" style="display: ;"
                                           for="relacionamento">Plataforma</label>
                                    <div class=""
                                         style="display: ;border: 1px solid #d6d6d6; height: 100px; padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['plataforma'] as $plataforma) {
                                            ?>
                                            <input type="checkbox"
                                                   id="plataforma_<?php echo $plataforma->getId() ?>"
                                                   class="plataforma" name="plataforma[]"
                                                   value="<?php echo $plataforma->getId() ?>">
                                            <label><?php echo $plataforma->getNome() ?></label><br><?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="" style="display: ;"
                                           for="relacionamento">Especialidade</label>
                                    <div class=""
                                         style="display: ;border: 1px solid #d6d6d6; height: 100px; padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['especialidade'] as $especialidade) {
                                            ?>
                                            <input type="checkbox" class="especialidade"
                                                   id="especialidade_<?php echo $especialidade->getId() ?>"
                                                   name="especialidade[]"
                                                   value="<?php echo $especialidade->getId() ?>">
                                            <label><?php echo $especialidade->getNome() ?></label><br><?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 text-left">
                                <div class="form-group form-group-sm">
                                    <label class="checkboxPlano" style="display: none;"
                                           for="relacionamento">Convênios</label>
                                    <div class="checkboxPlano"
                                         style="display: none;border: 1px solid #d6d6d6; height: 100px; padding: 2; overflow: auto">
                                        <?php
                                        foreach ($dados['convenio'] as $convenio) {
                                            ?>
                                            <input type="checkbox" id="convenio_<?php echo $convenio->getId() ?>"
                                                   class="convenio" name="convenio[]"
                                                   value="<?php echo $convenio->getId() ?>">
                                            <label><?php echo $convenio->getNome() ?></label><br><?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="terceiraParte" style="display: none">
                <div class="col-sm-12 text-left">
                    <div class="row">
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="cmbtipocontato">Tipo Contato</label>
                                <select id="cmbtipocontato" class="form-control" name="cmbtipocontato">
                                    <option value="">Selecione...</option>
                                    <?php
                                    foreach ($dados['tipocontato'] as $contato) {
                                        ?>
                                        <option
                                                value="<?php echo $contato->getId() ?>">
                                            <?php echo $contato->getDescricao() ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Decisor</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimDec"
                                        class="decisor btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoDec"
                                        class="decisor btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Médico</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimMed"
                                        class="medico btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoMed"
                                        class="medico btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="relacionamento">Residente</label><br>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnSimResi"
                                        class="residente btn "
                                        onclick="return false">SIM
                                </button>
                                <button style="margin-right: 3px;padding-top: 4px;padding-bottom: 4px"
                                        id="btnNaoResi"
                                        class="residente btn btn-danger"
                                        onclick="return false">NÃO
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="txtUF">UF</label>
                                <input id="txtUf"
                                       name="txtUf"
                                       type="text"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-2 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="txtCrm">CRM</label>
                                <input name="txtCrm"
                                       id="txtCrm"
                                       type="number"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left isMedico">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button name="buscarMedico"
                                        class="form-control btn btn-success"
                                        id="buscarMedico"
                                        onclick="return false">
                                    <span class="glyphicon glyphicon-search"></span>
                                </button>
                            </div>
                        </div>
                        <script>
                            $("#buscarMedico").click(function () {
                                var crm = $("#txtCrm").val();
                                var uf = $("#txtUf").val();
                                var adicionar = 7 - crm.length;
                                for (var i = 0; i < adicionar; i++) crm = '0' + crm;
                                crm = crm.slice(0, 7);
                                retorno = requisicaoAjax('https://' + window.location.host + '/sistemas/PlanoCNPJ/PaEmpresa/buscarCrm/' + uf + crm);
                                $("#txtCrm").val(crm);
                                if (retorno === '') {
                                    alert('Médico Ainda Não Cadastrado!\nEnvie os dados ao time de efetividade para inclusão.\nefetividade@ems.com.br')
                                    isMedico = false;
                                } else {
                                    alert('Médico Encontrado')
                                    $("#txtnomecontato").val(retorno);
                                    isMedico = true;
                                }
                            });
                        </script>
                    </div>
                </div>
                <div class="col-sm-12 text-left">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <div class="form-group form-group-sm">
                                <label for="fkUser">Nome</label>
                                <input type="text"
                                       name="txtnomecontato"
                                       id="txtnomecontato"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-3 text-left">
                            <div class="form-group form-group-sm">
                                <label for="fkUser">Dt. Nascimento</label>
                                <input type="date"
                                       id="txtdataContato"
                                       name="txtdataContato"
                                       class="form-control"/>
                            </div>
                        </div>
                        <div class="col-sm-5 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento">E-mail</label>
                                <input
                                        type="email"
                                        class="form-control"
                                        name="txtemailcontato"
                                        id="txtemailcontato">
                            </div>
                        </div>
                        <div class="col-sm-4 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento">Telefone</label>
                                <input
                                        type="text"
                                        class="form-control maskTel"
                                        name="txttelefonecontato"
                                        id="txttelefonecontato">
                            </div>
                        </div>
                        <div class="col-sm-2 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button
                                        name="whatsappContato"
                                        class="form-control btn btn-default"
                                        id="whatsappContato"
                                        onclick="return false"
                                >WhatsApp
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-1 text-left">
                            <div class="form-group form-group-sm">
                                <label for="complemento" style="color:white">.df</label>
                                <button
                                        name="addContato"
                                        class="form-control btn btn-success"
                                        id="addContato"
                                        onclick="return false"
                                ><b>&plus;</b>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <script>
                </script>
                <div class="col-sm-12 text-left">
                    <div style="overflow: auto">
                        <table class="table table-striped" style="font-size: 13px;" id="tbcontatos">
                            <thead>
                            <tr>
                                <th>Nome</th>
                                <th>Decisor</th>
                                <th>Tipo</th>
                                <th>Médico</th>
                                <th>Residente</th>
                                <th>CRM</th>
                                <th>E-mail</th>
                                <th>Telefone</th>
                                <th>WhatsApp</th>
                                <th>Nascimento</th>
                                <th class="text-right">Remover</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
        <?php
    }
}
