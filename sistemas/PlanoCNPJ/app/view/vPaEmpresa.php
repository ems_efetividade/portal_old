<?php
require_once('header.php');

class vPaEmpresa extends gridView
{
    public function __construct()
    {
        //$this->viewGrid();
        //$this->viewTeste();
        $this->viewPrincipal();
        $this->rodape();
    }

    public function viewPrincipal()
    {
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        ?>
        <link href="/../../public/css/signo.css" rel="stylesheet" type="text/css">
        <link href="/../../../../public/js/Notiflix/Minified/notiflix-1.4.0.min.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="/../../../../public/js/Notiflix/Minified/notiflix-1.4.0.min.js"></script>
        <script type="text/javascript" src="/../../../../public/js/Notiflix/AIO/notiflix-aio-1.4.0.min.js"></script>
        <script>
            Notiflix.Loading.Init({
                svgColor: "#00639A ",
                zindex: 999000,
            });
            Notiflix.Loading.Circle('Aguarde...');
            $( document ).ready(function() {
                setTimeout(function () {
                    Notiflix.Loading.Remove();
                }, 1000);
            });
        </script>
        <div class="container" id="conteudoView">
            <?php
            $control->controlSwitch('home');
            ?>
        </div>
        <script type="text/javascript" src="/../../../../public/js/signo.js"></script>
        <br>
        <br>
        <?php
    }

    public function rodape()
    {
        ?>
        <div id="home-rodape" style="bottom:0;width:100%;background-color: #0f0f0fc; position:fixed; z-index: 100000">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <h5>
                            <p class="text-center">
                               Portal Efetividade - Grupo NC
                            </p>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }

    public function viewTeste()
    {
        require_once('/../controller/cPaEmpresa.php');
        $control = new cPaEmpresa();
        ?>
        <style>
            .paggerPoint {
                cursor: pointer;
            }

            ul.nav.nav-pills {
                text-align: right;
            }

            .nav-pills > li {
                float: none;
                display: inline-block;
            }

            .nav-pills > li > a {
                padding: 7px 12px;
            }

            .progressbar {
                counter-reset: step;
                padding: 0;
            }

            .progressbar li {
                list-style-type: none;
                float: left;
                font-size: 12px;
                position: relative;
                text-align: center;
                text-transform: uppercase;
                color: #7d7d7d;
            }

            .progressbar li:before {
                width: 35px;
                height: 35px;
                content: counter(step);
                counter-increment: step;
                line-height: 30px;
                border: 2px solid #7d7d7d;
                display: block;
                text-align: center;
                margin: 0 auto 10px auto;
                border-radius: 50%;
                background-color: white;
            }

            .progressbar li.active:before {
                background-color: #4a9355;
                color: white;
                font-weight: bold;
            }

            .progressbar li.atual:before {
                background-color: #55b776;
                color: white;
                font-weight: bold;
                border-color: #55b776;
            }

            .progressbar li:after {
                width: 100%;
                height: 2px;
                content: '';
                position: absolute;
                background-color: #7d7d7d;
                top: 15px;
                left: -10%;
                z-index: -1;
            }

            #ultimoStep:after {
                left: -60%;
            }

            .progressbar li:first-child:after {
                content: none;
            }

            .progressbar li.active {
                color: green;
            }

            .progressbar li.active:before {
                border-color: #4a9355;
            }

            .progressbar li.active + li:after {
                background-color: #4a9355;
            }

            .inp {
                position: relative;
                margin-top: 15px;
                width: 100%;
            }

            .inp .label {
                position: absolute;
                top: 16px;
                left: 0;
                font-size: 16px;
                color: #9098a9;
                font-weight: 500;
                transform-origin: 0 0;
                transition: all 0.2s ease;
            }

            .inp .border {
                position: absolute;
                bottom: 0;
                left: 0;
                height: 2px;
                width: 100%;
                background: #07f;
                transform: scaleX(0);
                transform-origin: 0 0;
                transition: all 0.15s ease;
            }

            .inp input {
                -webkit-appearance: none;
                width: 100%;
                border: 0;
                font-family: inherit;
                padding: 12px 0;
                height: 48px;
                font-size: 16px;
                font-weight: 500;
                border-bottom: 2px solid #c8ccd4;
                background: none;
                border-radius: 0;
                color: #223254;
                transition: all 0.15s ease;
            }

            .inp input:hover {
                background: rgba(34, 50, 84, 0.03);
            }

            .inp input:not(:placeholder-shown) + span {
                color: #5a667f;
                transform: translateY(-26px) scale(0.75);
            }

            .inp input:focus {
                background: none;
                outline: none;
            }

            .inp input:focus + span {
                color: #07f;
                transform: translateY(-26px) scale(0.75);
            }

            .inp input:focus + span + .border {
                transform: scaleX(1);
            }

            .flat-button {
                position: relative;
                width: 100%;
                height: 34px;
                overflow: hidden;
                background: #a6a6a6;
                z-index: 1;
                top: 30;
                cursor: pointer;
                transition: color .3s;
                /* Typo */
                line-height: 35px;
                text-align: center;
                color: #fff;
                border-radius: 2px;
            }

            .flat-button:after {
                position: absolute;
                top: 90%;
                left: 0;
                width: 100%;
                height: 100%;
                background: #c6c6c6;
                content: "";
                z-index: -2;
                color: #000;
                transition: transform .3s;
            }

            .flat-button:hover::after {
                transform: translateY(-80%);
                transition: transform .3s;
            }

            .flat-button-info {
                background: #00639A;
            }

            .flat-button-info:after {
                background: #0e90d2;
            }

            .flat-button-info:hover::after {
                transform: translateY(-85%);
                transition: transform .4s;
            }

            .flat-button-danger {
                background: #d9534f;
            }

            .flat-button-danger:after {
                background: #e9322d;
            }

            .flat-button-danger:hover::after {
                transform: translateY(-85%);
                transition: transform .4s;
            }

            .flat-button-sm {
                width: 20%;
            }

            .switch__container {
                width: 60px;
            }

            .switch {
                visibility: hidden;
                position: absolute;
                margin-left: -9999px;
            }

            .switch + label {
                display: block;
                position: relative;
                cursor: pointer;
                outline: none;
                user-select: none;
            }

            .switch--shadow + label {
                padding: 2px;
                width: 60px;
                height: 30px;
                background-color: #DDDDDD;
                border-radius: 30px;
            }

            .switch--shadow + label:before,
            .switch--shadow + label:after {
                display: block;
                position: absolute;
                top: 1px;
                left: 1px;
                bottom: 1px;
                content: "";
            }

            .switch--shadow + label:before {
                right: 1px;
                background-color: #f1f1f1;
                border-radius: 30px;
                transition: background 0.4s;
            }

            .switch--shadow + label:after {
                width: 31px;
                background-color: #fff;
                border-radius: 100%;
                box-shadow: 0 2px 5px rgba(0, 0, 0, 0.3);
                transition: all 0.4s;
            }

            .switch--shadow:checked + label:before {
                background-color: #8ce196;
            }

            .switch--shadow:checked + label:after {
                transform: translateX(30px);
            }

            ul.ks-cboxtags {
                list-style: none;
                padding: 10px;
            }

            ul.ks-cboxtags li {
                display: inline;
            }

            ul.ks-cboxtags li label {
                display: inline-block;
                background-color: rgba(255, 255, 255, .9);
                border: 2px solid rgba(139, 139, 139, .3);
                color: #545b62;
                border-radius: 25px;
                white-space: nowrap;
                margin: 3px 0px;
                -webkit-touch-callout: none;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                -webkit-tap-highlight-color: transparent;
                transition: all .2s;
                font-size: 12px;
            }

            ul.ks-cboxtags li label {
                padding: 8px 8px;
                cursor: pointer;
            }

            ul.ks-cboxtags li label::before {
                margin-top: 2px;
                display: inline-block;
                font-style: normal;
                font-variant: normal;
                text-rendering: auto;
                -webkit-font-smoothing: antialiased;
                font-family: "Font Awesome 5 Free";
                font-weight: 900;
                font-size: 12px;
                padding: 2px 6px 2px 2px;
                content: "➕";
                transition: transform .3s ease-in-out;
            }

            ul.ks-cboxtags li input[type="checkbox"]:checked + label::before {
                margin-left: 2px;
                content: "✔";
                transform: rotate(-360deg);
                transition: transform .3s ease-in-out;
            }

            ul.ks-cboxtags li input[type="checkbox"]:checked + label {
                border: 2px solid #12bbd4;
                background-color: #12bbd4;
                color: #fff;
                transition: all .2s;
            }

            ul.ks-cboxtags li input[type="checkbox"] {
                display: absolute;
            }

            ul.ks-cboxtags li input[type="checkbox"] {
                position: absolute;
                opacity: 0;
            }

            ul.ks-cboxtags li input[type="checkbox"]:focus + label {
                border: 2px solid #12bbd4;
            }

            body {
                font-family: 'Segoe UI', Tahoma, Geneva, Verdana, sans-serif;
            }

            .select {
                border-bottom: 2px solid #c8ccd4;
                position: relative;
                display: block;
                margin: 0 auto;
                width: 100%;
                color: #cccccc;
                vertical-align: middle;
                text-align: left;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;
                user-select: none;
                -webkit-touch-callout: none;
            }

            .select .placeholder {
                position: relative;
                display: block;
                background-color: #393d41;
                z-index: 1;
                padding: 1em;
                border-radius: 2px;
                cursor: pointer;
            }

            .select .placeholder:hover {
                background: #34383c;
            }

            .select .placeholder:after {
                position: absolute;
                right: 1em;
                top: 50%;
                -webkit-transform: translateY(-50%);
                transform: translateY(-50%);
                font-family: 'FontAwesome';
                content: '▼';
                z-index: 10;
            }

            .select.is-open .placeholder:after {
                content: '▲';
                color: #07f;
            }

            .select.select--white.is-open {
                border-bottom: 2px solid #07f;
            }

            .select.is-open ul {
                display: block;
            }

            .select.select--white .placeholder {
                background: #fff;
                color: #999;
            }

            .select.select--white .placeholder:hover {
                background: #fafafa;
            }

            .select ul {
                display: none;
                position: absolute;
                overflow: hidden;
                overflow-y: auto;
                width: 100%;
                background: #fff;
                border-radius: 2px;
                top: 100%;
                left: 0;
                list-style: none;
                margin: 5px 0 0 0;
                padding: 0;
                z-index: 100;
                max-height: 120px;
            }

            .select ul li {
                display: block;
                text-align: left;
                padding: 0.8em 1em 0.8em 1em;
                color: #999;
                cursor: pointer;
            }

            .select ul li:hover {
                background: #4ebbf0;
                color: #fff;
            }

            ::-webkit-scrollbar {
                width: 6px;
            }

            ::-webkit-scrollbar-track {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            }

            ::-webkit-scrollbar-thumb {
                -webkit-box-shadow: inset 0 0 6px rgba(0, 0, 0, 0.3);
            }

            /**---------------------------------------------------*/
        </style>
        <div class="container" id="conteudoView">
            <?php
            $control->controlSwitch('home');
            ?>
        </div>
        <br>
        <br>
        <br>
        <script type="text/javascript" src="/../../../../public/js/signo.js"></script>
        <?php
    }

    public function viewGrid()
    {

        $cols = array('CNPJ', 'Razão Social', 'Nome Fantasia', 'Cidade');
        $colsPesquisaNome = array('CNPJ', 'Razão Social', 'Cidade', 'Nome Fantasia');
        //$colsPesquisaNome = array('ID','CNPJ','RAZAOSOCIAL','FANTASIA','CEP','CIDADE','BAIRRO','LOGRADOURO','NUMERO','COMPLEMENTO','ESTADO','NUMEROMEDICOS','NUMEROPACIENTES','PACIENTESDIA','FKUSER','ATIVO','DATAIN');
        $colsPesquisa = array('cnpj', 'razaoSocial', 'fantasia', 'cidade');
        $array_metodo = array('Id', 'Cnpj', 'RazaoSocial', 'Fantasia', 'Cidade');
        gridView::setGrid_titulo('SIGNO');
        gridView::setGrid_sub_titulo('Sistema Gerenciador de Negócios');
        gridView::setGrid_cols($cols);
        gridView::setGrid_editar('1');
        gridView::setGrid_visualizar("0");
        gridView::setGrid_pesquisar_nome($colsPesquisaNome);
        gridView::setGrid_pesquisar_cols($colsPesquisa);
        gridView::setGrid_objeto('PaEmpresa');
        gridView::setGrid_metodo('listObjFiltrado');
        gridView::setGrid_array_metodo($array_metodo);
        gridView::renderGrid();
    }

}