<?php

require_once('lib/appConexao.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/produtoModel.php');
require_once('app/model/rankProdutoModel.php');
require_once('app/model/trimestreModel.php');

class adminController extends appController {
    
    private $produto;
    private $rankProduto;
    private $trimestre;
    
    private $fileUpload;
    
    public function __construct() {
        $this->produto     = new produtoModel();
        $this->rankProduto = new rankProdutoModel();
        $this->trimestre   = new trimestreModel();
        
        $this->fileUpload = 'C:\\wwwroot\\sistemas\\PrescriptionShareOnline\\public\\upload\\';
    }
    
    public function main() {
        $this->render('admin/adminHomeView');
    }
    
    public function produto() {
        $this->set('listaProduto', $this->produto->listar());
        $this->render('admin/produto/produtoView');
    }
    
    public function produtoForm() {
        $this->set('dado', $this->produto);
        $this->render('admin/produto/produtoForm');
    }    
    
    public function produtoExcluir($mercado='',$produto='') {
        $this->produto->setMercado(base64_decode($mercado));
        $this->produto->setProduto(base64_decode($produto));
        $this->produto->excluir();
        header('Location: '.APP_CAMINHO.'admin/produto');
    }
    
    public function produtoImportarForm() {
        $this->render('admin/produto/produtoImportarForm');
    }
    
    public function produtoImportar() {
        $arquivo = (!empty($_FILES['arqImportar']['name'])) ? $_FILES['arqImportar'] : '' ;
        
        if($arquivo != "") {
        
            if(move_uploaded_file($arquivo['tmp_name'], $this->fileUpload.$arquivo['name'])) {

                $validar = $this->validarArquivoCSV($arquivo['name'], $this->produto->getLayout());
                if($validar == '') { 

                    $array = $this->csvToArray($arquivo['name']);
                    $this->produto->limparTabela();

                    foreach($array as $row) {
                        $this->produto->setMercado($row['MERCADO']);
                        $this->produto->setProduto($row['PRODUTO_SGP']);
                        $this->produto->salvar();
                    }

                    unlink($this->fileUpload.$arquivo['name']);
                    header('Location: '.APP_CAMINHO.'admin/produto');
                    
                } else {
                    $this->set('erro', $validar);
                    $this->render('admin/produto/produtoImportarForm');
                }
            }
        } else {
            $this->set('erro', 'Arquivo Inválido!');
            $this->render('admin/produto/produtoImportarForm');
        }
    }
    
    public function produtoSalvar() {
        $mercado = strtoupper($_POST['txtMercado']);
        $produto = strtoupper($_POST['txtProduto']);
        
        $this->produto->setMercado($mercado);
        $this->produto->setProduto($produto);
        $retorno = $this->produto->salvar();
        
        if($retorno != "") {
            
            $this->set('erro', $retorno);
            $this->set('dado', $this->produto);
            $this->render('admin/produto/produtoForm');
            
        } else {
            header('Location: '.APP_CAMINHO.'admin/produto');
        }
    }
    
    
    
    public function rankProduto() {
        $this->set('listaProduto', $this->rankProduto->listar());
        $this->render('admin/rankProduto/rankProdutoView');
    }
    
    public function rankProdutoForm($id='') {
        $this->rankProduto->setId(base64_decode($id));
        $this->rankProduto->selecionar();
        
        $this->set('dado', $this->rankProduto);
        $this->render('admin/rankProduto/rankProdutoForm');
    }    
    
    public function rankProdutoSalvar() {
        $id      = (isset($_POST['txtId'])) ? base64_decode($_POST['txtId']) :  '' ;
        $linha   = $_POST['txtLinha'];
        $ordem   = $_POST['txtOrdem'];
        $mercado = strtoupper($_POST['txtMercado']);
        $produto = strtoupper($_POST['txtProduto']);
        
        $this->rankProduto->setId($id);
        $this->rankProduto->setLinha($linha);
        $this->rankProduto->setOrdem($ordem);
        $this->rankProduto->setMercado($mercado);
        $this->rankProduto->setProduto($produto);
        
        $retorno = $this->rankProduto->salvar();
        
        if($retorno != "") {
            $this->set('erro', $retorno);
            $this->set('dado', $this->produto);
            $this->render('admin/rankProduto/rankProdutoForm');
            
        } else {
            
            header('Location: '.APP_CAMINHO.'admin/rankProduto');
        }
    }
    
    public function rankProdutoExcluir($id='') {
        $this->rankProduto->setId(base64_decode($id));
        $this->rankProduto->excluir();
        header('Location: '.APP_CAMINHO.'admin/rankProduto');
    }
    
    public function rankProdutoImportarForm() {
        $this->render('admin/rankProduto/rankProdutoImportarForm');
    }
    
    public function rankProdutoImportar() {
        $arquivo = (!empty($_FILES['arqImportar']['name'])) ? $_FILES['arqImportar'] : '' ;
        
        if($arquivo != "") {
        
            if(move_uploaded_file($arquivo['tmp_name'], $this->fileUpload.$arquivo['name'])) {

                $validar = $this->validarArquivoCSV($arquivo['name'], $this->rankProduto->getLayout());
                if($validar == '') { 

                    $array = $this->csvToArray($arquivo['name']);
                    $this->rankProduto->limparTabela();

                    foreach($array as $row) {
                        $this->rankProduto->setId('');
                        $this->rankProduto->setLinha($row['LINHA']);
                        $this->rankProduto->setOrdem($row['ORDEM']);
                        $this->rankProduto->setMercado($row['MERCADO']);
                        $this->rankProduto->setProduto($row['PRODUTO']);
                        $this->rankProduto->salvar();
                    }

                    unlink($this->fileUpload.$arquivo['name']);
                    header('Location: '.APP_CAMINHO.'admin/rankProduto');
                    
                } else {
                    $this->set('erro', $validar);
                    $this->render('admin/rankProduto/rankProdutoImportarForm');
                }
            }
        } else {
            $this->set('erro', 'Arquivo Inválido!');
            $this->render('admin/rankProduto/rankProdutoImportarForm');
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    public function trimestre() {
        $this->set('listaTrimestre', $this->trimestre->listar());
        $this->render('admin/trimestre/trimestreView');
    }
    
    public function trimestreForm($id='') {
        $this->trimestre->setId(base64_decode($id));
        $this->trimestre->selecionar();
        
        $this->set('dado', $this->trimestre);
        $this->render('admin/trimestre/trimestreForm');
    }    
    
    public function trimestreSalvar() {
        $id        = (isset($_POST['txtId'])) ? base64_decode($_POST['txtId']) :  '';
        $codigo    = $_POST['txtCodigo'];
        $trimestre = $_POST['txtTrimestre'];

        
        $this->trimestre->setId($id);
        $this->trimestre->setCodigo($codigo);
        $this->trimestre->setTrimestre($trimestre);
        
        $retorno = $this->trimestre->salvar();
        
        if($retorno != "") {
            $this->set('erro', $retorno);
            $this->set('dado', $this->trimestre);
            $this->render('admin/trimestre/trimestreForm');
            
        } else {
            
            header('Location: '.APP_CAMINHO.'admin/trimestre');
        }
    }
    
    public function trimestreExcluir($id='') {
        $this->trimestre->setId(base64_decode($id));
        $this->trimestre->excluir();
        header('Location: '.APP_CAMINHO.'admin/trimestre');
    }
    
    public function trimestreImportarForm() {
        $this->render('admin/trimestre/trimestreImportarForm');
    }
    
    public function trimestreImportar() {
        $arquivo = (!empty($_FILES['arqImportar']['name'])) ? $_FILES['arqImportar'] : '' ;
        
        if($arquivo != "") {
        
            if(move_uploaded_file($arquivo['tmp_name'], $this->fileUpload.$arquivo['name'])) {

                $validar = $this->validarArquivoCSV($arquivo['name'], $this->trimestre->getLayout());
                if($validar == '') { 

                    $array = $this->csvToArray($arquivo['name']);
                    $this->trimestre->limparTabela();

                    foreach($array as $row) {
                        $this->trimestre->setId('');
                        $this->trimestre->setCodigo($row['CODIGO']);
                        $this->trimestre->setTrimestre($row['TRIMESTRE']);
                        $this->trimestre->salvar();
                    }

                    unlink($this->fileUpload.$arquivo['name']);
                    header('Location: '.APP_CAMINHO.'admin/trimestre');
                    
                } else {
                    $this->set('erro', $validar);
                    $this->render('admin/trimestre/trimestreImportarForm');
                }
            }
        } else {
            $this->set('erro', 'Arquivo Inválido!');
            $this->render('admin/trimestre/trimestreImportarForm');
        }
    }
    
    
    
    
    
    
    
    public function validarArquivoCSV($arquivo='', $layout='') {
        $erro = '';
        
        $ext = explode(".", $arquivo);

        
        if(strtolower($ext[(count($ext)-1)]) != "csv") {
            unlink($this->fileUpload.$arquivo);
            $erro = 'Arquivo Inválido! Extensão não permitida!';
            return $erro;
            exit();
        }
        
        $arq = $this->fileUpload.$arquivo;
        $csv = array_map('str_getcsv', file($arq));
        $header = array_shift($csv);
        $header = explode(';', $header[0]);
        
        
        if(count($header) != count($layout)) {
            unlink($this->fileUpload.$arquivo);
            $erro = 'Arquivo Inválido! O número de campos não coincidem!';
            return $erro;
            exit();
        }
        
        for($i=0;$i<count($header);$i++) {
            
            if($header[$i] != $layout[$i]) {
                unlink($this->fileUpload.$arquivo);
                return 'Arquivo Inválido! Os campos não são iguais aos do layout!';
                exit();
            }
        }
        
        return '';
    }
    
    public function csvToArray($fileCSV) {
        $arq = $this->fileUpload.$fileCSV;

        $csv = array_map('str_getcsv', file($arq));
        $header = array_shift($csv);
        $header = explode(';', $header[0]);
        
        $a = array();
        foreach($csv as $i => $row) {
          $row = explode(';', $row[0]);
          
          for($i=0;$i<count($header);$i++) {
              $arr[$header[$i]] = $row[$i];
          }
          
          $a[] = $arr;
          
        }
        
        return $a;
        
        
        
        
        
        
      }
      
      
      
   
      
    public function sql() {
        $this->render('admin/sqlView');
    }
    
    public function sqlExecute() {
        $query = $_POST['txtExecute'];
        $cn = new appConexao();
        
        $rs = $cn->executarQueryArray($query);
        
        if($rs) {
            
            $html = '<table class="table table-striped table-condensed" style="font-size:11px">';
            $html .= '<tr>';
            $html .= '<th></th>';
            
            $colunas = array_keys($rs[1]);
            $arrCol = array();
            
            foreach ($colunas as $col) {
                if (!is_numeric($col)) {
                    
                    $html .=  '<th>'.$col.'</th>';
                    $arrCol[] = $col;
                }
            }
            
            $html .= '</th>';
            
            
            foreach($rs as $i => $row) {
                $html .= '<tr>';
                $html .=  '<td><b>'.($i).'</b></td>';
                foreach($arrCol as $c) {
                    $html .=  '<td>'.$row[$c].'</td>';
                }
                $html .= '</tr>';
            }
            
            $html .= '</table>';
            
            echo $html;
        }
        
    }
      
    
}