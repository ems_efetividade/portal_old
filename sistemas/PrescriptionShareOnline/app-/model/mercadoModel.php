<?php

require_once('lib/appConexao.php');

class mercadoModel extends appConexao {

	private $idLinha;
	private $mercado;
	private $produto;
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;	
	}
	
	public function setMercado($valor) {
		$this->mercado = $valor;	
	}
	
	public function getIdLinha() {
		return $this->idLinha;	
	}
	
	public function getMercado() {
		return $this->mercado;	
	}
	
	public function shareProduto($idSetor) {
		return $this->executarQueryArray("EXEC proc_ps_graficoShareProduto '".$idSetor."', '".$this->idLinha."', '".$this->mercado."'");
	}
	
	public function principaisProdutos() {
		$rs = $this->executarQueryArray("SELECT * FROM PS_RANK_PRODUTO WHERE ORDEM <= 4 AND ID_LINHA = ".$this->idLinha." AND MERCADO = '".$this->mercado."' ORDER BY ORDEM ASC");	
		
		$arr = array();
		foreach($rs as $produto) {
			$arr[] = $produto['PRODUTO'];
		}
		
		return $arr;
	}
	
	public function listar() {
		$rs = $this->executarQueryArray("SELECT
                                                        PS_PX.MERCADO
                                                FROM
                                                        PS_PX
                                                INNER JOIN
                                                        SETOR ON SETOR.ID_SETOR = PS_PX.ID_SETOR
                                                WHERE 
                                                        SETOR.ID_LINHA = ".$this->idLinha."
                                                GROUP BY
                                                        MERCADO
                                                ORDER BY
                                                        MERCADO ASC");
                
                
                
		$arr = array();
		foreach($rs as $registro) {
			$mercado = new mercadoModel();
			$mercado->setIdLinha($this->idLinha);
			$mercado->setMercado($registro['MERCADO']);
			
			$arr[] = $mercado;
		}
		
		return $arr;
		
	}
	
	public function combo($selecionar='') {
		$rs = $this->executarQueryArray("SELECT
											PS_PX.MERCADO
										FROM
											PS_PX
										INNER JOIN
											SETOR ON SETOR.ID_SETOR = PS_PX.ID_SETOR
										WHERE 
											SETOR.ID_LINHA = ".$this->idLinha."
										GROUP BY
											MERCADO
										ORDER BY
											MERCADO ASC");	
											
		$option = '';
									
		foreach($rs as $registro) {
			$select = ($registro['MERCADO'] == $selecionar) ? 'selected="selected"' : '';
			
			$option .= '<option value="'.$registro['MERCADO'].'" '.$select.'>'.$registro['MERCADO'].'</option>';	
		}
		
		return $option;
	}
	
	
	public function listarLinhas($mercado) {
		$rs = $this->executarQueryArray("SELECT
										LINHA.NOME
									FROM 
										LINHA 
									INNER JOIN 
										PS_MERCADO_LINHA_PX P ON 
										P.ID_LINHA = LINHA.ID_LINHA
									WHERE 
										MERCADO = '".$mercado."'
									ORDER BY LINHA.NOME ASC");
		
		return $rs;
					
	}

}