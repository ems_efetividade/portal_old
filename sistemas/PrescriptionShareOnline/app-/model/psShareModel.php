<?php

require_once('lib/appConexao.php');

class psShareModel extends appConexao {
	
	private $idSetor;
	private $idLinha;
	private $mercado;
	
	
	public function setIdSetor($valor) {
		$this->idSetor = $valor;
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;
	}
	
	public function setMercado($valor) {
		$this->mercado = $valor;
	}
	
	public function getTrimestre() {
		return $this->executarQueryArray("SELECT * FROM PS_TRIMESTRE");	
	}
	
	public function getPrescriptionShare($idSetor, $idLinha, $mercado) {
		return $this->executarQueryArray("EXEC proc_ps_shareSetor '".$idSetor."', '".$idLinha."', '".$mercado."'");	
	}
	
	public function getMedicos($mercado, $produto, $periodo, $idSetor) {
		return $this->executarQueryArray("EXEC proc_ps_medicosPorSetor '".$mercado."', 
																	   '".$produto."', 
																	   '".$periodo."', 
																	   ".$idSetor."");	
	}
	
	public function pesquisarMedico($idSetor, $campo, $criterio) {
		return $this->executarQueryArray("EXEC proc_ps_pesquisarMedico ".$idSetor.", '".$campo."', '".$criterio."'");	
	}
	
	public function getDadoMedico($crm, $idLinha) {
		$query = "SELECT
						M.ID_SETOR,
						M.NOME,
						M.ESPECIALIDADE,
						UPPER(M.ENDERECO) AS ENDERECO,
						UPPER(M.NRO) AS NRO,
						UPPER(M.COMPLEMENTO) AS COMPLEMENTO,
						UPPER(M.BAIRRO) AS BAIRRO,
						M.CEP,
						M.CIDADE,
						M.UF
					FROM
						PS_MEDICO M
					INNER JOIN
						SETOR S ON S.ID_SETOR = M.ID_SETOR	
					WHERE
						M.CRM = '".$crm."' AND
						S.ID_LINHA = ".$idLinha."
					GROUP BY
						M.ID_SETOR,
						M.NOME,
						M.ESPECIALIDADE,
						M.ENDERECO,
						M.NRO,
						M.COMPLEMENTO,
						M.BAIRRO,
						M.CEP,
						M.CIDADE,
						M.UF";	
		
		return $this->executarQueryArray($query);	
	}
	
	public function getEnderecoMedico($crm) {
		$query = "SELECT
						ID_SETOR,
						ESPECIALIDADE,
						UPPER(ENDERECO) AS ENDERECO,
						UPPER(NRO) AS NRO,
						UPPER(COMPLEMENTO) AS COMPLEMENTO,
						UPPER(BAIRRO) AS BAIRRO,
						CEP,
						CIDADE,
						UF
					FROM
						PS_MEDICO
					WHERE
						CRM = '".$crm."'
					GROUP BY
						ID_SETOR,
						ESPECIALIDADE,
						ENDERECO,
						NRO,
						COMPLEMENTO,
						BAIRRO,
						CEP,
						CIDADE,
						UF";
		return $this->executarQueryArray($query);	
	}
	
	
	public function getMercadoPxMedico($crm, $idLinha) {
		$query = "SELECT
					  M.MERCADO,
					  M.PRODUTO,
					  SUM(M.TRM00) AS PX,
					  SUM(M.TRM01) AS PX_ANT
					  
					FROM
					  PS_MEDICO M
					INNER JOIN
						SETOR S ON S.ID_SETOR = M.ID_SETOR
					WHERE
					  M.CRM = '".$crm."' AND
					  S.ID_LINHA = ".$idLinha."
					GROUP BY
					  M.MERCADO,
					  M.PRODUTO
					ORDER BY
					  M.MERCADO,
					  SUM(M.TRM00) DESC";
		return $this->executarQueryArray($query);	
	}
	
	public function getCatMercadoMedico($crm, $mercado) {
		$query = "SELECT * FROM PS_CATEGORIA_MERCADO WHERE CRM = '".$crm."' AND MERCADO = '".$mercado."'";
		return $this->executarQueryArray($query);
	}
	
	public function getGraficoDrill($idSetor, $mercado) {
		return $this->executarQueryArray("exec proc_ps_graficoDrillDown ".$idSetor.", '".$mercado."'");	
	}
	
	public function getConquistaBCG($idSetor, $mercado, $produto) {
		return $this->executarQueryArray("EXEC proc_ps_conquistaBCG ".$idSetor.", '".$mercado."', '".$produto."'");
	}
	
	
}

?>