<?php

require_once('lib/appConexao.php');

class rankProdutoModel extends appConexao {
    private $id;
    private $linha;
    private $mercado;
    private $produto;
    private $ordem;
    private $idLinha;
    
    private $layout;
    
    public function __construct() {
        parent::__construct();
        
        $this->linha   = 0;
        $this->mercado = '';
        $this->produto = '';
        $this->ordem   = 0;
        $this->idLinha = 0;
        
        $this->layout = array('LINHA', 'MERCADO', 'PRODUTO', 'ORDEM');
    }
    
    public function setId($id) {
        $this->id = $id;
    }
    
    public function setLinha($linha) {
        $this->linha = $linha;
    }

    public function setMercado($mercado) {
        $this->mercado = $mercado;
    }

    public function setProduto($produto) {
        $this->produto = $produto;
    }

    public function setOrdem($ordem) {
        $this->ordem = $ordem;
    }

    public function setIdLinha($idLinha) {
        $this->idLinha = $idLinha;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getLinha() {
        return $this->linha;
    }

    public function getMercado() {
        return $this->mercado;
    }

    public function getProduto() {
        return $this->produto;
    }

    public function getOrdem() {
        return $this->ordem;
    }

    public function getIdLinha() {
        return $this->idLinha;
    }
    
    public function getLayout() {
        return $this->layout;
    }

    public function salvar() {
        
        if($this->id == '') {
        
        $query = "INSERT INTO [PS_RANK_PRODUTO]
                    ([LINHA]
                    ,[MERCADO]
                    ,[PRODUTO]
                    ,[ORDEM]
                    ,[ID_LINHA])
              VALUES
                    ('".$this->linha."'
                    ,'".$this->mercado."'
                    ,'".$this->produto."'
                    ,".$this->ordem."
                    ,".$this->getIdLinha_($this->linha).")";
        
        } else {
            $query = "UPDATE [PS_RANK_PRODUTO] SET LINHA = '".$this->linha."', MERCADO = '".$this->mercado."', PRODUTO = '".$this->produto."', ORDEM = ".$this->ordem.", ID_LINHA = ".$this->getIdLinha_($this->linha)." WHERE LINHA + MERCADO + PRODUTO + CONVERT(VARCHAR, ORDEM) + CONVERT(VARCHAR, ID_LINHA) = '".$this->id."'";
        }
        
        
        $validar = $this->validar();
        
        if($validar == '') {
            $this->executar($query);
        } else {
            return $validar;
        }
    }
    
    private function validar() {
        $erro = '';
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS T FROM PS_PRODUTO_SGP WHERE LINHA + MERCADO + PRODUTO + CONVERT(VARCHAR, ORDEM) + CONVERT(VARCHAR, ID_LINHA) = '".$this->linha.$this->mercado.$this->produto.$this->ordem.$this->idLinha."'");
        
        if($rs[1]['T'] > 0) {
            $erro =  'Já existe um mercado e um produto com esse nome!';
        }
        
        return $erro;
    }
    
    
    public function excluir() {
        $this->executar("DELETE FROM PS_RANK_PRODUTO WHERE LINHA + MERCADO + PRODUTO + CONVERT(VARCHAR, ORDEM) + CONVERT(VARCHAR, ID_LINHA) = '".$this->id."'");
    }
    
    public function limparTabela() {
        $this->executar("DELETE FROM PS_RANK_PRODUTO");
    }
    
    private function getIdLinha_($idLinha) {
        $rs = $this->executarQueryArray("SELECT ID_LINHA FROM LINHA WHERE CODIGO = '".$idLinha."'");
        return $rs[1]['ID_LINHA'];
    }
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM PS_RANK_PRODUTO WHERE LINHA + MERCADO + PRODUTO + CONVERT(VARCHAR, ORDEM) + CONVERT(VARCHAR, ID_LINHA) = '".$this->id."'");
        $this->popular($rs[1]);
    }
    
    public function listar() {
        $arr = array();
        $rs = $this->executarQueryArray("SELECT * FROM PS_RANK_PRODUTO ORDER BY LINHA, MERCADO, ORDEM ASC");
        if($rs) {
            foreach($rs as $row) {
                $a = new rankProdutoModel();
                $a->popular($row);
                
                $arr[] = $a;
            }
        }
        return $arr;
    }
    
    protected function popular($row) {
        $this->linha   = $row['LINHA'];
        $this->mercado = $row['MERCADO'];
        $this->produto = $row['PRODUTO'];
        $this->ordem   = $row['ORDEM'];
        $this->idLinha = $row['ID_LINHA'];
        $this->id      = $this->linha.$this->mercado.$this->produto.$this->ordem.$this->idLinha;
    }
    
}