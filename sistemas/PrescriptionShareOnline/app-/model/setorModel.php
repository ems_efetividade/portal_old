<?php

require_once('lib/appConexao.php');

class setorModel extends appConexao {

	private $idSetor;
	private $idLinha;
	private $idPerfil;
	private $nivel;
	private $setor;
	private $nome;
	
	private $equipe;
	
	
	public function setIdSetor($valor) {
		$this->idSetor = $valor;
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;	
	}
	
	public function setNivel($valor) {
		$this->nivel = $valor;	
	}
	
	public function setSetor($valor) {
		$this->setor = $valor;	
	}
	
	public function setIdPerfil($valor) {
		$this->idPerfil = $valor;	
	}
	
	
	public function getIdSetor() {
		return $this->idSetor;
	}
	
	public function getIdLinha() {
		return $this->idLinha;	
	}
	
	public function getNivel() {
		return $this->nivel;	
	}
	
	public function getSetor() {
		return $this->setor;	
	}
	
	public function getNome() {
		$exp = explode(" ", trim($this->nome));
		
		return $exp[0].' '.$exp[count($exp)-1];	
	}
	
	public function getIdPerfil() {
		return $this->idPerfil;	
	}
	
	public function getEquipe() {
		return $this->equipe;	
	}
	
	private function popularVariaveis($rs) {
		$this->idLinha  = $rs['ID_LINHA'];
		$this->idSetor  = $rs['ID_SETOR'];
		$this->idPerfil = $rs['ID_PERFIL'];
		$this->setor    = $rs['SETOR'];
		$this->nivel    = $rs['NIVEL'];
		$this->nome     = $rs['COLABORADOR'];
	}
	
	public function selecionar() {
		$rs = $this->executarQueryArray("SELECT SETOR.*, VW.NOME AS COLABORADOR FROM SETOR INNER JOIN VW_COLABORADORSETOR VW ON VW.ID_SETOR = SETOR.ID_SETOR WHERE SETOR.ID_SETOR = ".$this->idSetor);	
		
		$rsEquipe = $this->executarQueryArray("SELECT SETOR.*, VW.NOME AS COLABORADOR FROM SETOR INNER JOIN VW_COLABORADORSETOR VW ON VW.ID_SETOR = SETOR.ID_SETOR WHERE SETOR.NIVEL = ".$this->idSetor." ORDER BY SETOR.SETOR ASC");
		
		
		if(count($rs) > 0) {
			$dados = $rs[1];
			$this->popularVariaveis($dados);
		}
		
		
		if(count($rsEquipe) > 0) {
			
			$this->equipe = array();
			foreach($rsEquipe as $equipe) {
			
				$setor = new setorModel();
				$setor->popularVariaveis($equipe);	
				
				$this->equipe[] = $setor;
			}
			
		}
		
	}
	
	public function listarHierarquia() {
		$query = "WITH G AS (
						SELECT ID_SETOR, SETOR, NIVEL FROM SETOR WHERE ID_SETOR = ".$this->idSetor."
						
						UNION ALL
						
						SELECT P.ID_SETOR, P.SETOR, P.NIVEL FROM SETOR P INNER JOIN G ON G.NIVEL = P.ID_SETOR
						WHERE P.NIVEL <> 0 AND P.ID_SETOR <> ".$this->idSetor."
					)
					
					SELECT * FROM G WHERE ID_SETOR <> ".$this->idSetor." ORDER BY SETOR ASC";	
			
		$rsHier = $this->executarQueryArray($query);
		
		if(count($rsHier) > 0) {
		$hier = array();
			foreach($rsHier as $equipe) {
			
				$setor = new setorModel();
				$setor->setIdSetor($equipe['ID_SETOR']);
				$setor->selecionar();
				
				$hier[] = $setor;
			}
		
		return $hier;
		}
	}
	
	
	
	

}


?>