<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Tabela PRODUTO_SGsP</b></h3>
            <hr>
            
            
            <div class="row">
                <div class="col-lg-12">
                    
                    <form method="Post" action="<?php echo APP_CAMINHO ?>admin/rankProdutoSalvar">
                        
                        <input type="hidden" name="txtId" value="<?php echo base64_encode($dado->getId()); ?>" />
                        
                        <div class="form-group">
                          <label for="exampleInputEmail1">Número Início Linha</label>
                          <input type="text" class="form-control" name="txtLinha" id="exampleInputEmail1" placeholder="" value="<?php echo $dado->getLinha() ?>">
                        </div>
                        
                        <div class="form-group">
                          <label for="exampleInputEmail1">Mercado</label>
                          <input type="text" class="form-control" name="txtMercado" id="exampleInputEmail1" placeholder="" value="<?php echo $dado->getMercado() ?>">
                        </div>
                        
                        <div class="form-group">
                          <label for="exampleInputPassword1">Produto</label>
                          <input type="text" class="form-control" name="txtProduto" id="exampleInputPassword1" placeholder="" value="<?php echo $dado->getProduto() ?>">
                        </div>
                        
                        <div class="form-group">
                          <label for="exampleInputPassword1">Ordem</label>
                          <input type="text" class="form-control" name="txtOrdem" id="exampleInputPassword1" placeholder="" value="<?php echo $dado->getOrdem() ?>">
                        </div>                        
                        
                        <h4 class="text-danger"><?php echo $erro ?></h4>
                        
                        <button type="submit" class="btn btn-default">Salvar</button>
                        <a href="<?php echo APP_CAMINHO ?>admin/rankProduto" class="btn btn-default">Voltar</a>
                    </form>
                    
                </div>
            </div>
            
            
            
            
        </div>
    </div>
</div>