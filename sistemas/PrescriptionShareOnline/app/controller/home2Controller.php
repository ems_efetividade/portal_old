<?php

require_once('lib/appConexao.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/mercadoModel.php');
require_once('app/model/setorModel.php');
require_once('app/model/psShareModel.php');

class home2Controller extends appController {

    private $tmpIdSetor = null;
    private $tmpCodLinha = null;
    
    public function homeController() {
		$this->tmpIdSetor  = appFunction::dadoSessao('id_setor');
		$this->tmpCodLinha = appFunction::dadoSessao('id_linha');
    }
    
    public function main() {
        $this->abrir();
    }

    public function abrir() {
        $this->render('homeView.2.php');
    }

}