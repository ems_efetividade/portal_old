<?php

require_once('lib/appConexao.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('app/model/mercadoModel.php');
require_once('app/model/setorModel.php');
require_once('app/model/psShareModel.php');

class homeController extends appController {
	
	private $conexao = null;
	private $funcao = null;
	private $mercado = null;
	private $setor = null;
	private $psShare = array();
	
	private $arrProduto = null;
	private $periodo = null;
	
	private $tmpIdSetor = null;
	private $tmpCodLinha = null;
	
	
	private $maxProd  = APP_MAX_PROD;
	
	private $valorProdEMSBrasil;
	
	public function homeController() {
		$this->funcao	   = new appFunction();
		$this->conexao     = new appConexao();
		$this->mercado     = new mercadoModel();
		$this->setor       = new setorModel();
		$this->psShare     = new psShareModel();
		
		$this->tmpIdSetor  = appFunction::dadoSessao('id_setor');
		$this->tmpCodLinha = appFunction::dadoSessao('id_linha');
		
		//$this->tmpIdSetor = 576;
		//$this->tmpCodLinha = 2;
		
		$rs = $this->psShare->getTrimestre();
		$chave = array_keys($rs);
		
		for($i=1;$i<=count($rs);$i++) {
			$this->periodo[$rs[$i]['CODIGO']] = $rs[$i]['TRIMESTRE'];
		}
	}

	public function main() {
            
            if (appFunction::dadoSessao('nivel_admin') == 0) {
				$_POST = appSanitize::filter($_POST);
                $this->mercado->setIdLinha($this->tmpCodLinha);
				$mercado = $this->mercado->listar();
				
			
                $_POST['mercado'] = $mercado[0]->getMercado();
                $this->selecionar();
            } else {
                header('Location: '.APP_CAMINHO.'/admin');
            }

	}
	
	public function selecionar() {
		$_POST = appSanitize::filter($_POST);
		
		$mercado = $_POST['mercado'];
		$concs = $_POST['concorrente'];
		$conc = $concs;


		//print_r($conc);
		
		$this->mercado->setIdLinha($this->tmpCodLinha);
		$this->mercado->setMercado($mercado);
		$this->mercado->setConcorrentes($concs);
		
		//$rsMerc = $this->psShare->getGraficoDrill2($this->tmpIdSetor,0,$mercado);
		
		$this->valorProdEMSBrasil = $rsMerc[1]['SHARE'];
		
		$princProduto = $this->mercado->principaisProdutos();
		$this->arrProduto = $princProduto;
		$produtoEMS = $princProduto[0];
		//$this->cabecalhoTabela();
		
		$this->graficoShareProdutoEMS($mercado);
		//$this->grafTeste($mercado);
		//$this->graficoDrill($mercado);
		//$this->graficoDrill2($this->tmpIdSetor,$mercado);
		
		$this->set('idSetor', $this->tmpIdSetor);
		
		$this->set('numProdutos', count($this->arrProduto));
		$this->set('trmAnt', $this->periodo['TRM01']);
		$this->set('trmAtu', $this->periodo['TRM00']);
		$this->set('nomeMercado', $mercado);
		$this->set('produtoEMS', $produtoEMS);
		$this->set('arrGraficoShareProduto', $this->graficoShareProduto($this->mercado));
		$this->set('princProdutos', $this->principaisProdutos($this->mercado));
		$this->set('linhaPrincProdutos', $this->cabecalhoProdutos($this->mercado));
		$this->set('linhaBrasil', $rsMerc[1]['SHARE']);
		$this->set('linhaSetor',   $this->dadosPsShare($mercado));
		$this->set('comboMercado', $this->mercado->combo($mercado));
		$this->set('listaMercado', $this->mercado->listar());
		$this->render('homeView.1');	
	}
	
	public function cabecalhoProdutos($mercado) {
		
		$produtos = $mercado->principaisProdutos();
		$produtosMercado = $mercado->getConcorrentes();

		$prods = ($produtosMercado != "") ? explode(",", $produtosMercado) : '';
		

		if($prods != "") {
			array_pop($prods);
			$ems = $produtos[0];
			$produtos = $prods;
			array_unshift($produtos, $ems);
		}

		$tr = '<tr>';
		for($i=0;$i<=2;$i++) {
		
			foreach($produtos as $key => $produto) {
				$indice = $key+1;
				$coluna = ($indice == 1) ? '<b>Prod. '.appFunction::dadoSessao('sigla').'</b>' : 'Conc.: '.($indice-1);
				

				

				$tr .= '<td width="6%">
							<p class="text-center alert alert-info" data-toggle="tooltip" data-placement="top" title="'.$produto.'"><small>'.$coluna.'</small></p>
						</td>';
			}
		}
		$tr .= '</tr>';
		return $tr;
	}
	
	public function dadosPsShare($mercado) {
		$this->setor->setIdSetor($this->tmpIdSetor);
		$this->setor->selecionar();
		
		
		
		
		
		//echo $this->valorProdEMSBrasil;
		

		$gerentes = $this->setor->listarHierarquia();
		
		$arrSetor = array();
		$tabela = '';
		
		// lista os gerentes acima
		if(is_array($gerentes)) {
			foreach($gerentes as $gerente) {
				$dadosPs = $this->psShare->getPrescriptionShare($gerente->getIdSetor(),$gerente->getIdLinha(),$mercado);
				$tabela .= $this->tabelaDados($dadosPs, $gerente, $mercado, 0);
			}
		}
		
		//lista o setor logado
		$dadosPs = $this->psShare->getPrescriptionShare($this->setor->getIdSetor(),$this->setor->getIdLinha(),$mercado);		
		$tabela .= $this->tabelaDados($dadosPs, $this->setor,$mercado, 1, 0);
		
		// lista a equipe dele (se houver)
		$equipe = $this->setor->getEquipe();
		if(is_array($equipe)) {
			foreach($equipe as $setor) {
				$dadosPs = $this->psShare->getPrescriptionShare($setor->getIdSetor(), $setor->getIdLinha(), $mercado);
				$tabela .= $this->tabelaDados($dadosPs, $setor, $mercado, 1, 1);
			}
		}
		
		//print_r($arrSetor);
		return $tabela;
		
		
	}
	
	public function selecionarSetor() {
		$idSetor = $_POST['idSetor'];
		$mercado = $_POST['mercado'];
		
		$this->setor->setIdSetor($idSetor);
		$this->setor->selecionar();
		
		$equipe = $this->setor->getEquipe();
		
		$rsMerc = $this->psShare->getGraficoDrill($idSetor,$mercado);
		$this->valorProdEMSBrasil = $rsMerc[1]['SHARE'];
		
		
		
		$this->mercado->setIdLinha($this->tmpCodLinha);
		$this->mercado->setMercado($mercado);
		$this->arrProduto = $this->mercado->principaisProdutos();

		
		
		$tr = '';
		if(is_array($equipe)) {
			foreach($equipe as $setor) {
				$dadosPs = $this->psShare->getPrescriptionShare($setor->getIdSetor(), $setor->getIdLinha(), $mercado);
				$tr .= $this->tabelaDados($dadosPs, $setor, '', 1, 1);
			}
		}

		

		echo $tr;
	}
	
	public function tabelaDados($dados, $setor, $mercado='', $listaMedico=0, $drill=0) {
		
		
		
		
		$dados = $dados[1];
		
		//$this->mercado->setIdLinha($setor->getIdLinha());
		//$this->mercado->setMercado($mercado);
		
		//$produtos = $this->mercado->principaisProdutos();
		
		//$a = array_keys($dados);
		//for($i=0;$i<=count($a);$i++) {
		//	$dados[$a[$i]] = $this->funcao->formatarMoeda($dados[$a[$i]],2);
		//}
		
		switch($setor->getIdPerfil()) {
			case 4: {
				$clsNivel = "alert-default";
				$bold = 'bold';
				break;	
			}
			
			case 2:{
				$clsNivel = "alert-soft";
				break;	
			}
			
			case 3:{
				$clsNivel = "alert-gray";
				break;	
			}
			
			default: {
				$espaço = '';
				$bold == '';
				$clsNivel = "alert-none";	
			}
		}
		
		//echo $brasil;
		//$clsNivel = ($setor->getIdPerfil() == 4) ? 'alert-default' : 'alert-gray';
		
		$clsAbrir = ($drill == 0) ? '' : 'abrirSetor';
		$clsListaMed = ($listaMedico == 0) ? '' : 'listarMedico';
		$clsConquista = ($listaMedico == 0) ? '' : 'listarConquista';
		
		if($setor->getIdPerfil() == 1) {
			$clsAbrir = '';
		}
		
		$html = '<table width="100%">
				<tr>
					<td width="28%" cod="'.$setor->getIdSetor().'" class="'.$clsAbrir.'">
						<p class="alert '.$clsNivel.'">
							<span style="font-weight:'.$bold.'">'.$setor->getSetor().' '.$setor->getNome().'</span>
							<span class="pull-right loading"><img src="'.APP_CAMINHO.'public/img/loading.gif" /></span>
						</p>
					</td>';
					
					
		$prodEMS = $this->arrProduto[0];
						
		for($i=0;$i<count($this->arrProduto);$i++) {
			$iconeAcimaBrasil = '';
			
			if($this->arrProduto[$i] == $prodEMS) {
				$iconeAcimaBrasil = $this->iconeAcimaBrasil($dados['PROD'.($i+1).'_TRM00']);	
			}
			
			
			$trm01 .= '<td width="6%" class="'.$clsListaMed.'" prod="'.$this->arrProduto[$i].'" per="TRM01" cod="'.$setor->getIdSetor().'">
						<p class="text-center alert '.$clsNivel.'">
							'.$this->funcao->formatarMoeda($dados['PROD'.($i+1).'_TRM01'],0).'%
						</p>
					</td>';
					
			$trm00 .= '<td width="6%" class="'.$clsListaMed.'" prod="'.$this->arrProduto[$i].'" per="TRM00" cod="'.$setor->getIdSetor().'">
						<p class="text-center alert  '.$clsNivel.'">
							<span class="'.$iconeAcimaBrasil.'">
							'.$this->funcao->formatarMoeda($dados['PROD'.($i+1).'_TRM00'],0).'%
							</span>
						</p>
					</td>';
					
			$pen .= '<td width="6%">
						<p class="text-center alert '.$clsNivel.' '.$clsConquista.'" prod="'.$this->arrProduto[$i].'" mercado="'.$mercado.'" cod="'.$setor->getIdSetor().'">
							<span class="'.$this->formatarPenetracao($this->funcao->formatarMoeda($dados['PROD'.($i+1).'_PEN'],0)).'">
								'.$this->funcao->formatarMoeda($dados['PROD'.($i+1).'_PEN'],0).'
							</span>
						</p>
					</td>';
		}
					
					
		$html .= $trm01.$trm00.$pen.'</table><div id="'.$setor->getIdSetor().'"></div>';
				
		return $html;
		
	}
	
	public function iconeAcimaBrasil($valor) {
		//return ($valor > $brasil) ? '<span class="text-success glyphicon glyphicon-star"></span> ' : '';
		return ($valor > $this->valorProdEMSBrasil) ? 'up ' : '';
	}
	
	public function principaisProdutos($mercado) {
		
		$this->mercado = $mercado;

		$produtos = $mercado->principaisProdutos();
		$produtosMercado = $mercado->getListaProdutoMercado();

		

		$tabela = '<table class="table table-condensed">';
		
		foreach($produtos as $i => $produto) {
			
			$combo = '';

			if($i == 0) {
				$class = "text-primary";
				$coluna = 'Produto ' . appFunction::dadoSessao('sigla');
				$combo = $produto;	
			} else {
				$combo = '';
				$class = "";
				$coluna = "Conc.: ".$i;	

				if($mercado->getConcorrentes() != '') {
					$prods = explode(',', $mercado->getConcorrentes());
					$produto = $prods[$i-1];
				}


				$combo = '<select class="form-control input-sm" name="cmbConcorrentes[]" id="cmbConc'.$i.'">';
				foreach($produtosMercado as $pp) {
						$combo .= '<option value="'.$pp.'" '.(($pp == $produto) ? "selected='selected'" : '').'>'.$pp.'</option>';
				}
				$combo .= '</select>';
			}



			$tabela .= '<tr>
							<td class="'.$class.'"><b>'.$coluna.'</b></td>
							<td class="'.$class.'">'.$combo.'</td>
						</tr>';
		}


		$tabela .= '<tr>
						<td colspan="2" ><a href="#" class="cmbMercadoConc" data-val="'.$mercado->getMercado().'">Recalcular</a></td>
					</tr>';

		$tabela .= '</table>';
		
		return $tabela;
		
	}
	
	
	public function listarMedicos() {
            
            
                
		$mercado = $_POST['txtMercado'];
		$produto = $_POST['txtProduto'];
		$periodo = $_POST['txtPeriodo'];
		$idSetor = $_POST['txtIdSetor'];
		
		$tmp = substr($periodo, -2);
		$trmAnt = 'TRM0'. ($tmp + 1);
		$trmAtu = 'TRM'.$tmp;
		
		
		
		$dados = $this->psShare->getMedicos($mercado,$produto,$periodo,$idSetor);
		$tabela = '';
		
		if(count($dados) > 0) {
			foreach($dados as $dado) {
				
				$classe = $this->formatarPenetracao(($dado['PX']-$dado['PX_ANT']));
				
				
				$tabela .= '<tr>
								<td><a href="#" class="fichaMedico" crm="'.$dado['CRM'].'"><b>'.$dado['CRM'].'</b></a> '.$dado['NOME'].'</td>
								<td valign="middle">'.$dado['CIDADE'].'/'.$dado['UF'].'</td>
								<td>'.$dado['ESPECIALIDADE'].'</td>
								<td class="text-center">'.$dado['CAT'].'</td>
								<td class="text-center">'.$dado['PX_ANT'].'</td>
								<td class="text-center">'.$dado['PX'].'</td>
								<td><span class="'.$classe.'">'.($dado['PX']-$dado['PX_ANT']).'</span></td>
							</tr>';	
			}
			
			echo '<div><h3 style="margin-top:5px"><strong>Prescritores de '.$produto.' no '.$this->periodo[$periodo].'</strong></h3>
					<h5><small>'.number_format(count($dados),0, ",", ".").' médico(s) encontrado(s) - *Ranking: '.$this->periodo['TRM00'].'</small></h5>
					</div>
					<hr>
				<h5>
					<small>
						<table class="table table-striped">
							<tr>
								<th>MEDICO</th>
								<th>CIDADE</th>
								<th>ESPECIALIDADE</th>
								<th class="text-center">CAT</th>
								<th class="text-center">'.$this->periodo[$trmAnt].'</th>
								<th class="text-center">*'.$this->periodo[$trmAtu].'</th>
								<th class="text-center">DIF</th>
							</tr>
							'.$tabela.'
						</table>
					</small>
				 </h5>';
			
		}
	}
	
	
	public function exportarMedicos() {
		$mercado = $_POST['txtMercado'];
		$produto = $_POST['txtProduto'];
		$periodo = $_POST['txtPeriodo'];
		$idSetor = $_POST['txtIdSetor'];
		
		$this->setor->setIdSetor($idSetor);
		$this->setor->selecionar();
		
		$dados = $this->psShare->getMedicos($mercado,$produto,$periodo,$idSetor);
		$colunas = array_keys($dados[1]);
		
                foreach ($colunas as $key => $value) {
                    if (is_int($value)) {
                        unset($colunas[$key]);
                    }
                }
                
		//$colunas = array_replace($colunas, 'PX_ANT', $this->periodo['TRM01']);
		
		for($i=0;$i<count($colunas);$i++) {
			
			$colunas[$i] = ($colunas[$i] == 'PX_ANT') ? $this->periodo['TRM01'] : $colunas[$i];
			$colunas[$i] = ($colunas[$i] == 'PX') ? $this->periodo['TRM00'] : $colunas[$i];
				
		}
		
		
		$nomeArquivo = strtolower($this->setor->getSetor().'_medicos_'.str_replace(' ', '_', $mercado)).'.csv';

	   	$df = fopen(ROOT.'temp\\'.$nomeArquivo, 'w');
		$arrTitulo[1] = utf8_decode('Médicos Prescritores do Mercado de '.$mercado.' e do produto '.$produto);
		
		fputcsv($df, $arrTitulo, ';');
	   	fputcsv($df, $colunas, ';');
	   	foreach ($dados as $row) {
                    $a = [];
                    for($i=0;$i<=count($row);$i++) {
                        $a[$i] = $row[$i];
                    }
                    
		  fputcsv($df, $a, ';');
		 // fputcsv($df, $row, ';');
	   	}
	   	fclose($df);
		
	   	header('Content-Description: File Transfer');
		header('Content-Disposition: attachment; filename="'.$nomeArquivo.'"');
		header('Content-Type: application/octet-stream');
		header('Content-Transfer-Encoding: binary');
		header('Content-Length: ' . filesize(ROOT.'temp\\'.$nomeArquivo));
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Expires: 0');
		
		// Envia o arquivo para o cliente
		readfile(ROOT.'temp\\'.$nomeArquivo);
		
		unlink(ROOT.'temp\\'.$nomeArquivo);
	}
	
	
	public function formatarPenetracao($valor) {
		
		if($valor == 0) {
			return 'middle';	
		} else {
			if($valor > 0) {
				return 'up';	
			} else {
				return 'down';
			}
		}
	}
	
	
	
	public function pesquisarMedico() {
		$idSetor = $this->tmpIdSetor;	
		$campo = $_POST['campo'];
		$criterio = $_POST['criterio'];
		
		$registros =  $this->psShare->pesquisarMedico($idSetor, $campo, $criterio);
		
		$tabela = '<p>Resultado da pesquisa por <b>"'.$criterio.'"</b> - '.number_format(count($registros),0, ",", ".").' médico(s) encontrado(s).</p><h5><small>
					
					<table class="table table-striped table-condensed">';
		if(count($registros) > 0) {

			foreach($registros as $registro) {
				
				$tabela .= '<tr> 
								<td><a href="#" class="fichaMedico" crm="'.$registro['CRM'].'"><b>'.$registro['CRM'].'</b></a>&nbsp;&nbsp;'.$registro['NOME'].'</td>
								<td>'.$registro['CIDADE'].'</td>
								<td>'.$registro['UF'].'</td>
								<td>'.$registro['ESPECIALIDADE'].'</td>
								<td>
									<button class="btn btn-default btn-sm fichaMedico" crm="'.$registro['CRM'].'" type="button">
										<span class="glyphicon glyphicon-search"></span>
									</button>
								</td>
							</tr>';
			}
				
		}
		$tabela .= '</table></small></h5>';
		
		echo $tabela;
	}
	
	public function fichaMedico() {
		$crm = $_POST['CRM'];
		
		$rsMedico = $this->psShare->getDadoMedico($crm, $this->tmpCodLinha);		
		
		$html = '<div class="row">
		
					<div class="col-lg-2">
						<img src="'.APP_CAMINHO.'public/img/steto.png" class="img-responsive" />
					</div>
					
					<div class="col-lg-10">
						<h3><strong>'.$rsMedico[1]['NOME'].'</strong> <small> ('.$crm.')</small></h3> 
						<h5>
							<small>
								<p><b><span class="glyphicon glyphicon-map-marker"></span> ENDEREÇO: </b>'.$rsMedico[1]['ENDERECO'].', '.$rsMedico[1]['NRO'].' -'.$rsMedico[1]['COMPLEMENTO'].'</p>
								
								<p><b><span class="glyphicon glyphicon-map-marker"></span> BAIRRO: </b>'.$rsMedico[1]['BAIRRO'].'</p>
								<p><b><span class="glyphicon glyphicon-map-marker"></span> CIDADE: </b> '.$rsMedico[1]['CIDADE'].' / '.$rsMedico[1]['UF'].'</p>
								
								<p><b><span class="glyphicon glyphicon-credit-card"></span> ESPECIALIDADE: </b> '.$rsMedico[1]['ESPECIALIDADE'].'</p>
							</small>
						</h5>
					</div>
				</div>
				
				<hr />';

			$html .= '<h4>
						<strong>
							<span class="glyphicon glyphicon-map-marker"></span> Outros Endereços
						</strong>
					 </h4>
			
					<h5>
						<small>'.$this->tabelaEnderecoMedico($crm).'</small>
					</h5>';

			$html .= '<h4>
						<strong>
							<span class="glyphicon glyphicon-edit"></span> Mercados e Prescrições
						</strong>
					</h4>
					<h5><small>';
				
			$html .= '<div class="row">
						<div class="col-lg-12">
							'.$this->tabelaMercadoFichaMedico($crm).'
						</div>
					  </div>
				
				
				
					<!--<form id="formPDF" action="'.APP_CAMINHO.'home/pdf" method="POST">
						<input type="hidden" name="crm" value="'.$crm.'" />
					</form>-->
				
				
				';
				
		echo $html;
		
		
	}
	
	public function tabelaEnderecoMedico($crm) {
		$rsEndereco = $this->psShare->getEnderecoMedico($crm);
		$html = '';
		
		if(count($rsEndereco) > 0) {
				
				
				
			$html .= '<table class="table table-striped">
					<tr class="strip">
						<th><span class="glyphicon glyphicon-map-marker"></span> ENDEREÇO</th>
						<th><span class="glyphicon glyphicon-map-marker"></span> COMPL.</th>
						<th><span class="glyphicon glyphicon-map-marker"></span> BAIRRO</th>
						<th><span class="glyphicon glyphicon-map-marker"></span> CIDADE</th>
						<th><span class="glyphicon glyphicon-credit-card"></span> ESPECIALIDADE</th>
						<th><span class="glyphicon glyphicon-user"></span> ENDEREÇO VISITADO POR</th>
					</tr>';
					
					
					foreach($rsEndereco as $i => $row) {
						$strip = ($i % 2) == 0 ? 'strip' : '';
						
						$setor = new setorModel();
						$setor->setIdSetor($row['ID_SETOR']);
						$setor->selecionar();
						
						$html .= '
						<tr class="'.$strip.'">
							<td>'.$row['ENDERECO'].' '.$row['NRO'].'</td>
							<td>'.$row['COMPLEMENTO'].'</td>
							<td>'.$row['BAIRRO'].'</td>
							<td>'.$row['CIDADE'].' / '.$row['UF'].'</td>
							<td>'.$row['ESPECIALIDADE'].'</td>
							<td>'.$setor->getSetor().' '.$setor->getNome().'</td>
						</tr>';
					}
					
				$html .= '</table>';
		
				}	
			return $html;
	}
	
	public function tabelaMercadoFichaMedico($crm) {
		$rsMercado = $this->psShare->getMercadoPxMedico($crm, $this->tmpCodLinha);
		$tabela = '';
		
		if(count($rsMercado) > 0) {
					$total = count($rsMercado);
					$i=1;
					$tabela = '';
					
					while($i <= $total) {
						$tr = "";
						$mercado = $rsMercado[$i]['MERCADO'];
						$sumAtual = 0;
						$sumAnterior = 0;
						
						$rs = $this->mercado->listarLinhas($rsMercado[$i]['MERCADO']);
						$linhas = '';
						foreach($rs as $registro) {
							$linhas .= $registro['NOME'].' - ';
						}
						$linhas = substr($linhas, 0, strlen($linhas)-3);
						
						
						$u = 1;
						while ($mercado == $rsMercado[$i]['MERCADO']) {
							$strip = ($u % 2) == 0 ? 'strip' : '';
							$sumAtual = $sumAtual + $rsMercado[$i]['PX'];
							$sumAnterior = $sumAnterior + $rsMercado[$i]['PX_ANT'];
							
							$class = (($rsMercado[$i]['PX'] - $rsMercado[$i]['PX_ANT']) < 0 ? 'text-danger' : '');
							//$icone = (($rsMercado[$i]['PX'] - $rsMercado[$i]['PX_ANT']) < 0 ? 'down-min' : 'up-min');
							
							$tr .= '<tr class="'.$strip.'">
										<td>'.$rsMercado[$i]['PRODUTO'].'</td>
										<td class="text-center">'.$rsMercado[$i]['PX_ANT'].'</td>
										<td class="text-center">'.$rsMercado[$i]['PX'].'</td>
										<td class="text-center '.$class.'">'.($rsMercado[$i]['PX'] - $rsMercado[$i]['PX_ANT']).'</td>
							       </tr>';
							$i++;
							$u++;
						}
						
						$class = (($sumAtual - $sumAnterior) < 0 ? 'text-danger' : '');
						$icone = $this->formatarPenetracao(($sumAtual - $sumAnterior));
						
						$rsCat = $this->psShare->getCatMercadoMedico($crm, $mercado);
						
						$catAnt = ($rsCat[1]['CAT_TRM02'] != "" ) ?  'CAT '.$rsCat[1]['CAT_TRM02'] : 'SEM CAT';
						$catAtu = ($rsCat[1]['CAT_TRM00'] != "" ) ?  'CAT '.$rsCat[1]['CAT_TRM00'] : 'SEM CAT';
						
						$tabela .= '<table class="table table-striped table-bordered">
									<tr class="active">
									
										<th width="50%" class="text-left">
											<h5><strong>'.$mercado.'</strong></h5>
											<h5><small>'.$linhas.'</small></h5>
										</th>
										
										<th class="text-center">
											<strong>PX '.$this->periodo['TRM01'].'</strong><br/>
											<h5><small>('.$catAnt.')<small></h5>
										</th>
										
										<th class="text-center">
											<strong>PX '.$this->periodo['TRM00'].'</strong><br />
											<h5><small>('.$catAtu.')<small></h5>
										</th>
										
										<th class="text-center">
											<strong>DIFERENÇA</strong>
											<h5><small>('.$this->periodo['TRM00'].' - '.$this->periodo['TRM01'].')<small></h5>
										
										</th>
										
									</tr>
									<tr class="active">
										<th class="text-left"><strong>TOTAL DO MERCADO</strong></th>
										<th class="text-center"><strong>'.$sumAnterior.'</strong></th>
										<th class="text-center"><strong>'.$sumAtual.'</strong></th>
										<th class="text-center '.$class.' imp-'.$icone.'"><strong><span class="'.$icone.'">'.($sumAtual - $sumAnterior).'</span></strong></th>
									</tr>
									'.$tr.'
								  </table>';
						
					}
				}
				
		
		return $tabela;
				
	}
	
	public function matrizBCG() {
		
		$idSetor = $_POST['setor'];	
		$mercado = $_POST['mercado'];
		$produto = $_POST['produto'];
		
		$dados = $this->psShare->getConquistaBCG($idSetor, $mercado, $produto);
		
		//print_r($dados);
		
		echo ' <h4>Análise de Prescrição do produto '.$produto.'</h4>
				
				<div class="row"><div class="col-lg-6">
						
				<table width="100%" class="bcg">
					<tr>
						<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;width:50%">
						
						     <div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-primary text-center">
										<strong><span class="glyphicon glyphicon glyphicon-thumbs-up"></span> '.$dados[1]['VALOR'].'</strong>
									</h1>
									<h5 class="text-primary text-center">médicos mantiveram a prescrição do produto.</h5>
								</div>
							</div>
						
						</td>
						<td style="border-bottom:1px solid #ccc;">
						
						
							<div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-success text-center">
										<strong><span class="glyphicon glyphicon-plus"></span> '.$dados[2]['VALOR'].'</strong>
									</h1>
									<h5 class="text-success text-center">aumentaram a prescrição.</h5>
								</div>
							</div>
						
						
						
						</td>
					</tr>
					
					<tr>
						<td style="border-right:1px solid #ccc">
						
						     <div class="panel">
								<div class="panel-body">
									<h1 class="text-warning text-center">
										<strong><span class="glyphicon glyphicon-minus"></span> '.$dados[3]['VALOR'].'</strong>
									</h1>
									<h5 class="text-warning text-center">diminuiram a prescrição do produto.</h5>
								</div>
							</div>
						
						</td>
						<td>
						
						     <div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-danger text-center">
										<strong><span class="glyphicon glyphicon-remove"></span> '.$dados[4]['VALOR'].'</strong>
									</h1>
									<h5 class="text-danger text-center">deixaram de prescrever o produto.</h5>
								</div>
							</div>
						
						
						</td>
					</tr>
				</table>
				
				</div>
				
				<div class="col-lg-6">
				
				
				<table width="100%" class="bcg">
					<tr>
						<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;width:50%">
						
						     <div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-primary text-center">
										<strong><span class="glyphicon glyphicon glyphicon-thumbs-up"></span> '.$dados[1]['VALOR'].'</strong>
									</h1>
									<h5 class="text-primary text-center">médicos mantiveram a prescrição do produto.</h5>
								</div>
							</div>
						
						</td>
						<td style="border-bottom:1px solid #ccc;">
						
						
							<div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-success text-center">
										<strong><span class="glyphicon glyphicon-plus"></span> '.$dados[2]['VALOR'].'</strong>
									</h1>
									<h5 class="text-success text-center">aumentaram a prescrição.</h5>
								</div>
							</div>
						
						
						
						</td>
					</tr>
					
					<tr>
						<td style="border-right:1px solid #ccc">
						
						     <div class="panel">
								<div class="panel-body">
									<h1 class="text-warning text-center">
										<strong><span class="glyphicon glyphicon-minus"></span> '.$dados[3]['VALOR'].'</strong>
									</h1>
									<h5 class="text-warning text-center">diminuiram a prescrição do produto.</h5>
								</div>
							</div>
						
						</td>
						<td>
						
						     <div class="panel panel-default">
								<div class="panel-body">
									<h1 class="text-danger text-center">
										<strong><span class="glyphicon glyphicon-remove"></span> '.$dados[4]['VALOR'].'</strong>
									</h1>
									<h5 class="text-danger text-center">deixaram de prescrever o produto.</h5>
								</div>
							</div>
						
						
						</td>
					</tr>
				</table>
				
				
				</div>
				
				</div>
				
				';	
		
	}
	
	public function pdf($crm) {
		$nomeArquivo = 'ficha_do_medico_'.$crm;
		
		$rsMedico = $this->psShare->getDadoMedico($crm, $this->tmpCodLinha);	
		
		$this->set('caminhoImagem', APP_CAMINHO.'public/img/');
		
		$this->set('nomeMedico', $rsMedico[1]['NOME']);
		$this->set('cidade', $rsMedico[1]['CIDADE']);
		$this->set('especialidade', $rsMedico[1]['ESPECIALIDADE']);
		$this->set('bairro', $rsMedico[1]['BAIRRO']);
		$this->set('uf', $rsMedico[1]['UF']);
		$this->set('crm', $crm);
		$this->set('endereco', $rsMedico[1]['ENDERECO'].', '.$rsMedico[1]['NRO'].' - '.$rsMedico[1]['COMPLEMENTO']);
		
		$this->set('tabelaEndereco', $this->tabelaEnderecoMedico($crm));
		$this->set('tabelaMercadoMedico', $this->tabelaMercadoFichaMedico($crm));
		$html = utf8_decode($this->renderToString('app/view/pdf/pdfFichaMedico.php'));
		//echo $html;
		appFunction::gerarPDF($html, '', $nomeArquivo);
	}
	
	public function graficoShareProdutoEMS($mercado) {
		$this->setor->setIdSetor($this->tmpIdSetor);
		$this->setor->selecionar();

		$gerentes = $this->setor->listarHierarquia();
		
		$arrSetor = array();
		$arrSetor2 = array();
		$arrCat = '';

		// lista os gerentes acima
		if(is_array($gerentes)) {
			foreach($gerentes as $gerente) {
				$dadosPs = $this->psShare->getPrescriptionShare($gerente->getIdSetor(),$gerente->getIdLinha(),$mercado);
				$d['TRM00'] = $dadosPs[1]['PROD1_TRM00'];
				$d['NOME'] = $gerente->getSetor().' '.$gerente->getNome();
				$d['DRILL'] = 0;
				array_push($arrSetor, $d);
			}
		}


		//lista o setor logado
		$dadosPs = $this->psShare->getPrescriptionShare($this->setor->getIdSetor(),$this->setor->getIdLinha(),$mercado);		

		$d['TRM00'] = $dadosPs[1]['PROD1_TRM00'];
		$d['NOME'] = $this->setor->getSetor().' '.$this->setor->getNome();
		$d['DRILL'] = 1;
		array_push($arrSetor, $d);
		
		// lista a equipe dele (se houver)
		$equipe = $this->setor->getEquipe();
		if(is_array($equipe)) {
			foreach($equipe as $setor) {
				$dadosPs = $this->psShare->getPrescriptionShare($setor->getIdSetor(), $setor->getIdLinha(), $mercado);
				$d['TRM00'] = $dadosPs[1]['PROD1_TRM00'];
				$d['NOME'] = $setor->getSetor().' '.$setor->getNome();
				$d['DRILL'] = 1;
				array_push($arrSetor, $d);
				
				$setor->selecionar();
				$equipe2 = $setor->getEquipe();
				if(is_array($equipe2)) {
					foreach($equipe2 as $setor2) {
						$dadosPs2 = $this->psShare->getPrescriptionShare($setor2->getIdSetor(),$setor2->getIdLinha(), $mercado);
						
						$d1['NOME'] = $setor2->getSetor().' '.$setor2->getNome();
						$d1['TRM00'] = $dadosPs2[1]['PROD1_TRM00'];
						$d1['NOME_GERENTE'] = $setor->getSetor().' '.$setor->getNome();
						array_push($arrSetor2, $d1);
					}
				}
			}
		}
		

		//$this->set('grafCategoria', $arrCat);
		//$this->set('grafDados', json_encode($arrSetor, JSON_NUMERIC_CHECK));
		
		
		$arrFinal = array();
		$arrFinal2 = array();
		
		foreach($arrSetor as $ger) {
			
			$setor = substr($ger['NOME'], 0, 5);

			$dado['name'] = $ger['NOME'];
			$dado['y'] = $ger['TRM00'];
			
			if($ger['DRILL'] == 1) {
				$dado['drilldown'] = $ger['NOME'];
			}
			
			array_push($arrFinal, $dado);
		}
		
		$final2 = '';
		
		foreach($arrSetor as $ger) {
			$tmp = '';
			$final2 .= '{
							name: "'.$ger['NOME'].'",
							id: "'.$ger['NOME'].'",';
			
			foreach($arrSetor2 as $ger2) {
				
				if($ger['NOME'] == $ger2['NOME_GERENTE']) {
				$tmp .= '[
									"'.$ger2['NOME'].'",
									'.$ger2['TRM00'].'
								],';
				}
			}
			
			$final2 .= 'data: ['.substr($tmp, 0, strlen($tmp)-1).']';
			$final2 .= '},';
		}
		
		$final2 = substr($final2, 0, strlen($final2)-1);
		//echo $final2;
	///print_r(json_encode($arrFinal2));
		
		//print json_encode($arrFinal, JSON_NUMERIC_CHECK);
                
		$this->set('grafDadosDrill', $final2);
		$this->set('grafDadosGerente', json_encode($arrFinal, JSON_NUMERIC_CHECK));
		
					
	}
	
	public function grafTeste($mercado) {
		$this->setor->setIdSetor($this->tmpIdSetor);
		$this->setor->selecionar();

		$gerentes = $this->setor->listarHierarquia();
		
		$arrSetor = array();
		$arrSeries = array();
		$arrFinal = array();
		$data = array();
		
		// lista os gerentes acima
		if(is_array($gerentes)) {
			foreach($gerentes as $gerente) {
				$dadosPs = $this->psShare->getPrescriptionShare($gerente->getIdSetor(),$gerente->getIdLinha(),$mercado);
				$arrUp['y'] = $dadosPs[1]['PROD1_TRM00'];
				$arrUp['name'] = $gerente->getSetor().' '.$gerente->getNome();
				array_push($arrSetor, $arrUp);
			}
		}
		
		
		//lista o setor logado
		$dadosPs = $this->psShare->getPrescriptionShare($this->setor->getIdSetor(),$this->setor->getIdLinha(),$mercado);		
		$arrUp['y'] = $dadosPs[1]['PROD1_TRM00'];
		$arrUp['name'] = $this->setor->getSetor().' '.$this->setor->getNome();
		
		array_push($arrSetor, $arrUp);

		//lista a equipe
		$equipe = $this->setor->getEquipe();
		if(is_array($equipe)) {				
			foreach($equipe as $setor) {
				$dadosPs = $this->psShare->getPrescriptionShare($setor->getIdSetor(),$setor->getIdLinha(),$mercado);
				
				if($this->setor->getIdPerfil() >= 3) {
					$arrUp['drilldown'] = $setor->getSetor();
					
					$setor->selecionar();
					$subEquipe = $setor->getEquipe();
					
					if(is_array($subEquipe)) {			
						
						$arrSer['id'] = $setor->getSetor();
						$arrSer['name'] = $setor->getSetor().' '.$setor->getNome();
						$tmp = array();
						
						foreach($subEquipe as $sub) {
							
							$dadosPsSub = $this->psShare->getPrescriptionShare($sub->getIdSetor(),
																			   $sub->getIdLinha(),
																			   $mercado);
					
							
							
							$a['name'] = $sub->getSetor().' '.$sub->getNome();
							$a['y'] = $dadosPsSub[1]['PROD1_TRM00'];
							
							if($sub->getIdPerfil() != 1) {
								$a['drilldown'] = $sub->getSetor();
								
								
								$sub->selecionar();
								$reps = $sub->getEquipe();
								
								if(is_array($reps)) {

									$arrSer2['id'] = $sub->getSetor();
									$tmp2 = array();
	
									foreach($reps as $rep) {
										$dadosPsSub2 = $this->psShare->getPrescriptionShare($rep->getIdSetor(),
																				   $rep->getIdLinha(),
																				   $mercado);
		
										$b['name'] = $rep->getSetor().' '.$rep->getNome();
										$b['y'] = $dadosPsSub2[1]['PROD1_TRM00'];
	
										
										array_push($tmp2, $b);								   
	
									}

									$arrSer2['data'] = $tmp2;
									array_push($data, $arrSer2);
								}
								
								
							}
							
							array_push($tmp, $a);

						}
						
						$arrSer['data'] = $tmp;
						array_push($arrSeries, $arrSer);
					}
				}
			
				$arrUp['y'] = $dadosPs[1]['PROD1_TRM00'];
				$arrUp['name'] = $setor->getSetor().' '.$setor->getNome();
				
				array_push($arrSetor, $arrUp);				
			}
		}
		

		
		
		foreach($arrSetor as $key => $arr) {
			$dado['name'] = $arr['name'];
			$dado['y'] = $arr['y'];
			
			if($arr['drilldown'] != "") {
				$dado['drilldown'] = $arr['drilldown'];	
			}
			
			array_push($arrFinal, $dado);
		}
		
		
		$series = array();
		foreach($arrSeries as $key => $serie) {
			$dado1['id'] = $serie['id'];
			$dado1['name'] = $serie['name'];
			
			if(is_array($serie['data'])) {
				$t = array();
				foreach($serie['data'] as $dt) {
					$tt['name'] = $dt['name'];
					$tt['y'] = $dt['y'];
					$tt['drilldown'] = $dt['drilldown'];
					
					array_push($t, $tt);
				}
				$dado1['data'] = $t;
			}
			
			array_push($series, $dado1);
			
		}
		
		
		
		foreach($data as $i => $sub) {
			$dado1['id'] = $sub['id'];
			if(is_array($serie['data'])) {
				$tx = array();
				foreach($sub['data'] as $dt) {
					$tu['name'] = $dt['name'];
					$tu['y'] = $dt['y'];
					
					array_push($tx, $tu);
				}
				$dado1['data'] = $tx;
			}
			array_push($series, $dado1);
		}
		
		
		
		//print json_encode($series);
		//print_r($series);
		//print_r($arrFinal);
		
		
		$this->set('dados', json_encode($arrFinal, JSON_NUMERIC_CHECK));
		$this->set('series', json_encode($series, JSON_NUMERIC_CHECK));
		$this->set('data', json_encode($data, JSON_NUMERIC_CHECK));

	}
	
	public function graficoDrill($mercado) {
		$this->setor->setIdSetor($this->tmpIdSetor);
		$this->setor->selecionar();
		
		$dados = $this->psShare->getGraficoDrill($this->tmpIdSetor, $mercado);
		
		$dadoBrasil = $dados[1]['SHARE'];
		
		$arrFinal = array();
		$series = array();
		
		for($i=1;$i<=count($dados);$i++) {
			$exp = explode(' ', trim($dados[$i]['NOME']));
			$dados[$i]['NOME'] = $exp[0].' '.$exp[count($exp)-1];
		}
		
		/*SE FOR REP OU GD LOGADO*/
		if($this->setor->getIdPerfil() == 1 || $this->setor->getIdPerfil() == 2) {
			
			
			foreach($dados as $key => $rep) {
				$dado['name'] = $rep['SETOR'].' '.$rep['NOME'];
				$dado['y'] = $rep['SHARE'];
				array_push($arrFinal, $dado);
			}
			
			array_push($series, $dado);
			
		}
		
		/*SE FOR GR LOGADO*/
		if($this->setor->getIdPerfil() == 3) {
			$arrTeste = array();
			$ser = array();
			foreach($dados as $key => $rep) {
				
				if($rep['ID_PERFIL'] >= 2) {
				
					$dado['name'] = $rep['SETOR'].' '.$rep['NOME'];
					
					$dado['y'] = $rep['SHARE'];
					
					if($rep['ID_PERFIL'] == 2) {
						$dado['drilldown'] = $rep['SETOR'];
						
						$serie['id'] = $rep['SETOR'];
						$serie['name'] = $rep['SETOR'].' '.$rep['NOME'];
						
						$a = array();
						$i = ($key+1);
						
						while($dados[$i]['ID_PERFIL'] == 1) {
							$aa['name'] = $dados[$i]['SETOR'].'  '.$dados[$i]['NOME'];
							$aa['y'] = $dados[$i]['SHARE'];
							
							array_push($a, $aa);
							
							$i++;
						}
						$serie['data'] = $a;
						
						
						array_push($series, $serie);
						$key = $i;

					}
					
					array_push($arrFinal, $dado);
				
				}
				
				
				
			}

			
			//$this->set('series', json_encode($ser, JSON_NUMERIC_CHECK));
			
		}
		
		
		/*SE FOR GN LOGADO*/
		if($this->setor->getIdPerfil() == 4) {
			$ser = array();
			$ser2 = array();
			
			foreach($dados as $key => $rep) {
				
				if($rep['ID_PERFIL'] >= 3) {
				
					$dado['name'] = $rep['SETOR'].' '.$rep['NOME'];
					$dado['y'] = $rep['SHARE'];
					
					if($rep['ID_PERFIL'] == 3) {
						$dado['drilldown'] = $rep['SETOR'];
						
						$serie['id'] = $rep['SETOR'];
						$serie['name'] = $rep['SETOR'].' '.$rep['NOME'];
						
						$a = array();
						$i = ($key+1);
						
						while($dados[$i]['ID_PERFIL'] == 2) {
							$aa['name'] = $dados[$i]['SETOR'].'  '.$dados[$i]['NOME'];
							$aa['y'] = $dados[$i]['SHARE'];
							$aa['drilldown'] = $dados[$i]['SETOR'];
							
							array_push($a, $aa);
							
							$serie2['id'] = $dados[$i]['SETOR'];
							
							$x=$i+1;
							$u = array();
							while($dados[$x]['ID_PERFIL'] == 1) {
								
								$bb['name'] = $dados[$x]['SETOR'].'  '.$dados[$x]['NOME'];
								$bb['y'] = $dados[$x]['SHARE'];
								
								array_push($u, $bb);
								$x++;
							}
							
							$serie2['data'] = $u;
							array_push($ser2, $serie2);
							$i = $x;
						}
						$serie['data'] = $a;
						
						
						array_push($ser, $serie);
						$key = $i;

					}
					
					
					array_push($arrFinal, $dado);
				
				}
			}
			
			
			
			foreach($ser as $key => $serie) {
				$dado1['id'] = $serie['id'];
				$dado1['name'] = $serie['name'];
				
				if(is_array($serie['data'])) {
					$t = array();
					foreach($serie['data'] as $dt) {
						$tt['name'] = $dt['name'];
						$tt['y'] = $dt['y'];
						$tt['drilldown'] = $dt['drilldown'];
						
						array_push($t, $tt);
					}
					$dado1['data'] = $t;
				}
				
				array_push($series, $dado1);
				
			}
			
			foreach($ser2 as $i => $sub) {
				$dado1['id'] = $sub['id'];
				if(is_array($serie['data'])) {
					$tx = array();
					foreach($sub['data'] as $dt) {
						$tu['name'] = $dt['name'];
						$tu['y'] = $dt['y'];
						
						array_push($tx, $tu);
					}
					$dado1['data'] = $tx;
				}
				array_push($series, $dado1);
			}
			
			
			
			
		}

		$this->set('series', json_encode($series, JSON_NUMERIC_CHECK));
		$this->set('dados', json_encode($arrFinal, JSON_NUMERIC_CHECK));
		
	}


	public function graficoDrill2($idsetor, $mercado, $nivel=0, $arr='') {
	
		$dados = $this->psShare->getGraficoDrillData($idsetor, $mercado);

		foreach($dados as $rep) {
			$nome = explode(' ', $rep['NOME']);
			$label = $nome[0]. ' '.$nome[count($nome)-1];

			$dado['name']      = '<b>'.$rep['SETOR'].'</b> <br>'.$label;
			$dado['y']         = $rep['SHARE'];
			$dado['drilldown'] = (count($this->psShare->getGraficoDrillSerie($rep['ID_SETOR'], $mercado)) > 0) ? $rep['ID_SETOR'] : null;	

			$dt[] = $dado;
			$series[] = $this->montarSerieDrillDown($rep, $mercado);

			//$d['id'] = $rep['SETOR'];
			//$d['name'] = '<b>'.$rep['SETOR'].'</b> <br>'.$label;
			$d = array(
				'name' => '<b>'.$rep['SETOR'].'</b> <br>'.$label,
				'y' => $rep['SHARE']
			);

			$cats[] = '<b>'.$rep['SETOR'].'</b> <br>'.$label;
			$dt2[] = $d;
		}

		$this->set('series', json_encode($series, JSON_NUMERIC_CHECK));
		$this->set('dados', json_encode($dt, JSON_NUMERIC_CHECK));

		$this->set('cats', json_encode($cats, JSON_NUMERIC_CHECK));
		$this->set('dados2', json_encode($dt2, JSON_NUMERIC_CHECK));

	}

	public function montarSerieDrillDown($rep, $mercado) {
		if($rep['ID_SETOR']) {
			$series = [];
			$serie = $this->psShare->getGraficoDrillSerie($rep['ID_SETOR'], $mercado);

			
				$ser['id']   = $rep['ID_SETOR'];
				$ser['name'] = $rep['ID_SETOR'];
				$ser['data'] = [];

				foreach($serie as $data) {

						$nome = explode(' ', $data['NOME']);
						$label = $nome[0]. ' '.$nome[count($nome)-1];

					$ser['data'][] = array('<b>'.$data['SETOR'].'</b> <br>'.$label , $data['SHARE']);

					if(count($this->psShare->getGraficoDrillSerie($data['ID_SETOR'], $mercado)) > 0) {
						$ser['drilldown'] = $data['ID_SETOR'];
						$series = $this->montarSerieDrillDown($data, $mercado);
					}
					
				}

				$series =  $ser;
			//}
			return $series;
		}
	}

	public function graficoDrill3($idsetor, $mercado, $nivel=0) {
		//

		$dados = $this->psShare->getGraficoDrill2($idsetor,  $nivel, $mercado);

		foreach($dados as $rep) {
			$dado['name'] = $rep['SETOR'];
			$dado['y'] = $rep['SHARE'];
			$dado['drilldown'] = $rep['SETOR'];	
			
			$sub = $this->graficoDrill3($rep['ID_SETOR'], $mercado, $rep['ID_SETOR']);

			if(count($sub) > 0) {
				
				$serie['id']       = $rep['SETOR'];
				$serie['name']     = $dado['name'];
			}
			
			$final[] = $dado;
		}
		return $final;
	}












	
	public function graficoShareProduto($mercado) {
		
		$rs = $mercado->shareProduto($this->tmpIdSetor);
		$arr = array();
		
		foreach($rs as $key => $produto) {
			
			$dado['name'] = $produto['PRODUTO'];
			$dado['y']    = $produto['SHARE'];
			$dado['sliced'] = "";
			$dado['selected'] = "";
			
			if($produto['PROD_EMS'] == 1) {
				$dado['sliced'] = "true";
				$dado['selected'] = "true";
			}
			
			array_push($arr, $dado);		
		}
		return json_encode($arr, JSON_NUMERIC_CHECK);
		
	}
	




	public function dadosDrill() {
		$setor = $_POST['setor'];
		$mercado = $_POST['mercado'];
		$cor = $_POST['cor'];
		$nivel = $_POST['nivel'];

		//$cores = ($cor) ? $cor : '';

		$idsetor = ($setor == '') ? $this->tmpIdSetor : $setor;
		
		$dados = $this->psShare->getGraficoDrillData($idsetor, $mercado);
		$rsMerc = $this->psShare->getGraficoDrill2($this->tmpIdSetor,0,$mercado);

		foreach($dados as $rep) {
			$nome = explode(' ', $rep['NOME']);
			$label = $nome[0]. ' '.$nome[count($nome)-1];

			$dado['setor'] = $rep['ID_SETOR'];
			$dado['sub'] = $rep['SUB'];
			$dado['nivel'] = $rep['NIVEL'];
			$dado['cor'] = $cor;
			$dado['indice'] = $rep['INDICE'];


			$nome = $rep['SETOR'].' '.$label;

			if($rep['INDICE'] == 0 && $rep['SUB'] != 0) {
				$nome = '<b>'.$nome.'</b>';
			}

			$dado['name'] = $nome;
			$dado['y'] = $rep['SHARE'];

			$data[] = $dado;

		}
		$final['brasil'] = $rsMerc[1]['SHARE'];
		$final['color'] = $cor;
		$final['colorByPoint'] = ($idsetor == appFunction::dadoSessao('id_setor')) ? true : false;
		$final['data'] = $data;

		echo json_encode($final, JSON_NUMERIC_CHECK);

	}





	public function salvarEnquete() {
            appFunction::salvarEnquete($_POST['txtNota'], $_POST['txtIdSetor'], $_POST['txtIdColaborador'], strtoupper($_POST['txtObservacao']));
	}
	
	
	
}


?>