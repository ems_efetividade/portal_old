<?php

require_once('lib/appConexao.php');

class psShareModel extends appConexao {
	
	private $idSetor;
	private $idLinha;
	private $mercado;
	
	
	public function setIdSetor($valor) {
		$this->idSetor = $valor;
	}
	
	public function setIdLinha($valor) {
		$this->idLinha = $valor;
	}
	
	public function setMercado($valor) {
		$this->mercado = $valor;
	}
	
	public function getTrimestre() {
		return $this->executarQueryArray("SELECT * FROM PS_TRIMESTRE");	
	}
	
	public function getPrescriptionShare($idSetor, $idLinha, $mercado) {
		//echo "EXEC proc_ps_shareSetor '".$idSetor."', '".$idLinha."', '".$mercado."'";
		return $this->executarQueryArray("EXEC proc_ps_shareSetor '".$idSetor."', '".$idLinha."', '".$mercado."'");	
	}
	
	public function getMedicos($mercado, $produto, $periodo, $idSetor) {
		return $this->executarQueryArray("EXEC proc_ps_medicosPorSetor '".$mercado."', 
																	   '".$produto."', 
																	   '".$periodo."', 
																	   ".$idSetor."");	
	}
	
	public function pesquisarMedico($idSetor, $campo, $criterio) {
		return $this->executarQueryArray("EXEC proc_ps_pesquisarMedico ".$idSetor.", '".$campo."', '".$criterio."'");	
	}
	
	public function getDadoMedico($crm, $idLinha) {
		$query = "SELECT
						M.ID_SETOR,
						M.NOME,
						M.ESPECIALIDADE,
						UPPER(M.ENDERECO) AS ENDERECO,
						UPPER(M.NRO) AS NRO,
						UPPER(M.COMPLEMENTO) AS COMPLEMENTO,
						UPPER(M.BAIRRO) AS BAIRRO,
						M.CEP,
						M.CIDADE,
						M.UF
					FROM
						PS_MEDICO M
					INNER JOIN
						vw_colaboradorSetor S ON S.ID_SETOR = M.ID_SETOR	
					WHERE
						M.CRM = '".$crm."' AND
						S.ID_LINHA = ".$idLinha."
					GROUP BY
						M.ID_SETOR,
						M.NOME,
						M.ESPECIALIDADE,
						M.ENDERECO,
						M.NRO,
						M.COMPLEMENTO,
						M.BAIRRO,
						M.CEP,
						M.CIDADE,
						M.UF";	

		return $this->executarQueryArray($query);	
	}
	
	public function getEnderecoMedico($crm) {
		$query = "SELECT
						ID_SETOR,
						ESPECIALIDADE,
						UPPER(ENDERECO) AS ENDERECO,
						UPPER(NRO) AS NRO,
						UPPER(COMPLEMENTO) AS COMPLEMENTO,
						UPPER(BAIRRO) AS BAIRRO,
						CEP,
						CIDADE,
						UF
					FROM
						PS_MEDICO
					WHERE
						CRM = '".$crm."'
					GROUP BY
						ID_SETOR,
						ESPECIALIDADE,
						ENDERECO,
						NRO,
						COMPLEMENTO,
						BAIRRO,
						CEP,
						CIDADE,
						UF";
		return $this->executarQueryArray($query);	
	}
	
	
	public function getMercadoPxMedico($crm, $idLinha) {
		$query = "SELECT
					  M.MERCADO,
					  M.PRODUTO,
					  SUM(M.TRM00) AS PX,
					  SUM(M.TRM01) AS PX_ANT
					  
					FROM
					  PS_MEDICO M
					INNER JOIN
						vw_colaboradorSetor S ON S.ID_SETOR = M.ID_SETOR
					WHERE
					  M.CRM = '".$crm."' AND
					  S.ID_LINHA = ".$idLinha."
					GROUP BY
					  M.MERCADO,
					  M.PRODUTO
					ORDER BY
					  M.MERCADO,
					  SUM(M.TRM00) DESC";
		return $this->executarQueryArray($query);	
	}
	
	public function getCatMercadoMedico($crm, $mercado) {
		$query = "SELECT * FROM PS_CATEGORIA_MERCADO WHERE CRM = '".$crm."' AND MERCADO = '".$mercado."'";
		return $this->executarQueryArray($query);
	}
	
	public function getGraficoDrill($idSetor, $mercado) {
		//echo "exec proc_ps_graficoDrillDown ".$idSetor.", '".$mercado."'";
            $rs = $this->executarQueryArray("exec proc_ps_graficoDrillDown ".$idSetor.", '".$mercado."'");
            
            for($i=1;$i<=count($rs);$i++) {
                $rs[$i]['SHARE'] = appFunction::formatarMoeda($rs[$i]['SHARE'], 0);
            }
            
            return $rs;
	}
	
	public function getGraficoDrill2($idSetor, $nivel=0,  $mercado) {
		//$rs = $this->executarQueryArray("exec proc_ps_graficoDrillDown2 ".$idSetor.", '".$mercado."'");
		$query = "SELECT * FROM PS_GRAF_DRILL WHERE ID_SETOR = ".$idSetor." AND MERCADO = '".$mercado."' ORDER BY SETOR ASC";
		
		if($nivel > 0) {	
			$query = "SELECT * FROM PS_GRAF_DRILL WHERE NIVEL = ".$idSetor." AND MERCADO = '".$mercado."' ORDER BY SETOR ASC";
		}

		$rs = $this->executarQueryArray($query);

		//echo $query;
		for($i=1;$i<=count($rs);$i++) {
			$rs[$i]['SHARE'] = appFunction::formatarMoeda($rs[$i]['SHARE'], 0);
		}
		
		return $rs;
	}



	public function getGraficoDrillData($idSetor='', $mercado) {
		/*
		$query = "SELECT 
						NULL AS ID_SETOR,
						A.SETOR,
						A.NOME,
						A.SHARE
					FROM 
						PS_GRAF_DRILL A 
					WHERE 
						A.ID_SETOR = ".$idSetor." AND 
						A.MERCADO = '".$mercado."' 
					
					
					
					UNION ALL
					
					
					SELECT 
						A.ID_SETOR,
						A.SETOR,
						A.NOME,
						A.SHARE
					FROM 
						PS_GRAF_DRILL D 
					INNER JOIN PS_GRAF_DRILL A ON D.ID_SETOR = A.NIVEL AND A.MERCADO = D.MERCADO
					WHERE 
						D.ID_SETOR = ".$idSetor." AND 
						D.MERCADO = '".$mercado."' 
					ORDER BY 
						A.SETOR ASC
					";
*/

//if($idSetor == '') {
			$query = "
			WITH GRUPO AS (
				SELECT * , 1 AS INDICE FROM PS_GRAF_DRILL WHERE ID_SETOR = ".$idSetor." AND MERCADO = '".$mercado."'
				UNION ALL
				SELECT A.*, G.INDICE+1 FROM PS_GRAF_DRILL A INNER JOIN GRUPO G ON G.NIVEL = A.ID_SETOR AND G.MERCADO = A.MERCADO
				)
				
				SELECT G.*, CASE WHEN (SELECT COUNT(*) FROM PS_GRAF_DRILL O WHERE O.NIVEL = G.ID_SETOR) > 0 THEN 1 ELSE 0 END AS SUB FROM GRUPO G
UNION ALL 
SELECT A.*,  0, CASE WHEN (SELECT COUNT(*) FROM PS_GRAF_DRILL O WHERE O.NIVEL = A.ID_SETOR)> 0 THEN 1 ELSE 0 END AS SUB FROM PS_GRAF_DRILL A WHERE A.NIVEL = ".$idSetor." AND A.MERCADO = '".$mercado."' ORDER BY SETOR ASC
			";
//}
			$rs = $this->executarQueryArray($query);

			//echo $query;
			for($i=1;$i<=count($rs);$i++) {
				$rs[$i]['SHARE'] = appFunction::formatarMoeda($rs[$i]['SHARE'], 0);
			}

			return $rs;
	}

	

	public function getGraficoDrillSerie($idSetor, $mercado) {
		//$idSetor = implode(",", array_filter($idSetores));

		if($idSetor != null) {

		$query = "SELECT 
					A.ID_SETOR,
					A.NIVEL,
					A.SETOR,
					A.NOME,
					A.SHARE
				FROM 
					PS_GRAF_DRILL D 
				INNER JOIN PS_GRAF_DRILL A ON D.ID_SETOR = A.NIVEL AND A.MERCADO = D.MERCADO
				WHERE 
					D.ID_SETOR = ".$idSetor." AND 
					D.MERCADO = '".$mercado."' 
				
				";

				//echo $query;

			$rs = $this->executarQueryArray($query);

			//echo $query;
			for($i=1;$i<=count($rs);$i++) {
				$rs[$i]['SHARE'] = appFunction::formatarMoeda($rs[$i]['SHARE'], 0);
			}

			return $rs;
		}
		}

	public function getConquistaBCG($idSetor, $mercado, $produto) {
		return $this->executarQueryArray("EXEC proc_ps_conquistaBCG ".$idSetor.", '".$mercado."', '".$produto."'");
	}
	
	
}

?>