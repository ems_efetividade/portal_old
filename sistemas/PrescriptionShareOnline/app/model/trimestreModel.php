<?php

require_once('lib/appConexao.php');

class trimestreModel extends appConexao {
    private $id;
    private $codigo;
    private $trimestre;
    
    private $layout;
    
    public function __construct() {
        parent::__construct();

        $this->id = '';
        $this->codigo = '';
        $this->trimestre = '';

        
        $this->layout = array('CODIGO', 'TRIMESTRE');
    }
    
    public function setId($id) {
        $this->id = $id;
    }    
    
    public function setCodigo($codigo) {
        $this->codigo = $codigo;
    }

    public function setTrimestre($trimestre) {
        $this->trimestre = $trimestre;
    }
    
    public function getId() {
        return $this->id;
    }

    public function getCodigo() {
        return $this->codigo;
    }

    public function getTrimestre() {
        return $this->trimestre;
    }

    public function getLayout() {
        return $this->layout;
    }

    public function salvar() {
        
        if($this->id == '') {
        
            $query = "INSERT INTO [PS_TRIMESTRE]
                            ([CODIGO]
                            ,[TRIMESTRE])
                      VALUES
                            ('".$this->codigo."'
                            ,'".$this->trimestre."')";
        
        } else {
            $query = "UPDATE [PS_TRIMESTRE]
                    SET [CODIGO] = '".$this->codigo."'
                       ,[TRIMESTRE] = '".$this->trimestre."'
                  WHERE [CODIGO] + [TRIMESTRE] = '".$this->id."'";
        }
        
        
        $validar = $this->validar();
        
        if($validar == '') {
            $this->executar($query);
        } else {
            return $validar;
        }
    }
    
    private function validar() {
        $erro = '';
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS T FROM PS_TRIMESTER WHERE CODIGO + TRIMESTRE = '".$this->id."'");
        
        if($rs[1]['T'] > 0) {
            $erro =  'Já existe um trimestre com esse nome!';
        }
        
        return $erro;
    }
    
    
    public function excluir() {
        $this->executar("DELETE FROM PS_TRIMESTRE WHERE CODIGO + TRIMESTRE = '".$this->id."'");
    }
    
    public function limparTabela() {
        $this->executar("DELETE FROM PS_TRIMESTRE");
    }
    
  
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM PS_TRIMESTRE WHERE CODIGO + TRIMESTRE = '".$this->id."'");
        $this->popular($rs[1]);
    }
    
    public function listar() {
        $arr = array();
        $rs = $this->executarQueryArray("SELECT * FROM PS_TRIMESTRE ORDER BY CODIGO ASC");
        if($rs) {
            foreach($rs as $row) {
                $a = new trimestreModel();
                $a->popular($row);
                
                $arr[] = $a;
            }
        }
        return $arr;
    }
    
    protected function popular($row) {
        $this->codigo    = $row['CODIGO'];
        $this->trimestre = $row['TRIMESTRE'];
        $this->id        = $this->codigo.$this->trimestre;
    }
    
}