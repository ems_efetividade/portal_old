<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Tabela PS_TRIMESTRE</b></h3>
            <hr>
            
            <p>
                <a href="<?php echo APP_CAMINHO ?>admin/trimestreForm" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> 
                    Adicionar
                </a>
                <a href="<?php echo APP_CAMINHO ?>admin/trimestreImportarForm" class="btn btn-default pull-right">
                    <span class="glyphicon glyphicon-import"></span> 
                    Importar Arquivo .csv
                </a>
            </p>
            
            <table class="table table-striped table-condensed">
                <tr>
                    <th><small>Código</small></th>
                    <th><small>Trimestre</small></th>
                    <th><small>ID Linha</small></th>
                    <th><small>Ação</small></th>
                </tr>
                    
                <?php foreach($listaTrimestre as $trim) { ?>
                <tr>
                    <td><small><?php echo $trim->getCodigo() ?></small></td>
                    <td><small><?php echo $trim->getTrimestre() ?></small></td>
                    <td>
                        <a href="<?php echo APP_CAMINHO ?>admin/trimestreForm/<?php echo base64_encode($trim->getId()) ?>">Editar</a>
                        <a href="<?php echo APP_CAMINHO ?>admin/trimestreExcluir/<?php echo base64_encode($trim->getId()) ?>">Excluir</a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>

        </div>
    </div>
</div>