<?php require_once('app/view/headerView.php'); ?>

<style>
table p {
	padding:0;
	margin:0px;	
}
.listarMedicos {
	cursor:pointer;
}	
.alert {
	padding:6px;
	margin:1px;	
}
.alert-default {
	color: #777777;
    background-image: -webkit-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: -o-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: linear-gradient(to bottom, #eeeeee 0, #dddddd 100%);
    background-repeat: repeat-x;
    border: 1px solid #ddd;	
}
.alert-none {	   
    border: 1px solid #eee;	
}

.alert-gray {
	background-color:#f1f1f1;	   
    border: 1px solid #e5e5e5;	
}

.alert-soft {
	background-color:#f9f9f9;	   
    border: 1px solid #e5e5e5;	
}
.alert-primary {
	color: #ffffff;
    background-image: -webkit-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: -o-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: linear-gradient(to bottom, #4c70ba 0, #3b5998 100%);
    background-repeat: repeat-x;
    border: 1px solid #3b5998;	
}

.up, .down, .middle {
	padding-left:16px;	
}
.up, .up-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/up.png) no-repeat left center;	
	color:#3c763d;
}

.down, .down-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/down.png) no-repeat left center;	
	color:#a94442;
}

.middle, .middle-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/middle.png) no-repeat left center;	
	color:#f0ad4e;
}

.up-min, .down-min {
	padding-left:13px;
	background-size: 13px 13px;
}

.plus {
	background:url(<?php echo APP_CAMINHO; ?>public/img/plus.png) no-repeat left left;	
}
.bold {
	font-weight:bold;	
}

.loading {
	display:none;	
}

.abrirSetor, .listarMedico {
	cursor:pointer;	
}

html {
  overflow-y: scroll;
}

body.modal-open,
.modal-open {
    margin-right: 0px;
    margin-right: 0; /* // fix */
}

/*
//.modal-open .navbar-fixed-top,
//.modal-open .navbar-fixed-bottom {
//padding-right: 17px;
//}
*/
.loading-gif {
   position:absolute;
   top:55%;
   left:50%;
   margin-top:-25px;
   margin-left:-25px;
}

.table tbody>tr>td{
    vertical-align: middle;
}


.bcg .panel {
	border:0px;
	-webkit-box-shadow: 0 0px 0px rgba(0,0,0,0);
}

.nav li a {
    color:#fff;
}

.nav li a:hover {
    color:#000;
}

</style>

<div style="padding-left:15px;padding-right:15px">

    <div class="row">
        <div class="col-lg-2 panel-left"  style="background: #2A3F54; color:#FFF">
        <br>
        <img class="center-block" height="90" src="https://www.evolvnetworks.com/wp-content/uploads/2016/12/icon-challenges-productivity-3.png" alt="">
        <p class="text-center">Prescription Share</p>
        <hr>
        <form class="" id="formSelMercado" method="post" action="<?php echo APP_CAMINHO ?>home/selecionar">
            <input type="hidden" name="mercado" value="0" />
            <input type="hidden" name="concorrente" value="0" />
            <ul class="nav nav-pills nav-stacked">

            <?php  //foreach($listaMercado as $lista) { ?>
                    
                <li role="presentation" class="<?php // echo ($nomeMercado == $lista->getMercado()) ? 'active' : ''; ?>">
                <a href="#" class="cmbMercado" data-val="<?php //echo $lista->getMercado(); ?>"><small><?php //echo $lista->getMercado(); ?></small></a>
                </li>
               
        <?php //} ?>
        </form>
    </ul>
        
        </div>
        <div class="col-lg-10 panel-right">
        
                <br>
            <!-- nome do mercado -->
            <div class="row" style="margin-bottom:10px;">
                <div class="col-lg-12">
                <h3 style="margin-top:0px;"> <?php print $nomeMercado ?> <small>Prescription Share </small></h3>
                </div>
            </div>


            <div class="row">

                <!-- coluna PRINCIPAIS PRODUTOS -->
                <div class="col-lg-4" style="padding-right:2px">

                    <div class="panel panel-default" style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <small><span class="glyphicon glyphicon-star"></span> Principais Produtos</small>
                            </h5>
                        </div>
                        
                        <div class="panel-body" style="min-height:181px">
                            <small><?php print $princProdutos; ?></small>
                        </div>
                    
                    </div>

                    <!-- coluna GRAFICO PIZZA -->
                    <div class="panel panel-default"  style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title">
                                <small class="text-primary">
                                    <span class="glyphicon glyphicon-stats"></span> Participação no Mercado - <?php print $trmAtu ?>
                                </small>
                            </h5>
                        </div>
                        <div class="panel-body">
                        <div id="container" style="min-width: 310px; height: 209px; max-width: 600px; margin: 0 auto"></div>
                        </div>
                    </div>
                    
                </div>
            
                <div class="col-lg-8"  style="padding-left:2px">
    
                    <div class="panel panel-default"  style="margin-bottom:3px">
                        <div class="panel-heading">
                            <h5 class="panel-title"><small><span class="glyphicon glyphicon-stats"></span> Participação de <b><?php print $produtoEMS ?></b> por região/setor</small></h5>
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col-lg-5"></div>
                                <div class="col-lg-12">
                                    <!-- <div id="vcontainer3" style="min-width: 310px; height: 400px; margin: 0 auto"></div> -->
                                    <div>
        <button style="position:absolute;z-index:100;right:10px" class="btn btn-xs btn-info" id="btnOpa"><< Voltar</button>
        <div id="container9" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
    </div>
                                    <p class="pull-right"><small>Fonte: Audit Pharma</small></p>
                                    <p class="pull-left"><small>Linha pontilhada = Market Share Brasil (*)</small></p>
                                </div>
                            </div>

                        </div>
                        
                    </div>

                </div>


                
        
            </div>
            <div class="panel panel-default">
        	<div class="panel-heading">
        		<small><span class="glyphicon glyphicon-list"></span> Prescription Share</small>
               <!-- <button type="button" class="btn btn-success btn-xs pull-right"><span class="glyphicon glyphicon-save" aria-hidden="true"></span> Exportar
                    </button>
                -->
              
                
                
      		</div>
	<div class="panel-body">
		<div class="table-responsive">
        <small>
            <table width="100%" class="taable taable-bordered taable-striped">
              <tr>
                <td rowspan="3" width="28%">
                   
                   <h4 class="text-primary"><b>
                   <!-- <img height="26" src="<?php print APP_CAMINHO ?>public/img/ems.png" /> -->
                   Mercado de <?php print $nomeMercado ?></b></h4>
                   
                    </td>
                <td colspan="<?php print $numProdutos ?>" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Trimestre <?php print $trmAnt ?> (%)
                     </strong></p></td>
                <td colspan="<?php print $numProdutos ?>" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Trimestre <?php print $trmAtu ?> (%)</strong>
                    </p></td>
                <td colspan="<?php print $numProdutos ?>" class="text-center info" width="24%">
                    <p class="alert alert-primary text-center icon-red">
                        <strong>Penetracao (P.P)</strong>
                    </p></td>
              </tr>
              <tr class="info">
                <?php print $linhaPrincProdutos; ?>
              </tr>
              <tr class="warning">
                <?php //print $linhaBrasil; ?>
              </tr>
              <!--<tr class="">-->
                
              <!--</tr>-->
              
             
            </table>
            <?php print $linhaSetor; ?>
        </small>
        </div>
    </div>

    

    <script>
        abrirSetorDrill('','', '');

        function abrirSetorDrill(setor, cor, nivel) {        
            $.ajax({
                type: "POST",
                url: '<?php print APP_CAMINHO ?>home/dadosDrill',
                data: {
                    setor: setor,
                    nivel: nivel,
                    mercado: '<?php print $nomeMercado; ?>',
                    cor: cor
                },
                dataType: 'JSON',
                
                beforeSend: function() {
                    //modalAguarde('show');
                },
                
                success: function(retorno) {

                    if(retorno.colorByPoint == true) {
                        $('#btnOpa').hide();
                    } else {
                        $('#btnOpa').show();
                    }

                    $('#btnOpa').unbind().click(function(e){
                        abrirSetorDrill(nivel, cor);
                    });

                   
                   atualizarDrill(retorno)
                }
            });    
        }    

        function atualizarDrill(dados) {
            console.log(dados);
            $('#container9').highcharts({
                chart: {
                    type: 'column',
                    backgroundColor: null
                },
                title: {
                    text: 'Prescription Share - <?php print $produtoEMS ?>'
                },
                subtitle: {
                    text: 'Visão por setores - <?php print $trmAtu ?>'
                },
                xAxis: {
                    
                    type: 'category',
                    /*categories: ,*/
                    crosshair: true,
                    labels: {
                        style: {
                            fontSize: '10px'
                        }
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    },
                    plotLines: [{
                        color: 'rgba(0, 0, 0, 0.5)',
                        width: 2,
                        value: dados.brasil,
                        dashStyle: 'ShortDot',
                        zIndex: 4,
                        label: {
                            text: '*' + dados.brasil + '%',
                            align: 'left',
                            x: -10,
                            style: {
                                color:'rgba(0, 0, 0, 0.5)',
                                fontSize: '10px',
                                padding: '5px',
                                fontWeight: "bold"
                            }
                        }
                    }]
                },
                legend: {
                    enabled: false,
                    
                },
                plotOptions: {
                    series: {
                        borderWidth: 0,
                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.1f}%'
                        },

                        dataLabels: {
                            enabled: true,
                            format: '{point.y:.0f}%',
                            backgroundColor: 'rgba(252, 255, 255, 0.7)',
                            style:{
                                fontSize: 12,
                                fontWeight: "bold"
                            }
                        },
                        
                        cursor: 'pointer',
                            events: {
                                click: function (event) {
                                    id = this.xData.indexOf(event.point.x);

                                    console.log(event);
                                    console.log(this);
                                    //console.log(this.points[id].color);


                                    //var cor = JSON.stringify(this.points[id].color);
                                    var cor = this.points[id].color;

                                    cor = (cor === undefined) ? this.userOptions.data[id].cor : cor;
                                  
                                  idSetor = this.userOptions.data[id].setor;
                                  sub = this.userOptions.data[id].sub;
                                  indice = this.userOptions.data[id].indice;
                                  nivel = this.userOptions.data[id].nivel;
                                  
                                  //cor = this.userOptions.data[id].color;

                                  

                                  if(sub > 0 && indice == 0) {
                                    abrirSetorDrill(idSetor, cor, nivel);
                                  }
                                  //console.log(this);
                                 // console.log(this);
                                 /*
                                    alert(
                                        this.userOptions.data[id].setor + ' clicked\n' +
                                        'Alt: ' + event.altKey + '\n' +
                                        'Control: ' + event.ctrlKey + '\n' +
                                        'Meta: ' + event.metaKey + '\n' +
                                        'Shift: ' + event.shiftKey
                                    );
                                    */
                                }
                            }
                            
                    }
                },
               
                color:  dados.colors
/*
                    radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 },
                    stops: [
                        [0, '#003399'],
                        [1, '#3366AA']
                    ]*/
                ,
                tooltip: {
                    headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                    pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                },
                credits: {
                    enabled: false
                },
                tooltip: {
                    headerFormat: '',
                    pointFormat: '<span style="font-size:10px;color:{point.color}">{point.name}</span><br/><b>{point.y:.0f}%</b> de participação<br/>'
                },
                series: [ dados]
            });

        }
    
    </script>
</div>
        </div>

</div>




</div>









<!--MODAL LISTA MÉDICOS-->
<div class="modal fade" id="modalListaMedico"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!--  <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Médicos</h5>
      </div>
      -->
      <div class="modal-body max-height" style="overflow-x:auto">
      
      
      </div>
          

      <div class="modal-footer panel-footer">
      	<form action="<?php print APP_CAMINHO ?>home/exportarMedicos" method="post">
        	<input type="hidden" name="txtMercado" value="<?php print $nomeMercado; ?>" />
            <input type="hidden" name="txtProduto" value="" />
            <input type="hidden" name="txtPeriodo" value="" />
            <input type="hidden" name="txtIdSetor" value="" />
            
             <button type="submit" class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-off"></span> Exportar
            </button>
        
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
        </form>

      </div>
    </div>
  </div>
</div>






<!--MODAL LISTA MÉDICOS-->
<div class="modal fade" id="modalBCG"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Análise de Prescrição</h5>
      </div>
      
      <div class="modal-body max-height" style="overflow-x:auto">
      

      
      </div>
          

      <div class="modal-footer">
      	<form>   
             <button type="submit" class="btn btn-primary btn-sm">
                <span class="glyphicon glyphicon-off"></span> Exportar
            </button>
        
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
        </form>

      </div>
    </div>
  </div>
</div>




<!--MODAL PESQUISAR-->
<div class="modal fade" id="modalPesquisarMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-user"></span> Médicos</h5>
      </div>
      
      <div class="modal-body max-height">
      
      	<div class="row">
        	<div class="col-lg-3">
            	<select class="form-control" id="comboCampoPesquisa" name="mercado">
                	<option value=0>Pesquisar por:</option>
                  	<option value="CRM">CRM</option>
                    <option value="NOME">NOME</option>
                    <option value="ENDERECO">ENDEREÇO</option>
                    <option value="BAIRRO">BAIRRO</option>
                    <option value="UF">UF</option>
                    <option value="CIDADE">CIDADE</option>
                    <option value="ESPECIALIDADE">ESPECIALIDADE</option>
                </select>
            </div>
            
            <div class="col-lg-9">
            	<div class="input-group">
                  <input type="text" class="form-control" id="txtCriterioPesquisa" placeholder="Critério...">
                  <span class="input-group-btn">
                    <button class="btn btn-primary" id="btnPesquisar" type="button">
                    	<span class="glyphicon glyphicon-remove"></span> Pesquisar
                    </button>
                  </span>
                </div>
    		</div>
            
        </div>
        
        <hr />
      
      <div id="resultadoPesquisaMedico" style="border:0px solid #ccc; height:300px;overflow:auto"></div>
      
      </div>
      
      
          

      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>









<!--MODAL PESQUISAR-->
<div class="modal" id="modalFichaMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-backdrop="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha do Médico</h5>
      </div>
      -->
      <div class="modal-body max-height">
 
 		
 
      </div>

      <div class="modal-footer">
           <a href="<?php print APP_CAMINHO ?>home/pdf" class="btn btn-primary btn-sm" id="btnImprimirPDF" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Exportar em PDF
            </a>   	
      
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>



<!--MODAL opiniao-->
<div class="modal fade" id="modalOpiniao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Enquete</h5>
      </div>
      
      <div class="modal-body">
 		<small>
        	<h3 class="text-info text-center"><span class="glyphicon glyphicon-thumbs-up"></span> Dê a sua opinião sobre a nossa ferramenta!!!</h3>
            <h4 class="text-center"><small>Isso nos ajuda a melhorá-la cada vez mais!</small></h4>
            <hr />
            

            	<button class="btn btn-default btn-sm btnOpiniao" type="button" value="1">Ruim</button>
                <button class="btn btn-default btn-sm btnOpiniao" type="button" value="2">Regular</button>
                <button class="btn btn-default btn-sm btnOpiniao" type="button" value="3">Bom</button>
                <button class="btn btn-default btn-sm btnOpiniao" type="button" value="4">Ótimo</button>
                <button class="btn btn-default btn-sm btnOpiniao" type="button" value="5">Excelente</button>

            
            <br />

            <br />
            
                <form id="formEnquete" action="<?php print APP_CAMINHO ?>home/salvarEnquete" method="POST">
                	<input type="hidden" name="txtNota" id="txtNota" value="0" />
                    <input type="hidden" name="txtIdSetor" id="txtIdSetor" value="<?php print appFunction::dadoSessao('id_setor'); ?>" />
                    <input type="hidden" name="txtIdColaborador" id="txtIdColaborador" value="<?php print appFunction::dadoSessao('id_colaborador'); ?>" />
                  <div class="form-group">
                    <label for="exampleInputPassword1">Observação/Sugestão</label>
                    <textarea class="form-control input-sm" rows="4" name="txtObservacao" id="txtObservacao"><?php print $detalhe ?></textarea>
                  </div>
                  
    
                </form>

            
 		</small>
      </div>

      <div class="modal-footer">
          	
      
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
            
             <button class="btn btn-info btn-sm" id="btnEnquete" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Enviar
            </button>   
      </div>
    </div>
  </div>
</div>




<!--MODAL opiniao-->
<div class="modal fade" id="modalRetornoOpiniao" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Enquete</h5>
      </div>
      
      <div class="modal-body">
 			<h3 class="text-info text-center"><span class="glyphicon glyphicon-thumbs-up"></span> Obrigado!</h3>
            <h4 class="text-center"><small>Sua opinião é muito importante para nós</small></h4>
      </div>

      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>

<script>
$(window).ready(function(e){
    resizePanelLeft();
});

$(document).ready(function(e) {
    
    

    $('#btnOpiniao').click(function(e) {
        $('#modalOpiniao').modal('show');
    });


    $('#modalOpiniao').on('show.bs.modal', function (event) {
	$('.btnOpiniao').removeClass('btn-primary');
	$('.btnOpiniao').addClass('btn-default');
        $('#txtNota').val(0);
    });
	
	$('.btnOpiniao').click(function(e) {
                $('.btnOpiniao').removeClass('btn-primary');
		$('.btnOpiniao').addClass('btn-default');
		
		$(this).removeClass('btn-default');
		$(this).addClass('btn-primary');
		
		$('#txtNota').val($(this).attr('value'));
    });
	
	
	$('#btnEnquete').click(function(e) {
		$.ajax({
			type: "POST",
		  	url: $('#formEnquete').attr('action'),
			data: $('#formEnquete').serialize(),
		  	
			beforeSend: function() {
				//modalAguarde('show');
			},
			
		  	success: function(retorno) {
                            
                 
				$('#modalOpiniao').modal('hide');
				
				$('#txtObservacao').val('');
				
				$('#modalRetornoOpiniao').modal('show');
				
				//modalAguarde('hide');
				//$('#modalBCG .modal-body').html(retorno);
				//$('#modalBCG').modal('show');
		   	}
		}); 
		
		
    });
    
    
    $('.cmbMercadoConc').click(function(e) {

    //if($(this).val() != "::SELECIONE O MERCADO::") {
        var merc = $(this).data('val');
        var conc = '';
        $('select[name="cmbConcorrentes[]"]').each(function (index, value) { 
            console.log((index+1));
            conc = conc + $('#' + $(this).attr('id')).val() + ',';
        });
        
        //console.log(conc);
        abrirMercado(merc, conc);

        

        //modalAguarde('show');
        //$('#formSelMercado').submit();
    //}
    });

    /* link que seleciona o mercado */
    $('.cmbMercado').click(function(e) {

        //if($(this).val() != "::SELECIONE O MERCADO::") {
            var merc = $(this).data('val');
           
            abrirMercado(merc, '');
        //}
    });


	/* combo que seleciona o mercado */
    $('#comboMercado').change(function(e) {

        if($(this).val() != "::SELECIONE O MERCADO::") {
		modalAguarde('show');
		$('#formSelMercado').submit();
            }
    });
	
	/* ativa os tooltips mostrando o nome dos 
	   produtos nas colunas da tabela */
	$('[data-toggle="tooltip"]').tooltip();
	
	/* pega os dados dos setores ao clicar
	   na coluna de um GD, GR ou GN */
	$('.abrirSetor').click(function(e) {
    	id = $(this).attr('cod');
	  	abrirSetor(id);
    });
	
	/* função que lista os médicos */
	listaMedico();
	//listaConquista();
	
	/* abre a caixa para pesquisa de médicos */
	$('#btnPesquisarMedico').click(function(e) {
		
       $('#modalPesquisarMedico').on('show.bs.modal', function (e) {
			$('#resultadoPesquisaMedico').html('');
			$('#txtCriterioPesquisa').val('');
			$('#comboCampoPesquisa').val(0);
	   });
	   
	   $('#modalPesquisarMedico').modal('show');
	   
	   $('#btnPesquisar').click(function(e) {
        	pesquisarMedico('576', $('#comboCampoPesquisa').val(), $('#txtCriterioPesquisa').val());
       });
	   
    });
});

function abrirMercado(mercado, concorrentes) {
    $('input[name="mercado"]').val(mercado);
    $('input[name="concorrente"]').val(concorrentes);

    modalAguarde('show');
    $('#formSelMercado').submit();
}

function resizePanelLeft() {
    var screenSize = ($('.panel-right').height() <  (screen.height-100)) ? (screen.height-100) : $('.panel-right').height(); 
    $('.panel-left').css('height', (screenSize));
    
   // a = $('#container-colab').height();
   // $('#container-setores').css('height', (screenSize -  a)-100);
   // $('#final').css('height', (screenSize)-100);
} 

function listaMedico() {
	
	$('.listarMedico').click(function(e) {

                //alert('<?php print APP_CAMINHO ?>home/listarMedicos');

		/* pega os campos */
	  	var produto = $(this).attr('prod');
		var periodo = $(this).attr('per');
		var setor   = $(this).attr('cod');	
	   	var mercado = '<?php print $nomeMercado ?>';
		
	  	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/listarMedicos',
		  	data: {
				txtMercado: mercado,
				txtProduto: produto,
				txtPeriodo: periodo,
				txtIdSetor: setor	
			},
			beforeSend: function() {
				modalAguarde('show');
			},
			
		  	success: function(retorno) {
				modalAguarde('hide');
				$('#modalListaMedico .modal-body').html(retorno);
				
				$('#modalListaMedico').on('shown.bs.modal', function () {
                                    $('#modalListaMedico').animate({ scrollTop: 0 }, 'slow');
				});

				$('#modalListaMedico').modal('show');
				
				$('#modalListaMedico').on('hidden.bs.modal', function (e) {
				  $('body').css('padding', '0px');
				})
				
				$('.fichaMedico').click(function(e) {
                                    fichaMedico($(this).attr('crm'));
                                });
				
				$('input[name="txtMercado"]').val(mercado);
				$('input[name="txtProduto"]').val(produto);
				$('input[name="txtPeriodo"]').val(periodo);
				$('input[name="txtIdSetor"]').val(setor);
		   	}
		});      
    });	
}


function listaConquista() {
	
	$('.listarConquista').click(function(e) {

		/* pega os campos */
	  	var produto = $(this).attr('prod');
		var setor   = $(this).attr('cod');	
	   	var mercado = '<?php print $nomeMercado ?>';
		
	  	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/matrizBCG',
			data: {
				mercado: mercado,
				produto: produto,
				setor: setor
			},
		  	
			beforeSend: function() {
				modalAguarde('show');
			},
			
		  	success: function(retorno) {
				//alert(retorno);
				modalAguarde('hide');
				$('#modalBCG .modal-body').html(retorno);
				$('#modalBCG').modal('show');
		   	}
		});      
    });	
}

function pesquisarMedico(idSetor, campo, criterio) {
	
	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/pesquisarMedico',
		  	data: {
				idSetor: idSetor,
				campo: campo,
				criterio: criterio	
			},
			
			beforeSend: function() {
				$('#resultadoPesquisaMedico').html('<div class="loading-gif"><p class="text-center"><small >Processando</small></p><img class="center-block" src="<?php print APP_CAMINHO ?>public/img/loading.gif" /></div>');
			},
			
		  	success: function(retorno) {
				modalAguarde('hide');
				$('#resultadoPesquisaMedico').html(retorno);
				$('.fichaMedico').click(function(e) {
                    fichaMedico($(this).attr('crm'));
                });
		   	}
	});  
}

function fichaMedico(crm) {
	$.ajax({
			type: "POST",
		  	url: '<?php print APP_CAMINHO ?>home/fichaMedico',
		  	data: {
				CRM: crm
			},
			
			beforeSend: function() {
				//modalAguarde('show');
			},
			
		  	success: function(retorno) {
				//modalAguarde('hide');
				$('#modalFichaMedico .modal-body').html(retorno);
				$('#modalFichaMedico').modal('show');
				
				$('#btnImprimirPDF').attr('href', '<?php print APP_CAMINHO ?>home/pdf/'+crm);
		   	}
	}); 
}

function abrirSetor(id) {
	
	if($('#'+id).html() != "") {
		
		$('td[cod="'+id+'"] .loading').show();
		$('#'+id).slideUp(100, function(e) {
			$('#'+id).html("");
			$('td[cod="'+id+'"] .loading').hide();
		});
		
	} else {
	
		$.ajax({
			type: "POST",
			url: '<?php print APP_CAMINHO ?>home/selecionarSetor',
			data: {
				idSetor: id,
				mercado: '<?php print $nomeMercado ?>'
			},
			beforeSend: function(e) {
				$('td[cod="'+id+'"] .loading').show();
			},
			success: function(retorno) {
				$('td[cod="'+id+'"] .loading').hide();
				$('#'+id).hide();
				$('#'+id).html(retorno);
				$('#'+id).slideDown(100);

				$('#'+id+' .abrirSetor').click(function(e) {
                    id = $(this).attr('cod');
	  				abrirSetor(id);
                });
				
				listaMedico();
				listaConquista();
			}
		});	
	
	}
}
</script>

<script>
/* grafico de pizza */
$(function () {
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
        return {
            radialGradient: {
                cx: 0.5,
                cy: 0.3,
                r: 0.7
            },
            stops: [
                [0, color],
                [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
            ]
        };
    });

    $(document).ready(function () {

        // Build the chart
        $('#container').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie',
                margin: [0,0,0,100],
                spacingTop: 0,
                spacingBottom: 0,
                spacingLeft: 0,
                spacingRight: 0,
            },
            xAxis: {
                min: 0,
                plotLines: [{
                    color: 'red',
                    value: '18',
                    width: '2',
                    zIndex: 2
                }]
            },
            title: {
                text: null
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>'
            },
            plotOptions: {
                series:{
                    dataLabels: {
                        enabled: true,
                        format: '{point.y:.0f}%'
                        /*formatter: function() {
                            return Math.round(this.percentage*100)/100 + ' %';
                        },*/
                    },
                },
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false,
						formatter: function() {
							return Math.round(this.percentage*100)/100 + ' %';
						},
                    },
                    showInLegend: true
                }
            },
			legend: {
				align: 'left',
				layout: 'vertical',
				verticalAlign: 'middle',
				itemStyle: {
					fontWeight: '',
					fontSize: '10px'
				}
			},
			
            series: [{
                name: "Mkt Share",
                colorByPoint: true,
                data: <?php print $arrGraficoShareProduto ?>
            }]
        });
    });
});

  
/* grafico de colunas drilldown */
$(function () {

    // Create the chart
    $('#container3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Prescription Share - <?php print $produtoEMS ?>'
        },
        subtitle: {
            text: 'Visão por setores - <?php print $trmAtu ?>'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: null
            },
			
            plotLines: [{
                color: 'rgba(0, 0, 0, 0.5)',
                width: 2,
                value: <?php print ($linhaBrasil + 0) ?>,
                dashStyle: 'ShortDot',
                zIndex: 4,
                label: {
                    text: '*<?php print appFunction::formatarMoeda($linhaBrasil,0) ?>%',
                    align: 'left',
                    x: -10,
                    style: {
                        color:'rgba(0, 0, 0, 0.5)',
                        fontSize: '10px',
                        padding: '5px',
                        fontWeight: "bold"
                    }
                }
            }]
        },
        legend: {
            enabled: false
        },
        plotOptions: {
			
            series: {
              
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.0f}%',
                    backgroundColor: 'rgba(252, 255, 255, 0.7)',
                    style:{
                        fontSize: 12,
                        fontWeight: "bold"
                    }
                }
            }
        },
		
		color: {
			radialGradient: { cx: 0.5, cy: 0.5, r: 0.5 },
			stops: [
			   [0, '#003399'],
			   [1, '#3366AA']
			]
		},
        tooltip: {
            headerFormat: '',
            pointFormat: '<span style="font-size:10px;color:{point.color}">{point.name}</span><br/><b>{point.y:.0f}%</b> de participação<br/>'
        },

        series: [{
            name: 'Região',
            colorByPoint: true,
            data: <?php print $dados ?>
        }],
        drilldown: {
			drillUpButton: {
                relativeTo: 'spacingBox',
                position: {
                    y: 0,
                    x: 0
                }
            },
			
			series : <?php print $series ?> 

        }
    });
});

</script>