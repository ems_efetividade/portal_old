<?php

class appView {
	
	public $dados = array();
	
	public function set($nome, $valor) {
		$this->dados[$nome] = $valor;	
	}
	
	public function bind($nome, $valor) {
        $this->dados[$nome] = $valor; 
    }
	
	public function get($nome='') {

        if ($nome == '') {
            return $this->dados;
        }
        else {
            if (isset($this->dados[$nome]) && ($this->dados[$nome] != '')) {
                return $this->dados[$nome];
            }
            else {
                return '';
            }
        }
    }
	
	public function render($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		
		if (file_exists("app/view/{$arquivo}.php")) {
            include "app/view/{$arquivo}.php";
			
        }
		
	}
	
	public function renderToString($arquivo) {
		foreach($this->get() as $chave => $item) {
            $$chave = $item;
        }
		
		if (file_exists($arquivo)) {
			ob_start();
			include($arquivo);
			return ob_get_clean();
        }
		
	}
	
}

?>