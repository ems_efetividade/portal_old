//### função que chama uma pagina em ajax
function requisicaoAjax(caminho) {
	var requisicao;
	
	//aguarde(1);
	
	$.ajaxSetup({async:false});
	
	$.post(caminho, function(retorno) {
		requisicao = retorno
	}).fail(function(xhr, textStatus, errorThrown) {
		//alert('ERRO: '+ xhr.responseText + ' ' +caminho);
  	});
	
	$.ajaxSetup({async:true});


	//aguarde(0);
	return requisicao;
}

function msgBox(texto) {
	$('#modalMsgBox #msgboxTexto').html(texto);
	$('#modalMsgBox').modal('show');
}	

function enviarFormulario(form, callback, msgbox) {

	$.ajax({
		 type: "POST",
		  url: form.attr('action'),
		  data: form.serialize(),
		  success: function(u) {
							  
			if(u == '') {
				window.location.href = $(location).attr('href');
			} else {
				
				msgBox(u);
				$('#alertErro').html(u).show();
				setTimeout(function() {
					$('#alertErro').hide();
				}, 5000);
			}
		   }
		})
}

function modalAguarde(ctrl) {
	$('#modalAguarde').modal(ctrl);
	
	$('#modalAguarde').on('shown.bs.modal', function (e) {
	  $('body').css('padding', '0px');
	});
	
	$('#modalAguarde').on('hidden.bs.modal', function (e) {
	  $('body').css('padding', '0px');
	})
}