<?php

require_once('lib/appConexao.php');
require_once('lib/appController.php');
require_once('lib/appFunction.php');

require_once('lib/_chart/chart/ePie.php');
require_once('lib/_chart/chart/eLine.php');
require_once('app/model/SignoModel.php');

require_once('app/model/mEmpresa.php');
require_once('app/model/mEmpresaRegua.php');
require_once('app/model/mRegua.php');
require_once('app/model/mContato.php');
require_once('app/model/mContatoTipo.php');
require_once('app/model/mEmpresaPlataforma.php');
require_once('app/model/mPlataforma.php');
require_once('app/model/mCrm.php');
require_once('app/model/mMedico.php');
require_once('app/model/mBasePxSoma.php');
require_once('app/model/mEmail.php');
require_once('app/model/mTelefone.php');
require_once('app/model/mEmpresaEmail.php');
require_once('app/model/mEmpresaTelefone.php');
require_once('app/model/mEmpresaEspecialidade.php');
require_once('app/model/mEmpresaConvenio.php');
require_once('app/model/mEmpresaResponsavel.php');
require_once('app/model/mEmpresaConsultas.php');
require_once('app/model/mConvenio.php');
require_once('app/model/mContatoTelefone.php');
require_once('app/model/mEspecialidade.php');
require_once('app/model/mMesoregiao.php');
require_once('app/model/mEmpresaObjetivo.php');
require_once('app/model/mColaborador.php');

require_once('app/model/mPxPeriodo.php');
require_once('app/model/mBaseSoma.php');

class homeController extends appController {
	
	private $signo;
	private $empresa;
	private $regua;
	private $empresaRegua;
	private $empresaResponsavel;
	
	public function homeController() {
		$this->signo       		  = new SignoModel();
		$this->empresa      	  = new mEmpresa();
		$this->regua        	  = new mRegua();
		$this->empresaRegua 	  = new mEmpresaRegua();
		$this->empresaResponsavel = new mEmpresaResponsavel();


		//$this->empresaResponsavel->injection(new mColaborador());
		$this->empresa->injection(new mEmpresaResponsavel(), new mColaborador());
	}

	public function main() {
		$view = [
			'emps' => $this->empresa->listar(),
		];

		$this->set('view', $view);
		$this->render('CNPJView');	
	}

	public function iniciar() {
		$emp = new mEmpresa();
		$emp->selecionarUltimoCadastro();

		//$_POST['id_cnpj'] = $emp->getId();
		//$this->ficha_cnpj();
		echo $emp->getCnpj();

	}


	public function ficha_cnpj() {
		$CNPJ = $_POST['id_cnpj'];

		/* Selecionando a Empresa */
		
		$this->empresa->selecionarByCNPJ($CNPJ);

		//var_dump($this->empresa);

		/* Selecionar Mesoregiao */
		$mesoregiao = new mMesoregiao();
		$mesoregiao->selecionar($this->empresa->getMesoregiao());

		/* Selecionar o Objetivo da empresa */
		$empresaObjetivo = new mEmpresaObjetivo();
		$empresaObjetivo->selecionarByEmpresa($this->empresa->getId());

		/* Selecionando o Email da Empresa */
		$empresaEmail = new mEmpresaEmail();
		$empresaEmail->injection(new mEmail());
		$empresaEmail->selecionarByEmpresa($this->empresa->getId());

		/*Listando o telefone da empresa */
		$empresaTelefone = new mEmpresaTelefone();
		$empresaTelefone->injection(null, new mTelefone());
		$empresaTelefones = $empresaTelefone->listarByEmpresa($this->empresa->getId());

		/* Listando os Convenios */
		$empresaConvenio = new mEmpresaConvenio();
		$empresaConvenio->injection(null, new mConvenio());
		$empresaConvenios = $empresaConvenio->listarByEmpresa($this->empresa->getId());

		/* Listando as Especialidades */
		$empresaEspecialidade = new mEmpresaEspecialidade();
		$empresaEspecialidade->injection(null, new mEspecialidade());
		$empresaEspecialidades = $empresaEspecialidade->listarByEmpresa($this->empresa->getId());

		/* Listando os Responsáveis pela empresa */
		//$empresaResponsavel = new mEmpresaResponsavel();
		//$empresaResponsavel->injection(new mColaborador(), new mEmpresa());
		//$empresaResponsavel = $empresaResponsavel->selecionarByEmpresa($this->empresa->getId());


		/* Lisando as plataformas */
		$empresaPlataforma = new mEmpresaPlataforma();
		$empresaPlataforma->injection(null, new mPlataforma());
		$plataformas = $empresaPlataforma->listarByEmpresa($this->empresa->getId());

		// Listando valores de consultas
		$empresaConsulta = new mEmpresaConsultas();
		$consulta = $empresaConsulta->selecionarByEmpresa($this->empresa->getId());

		/* Listando os Contatos */
		$contato = new mContato();
		$contatos = [];
		$listaContatos = $contato->listarByEmpresa($this->empresa->getId());

		if($listaContatos) {
			foreach($listaContatos as $cont) {
				$contatoMedico = new mContato();
				$contatoTelefone = new mContatoTelefone();

				$contatoTelefone->injection(new mTelefone());
				$contatoMedico->injection(new mContatoTipo(), null, $contatoTelefone);

				$medico = new mMedico();
				$medico->injection(new mCrm, $contatoMedico);
				$medico->selecionarByContato($cont->getId());
				$contatos[] = $medico;
			}
		}

		/* Selecionando a Régua */
		$empresaRegua = new mEmpresaRegua();
		$empresaRegua->injection(new mRegua());
		$reguaEmpresa = $empresaRegua->selecionarByEmpresa($this->empresa->getId());

		/* Somando PX */
		$meso = $this->empresa->getMesoregiao();
		$somaMeso = new mBasePxSoma();
		
		if($plataformas) {
			foreach($plataformas as $plat) {
				$fkPlataforma[] = $plat->Plataforma()->getId();
			}

			$somaMeso2 = new mBaseSoma();
			$somaMeso2->setFkEmpresa($this->empresa->getId());
			$graficoMktShare = $somaMeso2->marketShareCnpj($fkPlataforma);

			$dadosPX = $somaMeso->participacaoMesoEmpresa($this->empresa->getId(), $meso, $fkPlataforma);
			//$graficoMktShare = $somaMeso->marketShareCnpj($this->empresa->getId(), $fkPlataforma);
			//$graficoMktShare =$dadosPX;
		}
		$this->empresaRegua->selecionarByEmpresa($this->empresa->getId());
		$this->regua->selecionar($this->empresaRegua->getFkRegua());
		$dadosGrafLinha = array($graficoMktShare, $empresaObjetivo);

		$tipoContatos = new mContatoTipo();
		$periodo =  new mPxPeriodo();

		$periodo->selecionarPeriodoAtual();

		$this->set('cnpj', $this->empresa);
		$this->set('empresaTelefones', $empresaTelefones);
		$this->set('empresaEmail', $empresaEmail);
		$this->set('empresaConvenios', $empresaConvenios);
		//$this->set('empresaResponsavel', $empresaResponsavel);
		$this->set('empresaEspecialidades', $empresaEspecialidades);
		$this->set('empresaConsulta', $consulta);
		$this->set('empresaObjetivo', $empresaObjetivo);
		$this->set('part', $this->pie($dadosPX));
		$this->set('mktShare', $this->line($dadosGrafLinha));
		$this->set('contatos', $contatos);
		$this->set('reguaEmpresa',    $reguaEmpresa);
		$this->set('plataformas', $plataformas);
		$this->set('dadosPX', $dadosPX);
		$this->set('dadosShare', $graficoMktShare);
		$this->set('mesoregiao', $mesoregiao);
		$this->set('periodo', $periodo);
		$this->set('tipoContatos', $tipoContatos->listar());

		echo $this->renderToString('CNPJFichaView');
	}

	public function atualizarGrafRepres() {
		require_once('app/model/mEmpresa.php');
		require_once('app/model/mPlataforma.php');
		require_once('app/model/mBasePxSoma.php');

	
		$plataformas = ($_POST['plataformas']);
		$id_empresa  = $_POST['id_empresa'];


		$somaMeso = new mBasePxSoma();

		$empresa = new mEmpresa();
		$empresa->selecionar($id_empresa);

		/* Selecionar o Objetivo da empresa */
		$empresaObjetivo = new mEmpresaObjetivo();
		$empresaObjetivo->selecionarByEmpresa($id_empresa);

		
		$somaMeso2 = new mBaseSoma();
		$somaMeso2->setFkEmpresa($id_empresa);
		

		$dadosPX = $somaMeso->participacaoMesoEmpresa($id_empresa, $empresa->getMesoregiao(), $plataformas);
		//$graficoMktShare = $somaMeso->marketShareCnpj($id_empresa, $plataformas);
		$graficoMktShare = $somaMeso2->marketShareCnpj($plataformas);

		$dadosGrafLinha = array($graficoMktShare, $empresaObjetivo);

		$json = [
			'repres' =>	$this->pie($dadosPX, false),
			'mktshare' =>	$this->line($dadosGrafLinha, false),
			'query' => $somaMeso2->qy($plataformas)
		];

		echo json_encode($json, JSON_NUMERIC_CHECK);
	}

	public function pie(mBasePx $dados=null, $json=true) {

		if($dados === null) {
			return false;
		}

		$pie = new ePie();

		
		$pie->title->textAlign('center');
		
		//$pie->title->text(appFunction::formatarMoeda(($dados->getM00()*100), 1, "%"));

		$vlr = round(($dados->getM00() * 100),2);
		$vlr = number_format($vlr, 2, ",", '');
		$vlr = $vlr . '%';

		$pie->title->text($vlr);

		$pie->title->textStyle->fontSize('30');

		$pie->title->subtext('Mkt. Sh. Rx.');
		$pie->title->top('29%');
		$pie->title->left('50%');
	
		
		$pie->serie->data->name("PX");
		$pie->serie->data->value(round(($dados->getM00()*100),1));
		$pie->serie->label->show(true);
		$pie->serie->label->position('center');
		$pie->serie->data->add();

		$pie->serie->data->name("MESO");
		$pie->serie->data->value(100 - ($dados->getM00() * 100));
		$pie->serie->label->show(false);
		$pie->serie->data->add();



		$pie->legend->textStyle()->fontSize(10);
		$pie->legend->show(true);
		$pie->legend->orient('horizontal');
		$pie->legend->left('center');
		$pie->legend->top('bottom');
		

		$pie->tooltip->formatter('{b} : {d}%');
		$pie->tooltip->trigger('item');

		$pie->serie->type('pie');
		$pie->serie->radius(['50%', '70%']);
		$pie->serie->center(['50%', '40%']);

		//$pie->serie->label->show(true);
		//$pie->serie->label->position('inner');
		//$pie->serie->label->fontSize(10);

		$pie->serie->add();

		return ($json == true) ? $pie->renderToJson() : $pie->render();
	} 

	public function line($dados=null, $json=true) {

		if($dados[0] === null) {
			return false;
		}


		$periodo =  new mPxPeriodo();
		$dado = $dados[0];
		$obj = $dados[1];


		$lbl[] = 'Market Share';
		$lbl[] = 'Objetivo';

		$line = new eLine();




		$line->serie->type('bar');
		
		$line->serie->label->color('#777777');
		$line->serie->label->fontSize(10);
		$line->serie->label->fontWeight('bold');

		$line->serie->name($lbl[0]);
		$line->serie->barWidth('60%');
		$line->serie->label->formatter('{c}%');
		//$line->serie->label->normal->show(true);
		$line->serie->label->position('top');
		$line->serie->label->show(true);
		
		$line->serie->smooth(true);
		$line->serie->markLine->lineStyle->color('#000');

		$line->serie->markLine->data->name('Objetivo');
		$line->serie->markLine->label->show(true);
		$line->serie->markLine->label->formatter('{c}%');
		$line->serie->markLine->data->dataArray(array(array('name' => 'Objetivo', 'yAxis' => round($obj->getObjetivo(),1))));

		//$line->serie->markPoint->data->dataArray(array(array('type' => 'max', 'symbol' => 'arrow')));

		$line->serie->data->dataArray([
			round($dado->getM11()*100,1),
			round($dado->getM10()*100,1), 
			round($dado->getM09()*100,1), 
			round($dado->getM08()*100,1), 
			round($dado->getM07()*100,1), 
			round($dado->getM06()*100,1),
			round($dado->getM05()*100,1), 
			round($dado->getM04()*100,1), 
			round($dado->getM03()*100,1), 
			round($dado->getM02()*100,1), 
			round($dado->getM01()*100,1), 
			round($dado->getM00()*100,1)
		]);
		$line->xAxis->data($periodo->selecionarCabecalho(13));
		$line->serie->add();

		
			//$line->grid->height('220px');
				
		$line->grid->left('26');
		$line->grid->right('26');
		$line->grid->top('20');
		$line->grid->bottom('20');


		//$line->tooltip->formatter(' {a} : {c}% ');
		$line->tooltip->trigger('axis');


		$line->grid->containLabel(false);

		$line->xAxis->type('category');
		$line->xAxis->boundaryGap(true);
		//$line->xAxis->data(['Mar/19', 'Abr/19', 'Mai/19', 'Jun/19', 'Jul/19', 'Ago/19', 'Set/19', 'Out/19', 'Nov/19', 'Dez/19', 'Jan/19', 'Fev/19']);
		
		$line->xAxis->axisLabel()->fontSize(9);
		$line->xAxis->axisLabel()->interval(0);

		$line->xAxis->axisLine()->show(true);
		$line->yAxis->axisLine()->show(true);
		
		//$line->xAxis->axisLabel()->rotate(90);
	
		
		$maxObj  = round($obj->getObjetivo());
		$dadoMax = round(($dado->getMax()*100));

		$maxObj += 10;
		$dadoMax  = ($dadoMax - ($dadoMax%10)) + 10;

		$max = ($maxObj > $dadoMax) ? $maxObj : $dadoMax;
		//$max = ($obj->getObjetivo() > $dado->getMax()) ? round(($obj->getObjetivo()+10), 1) : round($dado->getMax() * 100, 1);


		$line->yAxis->min(0);
		$line->yAxis->max($max);
		$line->yAxis->type('value');

		$line->yAxis->axisLabel()->fontSize(10);
		$line->yAxis->axisLabel()->show(true);
		$line->yAxis->splitNumber(5);
		$line->legend->show(false);
		$line->legend->textStyle()->color('#777');
		$line->legend->textStyle()->fontSize(11);
		$line->legend->data($lbl);
		
		$line->legend->orient('center');
		$line->legend->orient('horizontal');
		$line->legend->left('center');
		$line->legend->top('bottom');


		//$line->label->show(true);
		//$line->label->position('top');

		return ($json == true) ? $line->renderToJson() : $line->render();
	}











	public function listar() {
		$this->render('CNPJView');
	}

	

	public function paginacao($dados) {
		$dados['pagina'] = isset($dados['pagina']) ? $dados['pagina'] : 1;

		$paginas = ceil($dados['total'] / $dados['max_pag']);
		$inicio = (($dados['max_pag'] * $dados['pagina']) - $dados['max_pag']+1);
		$maximo = $dados['max_pag'] * $dados['pagina'];


		$pag_atual = '';
		for($i=1;$i<$paginas; $i++) {
			$paginacao[] = $i;
		}

		return [
			'paginacao' => $paginacao,
			'pag_atual' => $dados['pagina'],
			'limite' => [
				'de' => $inicio,
				'ate' => $maximo
			]
		];
	}
	

	
}


?>