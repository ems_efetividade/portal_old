<?php

require_once('lib/appConexao.php');

class SignoModel extends appConexao {

    
    public function __construct() {
        parent::__construct();

    }
    

    public function getCNPJ() {
        $query = "EXEC proc_sgn_listarCNPJ";

        return $this->executarQueryArray($query);
    }

    public function dadosCNPJ($id_cnpj) {
        $query = "SELECT * FROM PA_EMPRESA WHERE ID = ".$id_cnpj." AND ATIVO = 1";
        return $this->executarQueryArray($query);
    }

    public function plataformaCNPJ($id_cnpj) {
        $query = "SELECT
                    B.ID,
                    B.NOME
                FROM 
                    [PA_EMPRESA_PLATAFORMA] A
                    INNER JOIN PA_PLATAFORMA B ON A.FK_PLATAFORMA = B.ID
                WHERE
                    FK_EMPRESA = ".$id_cnpj."";
                    
        return $this->executarQueryArray($query);
    }

    public function partMesoRegiao($id_empresa, $id_plataformas) {
        $filtro = ($id_plataformas == "") ? "" :  "" . implode(',', $id_plataformas) . "";
        
        $query = "SELECT 
                        (CAST(SUM(A.M01) AS FLOAT) / CAST(sum(B.M01) AS FLOAT)) * 100 AS SHARE_CNPJ_ANT,
                        (1 - CAST(SUM(A.M01) AS FLOAT) / CAST(sum(B.M01) AS FLOAT)) * 100 AS SHARE_TOTAL_ANT,
                        (CAST(SUM(A.M00) AS FLOAT) / CAST(sum(B.M00) AS FLOAT)) * 100 AS SHARE_CNPJ,
                        (1 - CAST(SUM(A.M00) AS FLOAT) / CAST(sum(B.M00) AS FLOAT)) * 100 AS SHARE_TOTAL
                        FROM 
                        VW_SGN_EMPRESA_PLATAFORMA A INNER JOIN PA_PX_MESO_PLATAFORMA B ON A.MESOREGIAO = B.MESO AND A.FK_PLATAFORMA = B.FK_PLATAFORMA
                    WHERE 
                        A.FK_PLATAFORMA IN (".$filtro.") AND 
                        A.ID = ".$id_empresa;
                    
        return $this->executarQueryArray($query);                       
    }

    public function evolucaoCNPJ($id_cnpj, $id_plataformas) {
        $filtro = ($id_plataformas == "") ? "" :  "" . implode(',', $id_plataformas) . "";
        $query = "
                SELECT
                    CAST(SUM(P01) AS FLOAT) / NULLIF(CAST(SUM(M01) AS FLOAT),0) AS M01,
                    CAST(SUM(P00) AS FLOAT) / NULLIF(CAST(SUM(M00) AS FLOAT),0) AS M00,
                    CAST(SUM(P11) AS FLOAT) / NULLIF(CAST(SUM(M11) AS FLOAT),0) AS M11,
                    CAST(SUM(P10) AS FLOAT) / NULLIF(CAST(SUM(M10) AS FLOAT),0) AS M10,
                    CAST(SUM(P09) AS FLOAT) / NULLIF(CAST(SUM(M09) AS FLOAT),0) AS M09,
                    CAST(SUM(P08) AS FLOAT) / NULLIF(CAST(SUM(M08) AS FLOAT),0) AS M08,
                    CAST(SUM(P07) AS FLOAT) / NULLIF(CAST(SUM(M07) AS FLOAT),0) AS M07,
                    CAST(SUM(P06) AS FLOAT) / NULLIF(CAST(SUM(M06) AS FLOAT),0) AS M06,
                    CAST(SUM(P05) AS FLOAT) / NULLIF(CAST(SUM(M05) AS FLOAT),0) AS M05,
                    CAST(SUM(P04) AS FLOAT) / NULLIF(CAST(SUM(M04) AS FLOAT),0) AS M04,
                    CAST(SUM(P03) AS FLOAT) / NULLIF(CAST(SUM(M03) AS FLOAT),0) AS M03,
                    CAST(SUM(P02) AS FLOAT) / NULLIF(CAST(SUM(M02) AS FLOAT),0) AS M02          
                FROM
                    PA_CONTATO A
                INNER JOIN PA_MEDICO B ON A.ID = B.FK_CONTATO
                INNER JOIN PA_CRM C ON C.ID = B.FK_CRM
                INNER JOIN PA_EMPRESA E ON E.ID = A.FK_EMPRESA
                INNER JOIN PA_EMPRESA_PLATAFORMA F ON F.FK_EMPRESA = E.ID
                INNER JOIN PA_BASE_PX D ON D.FK_CRM = C.ID AND F.FK_PLATAFORMA = D.FK_PLATAFORMA
                WHERE A.FK_EMPRESA = ".$id_cnpj." AND E.ATIVO = 1 AND F.FK_PLATAFORMA IN (".$filtro.")";
            return $this->executarQueryArray($query);
    }


    public function contatos($id_empresa) {
        $query = "SELECT DISTINCT 
                D.ID AS ID_PXSHARE,
                C.UF + C.NUMERO AS CRM,
                A.NOME,
                E.DESCRICAO,
                A.DECISOR
            FROM 
                [PA_CONTATO] A
            INNER JOIN PA_MEDICO B ON A.ID = B.FK_CONTATO	
            INNER JOIN PA_CRM C ON C.ID = B.FK_CRM
            LEFT JOIN PS_DIM_CRM D ON D.CRM = C.UF + C.NUMERO
            INNER JOIN PA_CONTATO_TIPO E ON E.ID = A.FK_CONTATO_TIPO
            WHERE 
                A.ATIVO = 1 AND 
                A.FK_EMPRESA = " . $id_empresa;

        return $this->executarQueryArray($query);                
    }

    public function convenios($id_empresa) {
        $query = "SELECT B.* FROM
                PA_EMPRESA_CONVENIO A
            INNER JOIN
                PA_CONVENIO B ON A.FK_CONVENIO = B.ID	
            WHERE 
                A.ID = " . $id_empresa;
        
        

        return $this->executarQueryArray($query);    
    }

    public function getPlataforma() {
        $query = "exec proc_sgn_listarPlataforma";
        return $this->executarQueryArray($query);
    }

    public function graficoPartCNPJ($ids) {
        $filtro = ($ids == "") ? "" : " AND B.FK_PLATAFORMA IN (" . implode(',', $ids) . ")";
        $query = "
                SELECT
                    COUNT(DISTINCT A.CNPJ) AS NUM_CNPJ,
                    C.NOME AS PLATAFORMA
                FROM 
                    PA_EMPRESA A
                INNER JOIN PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                INNER JOIN PA_PLATAFORMA C ON C.ID = B.FK_PLATAFORMA
                WHERE A.ATIVO = 1 " . $filtro . "
                group by
                    C.NOME
        ";

        //echo $query;
        return $this->executarQueryArray($query);
        
    }


    public function getTopCNPJ($ids) {
        $filtro = ($ids == "") ? "" : "(" . implode(',', $ids) . ")";
        $query = "WITH PLAT AS (
                    SELECT  SUM(M00) AS M00, SUM(M01) AS M01 FROM VW_SGN_CNPJ WHERE ID_PLATAFORMA IN ".$filtro." 
                )
                
                SELECT TOP 5
                    A.ID_EMPRESA,
                    D.FANTASIA,
                    CAST(SUM(A.M01) AS FLOAT) / NULLIF(CAST(SUM(B.M01) AS FLOAT),0) * 100 AS S01,
                    CAST(SUM(A.M00) AS FLOAT) / NULLIF(CAST(SUM(B.M00) AS FLOAT),0) * 100 AS S00
                FROM 
                    VW_SGN_CNPJ A
                    INNER JOIN PA_EMPRESA D ON D.ID = A.ID_EMPRESA 
                    CROSS JOIN PLAT B 
                    
                WHERE
                    A.ID_PLATAFORMA IN ".$filtro." 	
                GROUP BY 
                    A.ID_EMPRESA,
                    D.FANTASIA
                ORDER BY CAST(SUM(A.M00) AS FLOAT) / NULLIF(CAST(SUM(B.M00) AS FLOAT),0) DESC";
//echo $query;
            return $this->executarQueryArray($query);
    }

    public function getCountCNPJ($ids) {
        $filtro = ($ids == "") ? "" : " AND B.FK_PLATAFORMA IN (" . implode(',', $ids) . ")";

        $query = "
                SELECT
                    COUNT(DISTINCT A.CNPJ) AS NUM_CNPJ
                FROM 
                    PA_EMPRESA A
                INNER JOIN PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                WHERE A.ATIVO = 1 " . $filtro . "";

        $rs =  $this->executarQueryArray($query);
        return $rs[1]['NUM_CNPJ'];
    }

    public function tabelaCNPJ($ids="", $limite=['de' => 0, 'ate' => 20]) {
        $filtro = ($ids == "") ? "" : " AND B.FK_PLATAFORMA IN (" . implode(',', $ids) . ")";

        $query = "
            SELECT * FROM (
                
                SELECT ROW_NUMBER() OVER ( ORDER BY X.ID ) AS RANGE, X.* FROM (
                    SELECT DISTINCT
                        A.ID,
                        A.CNPJ,
                        A.RAZAO_SOCIAL,
                        A.FANTASIA,
                        C.PLATAFORMAS,
                        E.POTENCIAL,
                        E.RELACIONAMENTO
                    FROM 
                        PA_EMPRESA A
                    INNER JOIN PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                    INNER JOIN (
                    SELECT o.FK_EMPRESA, PLATAFORMAS= STUFF((
                        SELECT ' / ' + A.NOME 
                        FROM PA_PLATAFORMA AS a
                        INNER JOIN PA_EMPRESA_PLATAFORMA AS b
                        ON A.id = b.FK_PLATAFORMA
                        WHERE b.FK_EMPRESA = O.FK_EMPRESA ORDER BY A.NOME ASC
                        FOR XML PATH, TYPE).value(N'.[1]', N'varchar(max)'), 1, 2, '') 
                    FROM PA_EMPRESA_PLATAFORMA AS o
                    GROUP BY o.FK_EMPRESA

                    ) C ON C.FK_EMPRESA = B.FK_EMPRESA
                    INNER JOIN PA_EMPRESA_REGUA D ON D.FK_EMPRESA = A.ID
                    INNER JOIN PA_REGUA E ON E.ID = D.FK_REGUA
                    WHERE A.ATIVO = 1  " . $filtro . "
                ) X
            ) A WHERE A.RANGE BETWEEN ".$limite['de']." AND ".$limite['ate']."";

            //return $query;
        $dados = $this->executarQueryArray($query);

        return $dados;
    }

    public function contarListaCNPJPlataforma($ids) {
        $filtro = ($ids == "") ? "" : " AND C.ID IN (" . implode(',', $ids) . ")";

        $queryContar = "
                SELECT 
                COUNT(*) AS TOTAL
                FROM 
                    PA_EMPRESA A
                INNER JOIN PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                INNER JOIN PA_PLATAFORMA C ON C.ID = B.FK_PLATAFORMA
                INNER JOIN PA_EMPRESA_REGUA D ON D.FK_EMPRESA = A.ID
                INNER JOIN PA_REGUA E ON E.ID = D.FK_REGUA
                WHERE A.ATIVO = 1 " . $filtro . "";

        $contar = $this->executarQueryArray($queryContar);
        return $contar[1]['TOTAL'];
    }

    public function filtro($query) {
        return $this->executarQueryArray($query);
    }
    

    public function resumo($ids) {
        $filtro = ($ids == "") ? "" : " AND B.FK_PLATAFORMA IN (" . implode(',', $ids) . ")";
        $query = "

                SELECT 
                    'N de CPNJ(s)' AS TITULO,
                    COUNT(DISTINCT A.CNPJ) AS TOTAL
                FROM
                    PA_EMPRESA A
                INNER JOIN 
                    PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                WHERE
                    A.ATIVO = 1 " . $filtro . "
                    
                UNION ALL
                
                    
                SELECT 
                    'N de Medicos(s)' AS TITULO,
                    COUNT(DISTINCT D.FK_CRM) as TOTAL
                FROM
                    PA_EMPRESA A
                INNER JOIN 
                    PA_EMPRESA_PLATAFORMA B ON A.ID = B.FK_EMPRESA
                INNER JOIN
                    PA_CONTATO C ON C.FK_EMPRESA = A.ID
                INNER JOIN 
                    PA_MEDICO D ON D.FK_CONTATO = C.ID
                WHERE
                    A.ATIVO = 1 " . $filtro ."";

        return $this->executarQueryArray($query);
    }

    public function partMeso() {

        


        $query = "SELECT
                        CAST(SUM(G.M01) AS FLOAT) / NULLIF(CAST(SUM(K.M01) AS FLOAT),0) * 100 AS PART_MESO
                    FROM PA_MESO_BRICK A
                    INNER JOIN PA_CEP_BRICK B ON A.BRICK = B.BRICK
                    INNER JOIN PA_EMPRESA C ON C.CEP = B.CEP
                    INNER JOIN PA_CONTATO D ON D.FK_EMPRESA = C.ID
                    INNER JOIN PA_MEDICO E ON D.ID = E.FK_CONTATO
                    INNER JOIN PA_CRM F ON F.ID = E.FK_CRM
                    INNER JOIN CNPJ_CRM_MERC G ON G.CRM = F.UF + F.NUMERO
                    
                    INNER JOIN VW_SGN_TOTAL_MESO K ON K.DESCRICAO = A.DESCRICAO
                    WHERE 
                        C.ATIVO = 1 ";

        $rs = $this->executarQueryArray($query);

        return array(
            array('LABEL' => 'CNPJ', 'VALOR' => $rs[1]['PART_MESO']),
            array('LABEL' => 'MESO', 'VALOR' => (100 - $rs[1]['PART_MESO']))
        );
    }

    public function grafShare() {
        $query = "
                SELECT TOP 5
                    CNPJ,
                    RAZAO_SOCIAL,
                    (CAST(SUM(JAN_PROD) AS FLOAT) / NULLIF(CAST(SUM(JAN_MERC) AS FLOAT),0)*100) AS SHARE_ANT,
                    (CAST(SUM(MES_PROD) AS FLOAT) / NULLIF(CAST(SUM(MES_MERC) AS FLOAT),0)*100) AS SHARE_ATU
                FROM VW_SIGNO_BASE
                WHERE
                    UGN = 'CO_NO'
                GROUP BY
                    CNPJ,
                    RAZAO_SOCIAL
                ORDER BY 
                    CAST(SUM(MES_PROD) AS FLOAT) / NULLIF(CAST(SUM(MES_MERC) AS FLOAT),0)  DESC	
    
        ";

        return $this->executarQueryArray($query);
    }
    
}