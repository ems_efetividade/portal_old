<?php 
require_once('lib/appConexao.php');

class mBasePx extends appConexao {


	private $Ufcrm;
	private $NomePlataforma;
	private $Meso;
	private $FkPlataforma;
	private $FkCrm;
	private $M11;
	private $M10;
	private $M09;
	private $M08;
	private $M07;
	private $M06;
	private $M05;
	private $M04;
	private $M03;
	private $M02;
	private $M01;
	private $M00;
	private $P11;
	private $P10;
	private $P09;
	private $P08;
	private $P07;
	private $P06;
	private $P05;
	private $P04;
	private $P03;
	private $P02;
	private $P01;
	private $P00;
	private $Id;
	private $max;

	private $Plataforma;
	private $Crm;

	public function __construct($Ufcrm=null, $NomePlataforma=null, $Meso=null, $FkPlataforma=null, $FkCrm=null, $M11=null, $M10=null, $M09=null, $M08=null, $M07=null, $M06=null, $M05=null, $M04=null, $M03=null, $M02=null, $M01=null, $M00=null, $P11=null, $P10=null, $P09=null, $P08=null, $P07=null, $P06=null, $P05=null, $P04=null, $P03=null, $P02=null, $P01=null, $P00=null, $Id=null, $max=null) {
		$this->setUfcrm($Ufcrm);
		$this->setNomePlataforma($NomePlataforma);
		$this->setMeso($Meso);
		$this->setFkPlataforma($FkPlataforma);
		$this->setFkCrm($FkCrm);
		$this->setM11($M11);
		$this->setM10($M10);
		$this->setM09($M09);
		$this->setM08($M08);
		$this->setM07($M07);
		$this->setM06($M06);
		$this->setM05($M05);
		$this->setM04($M04);
		$this->setM03($M03);
		$this->setM02($M02);
		$this->setM01($M01);
		$this->setM00($M00);
		$this->setP11($P11);
		$this->setP10($P10);
		$this->setP09($P09);
		$this->setP08($P08);
		$this->setP07($P07);
		$this->setP06($P06);
		$this->setP05($P05);
		$this->setP04($P04);
		$this->setP03($P03);
		$this->setP02($P02);
		$this->setP01($P01);
		$this->setP00($P00);
		$this->setId($Id);
		$this->setMax($max);

	}

	protected function setMethods($row) {
		$this->setUfcrm($row["UFCRM"]);
		$this->setNomePlataforma($row["NOME_PLATAFORMA"]);
		$this->setMeso($row["MESO"]);
		$this->setFkPlataforma($row["FK_PLATAFORMA"]);
		$this->setFkCrm($row["FK_CRM"]);
		$this->setM11($row["M11"]);
		$this->setM10($row["M10"]);
		$this->setM09($row["M09"]);
		$this->setM08($row["M08"]);
		$this->setM07($row["M07"]);
		$this->setM06($row["M06"]);
		$this->setM05($row["M05"]);
		$this->setM04($row["M04"]);
		$this->setM03($row["M03"]);
		$this->setM02($row["M02"]);
		$this->setM01($row["M01"]);
		$this->setM00($row["M00"]);
		$this->setP11($row["P11"]);
		$this->setP10($row["P10"]);
		$this->setP09($row["P09"]);
		$this->setP08($row["P08"]);
		$this->setP07($row["P07"]);
		$this->setP06($row["P06"]);
		$this->setP05($row["P05"]);
		$this->setP04($row["P04"]);
		$this->setP03($row["P03"]);
		$this->setP02($row["P02"]);
		$this->setP01($row["P01"]);
		$this->setP00($row["P00"]);
		$this->setId($row["ID"]);
		$this->setMax($this->selMax());
	
		if(is_object($this->Plataforma)) {
			$this->Plataforma->selecionar($row["FK_PLATAFORMA"]);
		}

		if(is_object($this->Crm)) {
			$this->Crm->selecionar($row["FK_CRM"]);
		}

	}
	
	public function injection(mPlataforma $Plataforma=null, mCrm $Crm=null) {
		$this->Plataforma = $Plataforma;
		$this->Crm = $Crm;
	}
	
	protected function createObjects($row) {
		$obj = new mBasePx(); 

		$this->Plataforma = is_object($this->Plataforma) ? new $this->Plataforma : null;
		$this->Crm = is_object($this->Crm) ? new $this->Crm : null;

		$obj->injection($this->Plataforma, $this->Crm); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setUfcrm($value=null) {
		$this->Ufcrm = $value;
	} 

	public function setNomePlataforma($value=null) {
		$this->NomePlataforma = $value;
	} 

	public function setMeso($value=null) {
		$this->Meso = $value;
	} 

	public function setFkPlataforma($value=null) {
		$this->FkPlataforma = $value;
	} 

	public function setFkCrm($value=null) {
		$this->FkCrm = $value;
	} 

	public function setM11($value=null) {
		$this->M11 = $value;
	} 

	public function setM10($value=null) {
		$this->M10 = $value;
	} 

	public function setM09($value=null) {
		$this->M09 = $value;
	} 

	public function setM08($value=null) {
		$this->M08 = $value;
	} 

	public function setM07($value=null) {
		$this->M07 = $value;
	} 

	public function setM06($value=null) {
		$this->M06 = $value;
	} 

	public function setM05($value=null) {
		$this->M05 = $value;
	} 

	public function setM04($value=null) {
		$this->M04 = $value;
	} 

	public function setM03($value=null) {
		$this->M03 = $value;
	} 

	public function setM02($value=null) {
		$this->M02 = $value;
	} 

	public function setM01($value=null) {
		$this->M01 = $value;
	} 

	public function setM00($value=null) {
		$this->M00 = $value;
	}
	
	public function setMax($value=null) {
		$this->max = $value;
	}

	public function setP11($value=null) {
		$this->P11 = $value;
	} 

	public function setP10($value=null) {
		$this->P10 = $value;
	} 

	public function setP09($value=null) {
		$this->P09 = $value;
	} 

	public function setP08($value=null) {
		$this->P08 = $value;
	} 

	public function setP07($value=null) {
		$this->P07 = $value;
	} 

	public function setP06($value=null) {
		$this->P06 = $value;
	} 

	public function setP05($value=null) {
		$this->P05 = $value;
	} 

	public function setP04($value=null) {
		$this->P04 = $value;
	} 

	public function setP03($value=null) {
		$this->P03 = $value;
	} 

	public function setP02($value=null) {
		$this->P02 = $value;
	} 

	public function setP01($value=null) {
		$this->P01 = $value;
	} 

	public function setP00($value=null) {
		$this->P00 = $value;
	} 

	public function setId($value=null) {
		$this->Id = $value;
	} 


	public function getUfcrm() {
		return $this->Ufcrm;
	} 

	public function getNomePlataforma() {
		return $this->NomePlataforma;
	} 

	public function getMeso() {
		return $this->Meso;
	} 

	public function getFkPlataforma() {
		return $this->FkPlataforma;
	} 

	public function getFkCrm() {
		return $this->FkCrm;
	} 

	public function getM11() {
		return $this->M11;
	} 

	public function getM10() {
		return $this->M10;
	} 

	public function getM09() {
		return $this->M09;
	} 

	public function getM08() {
		return $this->M08;
	} 

	public function getM07() {
		return $this->M07;
	} 

	public function getM06() {
		return $this->M06;
	} 

	public function getM05() {
		return $this->M05;
	} 

	public function getM04() {
		return $this->M04;
	} 

	public function getM03() {
		return $this->M03;
	} 

	public function getM02() {
		return $this->M02;
	} 

	public function getM01() {
		return $this->M01;
	} 

	public function getM00() {
		return $this->M00;
	} 

	public function getP11() {
		return $this->P11;
	} 

	public function getP10() {
		return $this->P10;
	} 

	public function getP09() {
		return $this->P09;
	} 

	public function getP08() {
		return $this->P08;
	} 

	public function getP07() {
		return $this->P07;
	} 

	public function getP06() {
		return $this->P06;
	} 

	public function getP05() {
		return $this->P05;
	} 

	public function getP04() {
		return $this->P04;
	} 

	public function getP03() {
		return $this->P03;
	} 

	public function getP02() {
		return $this->P02;
	} 

	public function getP01() {
		return $this->P01;
	} 

	public function getP00() {
		return $this->P00;
	} 

	public function getMax() {
		return $this->max;
	} 

	public function getId() {
		return $this->Id;
	} 


	public function Plataforma() {
		return $this->Plataforma;
	} 

	public function Crm() {
		return $this->Crm;
	} 


	public function selecionar($id=null) {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByPlataforma($value=null) {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByPlataforma($value=null) {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByCrm($value=null) {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE FK_CRM =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByCrm($value=null) {
		$query = "SELECT UFCRM, NOME_PLATAFORMA, MESO, FK_PLATAFORMA, FK_CRM, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00, P11, P10, P09, P08, P07, P06, P05, P04, P03, P02, P01, P00, ID FROM [PA_BASE_PX] WHERE FK_CRM =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	private function selMax() {
		$max = 0;

		for($i=0; $i<=11; $i++) {
			$method = 'getM'.str_pad($i, 2, "0", STR_PAD_LEFT);
			$max = ($max < $this->{$method}()) ? $this->{$method}() : $max;
		}

		return $max;
	}

}