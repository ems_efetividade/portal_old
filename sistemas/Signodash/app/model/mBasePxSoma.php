<?php 

require_once('app/model/mBasePx.php');

class mBasePxSoma extends mBasePx {

    public function __construct() {
        parent::__construct();
    }


    public function somaMesoPlataforma($meso=null, $plataforma=null) {
        $filtroPlataforma = (is_array($plataforma)) ? ' IN ('.implode(',', $plataforma).')' : ' = ' . $plataforma;
		$query = "SELECT * FROM VW_SGN_SOMAMESOREGIAO WHERE MESO = '".$meso."' AND FK_PLATAFORMA " . $filtroPlataforma;
		$rs = $this->executarQueryArray($query);
        return $this->createObjects($rs[1]);
    }

    public function somaMesoPlataformaEmpresa($idEmpresa=null, $plataforma=null) {
        $filtroPlataforma = (is_array($plataforma)) ? ' IN ('.implode(',', $plataforma).')' : ' = ' . $plataforma;
        $query = "SELECT * FROM VW_SGN_SOMAEMPRESA WHERE ID = ".$idEmpresa." AND FK_PLATAFORMA ".$filtroPlataforma;              
        $rs = $this->executarQueryArray($query);
        return $this->createObjects($rs[1]);              
    }

    public function participacaoMesoEmpresa($idEmpresa=null, $mesoregiao=null, $plataforma=null) {
        $filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;
        
        $query = "EXEC PROC_SGN_PARTMESO '".$filtroPlataforma."', ".$idEmpresa." ";
        //echo $query;
        $rs = $this->executarQueryArray($query);
        return $this->createObjects($rs[1]);

/*
        $queryMercado = "SELECT 
                            SUM([M11]) AS M11
                            ,SUM([M10]) AS M10
                            ,SUM([M09]) AS M09
                            ,SUM([M08]) AS M08
                            ,SUM([M07]) AS M07
                            ,SUM([M06]) AS M06
                            ,SUM([M05]) AS M05
                            ,SUM([M04]) AS M04
                            ,SUM([M03]) AS M03
                            ,SUM([M02]) AS M02
                            ,SUM([M01]) AS M01
                            ,SUM([M00]) AS M00
                            ,SUM([P11]) AS P11
                            ,SUM([P10]) AS P10
                            ,SUM([P09]) AS P09
                            ,SUM([P08]) AS P08
                            ,SUM([P07]) AS P07
                            ,SUM([P06]) AS P06
                            ,SUM([P05]) AS P05
                            ,SUM([P04]) AS P04
                            ,SUM([P03]) AS P03
                            ,SUM([P02]) AS P02
                            ,SUM([P01]) AS P01
                            ,SUM([P00]) AS P00
                        FROM [PNC].[dbo].[PA_PX_MESO_PLAT]
                        WHERE
                        FK_PLATAFORMA IN (".$filtroPlataforma.") AND FK_MESOREGIAO = " .$mesoregiao;


        $queryCnpj  = "
                SELECT     
                    SUM(G.M11) AS M11, 
                    SUM(G.M10) AS M10, 
                    SUM(G.M09) AS M09, 
                    SUM(G.M08) AS M08, 
                    SUM(G.M07) AS M07, 
                    SUM(G.M06) AS M06, 
                    SUM(G.M05) AS M05, 
                    SUM(G.M04) AS M04, 
                    SUM(G.M03) AS M03, 
                    SUM(G.M02) AS M02, 
                    SUM(G.M01) AS M01, 
                    SUM(G.M00) AS M00, 
                    SUM(G.P11) AS P11, 
                    SUM(G.P10) AS P10, 
                    SUM(G.P09) AS P09, 
                    SUM(G.P08) AS P08, 
                    SUM(G.P07) AS P07, 
                    SUM(G.P06) AS P06, 
                    SUM(G.P05) AS P05, 
                    SUM(G.P04) AS P04, 
                    SUM(G.P03) AS P03, 
                    SUM(G.P02) AS P02, 
                    SUM(G.P01) AS P01, 
                    SUM(G.P00) AS P00
                FROM         dbo.PA_CEP_BRICK AS A INNER JOIN
                    dbo.PA_MESO_BRICK AS B ON B.BRICK = A.BRICK INNER JOIN
                    dbo.PA_EMPRESA AS C ON C.CEP = A.CEP AND C.ID = ".$idEmpresa." INNER JOIN
                    dbo.PA_CONTATO AS D ON D.FK_EMPRESA = C.ID INNER JOIN
                    dbo.PA_MEDICO AS E ON E.FK_CONTATO = D.ID INNER JOIN
                    dbo.PA_CRM AS F ON F.ID = E.FK_CRM INNER JOIN
                    dbo.PA_BASE_PX_ AS G ON G.UFCRM = F.UF + F.NUMERO INNER JOIN
                    dbo.PA_EMPRESA_PLATAFORMA AS H ON H.FK_EMPRESA = C.ID AND G.FK_PLATAFORMA = H.FK_PLATAFORMA AND H.FK_PLATAFORMA IN (".$filtroPlataforma.") LEFT OUTER JOIN
                    dbo.PA_MESO_BRICK AS I ON I.BRICK = A.BRICK LEFT OUTER JOIN
                    dbo.PA_MESOREGIAO AS J ON J.MESOREGIAO = I.DESCRICAO      
                WHERE
                    J.ID = ".$mesoregiao."
                GROUP BY
                    J.ID  ";

        $rsMercado = $this->executarQueryArray($queryMercado);                 
        $rsCnpj   = $this->executarQueryArray($queryCnpj);                 

        $mercado = $this->createObjects($rsMercado[1]);
        $cnpj    = $this->createObjects($rsCnpj[1]);

        return  new mBasePxSoma(
            null,
            null,
            null,
            null,
            ($cnpj->getM11() / $mercado->getM11()),
            ($cnpj->getM10() / $mercado->getM10()),
            ($cnpj->getM09() / $mercado->getM09()),
            ($cnpj->getM08() / $mercado->getM08()),
            ($cnpj->getM07() / $mercado->getM07()),
            ($cnpj->getM06() / $mercado->getM06()),
            ($cnpj->getM05() / $mercado->getM05()),
            ($cnpj->getM04() / $mercado->getM04()),
            ($cnpj->getM03() / $mercado->getM03()),
            ($cnpj->getM02() / $mercado->getM02()),
            ($cnpj->getM01() / $mercado->getM01()),
            ($cnpj->getM00() / $mercado->getM00()),
            ($cnpj->getP11() / $mercado->getP11()),
            ($cnpj->getP10() / $mercado->getP10()),
            ($cnpj->getP09() / $mercado->getP09()),
            ($cnpj->getP08() / $mercado->getP08()),
            ($cnpj->getP07() / $mercado->getP07()),
            ($cnpj->getP06() / $mercado->getP06()),
            ($cnpj->getP05() / $mercado->getP05()),
            ($cnpj->getP04() / $mercado->getP04()),
            ($cnpj->getP03() / $mercado->getP03()),
            ($cnpj->getP02() / $mercado->getP02()),
            ($cnpj->getP01() / $mercado->getP01()),
            ($cnpj->getP00() / $mercado->getP00())
        );

        

*/

    }



    public function marketShareCnpj($idEmpresa=null, $plataforma=null) {
        //$filtroPlataforma = (is_array($plataforma)) ? ' IN ('.implode(',', $plataforma).')' : ' = ' . $plataforma;
        $filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;
        
        //$query = "SELECT * FROM VW_SGN_MKTSHARECNPJ  WHERE ID = ".$idEmpresa." AND FK_PLATAFORMA  ".$filtroPlataforma;
        $query = "                
                SELECT
                    NULLIF(CAST(SUM(B.P11) AS FLOAT),0) / CAST(SUM(A.M11) AS FLOAT) AS M11,
                    NULLIF(CAST(SUM(B.P10) AS FLOAT),0) / CAST(SUM(A.M10) AS FLOAT) AS M10,
                    NULLIF(CAST(SUM(B.P09) AS FLOAT),0) / CAST(SUM(A.M09) AS FLOAT) AS M09,
                    NULLIF(CAST(SUM(B.P08) AS FLOAT),0) / CAST(SUM(A.M08) AS FLOAT) AS M08,
                    NULLIF(CAST(SUM(B.P07) AS FLOAT),0) / CAST(SUM(A.M07) AS FLOAT) AS M07,
                    NULLIF(CAST(SUM(B.P06) AS FLOAT),0) / CAST(SUM(A.M06) AS FLOAT) AS M06,
                    NULLIF(CAST(SUM(B.P05) AS FLOAT),0) / CAST(SUM(A.M05) AS FLOAT) AS M05,
                    NULLIF(CAST(SUM(B.P04) AS FLOAT),0) / CAST(SUM(A.M04) AS FLOAT) AS M04,
                    NULLIF(CAST(SUM(B.P03) AS FLOAT),0) / CAST(SUM(A.M03) AS FLOAT) AS M03,
                    NULLIF(CAST(SUM(B.P02) AS FLOAT),0) / CAST(SUM(A.M02) AS FLOAT) AS M02,
                    NULLIF(CAST(SUM(B.P01) AS FLOAT),0) / CAST(SUM(A.M01) AS FLOAT) AS M01,
                    NULLIF(CAST(SUM(B.P00) AS FLOAT),0) / CAST(SUM(A.M00) AS FLOAT) AS M00,
                    NULLIF(CAST(SUM(B.P11) AS FLOAT),0) / CAST(SUM(A.P11) AS FLOAT) AS P11,
                    NULLIF(CAST(SUM(B.P10) AS FLOAT),0) / CAST(SUM(A.P10) AS FLOAT) AS P10,
                    NULLIF(CAST(SUM(B.P09) AS FLOAT),0) / CAST(SUM(A.P09) AS FLOAT) AS P09,
                    NULLIF(CAST(SUM(B.P08) AS FLOAT),0) / CAST(SUM(A.P08) AS FLOAT) AS P08,
                    NULLIF(CAST(SUM(B.P07) AS FLOAT),0) / CAST(SUM(A.P07) AS FLOAT) AS P07,
                    NULLIF(CAST(SUM(B.P06) AS FLOAT),0) / CAST(SUM(A.P06) AS FLOAT) AS P06,
                    NULLIF(CAST(SUM(B.P05) AS FLOAT),0) / CAST(SUM(A.P05) AS FLOAT) AS P05,
                    NULLIF(CAST(SUM(B.P04) AS FLOAT),0) / CAST(SUM(A.P04) AS FLOAT) AS P04,
                    NULLIF(CAST(SUM(B.P03) AS FLOAT),0) / CAST(SUM(A.P03) AS FLOAT) AS P03,
                    NULLIF(CAST(SUM(B.P02) AS FLOAT),0) / CAST(SUM(A.P02) AS FLOAT) AS P02,
                    NULLIF(CAST(SUM(B.P01) AS FLOAT),0) / CAST(SUM(A.P01) AS FLOAT) AS P01,
                    NULLIF(CAST(SUM(B.P00) AS FLOAT),0) / CAST(SUM(A.P00) AS FLOAT) AS P00
                FROM 
                    PA_PX_MESO_PLAT A 
                INNER JOIN 
                    VW_SGN_SOMAEMPRESA B ON B.FK_PLATAFORMA = A.FK_PLATAFORMA  AND A.FK_MESOREGIAO = B.FK_MESOREGIAO 
                WHERE 
                    B.FK_EMPRESA = ".$idEmpresa." AND b.FK_PLATAFORMA  IN  (".$filtroPlataforma.")";
        //echo $query;
                    //$query = "EXEC PROC_SGN_PARTMESO '".$filtroPlataforma."', ".$idEmpresa." ";
        $rs = $this->executarQueryArray($query);
        //return array($this->createObjects($rs[1]), $query);
        return $this->createObjects($rs[1]);
        //foreach($rs as $row) {
		//	$objs[] = $this->createObjects($row);
        //}
        
        //return $objs;
    }

    public function qy($idEmpresa=null, $plataforma=null) {

        $filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;

        $query = "                
                SELECT
                    NULLIF(CAST(SUM(B.P11) AS FLOAT),0) / CAST(SUM(A.M11) AS FLOAT) AS M11,
                    NULLIF(CAST(SUM(B.P10) AS FLOAT),0) / CAST(SUM(A.M10) AS FLOAT) AS M10,
                    NULLIF(CAST(SUM(B.P09) AS FLOAT),0) / CAST(SUM(A.M09) AS FLOAT) AS M09,
                    NULLIF(CAST(SUM(B.P08) AS FLOAT),0) / CAST(SUM(A.M08) AS FLOAT) AS M08,
                    NULLIF(CAST(SUM(B.P07) AS FLOAT),0) / CAST(SUM(A.M07) AS FLOAT) AS M07,
                    NULLIF(CAST(SUM(B.P06) AS FLOAT),0) / CAST(SUM(A.M06) AS FLOAT) AS M06,
                    NULLIF(CAST(SUM(B.P05) AS FLOAT),0) / CAST(SUM(A.M05) AS FLOAT) AS M05,
                    NULLIF(CAST(SUM(B.P04) AS FLOAT),0) / CAST(SUM(A.M04) AS FLOAT) AS M04,
                    NULLIF(CAST(SUM(B.P03) AS FLOAT),0) / CAST(SUM(A.M03) AS FLOAT) AS M03,
                    NULLIF(CAST(SUM(B.P02) AS FLOAT),0) / CAST(SUM(A.M02) AS FLOAT) AS M02,
                    NULLIF(CAST(SUM(B.P01) AS FLOAT),0) / CAST(SUM(A.M01) AS FLOAT) AS M01,
                    NULLIF(CAST(SUM(B.P00) AS FLOAT),0) / CAST(SUM(A.M00) AS FLOAT) AS M00,
                    NULLIF(CAST(SUM(B.P11) AS FLOAT),0) / CAST(SUM(A.P11) AS FLOAT) AS P11,
                    NULLIF(CAST(SUM(B.P10) AS FLOAT),0) / CAST(SUM(A.P10) AS FLOAT) AS P10,
                    NULLIF(CAST(SUM(B.P09) AS FLOAT),0) / CAST(SUM(A.P09) AS FLOAT) AS P09,
                    NULLIF(CAST(SUM(B.P08) AS FLOAT),0) / CAST(SUM(A.P08) AS FLOAT) AS P08,
                    NULLIF(CAST(SUM(B.P07) AS FLOAT),0) / CAST(SUM(A.P07) AS FLOAT) AS P07,
                    NULLIF(CAST(SUM(B.P06) AS FLOAT),0) / CAST(SUM(A.P06) AS FLOAT) AS P06,
                    NULLIF(CAST(SUM(B.P05) AS FLOAT),0) / CAST(SUM(A.P05) AS FLOAT) AS P05,
                    NULLIF(CAST(SUM(B.P04) AS FLOAT),0) / CAST(SUM(A.P04) AS FLOAT) AS P04,
                    NULLIF(CAST(SUM(B.P03) AS FLOAT),0) / CAST(SUM(A.P03) AS FLOAT) AS P03,
                    NULLIF(CAST(SUM(B.P02) AS FLOAT),0) / CAST(SUM(A.P02) AS FLOAT) AS P02,
                    NULLIF(CAST(SUM(B.P01) AS FLOAT),0) / CAST(SUM(A.P01) AS FLOAT) AS P01,
                    NULLIF(CAST(SUM(B.P00) AS FLOAT),0) / CAST(SUM(A.P00) AS FLOAT) AS P00
                FROM 
                    PA_PX_MESO_PLAT A 
                INNER JOIN 
                    VW_SGN_SOMAEMPRESA B ON B.FK_PLATAFORMA = A.FK_PLATAFORMA  AND A.FK_MESOREGIAO = B.FK_MESOREGIAO 
                WHERE 
                    B.FK_EMPRESA = ".$idEmpresa." AND b.FK_PLATAFORMA  IN  (".$filtroPlataforma.")";
        return  $query;
       
    }


}