<?php 
require_once('lib/appConexao.php');

class mBaseSoma extends appConexao {
	
	private $FkEmpresa;
	private $FkPlataforma;
	private $FkMesoregiao;
	private $Mat02;
	private $Mat00;
	private $Sem03;
	private $Sem02;
	private $Sem01;
	private $Sem00;
	private $Trm07;
	private $Trm06;
	private $Trm05;
	private $Trm04;
	private $Trm03;
	private $Trm02;
	private $Trm01;
	private $Trm00;
	private $M23;
	private $M22;
	private $M21;
	private $M20;
	private $M19;
	private $M18;
	private $M17;
	private $M16;
	private $M15;
	private $M14;
	private $M13;
	private $M12;
	private $M11;
	private $M10;
	private $M09;
	private $M08;
	private $M07;
	private $M06;
	private $M05;
	private $M04;
	private $M03;
	private $M02;
	private $M01;
	private $M00;


	public function __construct($FkEmpresa=null, $FkPlataforma=null, $FkMesoregiao=null, $Mat02=null, $Mat00=null, $Sem03=null, $Sem02=null, $Sem01=null, $Sem00=null, $Trm07=null, $Trm06=null, $Trm05=null, $Trm04=null, $Trm03=null, $Trm02=null, $Trm01=null, $Trm00=null, $M23=null, $M22=null, $M21=null, $M20=null, $M19=null, $M18=null, $M17=null, $M16=null, $M15=null, $M14=null, $M13=null, $M12=null, $M11=null, $M10=null, $M09=null, $M08=null, $M07=null, $M06=null, $M05=null, $M04=null, $M03=null, $M02=null, $M01=null, $M00=null) {
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkPlataforma($FkPlataforma);
		$this->setFkMesoregiao($FkMesoregiao);
		$this->setMat02($Mat02);
		$this->setMat00($Mat00);
		$this->setSem03($Sem03);
		$this->setSem02($Sem02);
		$this->setSem01($Sem01);
		$this->setSem00($Sem00);
		$this->setTrm07($Trm07);
		$this->setTrm06($Trm06);
		$this->setTrm05($Trm05);
		$this->setTrm04($Trm04);
		$this->setTrm03($Trm03);
		$this->setTrm02($Trm02);
		$this->setTrm01($Trm01);
		$this->setTrm00($Trm00);
		$this->setM23($M23);
		$this->setM22($M22);
		$this->setM21($M21);
		$this->setM20($M20);
		$this->setM19($M19);
		$this->setM18($M18);
		$this->setM17($M17);
		$this->setM16($M16);
		$this->setM15($M15);
		$this->setM14($M14);
		$this->setM13($M13);
		$this->setM12($M12);
		$this->setM11($M11);
		$this->setM10($M10);
		$this->setM09($M09);
		$this->setM08($M08);
		$this->setM07($M07);
		$this->setM06($M06);
		$this->setM05($M05);
		$this->setM04($M04);
		$this->setM03($M03);
		$this->setM02($M02);
		$this->setM01($M01);
		$this->setM00($M00);

	}

	private function setMethods($row) {
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkPlataforma($row["FK_PLATAFORMA"]);
		$this->setFkMesoregiao($row["FK_MESOREGIAO"]);
		$this->setMat02($row["MAT02"]);
		$this->setMat00($row["MAT00"]);
		$this->setSem03($row["SEM03"]);
		$this->setSem02($row["SEM02"]);
		$this->setSem01($row["SEM01"]);
		$this->setSem00($row["SEM00"]);
		$this->setTrm07($row["TRM07"]);
		$this->setTrm06($row["TRM06"]);
		$this->setTrm05($row["TRM05"]);
		$this->setTrm04($row["TRM04"]);
		$this->setTrm03($row["TRM03"]);
		$this->setTrm02($row["TRM02"]);
		$this->setTrm01($row["TRM01"]);
		$this->setTrm00($row["TRM00"]);
		$this->setM23($row["M23"]);
		$this->setM22($row["M22"]);
		$this->setM21($row["M21"]);
		$this->setM20($row["M20"]);
		$this->setM19($row["M19"]);
		$this->setM18($row["M18"]);
		$this->setM17($row["M17"]);
		$this->setM16($row["M16"]);
		$this->setM15($row["M15"]);
		$this->setM14($row["M14"]);
		$this->setM13($row["M13"]);
		$this->setM12($row["M12"]);
		$this->setM11($row["M11"]);
		$this->setM10($row["M10"]);
		$this->setM09($row["M09"]);
		$this->setM08($row["M08"]);
		$this->setM07($row["M07"]);
		$this->setM06($row["M06"]);
		$this->setM05($row["M05"]);
		$this->setM04($row["M04"]);
		$this->setM03($row["M03"]);
		$this->setM02($row["M02"]);
		$this->setM01($row["M01"]);
		$this->setM00($row["M00"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mBaseSoma(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkPlataforma($value=null) {
		$this->FkPlataforma = $value;
	} 

	public function setFkMesoregiao($value=null) {
		$this->FkMesoregiao = $value;
	} 

	public function setMat02($value=null) {
		$this->Mat02 = $value;
	} 

	public function setMat00($value=null) {
		$this->Mat00 = $value;
	} 

	public function setSem03($value=null) {
		$this->Sem03 = $value;
	} 

	public function setSem02($value=null) {
		$this->Sem02 = $value;
	} 

	public function setSem01($value=null) {
		$this->Sem01 = $value;
	} 

	public function setSem00($value=null) {
		$this->Sem00 = $value;
	} 

	public function setTrm07($value=null) {
		$this->Trm07 = $value;
	} 

	public function setTrm06($value=null) {
		$this->Trm06 = $value;
	} 

	public function setTrm05($value=null) {
		$this->Trm05 = $value;
	} 

	public function setTrm04($value=null) {
		$this->Trm04 = $value;
	} 

	public function setTrm03($value=null) {
		$this->Trm03 = $value;
	} 

	public function setTrm02($value=null) {
		$this->Trm02 = $value;
	} 

	public function setTrm01($value=null) {
		$this->Trm01 = $value;
	} 

	public function setTrm00($value=null) {
		$this->Trm00 = $value;
	} 

	public function setM23($value=null) {
		$this->M23 = $value;
	} 

	public function setM22($value=null) {
		$this->M22 = $value;
	} 

	public function setM21($value=null) {
		$this->M21 = $value;
	} 

	public function setM20($value=null) {
		$this->M20 = $value;
	} 

	public function setM19($value=null) {
		$this->M19 = $value;
	} 

	public function setM18($value=null) {
		$this->M18 = $value;
	} 

	public function setM17($value=null) {
		$this->M17 = $value;
	} 

	public function setM16($value=null) {
		$this->M16 = $value;
	} 

	public function setM15($value=null) {
		$this->M15 = $value;
	} 

	public function setM14($value=null) {
		$this->M14 = $value;
	} 

	public function setM13($value=null) {
		$this->M13 = $value;
	} 

	public function setM12($value=null) {
		$this->M12 = $value;
	} 

	public function setM11($value=null) {
		$this->M11 = $value;
	} 

	public function setM10($value=null) {
		$this->M10 = $value;
	} 

	public function setM09($value=null) {
		$this->M09 = $value;
	} 

	public function setM08($value=null) {
		$this->M08 = $value;
	} 

	public function setM07($value=null) {
		$this->M07 = $value;
	} 

	public function setM06($value=null) {
		$this->M06 = $value;
	} 

	public function setM05($value=null) {
		$this->M05 = $value;
	} 

	public function setM04($value=null) {
		$this->M04 = $value;
	} 

	public function setM03($value=null) {
		$this->M03 = $value;
	} 

	public function setM02($value=null) {
		$this->M02 = $value;
	} 

	public function setM01($value=null) {
		$this->M01 = $value;
	} 

	public function setM00($value=null) {
		$this->M00 = $value;
	} 


	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkPlataforma() {
		return $this->FkPlataforma;
	} 

	public function getFkMesoregiao() {
		return $this->FkMesoregiao;
	} 

	public function getMat02() {
		return $this->Mat02;
	} 

	public function getMat00() {
		return $this->Mat00;
	} 

	public function getSem03() {
		return $this->Sem03;
	} 

	public function getSem02() {
		return $this->Sem02;
	} 

	public function getSem01() {
		return $this->Sem01;
	} 

	public function getSem00() {
		return $this->Sem00;
	} 

	public function getTrm07() {
		return $this->Trm07;
	} 

	public function getTrm06() {
		return $this->Trm06;
	} 

	public function getTrm05() {
		return $this->Trm05;
	} 

	public function getTrm04() {
		return $this->Trm04;
	} 

	public function getTrm03() {
		return $this->Trm03;
	} 

	public function getTrm02() {
		return $this->Trm02;
	} 

	public function getTrm01() {
		return $this->Trm01;
	} 

	public function getTrm00() {
		return $this->Trm00;
	} 

	public function getM23() {
		return $this->M23;
	} 

	public function getM22() {
		return $this->M22;
	} 

	public function getM21() {
		return $this->M21;
	} 

	public function getM20() {
		return $this->M20;
	} 

	public function getM19() {
		return $this->M19;
	} 

	public function getM18() {
		return $this->M18;
	} 

	public function getM17() {
		return $this->M17;
	} 

	public function getM16() {
		return $this->M16;
	} 

	public function getM15() {
		return $this->M15;
	} 

	public function getM14() {
		return $this->M14;
	} 

	public function getM13() {
		return $this->M13;
	} 

	public function getM12() {
		return $this->M12;
	} 

	public function getM11() {
		return $this->M11;
	} 

	public function getM10() {
		return $this->M10;
	} 

	public function getM09() {
		return $this->M09;
	} 

	public function getM08() {
		return $this->M08;
	} 

	public function getM07() {
		return $this->M07;
	} 

	public function getM06() {
		return $this->M06;
	} 

	public function getM05() {
		return $this->M05;
	} 

	public function getM04() {
		return $this->M04;
	} 

	public function getM03() {
		return $this->M03;
	} 

	public function getM02() {
		return $this->M02;
	} 

	public function getM01() {
		return $this->M01;
	} 

	public function getM00() {
		return $this->M00;
	} 

	public function selecionar($id=null) {
		$query = "SELECT FK_EMPRESA, FK_PLATAFORMA, FK_MESOREGIAO, MAT02, MAT00, SEM03, SEM02, SEM01, SEM00, TRM07, TRM06, TRM05, TRM04, TRM03, TRM02, TRM01, TRM00, M23, M22, M21, M20, M19, M18, M17, M16, M15, M14, M13, M12, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00 FROM [PA_PX_FATO_MERCADO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT FK_EMPRESA, FK_PLATAFORMA, FK_MESOREGIAO, MAT02, MAT00, SEM03, SEM02, SEM01, SEM00, TRM07, TRM06, TRM05, TRM04, TRM03, TRM02, TRM01, TRM00, M23, M22, M21, M20, M19, M18, M17, M16, M15, M14, M13, M12, M11, M10, M09, M08, M07, M06, M05, M04, M03, M02, M01, M00 FROM [PA_PX_FATO_MERCADO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
    }
    
    public function marketShareCnpj($plataforma=null) {
		$filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;
		$query = "EXEC PROC_SGN_SHARE_CNPJ '".$filtroPlataforma."', '" . $this->getFkEmpresa() . "'" ;
		$rs = $this->executarQueryArray($query);
		return $this->createObjects($rs[1]);
    }

    public function qy($plataforma=null) {

        $filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;

        $query = "
                    SELECT
                        NULLIF(CAST(SUM(A.MAT02) AS FLOAT),0) / CAST(SUM(B.MAT02) AS FLOAT) AS MAT02,
                        NULLIF(CAST(SUM(A.MAT00) AS FLOAT),0) / CAST(SUM(B.MAT00) AS FLOAT) AS MAT00,
                        NULLIF(CAST(SUM(A.SEM03) AS FLOAT),0) / CAST(SUM(B.SEM03) AS FLOAT) AS SEM03,
                        NULLIF(CAST(SUM(A.SEM02) AS FLOAT),0) / CAST(SUM(B.SEM02) AS FLOAT) AS SEM02,
                        NULLIF(CAST(SUM(A.SEM01) AS FLOAT),0) / CAST(SUM(B.SEM01) AS FLOAT) AS SEM01,
                        NULLIF(CAST(SUM(A.SEM00) AS FLOAT),0) / CAST(SUM(B.SEM00) AS FLOAT) AS SEM00,
                        NULLIF(CAST(SUM(A.TRM07) AS FLOAT),0) / CAST(SUM(B.TRM07) AS FLOAT) AS TRM07,
                        NULLIF(CAST(SUM(A.TRM06) AS FLOAT),0) / CAST(SUM(B.TRM06) AS FLOAT) AS TRM06,
                        NULLIF(CAST(SUM(A.TRM05) AS FLOAT),0) / CAST(SUM(B.TRM05) AS FLOAT) AS TRM05,
                        NULLIF(CAST(SUM(A.TRM04) AS FLOAT),0) / CAST(SUM(B.TRM04) AS FLOAT) AS TRM04,
                        NULLIF(CAST(SUM(A.TRM03) AS FLOAT),0) / CAST(SUM(B.TRM03) AS FLOAT) AS TRM03,
                        NULLIF(CAST(SUM(A.TRM02) AS FLOAT),0) / CAST(SUM(B.TRM02) AS FLOAT) AS TRM02,
                        NULLIF(CAST(SUM(A.TRM01) AS FLOAT),0) / CAST(SUM(B.TRM01) AS FLOAT) AS TRM01,
                        NULLIF(CAST(SUM(A.TRM00) AS FLOAT),0) / CAST(SUM(B.TRM00) AS FLOAT) AS TRM00,
                        NULLIF(CAST(SUM(A.M23) AS FLOAT),0) / CAST(SUM(B.M23) AS FLOAT) AS M23,
                        NULLIF(CAST(SUM(A.M22) AS FLOAT),0) / CAST(SUM(B.M22) AS FLOAT) AS M22,
                        NULLIF(CAST(SUM(A.M21) AS FLOAT),0) / CAST(SUM(B.M21) AS FLOAT) AS M21,
                        NULLIF(CAST(SUM(A.M20) AS FLOAT),0) / CAST(SUM(B.M20) AS FLOAT) AS M20,
                        NULLIF(CAST(SUM(A.M19) AS FLOAT),0) / CAST(SUM(B.M19) AS FLOAT) AS M19,
                        NULLIF(CAST(SUM(A.M18) AS FLOAT),0) / CAST(SUM(B.M18) AS FLOAT) AS M18,
                        NULLIF(CAST(SUM(A.M17) AS FLOAT),0) / CAST(SUM(B.M17) AS FLOAT) AS M17,
                        NULLIF(CAST(SUM(A.M16) AS FLOAT),0) / CAST(SUM(B.M16) AS FLOAT) AS M16,
                        NULLIF(CAST(SUM(A.M15) AS FLOAT),0) / CAST(SUM(B.M15) AS FLOAT) AS M15,
                        NULLIF(CAST(SUM(A.M14) AS FLOAT),0) / CAST(SUM(B.M14) AS FLOAT) AS M14,
                        NULLIF(CAST(SUM(A.M13) AS FLOAT),0) / CAST(SUM(B.M13) AS FLOAT) AS M13,
                        NULLIF(CAST(SUM(A.M12) AS FLOAT),0) / CAST(SUM(B.M12) AS FLOAT) AS M12,
                        NULLIF(CAST(SUM(A.M11) AS FLOAT),0) / CAST(SUM(B.M11) AS FLOAT) AS M11,
                        NULLIF(CAST(SUM(A.M10) AS FLOAT),0) / CAST(SUM(B.M10) AS FLOAT) AS M10,
                        NULLIF(CAST(SUM(A.M09) AS FLOAT),0) / CAST(SUM(B.M09) AS FLOAT) AS M09,
                        NULLIF(CAST(SUM(A.M08) AS FLOAT),0) / CAST(SUM(B.M08) AS FLOAT) AS M08,
                        NULLIF(CAST(SUM(A.M07) AS FLOAT),0) / CAST(SUM(B.M07) AS FLOAT) AS M07,
                        NULLIF(CAST(SUM(A.M06) AS FLOAT),0) / CAST(SUM(B.M06) AS FLOAT) AS M06,
                        NULLIF(CAST(SUM(A.M05) AS FLOAT),0) / CAST(SUM(B.M05) AS FLOAT) AS M05,
                        NULLIF(CAST(SUM(A.M04) AS FLOAT),0) / CAST(SUM(B.M04) AS FLOAT) AS M04,
                        NULLIF(CAST(SUM(A.M03) AS FLOAT),0) / CAST(SUM(B.M03) AS FLOAT) AS M03,
                        NULLIF(CAST(SUM(A.M02) AS FLOAT),0) / CAST(SUM(B.M02) AS FLOAT) AS M02,
                        NULLIF(CAST(SUM(A.M01) AS FLOAT),0) / CAST(SUM(B.M01) AS FLOAT) AS M01,
                        NULLIF(CAST(SUM(A.M00) AS FLOAT),0) / CAST(SUM(B.M00) AS FLOAT) AS M00
                    FROM
                        PA_PX_FATO_PRODUTO A
                    INNER JOIN PA_PX_FATO_MERCADO B ON
                        B.FK_PLATAFORMA = A.FK_PLATAFORMA AND
                        B.FK_MESOREGIAO = A.FK_MESOREGIAO AND
                        B.FK_EMPRESA = A.FK_EMPRESA
                    WHERE
                        A.FK_EMPRESA  = ".$this->getFkEmpresa()." AND A.FK_PLATAFORMA  IN  (".$filtroPlataforma.")";
        return  $query;
       
	}
	
	public function participacaoMesoEmpresa($idEmpresa=null, $mesoregiao=null, $plataforma=null) {
        $filtroPlataforma = (is_array($plataforma)) ? implode(',', $plataforma) :  $plataforma;
        
        $query = "EXEC PROC_SGN_PARTMESO '".$filtroPlataforma."', ".$idEmpresa." ";
        //echo $query;
        $rs = $this->executarQueryArray($query);
        return $this->createObjects($rs[1]);


	}
	
	public function getMax() {
		$max = 0;

		for($i=0; $i<=11; $i++) {
			$method = 'getM'.str_pad($i, 2, "0", STR_PAD_LEFT);
			$max = ($max < $this->{$method}()) ? $this->{$method}() : $max;
		}

		return $max;
	}

}