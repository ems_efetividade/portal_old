<?php 
require_once('lib/appConexao.php');

class mBrickSetor extends appConexao {


	private $Brick;
	private $Setor;
	private $Id;


	public function __construct($Brick=null, $Setor=null, $Id=null) {
		$this->setBrick($Brick);
		$this->setSetor($Setor);
		$this->setId($Id);

	}

	private function setMethods($row) {
		$this->setBrick($row["BRICK"]);
		$this->setSetor($row["SETOR"]);
		$this->setId($row["ID"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mBrickSetor(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setBrick($value=null) {
		$this->Brick = $value;
	} 

	public function setSetor($value=null) {
		$this->Setor = $value;
	} 

	public function setId($value=null) {
		$this->Id = $value;
	} 


	public function getBrick() {
		return $this->Brick;
	} 

	public function getSetor() {
		return $this->Setor;
	} 

	public function getId() {
		return $this->Id;
	} 



	public function selecionar($id=null) {
		$query = "SELECT BRICK, SETOR, ID FROM [PA_BRICK_SETOR] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT BRICK, SETOR, ID FROM [PA_BRICK_SETOR] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}