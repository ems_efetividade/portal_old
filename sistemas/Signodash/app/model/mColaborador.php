<?php 
require_once('lib/appConexao.php');

class mColaborador extends appConexao {


	private $IdEmpresa;
	private $IdUnNegocio;
	private $IdSetor;
	private $IdColaborador;
	private $Nivel;
	private $Empresa;
	private $UnidadeNeg;
	private $Linha;
	private $Setor;
	private $Regional;
	private $NomeSetor;
	private $Nome;
	private $NivelPerfil;
	private $Perfil;
	private $DtInicio;
	private $DtFinal;
	private $FoneCorporativo;
	private $FoneParticular;
	private $Email;
	private $Id;
	private $Foto;
	private $IdPerfil;
	private $Expr1;
	private $SetorAtivo;
	private $NivelAdmin;
	private $IdLinha;
	private $Matricula;
	private $Cpf;
	private $SetorVago;

	private $Mempresa;
	private $MunNegocio;
	private $Msetor;
	private $Mcolaborador;
	private $Mperfil;
	private $Mlinha;

	public function __construct($IdEmpresa=null, $IdUnNegocio=null, $IdSetor=null, $IdColaborador=null, $Nivel=null, $Empresa=null, $UnidadeNeg=null, $Linha=null, $Setor=null, $Regional=null, $NomeSetor=null, $Nome=null, $NivelPerfil=null, $Perfil=null, $DtInicio=null, $DtFinal=null, $FoneCorporativo=null, $FoneParticular=null, $Email=null, $Id=null, $Foto=null, $IdPerfil=null, $Expr1=null, $SetorAtivo=null, $NivelAdmin=null, $IdLinha=null, $Matricula=null, $Cpf=null, $SetorVago=null) {
		$this->setIdEmpresa($IdEmpresa);
		$this->setIdUnNegocio($IdUnNegocio);
		$this->setIdSetor($IdSetor);
		$this->setIdColaborador($IdColaborador);
		$this->setNivel($Nivel);
		$this->setEmpresa($Empresa);
		$this->setUnidadeNeg($UnidadeNeg);
		$this->setLinha($Linha);
		$this->setSetor($Setor);
		$this->setRegional($Regional);
		$this->setNomeSetor($NomeSetor);
		$this->setNome($Nome);
		$this->setNivelPerfil($NivelPerfil);
		$this->setPerfil($Perfil);
		$this->setDtInicio($DtInicio);
		$this->setDtFinal($DtFinal);
		$this->setFoneCorporativo($FoneCorporativo);
		$this->setFoneParticular($FoneParticular);
		$this->setEmail($Email);
		$this->setId($Id);
		$this->setFoto($Foto);
		$this->setIdPerfil($IdPerfil);
		$this->setExpr1($Expr1);
		$this->setSetorAtivo($SetorAtivo);
		$this->setNivelAdmin($NivelAdmin);
		$this->setIdLinha($IdLinha);
		$this->setMatricula($Matricula);
		$this->setCpf($Cpf);
		$this->setSetorVago($SetorVago);

	}

	private function setMethods($row) {
		$this->setIdEmpresa($row["ID_EMPRESA"]);
		$this->setIdUnNegocio($row["ID_UN_NEGOCIO"]);
		$this->setIdSetor($row["ID_SETOR"]);
		$this->setIdColaborador($row["ID_COLABORADOR"]);
		$this->setNivel($row["NIVEL"]);
		$this->setEmpresa($row["EMPRESA"]);
		$this->setUnidadeNeg($row["UNIDADE_NEG"]);
		$this->setLinha($row["LINHA"]);
		$this->setSetor($row["SETOR"]);
		$this->setRegional($row["REGIONAL"]);
		$this->setNomeSetor($row["NOME_SETOR"]);
		$this->setNome($row["NOME"]);
		$this->setNivelPerfil($row["NIVEL_PERFIL"]);
		$this->setPerfil($row["PERFIL"]);
		$this->setDtInicio($row["DT_INICIO"]);
		$this->setDtFinal($row["DT_FINAL"]);
		$this->setFoneCorporativo($row["FONE_CORPORATIVO"]);
		$this->setFoneParticular($row["FONE_PARTICULAR"]);
		$this->setEmail($row["EMAIL"]);
		$this->setId($row["ID"]);
		$this->setFoto($row["FOTO"]);
		$this->setIdPerfil($row["ID_PERFIL"]);
		$this->setExpr1($row["Expr1"]);
		$this->setSetorAtivo($row["SETOR_ATIVO"]);
		$this->setNivelAdmin($row["NIVEL_ADMIN"]);
		$this->setIdLinha($row["ID_LINHA"]);
		$this->setMatricula($row["MATRICULA"]);
		$this->setCpf($row["CPF"]);
		$this->setSetorVago($row["SETOR_VAGO"]);
	
		if(is_object($this->Mempresa)) {
			$this->Mempresa->selecionar($row["ID_EMPRESA"]);
		}

		if(is_object($this->MunNegocio)) {
			$this->MunNegocio->selecionar($row["ID_UN_NEGOCIO"]);
		}

		if(is_object($this->Msetor)) {
			$this->Msetor->selecionar($row["ID_SETOR"]);
		}

		if(is_object($this->Mcolaborador)) {
			$this->Mcolaborador->selecionar($row["ID_COLABORADOR"]);
		}

		if(is_object($this->Mperfil)) {
			$this->Mperfil->selecionar($row["ID_PERFIL"]);
		}

		if(is_object($this->Mlinha)) {
			$this->Mlinha->selecionar($row["ID_LINHA"]);
		}

	}
	
	public function injection(mMempresa $Mempresa=null, mMunNegocio $MunNegocio=null, mMsetor $Msetor=null, mMcolaborador $Mcolaborador=null, mMperfil $Mperfil=null, mMlinha $Mlinha=null) {
		$this->Mempresa = $Mempresa;
		$this->MunNegocio = $MunNegocio;
		$this->Msetor = $Msetor;
		$this->Mcolaborador = $Mcolaborador;
		$this->Mperfil = $Mperfil;
		$this->Mlinha = $Mlinha;
	}
	
	private function createObjects($row) {
		$obj = new mColaborador(); 

		$this->Mempresa = is_object($this->Mempresa) ? new $this->Mempresa : null;
		$this->MunNegocio = is_object($this->MunNegocio) ? new $this->MunNegocio : null;
		$this->Msetor = is_object($this->Msetor) ? new $this->Msetor : null;
		$this->Mcolaborador = is_object($this->Mcolaborador) ? new $this->Mcolaborador : null;
		$this->Mperfil = is_object($this->Mperfil) ? new $this->Mperfil : null;
		$this->Mlinha = is_object($this->Mlinha) ? new $this->Mlinha : null;

		$obj->injection($this->Mempresa, $this->MunNegocio, $this->Msetor, $this->Mcolaborador, $this->Mperfil, $this->Mlinha); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setIdEmpresa($value=null) {
		$this->IdEmpresa = $value;
	} 

	public function setIdUnNegocio($value=null) {
		$this->IdUnNegocio = $value;
	} 

	public function setIdSetor($value=null) {
		$this->IdSetor = $value;
	} 

	public function setIdColaborador($value=null) {
		$this->IdColaborador = $value;
	} 

	public function setNivel($value=null) {
		$this->Nivel = $value;
	} 

	public function setEmpresa($value=null) {
		$this->Empresa = $value;
	} 

	public function setUnidadeNeg($value=null) {
		$this->UnidadeNeg = $value;
	} 

	public function setLinha($value=null) {
		$this->Linha = $value;
	} 

	public function setSetor($value=null) {
		$this->Setor = $value;
	} 

	public function setRegional($value=null) {
		$this->Regional = $value;
	} 

	public function setNomeSetor($value=null) {
		$this->NomeSetor = $value;
	} 

	public function setNome($value=null) {
		$this->Nome = $value;
	} 

	public function setNivelPerfil($value=null) {
		$this->NivelPerfil = $value;
	} 

	public function setPerfil($value=null) {
		$this->Perfil = $value;
	} 

	public function setDtInicio($value=null) {
		$this->DtInicio = $value;
	} 

	public function setDtFinal($value=null) {
		$this->DtFinal = $value;
	} 

	public function setFoneCorporativo($value=null) {
		$this->FoneCorporativo = $value;
	} 

	public function setFoneParticular($value=null) {
		$this->FoneParticular = $value;
	} 

	public function setEmail($value=null) {
		$this->Email = $value;
	} 

	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFoto($value=null) {
		$this->Foto = $value;
	} 

	public function setIdPerfil($value=null) {
		$this->IdPerfil = $value;
	} 

	public function setExpr1($value=null) {
		$this->Expr1 = $value;
	} 

	public function setSetorAtivo($value=null) {
		$this->SetorAtivo = $value;
	} 

	public function setNivelAdmin($value=null) {
		$this->NivelAdmin = $value;
	} 

	public function setIdLinha($value=null) {
		$this->IdLinha = $value;
	} 

	public function setMatricula($value=null) {
		$this->Matricula = $value;
	} 

	public function setCpf($value=null) {
		$this->Cpf = $value;
	} 

	public function setSetorVago($value=null) {
		$this->SetorVago = $value;
	} 


	public function getIdEmpresa() {
		return $this->IdEmpresa;
	} 

	public function getIdUnNegocio() {
		return $this->IdUnNegocio;
	} 

	public function getIdSetor() {
		return $this->IdSetor;
	} 

	public function getIdColaborador() {
		return $this->IdColaborador;
	} 

	public function getNivel() {
		return $this->Nivel;
	} 

	public function getEmpresa() {
		return $this->Empresa;
	} 

	public function getUnidadeNeg() {
		return $this->UnidadeNeg;
	} 

	public function getLinha() {
		return $this->Linha;
	} 

	public function getSetor() {
		return $this->Setor;
	} 

	public function getRegional() {
		return $this->Regional;
	} 

	public function getNomeSetor() {
		return $this->NomeSetor;
	} 

	public function getNome() {
		return $this->Nome;
	} 

	public function getNivelPerfil() {
		return $this->NivelPerfil;
	} 

	public function getPerfil() {
		return $this->Perfil;
	} 

	public function getDtInicio() {
		return $this->DtInicio;
	} 

	public function getDtFinal() {
		return $this->DtFinal;
	} 

	public function getFoneCorporativo() {
		return $this->FoneCorporativo;
	} 

	public function getFoneParticular() {
		return $this->FoneParticular;
	} 

	public function getEmail() {
		return $this->Email;
	} 

	public function getId() {
		return $this->Id;
	} 

	public function getFoto() {
		return $this->Foto;
	} 

	public function getIdPerfil() {
		return $this->IdPerfil;
	} 

	public function getExpr1() {
		return $this->Expr1;
	} 

	public function getSetorAtivo() {
		return $this->SetorAtivo;
	} 

	public function getNivelAdmin() {
		return $this->NivelAdmin;
	} 

	public function getIdLinha() {
		return $this->IdLinha;
	} 

	public function getMatricula() {
		return $this->Matricula;
	} 

	public function getCpf() {
		return $this->Cpf;
	} 

	public function getSetorVago() {
		return $this->SetorVago;
	} 


	public function Mempresa() {
		return $this->Mempresa;
	} 

	public function MunNegocio() {
		return $this->MunNegocio;
	} 

	public function Msetor() {
		return $this->Msetor;
	} 

	public function Mcolaborador() {
		return $this->Mcolaborador;
	} 

	public function Mperfil() {
		return $this->Mperfil;
	} 

	public function Mlinha() {
		return $this->Mlinha;
	} 


	public function selecionar($id=null) {
		$query = "SELECT TOP 1 ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_COLABORADOR = " . $id . " ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByMempresa($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_EMPRESA =  ".$value."   ";
		echo $query;
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMempresa($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_EMPRESA =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByMunNegocio($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_UN_NEGOCIO =  ".$value."   ";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMunNegocio($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_UN_NEGOCIO =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByMsetor($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_SETOR =  ".$value."   ";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMsetor($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_SETOR =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByMcolaborador($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_COLABORADOR =  ".$value."   ";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMcolaborador($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_COLABORADOR =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByMperfil($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_PERFIL =  ".$value."   ";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMperfil($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_PERFIL =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByMlinha($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_LINHA =  ".$value."   ";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMlinha($value=null) {
		$query = "SELECT ID_EMPRESA, ID_UN_NEGOCIO, ID_SETOR, ID_COLABORADOR, NIVEL, EMPRESA, UNIDADE_NEG, LINHA, SETOR, REGIONAL, NOME_SETOR, NOME, NIVEL_PERFIL, PERFIL, DT_INICIO, DT_FINAL, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, ID, FOTO, ID_PERFIL, Expr1, SETOR_ATIVO, NIVEL_ADMIN, ID_LINHA, MATRICULA, CPF, SETOR_VAGO FROM [VW_COLABORADORSETOR] WHERE ID_LINHA =  ".$value."   ";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}