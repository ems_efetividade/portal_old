<?php 
require_once('lib/appConexao.php');

class mContato extends appConexao {


	private $Id;
	private $Nome;
	private $FkUser;
	private $Ativo;
	private $DataIn;
	private $DataNasc;
	private $FkContatoTipo;
	private $FkEmpresa;
	private $Decisor;

	private $ContatoTipo;
	private $ContatoTelefone;
	private $Telefone;
	private $Empresa;

	public function __construct($Id=null, $Nome=null, $FkUser=null, $Ativo=null, $DataIn=null, $DataNasc=null, $FkContatoTipo=null, $FkEmpresa=null, $Decisor=null) {
		$this->setId($Id);
		$this->setNome($Nome);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		$this->setDataNasc($DataNasc);
		$this->setFkContatoTipo($FkContatoTipo);
		$this->setFkEmpresa($FkEmpresa);
		$this->setDecisor($Decisor);
	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setNome($row["NOME"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setDataNasc($row["DATA_NASC"]);
		$this->setFkContatoTipo($row["FK_CONTATO_TIPO"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setDecisor($row["DECISOR"]);
	
		if(is_object($this->ContatoTipo)) {
			$this->ContatoTipo->selecionar($row["FK_CONTATO_TIPO"]);
		}

		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

		if(is_object($this->ContatoTelefone)) {
			$this->ContatoTelefone->selecionarByContato($row["ID"]);
		}

		

	}
	
	public function injection(mContatoTipo $ContatoTipo=null, mEmpresa $Empresa=null, mContatoTelefone $ContatoTelefone=null, mTelefone  $Telefone=null) {
		$this->ContatoTipo     = $ContatoTipo;
		$this->Empresa         = $Empresa;
		$this->ContatoTelefone = $ContatoTelefone;
		$this->Telefone		   = $Telefone;	 
	}
	
	private function createObjects($row) {
		$obj = new mContato(); 

		$this->ContatoTipo 		= is_object($this->ContatoTipo)     ? new $this->ContatoTipo     : null;
		$this->Empresa     		= is_object($this->Empresa)         ? new $this->Empresa         : null;
		$this->ContatoTelefone  = is_object($this->ContatoTelefone) ? new $this->ContatoTelefone : null;
		$this->Telefone         = is_object($this->Telefone)        ? new $this->Telefone : null;
		

		$obj->injection($this->ContatoTipo, $this->Empresa, $this->ContatoTelefone, $this->Telefone); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setNome($value=null) {
		$this->Nome = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setDataNasc($value=null) {
		$this->DataNasc = ($value === '1900-01-01') ? null : $value;
	} 

	public function setFkContatoTipo($value=null) {
		$this->FkContatoTipo = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setDecisor($value=null) {
		$this->Decisor = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getNome() {
		return $this->Nome;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getDataNasc() {
		return $this->DataNasc;
	} 

	public function getFkContatoTipo() {
		return $this->FkContatoTipo;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getDecisor() {
		return $this->Decisor;
	} 


	public function ContatoTipo() {
		return $this->ContatoTipo;
	} 

	public function Empresa() {
		return $this->Empresa;
	} 

	public function ContatoTelefone() {
		return $this->ContatoTelefone;
	}

	public function Telefone() {
		return $this->Telefone;
	}

	public function isBirthday() {
		$data = explode("-", $this->getDataNasc());
		return ($data[1] === date('m') && $data[2] === date('d')) ? true : false;
	}


	public function selecionar($id=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}

	public function listarByContatoTipo($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE FK_CONTATO_TIPO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}
	public function selecionarByContatoTipo($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE FK_CONTATO_TIPO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT  ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}