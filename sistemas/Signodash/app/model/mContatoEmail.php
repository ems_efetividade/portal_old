<?php 
require_once('lib/appConexao.php');

class mContatoEmail extends appConexao {


	private $Id;
	private $FkEmail;
	private $FkContato;
	private $Principal;
	private $Ativo;
	private $FkUser;
	private $DataIn;

	private $Email;
	private $Contato;

	public function __construct($Id=null, $FkEmail=null, $FkContato=null, $Principal=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkEmail($FkEmail);
		$this->setFkContato($FkContato);
		$this->setPrincipal($Principal);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmail($row["FK_EMAIL"]);
		$this->setFkContato($row["FK_CONTATO"]);
		$this->setPrincipal($row["PRINCIPAL"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Email)) {
			$this->Email->selecionar($row["FK_EMAIL"]);
		}

		if(is_object($this->Contato)) {
			$this->Contato->selecionar($row["FK_CONTATO"]);
		}

	}
	
	public function injection(mEmail $Email=null, mContato $Contato=null) {
		$this->Email = $Email;
		$this->Contato = $Contato;
	}
	
	private function createObjects($row) {
		$obj = new mContatoEmail(); 

		$this->Email = is_object($this->Email) ? new $this->Email : null;
		$this->Contato = is_object($this->Contato) ? new $this->Contato : null;

		$obj->injection($this->Email, $this->Contato); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmail($value=null) {
		$this->FkEmail = $value;
	} 

	public function setFkContato($value=null) {
		$this->FkContato = $value;
	} 

	public function setPrincipal($value=null) {
		$this->Principal = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmail() {
		return $this->FkEmail;
	} 

	public function getFkContato() {
		return $this->FkContato;
	} 

	public function getPrincipal() {
		return $this->Principal;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Email() {
		return $this->Email;
	} 

	public function Contato() {
		return $this->Contato;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmail($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE FK_EMAIL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmail($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE FK_EMAIL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByContato($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByContato($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_CONTATO, PRINCIPAL, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_EMAIL] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}