<?php 
require_once('lib/appConexao.php');

class mContatoTelefone extends appConexao {


	private $Id;
	private $Ddd;
	private $FkTelefone;
	private $FkContato;
	private $Principal;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Telefone;
	private $Contato;

	public function __construct($Id=null, $Ddd=null, $FkTelefone=null, $FkContato=null, $Principal=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setDdd($Ddd);
		$this->setFkTelefone($FkTelefone);
		$this->setFkContato($FkContato);
		$this->setPrincipal($Principal);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		return $this;
	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setDdd($row["DDD"]);
		$this->setFkTelefone($row["FK_TELEFONE"]);
		$this->setFkContato($row["FK_CONTATO"]);
		$this->setPrincipal($row["PRINCIPAL"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Telefone)) {
			$this->Telefone->selecionar($row["FK_TELEFONE"]);
		}

		if(is_object($this->Contato)) {
			$this->Contato->selecionar($row["FK_CONTATO"]);
		}

	}
	
	public function injection(mTelefone $Telefone=null, mContato $Contato=null) {
		$this->Telefone = $Telefone;
		$this->Contato = $Contato;
	}
	
	private function createObjects($row) {
		$obj = new mContatoTelefone(); 

		$this->Telefone = is_object($this->Telefone) ? new $this->Telefone : null;
		$this->Contato = is_object($this->Contato) ? new $this->Contato : null;

		$obj->injection($this->Telefone, $this->Contato); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setDdd($value=null) {
		$this->Ddd = $value;
	} 

	public function setFkTelefone($value=null) {
		$this->FkTelefone = $value;
	} 

	public function setFkContato($value=null) {
		$this->FkContato = $value;
	} 

	public function setPrincipal($value=null) {
		$this->Principal = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getDdd() {
		return $this->Ddd;
	} 

	public function getFkTelefone() {
		return $this->FkTelefone;
	} 

	public function getFkContato() {
		return $this->FkContato;
	} 

	public function getPrincipal() {
		return $this->Principal;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Telefone() {
		return $this->Telefone;
	} 

	public function Contato() {
		return $this->Contato;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByTelefone($value=null) {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE FK_TELEFONE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByTelefone($value=null) {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE FK_TELEFONE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByContato($value=null) {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByContato($value=null) {
		$query = "SELECT ID, DDD, FK_TELEFONE, FK_CONTATO, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_CONTATO_TELEFONE] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}