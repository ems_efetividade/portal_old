<?php 
require_once('lib/appConexao.php');

class mContatoTipo extends appConexao {


	private $Id;
	private $Descricao;
	private $Ativo;
	private $FkUser;
	private $DataIn;


	public function __construct($Id=null, $Descricao=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setDescricao($Descricao);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setDescricao($row["DESCRICAO"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mContatoTipo(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setDescricao($value=null) {
		$this->Descricao = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getDescricao() {
		return $this->Descricao;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_TIPO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_TIPO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}