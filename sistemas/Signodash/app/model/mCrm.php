<?php 
require_once('lib/appConexao.php');

class mCrm extends appConexao {


	private $Id;
	private $Uf;
	private $Numero;
	private $FkUser;
	private $Ativo;
	private $DataIn;


	public function __construct($Id=null, $Uf=null, $Numero=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setUf($Uf);
		$this->setNumero($Numero);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setUf($row["UF"]);
		$this->setNumero($row["NUMERO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mCrm(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setUf($value=null) {
		$this->Uf = $value;
	} 

	public function setNumero($value=null) {
		$this->Numero = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getUf() {
		return $this->Uf;
	} 

	public function getNumero() {
		return $this->Numero;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, UF, NUMERO, FK_USER, ATIVO, DATA_IN FROM [PA_CRM] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, UF, NUMERO, FK_USER, ATIVO, DATA_IN FROM [PA_CRM] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function inPxShare() {
		$query = "SELECT ID FROM PS_DIM_CRM WHERE CRM = '".$this->getUf() . $this->getNumero()."'";
		$rs = $this->executarQueryArray($query);

		if(!$rs) {
			return false;
		}

		return $rs[1]['ID'];

		
	}

}