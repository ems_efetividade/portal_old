<?php 
require_once('lib/appConexao.php');

class mEmail extends appConexao {


	private $Id;
	private $Email;
	private $Ativo;
	private $FkUser;
	private $DataIn;


	public function __construct($Id=null, $Email=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setEmail($Email);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setEmail($row["EMAIL"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mEmail(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setEmail($value=null) {
		$this->Email = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getEmail() {
		return $this->Email;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, EMAIL, ATIVO, FK_USER, DATA_IN FROM [PA_EMAIL] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, EMAIL, ATIVO, FK_USER, DATA_IN FROM [PA_EMAIL] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}