<?php 
require_once('lib/appConexao.php');

class mEmpresa extends appConexao {


	private $Id;
	private $Cnpj;
	private $RazaoSocial;
	private $Fantasia;
	private $Cep;
	private $Cidade;
	private $Bairro;
	private $Logradouro;
	private $Numero;
	private $Complemento;
	private $Estado;
	private $NumeroMedicos;
	private $NumeroPacientes;
	private $PacientesDia;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $EmpresaResponsavel;
	private $Colaborador;


	public function __construct($Id=null, $Cnpj=null, $RazaoSocial=null, $Fantasia=null, $Cep=null, $Cidade=null, $Bairro=null, $Logradouro=null, $Numero=null, $Complemento=null, $Estado=null, $NumeroMedicos=null, $NumeroPacientes=null, $PacientesDia=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setCnpj($Cnpj);
		$this->setRazaoSocial($RazaoSocial);
		$this->setFantasia($Fantasia);
		$this->setCep($Cep);
		$this->setCidade($Cidade);
		$this->setBairro($Bairro);
		$this->setLogradouro($Logradouro);
		$this->setNumero($Numero);
		$this->setComplemento($Complemento);
		$this->setEstado($Estado);
		$this->setNumeroMedicos($NumeroMedicos);
		$this->setNumeroPacientes($NumeroPacientes);
		$this->setPacientesDia($PacientesDia);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

		$this->EmpresaResponsavel = null;
		$this->Colaborador = null;

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setCnpj($row["CNPJ"]);
		$this->setRazaoSocial($row["RAZAO_SOCIAL"]);
		$this->setFantasia($row["FANTASIA"]);
		$this->setCep($row["CEP"]);
		$this->setCidade($row["CIDADE"]);
		$this->setBairro($row["BAIRRO"]);
		$this->setLogradouro($row["LOGRADOURO"]);
		$this->setNumero($row["NUMERO"]);
		$this->setComplemento($row["COMPLEMENTO"]);
		$this->setEstado($row["ESTADO"]);
		$this->setNumeroMedicos($row["NUMERO_MEDICOS"]);
		$this->setNumeroPacientes($row["NUMERO_PACIENTES"]);
		$this->setPacientesDia($row["PACIENTES_DIA"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);

		

		if(is_object($this->Colaborador)) {
			$this->Colaborador->selecionar($row["FK_USER"]);
		}

		if(is_object($this->EmpresaResponsavel)) {
			$this->EmpresaResponsavel->injection(new $this->Colaborador);
			$this->EmpresaResponsavel->selecionarByEmpresa($row["ID"]);
		}

	}
	
	public function injection(mEmpresaResponsavel $EmpresaResponsavel=null, mColaborador $colaborador=null) {
		$this->EmpresaResponsavel = $EmpresaResponsavel;
		$this->Colaborador        = $colaborador;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresa(); 

		$this->Colaborador        = is_object($this->Colaborador)        ? new $this->Colaborador        : null;
		$this->EmpresaResponsavel = is_object($this->EmpresaResponsavel) ? new $this->EmpresaResponsavel : null;


		$obj->injection($this->EmpresaResponsavel, $this->Colaborador); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setCnpj($value=null) {
		$this->Cnpj = $value;
	} 

	public function setRazaoSocial($value=null) {
		$this->RazaoSocial = $value;
	} 

	public function setFantasia($value=null) {
		$this->Fantasia = $value;
	} 

	public function setCep($value=null) {
		$this->Cep = $value;
	} 

	public function setCidade($value=null) {
		$this->Cidade = $value;
	} 

	public function setBairro($value=null) {
		$this->Bairro = $value;
	} 

	public function setLogradouro($value=null) {
		$this->Logradouro = $value;
	} 

	public function setNumero($value=null) {
		$this->Numero = $value;
	} 

	public function setComplemento($value=null) {
		$this->Complemento = $value;
	} 

	public function setEstado($value=null) {
		$this->Estado = $value;
	} 

	public function setNumeroMedicos($value=null) {
		$this->NumeroMedicos = $value;
	} 

	public function setNumeroPacientes($value=null) {
		$this->NumeroPacientes = $value;
	} 

	public function setPacientesDia($value=null) {
		$this->PacientesDia = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getCnpj() {
		return $this->Cnpj;
	} 

	public function getRazaoSocial() {
		return $this->RazaoSocial;
	} 

	public function getFantasia() {
		return $this->Fantasia;
	} 

	public function getCep() {
		return $this->Cep;
	} 

	public function getCidade() {
		return $this->Cidade;
	} 

	public function getBairro() {
		return $this->Bairro;
	} 

	public function getLogradouro() {
		return $this->Logradouro;
	} 

	public function getNumero() {
		return $this->Numero;
	} 

	public function getComplemento() {
		return $this->Complemento;
	} 

	public function getEstado() {
		return $this->Estado;
	} 

	public function getNumeroMedicos() {
		return $this->NumeroMedicos;
	} 

	public function getNumeroPacientes() {
		return $this->NumeroPacientes;
	} 

	public function getPacientesDia() {
		return $this->PacientesDia;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Responsavel() {
		return $this->EmpresaResponsavel;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, CNPJ, RAZAO_SOCIAL, FANTASIA, CEP, CIDADE, BAIRRO, LOGRADOURO, NUMERO, COMPLEMENTO, ESTADO, NUMERO_MEDICOS, NUMERO_PACIENTES, PACIENTES_DIA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function selecionarByCNPJ($id=null) {
		$query = "SELECT ID, CNPJ, RAZAO_SOCIAL, FANTASIA, CEP, CIDADE, BAIRRO, LOGRADOURO, NUMERO, COMPLEMENTO, ESTADO, NUMERO_MEDICOS, NUMERO_PACIENTES, PACIENTES_DIA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA] WHERE CNPJ = '" . $id . "' AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function selecionarUltimoCadastro() {
		$query = "SELECT TOP 1 ID, CNPJ, RAZAO_SOCIAL, FANTASIA, CEP, CIDADE, BAIRRO, LOGRADOURO, NUMERO, COMPLEMENTO, ESTADO, NUMERO_MEDICOS, NUMERO_PACIENTES, PACIENTES_DIA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA] WHERE ATIVO = 1 ORDER BY DATA_IN DESC";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, CNPJ, RAZAO_SOCIAL, FANTASIA, CEP, CIDADE, BAIRRO, LOGRADOURO, NUMERO, COMPLEMENTO, ESTADO, NUMERO_MEDICOS, NUMERO_PACIENTES, PACIENTES_DIA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA] WHERE ATIVO = 1 ORDER BY DATA_IN DESC";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function getMesoregiao() {
		$query = "SELECT DISTINCT
					B.DESCRICAO AS MESOREGIAO,
					E.ID AS FK_MESOREGIAO,
					D.FK_EMPRESA
				FROM
					PA_CEP_BRICK A
				INNER JOIN
					PA_MESO_BRICK B ON B.BRICK = A.BRICK
				INNER JOIN
					PA_EMPRESA C ON C.CEP = A.CEP
				INNER JOIN
					PA_EMPRESA_PLATAFORMA D ON D.FK_EMPRESA = C.ID
				INNER JOIN
					PA_MESOREGIAO E ON E.MESOREGIAO = B.DESCRICAO	
				WHERE
					A.ATIVO = 1	AND FK_EMPRESA = " . $this->getId();
			
		$rs = $this->executarQueryArray($query);
		return $rs[1]['FK_MESOREGIAO'];
	}

	public function listarByResponsavel($value=null) {
		$query = "SELECT ID, CNPJ, RAZAO_SOCIAL, FANTASIA, CEP, CIDADE, BAIRRO, LOGRADOURO, NUMERO, COMPLEMENTO, ESTADO, NUMERO_MEDICOS, NUMERO_PACIENTES, PACIENTES_DIA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA] WHERE FK_USER = ".$value." ATIVO = 1 ORDER BY DATA_IN DESC";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
		return $objs;
	}

}