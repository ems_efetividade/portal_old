<?php 
require_once('lib/appConexao.php');

class mEmpresaConsultas extends appConexao {


	private $Id;
	private $Valor;
	private $Percentual;
	private $FkUser;
	private $Ativo;
	private $DataIn;
	private $FkEmpresa;

	private $Empresa;

	public function __construct($Id=null, $Valor=null, $Percentual=null, $FkUser=null, $Ativo=null, $DataIn=null, $FkEmpresa=null) {
		$this->setId($Id);
		$this->setValor($Valor);
		$this->setPercentual($Percentual);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		$this->setFkEmpresa($FkEmpresa);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setValor($row["VALOR"]);
		$this->setPercentual($row["PERCENTUAL"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null) {
		$this->Empresa = $Empresa;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaConsultas(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;

		$obj->injection($this->Empresa); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setValor($value=null) {
		$this->Valor = $value;
	} 

	public function setPercentual($value=null) {
		$this->Percentual = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getValor() {
		return $this->Valor;
	} 

	public function getPercentual() {
		return trim(str_replace("%", "", $this->Percentual));
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, VALOR, PERCENTUAL, FK_USER, ATIVO, DATA_IN, FK_EMPRESA FROM [PA_EMPRESA_CONSULTAS] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, VALOR, PERCENTUAL, FK_USER, ATIVO, DATA_IN, FK_EMPRESA FROM [PA_EMPRESA_CONSULTAS] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, VALOR, PERCENTUAL, FK_USER, ATIVO, DATA_IN, FK_EMPRESA FROM [PA_EMPRESA_CONSULTAS] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, VALOR, PERCENTUAL, FK_USER, ATIVO, DATA_IN, FK_EMPRESA FROM [PA_EMPRESA_CONSULTAS] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}