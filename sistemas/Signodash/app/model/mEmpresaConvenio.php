<?php 
require_once('lib/appConexao.php');

class mEmpresaConvenio extends appConexao {


	private $Id;
	private $FkEmpresa;
	private $FkConvenio;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Empresa;
	private $Convenio;

	public function __construct($Id=null, $FkEmpresa=null, $FkConvenio=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkConvenio($FkConvenio);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkConvenio($row["FK_CONVENIO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

		if(is_object($this->Convenio)) {
			$this->Convenio->selecionar($row["FK_CONVENIO"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null, mConvenio $Convenio=null) {
		$this->Empresa = $Empresa;
		$this->Convenio = $Convenio;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaConvenio(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;
		$this->Convenio = is_object($this->Convenio) ? new $this->Convenio : null;

		$obj->injection($this->Empresa, $this->Convenio); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkConvenio($value=null) {
		$this->FkConvenio = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkConvenio() {
		return $this->FkConvenio;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 

	public function Convenio() {
		return $this->Convenio;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByConvenio($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE FK_CONVENIO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByConvenio($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_CONVENIO, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_CONVENIO] WHERE FK_CONVENIO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}