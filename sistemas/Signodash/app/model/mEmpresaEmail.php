<?php 
require_once('lib/appConexao.php');

class mEmpresaEmail extends appConexao {


	private $Id;
	private $FkEmail;
	private $FkEmpresa;
	private $Principal;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Email;
	private $Empresa;

	public function __construct($Id=null, $FkEmail=null, $FkEmpresa=null, $Principal=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkEmail($FkEmail);
		$this->setFkEmpresa($FkEmpresa);
		$this->setPrincipal($Principal);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmail($row["FK_EMAIL"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setPrincipal($row["PRINCIPAL"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Email)) {
			$this->Email->selecionar($row["FK_EMAIL"]);
		}

		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

	}
	
	public function injection(mEmail $Email=null, mEmpresa $Empresa=null) {
		$this->Email = $Email;
		$this->Empresa = $Empresa;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaEmail(); 

		$this->Email = is_object($this->Email) ? new $this->Email : null;
		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;

		$obj->injection($this->Email, $this->Empresa); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmail($value=null) {
		$this->FkEmail = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setPrincipal($value=null) {
		$this->Principal = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmail() {
		return $this->FkEmail;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getPrincipal() {
		return $this->Principal;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Email() {
		return $this->Email;
	} 

	public function Empresa() {
		return $this->Empresa;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmail($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE FK_EMAIL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmail($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE FK_EMAIL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMAIL, FK_EMPRESA, PRINCIPAL, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_EMAIL] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}