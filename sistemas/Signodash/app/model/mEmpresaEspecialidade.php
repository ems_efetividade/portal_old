<?php 
require_once('lib/appConexao.php');

class mEmpresaEspecialidade extends appConexao {


	private $Id;
	private $FkEmpresa;
	private $FkEspecialidade;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Empresa;
	private $Especialidade;

	public function __construct($Id=null, $FkEmpresa=null, $FkEspecialidade=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkEspecialidade($FkEspecialidade);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkEspecialidade($row["FK_ESPECIALIDADE"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

		if(is_object($this->Especialidade)) {
			$this->Especialidade->selecionar($row["FK_ESPECIALIDADE"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null, mEspecialidade $Especialidade=null) {
		$this->Empresa = $Empresa;
		$this->Especialidade = $Especialidade;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaEspecialidade(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;
		$this->Especialidade = is_object($this->Especialidade) ? new $this->Especialidade : null;

		$obj->injection($this->Empresa, $this->Especialidade); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkEspecialidade($value=null) {
		$this->FkEspecialidade = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkEspecialidade() {
		return $this->FkEspecialidade;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 

	public function Especialidade() {
		return $this->Especialidade;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEspecialidade($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE FK_ESPECIALIDADE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEspecialidade($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_ESPECIALIDADE] WHERE FK_ESPECIALIDADE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}