<?php 
require_once('lib/appConexao.php');

class mEmpresaObjetivo extends appConexao {


	private $Id;
	private $Objetivo;
	private $FkEmpresa;
	private $Prazo;
	private $Ativo;
	private $FkUser;
	private $DataIn;

	private $Empresa;

	public function __construct($Id=null, $Objetivo=null, $FkEmpresa=null, $Prazo=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setObjetivo($Objetivo);
		$this->setFkEmpresa($FkEmpresa);
		$this->setPrazo($Prazo);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setObjetivo($row["OBJETIVO"]+0);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setPrazo($row["PRAZO"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null) {
		$this->Empresa = $Empresa;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaObjetivo(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;

		$obj->injection($this->Empresa); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setObjetivo($value=null) {
		$this->Objetivo = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setPrazo($value=null) {
		$this->Prazo = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getObjetivo() {
		return $this->Objetivo;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getPrazo() {
		return $this->Prazo;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, OBJETIVO, FK_EMPRESA, PRAZO, ATIVO, FK_USER, DATA_IN FROM [PA_EMPRESA_OBJETIVO] WHERE ID = " . $id . " AND ATIVO = 1";
		echo $query;
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, OBJETIVO, FK_EMPRESA, PRAZO, ATIVO, FK_USER, DATA_IN FROM [PA_EMPRESA_OBJETIVO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, OBJETIVO, FK_EMPRESA, PRAZO, ATIVO, FK_USER, DATA_IN FROM [PA_EMPRESA_OBJETIVO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, OBJETIVO, FK_EMPRESA, PRAZO, ATIVO, FK_USER, DATA_IN FROM [PA_EMPRESA_OBJETIVO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}