<?php 
require_once('lib/appConexao.php');

class mEmpresaPlataforma extends appConexao {


	private $Id;
	private $FkEmpresa;
	private $FkPlataforma;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Empresa;
	private $Plataforma;

	public function __construct($Id=null, $FkEmpresa=null, $FkPlataforma=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkPlataforma($FkPlataforma);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkPlataforma($row["FK_PLATAFORMA"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

		if(is_object($this->Plataforma)) {
			$this->Plataforma->selecionar($row["FK_PLATAFORMA"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null, mPlataforma $Plataforma=null) {
		$this->Empresa = $Empresa;
		$this->Plataforma = $Plataforma;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaPlataforma(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;
		$this->Plataforma = is_object($this->Plataforma) ? new $this->Plataforma : null;

		$obj->injection($this->Empresa, $this->Plataforma); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkPlataforma($value=null) {
		$this->FkPlataforma = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkPlataforma() {
		return $this->FkPlataforma;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 

	public function Plataforma() {
		return $this->Plataforma;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE FK_EMPRESA =  ".$value."   AND (ATIVO = 1 OR ATIVO IS NULL)";
		//$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE FK_EMPRESA =  ".$value."   AND (ATIVO = 1)";
		$rs = $this->executarQueryArray($query);
		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE FK_EMPRESA =  ".$value."   /*AND ATIVO = 1*/";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByPlataforma($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByPlataforma($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_PLATAFORMA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_PLATAFORMA] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}