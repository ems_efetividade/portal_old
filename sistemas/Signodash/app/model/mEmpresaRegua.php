<?php 
require_once('lib/appConexao.php');

class mEmpresaRegua extends appConexao {


	private $Id;
	private $FkRegua;
	private $FkEmpresa;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Regua;
	private $Empresa;

	public function __construct($Id=null, $FkRegua=null, $FkEmpresa=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkRegua($FkRegua);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkRegua($row["FK_REGUA"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Regua)) {
			$this->Regua->selecionar($row["FK_REGUA"]);
		}

		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

	}
	
	public function injection(mRegua $Regua=null, mEmpresa $Empresa=null) {
		$this->Regua = $Regua;
		$this->Empresa = $Empresa;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaRegua(); 

		$this->Regua = is_object($this->Regua) ? new $this->Regua : null;
		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;

		$obj->injection($this->Regua, $this->Empresa); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkRegua($value=null) {
		$this->FkRegua = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkRegua() {
		return $this->FkRegua;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Regua() {
		return $this->Regua;
	} 

	public function Empresa() {
		return $this->Empresa;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByRegua($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE FK_REGUA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByRegua($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE FK_REGUA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}