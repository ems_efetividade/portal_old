<?php 
require_once('lib/appConexao.php');

class mEmpresaResponsavel extends appConexao {


	private $Id;
	private $FkResponsavel;
	private $FkEmpresa;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Colaborador;
	private $Empresa;

	public function __construct($Id=null, $FkResponsavel=null, $FkEmpresa=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkResponsavel($FkResponsavel);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

		$this->Colaborador = null;

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkResponsavel($row["FK_RESPONSAVEL"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);

		if(is_object($this->Colaborador)) {
			$this->Colaborador->selecionar($row["FK_RESPONSAVEL"]);
		}

		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

	}
	
	public function injection(mColaborador $colaborador=null, mEmpresa $Empresa=null) {
		$this->Colaborador = $colaborador;
		$this->Empresa = $Empresa;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaResponsavel(); 

		$this->Colaborador = is_object($this->Colaborador) ? new $this->Colaborador : null;
		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;

		$obj->injection($this->Colaborador, $this->Empresa); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkResponsavel($value=null) {
		$this->FkResponsavel = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkResponsavel() {
		return $this->FkResponsavel;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Colaborador() {
		return $this->Colaborador;
	} 

	public function Empresa() {
		return $this->Empresa;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByResponsavel($value=null) {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE FK_RESPONSAVEL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByResponsavel($value=null) {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE FK_RESPONSAVEL =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_RESPONSAVEL, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_RESPONSAVEL] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}