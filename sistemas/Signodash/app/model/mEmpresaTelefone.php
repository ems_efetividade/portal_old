<?php 
require_once('lib/appConexao.php');

class mEmpresaTelefone extends appConexao {


	private $Id;
	private $FkEmpresa;
	private $FkTelefone;
	private $Ativo;
	private $FkUser;
	private $DataIn;
	private $Principal;

	private $Empresa;
	private $Telefone;

	public function __construct($Id=null, $FkEmpresa=null, $FkTelefone=null, $Ativo=null, $FkUser=null, $DataIn=null, $Principal=null) {
		$this->setId($Id);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkTelefone($FkTelefone);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);
		$this->setPrincipal($Principal);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkTelefone($row["FK_TELEFONE"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setPrincipal($row["PRINCIPAL"]);
	
		if(is_object($this->Empresa)) {
			$this->Empresa->selecionar($row["FK_EMPRESA"]);
		}

		if(is_object($this->Telefone)) {
			$this->Telefone->selecionar($row["FK_TELEFONE"]);
		}

	}
	
	public function injection(mEmpresa $Empresa=null, mTelefone $Telefone=null) {
		$this->Empresa = $Empresa;
		$this->Telefone = $Telefone;
	}
	
	private function createObjects($row) {
		$obj = new mEmpresaTelefone(); 

		$this->Empresa = is_object($this->Empresa) ? new $this->Empresa : null;
		$this->Telefone = is_object($this->Telefone) ? new $this->Telefone : null;

		$obj->injection($this->Empresa, $this->Telefone); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkTelefone($value=null) {
		$this->FkTelefone = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setPrincipal($value=null) {
		$this->Principal = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkTelefone() {
		return $this->FkTelefone;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getPrincipal() {
		return $this->Principal;
	} 


	public function Empresa() {
		return $this->Empresa;
	} 

	public function Telefone() {
		return $this->Telefone;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		if($rs) {
			foreach($rs as $row) {
				$objs[] = $this->createObjects($row);
			}
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByTelefone($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE FK_TELEFONE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByTelefone($value=null) {
		$query = "SELECT ID, FK_EMPRESA, FK_TELEFONE, ATIVO, FK_USER, DATA_IN, PRINCIPAL FROM [PA_EMPRESA_TELEFONE] WHERE FK_TELEFONE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}