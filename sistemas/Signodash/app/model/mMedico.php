<?php 
require_once('lib/appConexao.php');

class mMedico extends appConexao {


	private $Id;
	private $FkCrm;
	private $FkContato;
	private $Ativo;
	private $FkUser;
	private $DataIn;
	private $Residente;

	private $Crm;
	private $Contato;

	public function __construct($Id=null, $FkCrm=null, $FkContato=null, $Ativo=null, $FkUser=null, $DataIn=null, $Residente=null) {
		$this->setId($Id);
		$this->setFkCrm($FkCrm);
		$this->setFkContato($FkContato);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);
		$this->setResidente($Residente);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkCrm($row["FK_CRM"]);
		$this->setFkContato($row["FK_CONTATO"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setResidente($row["RESIDENTE"]);
	
		if(is_object($this->Crm)) {
			$this->Crm->selecionar($row["FK_CRM"]);
		}

		if(is_object($this->Contato)) {
			$this->Contato->selecionar($row["FK_CONTATO"]);
		}

	}
	
	public function injection(mCrm $Crm=null, mContato $Contato=null) {
		$this->Crm = $Crm;
		$this->Contato = $Contato;
	}
	
	private function createObjects($row) {
		$obj = new mMedico(); 

		$this->Crm = is_object($this->Crm) ? new $this->Crm : null;
		$this->Contato = is_object($this->Contato) ? new $this->Contato : null;

		$obj->injection($this->Crm, $this->Contato); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkCrm($value=null) {
		$this->FkCrm = $value;
	} 

	public function setFkContato($value=null) {
		$this->FkContato = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setResidente($value=null) {
		$this->Residente = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkCrm() {
		return $this->FkCrm;
	} 

	public function getFkContato() {
		return $this->FkContato;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getResidente() {
		return $this->Residente;
	} 


	public function Crm() {
		return $this->Crm;
	} 

	public function Contato() {
		return $this->Contato;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByCrm($value=null) {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE FK_CRM =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByCrm($value=null) {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE FK_CRM =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByContato($value=null) {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByContato($value=null) {
		$query = "SELECT ID, FK_CRM, FK_CONTATO, ATIVO, FK_USER, DATA_IN, RESIDENTE FROM [PA_MEDICO] WHERE FK_CONTATO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}