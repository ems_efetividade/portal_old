<?php 
require_once('lib/appConexao.php');

class mMedicoConsulta extends appConexao {


	private $Id;
	private $Crm;
	private $Uf;
	private $DataIn;
	private $Ativo;
	private $Nome;


	public function __construct($Id=null, $Crm=null, $Uf=null, $DataIn=null, $Ativo=null, $Nome=null) {
		$this->setId($Id);
		$this->setCrm($Crm);
		$this->setUf($Uf);
		$this->setDataIn($DataIn);
		$this->setAtivo($Ativo);
		$this->setNome($Nome);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setCrm($row["CRM"]);
		$this->setUf($row["UF"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setNome($row["NOME"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mMedicoConsulta(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setCrm($value=null) {
		$this->Crm = $value;
	} 

	public function setUf($value=null) {
		$this->Uf = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setNome($value=null) {
		$this->Nome = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getCrm() {
		return $this->Crm;
	} 

	public function getUf() {
		return $this->Uf;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getNome() {
		return $this->Nome;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, CRM, UF, DATA_IN, ATIVO, NOME FROM [PA_MEDICO_CONSULTA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, CRM, UF, DATA_IN, ATIVO, NOME FROM [PA_MEDICO_CONSULTA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}