<?php 
require_once('lib/appConexao.php');

class mMedicoEspecialidade extends appConexao {


	private $Id;
	private $FkMedico;
	private $FkEspecialidade;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	private $Medico;
	private $Especialidade;

	public function __construct($Id=null, $FkMedico=null, $FkEspecialidade=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkMedico($FkMedico);
		$this->setFkEspecialidade($FkEspecialidade);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkMedico($row["FK_MEDICO"]);
		$this->setFkEspecialidade($row["FK_ESPECIALIDADE"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
		if(is_object($this->Medico)) {
			$this->Medico->selecionar($row["FK_MEDICO"]);
		}

		if(is_object($this->Especialidade)) {
			$this->Especialidade->selecionar($row["FK_ESPECIALIDADE"]);
		}

	}
	
	public function injection(mMedico $Medico=null, mEspecialidade $Especialidade=null) {
		$this->Medico = $Medico;
		$this->Especialidade = $Especialidade;
	}
	
	private function createObjects($row) {
		$obj = new mMedicoEspecialidade(); 

		$this->Medico = is_object($this->Medico) ? new $this->Medico : null;
		$this->Especialidade = is_object($this->Especialidade) ? new $this->Especialidade : null;

		$obj->injection($this->Medico, $this->Especialidade); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkMedico($value=null) {
		$this->FkMedico = $value;
	} 

	public function setFkEspecialidade($value=null) {
		$this->FkEspecialidade = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkMedico() {
		return $this->FkMedico;
	} 

	public function getFkEspecialidade() {
		return $this->FkEspecialidade;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function Medico() {
		return $this->Medico;
	} 

	public function Especialidade() {
		return $this->Especialidade;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByMedico($value=null) {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE FK_MEDICO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByMedico($value=null) {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE FK_MEDICO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listarByEspecialidade($value=null) {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE FK_ESPECIALIDADE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByEspecialidade($value=null) {
		$query = "SELECT ID, FK_MEDICO, FK_ESPECIALIDADE, FK_USER, ATIVO, DATA_IN FROM [PA_MEDICO_ESPECIALIDADE] WHERE FK_ESPECIALIDADE =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}