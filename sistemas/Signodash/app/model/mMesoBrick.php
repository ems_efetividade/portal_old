<?php 
require_once('lib/appConexao.php');

class mMesoBrick extends appConexao {


	private $Id;
	private $Brick;
	private $Cep;
	private $Descricao;
	private $CodDescricao;
	private $FkUser;
	private $Ativo;
	private $DataIn;


	public function __construct($Id=null, $Brick=null, $Cep=null, $Descricao=null, $CodDescricao=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setBrick($Brick);
		$this->setCep($Cep);
		$this->setDescricao($Descricao);
		$this->setCodDescricao($CodDescricao);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setBrick($row["BRICK"]);
		$this->setCep($row["CEP"]);
		$this->setDescricao($row["DESCRICAO"]);
		$this->setCodDescricao($row["COD_DESCRICAO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mMesoBrick(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setBrick($value=null) {
		$this->Brick = $value;
	} 

	public function setCep($value=null) {
		$this->Cep = $value;
	} 

	public function setDescricao($value=null) {
		$this->Descricao = $value;
	} 

	public function setCodDescricao($value=null) {
		$this->CodDescricao = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getBrick() {
		return $this->Brick;
	} 

	public function getCep() {
		return $this->Cep;
	} 

	public function getDescricao() {
		return $this->Descricao;
	} 

	public function getCodDescricao() {
		return $this->CodDescricao;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, BRICK, CEP, DESCRICAO, COD_DESCRICAO, FK_USER, ATIVO, DATA_IN FROM [PA_MESO_BRICK] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, BRICK, CEP, DESCRICAO, COD_DESCRICAO, FK_USER, ATIVO, DATA_IN FROM [PA_MESO_BRICK] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}