<?php 
require_once('lib/appConexao.php');

class mMesoregiao extends appConexao {


	private $Id;
	private $Mesoregiao;
	private $Ativo;
	private $DataIn;


	public function __construct($Id=null, $Mesoregiao=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setMesoregiao($Mesoregiao);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setMesoregiao($row["MESOREGIAO"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mMesoregiao(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setMesoregiao($value=null) {
		$this->Mesoregiao = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getMesoregiao() {
		return $this->Mesoregiao;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, MESOREGIAO, ATIVO, DATA_IN FROM [PA_MESOREGIAO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, MESOREGIAO, ATIVO, DATA_IN FROM [PA_MESOREGIAO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}