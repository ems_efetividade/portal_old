<?php 
require_once('lib/appConexao.php');

class mPlataforma extends appConexao {


	private $Id;
	private $Nome;
	private $FkUser;
	private $Ativo;
	private $DataIn;
	private $Depara;


	public function __construct($Id=null, $Nome=null, $FkUser=null, $Ativo=null, $DataIn=null, $Depara=null) {
		$this->setId($Id);
		$this->setNome($Nome);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		$this->setDepara($Depara);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setNome($row["NOME"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setDepara($row["DEPARA"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mPlataforma(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setNome($value=null) {
		$this->Nome = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setDepara($value=null) {
		$this->Depara = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getNome() {
		return $this->Nome;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getDepara() {
		return $this->Depara;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN FROM [PA_PLATAFORMA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN FROM [PA_PLATAFORMA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}