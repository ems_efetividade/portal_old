<?php 
require_once('lib/appConexao.php');

class mPxMesoPlataforma extends appConexao {


	private $FkPlataforma;
	private $Meso;
	private $M01;
	private $M00;
	private $Id;

	private $Plataforma;

	public function __construct($FkPlataforma=null, $Meso=null, $M01=null, $M00=null, $Id=null) {
		$this->setFkPlataforma($FkPlataforma);
		$this->setMeso($Meso);
		$this->setM01($M01);
		$this->setM00($M00);
		$this->setId($Id);

	}

	private function setMethods($row) {
		$this->setFkPlataforma($row["FK_PLATAFORMA"]);
		$this->setMeso($row["MESO"]);
		$this->setM01($row["M01"]);
		$this->setM00($row["M00"]);
		$this->setId($row["ID"]);
	
		if(is_object($this->Plataforma)) {
			$this->Plataforma->selecionar($row["FK_PLATAFORMA"]);
		}

	}
	
	public function injection(mPlataforma $Plataforma=null) {
		$this->Plataforma = $Plataforma;
	}
	
	private function createObjects($row) {
		$obj = new mPxMesoPlataforma(); 

		$this->Plataforma = is_object($this->Plataforma) ? new $this->Plataforma : null;

		$obj->injection($this->Plataforma); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setFkPlataforma($value=null) {
		$this->FkPlataforma = $value;
	} 

	public function setMeso($value=null) {
		$this->Meso = $value;
	} 

	public function setM01($value=null) {
		$this->M01 = $value;
	} 

	public function setM00($value=null) {
		$this->M00 = $value;
	} 

	public function setId($value=null) {
		$this->Id = $value;
	} 


	public function getFkPlataforma() {
		return $this->FkPlataforma;
	} 

	public function getMeso() {
		return $this->Meso;
	} 

	public function getM01() {
		return $this->M01;
	} 

	public function getM00() {
		return $this->M00;
	} 

	public function getId() {
		return $this->Id;
	} 


	public function Plataforma() {
		return $this->Plataforma;
	} 


	public function selecionar($id=null) {
		$query = "SELECT FK_PLATAFORMA, MESO, M01, M00, ID FROM [PA_PX_MESO_PLATAFORMA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT FK_PLATAFORMA, MESO, M01, M00, ID FROM [PA_PX_MESO_PLATAFORMA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function listarByPlataforma($value=null) {
		$query = "SELECT FK_PLATAFORMA, MESO, M01, M00, ID FROM [PA_PX_MESO_PLATAFORMA] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}
	public function selecionarByPlataforma($value=null) {
		$query = "SELECT FK_PLATAFORMA, MESO, M01, M00, ID FROM [PA_PX_MESO_PLATAFORMA] WHERE FK_PLATAFORMA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

}