<?php 
require_once('lib/appConexao.php');

class mPxPeriodo extends appConexao {


	private $Id;
	private $Periodo;
	private $Mes;
	private $Ano;
	private $Label;
	private $Ativo;
	private $DataIn;

	private $nomeMes;


	public function __construct($Id=null, $Periodo=null, $Mes=null, $Ano=null, $Label=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setPeriodo($Periodo);
		$this->setMes($Mes);
		$this->setAno($Ano);
		$this->setLabel($Label);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);


		$this->nomeMes = array(
			'Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'
		);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setPeriodo($row["PERIODO"]);
		$this->setMes($row["MES"]);
		$this->setAno($row["ANO"]);
		$this->setLabel($row["LABEL"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mPxPeriodo(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setPeriodo($value=null) {
		$this->Periodo = $value;
	} 

	public function setMes($value=null) {
		$this->Mes = $value;
	} 

	public function setAno($value=null) {
		$this->Ano = $value;
	} 

	public function setLabel($value=null) {
		$this->Label = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getPeriodo() {
		return $this->Periodo;
	} 

	public function getMes() {
		return $this->Mes;
	} 

	public function getAno() {
		return $this->Ano;
	} 

	public function getLabel() {
		return $this->Label;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 



	public function selecionar($id=null) {
		$query = "SELECT ID, PERIODO, MES, ANO, LABEL, ATIVO, DATA_IN FROM [PA_PX_PERIODO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, PERIODO, MES, ANO, LABEL, ATIVO, DATA_IN FROM [PA_PX_PERIODO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

	public function selecionarPeriodoAtual() {
		$query = "SELECT TOP 1 ID, PERIODO, MES, ANO, LABEL, ATIVO, DATA_IN FROM [PA_PX_PERIODO] WHERE  ATIVO = 1 ORDER BY ANO DESC, MES DESC";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function selecionarCabecalho($max=12) {
		$this->selecionarPeriodoAtual();
		$data = date('Y-m-d', strtotime("-11 months", strtotime($this->getAno() . '-'.$this->getMes() .'-01')));

		for($i=1; $i<$max; $i++ ) {
			$n = date('Y-m-d', strtotime("+" . ($i-1) . " months", strtotime($data))); 			
			$a[] = $this->nomeMes[(date('n', strtotime($n))-1)] . '/' . date('y', strtotime($n));
		}

		return $a;

	}

}