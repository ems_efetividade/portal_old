<?php 
require_once('lib/appConexao.php');

class mTelefone extends appConexao {


	private $Id;
	private $Ddd;
	private $Tel;
	private $Principal;
	private $FkUser;
	private $Ativo;
	private $DataIn;
	private $Whatsapp;


	public function __construct($Id=null, $Ddd=null, $Tel=null, $Principal=null, $FkUser=null, $Ativo=null, $DataIn=null, $Whatsapp=null) {
		$this->setId($Id);
		$this->setDdd($Ddd);
		$this->setTel($Tel);
		$this->setPrincipal($Principal);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		$this->setWhatsapp($Whatsapp);

	}

	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setDdd($row["DDD"]);
		$this->setTel($row["TEL"]);
		$this->setPrincipal($row["PRINCIPAL"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setWhatsapp($row["WHATSAPP"]);
	
			}
	
	public function injection() {
			}
	
	private function createObjects($row) {
		$obj = new mTelefone(); 

		
		$obj->injection(); 
		$obj->setMethods($row); 
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setDdd($value=null) {
		$this->Ddd = $value;
	} 

	public function setTel($value=null) {
		$this->Tel = $value;
	} 

	public function setPrincipal($value=null) {
		$this->Principal = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setWhatsapp($value=null) {
		$this->Whatsapp = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getDdd() {
		return $this->Ddd;
	} 

	public function getTel() {
		return $this->Tel;
	} 

	public function getPrincipal() {
		return $this->Principal;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getWhatsapp() {
		return $this->Whatsapp;
	} 

	public function getWhatsappIcon() {
		return ($this->getWhatsapp() == 1) ? '<i style="" class="text-success fab fa-whatsapp"></i>' : null;
	}



	public function selecionar($id=null) {
		$query = "SELECT ID, DDD, TEL, PRINCIPAL, FK_USER, ATIVO, DATA_IN, WHATSAPP FROM [PA_TELEFONE] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, DDD, TEL, PRINCIPAL, FK_USER, ATIVO, DATA_IN, WHATSAPP FROM [PA_TELEFONE] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = $this->createObjects($row);
		}
	
		return $objs;
	
	}

}