<?php 
require_once('lib/appConexao.php');

class sgnContatoModel extends appConexao {


	private $Id;
	private $Nome;
	private $FkUser;
	private $Ativo;
	private $DataIn;
	private $DataNasc;
	private $FkContatoTipo;
	private $FkEmpresa;
	private $Decisor;

	private $Empresa;
	private $ContatoTipo;

	public function __construct($Id=null, $Nome=null, $FkUser=null, $Ativo=null, $DataIn=null, $DataNasc=null, $FkContatoTipo=null, $FkEmpresa=null, $Decisor=null) {
		$this->setId($Id);
		$this->setNome($Nome);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);
		$this->setDataNasc($DataNasc);
		$this->setFkContatoTipo($FkContatoTipo);
		$this->setFkEmpresa($FkEmpresa);
		$this->setDecisor($Decisor);

	}
	
	private function setMethods($row) {
		if(!isset($row)) {
			return false;
		}


		$this->setId($row["ID"]);
		$this->setNome($row["NOME"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
		$this->setDataNasc($row["DATA_NASC"]);
		$this->setFkContatoTipo($row["FK_CONTATO_TIPO"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setDecisor($row["DECISOR"]);

		$this->Empresa->selecionar($row["FK_EMPRESA"]);
		//$this->ContatoTipo->selecionar($row["FK_CONTATO_TIPO"]);
}

	private function criarObjeto($dado) {
		$obj = new sgnContatoModel();
		$obj->setMethods($dado);
		return $obj;
	}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setNome($value=null) {
		$this->Nome = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 

	public function setDataNasc($value=null) {
		$this->DataNasc = $value;
	} 

	public function setFkContatoTipo($value=null) {
		$this->FkContatoTipo = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setDecisor($value=null) {
		$this->Decisor = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getNome() {
		return $this->Nome;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 

	public function getDataNasc() {
		return $this->DataNasc;
	} 

	public function getFkContatoTipo() {
		return $this->FkContatoTipo;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getDecisor() {
		return $this->Decisor;
	} 

	public function Empresa() {
		return $this->Empresa;
	}

	public function ContatoTipo() {
		return $this->ContatoTipo;
	}

	public function DI(sgnContatoTipoModel $ContatoTipo, sgnEmpresaModel $Empresa) {
		$this->Empresa = $Empresa;
		$this->ContatoTipo = $ContatoTipo;
	}


	public function selecionar($id=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT top 10 ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA, DECISOR FROM [PA_CONTATO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			//$objs[] = new sgnContatoModel($row["ID"], $row["NOME"], $row["FK_USER"], $row["ATIVO"], $row["DATA_IN"], $row["DATA_NASC"], $row["FK_CONTATO_TIPO"], $row["FK_EMPRESA"], $row["DECISOR"]);
			$objs[] = $this->criarObjeto($row);
		}
	
		return $objs;
	
	}

	public function listarByUser($value=null) {
		$query = "SELECT ID, NOME, FK_USER FROM [PA_CONTATO] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnContatoModel($row["ID"], $row["NOME"], $row["FK_USER"]);
		}
	
		return $objs;
	
	}
	public function selecionarByUser($value=null) {
		$query = "SELECT ID, NOME, FK_USER FROM [PA_CONTATO] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["NOME"], $rs[1]["FK_USER"]);
	}

	public function listarByContato_tipo($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO FROM [PA_CONTATO] WHERE FK_CONTATO_TIPO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnContatoModel($row["ID"], $row["NOME"], $row["FK_USER"], $row["ATIVO"], $row["DATA_IN"], $row["DATA_NASC"], $row["FK_CONTATO_TIPO"]);
		}
	
		return $objs;
	
	}
	public function selecionarByContato_tipo($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO FROM [PA_CONTATO] WHERE FK_CONTATO_TIPO =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["NOME"], $rs[1]["FK_USER"], $rs[1]["ATIVO"], $rs[1]["DATA_IN"], $rs[1]["DATA_NASC"], $rs[1]["FK_CONTATO_TIPO"]);
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA FROM [PA_CONTATO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnContatoModel($row["ID"], $row["NOME"], $row["FK_USER"], $row["ATIVO"], $row["DATA_IN"], $row["DATA_NASC"], $row["FK_CONTATO_TIPO"], $row["FK_EMPRESA"]);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, NOME, FK_USER, ATIVO, DATA_IN, DATA_NASC, FK_CONTATO_TIPO, FK_EMPRESA FROM [PA_CONTATO] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["NOME"], $rs[1]["FK_USER"], $rs[1]["ATIVO"], $rs[1]["DATA_IN"], $rs[1]["DATA_NASC"], $rs[1]["FK_CONTATO_TIPO"], $rs[1]["FK_EMPRESA"]);
	}

}