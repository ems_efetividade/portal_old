<?php 
require_once('lib/appConexao.php');

class sgnContatoTipoModel extends appConexao {


	private $Id;
	private $Descricao;
	private $Ativo;
	private $FkUser;
	private $DataIn;

	public function __construct($Id=null, $Descricao=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setDescricao($Descricao);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}
	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setDescricao($row["DESCRICAO"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setDescricao($value=null) {
		$this->Descricao = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getDescricao() {
		return $this->Descricao;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_TIPO] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
		return $this;
	}

	public function listar() {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER, DATA_IN FROM [PA_CONTATO_TIPO] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnContatoTipoModel($row["ID"], $row["DESCRICAO"], $row["ATIVO"], $row["FK_USER"], $row["DATA_IN"]);
		}
	
		return $objs;
	
	}

	public function listarByUser($value=null) {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER FROM [PA_CONTATO_TIPO] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnContatoTipoModel($row["ID"], $row["DESCRICAO"], $row["ATIVO"], $row["FK_USER"]);
		}
	
		return $objs;
	
	}
	public function selecionarByUser($value=null) {
		$query = "SELECT ID, DESCRICAO, ATIVO, FK_USER FROM [PA_CONTATO_TIPO] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["DESCRICAO"], $rs[1]["ATIVO"], $rs[1]["FK_USER"]);
	}

}