<?php 
require_once('lib/appConexao.php');

class sgnEmpresaReguaModel extends appConexao {


	private $Id;
	private $FkRegua;
	private $FkEmpresa;
	private $FkUser;
	private $Ativo;
	private $DataIn;

	public function __construct($Id=null, $FkRegua=null, $FkEmpresa=null, $FkUser=null, $Ativo=null, $DataIn=null) {
		$this->setId($Id);
		$this->setFkRegua($FkRegua);
		$this->setFkEmpresa($FkEmpresa);
		$this->setFkUser($FkUser);
		$this->setAtivo($Ativo);
		$this->setDataIn($DataIn);

	}
	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setFkRegua($row["FK_REGUA"]);
		$this->setFkEmpresa($row["FK_EMPRESA"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setDataIn($row["DATA_IN"]);
}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setFkRegua($value=null) {
		$this->FkRegua = $value;
	} 

	public function setFkEmpresa($value=null) {
		$this->FkEmpresa = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getFkRegua() {
		return $this->FkRegua;
	} 

	public function getFkEmpresa() {
		return $this->FkEmpresa;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function selecionar($id=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
	}

	public function listar() {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER, ATIVO, DATA_IN FROM [PA_EMPRESA_REGUA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnEmpresaReguaModel($row["ID"], $row["FK_REGUA"], $row["FK_EMPRESA"], $row["FK_USER"], $row["ATIVO"], $row["DATA_IN"]);
		}
	
		return $objs;
	
	}

	public function listarByRegua($value=null) {
		$query = "SELECT ID, FK_REGUA FROM [PA_EMPRESA_REGUA] WHERE FK_REGUA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnEmpresaReguaModel($row["ID"], $row["FK_REGUA"]);
		}
	
		return $objs;
	
	}
	public function selecionarByRegua($value=null) {
		$query = "SELECT ID, FK_REGUA FROM [PA_EMPRESA_REGUA] WHERE FK_REGUA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["FK_REGUA"]);
	}

	public function listarByEmpresa($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA FROM [PA_EMPRESA_REGUA] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnEmpresaReguaModel($row["ID"], $row["FK_REGUA"], $row["FK_EMPRESA"]);
		}
	
		return $objs;
	
	}
	public function selecionarByEmpresa($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA FROM [PA_EMPRESA_REGUA] WHERE FK_EMPRESA =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["FK_REGUA"], $rs[1]["FK_EMPRESA"]);
	}

	public function listarByUser($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER FROM [PA_EMPRESA_REGUA] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnEmpresaReguaModel($row["ID"], $row["FK_REGUA"], $row["FK_EMPRESA"], $row["FK_USER"]);
		}
	
		return $objs;
	
	}
	public function selecionarByUser($value=null) {
		$query = "SELECT ID, FK_REGUA, FK_EMPRESA, FK_USER FROM [PA_EMPRESA_REGUA] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["FK_REGUA"], $rs[1]["FK_EMPRESA"], $rs[1]["FK_USER"]);
	}

}