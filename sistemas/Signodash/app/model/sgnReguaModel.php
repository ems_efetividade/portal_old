<?php 
require_once('lib/appConexao.php');

class sgnReguaModel extends appConexao {


	private $Id;
	private $Relacionamento;
	private $Potencial;
	private $Ativo;
	private $FkUser;
	private $DataIn;

	public function __construct($Id=null, $Relacionamento=null, $Potencial=null, $Ativo=null, $FkUser=null, $DataIn=null) {
		$this->setId($Id);
		$this->setRelacionamento($Relacionamento);
		$this->setPotencial($Potencial);
		$this->setAtivo($Ativo);
		$this->setFkUser($FkUser);
		$this->setDataIn($DataIn);

	}
	private function setMethods($row) {
		$this->setId($row["ID"]);
		$this->setRelacionamento($row["RELACIONAMENTO"]);
		$this->setPotencial($row["POTENCIAL"]);
		$this->setAtivo($row["ATIVO"]);
		$this->setFkUser($row["FK_USER"]);
		$this->setDataIn($row["DATA_IN"]);
}
	
	public function setId($value=null) {
		$this->Id = $value;
	} 

	public function setRelacionamento($value=null) {
		$this->Relacionamento = $value;
	} 

	public function setPotencial($value=null) {
		$this->Potencial = $value;
	} 

	public function setAtivo($value=null) {
		$this->Ativo = $value;
	} 

	public function setFkUser($value=null) {
		$this->FkUser = $value;
	} 

	public function setDataIn($value=null) {
		$this->DataIn = $value;
	} 


	public function getId() {
		return $this->Id;
	} 

	public function getRelacionamento() {
		return $this->Relacionamento;
	} 

	public function getPotencial() {
		return $this->Potencial;
	} 

	public function getAtivo() {
		return $this->Ativo;
	} 

	public function getFkUser() {
		return $this->FkUser;
	} 

	public function getDataIn() {
		return $this->DataIn;
	} 


	public function selecionar($id=null) {
		if($id=null) {
			return false;
		}
		
		$query = "SELECT ID, RELACIONAMENTO, POTENCIAL, ATIVO, FK_USER, DATA_IN FROM [PA_REGUA] WHERE ID = " . $id . " AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		$this->setMethods($rs[1]);
	}

	public function listar() {
		$query = "SELECT ID, RELACIONAMENTO, POTENCIAL, ATIVO, FK_USER, DATA_IN FROM [PA_REGUA] WHERE ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnReguaModel($row["ID"], $row["RELACIONAMENTO"], $row["POTENCIAL"], $row["ATIVO"], $row["FK_USER"], $row["DATA_IN"]);
		}
	
		return $objs;
	
	}

	public function listarByUser($value=null) {
		$query = "SELECT ID, RELACIONAMENTO, POTENCIAL, ATIVO, FK_USER FROM [PA_REGUA] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);

		foreach($rs as $row) {
			$objs[] = new sgnReguaModel($row["ID"], $row["RELACIONAMENTO"], $row["POTENCIAL"], $row["ATIVO"], $row["FK_USER"]);
		}
	
		return $objs;
	
	}
	public function selecionarByUser($value=null) {
		$query = "SELECT ID, RELACIONAMENTO, POTENCIAL, ATIVO, FK_USER FROM [PA_REGUA] WHERE FK_USER =  ".$value."   AND ATIVO = 1";
		$rs = $this->executarQueryArray($query);
		return $this->__construct($rs[1]["ID"], $rs[1]["RELACIONAMENTO"], $rs[1]["POTENCIAL"], $rs[1]["ATIVO"], $rs[1]["FK_USER"]);
	}

}