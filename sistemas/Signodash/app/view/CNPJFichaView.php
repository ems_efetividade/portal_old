<style>

.input-search-custom {
    width:100%;
    border:0px;
    border-bottom: 1px solid #ddd;
    padding:10px 0px 10px 0px;
    margin:0px;
    font-weighst: normal;
    background-color: transparent;
}
</style>

<?php if (isset($cnpj)) { ?>

<!-- DADOS DO CNPJ   -->
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="panel panel-default m-3">
            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">

                        <div class="row">
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">

                                <h3 class="m-3 mb-5" style="color:#000">
                                    <strong><?php echo $cnpj->getFantasia() ?></strong>
                                </h3>

                                <h4 class="m-3 mb-9">
                                    <small>
                                        <b><?php echo $cnpj->getRazaoSocial() ?></b>
                                    </small>
                                </h4>

                                <h5 class="m-3 mb-5">
                                    <small>
                                        <?php echo $cnpj->getLogradouro().' '.$cnpj->getNumero() ?> - <?php echo $cnpj->getComplemento() ?> <?php echo$cnpj->getBairro() ?> -  <?php echo $cnpj->getCidade().'/'.$cnpj->getEstado() ?> - <?php echo$cnpj->getCep() ?>
                                    </small>
                                </h5>

                                <h5 class="m-3 mt-11">
                                    <small>
                                        <b>Responsável: </b><?php echo $cnpj->Responsavel()->Colaborador()->getNome(); ?>
                                    </small>
                                </h5>


                            </div>
                            
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 text-left">

                                <?php if(isset($empresaTelefones)) { ?>

                                    <?php foreach($empresaTelefones as $empresaTelefone) { ?>

                                        <h5 class="m-6"><small>
                                        <b>Telefone:</b>
                                            <a href="whatsapp://send?text=Hello World!&phone=+<?php echo $empresaTelefone->Telefone()->getTel() ?>">
                                                <?php echo appFunction::formatarTelefone($empresaTelefone->Telefone()->getTel()) ?>
                                            </a> <?php echo $empresaTelefone->Telefone()->getWhatsappIcon() ?>
                                    </small></h5>

                                    <?php } ?>

                                <?php } ?>

                                <?php if(isset($empresaEmail)) { ?>
                                    
                                    <h5 class="m-6" style="font-weight: normal"><small>
                                        <b>Email:</b> <?php echo $empresaEmail->Email()->getEmail() ?>
                                        </small></h5>

                                        <h5 class="m-6" style="font-weight: normal"><small>
                                        <b>Cadastro:</b> <?php echo appFunction::formatarData($cnpj->getDataIn()) ?>
                                </small></h5>

                                    <h5 class="m-6" style="font-weight: normal"><small>
                                        <b>CNPJ:</b>  <?php echo appFunction::formatarCnpj($cnpj->getCnpj()) ?> 
                                </small></h5>

                                <?php } ?>

                               
                            
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- FIM DADOS DO CNPJ -->

<?php } ?>

<?php if(isset($reguaEmpresa)) { ?>

<div class="row">

    <div class="col-lg-12">

    </div>

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="row">
            
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
                <?php appComp::get('box', [
                    'titulo' => 'Ranking', 
                    'subtitulo' => 'Relacionamento', 
                    'valor' => $reguaEmpresa->Regua()->getPotencial().$reguaEmpresa->Regua()->getRelacionamento(),
                    'descricao' => '&nbsp;'
                ]) ?>

            </div>
            

            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
                <?php appComp::get('box', [
                    'titulo' => 'Nº Pacientes', 
                    'subtitulo' => 'Mês', 
                    'valor' => appFunction::formatarMoeda($cnpj->getNumeroPacientes(),0), 
                    'descricao' => '('.(appFunction::formatarMoeda($cnpj->getPacientesDia(),0)).' dia).'
                ]) ?>
            </div>

            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
                
                <div class="panel panel-default m-3">
                    <div class="panel-body">
                        <p class="text-center m-0 p-0"><small><b>Consultas</b></small></p>
                        
                        <div class="row">


                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                            
                                <?php if($empresaConsulta->getPercentual() != "") { ?>

                                <div class="row mb-4">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 p-0 text-center">
                                        <p style="font-size:28px" class="p-0 m-0 "><b><?php echo $empresaConsulta->getPercentual() ?>%</b></p>
                                        <div class="text-center"><small>Particular </small></div>
                                        <div><small><small><b>(R$ <?php echo appFunction::formatarMoeda($empresaConsulta->getValor(),2) ?>)</b></small></small></div>
                                    </div>

                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 p-0 text-center">
                                        <p style="font-size:28px" class="p-0 m-0 "><b><?php echo (100 - $empresaConsulta->getPercentual()) ?>%</b></p>
                                        <div class="text-center"><small>Convênios</small></div>
                                    </div>
                                </div>

                                <?php } else { ?>
                                    <?php appComp::get('empty', ['h_type' => '6', 'tam_fonte_icone' => '18']) ?>
                                <?php } ?>


                            </div>

                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                                <div class="dropdown">
                
                                    <small class="m-0" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="cursor:pointer">
                                        <small>Convênios Atendidos <span class="caret p-0 m-0 mr-2 text-primary"></span></small>
                                    </small>

                                    <div class="dropdown-menu p-10" aria-labelledby="dropdownMenu1">
                                        <?php if(isset($empresaConvenios)) { ?>
                                        <div class="table-responsive">
                                            <table class="table table-condensed table-striped" style="font-size:11px">
                                                <tbody>
                                                    <?php foreach($empresaConvenios as $convenio) { ?>
                                                    <tr>
                                                        <td>
                                                            <?php echo trim($convenio->Convenio()->getNome())  ?>
                                                        </td>
                                                    </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php } else { ?>
                                            <?php appComp::get('empty') ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>


                            
                            
                            
                        
                        </div>
                        

                    </div>
                </div>
                
            </div>

            <?php if(isset($dadosPX)) { ?>
            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
               
                    <?php appComp::get('box', [
                        'titulo' => 'Repres. Meso', 
                        'subtitulo' => $periodo->selecionarPeriodoAtual()->getPeriodo(), 
                        'valor' => appFunction::formatarMoeda($dadosPX->getM00()*100, 2, '%'), 
                        'descricao' => '<span style="border-radius:4px !important;" class="label label-'.((($dadosPX->getM00() - $dadosPX->getM01()) *100 <= 0) ? 'danger' : 'success').'">'.
                            (appFunction::formatarMoeda( ($dadosPX->getM00() - $dadosPX->getM01()) *100 , 1, '%')).
                            '</span> mês ant.'
                    ]) ?>
               
            </div>


            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
                <?php appComp::get('box', [
                    'titulo' => 'Market Share', 
                    'subtitulo' =>  $periodo->selecionarPeriodoAtual()->getPeriodo(), 
                    'valor' => appFunction::formatarMoeda($dadosShare->getM00()*100, 1, '%'), 
                    'descricao' => '<span style="border-radius:4px !important;" class="label label-'.((($dadosShare->getM00() - $dadosShare->getM01()) *100 <= 0) ? 'danger' : 'success').'">'.
                        (appFunction::formatarMoeda( ($dadosShare->getM00() - $dadosShare->getM01()) *100 , 1, '%')).
                        '</span> mês ant.'
                ]) ?>
            </div>
            

            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-4 p-0">
                    
                <?php if(isset($empresaObjetivo)) { 
                    appComp::get('box', [
                    'titulo' => 'Objetivo', 
                    'subtitulo' => 'Mkt. Share',
                    'valor' => appFunction::formatarMoeda($empresaObjetivo->getObjetivo(), 1, '%'),
                    'descricao' => 'Prazo: ' . appFunction::formatarData($empresaObjetivo->getPrazo()), 
                ]); 
                
                    }?>
            </div>
<?php } ?>
            
        
        </div>

    </div>
    

    
</div>

<?php } ?>


<!-- PLATAFORMAS -->
<div class="row">

    <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12 p-0">
        <div class="panel panel-default m-3">
            <div class="panel-body" id="panel-plataforma">

                <h5 class="p-0 m-0"><b><i class="fas fa-stethoscope"></i> Plataformas</b> <small>(<?php echo count($plataformas) ?>)</small></h5>
               <br>
                <?php if(isset($plataformas)) { ?>

                <div style="max-height:305px;" class="mCustomScrollbar" >
                    <div class="table-responsive">

                        <table class="table table-hover table-striped table-consdensed" style="font-size:11px">
                            <tbody>
                                <tr>
                                    <td class="text-left"><input type="checkbox" checked="checked" onclick="checkAll(this)"  class="p-0 m-0"></td>
                                    <td class="text-left"><b>(MARCAR/DESMARCAR TUDO)</b></td>
                                </tr>
                            <?php foreach($plataformas as $plat) { ?>
                                <?php if($plat->Plataforma()->getId() != "") { ?>
                                <tr>
                                    <td class="text-left"><input type="checkbox" checked="checked" class="check-plataforma p-0 m-0" value="<?php echo $plat->Plataforma()->getId() ?>"></td>
                                    <td class="text-left"><?php echo $plat->Plataforma()->getNome() ?></td>
                                </tr>
                                <?php } ?>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                </div>

                <button class="btn btn-primary btn-sm btn-block mdb-10 mt-20" type="button" onClick="atualizarGrafico('<?php echo $cnpj->getId() ?>')">Filtrar</button>

               
                
                <?php } else { ?>
                    <?php appComp::get('empty') ?>
                <?php } ?>

            </div>
        </div>
    </div>

    


    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-9">
        <div class="row">
            <div class="panel panel-default m-3">
                <div class="panel-body" id="panel-graficos" stysle="background-color:#fff">
                    <div class="row">

            
                        <div class="col-lg-4 col-md-4 col-sm-4 container-grafs">
                            <h5 class="p-0 m-0"><b><i class="fas fa-chart-line"></i> Represent. PX / Meso</b></h5>
                            <?php if($part) { ?>
                            <h5 class="mb-10"><small><i class="fas fa-map-marker"></i> <?php echo $mesoregiao->getMesoregiao() ?></small></h5>

                            
                            <?php appComp::get('piechart', ['name' =>'graf-rep', 'option' => $part, 'height' => '270px']) ?>
                            <?php } else { ?>
                                <?php appComp::get('empty') ?>
                            <?php } ?>
                        </div>

                        <div class="col-lg-8 col-md-8 col-sm-8 container-grafs">
                            <h5 class="p-0 m-0"><b><i class="fas fa-chart-line"></i> Market Share em Receituário</b></h5>
                            <?php if($mktShare) { ?>
                            <h5 class="mb-10"><small>Fonte: Audit Pharma  <?php echo $periodo->getPeriodo(); ?></small></h5>

                            <?php appComp::get('linechart', ['name' =>'graf-share','option' => $mktShare, 'height' => '270px']) ?>
                            <?php } else { ?>
                                <?php appComp::get('empty') ?>
                            <?php } ?>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <hr>
                            <span><b>ESPECIALIDADES: </b></span> 
                                <?php if(isset($empresaEspecialidades)) { ?>
                            
                                    <?php foreach($empresaEspecialidades as $especialidade) { ?>
                                        <div class="label label-success" style="border-radius:4px !important; font-weight: normal;"><?php echo $especialidade->Especialidade()->getNome()  ?></div>
                                    <?php } ?>

                                </div>
                            <?php } else { ?>
                                <?php appComp::get('empty') ?>
                            <?php } ?>
                        </div>

                    </div>
            
                </div>
            </div>
        </div>
    </div>    
</div>


<!-- CONTATOS -->
<div class="row">

    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 p-0">
        <div class="panel panel-default m-3">

            <div class="panel-body">
                <h5 class="p-0 m-0 mb-10"><b><i class="fas fa-user-md"></i> Contatos do CPNJ</b> <small>(<?php echo count($contatos) ?>)</small></h5>

                <?php if(isset($contatos)) { ?>
                
                    <div class="table-responsive mCustomScrollbar"  style="max-height:400px;">
                    <table class="table table-hover table-condensed table-striped" style="font-size:13px" id="tabela-contatoo">
                        <thead>
             
                            <tr class="active">
                          
                                <th class="p-0 m-0">
                                    <input type="text" name="" class="active input-search-custom" onkeyup="buscaGridCnpj(0, this)" id="txt-busca-contato" ssyle="width:100%" placeholder="Contato" />
                                </th>
                                <th class="p-0 m-0">
                                    <!-- <input type="text" name="" class="input-search-custom" onkeyup="buscaGridCnpj(1, this)" id="txt-busca-tipo" ssyle="width:100%" placeholder="Tipo" /> -->
                                    <select class="input-search-custom" onchange="buscaGridCnpj(1, this)">
                                        <option value="">Tipo</option>
                                        <?php foreach($tipoContatos as $tipoContato) { ?>
                                            <option value="<?php echo $tipoContato->getDescricao() ?>"><?php echo $tipoContato->getDescricao() ?></option> 
                                        <?php } ?>
                                    </select>
                                </th>
                                <th class="p-0 m-0">
                                    <input type="text" name="" class="input-search-custom" onkeyup="buscaGridCnpj(2, this)" id="txt-busca-decisor" ssyle="width:100%" placeholder="Decisor" />
                                </th>
                                <th class="p-0 m-0" colspan="2">
                                    <input type="text" name="" class="input-search-custom" onkeyup="buscaGridCnpj(3, this)" id="txt-busca-telefone" ssyle="width:100%" placeholder="Telefone" />
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($contatos as $contato) { ?>
                            <tr>
                               
                                <td>
                                    <p><strong><?php echo $contato->Contato()->getNome()  ?></strong>
                                        <?php echo ($contato->Contato()->isBirthday() === true) ? '<span class="text-primary"><i class="fas fa-birthday-cake"></i></span>' : '' ; ?>
                                    </p>
                                    <p><small><?php echo $contato->Crm()->getUF().$contato->Crm()->getNumero() ?></small></p>
                                </td>

                                <td>
                                    <?php echo $contato->Contato()->ContatoTipo()->getDescricao() ?>
                                </td>

                                <td>
                                    <?php echo ($contato->Contato()->getDecisor()==1) ? '<span class="label label-primary" style="border-radius:4px !important;">DECISOR</span>' : null; ?> 
                                </td>


                                <td>
                                <?php echo $contato->Contato()->ContatoTelefone()->Telefone()->getWhatsappIcon() ?> <?php echo appFunction::formatarTelefone($contato->Contato()->ContatoTelefone()->Telefone()->getTel()) ?>
                                </td>

                                <td>
                                    <?php if($contato->Crm()->inPxShare() !== false) { ?> 
                                        <img width="16" src="/../../public/img/iconsFerramentas/analytics-8.png" style="cursor:pointer" onclick="fichaMedico('<?php echo $contato->Crm()->inPxShare() ?>', '<?php echo $contato->Contato()->getNome()  ?>', '', 'TRM01|TRM00')">
<!--                                         
                                        <button type="button" class="btn btn-success btn-sm" onclick="fichaMedico('<?php echo $contato->Crm()->inPxShare() ?>', '<?php echo $contato->Contato()->getNome()  ?>', '', 'TRM01|TRM00')">
                                            <img width="16" src="/../../public/img/iconsFerramentas/analytics-8.png">
                                        </button> -->
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>

                <?php } else { ?>
                    <?php appComp::get('empty') ?>
                <?php } ?>

            </div>
        </div>
    </div>
</div>



<!--MODAL PESQUISAR-->
<div class="modal" id="modalFichaMedico" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-backdrop="false">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Ficha do Médico - Prescription Share OnLine</h5>
      </div>
      
      <div class="modal-body max-height">
 
 		asdasd
 
      </div>

      <div class="modal-footer">
           <a href="#" class="btn btn-primary btn-sm" data-crm="" data-periodo=""  id="btn-exportar-ficha-medico" data-dismisss="modal">
                <span class="glyphicon glyphicon-file"></span> Exportar em CSV
            </a>   	
      
            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                <span class="glyphicon glyphicon-remove"></span> Fechar
            </button>
      </div>
    </div>
  </div>
</div>


<script>
    (function($){
        $(document).ready(function(e) {
            $(".mCustomScrollbar").mCustomScrollbar({
                theme:"dark"
            });
        });
    })(jQuery);




function checkAll(obj) {
    var checkbox = document.getElementsByClassName('check-plataforma');
    for (var i = 0; i < checkbox.length; ++i)
        checkbox[i].checked = obj.checked;
}

function buscaGridCnpj(col, obj) {

    filtro = document.getElementById(obj.id)
    tabela = document.getElementById('tabela-contatoo')
    nomeFiltro = obj.value.toUpperCase() /*filtro.value.toUpperCase();*/

    for (var i = 1; i < tabela.rows.length; i++) {
        var conteudoCelula = tabela.rows[i].cells[col].innerText.toUpperCase()
        var corresponde = conteudoCelula.indexOf(nomeFiltro) >= 0
        tabela.rows[i].style.display = corresponde ? '' : 'none'
    }
    //document.getElementById('cidade').value = '';
    //document.getElementById('empresa').value = '';
    //document.getElementById('ugn').value = '';
}




function fichaMedico(crm, nome, esp, periodo) {



    $.ajax({
        type: "POST",
        url: 'https://'+ window.location.host +'/sistemas/NewPrescriptionShareOnline/home/fichaMedicoForever',
        data: {
            crm: crm,
            nome: nome,
            esp: esp,
            periodo: periodo
        },
        
        beforeSend: function() {
            $(".loader-wrapper").css('top', window.pageYOffset);
            $(".loader-wrapper").show();
        },
        
        success: function(retorno) {
           // console.log(retorno);
            
            $('#modalFichaMedico .modal-body').html(retorno);
            $('#modalFichaMedico').modal('show');

            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-crm', crm);
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-periodo', periodo);

            $(".loader-wrapper").fadeOut("slow");

            $('#btn-exportar-ficha-medico').unbind().click(function(s){
                s.preventDefault();
                window.location.href = "https://"+ window.location.host +"/sistemas/NewPrescriptionShareOnline/home/exportarFichaMedico/"+crm+"/"+periodo+"";
            });

            $('#modalFichaMedico').modal('show');

           
            
        }
    });  
}


</script>