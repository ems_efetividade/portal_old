<?php foreach($view['emps'] as $i => $cnpj) {
    $filtro = $cnpj->getCnpj().$cnpj->getFantasia().$cnpj->getRazaoSocial().$cnpj->getEstado().$cnpj->getCidade().$cnpj->Responsavel()->Colaborador()->getNome();
    $responsavel = $cnpj->Responsavel()->Colaborador()->getSetor() . ' '. $cnpj->Responsavel()->Colaborador()->getNome();
    $id = $cnpj->getCnpj();
    $fantasia = strtoupper($cnpj->getFantasia());
    $razao = strtoupper($cnpj->getRazaoSocial());
    $cidade = strtoupper($cnpj->getEstado() . '/' . $cnpj->getCidade());
?>

<tr class="hhidden-sm hhidden-md hhidden-xs">
    <td class="filter p-10 m-0 pl-0 pr-0 td-normal <?php echo ($i%2) ? 'td-bg' : '' ;?>"   data-name="<?php echo $filtro ?>">

        <a  class="p-0 m-0"  onClick="ficha_cnpj('<?php echo $id ?>')"> 
            <p style="font-size:12px"><b><?php echo $fantasia  ?></b></p>
            <div class="subtext">
                <p ><?php echo $razao   ?></p>
                <p class="mb-5"><i class="fas fa-map-marker"></i>  <?php echo $cidade  ?></p>
                <p><i class="fas fa-user-tie"></i>  <?php echo $responsavel  ?></p>
            </div>
        </a>
        
    </td>
</tr>
<?php } ?>

