<?php require_once('app/view/headerView.php'); ?>

<script src="<?php echo APP_CAMINHO ?>plugins/echarts.min.js"></script>
<style>

table p {
	padding:0;
	margin:0px;	
}
.listarMedicos {
	cursor:pointer;
}	
.alert {
	padding:6px;
	margin:1px;	
}
.alert-default {
	color: #777777;
    background-image: -webkit-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: -o-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: linear-gradient(to bottom, #eeeeee 0, #dddddd 100%);
    background-repeat: repeat-x;
    border: 1px solid #ddd;	
}
.alert-none {	   
    border: 1px solid #eee;	
}

.alert-gray {
	background-color:#f1f1f1;	   
    border: 1px solid #e5e5e5;	
}

.alert-soft {
	background-color:#f9f9f9;	   
    border: 1px solid #e5e5e5;	
}
.alert-primary {
	color: #ffffff;
    background-image: -webkit-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: -o-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: linear-gradient(to bottom, #4c70ba 0, #3b5998 100%);
    background-repeat: repeat-x;
    border: 1px solid #3b5998;	
}

.up, .down, .middle {
	padding-left:16px;	
}
.up, .up-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/up.png) no-repeat left center;	
	color:#3c763d;
}

.down, .down-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/down.png) no-repeat left center;	
	color:#a94442;
}

.middle, .middle-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/middle.png) no-repeat left center;	
	color:#f0ad4e;
}

.up-min, .down-min {
	padding-left:13px;
	background-size: 13px 13px;
}

.plus {
	background:url(<?php echo APP_CAMINHO; ?>public/img/plus.png) no-repeat left left;	
}
.bold {
	font-weight:bold;	
}

.loading {
	display:none;	
}

.abrirSetor, .listarMedico {
	cursor:pointer;	
}

html {
  overflow-y: scroll;
}

body.modal-open,
.modal-open {
    margin-right: 0px;
    margin-right: 0; /* // fix */
}

/*
//.modal-open .navbar-fixed-top,
//.modal-open .navbar-fixed-bottom {
//padding-right: 17px;
//}
*/
.loading-gif {
   position:absolute;
   top:55%;
   left:50%;
   margin-top:-25px;
   margin-left:-25px;
}

.table tbody>tr>td{
    vertical-align: middle;
}


.bcg .panel {
	border:0px;
	-webkit-box-shadow: 0 0px 0px rgba(0,0,0,0);
}


.custom-border {
    border-left:2px solid #ddd !important;
}


.content {
  display: flex;
  justify-content: center;
  align-items: center;
  width:100%;
  height:100%;
  opacity: 0.8;
}
.loader-text {
    width: 100%;
    height: 100%;
    position: absolute;
    top: 0;
    left: 0;
    color:white;
    display:flex;
  justify-content: center;
  align-items: center;
  padding-top:3%;
}
.loader-wrapper {
  width: 100%;
  height: 100%;
  position: absolute;
  top: 0;
  left: 0;
  background-color: #242f3f;
  display:flex;
  justify-content: center;
  align-items: center;
  
}
.loader {
  display: inline-block;
  width: 30px;
  height: 30px;
  position: relative;
  border: 4px solid #Fff;
  animation: loader 2s infinite ease;
  
}
.loader-inner {
  vertical-align: top;
  display: inline-block;
  width: 100%;
  background-color: #fff;
  animation: loader-inner 2s infinite ease-in;
}
@keyframes loader {
  0% { transform: rotate(0deg);}
  25% { transform: rotate(180deg);}
  50% { transform: rotate(180deg);}
  75% { transform: rotate(360deg);}
  100% { transform: rotate(360deg);}
}
@keyframes loader-inner {
  0% { height: 0%;}
  25% { height: 0%;}
  50% { height: 100%;}
  75% { height: 100%;}
  100% { height: 0%;}
}

</style>

<?php //var_dump($dados['part']); ?>

<div style="padding-left:15px;padding-right:15px">

<div class="row">

    
    <?php require_once('menuCNPJ.php') ?>

    <div class="col-lg-10 col-md-12 panel-right p-0">

        <?php require_once('menuSuperior.php') ?>

        <div class="pt-8 p-20 mt-0" id="div-center">
            <?php //require_once('CNPJFichaView.php') ?>
        </div>

    </div>

    </div>
</div>

<div class="content">
    <div class="loader-wrapper">
        <span class="loader"><span class="loader-inner"></span></span>
        <div class="loader-text">
        </div>
     
    </div>
    
</div>

<script>
        $(window).on("load",function(){
            $.post('<?php echo APP_CAMINHO ?>home/iniciar/', function(response){
                id = parseInt(response);
                ficha_cnpj(id);
                //$(".loader-wrapper").fadeOut();
            });

        });
    </script>
<script>


function selectRow(obj) {
    //obj.style.background = "#000";
}

function ficha_cnpj(id) {



    $('#search-box').hide();

    $.ajax({
        type: "post",
        url: '<?php echo APP_CAMINHO ?>home/ficha_cnpj/',
        data: {
            id_cnpj: id
        },
        beforeSend:function() {
            $(".loader-wrapper").css('top', window.pageYOffset);
            $(".loader-wrapper").show();
        },
        success: function (response) {
           
            $('#div-center').html(response);
            resizeGraph();
            resizePanelLeft();
           
            $(".loader-wrapper").fadeOut("slow");
            
            var panel = document.getElementById('panel-graficos');
            var plat = document.getElementById('panel-plataforma');


            var tam = plat.offsetHeight + (panel.offsetHeight - plat.offsetHeight);

            plat.style.height = tam + 'px';
            
        }
    });
}


function resizeGraph() {
    graphs = document.getElementsByClassName('echart-comp');
   
    for(i=0; i< graphs.length;i++) {
        var chart = echarts.init(document.getElementById(graphs[i].id));
        x = chart.getOption();

        if(typeof x !== 'undefined') {
            chart.setOption(x, true);
            chart.resize(null, null, true);
        }
    }
}

function atualizarGrafico(id) {
    
    var checks = document.getElementsByClassName('check-plataforma');
    var plat = [];

    var fade = document.getElementsByClassName('container-grafs');

    for(i = 0;i < checks.length; i++) {
        ck = checks[i];
        if(ck.checked  === true) {
            plat.push(ck.value);
        }
    }




    if(plat.length > 0) {
        $.ajax({
            type: "post",
            url: '<?php echo APP_CAMINHO ?>home/atualizarGrafRepres/',
            dataType: 'json',
            data: {
                plataformas: plat,
                id_empresa: id
            },
            beforeSend:function() {
                for (var i = 0; i < fade.length; ++i)
                    fade[i].style.opacity = '0.2';
            },
            success: function (response) {
                
                chart = echarts.init(document.getElementById('graf-rep'));
                chart.setOption(response.repres, true);

                chart = echarts.init(document.getElementById('graf-share'));
                chart.setOption(response.mktshare, true);

                for (var i = 0; i < fade.length; ++i)
                    fade[i].style.opacity = '1';
            }
        });
    }
}

$(window).ready(function(e){
    resizePanelLeft();
});



</script>

<script>

$('#search-box').hide();
/*
$('.filtrar-cnpj').on("keyup", function() {

    

    if($(this).val() != '') {
        $('#search-box').show();
    } else {
        $('#search-box').hide();
    }
    
    console.log($(".filter").closest('tr').is(':visible').length);

    var value = $(this).val().toLowerCase();
    $(".filter").filter(function() {
        $(this).toggle($(this).data('name').toLowerCase().indexOf(value) > -1)
    });

});
*/
</script>


<script>



    

function filtrarTabela(val, table) {

    if(typeof val === 'undefined') {
        return false;
    }


    if(val != '') {
        $('#search-box').show();
    } else {
        $('#search-box').hide();
    }


    

    var filtro = val.toUpperCase();
  
        var tabela = document.getElementById(table);
        for (var i = 0; i < tabela.rows.length; i++) {
           
            
            var conteudo = tabela.rows[i].cells[0].dataset.name.toUpperCase();
            var corresponde = conteudo.indexOf(filtro) >= 0;
            tabela.rows[i].cells[0].style.display = corresponde ? 'block' : 'none';

            //console.log(conteudo +  ' - ' +  filtro);
        }

    

   
        

}


</script>


<script>
    (function($){
        $(document).ready(function(e) {
            $("#container-setores, .mCustomScrollbar").mCustomScrollbar({
                theme:"dark"
            });
        });
    })(jQuery);
</script>