<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <br>
            <h3><b>Tabela RNK_PRODUTO</b></h3>
            <hr>
            
            <p>
                <a href="<?php echo APP_CAMINHO ?>admin/rankProdutoForm" class="btn btn-success">
                    <span class="glyphicon glyphicon-plus"></span> 
                    Adicionar
                </a>
                <a href="<?php echo APP_CAMINHO ?>admin/rankProdutoImportarForm" class="btn btn-default pull-right">
                    <span class="glyphicon glyphicon-import"></span> 
                    Importar Arquivo .csv
                </a>
            </p>
            
            <table class="table table-striped table-condensed">
                <tr>
                    <th><small>Linha</small></th>
                    <th><small>Mercado</small></th>
                    <th><small>Produto</small></th>
                    <th><small>Ordem</small></th>
                    <th><small>ID Linha</small></th>
                    <th><small>Ação</small></th>
                </tr>
                    
                <?php foreach($listaProduto as $produto) { ?>
                <tr>
                    <td><small><?php echo $produto->getLinha() ?></small></td>
                    <td><small><?php echo $produto->getMercado() ?></small></td>
                    <td><small><?php echo $produto->getProduto() ?></small></td>
                    <td><small><?php echo $produto->getOrdem() ?></small></td>
                    <td><small><?php echo $produto->getIdLinha() ?></small></td>
                    <td>
                        <a href="<?php echo APP_CAMINHO ?>admin/rankProdutoForm/<?php echo base64_encode($produto->getId()) ?>">Editar</a>
                        <a href="<?php echo APP_CAMINHO ?>admin/rankProdutoExcluir/<?php echo base64_encode($produto->getId()) ?>">Excluir</a>
                        
                    </td>
                </tr>
                <?php } ?>
            </table>

        </div>
    </div>
</div>