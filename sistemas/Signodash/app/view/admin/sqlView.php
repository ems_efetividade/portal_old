<?php include('app/view/admin/adminHeader.php'); ?>

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2 panel-left">
            <?php require_once('app/view/admin/adminMenu.php'); ?>
        </div>
        <div class="col-lg-10">
            <BR>
            <form id="formExec" method="POST" action="<?php echo APP_CAMINHO ?>admin/sqlExecute">
            <textarea name="txtExecute" class="form-control" rows="13"></textarea>
            </form>
            
            <a href="#" id="btnExecute" class="btn btn-block btn-success">
                    <span class="glyphicon glyphicon-plus"></span> 
                    Adicionar
            </a>
            
        
            <div id="return"></div>
            
        </div>
    </div>
</div>

<script>

    $('#btnExecute').click(function(e) {
        $.ajax({
                type: "POST",
                url: $('#formExec').attr('action'),
                data: $('#formExec').serialize(),

                beforeSend: function() {
                        //modalAguarde('show');
                },

                success: function(retorno) {
                    $('#return').html(retorno);
                }
        }); 
    });

</script>