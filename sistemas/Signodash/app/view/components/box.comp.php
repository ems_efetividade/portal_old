<?php

$data = array(
    'titulo'    => (isset($titulo)    ? $titulo    : 'Titulo do Box'),
    'valor'     => (isset($valor)     ? $valor     : ''),
    'descricao' => (isset($descricao) ? $descricao : '')
);

?>


<div class="panel panel-default m-3" style="fonts-family:arial">
    
    <div class="panel-body">

        <div class="text-center">
            <p><?php echo $data['titulo'] ?></p>
            <h2><strong><?php echo $data['valor'] ?></strong></h2>  
            <p><small><?php echo $data['descricao'] ?></small></p>  
        </div>
        

    </div>

</div>