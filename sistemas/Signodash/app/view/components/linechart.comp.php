<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 

?>


<div class="panel panel-default m-3">
    <div class="panel-body">
        <div id="<?php echo $timestamp ?>" class="charts" style="width: 100%; height: 200px; margin: 0 auto"></div>
    </div>
</div>


<script>

var dom = document.getElementById("<?php echo $timestamp ?>");
var myChart = echarts.init(dom);
var app = {};
option = null;
app.title = '坐标轴刻度与标签对齐';

option = {
    title: {
        text: '折线图堆叠'
    },
    tooltip: {
        trigger: 'axis'
    },
    
    grid: {
        left: '3%',
        right: '4%',
        bottom: '3%',
        containLabel: true
    },
    toolbox: {
        feature: {
            saveAsImage: {}
        }
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: ["Abr\/18","Mai\/18","Jun\/18","Jul\/18","Ago\/18","Set\/18","Out\/18","Nov\/18","Dez\/18","Jan\/19","Fev\/19","Mar\/19"]
    },
    yAxis: {
        type: 'value'
    },
    series: [{"name":"Cob. Objetivo","yAxis":0,"data":[null,null,null,null,null,null,null,null,null,110.2570758,118.5880773,null]},{"name":"Market Share","yAxis":1,"data":[12.14500847,11.97340681,11.2650753,13.17482539,11.33145671,13.69150138,13.44435343,13.87086285,12.48303349,13.58885398,15.54065545,null]},{"name":"Prescription Share","yAxis":1,"data":[16.74780915,19.90116801,14.38496118,15.89719181,16.90570644,14.75602587,14.58248473,16.05754277,17.30848861,13.97468354,13.00383877,null]},{"name":"Produtividade","yAxis":0,"data":[null,null,null,null,null,null,null,null,null,91.39178941,91.39178941,97.61763321]}]
};

if (option && typeof option === "object") {
    myChart.setOption(option, true);
}
       

</script>
