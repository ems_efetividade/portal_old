<?php
$numbers = range(0, 99999);
shuffle($numbers);

$timestamp = ""; 
$timestamp = $numbers[0]; 

?>


<div class="panel panel-default m-3">
    <div class="panel-body">
        <div id="<?php echo $timestamp ?>" class="charts" style="width: 100%; height: 200px; margin: 0 auto"></div>
    </div>
</div>


<script>
option = {
    toolbox: {
        feature: {
            saveAsImage: {
                show: true,
                title: 'Exportar',
                name: 'PxShare'
            }
        }
    },
    tooltip : {
        trigger: 'item',
        textStyle: {
            fontSize:10
        },
        formatter : function(params) {

           param = params;
           return '<span style="display:inline-block;margin-right:5px;border-radius:10px;width:10px;height:10px;background-color:' + param.color + ';"></span> Share: ' + param.data.name;
       },
    },
    legend: {
        orient: 'vertical',
        x: 'left',
        top: '10%',
        textStyle: {
            fontSize:9
        },
        data:["BRASART SGP (19,4%)","DIOVAN NVR (29,8%)","VALSARTAN LNI LNI (28,3%)","BRAVAN ACH (9,5%)","AVAL SRX (7,5%)","OUTROS (5,5%)"]    },
    series : [
        {
            name: 'Mercado',
            type: 'pie',
            center: ['72%', '50%'],
            radius: ['0', '80%'],
            data: [{"name":"BRASART SGP (19,4%)","value":19.4,"sliced":"true","selected":"true"},{"name":"DIOVAN NVR (29,8%)","value":29.8,"sliced":false,"selected":false},{"name":"VALSARTAN LNI LNI (28,3%)","value":28.3,"sliced":false,"selected":false},{"name":"BRAVAN ACH (9,5%)","value":9.5,"sliced":false,"selected":false},{"name":"AVAL SRX (7,5%)","value":7.5,"sliced":false,"selected":false},{"name":"OUTROS (5,5%)","value":5.5,"sliced":false,"selected":false}],
            label: {
               show: true,
               normal: {
                    position: 'inner',
                    formatter: '{c}%',
                }
            },
            itemStyle: {
                emphasis: {
                    shadowBlur: 10,
                    shadowOffsetX: 0,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            }
        }
    ]
};

var pizza = echarts.init(document.getElementById('<?php echo $timestamp ?>'));
    pizza.setOption(option, true);
</script>
