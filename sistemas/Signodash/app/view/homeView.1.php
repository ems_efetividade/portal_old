<?php require_once('app/view/headerView.php'); ?>

<style>

table p {
	padding:0;
	margin:0px;	
}
.listarMedicos {
	cursor:pointer;
}	
.alert {
	padding:6px;
	margin:1px;	
}
.alert-default {
	color: #777777;
    background-image: -webkit-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: -o-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: linear-gradient(to bottom, #eeeeee 0, #dddddd 100%);
    background-repeat: repeat-x;
    border: 1px solid #ddd;	
}
.alert-none {	   
    border: 1px solid #eee;	
}

.alert-gray {
	background-color:#f1f1f1;	   
    border: 1px solid #e5e5e5;	
}

.alert-soft {
	background-color:#f9f9f9;	   
    border: 1px solid #e5e5e5;	
}
.alert-primary {
	color: #ffffff;
    background-image: -webkit-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: -o-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: linear-gradient(to bottom, #4c70ba 0, #3b5998 100%);
    background-repeat: repeat-x;
    border: 1px solid #3b5998;	
}

.up, .down, .middle {
	padding-left:16px;	
}
.up, .up-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/up.png) no-repeat left center;	
	color:#3c763d;
}

.down, .down-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/down.png) no-repeat left center;	
	color:#a94442;
}

.middle, .middle-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/middle.png) no-repeat left center;	
	color:#f0ad4e;
}

.up-min, .down-min {
	padding-left:13px;
	background-size: 13px 13px;
}

.plus {
	background:url(<?php echo APP_CAMINHO; ?>public/img/plus.png) no-repeat left left;	
}
.bold {
	font-weight:bold;	
}

.loading {
	display:none;	
}

.abrirSetor, .listarMedico {
	cursor:pointer;	
}

html {
  overflow-y: scroll;
}

body.modal-open,
.modal-open {
    margin-right: 0px;
    margin-right: 0; /* // fix */
}

/*
//.modal-open .navbar-fixed-top,
//.modal-open .navbar-fixed-bottom {
//padding-right: 17px;
//}
*/
.loading-gif {
   position:absolute;
   top:55%;
   left:50%;
   margin-top:-25px;
   margin-left:-25px;
}

.table tbody>tr>td{
    vertical-align: middle;
}


.bcg .panel {
	border:0px;
	-webkit-box-shadow: 0 0px 0px rgba(0,0,0,0);
}


.custom-border {
    border-left:2px solid #ddd !important;
}

</style>

<?php //var_dump($dados['part']); ?>

<div style="padding-left:15px;padding-right:15px">

<div class="row">

<?php require_once('menuMercado.php') ?>

<div class="col-lg-10 col-md-12 panel-right p-0">

    <div class="p-15" style="background: #f5f5f5; border-bottom:1px solid #ddd; ">
        <b>Signo Dashboard</b>
    </div>

           

    <div class="pt-8 p-20 mt-0">
    
     
        <div class="row">
            <?php foreach($dados['resumo'] as $res) { ?>
                    <div class="col-lg-3 p-0">
                        <?php appComp::get('box', ['titulo' => $res['TITULO'], 'subtitulo' => '(selecionados)', 'valor' => $res['TOTAL']]) ?>
                    </div>
                    <?php } ?>

                    <div class="col-lg-3 p-0">
                        <?php appComp::get('box', ['titulo' => 'Relacionamento',  'valor' => 'B4']) ?>
                    </div>

                    <div class="col-lg-3 p-0">
                        <?php appComp::get('box', ['titulo' => 'Nº Pacientes',  'valor' => '64']) ?>
                    </div>

        </div>



    
        <div class="row">
        
            


            <div class="col-lg-4">
                <div class="row">
                    
                    <div class="col-lg-12 p-0">
                        <div class="panel panel-default m-3">
                            <div class="panel-body">
                                <?php appComp::get('piechart', ['option' => $dados['part'], 'height' => '342px']) ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="col-lg-5 p-0">
                <div class="panel panel-default m-3">
                    <div class="panel-body">
                        <?php appComp::get('linechart', ['option' => $dados['line'], 'height' => '342px']) ?>
                    </div>
                </div>
            </div>

            

            
<!--
            <div class="col-lg-4 p-0">
                <?php //appComp::get('box', ['titulo' => "Total de CNPJs (UGN)", 'valor' => $dados['resumo'][1]['NUM_CNPJ'], 'descricao' => '<span class="label label-success">' . appFunction::formatarMoeda($dados['resumo'][1]['PART_CNPJ']*100,1) . '%</span><small> do total cadastrado.</small>']) ?>
            </div>

            <div class="col-lg-4 p-0">
                <?php //appComp::get('box', ['titulo' => "Total de Plataformas (UGN)", 'valor' => $dados['resumo'][1]['NUM_PLAT'], 'descricao' => '<span class="label label-success">' . appFunction::formatarMoeda($dados['resumo'][1]['PART_PLAT']*100,1) . '%</span><small> do total cadastrado.</small>']) ?>
            </div>
-->
        </div>
    


        <div class="row">
            <div class="col-lg-4 p-0">
                
            </div>

            <div class="col-lg-8 p-0">
            <?php //appComp::get('linechart', ['options' => $dados['line']]) ?>
            </div>

        </div>       



        <div class="row">
            <div class="col-lg-12 p-0">



            <div class="panel panel-default m-3">
                
                <div class="panel-body">

                    <p><b>Lista de CNPJ Cadastrados <small>(<?php //echo $dados['tabelaCNPJ']['total'] ?>)</small></b></p>

                    <table class="table table-striped tsable-sm" style="font-size:13px">
                        <thead>
                            <tr class="active">
                                <th>Régua</th>
                                <th>Razão Social</th>
                                <th>Participação</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($dados['tabelaCNPJ'] as $row) { ?>
                            <tr>
                            
                                <td>
                                    <span class="alert alert-info"><b><?php echo $row['POTENCIAL'].$row['RELACIONAMENTO'] ?></b></span>
                                </td>

                                <td>
                                    <p><b><?php echo $row['FANTASIA'] ?></b></p>
                                    <p><small><?php echo $row['CNPJ'] ?> <?php echo $row['RAZAO_SOCIAL'] ?></small></p>
                                </td>
                              
                               
                            </tr>
                            <?php  } ?>
                        </tbody>

                    </table>

               

                    <?php foreach($dados['paginacao']['paginacao'] as $pag) { ?>
                        <a href="#" onclick="setarPagina(<?php echo $pag ?>)"><?php echo $pag ?></a>
                    <?php } ?>
                    

                </div>

            </div>




                
            </div>
        </div>






                
    </div>

      
    
</div>

    

    

    
</div>
        


<script>

$(window).ready(function(e){
    resizePanelLeft();
});


function setarPagina(pag) {
    $('input[name="pagina"]').val(pag);
    $('#form-filtro').submit();
}
</script>





