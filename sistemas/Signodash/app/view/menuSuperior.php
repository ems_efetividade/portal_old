

<style>
#tb-cnpj-superior  {
    width:100%;
}

#tb-cnpj-superior td a {
    cursor:pointer
}

#tb-cnpj-superior td  {
    border-bottom:1px solid #ddd;
}

#tb-cnpj-superior td:hover  {
    background-color: #f5f5f5;
}

#tb-cnpj-superior .subtext {
    font-weight: normal; color:#777;
}

#tb-cnpj-superior  .td-bg {
    background-color: #f9f9f9;
}


</style>



<div class="p-15" style="background: #f5f5f5; border-bottom:1px solid #ddd; ">

    <div class="row">
        <div class="col-lg-2 col-md-6 col-sm-6 col-xs-3">
            <b>Signo</b>
        </div>

        <div class="col-lg-2">
            <div class="hidden-sm hidden-md hidden-xs">
               <!-- <a href="<?php echo APP_CAMINHO ?>">Plataformas</a>
                <a href="#">CNPJS</a> -->
            </div>
        </div>


        <div class="col-lg-8 col-md-6 col-sm-6 col-xs-9">
            <div class="">
            <?php $filename = basename(__FILE__, '.php') ?>
            <input type="text" name="" id="superior" onkeyup="filtrarTabela(this.value, 'tb-cnpj-superior')" class="hidden-lg filtrar-cnpj form-control input-sm" value="" pattern="" title="" autocomplete="off" placeholder="Pesquisar CNPJ..." />
            
            <div id="search-box" class="hidden-lg mCustomScrollbar" style="position:absolute; z-index:1000; height: 300px; overflow:auto; width:100%">
                <div class="panel panel-default">
                    <div class="panel-body" style="font-size:10px;color: #000; ">
                        <table class="m-0 p-0 tablse table-condensed tb-cnpj" id="tb-cnpj-superior">
                        <?php require('CNPJLista.php') ?>
                        </table>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>

</div>

