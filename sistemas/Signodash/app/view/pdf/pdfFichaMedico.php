<style>
.row { margin-bottom:0px; }
.strip { background-color:#f9f9f9; }
.table { font-size:9px; color:#666; width:100%;margin-bottom:10px;}
.table th {font-size:10px;}
.table {padding:0px; border:none; border-collapse:collapse; }
.table td, table th {padding:8px;}
.table { border:1px solid #ddd }
.table td, .table th {border:1px solid #ddd;  }
.active { background-color:#f3f3f3; }
.text-center { text-align:center !important }
.text-left { text-align:left }
.text-danger { color:#a94442 !important }
p { font-size:10px; color:#666; margin:0px; margin-bottom:2px;}
hr { border:0px; margin:0px;  border-bottom:1px solid #ccc; }
h5 { font-size:10px; text-align:left }
h3 { margin:0px; padding:0px; }
h5 small { font-size:80%; font-weight:normal  }
.hr { border-bottom:1px solid #000; margin-bottom:10px; }

.imp-up     { background: url(<?php echo ROOT ?>/public/img/up.png)     no-repeat 47px center; color:#3c763d; }
.imp-down   { background: url(<?php echo ROOT ?>/public/img/down.png)   no-repeat 47px center; color:#a94442; }
.imp-middle { background: url(<?php echo ROOT ?>/public/img/middle.png) no-repeat 47px center; color:#f0ad4e; }

</style>

<div class="row">
    <table width="100%">
        <tr>
            <td width="1%"><img src="public/img/logo.png" height="40" /></td>
            <td valign="middle"></td>
            <td align="right">
                <h2>Ficha do Médico</h2>
                <p>Prescription Share Online</p>
            </td>
        </tr>
    </table>
</div>

<hr />
<br />
<br />

<div class="row">
    <h3><strong><?php print $nomeMedico ?></strong> <small> (<?php print $crm ?>)</small></h3>
    <p><b>ENDEREÇO: </b><?php print $endereco ?></p>
    <p><b>BAIRRO:   </b><?php print $bairro ?></p>
    <p><b>CIDADE:   </b><?php print $cidade ?> / <?php print $uf ?></p>
    <p><b>ESPECIALIDADE: </b> <?php print $especialidade ?></p>
</div>

<br />

<div class="row">
    <h3><strong><img src="public/img/check.png" />&nbsp;Outros Endereços</strong></h3>
    <div class="hr"></div>
    <h5>
        <small>
            <?php print $tabelaEndereco ?>
        </small>
    </h5>
</div>

<div class="row">
    <h3><strong><img src="public/img/check.png" />&nbsp;Mercados e Prescrições</strong></h3>
    <div class="hr"></div>
    <h5>
    	<small>
            <?php print $tabelaMercadoMedico ?>
        </small>
     </h5>
</div>