<?php require_once('app/view/headerView.php'); ?>

<script src="<?php echo APP_CAMINHO ?>plugins/echarts.min.js"></script>
<style>

table p {
	padding:0;
	margin:0px;	
}
.listarMedicos {
	cursor:pointer;
}	
.alert {
	padding:6px;
	margin:1px;	
}
.alert-default {
	color: #777777;
    background-image: -webkit-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: -o-linear-gradient(top, #eeeeee 0, #dddddd 100%);
    background-image: linear-gradient(to bottom, #eeeeee 0, #dddddd 100%);
    background-repeat: repeat-x;
    border: 1px solid #ddd;	
}
.alert-none {	   
    border: 1px solid #eee;	
}

.alert-gray {
	background-color:#f1f1f1;	   
    border: 1px solid #e5e5e5;	
}

.alert-soft {
	background-color:#f9f9f9;	   
    border: 1px solid #e5e5e5;	
}
.alert-primary {
	color: #ffffff;
    background-image: -webkit-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: -o-linear-gradient(top, #4c70ba 0, #3b5998 100%);
    background-image: linear-gradient(to bottom, #4c70ba 0, #3b5998 100%);
    background-repeat: repeat-x;
    border: 1px solid #3b5998;	
}

.up, .down, .middle {
	padding-left:16px;	
}
.up, .up-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/up.png) no-repeat left center;	
	color:#3c763d;
}

.down, .down-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/down.png) no-repeat left center;	
	color:#a94442;
}

.middle, .middle-min {
	background:url(<?php echo APP_CAMINHO; ?>public/img/middle.png) no-repeat left center;	
	color:#f0ad4e;
}

.up-min, .down-min {
	padding-left:13px;
	background-size: 13px 13px;
}

.plus {
	background:url(<?php echo APP_CAMINHO; ?>public/img/plus.png) no-repeat left left;	
}
.bold {
	font-weight:bold;	
}

.loading {
	display:none;	
}

.abrirSetor, .listarMedico {
	cursor:pointer;	
}

html {
  overflow-y: scroll;
}

body.modal-open,
.modal-open {
    margin-right: 0px;
    margin-right: 0; /* // fix */
}

/*
//.modal-open .navbar-fixed-top,
//.modal-open .navbar-fixed-bottom {
//padding-right: 17px;
//}
*/
.loading-gif {
   position:absolute;
   top:55%;
   left:50%;
   margin-top:-25px;
   margin-left:-25px;
}

.table tbody>tr>td{
    vertical-align: middle;
}


.bcg .panel {
	border:0px;
	-webkit-box-shadow: 0 0px 0px rgba(0,0,0,0);
}


.custom-border {
    border-left:2px solid #ddd !important;
}

</style>

<?php //var_dump($dados['part']); ?>

<div style="padding-left:15px;padding-right:15px">

<div class="row">

    <?php require_once('menuMercado.php') ?>

    <div class="col-lg-10 col-md-12 panel-right p-0">

        <?php require_once('menuSuperior.php') ?>

        <div class="pt-8 p-20 mt-0" id="div-center">
            <?php require_once('platView.php') ?>
        </div>

    </div>

    </div>
</div>

<script>

function enviar() {
    $.ajax({
        type: "post",
        url: $('#form-filtro').attr('action'),
        data: $('#form-filtro').serialize(),
        success: function (response) {
            //console.log(response);
            $('#div-center').html(response).show();
            resizeGraph();
        }
    });
}


function resizeGraph() {

    graphs = document.getElementsByClassName('echart-comp');

    for(i=0; i< graphs.length;i++) {
        var chart = echarts.init(document.getElementById(graphs[i].id));
        chart.resize(null, null, true);
    }

}

$(window).ready(function(e){
    resizePanelLeft();
});


$(window).ready(function(e){
   
});


</script>




