<?php 

?>

<h3 style="margin-top:8px"><b><?php echo $criterio['NOME'] ?></b> <small><?php //echo $criterio['CRM'] ?></small></h3>
<div></div>
<div><small><?php echo $criterio['ESP'] ?></small></div>

<hr>


<div>

<!-- Nav tabs -->
<ul class="nav nav-tabs nav-justified" role="tablist">

  <li role="presentation" class="active">
    <a href="#home" aria-controls="home" role="tab" data-toggle="tab"><b>Endereços de Visitação</b></a>
  </li>

  <li role="presentation">
    <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab"><b>Participação em Mercados</b></a>
  </li>

</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="home">
  <h4><b>Outros Endereços</b></h4>
  <table id="tb-ficha-medico" class="table table-condensed table-striped" style="font-size:10px">
    <tr>
        <th>Endereço</th>
        <th>Nro</th>
        <th>Complemento</th>
        <th>Bairro</th>
        <th>Cidade/UF</th>
        <th>Visitador por:</th>
    </tr>

    <?php foreach($dados['enderecos'] as $end) { ?>
        <tr>
            <td><?php echo $end['ENDERECO'] ?></td>
            <td><?php echo $end['NRO'] ?></td>
            <td><?php echo $end['COMPLEMENTO'] ?></td>
            <td><?php echo $end['BAIRRO'] ?></td>
            <td><?php echo $end['CIDADE'].'/'.$end['UF'] ?></td>
            <td><?php echo $end['SETOR'].' '.$end['NOME'] ?></td>
        </tr>
    <?php } ?>
</table>
  </div>
  <div role="tabpanel" class="tab-pane" id="profile">
  <h4><b>Participação em Mercados</b></h4>
  <table width="100%" class="table-bordered table-condensed" style="font-size:11px">

    <?php 
    
        $i = 1;
        

        while(isset($dados['dados'][$i])) {
            $row = $dados['dados'][$i];        
            $mercado = trim($row['MERCADO']);
            ?>
            <tr>
                <td class="text-center" style="border-topw:2px solid #ddd">
                    <p><b><?php echo $mercado ?></b></p>
                </td>

                <td>
                    <table  id="tb-part-mercados" class="table table-hover table-condensed" style="margin-bottom:0px: font-size:11px">
                        <tr class="active">
                            <th >Produto</th>
                            <th class="text-center" width="15%"><?php echo $dados['header']['TRM01'] ?></th>
                            <th class="text-center" width="15%"><?php echo $dados['header']['TRM00'] ?></th>
                            <th class="text-center" width="15%">DIF.</th>
                        </tr>
                    <?php
                    
                    while ($mercado == trim($row['MERCADO'])) { ?>
                        <tr class="<?php echo (($row['PRODUTO_BU'] == 1) ? 'warning' : ''); ?>">
                            <td><p><?php echo $row['PRODUTO'] ?></p></td>
                            <td class="text-center"><p><?php echo appFunction::formatarMoeda($row['PX_ANT'], $config['DECIMAL'], $config['SUFIXO']) ?></p></td>
                            <td class="text-center"><p><?php echo appFunction::formatarMoeda($row['PX_ATU'], $config['DECIMAL'], $config['SUFIXO']) ?></p></td>
                            <td class="text-center"><p><?php echo appFunction::iconTable($row['TOTAL'], $config) ?></p></td>
                        </tr>
                    <?php  $i++;  $row = $dados['dados'][$i]; }  ?>
                    </table>
                </td>
        </tr>

        <?php } ?>
</table>
  </div>
  <div role="tabpanel" class="tab-pane" id="messages">...</div>
  <div role="tabpanel" class="tab-pane" id="settings">...</div>
</div>

</div>



<BR>



