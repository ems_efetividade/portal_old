<?php 
    function iconBrasil($brasil, $val, $config) {
        $valFormatado = appFunction::formatarMoeda($val,$config['DECIMAL'],$config['SUFIXO']);
        return ($val > $brasil) ? '<span class="text-success"><i style="font-size:'.$config['TAM_ICON'].'px" class="glyphicon glyphicon-arrow-up text-success"></i>'.$valFormatado.'</span>' : $valFormatado;
    }
?>
<?php 

$br = $tabelaDados['dados']['dados'][1];


foreach($tabelaDados['princProd'] as $i => $row) {
    if($row['PRODUTO'] != "") {
        $arr[$i] = $row;
    }
}

$tabelaDados['princProd'] = $arr;
$colSize = (90 / (count($arr)*3));


?>


<?php if(isset($conf) && $conf['table'] == 1) { ?>
<style>

.tb-bold {
    font-weight: bold;
}

#table-ps a {
    color: #555;
    text-decoration:none;
}

#table-ps a:hover { 
    text-decoration:none;
}
</style>


<table id="table-ps" class="table table-bordered table-sm table-condensed table-strRiped table-hover" style="font-size:11px;">
    <tr class="active" style="ccolor:#fff;background:#222">

        <th  rowspan="2" widDth="25%" style="fonst-size:17px">
        <h5><small>Mercado de</small></h5>
        <h5><strong><?php echo $tabelaDados['mercado']['MERCADO'] ?></strong></h5>
        </th>
        <th wsidth="25%" colspan="<?php echo count($tabelaDados['princProd']) ?>" class="text-center custom-border" style="font-size:15px"><?php echo $tabelaDados['dados']['header'][1]['TRM01'] ?></th>
        <th wsidth="25%" colspan="<?php echo count($tabelaDados['princProd']) ?>" class="text-center custom-border" style="font-size:15px"><?php echo $tabelaDados['dados']['header'][1]['TRM00'] ?></th>
        <th wsidth="25%" colspan="<?php echo count($tabelaDados['princProd']) ?>" class="text-center custom-border" style="font-size:15px">Penetração (P.P.)</th>
    </tr>

    <tr class="active" style="ccolor:#fff;background:#222">
        <?php foreach($tabelaDados['princProd'] as $i => $row) { ?>
            <th width=""  class="text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>"><small><?php echo $row['PRODUTO'] ?></small></th>
        <?php } ?>

        <?php foreach($tabelaDados['princProd'] as $i => $row) { ?>
            <th width="" class="text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>"><small><?php echo $row['PRODUTO'] ?></small></th>
        <?php } ?>

        <?php foreach($tabelaDados['princProd'] as $i => $row) { ?>
            <th width="" class="text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>"><small><?php echo $row['PRODUTO'] ?></small></th>
        <?php } ?>
    </tr>

<?php } ?>

    <?php foreach($tabelaDados['dados']['dados'] as $indice => $dado) { ?>

        <tr id="<?php echo $dado['ID_SETOR'] ?>" class="n-<?php echo $dado['ID_NIVEL'] ?>">

        
            <td width="20%" class="<?php echo ($dado['ID_PERFIL'] > 1) ? 'tb-bold active' : ''; ?>"><?php echo $conf['pad'] ?>
                <?php if ($dado['ID_PERFIL'] > 1 && $dado['VER_MEDICOS'] == 1) { ?>
                    <a href="#" data-id="<?php echo $dado['ID_SETOR'] ?>" data-container="<?php echo $dado['ID_SETOR'] ?>" class="abrir-setor" >
                        <?php $nomes = explode(' ', $dado['NOME']); echo $dado['SETOR'].' '.$nomes[0].' '.$nomes[count($nomes)-1] ?>
                    </a>
                <?php } else { ?> 
                    <?php $nomes = explode(' ', $dado['NOME']); 
                    echo $dado['SETOR'].' '.$nomes[0].' '.$nomes[count($nomes)-1] ?>
                <?php } ?> 
            </td>
            
            

            <?php for($i=1;$i<=count($tabelaDados['princProd']);$i++) { ?>
                <td style="white-space:nowrap;" class="<?php echo ($dado['ID_PERFIL'] > 1) ? 'tb-bold active' : ''; ?> text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>">
                <?php if ($dado['VER_MEDICOS'] == 1) { ?>
                    <a href="#" class="listar-medico" data-mercado="<?php echo $tabelaDados['mercado']['ID_MERCADO']?>" data-produto="<?php echo $tabelaDados['princProd'][$i]['ID_PRODUTO']  ?>" data-periodo="TRM02|TRM01" data-setor="<?php echo $dado['ID_SETOR'] ?>" data-nomemercado="<?php echo $tabelaDados['mercado']['MERCADO'] ?>|<?php echo $tabelaDados['princProd'][$i]['PRODUTO']  ?>">
                        <?php echo appFunction::formatarMoeda($dado['PROD'.$i.'_TRM01'], $config['DECIMAL'], $config['SUFIXO'])  ?>
                    </a>
                <?php } else { ?> 
                    <?php echo appFunction::formatarMoeda($dado['PROD'.$i.'_TRM01'], $config['DECIMAL'], $config['SUFIXO'])  ?>
                <?php } ?> 
                </td>
            <?php } ?>

            <?php for($i=1;$i<=count($tabelaDados['princProd']);$i++) { ?>
                <td style="white-space:nowrap;" class=" <?php echo ($dado['ID_PERFIL'] > 1) ? 'tb-bold active' : ''; ?> text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>">
                <?php if ($dado['VER_MEDICOS'] == 1) { ?>
                    <a href="#" class="listar-medico" data-mercado="<?php echo $tabelaDados['mercado']['ID_MERCADO']?>" data-produto="<?php echo $tabelaDados['princProd'][$i]['ID_PRODUTO']  ?>" data-periodo="TRM01|TRM00" data-setor="<?php echo $dado['ID_SETOR'] ?>">
                    <?php echo ($tabelaDados['princProd'][$i]['PRODUTO_BU'] == 1) ? iconBrasil($br['PROD'.$i.'_TRM00'], $dado['PROD'.$i.'_TRM00'], $config) : appFunction::formatarMoeda($dado['PROD'.$i.'_TRM00'], $config['DECIMAL'], $config['SUFIXO'])  ?>
                    </a>
                    <?php } else { ?> 
                        <?php echo ($tabelaDados['princProd'][$i]['PRODUTO_BU'] == 1) ? iconBrasil($br['PROD'.$i.'_TRM00'], $dado['PROD'.$i.'_TRM00'], $config) : appFunction::formatarMoeda($dado['PROD'.$i.'_TRM00'], $config['DECIMAL'], $config['SUFIXO'])  ?>
                    <?php } ?> 
                </td>
            <?php } ?>

            <?php for($i=1;$i<=count($tabelaDados['princProd']);$i++) { ?>
                <td style="white-space:nowrap;" class=" <?php echo ($dado['ID_PERFIL'] > 1) ? 'tb-bold active' : ''; ?> text-center <?php echo ($i==1) ? 'custom-border' : ''; ?>">
                <?php echo appFunction::iconTable($dado['PROD'.$i.'_PEN'], $config) ?>
                </td>
            <?php } ?>
        </tr>
    <?php } ?>

    <?php if(isset($conf) && $conf['table'] == 1) { ?>        
    </table>
    <?php } ?>

    <script>
    /*
$('#table-ps .abrir-setor').on('click', function(e){
        e.preventDefault();

        var label = $(this).text();
        var id_setor = $(this).data('id');
        var place = $(this).data('container');

        abrrirSetor($(this));
});
    

function abrrirSetor(obj) {
    console.log(obj);
    var icon = '...';

    var label    = obj.text();
    var id_setor = obj.data('id');
    var place    = obj.data('container');

    existe = $('.n-' + id_setor).length;

    if(existe == 0) {

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/tabelaEquipe',
        data: {
            id_setor: id_setor,
            mercado:  <?php echo $tabelaDados['mercado']['ID_MERCADO'] ?>,
            produtos: '<?php echo json_encode($tabelaDados['princProd']) ?>'
        },
        
        beforeSend: function() {
            obj.text(label + icon);
        },
        
        success: function(retorno) {
            obj.text(label);

            //$('.n-' + id_setor).remove();
            $('#' + place).last().after(retorno);

            
            
            $('.n-' + id_setor + ' a.abrir-setor').unbind().click(function(r){
                r.preventDefault();
                abrrirSetor($(this));
            });

            $('.n-' + id_setor + ' a.listar-medico').unbind().click(function(e){
                e.preventDefault();
                listarMedico($(this));
            });

            resizePanelLeft();
        }
    }); 

    } else {
        $('.n-' + id_setor).remove();
        resizePanelLeft();
    }
}

    */
    </script>