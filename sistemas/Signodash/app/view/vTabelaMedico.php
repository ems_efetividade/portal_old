<?php 
    function iconTable($val=0, $config) {

        $valor = appFunction::formatarMoeda($val, $config['DECIMAL'], $config['SUFIXO']);

        if($val > 0) {
            $icon = '<span class="glyphicon glyphicon-arrow-up text-success"></span>';
        } else {
            if($val == 0) {
                $icon = '<span class="glyphicon glyphicon-arrow-right text-warning"></span>';
            } else {
                $icon = '<span class="glyphicon glyphicon-arrow-down text-danger"></span>';
            }
        }

        return '<span class="text-success">'.$icon.$valor.'</span>';
    }



    function inicioPx($ant, $atu) {
        $class = '';

        if($ant == null && $atu != null) {
            $class = 'ttext-primary';
        }

        if($ant != null && $atu == null) {
            $class = 'tdanger';
        }

        return $class;
    }

    $periodo = explode('|', $criterio['PERIODO']);
?>
<style>
.col-xs-5ths,
.col-sm-5ths,
.col-md-5ths,
.col-lg-5ths {
    position: relative;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
}

.col-xs-5ths {
    width: 20%;
    float: left;
}

@media (min-width: 768px) {
    .col-sm-5ths {
        width: 20%;
        float: left;
    }
}

@media (min-width: 992px) {
    .col-md-5ths {
        width: 20%;
        float: left;
    }
}

@media (min-width: 1200px) {
    .col-lg-5ths {
        width: 20%;
        float: left;
    }
}
</style>

<h4>Prescritores de <b><?php echo $merc['PRODUTO']['PRODUTO'] ?></b><small>  -  Mercado de <b><?php echo $merc['MERCADO']['MERCADO'] ?></b> no <b><?php echo $trim[1][$periodo[1]] ?></b></small></h4>

<div style="padding-right:10px;padding-left:10px;">
    <div class="row">
        
        <?php foreach($dados['resumo'] as $u => $res) { ?>
            <div class="col-lg-5ths col-md-5ths col-sm-5ths col-xs-6" style="padding-right:3px;padding-left:3px;">
                <div class="panel panel-default" >
                    <div class="panel-body text-center" style="padding:10px;"> 
                        <div><small><small><?php echo ($res['CAT'] == 0) ? '<B>Total de Médicos</B>' : 'Cat ' . $res['CAT'] ?></small></small></div>
                        <h3 style="margin-top:5px;margin-bottom:5px"><strong><?php echo appFunction::formatarMoeda($res['MED'],0) ?></strong></h3>
                        <P><small><?php echo  appFunction::formatarMoeda($res['PX'],$config['DECIMAL'], $config['SUFIXO']) ?></small></P>
                        <div>
                            <small><b><?php echo ($res['EVOL'] > 0) ? '<span class="label label-success">+' . appFunction::formatarMoeda($res['EVOL'],$config['DECIMAL'], $config['SUFIXO']). '</span>' : '<span class="label label-danger">' . appFunction::formatarMoeda($res['EVOL'],$config['DECIMAL'], $config['SUFIXO']).'</span>' ?></b><small> ref. trim. ant.</small></small>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>


 
<table id="tb-medico" class="table table-bordered table-condensed table-striped" style="font-size:10px">
<thead>
    <tr class="active">
        <th>MEDICO</th>  
        <th>ESP</th>
        
        <!-- <th rowspan="2" >CIDADE</th> -->
        <th rowspan="2" class="text-center">
            <div class="btn-group">CAT 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
            <li><a href="#" class="filtra-cat" data-val="0" ><small>Todas as Categorias</small></a></li>
            <li role="separator" class="divider"></li>
                <?php for($a=1;$a<=5;$a++) { ?>
                <li><a href="#" class="filtra-cat" data-val="<?php echo $a ?>" ><small>Categoria <?php echo $a ?></small></a></li>
                <?php } ?>
            </ul>
            </div>
        </th>
        <th class="text-center"><?php echo $dados['header']['TRM01'] ?></th>
        <th class="text-center"><?php echo $dados['header']['TRM00'] ?></th>
        <th class="text-center">Dif</th>
    </tr>

    </thead>
    <tbody>
    <?php foreach($dados['dados'] as $dado) { ?>
    
    <tr data-cat="<?php echo $dado['CAT'] ?>" class="<?php echo inicioPx($dado['PX_ANT'], $dado['PX_ATU']); ?>">
        <td><a href="#" class="ficha-medico" 
                        data-crm="<?php echo $dado['ID_CRM'] ?>"
                        data-nome="<?php echo $dado['NOME'] ?>"
                        data-esp="<?php echo $dado['ESP'] ?>"
                        data-periodo="<?php echo $criterio['PERIODO'] ?>"
                        >
            
            <?php echo $dado['CRM'].'</a> '. $dado['NOME'] ?>
        </td>
        <td><?php echo $dado['ESP'] ?></td>
        
        <!-- <td><?php echo $dado['CIDADE'].'/'.$dado['UF']  ?></td> -->
        <td class="text-center"><?php echo $dado['CAT'] ?></td>
        
        <td class="text-center"><?php echo appFunction::formatarMoeda($dado['TRM_ANT'],$config['DECIMAL'], $config['SUFIXO']) ?></td>
        <td class="text-center"><?php echo appFunction::formatarMoeda($dado['TRM_ATU'],$config['DECIMAL'], $config['SUFIXO']) ?></td>
        <td class="text-center"><?php echo appFunction::iconTable($dado['DIF'], $config) ?></td>
    </tr>
    <?php } ?>
    </tbody>
</table>

<script>

$('.filtra-cat').unbind().click(function(e){
    e.preventDefault();
    var cat = $(this).data('val');

    $('#tb-medico').removeClass('table-striped');
    $('#tb-medico tbody tr[data-cat]').show();

    if(cat > 0) {
        $('#tb-medico tbody tr[data-cat!="'+cat+'"]').hide();
    }
    $('#tb-medico').addClass('table-striped');
});

$('.ficha-medico').unbind().click(function(e){
    e.preventDefault();
    
    var crm     = $(this).data('crm');
    var nome    = $(this).data('nome');
    var esp     = $(this).data('esp');
    var periodo = $(this).data('periodo');

    $.ajax({
        type: "POST",
        url: '<?php print APP_CAMINHO ?>home/fichaMedico',
        data: {
            crm: crm,
            nome: nome,
            esp: esp,
            periodo: periodo
        },
        
        beforeSend: function() {

        },
        
        success: function(retorno) {
            $('#modalFichaMedico .modal-body').html(retorno);
            $('#modalFichaMedico').modal('show');

            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-crm', crm);
            $('#modalFichaMedico #btn-exportar-ficha-medico').attr('data-periodo', periodo);
            $('#modalFichaMedico').modal('show');

            $('#btn-exportar-ficha-medico').unbind().click(function(s){
                s.preventDefault();
                window.location.href = "<?php print APP_CAMINHO ?>home/exportarFichaMedico/"+crm+"/"+periodo+"";
            });

        }
    });  

});



</script>
