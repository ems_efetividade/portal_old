<?php 

require_once(getcwd() . '/lib/_chart/ide/eData.php');
require_once(getcwd() . '/lib/_chart/ide/eSerie.php');
require_once(getcwd() . '/lib/_chart/ide/eLegend.php');
require_once(getcwd() . '/lib/_chart/ide/eTooltip.php');
require_once(getcwd() . '/lib/_chart/ide/eTitle.php');
require_once(getcwd() . '/lib/_chart/ide/eGrid.php');
require_once(getcwd() . '/lib/_chart/ide/eLabel.php');

require_once(getcwd() . '/lib/_chart/ide/eXAxis.php');
require_once(getcwd() . '/lib/_chart/ide/eYAxis.php');

class eLine {

    public $graf;

    public $data;
    public $serie;
    public $legend;
    public $tooltip;
    public $title;
    public $grid;
    public $label;

    public $xAxis;
    public $yAxis;

    public function __construct() {
        $this->label   = new eLabel();
        $this->data    = new eData();
        $this->serie   = new eSerie(get_class());
        $this->legend  = new eLegend();
        $this->tooltip = new eTooltip();
        $this->title   = new eTitle();
        $this->grid    = new eGrid();

        $this->xAxis = new eXAxis();
        $this->yAxis = new eYAxis();
    }

    private function setGraf() {
        //$this->serie->data($this->data);

        $this->graf = [
            'title'   => $this->title->getTitle(),
            'legend'  => $this->legend->getLegend(),
            'tooltip' => $this->tooltip->getTooltip(),
            'grid'    => $this->grid->getGrid(),
            'xAxis' => $this->xAxis->getXAxis(),
            'yAxis' => $this->yAxis->getYAxis(),
            'series'  => $this->serie->getSerie(),
            'label' => $this->label->getLabel()
        ];
    }

    public function render() {
        $this->setGraf();
        return $this->graf;
    }

    public function renderToJson() {
        $this->setGraf();
        return preg_replace('/"([^"]+)"\s*:\s*/', '$1:', json_encode($this->graf, JSON_NUMERIC_CHECK));
    }

}

?>
