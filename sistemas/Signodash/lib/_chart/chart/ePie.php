<?php 

require_once(getcwd() . '/lib/_chart/ide/eData.php');
require_once(getcwd() . '/lib/_chart/ide/eSerie.php');
require_once(getcwd() . '/lib/_chart/ide/eLegend.php');
require_once(getcwd() . '/lib/_chart/ide/eTooltip.php');
require_once(getcwd() . '/lib/_chart/ide/eTitle.php');

class ePie {

    public $graf;
    public $label;

    public $data;
    public $serie;
    public $legend;
    public $tooltip;
    public $title;

    private $class;

    public function __construct() {
        $this->class = get_class();
        $this->label = [];
        $this->data  = new eData();
        $this->serie = new eSerie($this->class);
        $this->legend = new eLegend();
        $this->tooltip = new eTooltip();
        $this->title = new eTitle();
    }

    private function setGraf() {
        //$this->serie->data($this->data);
        
        $this->graf = [
            'title'   => $this->title->getTitle(),
            'legend'  => $this->legend->getLegend(),
            'tooltip' => $this->tooltip->getTooltip(),
            'series'  => $this->serie->getSerie(),
        ];
    }

    public function label($property, $value) {
        $this->label[$property] = $value;
    }

    public function render() {
        $this->setGraf();
        return $this->graf;
    }

    public function renderToJson() {
        $this->setGraf();
        return json_encode($this->graf, JSON_NUMERIC_CHECK);
    }

}

?>