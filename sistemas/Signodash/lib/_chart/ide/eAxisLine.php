<?php 

class eAxisLine  {
    
    private $obj;

    public function show($value=null) {
        $this->obj['show'] = $value;
    }

    public function name($value=null) {
        $this->obj['name'] = $value;
    }

    public function position($value=null) {
        $this->obj['position'] = $value;
    }

    public function fontSize($value=null) {
        $this->obj['fontSize'] = $value;
    }

    public function interval($value=null) {
        $this->obj['interval'] = $value;
    }

    public function inside($value=null) {
        $this->obj['inside'] = $value;
    }

    public function rotate($value=null) {
        $this->obj['rotate'] = $value;
    }

    public function formatter($value=null) {
        $this->obj['formatter'] = $value;
    }

    public function getLine() {
        return $this->obj;
    }



}

?>