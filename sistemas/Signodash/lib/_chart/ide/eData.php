<?php 

require_once(getcwd() . '/lib/_chart/ide/eLabel.php');
require_once(getcwd() . '/lib/_chart/ide/eTextStyle.php');

class eData  {

    private $obj;
    private $tmp;

    public $label;
    private $textStyle;


    public function __construct()
    {
        $this->label = new eLabel();
        $this->textStyle = new eTextStyle();
    }

    public function name($value=null) {
        $this->tmp['name'] = $value;
    }

    public function value($value=null) {
        $this->tmp['value'] = $value;
    }

    public function selected($value=null) {
        $this->tmp['selected'] = $value;
    }

    public function yAxis($value=null) {
        $this->tmp['yAxis'] = $value;
    }

    public function x($value=null) {
        $this->tmp['x'] = $value;
    }

    public function y($value=null) {
        $this->tmp['y'] = $value;
    }

    public function dataArray($data=null) {
        $this->obj = $data;
    }

    public function textStyle() {
        return $this->textStyle;
    }





    public function add() {
        $this->tmp['label'] = $this->label->getLabel();
        $this->tmp['textStyle'] = $this->textStyle->getTextStyle();
        $this->obj[] = $this->tmp;
    }
    public function getData() {
        return $this->obj;
        $this->obj = null;
    }



}

?>