<?php 

class eGrid  {
    private $obj;

    public function left($value=null) {
        $this->obj['left'] = $value;
    }

    public function top($value=null) {
        $this->obj['top'] = $value;
    }

    public function right($value=null) {
        $this->obj['right'] = $value;
    }

    public function bottom($value=null) {
        $this->obj['bottom'] = $value;
    }

    public function containLabel($value=null) {
        $this->obj['containLabel'] = $value;
    }

    public function width($value=null) {
        $this->obj['width'] = $value;
    }

    public function show($value=null) {
        $this->obj['show'] = $value;
    }

    public function height($value=null) {
        $this->obj['height'] = $value;
    }

    public function getGrid() {
        return $this->obj;
    }



}

?>    