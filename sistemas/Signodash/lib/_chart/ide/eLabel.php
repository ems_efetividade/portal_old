<?php 

require_once(getcwd() . '/lib/_chart/ide/eNormal.php');

class eLabel  {
    private $obj;

    public $normal;

    public function __construct() {
       
        $this->normal = new eNormal();
    }

    public function show($value=null) {
        $this->obj['show'] = $value;
    }

    public function name($value=null) {
        $this->obj['name'] = $value;
    }

    public function position($value=null) {
        $this->obj['position'] = $value;
    }

    public function fontSize($value=null) {
        $this->obj['fontSize'] = $value;
    }

    public function fontWeight($value=null) {
        $this->obj['fontWeight'] = $value;
    }

    public function formatter($value=null) {
        $this->obj['formatter'] = $value;
    }

    public function color($value=null) {
        $this->obj['color'] = $value;
    }

    public function getLabel() {
        $this->obj['normal'] = $this->normal->getNormal();
        return $this->obj;
    }



}

?>