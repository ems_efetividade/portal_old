<?php 

require_once('eTextStyle.php');

class eLegend {

    private $obj;

    private $textStyle;

    public function __construct()
    {
        $this->textStyle = new eTextStyle();
    }

    public function orient($value) {
        $this->obj['orient'] = $value;
    }

    public function left($value) {
        $this->obj['left'] = $value;
    }

    public function data($value) {
        $this->obj['data'] = $value;
    }

    public function type($value) {
        $this->obj['type'] = $value;
    }

    public function show($value) {
        $this->obj['show'] = $value;
    }

    public function bottom($value) {
        $this->obj['bottom'] = $value;
    }

    public function right($value) {
        $this->obj['right'] = $value;
    }

    public function top($value) {
        $this->obj['top'] = $value;
    }

    public function textStyle() {
        return $this->textStyle;
    }


    public function getLegend() {
        $this->obj['textStyle'] = $this->textStyle->getTextStyle();
        return $this->obj;
    }

}

?>