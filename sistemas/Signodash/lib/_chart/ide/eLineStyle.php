<?php 

class eLineStyle  {
    private $obj;

    public function type($value=null) {
        $this->obj['type'] = $value;
    }

    public function color($value=null) {
        $this->obj['color'] = $value;
    }

    public function width($value=null) {
        $this->obj['width'] = $value;
    }

    public function bottom($value=null) {
        $this->obj['bottom'] = $value;
    }

    

    public function getLineStyle() {
        return $this->obj;
    }



}

?>    