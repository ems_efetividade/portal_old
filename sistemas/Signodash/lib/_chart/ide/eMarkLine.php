<?php 

require_once(getcwd() . '/lib/_chart/ide/eLineStyle.php');
require_once(getcwd() . '/lib/_chart/ide/eData.php');
require_once(getcwd() . '/lib/_chart/ide/eLabel.php');

class eMarkLine  {
    private $obj;

    public $lineStyle;
    public $data;
    public $label;

    public function __construct()
    {
        $this->lineStyle = new eLineStyle();
        $this->data = new eData();
        $this->label = new eLabel();
    }

    public function show($value=null) {
        $this->obj['show'] = $value;
    }

    public function position($value=null) {
        $this->obj['position'] = $value;
    }

   
    public function getMarkLine() {
        $this->obj['lineStyle'] = $this->lineStyle->getLineStyle();
        $this->obj['data'] = $this->data->getData();
        $this->obj['label'] = $this->label->getLabel();
        return $this->obj;
    }



}

?>