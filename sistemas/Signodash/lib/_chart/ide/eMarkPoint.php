<?php 


require_once(getcwd() . '/lib/_chart/ide/eData.php');


class eMarkPoint  {
    private $obj;

    public $data;


    public function __construct()
    {
        $this->obj = [];
        $this->data = new eData();
    }

    public function show($value=null) {
        $this->obj['show'] = $value;
    }

    public function position($value=null) {
        $this->obj['position'] = $value;
    }

   
    public function getMarkPoint() {
        $this->obj['data'] = $this->data->getData();
        return $this->obj;
    }



}

?>