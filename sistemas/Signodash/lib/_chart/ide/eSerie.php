<?php 

require_once(getcwd() . '/lib/_chart/ide/eLabel.php');
require_once(getcwd() . '/lib/_chart/ide/eData.php');
require_once(getcwd() . '/lib/_chart/ide/eLineStyle.php');
require_once(getcwd() . '/lib/_chart/ide/eMarkLine.php');
require_once(getcwd() . '/lib/_chart/ide/eMarkPoint.php');

class eSerie {

    private $obj;

    public $label;
    public $data;
    public $lineStyle;
    public $markLine;
    public $markPoint;

    private $chart;

    public function __construct($chart=null) {

        $this->chart = $chart;

        $this->tmp       = [];
        $this->label     = new eLabel();
        $this->data      = new eData();
        $this->lineStyle = new eLineStyle();
        $this->markLine  = new eMarkLine(); 
        $this->markPoint = new eMarkPoint();

        
    }

    public function name($value=null) {
        $this->obj['name'] = $value;
    }

    public function type($value=null) {
        $this->obj['type'] = $value;
    }

    public function left($value=null) {
        $this->obj['left'] = $value;
    }

    public function radius($value=null) {
        $this->obj['radius'] = $value;
    }

    public function center($value=null) {
        $this->obj['center'] = $value;
    }

    public function stack($value) {
        $this->obj['stack'] = $value;
    }

    public function smooth($value) {
        $this->obj['smooth'] = $value;
    }

    public function symbol($value) {
        $this->obj['symbol'] = $value;
    }

    public function barWidth($value) {
        $this->obj['barWidth'] = $value;
    }


    public function add() {

        $this->obj['label']      = $this->label->getLabel();
        $this->obj['data']       = $this->data->getData();
        $this->obj['lineStyle']  = $this->lineStyle->getLineStyle();
        $this->obj['markLine']   = $this->markLine->getMarkLine();

       // if($this->chart != 'ePie') {
            $this->obj['markPoint']  = $this->markPoint->getMarkPoint();
        //}
       
        $this->tmp[] = $this->obj; 
    }

    public function getSerie() {
        return $this->tmp;
        $this->tmp = '';

    }

}

?>
