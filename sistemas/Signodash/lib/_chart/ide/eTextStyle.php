<?php 


class eTextStyle {

    private $obj;

    public function color($value=null) {
        $this->obj['color'] = $value;
    }

    public function fontStyle($value=null) {
        $this->obj['fontStyle'] = $value;
    }

    public function fontWeight($value=null) {
        $this->obj['fontWeight'] = $value;
    }

    public function fontFamily($value=null) {
        $this->obj['fontFamily'] = $value;
    }

    public function fontSize($value=null) {
        $this->obj['fontSize'] = $value;
    }

   
    public function getTextStyle() {
        return $this->obj;
    }

}

?>