<?php 

require_once(getcwd() . '/lib/_chart/ide/eTextStyle.php');

class eTitle {

    private $obj;

    public $textStyle;

    public function __construct() {
        $this->textStyle = new eTextStyle();
    }

    public function text($value) {
        $this->obj['text'] = $value;
    }

    public function subtext($value) {
        $this->obj['subtext'] = $value;
    }

    public function x($value) {
        $this->obj['x'] = $value;
    }

    public function top($value) {
        $this->obj['top'] = $value;
    }

    public function left($value) {
        $this->obj['left'] = $value;
    }

    public function right($value) {
        $this->obj['right'] = $value;
    }

    public function bottom($value) {
        $this->obj['bottom'] = $value;
    }

    public function textAlign($value) {
        $this->obj['textAlign'] = $value;
    }

    public function padding($value) {
        $this->obj['padding'] = $value;
    }

    public function getTitle() {
        if ($this->textStyle->getTextStyle() <> []) {
            $this->obj['textStyle'] = $this->textStyle->getTextStyle();
        }
        return $this->obj;
    }

}

?>