<?php 

class eTooltip {

    private $obj;

    public function trigger($value) {
        $this->obj['trigger'] = $value;
    }

    public function formatter($value) {
        $this->obj['formatter'] = $value;
    }

    public function getTooltip() {
        return $this->obj;
    }

}

?>