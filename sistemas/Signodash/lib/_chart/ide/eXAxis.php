<?php 

require_once('eTextStyle.php');
require_once('eAxisLabel.php');
require_once('eAxisLine.php');

class eXAxis {

    private $obj;
    private $textStyle;
    private $axisLabel;
    private $axisLine;

    public function __construct()
    {
        $this->textStyle = new eTextStyle();
        $this->axisLabel = new eAxisLabel();
        $this->axisLine = new eAxisLine();
    }
    
    public function type($value) {
        $this->obj['type'] = $value;
    }

    public function data($value='') {
        $this->obj['data'] = $value;
        return $this;
    }

    public function boundaryGap($value=false) {
        $this->obj['boundaryGap'] = $value;
    }

    public function axisLabel() {
        return $this->axisLabel;
    }

    public function axisLine() {
        return $this->axisLine;
    }


    public function getXAxis() {
        //$this->obj['axisLabel'] = $this->textStyle->getTextStyle();
        $this->obj['axisLabel'] = $this->axisLabel()->getLabel();
        $this->obj['axisLine'] = $this->axisLine->getLine();
        return $this->obj;
    }

}

?>