<?php 

require_once('eTextStyle.php');
require_once('eAxisLabel.php');
require_once('eAxisLine.php');

class eYAxis {

    private $obj;
    private $textStyle;
    private $axisLabel;
    private $axisLine;

    public function __construct()
    {
        $this->textStyle = new eTextStyle();
        $this->axisLabel = new eAxisLabel();
        $this->axisLine = new eAxisLine();
    }

    
    public function type($value) {
        $this->obj['type'] = $value;
    }

    public function min($value) {
        $this->obj['min'] = $value;
    }

    public function max($value) {
        $this->obj['max'] = $value;
    }

    public function scale($value) {
        $this->obj['scale'] = $value;
    }

    public function splitNumber($value) {
        $this->obj['splitNumber'] = $value;
    }



    public function axisLabel() {
        return $this->axisLabel;
    }

    public function axisLine() {
        return $this->axisLine;
    }

   
    public function getYAxis() {
        $this->obj['axisLabel'] = $this->axisLabel->getLabel();
        $this->obj['axisLine'] = $this->axisLine->getLine();
        return $this->obj;
    }

}

?>