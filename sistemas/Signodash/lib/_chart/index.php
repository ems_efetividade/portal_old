<?php 

require_once('chart/ePie.php');


$pie = new ePie();

$pie->title->text('Teste de Título de Gráfico');
$pie->title->subtext('Subtítulo do Gráfico');
$pie->title->x('center');




for($i=1;$i<=2;$i++) {
    $campo[] = 'Campo' . $i;
    $pie->data->name('Campo' . $i);
    $pie->data->value(rand());
    $pie->data->put();
}

/*
$pie->data->name('Campo 1');
$pie->data->value(20);
$pie->data->put();

$pie->data->name('Campo 2');
$pie->data->value(80);
$pie->data->put();
*/

$
$pie->legend->type('scroll');
$pie->legend->orient('vertical');
$pie->legend->left('left');
$pie->legend->data($campo);

$pie->tooltip->formatter('{b} : {c} ({d}%)');
$pie->tooltip->trigger('item');

$pie->serie->name('Pizza');
$pie->serie->type('pie');
$pie->serie->left('left');
$pie->serie->radius('55%');
$pie->serie->center(['50%', '50%']);

echo '<pre>';
//echo json_encode($pie->getPie(), JSON_ERROR_SYNTAX);
//print_r($pie->getPie());
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- including ECharts file -->
    <script src="echarts.min.js"></script>
</head>

<body>
    <!-- preparing a DOM with width and height for ECharts -->
    <div id="main" style="width:600px; height:400px;"></div>
</body>


<script>
var myChart = echarts.init(document.getElementById('main'));
var option = {
    title : {
        text: '某站点用户访问来源',
        subtext: '纯属虚构',
        x:'center'
    },
    tooltip : {
        trigger: 'item',
        formatter: "{a} {b} : {c} ({d}%)"
    },
    legend: {
        orient: 'vertical',
        left: 'left',
        data: ['a','b','c','d','e']
    },
    series : [
        {
            name: '访问来源',
            type: 'pie',
            radius : '55%',
            center: ['50%', '60%'],
            data:[
                {value:335, name:'直接访问'},
                {value:310, name:'邮件营销'},
                {value:234, name:'联盟广告'},
                {value:135, name:'视频广告'},
                {value:1548, name:'搜索引擎'}
            ]
        }
    ]
};
console.log(option);

var option = <?php echo json_encode($pie->render()); ?>

myChart.setOption(option);


console.log(<?php echo json_encode($pie->render()); ?>);

</script>

</html>