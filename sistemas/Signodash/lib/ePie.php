<?php 

class ePie {

    private $opt;
    private $key;
    private $serie;

    public function __construct()
    {
        $this->opt = [
            'title' => [
                'text' => 'Title',
                'subtext' => 'Subtitle'
            ],

            'series' => 
                '', 
                'pie', 
                '55%', 
                ['50%','60%'], 
                [
                    ['value' => 335,  'name' => 'a'],
                    ['value' => 310,  'name' => 'b'],
                    ['value' => 234,  'name' => 'c'],
                    ['value' => 135,  'name' => 'd'],
                    ['value' => 1548, 'name' => 'e']
                ]
            
        ];
    }

    public function title() {
        $this->key = 'title';
        $this->opt[$this->key];
        return $this;
    }

    public function tooltip() {
        $this->key = 'tooltip';
        @$this->opt[$this->key];
        return $this;
    }

    public function legend() {
        $this->key = 'legend';
        @$this->opt[$this->key];
        return $this;
    }

    /*
    public function series($v=[]) {
        $this->key = 'series';
        @$this->opt[$this->key][]= $v;
        return $this;
    }
*/

    public function sseries($name='', $type='pie', $radius='55%', $center=['50%','60%'], $data=[]) {
        $serie = [
            'name' => $name,
            'type' => $type,
            'radius' => $radius,
            'center' => $center,
            'data' => $data
        ];

        $this->opt['series'][]= $serie;

    }

    public function addSerie($chave, $valor=null) {
        if($valor !== null) {
            $this->serie[$chave] = $valor;
        }
    }

        public function serieName($val=null) {
            $this->addSerie('name', $val);
        }

        public function serieType($val=null) {
            $this->addSerie('type', $val);
        }

        public function serieRadius($val=null) {
            $this->addSerie('radius', $val);
        }

        public function serieCenter($val=null) {
            $this->addSerie('center', $val);
        }

        public function serieData($val=null, $name=null) {
            $this->addSerie('data', array('name' => $name, 'value' => $val));
        }

        public function setSerie() {
            $this->opt['series'][]= $this->serie;
            $this->serie = [];
        }


    public function text($val='Title') {
        $this->opt[$this->key]['text'] = $val;
        return $this;
    }

    public function subtext($val='Subtext') {
        $this->opt[$this->key]['subtext'] = $val;
        return $this;
    }

    public function x($val='center') {
        $this->opt[$this->key]['x'] = $val;
        return $this;
    }


    public function trigger($val='item') {
        $this->opt[$this->key]['trigger'] = $val;
        return $this;
    }

    public function formatter($val='') {
        $this->opt[$this->key]['formatter'] = $val;
        return $this;
    }

    public function orient($val='vertical') {
        $this->opt[$this->key]['orient'] = $val;
        return $this;
    }

    public function left($val='left') {
        $this->opt[$this->key]['left'] = $val;
        return $this;
    }

    public function data($val='') {
        $this->opt[$this->key]['data'] = $val;
        return $this;
    }


    public function render() {
        return json_encode($this->opt);
    }



}


?>
