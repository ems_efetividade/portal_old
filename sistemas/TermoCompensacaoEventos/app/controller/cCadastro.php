<?php 

require_once('lib/appController.php');

require_once('app/model/mCadastro.php');
require_once('app/model/mTermo.php');
require_once('app/model/mPeriodo.php');
require_once('app/model/mColaboradorSetor.php');
require_once('app/model/mCompensacao.php');

class cCadastro extends appController {

    private $cadastro = null;
    private $termo = null;
    private $colaborador = null;
    private $periodo = null;
    
    public function __construct() {
        $this->cadastro = new mCadastro();
        $this->termo = new mTermo();
        $this->periodo = new mPeriodo();
        $this->colaborador = new mColaboradorSetor();
    }
    
    public function abrirFormCadastro($idCadastro=0) {
        
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->selecionar();
        
        $this->colaborador->setIdSetor(fn_dadoSessao('id_setor'));
        
        $this->set('cadastro', $this->cadastro);
        $this->set('periodo', $this->periodo);
        $this->set('colaborador', $this->colaborador);
        $this->set('termo', $this->termo);
        $this->render('vCadastroForm');
    }
    
    public function verificarTermo($idColaborador) {
        $this->cadastro->setIdColaborador($idColaborador);
        return $this->cadastro->verificarTermo();
    }
    
    public function excluir($idCadastro) {
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->excluir();
        $this->reload();
    }
    
    public function salvar() {
        $_POST = appSanitize::filter($_POST);
        
        $idCadastro = $_POST['txtIdCadastro'];
        $idTermo = $_POST['cmbTermo'];
        $colaborador = explode("|", $_POST['cmbEquipe']);
        $dataTrabalhada = $_POST['txtDataTrabalhada'];
        $idPeriodoTrabalhado = $_POST['cmbPeriodoTrabalhado'];
        $dataCompensar = $_POST['txtDataCompensar'];
        $idPeriodoCompensar = $_POST['cmbPeriodoCompensar'];  
        $observacao = $_POST['txtObservacao'];
        
        
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->setIdTermo($idTermo);
        $this->cadastro->setDataTrabalhada(fn_formatarData($dataTrabalhada));
        $this->cadastro->setIdPeriodoTrabalhado($idPeriodoTrabalhado);
        $this->cadastro->setDataCompensar(fn_formatarData($dataCompensar));
        $this->cadastro->setIdPeriodoCompensar($idPeriodoCompensar);
        $this->cadastro->setIdSetor($colaborador[0]);
        $this->cadastro->setIdColaborador($colaborador[1]);
        $this->cadastro->setIdSetorCadastro(fn_dadoSessao('id_setor'));
        $this->cadastro->setIdColaboradorCadastro(fn_dadoSessao('id_colaborador'));
        $this->cadastro->setObservacao($observacao);
        
        echo $this->cadastro->salvar();
        
        //$this->reload();
        
    }
    
    public function visualizar($idCadastro) {
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->selecionar();
        
        
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($this->cadastro->getIdColaborador());
        $col->selecionarPorColaborador();
        
        
        $texto = $this->cadastro->termo->getTexto();
        
        $texto = fn_substituirAlias('{NOME}',           $col->getColaborador(), $texto);
        $texto = fn_substituirAlias('{ID}',             $col->getId(), $texto);
        $texto = fn_substituirAlias('{CARGO}',          $col->getPerfil(), $texto);
        $texto = fn_substituirAlias('{SETOR}',          $col->getSetor(), $texto);
        $texto = fn_substituirAlias('{LINHA}',          $col->getLinha(), $texto);
        $texto = fn_substituirAlias('{EVENTO}',         $this->cadastro->getObservacao(), $texto);
        $texto = fn_substituirAlias('{DATA TRABALHADA}',fn_formatarData($this->cadastro->getDataTrabalhada()), $texto);
        $texto = fn_substituirAlias('{COMPENSACAO}',    fn_formatarData($this->cadastro->getDataCompensar()), $texto);
        
        $this->set('cadastro', $this->cadastro);
        $this->set('termo', $texto);
        echo $this->renderToString('vVisualizarTermo');

    }
    
    public function listarTermosAssinados() {
        $this->cadastro->setIdColaborador(fn_dadoSessao('id_colaborador'));
        $documentos = $this->cadastro->listarPorColaborador();
        
        $link = '';
        foreach($documentos as $doc) {
            $link .= '<a href="#">'.$doc->termo->getNome().'</a>';
        }
        
        return $link;
    }
    
    public function pdf($idCadastro) {
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->selecionar();
        
        
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($this->cadastro->getIdColaborador());
        $col->selecionarPorColaborador();
        
        
        $texto = $this->cadastro->termo->getTexto();
        
        $texto = fn_substituirAlias('{NOME}',           $col->getColaborador(), $texto);
        $texto = fn_substituirAlias('{ID}',             $col->getId(), $texto);
        $texto = fn_substituirAlias('{CARGO}',          $col->getPerfil(), $texto);
        $texto = fn_substituirAlias('{SETOR}',          $col->getSetor(), $texto);
        $texto = fn_substituirAlias('{LINHA}',          $col->getLinha(), $texto);
        $texto = fn_substituirAlias('{EVENTO}',         $this->cadastro->getObservacao(), $texto);
        $texto = fn_substituirAlias('{DATA TRABALHADA}',fn_formatarData($this->cadastro->getDataTrabalhada()), $texto);
        $texto = fn_substituirAlias('{COMPENSACAO}',    fn_formatarData($this->cadastro->getDataCompensar()), $texto);
        
        $this->set('cadastro', $this->cadastro);
        $this->set('termo', $texto);
        $html = $this->renderToString('vPDF');
        
        $nomeArquivo = str_replace(" ", "_",utf8_decode($col->getColaborador().'_'.$this->cadastro->termo->getNome().'_'.str_replace(" ", "_", str_replace(":", "_", $this->cadastro->getDataAssinatura())).'_'.$this->cadastro->getIdCadastro()));
        
        fn_gerarPDF($html, '', $nomeArquivo);
    }
    
    public function substituirAlias($de, $para, $texto) {
        $texto = str_replace($de, $para, $texto);
        return $texto;
    }
    
    public function substituirTags($texto, $idColaborador) {
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($idColaborador);
        $col->selecionarPorColaborador();
        
        $tags     = array('{ID}','{NOME}','{LINHA}','{SETOR}','{CARGO}');
        $valorTag = array('{ID}' => $col->getID(),
                      '{NOME}' => $col->getColaborador(),
                      '{LINHA}' => $col->getLinha(),
                      '{SETOR}' => $col->getSetor(),
                      '{CARGO}' => $col->getPerfil()
                     );
        
        
        foreach($tags as $tag) {
            $texto = str_replace($tag, $valorTag[$tag], $texto);
        }
        
        return $texto;
    }
    
    
}