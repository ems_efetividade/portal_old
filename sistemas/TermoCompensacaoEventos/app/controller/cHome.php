<?php 

require_once('lib/appController.php');
require_once('app/model/mCadastro.php');
require_once('app/model/mColaboradorSetor.php');

class cHome extends appController {
    
    public $cadastro = null;
    private $maxRegPag;
    
    public function __construct() {
        $this->maxRegPag = 10;
        $this->cadastro = new mCadastro();
    }
    
    public function main() {
        $this->listar();
    }
    
    public function listar($pagina=0) {
        $this->cadastro->setIdSetorCadastro(fn_dadoSessao('id_setor'));
        
        
        $de = ((($pagina * $this->maxRegPag) - $this->maxRegPag) <= 0) ? 1 : (($pagina * $this->maxRegPag) - $this->maxRegPag)+1;
        $ate = ($pagina == 0) ? $this->maxRegPag : ($pagina * $this->maxRegPag);
        
        //echo $de . ' '.$ate;
        
        $this->set('paginacao', $this->paginacao($pagina));
        $this->set('cadastros', $this->cadastro->listar2($de, $ate));
        $this->set('links', $this->listarTermosAssinados());
        $this->render('vHome');
    }
    
    public function pesquisar($pagina=0) {
        $campo    = $_POST['cmbCampo'];
        $criterio = $_POST['txtCriterio'];
        
        //echo $campo.' '.$criterio;
        
        $this->cadastro->setIdSetorCadastro(fn_dadoSessao('id_setor'));
        
        
        $de = ((($pagina * $this->maxRegPag) - $this->maxRegPag) <= 0) ? 1 : (($pagina * $this->maxRegPag) - $this->maxRegPag)+1;
        $ate = ($pagina == 0) ? $this->maxRegPag : ($pagina * $this->maxRegPag);
        
        //echo $de . ' '.$ate;
        
        $this->set('paginacao', $this->paginacao($pagina));
        $this->set('cadastros', $this->cadastro->pesquisar($campo, $criterio));
        $this->render('vHome');
    }
    
    public function listarTermosAssinados() {
        $this->cadastro->setIdColaborador(fn_dadoSessao('id_colaborador'));
        $documentos = $this->cadastro->listarPorColaborador();
        
        $link = '';
        foreach($documentos as $doc) {
            $link .= '<p><a href="'.appConf::caminho.'cadastro/pdf/'.$doc->getIdCadastro().'">'.$doc->termo->getNome().' ('.fn_formatarData($doc->getDataCadastro()).')</a></p>';
        }
        echo $link;
    }
    
    public function mostrarTermo() {
        
        $this->cadastro->setIdColaborador(fn_dadoSessao('id_colaborador'));
        $this->cadastro->selecionarNaoAssinado();
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($this->cadastro->getIdColaborador());
        $col->selecionarPorColaborador();
        
        $texto = $this->cadastro->termo->getTexto();
        
        $texto = fn_substituirAlias('{NOME}',           $col->getColaborador(), $texto);
        $texto = fn_substituirAlias('{ID}',             $col->getId(), $texto);
        $texto = fn_substituirAlias('{CARGO}',          $col->getPerfil(), $texto);
        $texto = fn_substituirAlias('{SETOR}',          $col->getSetor(), $texto);
        $texto = fn_substituirAlias('{LINHA}',          $col->getLinha(), $texto);
        $texto = fn_substituirAlias('{EVENTO}',         $this->cadastro->getObservacao(), $texto);
        $texto = fn_substituirAlias('{DATA TRABALHADA}',fn_formatarData($this->cadastro->getDataTrabalhada()), $texto);
        $texto = fn_substituirAlias('{COMPENSACAO}',    fn_formatarData($this->cadastro->getDataCompensar()), $texto);
        

        $this->set('cadastro', $this->cadastro);
        $this->set('texto', $texto);
        $this->render('vTermo');

    }
    
    public function confirmar() {
        $this->cadastro->setIdCadastro($_POST['txtIdCadastro']);
        $this->cadastro->assinar();
        
        $this->salvarPDF($_POST['txtIdCadastro']);
        
        header('Location: '.appConf::redirecionar);
    }
    
    
    public function salvarPDF($idCadastro) {
        $this->cadastro->setIdCadastro($idCadastro);
        $this->cadastro->selecionar();
        
        $col = new mColaboradorSetor();
        $col->setIdColaborador($this->cadastro->getIdColaborador());
        $col->selecionarPorColaborador();
        
        $texto = $this->cadastro->termo->getTexto();
        
        $texto = fn_substituirAlias('{NOME}',           $col->getColaborador(), $texto);
        $texto = fn_substituirAlias('{ID}',             $col->getId(), $texto);
        $texto = fn_substituirAlias('{CARGO}',          $col->getPerfil(), $texto);
        $texto = fn_substituirAlias('{SETOR}',          $col->getSetor(), $texto);
        $texto = fn_substituirAlias('{LINHA}',          $col->getLinha(), $texto);
        $texto = fn_substituirAlias('{EVENTO}',         $this->cadastro->getObservacao(), $texto);
        $texto = fn_substituirAlias('{DATA TRABALHADA}',fn_formatarData($this->cadastro->getDataTrabalhada()), $texto);
        $texto = fn_substituirAlias('{COMPENSACAO}',    fn_formatarData($this->cadastro->getDataCompensar()), $texto);
        
        $this->set('cadastro', $this->cadastro);
        $this->set('termo', $texto);
        $html = $this->renderToString('vPDF');
        
        $nomeArquivo = str_replace(" ", "_",utf8_decode($col->getColaborador().'_'.$this->cadastro->termo->getNome().'_'.str_replace(" ", "_", str_replace(":", "_", $this->cadastro->getDataAssinatura())).'_'.$this->cadastro->getIdCadastro()));
        
        fn_gerarPDF($html, '', $nomeArquivo, appConf::pastaPDF);
    }
    
    public function paginacao($pagina=0) {
        $this->cadastro->setIdSetorCadastro(fn_dadoSessao('id_setor'));
        
        $total = $this->cadastro->getNumRows();
        $max = $this->maxRegPag;
        $paginas = ceil($total / $max);
        
        $pagina = ($pagina == 0) ? 1 : $pagina;
        
        $prox = $pagina + 1;
        $ant = $pagina - 1;
        
        $html = '<small><nav>
                    <ul class="pager">
                        <li><a href="'.appConf::caminho.'home/listar/1">Primeiro</a></li>
                        <li><a href="'.appConf::caminho.'home/listar/'.$ant.'">Anterior</a></li>
                        <li><small>'.$pagina.'/'.$paginas.'</small></li>
                        <li><a href="'.appConf::caminho.'home/listar/'.$prox.'">Próximo</a></li>
                        <li><a href="'.appConf::caminho.'home/listar/'.$paginas.'">Último</a></li>
                    </ul>
                </nav></small>';
        
        return $html;
        
    }
    
    
    
    
    
    
    
}