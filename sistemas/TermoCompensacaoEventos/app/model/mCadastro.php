<?php

require_once('lib/appConexao.php');
require_once('app/model/mTermo.php');
require_once('app/model/mPeriodo.php');

require_once('app/model/mColaboradorSetor.php');

class mCadastro extends appConexao {
    
    private $idCadastro;
    private $idPeriodoCompesar;
    private $idPeriodoTrabalhado;
    private $idTermo;
    private $dataTrabalhada;
    private $dataCompensar;
    private $idSetor;
    private $idColaborador;
    private $idSetorCadastro;
    private $idColaboradorCadastro;
    private $observacao;
    private $dataCadastro;
    private $ativo;
    private $assinado;
    private $dataAssinatura;
    private $numReg;
    
    private $colabTermo;
    private $colabCadastro;
    
    public $termo;
    public $perTrabalhado;
    public $perCompensar;
    public $colaborador;
    
    public function __construct() {
        $this->idCadastro            = 0;
        $this->idPeriodoCompesar     = 0;
        $this->idPeriodoTrabalhado   = 0;
        $this->idTermo               = 0;
        $this->dataTrabalhada        = '';
        $this->dataCompensar         = '';         
        $this->ativo                 = 0;
        $this->idSetor               = 0;
        $this->idColaborador         = 0;        
        $this->idSetorCadastro       = 0;
        $this->observacao            = '';
        $this->idColaboradorCadastro = 0;
        $this->dataCadastro          = '';
        $this->dataAssinatura = '';
        $this->assinado = 0;
        
        $this->numReg = 0;
        
        
        $this->termo = new mTermo();

        
        $this->perCompensar = new mPeriodo();
        $this->perTrabalhado = new mPeriodo();
        $this->colaborador = new mColaboradorSetor();
    }
    
    public function setIdCadastro($valor) {
        $this->idCadastro = $valor;
    }
    
    public function setIdTermo($valor) {
        $this->idTermo = $valor;
    }
    
    public function setIdPeriodoCompensar($valor) {
        $this->idPeriodoCompesar = $valor;
    }
    
    public function setIdPeriodoTrabalhado($valor) {
        $this->idPeriodoTrabalhado = $valor;
    }    
    
    public function setDataTrabalhada($valor) {
        $this->dataTrabalhada = $valor;
    }
    
    public function setDataCompensar($valor) {
        $this->dataCompensar = $valor;
    }
    
    public function setIdSetorCadastro($valor) {
        $this->idSetorCadastro = $valor;
    }
    
    public function setIdColaboradorCadastro($valor) {
        $this->idColaboradorCadastro = $valor;
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    } 
    
    public function setObservacao($valor) {
        $this->observacao = $valor;
    }     
    
    public function setAtivo($valor) {
        $this->ativo = $valor;
    }
    
    public function setCompensacao($valor) {
        $this->arrCompensacao[] = $valor;
    }
    
    
    public function getIdCadastro() {
        return $this->idCadastro;
    }
    
    public function getIdTermo() {
        return $this->idTermo;
    }
    
    public function getIdPeriodoCompensar() {
        return $this->idPeriodoCompesar;
    }
    
    public function getIdPeriodoTrabalhado() {
        return $this->idPeriodoTrabalhado;
    }    
    
    public function getDataTrabalhada() {
        return $this->dataTrabalhada;
    }
    
    public function getDataCompensar() {
        return $this->dataCompensar;
    }
    
    public function getIdSetorCadastro() {
        return $this->idSetorCadastro;
    }
    
    public function getIdColaboradorCadastro() {
        return $this->idColaboradorCadastro;
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    } 
    
    public function getObservacao() {
        return $this->observacao;
    }     
    
    public function getAtivo() {
        return $this->ativo;
    }  
    
    public function getAssinado() {
        return $this->assinado;
    }
    
    public function getDataCadastro() {
        return $this->dataCadastro;
    }
    
    public function getColaborador() {
        return $this->colabTermo;
    }
    
    public function getDataAssinatura() {
        return $this->dataAssinatura;
    }
    
    public function getNumRows() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) as ROWS FROM TC_CADASTRO WHERE ID_SETOR_CADASTRO = ".$this->idSetorCadastro);
        return $rs[1]['ROWS'];
    }
    
    public function salvar() {
        $validar = $this->validar();
        if($validar == "") {
        $rs = $this->executarQueryArray("EXEC proc_tc_cadastroSalvar  ".$this->idCadastro.",
                                                                      ".$this->idPeriodoCompesar.",
                                                                     ".$this->idPeriodoTrabalhado.",
                                                                    ".$this->idTermo.",
                                                                    '".$this->dataTrabalhada."',
                                                                    '".$this->dataCompensar."',
                                                                    ".$this->idSetor.",
                                                                    ".$this->idColaborador.",                                                                        
                                                                    ".$this->idSetorCadastro.",
                                                                    ".$this->idColaboradorCadastro.",
                                                                    '".$this->observacao."',
                                                                    ".$this->ativo."");
        
        } else {
            return $validar;
        }
      
        
    }
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("EXEC proc_tc_cadastroSelecionar '".$this->idCadastro."'"));
    }
    
    public function selecionarAssinar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_CADASTRO FROM TC_CADASTRO WHERE ID_COLABORADOR = ".$this->idColaborador));
    }
    
    public function excluir() {
        $this->executar("EXEC proc_tc_cadastroExcluir '".$this->idCadastro."'");
    }
    
    public function listar2($de=1, $ate=20) {

        $rs = $this->executarQueryArray("SELECT A.ID_CADASTRO FROM (
                                        SELECT 
                                                ROW_NUMBER() OVER (order BY ID_CADASTRO) AS LIMIT, 
                                                ID_CADASTRO FROM TC_CADASTRO 
                                        WHERE ID_SETOR_CADASTRO = ".$this->idSetorCadastro."

                                        ) A WHERE A.LIMIT BETWEEN ".$de." AND ".$ate."");
        
        $cadastros = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $cadastro = new mCadastro();
                $cadastro->setIdCadastro($row['ID_CADASTRO']);
                $cadastro->selecionar();
                
                $cadastros[] = $cadastro;
                    
            }
        }
        
        return $cadastros;
    }
    
    public function listarPorColaborador() {
        $rs = $this->executarQueryArray("SELECT ID_CADASTRO FROM TC_CADASTRO WHERE ID_COLABORADOR = ".$this->idColaborador);
        $cadastros = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $cadastro = new mCadastro();
                $cadastro->setIdCadastro($row['ID_CADASTRO']);
                $cadastro->selecionar();
                
                $cadastros[] = $cadastro;
                    
            }
        }
        
        return $cadastros;
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("EXEC proc_tc_cadastroListar '".$this->idSetorCadastro."'");
        $cadastros = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $cadastro = new mCadastro();
                $cadastro->setIdCadastro($row['ID_CADASTRO']);
                $cadastro->selecionar();
                
                $cadastros[] = $cadastro;
                    
            }
        }
        
        return $cadastros;
    }
    
    public function pesquisar($campo, $criterio) {
        
        $filtro['SETOR'] = "AND S.SETOR = '".$criterio."'";
        $filtro['NOME']  = "AND C.NOME LIKE '%".$criterio."%'";
        
        $query = "SELECT 
                ROW_NUMBER() OVER (order BY TC.ID_CADASTRO) AS LIMIT, 
                C.NOME,
                S.SETOR,
                TC.ID_CADASTRO FROM TC_CADASTRO TC
                INNER JOIN COLABORADOR C ON C.ID_COLABORADOR = TC.ID_COLABORADOR
                INNER JOIN SETOR S ON S.ID_SETOR = TC.ID_SETOR
        WHERE ID_SETOR_CADASTRO = ".$this->idSetorCadastro." ".$filtro[$campo]."";
        
        $rs = $this->executarQueryArray($query);
        $cadastros = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $cadastro = new mCadastro();
                $cadastro->setIdCadastro($row['ID_CADASTRO']);
                $cadastro->selecionar();
                
                $cadastros[] = $cadastro;
                    
            }
        }
        
        return $cadastros;
        
    }
    
    public function assinar() {
        $this->executar("UPDATE TC_CADASTRO SET ASSINADO = 1, DATA_ASSINATURA = GETDATE() WHERE ID_CADASTRO = ".$this->idCadastro);
    }
    
    public function selecionarNaoAssinado() {
        $rs = $this->executarQueryArray("SELECT TOP 1 * FROM TC_CADASTRO WHERE ID_COLABORADOR = ".$this->idColaborador." AND ASSINADO = 0");
        $this->popularVariaveis($rs);
//        $cadastros = array();
//        
//        if (count($rs) > 0) {
//            
//            foreach($rs as $row) {
//                $cadastro = new mCadastro();
//                $cadastro->setIdCadastro($row['ID_CADASTRO']);
//                $cadastro->selecionar();
//                
//                $cadastros[] = $cadastro;
//                    
//            }
//        }
//        
//        return $cadastros;
    }
    
    public function verificarTermo() {
        $rs = $this->executarQueryArray("SELECT COUNT(*) AS EXISTE FROM TC_CADASTRO WHERE ID_COLABORADOR = ".$this->idColaborador." AND ASSINADO = 0");
        return $rs[1]['EXISTE'];
    }
    
    private function validar() {
        $msg = '';
        
        if($this->idTermo == '') {
            $msg = 'Por favor, selecione o termo.';
            return $msg;
        }
        
        if($this->idColaborador == '') {
            $msg = 'Por favor, selecione o colaborador.';
            return $msg;
        }
        
        if($this->dataTrabalhada == '' || $this->idPeriodoTrabalhado == '') {
            $msg = 'Por favor, digite a data trabalhada e o período trabalhdo.';
            return $msg;
        }
        
        if($this->dataCompensar == '' || $this->idPeriodoCompesar == '') {
            $msg = 'Por favor, digite a data trabalhada e o período trabalhdo.';
            return $msg;
        }      
        
        $semanaDataCompensar = date("w", strtotime($this->dataCompensar));
        if($semanaDataCompensar == 0 || $semanaDataCompensar == 6) {
            $msg = '"Data a compensar" não pode ser um fim de semana.';
            return $msg;
        }
            
            
        $dtTrab = strtotime($this->dataTrabalhada);
        $dtComp = strtotime($this->dataCompensar);
        
        if($dtComp <= $dtTrab) {
            $msg = 'A data a compensar não pode ser menor ou igual a data trabalhada.';
            return $msg;
        }
        
        
        
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {           
            
            
            $this->idCadastro            = $rs[1]['ID_CADASTRO'];
            $this->idPeriodoCompesar     = $rs[1]['ID_PERIODO_COMPENSAR'];
            $this->idPeriodoTrabalhado   = $rs[1]['ID_PERIODO_TRABALHADO'];
            $this->idTermo               = $rs[1]['ID_TERMO'];
            $this->dataTrabalhada        = $rs[1]['DATA_TRABALHADA'];
            $this->dataCompensar         = $rs[1]['DATA_COMPENSAR'];          
            $this->ativo                 = $rs[1]['ATIVO'];
            $this->idSetor               = $rs[1]['ID_SETOR'];
            $this->idColaborador         = $rs[1]['ID_COLABORADOR'];       
            $this->idSetorCadastro       = $rs[1]['ID_SETOR_CADASTRO'];
            $this->idColaboradorCadastro = $rs[1]['ID_COLABORADOR_CADASTRO'];
            $this->observacao            = $rs[1]['OBSERVACAO'];
            $this->dataCadastro          = $rs[1]['DATA_CADASTRO'];
            $this->dataAssinatura        = $rs[1]['DATA_ASSINATURA'];
            $this->assinado                 = $rs[1]['ASSINADO'];
            
            
            
            $this->colaborador->setIdSetor($this->idSetor);
            $this->colaborador->selecionar();
            $this->colabTermo = $this->colaborador->getSetor().' ';
            
            $this->colaborador->setIdColaborador($this->idColaborador);
            $this->colaborador->selecionarPorColaborador();
            $this->colabTermo .=  $this->colaborador->getColaborador();
            
            $this->termo->setIdTermo($this->idTermo);
            $this->termo->selecionar();

            $this->perCompensar->setIdPeriodo($this->idPeriodoCompesar);
            $this->perCompensar->selecionar();
            
            $this->perTrabalhado->setIdPeriodo($this->idPeriodoTrabalhado);
            $this->perTrabalhado->selecionar();
        }
    }
    
}