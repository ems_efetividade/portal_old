<?php

require_once('lib/appConexao.php');

class mColaboradorSetor extends appConexao {
    
    private $idSetor;
    private $idColaborador;
    private $nivel;
    private $nivelPerfil;
    private $setor;
    private $colaborador;
    private $perfil;
    private $linha;
    private $foneCorp;
    private $fonePart;
    private $email;
    private $id;
    private $foto;
    
    public function __construct() {
        $this->idSetor = 0;
        $this->idColaborador = 0;
        $this->nivel = 0;
        $this->nivelPerfil = 0;
        $this->setor = '';
        $this->colaborador = '';
        $this->perfil = '';
        $this->linha = '';
        $this->foneCorp = '';
        $this->fonePart = '';
        $this->email = '';
        $this->foto = '';
        $this->id = '';
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setIdColaborador($valor=0) {
        $this->idColaborador = $valor;
    }
    
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }
    
    public function getNivel() {
        return $this->nivel;
    }
    
    public function getNivelPerfil() {
        return $this->nivelPerfil;
    }
    
    public function getSetor() {
        return $this->setor;
    }
    
    public function getColaborador() {
        return $this->colaborador;
    }
    
    public function getPerfil() {
        return $this->perfil;
    }
    
    public function getLinha() {
        return $this->linha;
    }
    
    public function getFoneCorp() {
        return $this->foneCorp;
    }
    
    public function getFonePart() {
        return $this->fonePart;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function getFoto() {
        return $this->foto;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_SETOR, ID_COLABORADOR, NIVEL, NIVEL_PERFIL, SETOR, NOME, PERFIL, LINHA, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, FOTO, ID FROM vw_colaboradorSetor WHERE ID_SETOR = ".($this->idSetor+0)));
    }
    
    public function selecionarPorColaborador() {
        $this->popularVariaveis($this->executarQueryArray("SELECT ID_SETOR, ID_COLABORADOR, NIVEL, NIVEL_PERFIL, SETOR, NOME, PERFIL, LINHA, FONE_CORPORATIVO, FONE_PARTICULAR, EMAIL, FOTO, ID FROM vw_colaboradorSetor WHERE ID_COLABORADOR = ".($this->idColaborador+0)));
    }
    
    public function selecionarEquipe() {
        $rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = ".$this->idSetor);
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }
    
    public function listarGerentes($perfil) {
        $rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL_PERFIL = '".$perfil."' ORDER BY SETOR ASC");
        
        if(count($rs) > 0) {
            
          $arr = array();
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              $arr[] = $setor;
          }
          
          return $arr;
            
        }
    }
    
    public function listarEquipe() {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = '".$idSetor."' ORDER BY SETOR ASC");
//        $rs = $this->executarQueryArray("WITH GRUPO AS (
//        SELECT ID_SETOR, NIVEL, SETOR FROM vw_colaboradorSetor WHERE ID_SETOR = ".$this->idSetor."
//        UNION ALL
//        SELECT V.ID_SETOR, V.NIVEL, V.SETOR FROM vw_colaboradorSetor V inner join GRUPO G ON G.ID_SETOR = V.NIVEL
//        )
//
//        SELECT ID_SETOR FROM GRUPO WHERE ID_SETOR <> ".$this->idSetor." ORDER BY SETOR ASC");
//        
        
        
        $query = 'WITH GRUPO AS (
                        SELECT ID_SETOR, NIVEL, SETOR FROM vw_colaboradorSetor WHERE NIVEL = '.$this->idSetor.'
                        UNION ALL
                        SELECT A.ID_SETOR, A.NIVEL, A.SETOR FROM vw_colaboradorSetor A INNER JOIN GRUPO B ON A.NIVEL = B.ID_SETOR
                )

                SELECT ID_SETOR FROM GRUPO ORDER BY SETOR ASC';
        
        
        $query = "SELECT ID_SETOR FROM vw_colaboradorSetor WHERE NIVEL = ".$this->idSetor;
        
        $rs = $this->executarQueryArray($query);
        
        
         $arr = array();
        if(count($rs) > 0) {
            
         
          
          foreach($rs as $row) {
              $setor = new mColaboradorSetor();
              $setor->setIdSetor($row['ID_SETOR']);
              $setor->selecionar();
              
              
              $arr[] = $setor;
          }
          
          
            
        }
        return $arr;
    }
    
    public function comboGerentes($perfil, $selecionar=0, $nomeCombo='cmbGerente') {
        //$rs = $this->executarQueryArray("SELECT ID_SETOR FROM vw_colaboradorSetor WHERE PERFIL = '".$perfil."' ORDER BY SETOR ASC");
        $rs = $this->listarGerentes($perfil);
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $colab) {
            
            $marcar = ($colab->getIdColaborador()."|".$colab->getIdSetor() == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$colab->getIdColaborador()."|".$colab->getIdSetor().'" '.$marcar.'>'.$colab->getSetor().' '.$colab->getColaborador().'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function comboEquipe($selecionar=0, $nomeCombo='cmbEquipe') {
        $rs = $this->listarEquipe();
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($rs as $colab) {
            
            $marcar = ($colab->getIdSetor()."|".$colab->getIdColaborador() == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$colab->getIdSetor()."|".$colab->getIdColaborador().'" '.$marcar.'>'.$colab->getSetor().' '.$colab->getColaborador().'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    public function comboPerfil($selecionar=0, $nomeCombo='cmbPerfil') {
        $rs = $this->executarQueryArray("SELECT * FROM PERFIL WHERE ID_PERFIL <= 4 and NIVEL > ".appFunction::dadoSessao('perfil')." ORDER BY ID_PERFIL DESC");
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option value="0">:: PERFIL (TODOS) ::</option>';
        
        foreach($rs as $row) {
            
            $marcar = ($row['ID_PERFIL'] == $selecionar) ? 'selected' : '';
            
            $option .= '<option value="'.$row['ID_PERFIL'].'" '.$marcar.'>'.$row['PERFIL'].'</option>';
        }
        
        $select .= $option.'</select>';
        
        return $select;
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idSetor       = $rs[1]['ID_SETOR'];
            $this->idColaborador = $rs[1]['ID_COLABORADOR'];
            $this->nivel         = $rs[1]['NIVEL'];
            $this->nivelPerfil   = $rs[1]['NIVEL_PERFIL'];
            $this->setor         = $rs[1]['SETOR'];
            $this->colaborador   = $rs[1]['NOME'];
            $this->perfil        = $rs[1]['PERFIL'];
            $this->linha         = $rs[1]['LINHA'];
            $this->foneCorp      = $rs[1]['FONE_CORPORATIVO'];
            $this->fonePart      = $rs[1]['FONE_PARTICULAR'];
            $this->email         = $rs[1]['EMAIL'];
            $this->foto          = $rs[1]['FOTO'];
            $this->id            = $rs[1]['ID'];
        }
    }
    
}