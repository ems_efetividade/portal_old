<?php

require_once('lib/appConexao.php');
require_once('app/model/mColaboradorSetor.php');

class mCompensacao extends appConexao {
    
    private $idCadastro;
    private $idColaborador;
    private $idSetor;
    private $aceite;
    private $dataAceite;
    
    public $colaborador;
    
    public function __construct() {
        $this->idCadastro    = 0;
        $this->idColaborador = 0;
        $this->idSetor       = 0;
        $this->aceite        = 0;
        $this->dataAceite    = '';
        
        $this->colaborador = new mColaboradorSetor();
    }
    
    public function setIdCadastro($valor) {
        $this->idCadastro = $valor;
    }
    
    public function setIdColaborador($valor) {
        $this->idColaborador = $valor;
    }
    
    public function setIdSetor($valor) {
        $this->idSetor = $valor;
    }
    
    public function setAceite($valor) {
        $this->aceite = $valor;
    }
    
    public function setDataAceite($valor) {
        $this->dataAceite = $valor;
    }
    
    
    
    public function getIdCadastro() {
        return $this->idCadastro;
    }
    
    public function getIdColaborador() {
        return $this->idColaborador;
    }
    
    public function getIdSetor() {
        return $this->idSetor;
    }
    
    public function getAceite() {
        return $this->aceite;
    }
    
    public function getDataAceite() {
        return $this->dataAceite;
    }    
    
    
    public function selecionar() {
        $rs = $this->executarQueryArray("SELECT * FROM TC_COMPENSACAO WHERE ID_CADASTRO = ".$this->idCadastro);
        $this->popularVariaveis($rs);
    }
    
    public function salvar() {
        $this->executar("INSERT INTO [TC_COMPENSACAO]
           ([ID_CADASTRO]
           ,[ID_COLABORADOR]
           ,[ID_SETOR])
     VALUES
           (".$this->idCadastro."
           ,".$this->idColaborador."
           ,".$this->idSetor.")");
    }
    
    private function popularVariaveis($rs) {
        
        $this->idCadastro    = $rs[1]['ID_CADASTRO'];
        $this->idColaborador = $rs[1]['ID_COLABORADOR'];
        $this->idSetor       = $rs[1]['ID_SETOR'];
        $this->aceite        = $rs[1]['ACEITE'];
        $this->dataAceite    = $rs[1]['DATA_ACEITE'];

        $this->colaborador->setIdColaborador($this->idColaborador);
        $this->colaborador->selecionarPorColaborador();
    }
    
}