<?php

require_once('lib/appConexao.php');


class mPeriodo extends appConexao {
    
    private $idPeriodo;
    private $periodo;
    
    
    public function __construct() {
        $this->idPeriodo = 0;
        $this->periodo = '';
    }
    
    public function setIdPeriodo($valor) {
        $this->idPeriodo = $valor;
    }
    
    public function setPeriodo($valor) {
        $this->periodo = $valor;
    }
    
    public function getIdPeriodo() {
        return $this->idPeriodo;
    }
    
    public function getPeriodo() {
        return $this->periodo;
    }
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("EXEC proc_tc_periodoSelecionar '".$this->idPeriodo."'"));
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("EXEC proc_tc_periodoListar");
        $periodos = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $periodo = new mPeriodo();
                $periodo->setIdPeriodo($row['ID_PERIODO']);
                $periodo->selecionar();
                
                $periodos[] = $periodo;
                    
            }
        }
        
        return $periodos;
    }
    
    public function comboPeriodo($sel=0, $nomeCombo='cmbPeriodo') {
        $periodos = $this->listar();
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($periodos as $periodo) {
            $selected = ($sel == $periodo->getIdPeriodo()) ? "selected" : '';
            $option .= '<option value="'.$periodo->getIdPeriodo().'" '.$selected.'>'.$periodo->getPeriodo().'</option>';
        }
        
        return $select.$option.'</select>';
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idPeriodo = $rs[1]['ID_PERIODO'];
            $this->periodo   = $rs[1]['PERIODO'];
        }
    }
    
}