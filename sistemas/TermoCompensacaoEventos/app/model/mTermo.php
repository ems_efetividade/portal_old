<?php

require_once('lib/appConexao.php');


class mTermo extends appConexao {
    
    private $idTermo;
    private $nome;
    private $arquivo;
    private $ativo;
    private $texto;
    
    public function __construct() {
        $this->idTermo = 0;
        $this->nome = '';
        $this->arquivo = '';
        $this->ativo = 0;
    }
    
    public function setIdTermo($valor) {
        $this->idTermo = $valor;
    }
    
    public function setNome($valor) {
        $this->nome = $valor;
    }
    
    public function setArquivo($valor) {
        $this->arquivo = $valor;
    }
    
    public function setAtivo($valor) {
        $this->ativo = $valor;
    }
    
    
    public function getIdTermo() {
        return $this->idTermo;
    }
    
    public function getNome() {
        return $this->nome;
    }
    
    public function getArquivo() {
        return $this->arquivo;
    }
    
    public function getAtivo() {
        return $this->ativo;
    }
    
    public function getTexto() {
        return $this->texto;
    }
    
    
    public function selecionar() {
        $this->popularVariaveis($this->executarQueryArray("EXEC proc_tc_termoSelecionar '".$this->idTermo."'"));
    }
    
    public function listar() {
        $rs = $this->executarQueryArray("EXEC proc_tc_termoListar");
        $termos = array();
        
        if (count($rs) > 0) {
            
            foreach($rs as $row) {
                $termo = new mTermo();
                $termo->setIdTermo($row['ID_TERMO']);
                $termo->selecionar();
                
                $termos[] = $termo;
                    
            }
        }
        
        return $termos;
    }
    
    public function comboTermo($sel=0, $nomeCombo='cmbTermo') {
        $termos = $this->listar();
        
        $select = '<select class="form-control input-sm" name="'.$nomeCombo.'" id="'.$nomeCombo.'">';
        $option = '<option></option>';
        
        foreach($termos as $termo) {
            
            $selected = ($sel == $termo->getIdTermo()) ? "selected" : '';
            
            $option .= '<option value="'.$termo->getIdTermo().'" '.$selected.' >'.$termo->getNome().'</option>';
        }
        
        return $select.$option.'</select>';
    }
    
    private function popularVariaveis($rs) {
        if(count($rs) > 0) {
            $this->idTermo  = $rs[1]['ID_TERMO'];
            $this->nome     = $rs[1]['NOME'];
            $this->arquivo  = $rs[1]['ARQUIVO'];
            $this->ativo    = $rs[1]['ATIVO'];
            
            
            
            
            if($rs[1]['ARQUIVO'] != '') {
                
                
                $file = fopen(appConf::caminhoFisico.'public\\files\\'.$rs[1]['ARQUIVO'],"r");
                while(! feof($file)){
                    $linha = fgets($file);
                    if(trim($linha) != "") {
                        $this->texto .= "<p>".utf8_encode($linha). "</p>";
                    }
                }
                fclose($file);
                 
            }
            
            
        }
    }
    
    
    
}