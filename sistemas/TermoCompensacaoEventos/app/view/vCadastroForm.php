<script>

$(document).ready(function(e) {
    
    $('#btnAddSetor').click(function(e) {
        id = $('#cmbEquipe').val();
        texto = $('#cmbEquipe option:selected').text();
        
        tr = '<tr><td><input type="hidden" name="txtSetor[]" value ="'+id+'" />'+ texto + '</td><td><button type="button" class="btn btn-xs btn-danger pull-right">Excluir</button></td></tr>';
        
        $('#tbPermitido').append(tr);
        
    });
    
    
//    $('#btnSalvarCadastro').click(function(e) {
//        $('#formSalvar').submit();
//    });
    
});

</script>


<style>
    label { font-size: 11px; }
</style>

<form id="formSalvar" action="<?php print appConf::caminho ?>cadastro/salvar" method="post">
    <input type="hidden" name="txtIdCadastro" value="<?php echo $cadastro->getIdCadastro(); ?>" />
<div class="row">
    
    
    <div class="col-lg-12">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Termo/Documento</label>
            <?php echo $termo->comboTermo($cadastro->getIdTermo()); ?>
        </div>  
    </div>    
    
    <div class="col-lg-12">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Colaborador</label>
            <?php  echo $colaborador->comboEquipe($cadastro->getIdSetor()."|".$cadastro->getIdColaborador()); ?>
        </div>  
    </div>  

    <div class="col-lg-12">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Nome do Evento</label>
            <textarea class="form-control" rows="1" name="txtObservacao"><?php echo $cadastro->getObservacao() ?></textarea>
        </div>  
    </div> 
    
    <div class="col-lg-3">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Data Trabalhada</label>
            <input type="text" class="form-control maskData" name="txtDataTrabalhada" value="<?php echo fn_formatarData($cadastro->getDataTrabalhada()) ?>">
        </div>  
    </div>
    
    <div class="col-lg-9">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Período Trabalhado</label>
            <?php echo $periodo->comboPeriodo($cadastro->getIdPeriodoTrabalhado(), 'cmbPeriodoTrabalhado'); ?>
        </div>  
    </div>       
    
    <div class="col-lg-3">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Data a Compensar</label>
            <input type="text" class="form-control maskData" name="txtDataCompensar" value="<?php echo fn_formatarData($cadastro->getDataCompensar()) ?>">
        </div>  
    </div>   
    
    <div class="col-lg-9">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Período Compensar</label>
            <?php echo $periodo->comboPeriodo($cadastro->getIdPeriodoCompensar(), 'cmbPeriodoCompensar'); ?>
        </div>  
    </div>      
<!--    
    <div class="col-lg-3">
        <div class="form-group form-group-sm">
            <label for="exampleInputEmail1">Data Expirar Termo</label>
            <input type="text" class="form-control maskData" name="txtDataExpirar" value="">
        </div>  
    </div>       
    -->
  
    

    

</div>

<!--<h5>Colaboradores</h5>
<div class="row">
    <div class="col-lg-12">
        
        <div class="input-group">
      <?php  echo $colaborador->comboEquipe(); ?>
      <span class="input-group-btn">
        <button class="btn btn-default btn-sm" type="button" id="btnAddSetor">Adicionar</button>
      </span>
    </div>
        
      
        
    </div>
    
    <div class="col-lg-3">
        <button type="button" class="btn btn-xs btn-success pull-right" data-toggle="modal" data-target="#modalCadastro">Adicionar</button>
    </div>
    
    <div class="col-lg-12">
        <div style="overflow: auto; max-height: 200px">
        <br>
        <small>
        <table id="tbPermitido" class="table table-condensed table-striped">
            <tr>
                <th>Setor</th>
                <th>Ação</th>
            </tr>
        </table>
            </small>
        </div>
    </div>
</div>-->
    
</form>