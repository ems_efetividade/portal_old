<?php
    require_once('header.php');
?>

<script>
$(document).ready(function(e) { 
    
    $('#modalCadastro').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = (button.data('id') === undefined) ? 0 : parseInt(button.data('id')); // Extract info from data-* attributes
        formulario = requisicaoAjax('<?php print appConf::caminho ?>cadastro/abrirFormCadastro/'+id);
        $('#modalCadastro .modal-body').html(formulario); 
        
            $('#btnSalvarCadastro').unbind().click(function(e) {
                $.ajax({
                    type: "POST",
                    url: $('#formSalvar').attr('action'),
                    data: new FormData($('#formSalvar')[0]),
                    contentType: false,
                    processData: false,
                    success: function(retorno) {
                       
                       modalAguarde('hide');
                        if(retorno != '') {
                           //alert(retorno); 
                           msgBox(retorno);
                        } else {
                            location.reload();
                        }
                    },
                    beforeSend: function (a) {
                       //modalAguarde('show');//$('#comboCidade').html('<select class="form-control"><option>Processando...</option></select>');
                    }
                });
            });
    });
    
    $('#modalExcluirCadastro').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = (button.data('id') === undefined) ? 0 : parseInt(button.data('id')); // Extract info from data-* attributes
        
        
        $('#btnExcluirCadastro').click(function(e) {
            location.href = '<?php print appConf::caminho ?>cadastro/excluir/'+id;
        });
        
        
    });     
    
    $('#modalVisualizarCadastro').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var id = (button.data('id') === undefined) ? 0 : parseInt(button.data('id')); // Extract info from data-* attributes
        
        formulario = requisicaoAjax('<?php print appConf::caminho ?>cadastro/visualizar/'+id);

        $('#modalVisualizarCadastro .modal-body').html(formulario); 
    });    
    
    
    $('#btnPesquisar').click(function(e) {
        campo = $('')
    });
    
});
</script>

<div class="container">
    
    
    
    <?php echo $links; ?>
    
    
    <div class="row">
        <div class="col-lg-7">
            <h3>Cadastro de termos</h3>
        </div>
        <div class="col-lg-5 text-right">
            <br><button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalCadastro" data-id="0"><span class="glyphicon glyphicon-plus"></span>  Adicionar</button>
        </div>
    </div>
    
    <hr>
    
    <form action="<?php echo appConf::caminho ?>home/pesquisar" method="post">
    <div class="row">
        <div class="col-lg-4"></div>
        
        <div class="col-lg-2">
            <div class="form-group form-group-sm">
            <select class="form-control" name="cmbCampo">
                    <option>Pesquisar Por</option>
                    <option value="SETOR">SETOR</option>
                    <option value="NOME">NOME</option>
                    <option value="DATA_TRABALHADA">DATA TRABALHADA</option>
                    <option value="DATA_COMPENSADA">DATA COMPENSADA</option>
                  </select>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="input-group input-group-sm">
                
              <input type="text" class="form-control" name="txtCriterio" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-default" type="submit" id="btnPesquisar">Pesquisar</button>
              </span>
            </div><!-- /input-group -->
        </div>
    </div>
    </form>
    <br>
    <small>
    <table class="table table-cosndensed table-striped">
        <tr>
            <th>Data de Cadastro</th>
            <th>Termo/Documento</th>
            <th>Colaborador</th>
            <th>Data Trabalhada</th>
            <th>Data Compensar</th>
<!--            <th>Periodo</th>-->
            <th>Assinado?</th>
            <th>Data Assinatura</th>
            <th>&nbsp;</th>
               
        </tr>
        <?php foreach($cadastros as $cadastro) {  ?>
        <tr>
            <td><?php echo fn_formatarData($cadastro->getDataCadastro()) ?></td>
            <td><?php echo $cadastro->termo->getNome() ?></td>
            <td><?php echo $cadastro->getColaborador() ?></td>
            <td><?php echo fn_formatarData($cadastro->getDataTrabalhada())  ?></td>
            <td><?php echo fn_formatarData($cadastro->getDataCompensar()) ?></td>
            <td>
                <?php echo ($cadastro->getAssinado() == 0) ? '<span class="label label-danger">NAO</span>' : '<span class="label label-success">SIM</span>'  ?>
            </td>
            <td><?php echo fn_formatarData($cadastro->getDataAssinatura()) ?></td>
            <td class="text-right">
                <?php if($cadastro->getAssinado() == 0) { ?> 
                <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#modalCadastro" data-id="<?php echo $cadastro->getIdCadastro() ?>"><span class="glyphicon glyphicon-pencil"></span></button>
                <button type="button" class="btn btn-xs btn-danger" data-toggle="modal" data-target="#modalExcluirCadastro" data-id="<?php echo $cadastro->getIdCadastro() ?>"><span class="glyphicon glyphicon-remove"></span></button>
                <?php }  else { ?>
                    <a href="<?php echo appConf::caminho ?>cadastro/pdf/<?php echo $cadastro->getIdCadastro() ?>" class="btn btn-xs btn-primary"><span class="glyphicon glyphicon-file"></span></a>
                <?php } ?>
                <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#modalVisualizarCadastro" data-id="<?php echo $cadastro->getIdCadastro() ?>"><span class="glyphicon glyphicon-eye-open"></span></button>
            </td>
        </tr>
        <?php } ?>
    </table>
    </small>
    
    <br>
    
    <?php print $paginacao; ?>
</div> 



<div class="modal fade" id="modalCadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-plus"></span> Adicionar/Editar Termo</p>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
          <button type="button" class="btn btn-default  btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary  btn-sm" id="btnSalvarCadastro"><span class="glyphicon glyphicon-ok"></span> Salvar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modalExcluirCadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-danger">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Cadastro</p>
      </div>
      <div class="modal-body">
          <h4>Deseja excluir esse cadastro?</h4>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default  btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-danger btn-sm" id="btnExcluirCadastro"><span class="glyphicon glyphicon-trash"></span> Excluir</button>
      </div>
    </div>
  </div>
</div>


<div class="modal fade" id="modalVisualizarCadastro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header btn-info">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <p class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-trash"></span> Excluir Cadastro</p>
      </div>
      <div class="modal-body">
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default  btn-sm" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
      </div>
    </div>
  </div>
</div>