<link href="<?php echo appConf::caminho ?>plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<style>
    .bold { font-weight: bold; }
    b {font-weight: bold;}
    p {margin-bottom: 80px;margin-left:20px; margin-right:20px; line-height: 200%}
</style>
    
<div class="panel panel-default">
    <div class="panel-body" style="height: 1200px;">
        <img class="pull-right" src="public/img/ems.png" />
        <br>
        <br>
        <br>
        <br>
        <br>
        <h4 class="bold text-center"><b><?php echo $cadastro->termo->getNome() ?></b></h4>
        <br>
        <br>
        <br>
        <br>
        <div class="text-justify"><?php echo $termo; ?></div>
        <br><br><br>
        
        <div class="alert alert-success" role="alert">
            <div class="text-center"><b>Data da assinatura: <?php print fn_formatarData($cadastro->getDataAssinatura()) ?></b></div>
      </div>
        
    </div>
</div>