<?php include('header.php');?>

<style>
   
    p {
        font-size: 14px;
    }
    
    .doc-texto {
        misn-height: 325px;
        overflow: auto;
        
    }
    
    .doc-texto p {
        line-height: 170%;
    }

ul {
	list-style-type:none;
} 


li {
        text-align: justify;    
        
    }
li ul {
    
    list-style:none;
}

.list-style {
    list-style:disc;
}
table {
    margin-top:20px;
    margin-bottom:20px;
    width: 100%;
}
    table td {
        border-collapse: collapse;
        padding:8px;
        border:1px solid #ddd;
    }
</style>

<script>
    
function redimensionarDiv() {
    vph = $(window).innerHeight() - 325;
    $('.doc-texto').css({'height': vph + 'px'});
}    
function modalAguarde(ctrl) {
    $('#modalAguarde').modal(ctrl);

    $('#modalAguarde').on('shown.bs.modal', function (e) {
      $('body').css('padding', '0px');
    });

    $('#modalAguarde').on('hidden.bs.modal', function (e) {
      $('body').css('padding', '0px');
    })
}
$(document).ready(function(e) {
    
    redimensionarDiv();
    
    $('#btnRecarregarCaptcha').click(function(e) {
       img = $('#captcha img');
       
       $('#captcha img').attr('src', img.attr('src')).fadeIn(200);   
       $('input[name="txtCaptcha"]').val('');
       return false;
    });
    
    window.onresize = function(event) {
        redimensionarDiv();
    }
    
    
    $('#formResposta').keypress(function(e){
        if (e.keyCode == 13) {               
            e.preventDefault();
            return false;
          }
        //$('#btnConfirmar').click();
    })
    
    
    $('#btnConfirmar').click(function(e) {
        $('#formAssinar').submit();
//       $.ajax({
//            type: "POST",
//            url: '<?php echo appConf::caminho ?>home/confirmar',
//            success: function(e) {
//               if(e == "erro") {
//                   $('input[name="txtCaptcha"]').val('');
//                   $('#btnRecarregarCaptcha').click();
//               } else {
//                   $('#modalResposta').modal('hide');
//                   modalAguarde('show');
//                   //alert('<?php print appConf::caminho ?>home/gerarPDF/'+e);
//                   window.location = '<?php print appConf::caminho ?>home/gerarPDF/'+e;
//                   setTimeout(redirecionar, 5000);
//               }
//            }
//       }) 
    });
    
    
    
    $('#modalResposta').on('show.bs.modal', function (event) {

        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('ans') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
      
        $('input[name="txtResposta"]').val(recipient);
        $('#btnRecarregarCaptcha').click();
        $('input[name="txtCaptcha"]').val('');
        
        $('#S').show();
        $('#N').show();
        
        //alert('recipient: '+recipient + ' / ' + '<?php echo base64_encode('S'); ?>');
        
        if(recipient == '<?php echo base64_encode('S'); ?>') {
            $('#N').hide();
        } else {
            $('#S').hide();
        }
        
        
      });
    
});

function redirecionar() {
    window.location = '<?php print appConf::caminho ?>';
}


</script>

<div style="margins-top:20px;"></div>

<form id="formAssinar" action="<?php echo appConf::caminho ?>home/confirmar" method="post">
    <input type="hidden" name="txtIdCadastro" value="<?php echo  $cadastro->getIdCadastro() ?>" />
</form>

<div class="container">
    
    <div class="row">
        <div class="col-lg-12">

<!--            <h5>
                <div class="alert alert-gray" role="alert"> <strong><span class="glyphicon glyphicon-info-sign"></span> Atenção:</strong> Leia o documento e dê a sua resposta para continuar a acessar o portal. </div>
            </h5>-->
            
            <h2><strong><span class="glyphicon glyphicon-file"></span> <?php print $cadastro->termo->getNome(); ?></strong></h2>
            <h4 class="bg-info" style="padding:10px;"><small class="text-info "><strong>Atenção: </strong>Leia atentamente todo o documento abaixo e dê a sua resposta no final da página. </small></h4>
            <hr>
            
            <div class="panel panel-default">
                <div class="panel-body">
                    <div class="doc-texto">
                        <?php echo $texto; ?>
                    </div>
                </div>
            </div>
          
            
            
        </div>
    </div>
    
    <div class="row" id="buttons">
        
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <button type="button" data-ans="<?php echo base64_encode('S'); ?>" class="btn btn-lg btn-block btn-success" data-toggle="modal" data-target="#modalResposta"><span class="glyphicon glyphicon-thumbs-up"></span> Li e ESTOU de acordo!</button>
        </div>
    </div>
    
    <br>
    
</div>    




<div class="modal fade" id="modalResposta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modadl-sm" role="document">
    <div class="modal-content ">
      <div class="modal-header btn-primary">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Confirmar - <?php echo $cadastro->termo->getNome() ?></h4>
      </div>
      <div class="modal-body">
          
         
 
          <div class="row" >
              
              
              <div class="col-lg-12">
                  <div class="text-success" id="S">
                   <div class="panel panel-success">
                <div class="panel-body bg-success">   
                    <p class="text-center"><?php print $titulo; ?></p>
                   <h1 class="text-center"><span class="glyphicon glyphicon-thumbs-up"></span></h1>
                   <h3 class="text-center"><b>Li e ESTOU de acordo</b></h3>
                </div></div>
                  </div>
                  
                <div class="text-danger" id="N">
                    <div class="panel panel-danger">
                <div class="panel-body bg-danger">  
                    <p class="text-center"><?php print $titulo; ?></p>
                   <h1 class="text-center"><span class="glyphicon glyphicon-thumbs-down"></span></h1>
                   <h2 class="text-center"><b>Li e NÃO ESTOU de acordo </b></h2>
                  </div>          
                    </div></div>
              </div>
              
              
              <div class="col-lg-6">
                  
                  
                  
                  
              </div>
              
              <div class="col-lg-12">
                  <h5 class="bg-warnisng text-center" style="padding:5px;"><small class="text-gray "><strong>Atenção: </strong>Esta operação não poderá ser cancelada ou alterada. </small></h5>
              </div>
          </div>
        
          
          
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Fechar</button>
        <button type="button" class="btn btn-primary" id="btnConfirmar"><span class="glyphicon glyphicon-ok"></span> Confirmar</button>
      </div>
    </div>
  </div>
</div>