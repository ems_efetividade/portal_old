<?php

require_once('appConexao.php');
require_once('../../plugins/mpdf57/mpdf.php');


function fn_formatarData($data) {
    if($data != "") {

        $dataHora = explode(" ", $data);

        if(count($dataHora) > 0) {
                $campos = explode("-", $dataHora[0]);
                $hora = explode(".", $dataHora[1]);
        } else {
                $campos = explode("-", $data);
                $hora = '';
        }

        if(count($campos) == 1) {
                $campos = explode("/", $data);
                return $campos[2]."-".$campos[1]."-".$campos[0];
        } else {
                return $campos[2]."/".$campos[1]."/".$campos[0].' '.$hora[0];
        }

    }
}

function fn_dadoSessao($chave) {
    @session_start();
    return $_SESSION[$chave];
}

function fn_formatarMoedaBRL($valor=0) {
    return number_format($valor, 2, ",", ".");
}

function fn_formatarMoedaSQL($valor) {
    return str_replace(",", ".", str_replace(".", "", $valor));
}

function fn_substituirAlias($de, $para, $texto) {
    $texto = str_replace($de, $para, $texto);
    return $texto;
}



function fn_gerarPDF($html='', $css='', $nomeArquivo='', $salvarEm='') {
    ob_start();
    $html = utf8_decode(urldecode($html));
    $nome_arquivo = $nomeArquivo;
    $arquivo_css = $css;

    //$stylesheet = file_get_contents('css/css_imprimir_print.css');
    $pdf = new mPDF();
    $pdf->allow_charset_conversion=true;
    $pdf->showImageErrors = true;
    $pdf->charset_in='iso-8859-1';
    $pdf->SetDisplayMode('fullpage');
    //$pdf->SetFooter('{DATE d/m/y H:i:s}||{PAGENO}');
    //$pdf->WriteHTML($stylesheet,1);
    if($arquivo_css != "") {
            $stylesheet = file_get_contents(appConf::caminho.'public/css/'.$arquivo_css);
            $pdf->WriteHTML($stylesheet,1); // The parameter 1 tells that this is css/style only and no body/html/text
    }
    $pdf->WriteHTML($html);
    
    $t = ($salvarEm == '') ? 'D' : 'F';
    
    $pdf->Output($salvarEm.$nome_arquivo.'.pdf', $t);
    //exit();
}
    
    
    
