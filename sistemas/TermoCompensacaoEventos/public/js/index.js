//### função que chama uma pagina em ajax

//
//$(document).on('hidden.bs.modal', function (e) {
//  $('body').css('padding', '0px');
//});
//
//
//$(document).on('hidden.bs.modal', '.modal', function () {
//    $('.modal:visible').length && $(document.body).addClass('modal-open');
//});

$('.maskData').mask('00/00/0000');
$('.maskMoney').mask('000.000.000.000.000,00', {reverse: true});
$('.maskNumber').mask('000');
$('.maskInt').mask('0#');

function requisicaoAjax(caminho) {
	var requisicao;
	
	//aguarde(1);
	
	$.ajaxSetup({async:false});
	
	$.post(caminho, function(retorno) {
		requisicao = retorno
	}).fail(function(xhr, textStatus, errorThrown) {
		//alert('ERRO: '+ xhr.responseText + ' ' +caminho);
  	});
	
	$.ajaxSetup({async:true});


	//aguarde(0);
	return requisicao;
}

function msgBox(texto) {
	$('#modalMsgBox #msgboxTexto').html(texto);
	$('#modalMsgBox').modal('show');
}	

function enviarFormulario(form, callback, msgbox) {

	$.ajax({
            type: "POST",
            url: form.attr('action'),
            data: form.serialize(),
            success: function(u) {

                if(u == '') {
                       window.location.href = $(location).attr('href');
                } else {

                    msgBox(u);
                    $('#alertErro').html(u).show();
                    setTimeout(function() {
                            $('#alertErro').hide();
                    }, 5000);
                }
            }
       });
}

function modalAguarde(ctrl) {
    $('#modalAguarde').modal(ctrl);

    $('#modalAguarde').on('shown.bs.modal', function (e) {
      $('body').css('padding', '0px');
    });

    $('#modalAguarde').on('hidden.bs.modal', function (e) {
      $('body').css('padding', '0px');
    })
}


function maskData(campo){
    
    cData = campo.val();
    
    if(cData.length == 2 || cData.length == 5) {
        cData = cData + '/';
        campo.val(cData);
    } 
}


